export class JsLogger {

    constructor(){
        this.LOG_ENABLED = process.env.REACT_APP_LOG_ENABLED === 'true' || false;
        this.INFO_ENABLED = process.env.REACT_APP_INFO_ENABLED === 'true' || false;
        this.WARNING_ENABLED = process.env.REACT_APP_WARNING_ENABLED === 'true' || false;
        this.ERROR_ENABLED = process.env.REACT_APP_ERROR_ENABLED === 'true' || false;
    }

    _log = (...args) => (this.LOG_ENABLED === true || localStorage.getItem('LOG_ENABLED') === 'true') && console.log.apply(console, Array.prototype.slice.call(args));
    _info = (...args) => (this.INFO_ENABLED === true || localStorage.getItem('INFO_ENABLED') === 'true') && console.info.apply(console, Array.prototype.slice.call(args));
    _warn = (...args) => (this.WARNING_ENABLED === true || localStorage.getItem('WARNING_ENABLED') === 'true') && console.warn.apply(console, Array.prototype.slice.call(args));
    _error = (...args) => (this.ERROR_ENABLED === true || localStorage.getItem('ERROR_ENABLED') === 'true') && console.error.apply(console, Array.prototype.slice.call(args));
}