/* eslint-disable no-unused-vars */
import { Component } from 'react';
import API from '../shared/eaxios';

export class UserBusiness extends Component {

    constructor(){
        super();
        this.sessionKey = null;
        this.storageType = 'sessionStorage';
        this.getData();
    }

    getData = async (i=0) => {
        if(this.checkCredentials() === true){
            return Promise.all([this.callAPI('user', 'user_data'), this.callAPI('business', 'business_data')]).then(data => {
                return data[i];
            });
        }else console.warn('Failed to prefetch user and business data');
    }

    checkCredentials = () => {
        const sessionKey = localStorage.getItem('sessionKey') || '';
        if(sessionKey) return true;
        return false;
    }

    callAPI = (path, key) => {
        return new Promise((resolve, reject) => {
            let cacheData = window[this.storageType].getItem(key) || '';
            try{
                cacheData = JSON.parse(cacheData);
            }catch(e){
                window[this.storageType].removeItem(key);
                cacheData = null;
            }
    
            if(cacheData) resolve(cacheData);
            else{
                API.get('/'+path).then(response => {
                    if(response.status === 200){
                        window[this.storageType].setItem(key, JSON.stringify(response.data));
                        resolve(response.data);
                    }else reject(response);
                });
            }
        });
    }
    
    getPermissions = () => {
        return new Promise((resolve, reject) => {
            try{
                this.getData(0).then(data=> {
                    let authArray = [];
                    authArray = data.roleEntities[0].permissions.map(temp => {
                        return temp.authority;
                    });
                    resolve(authArray);
                });                
            }catch(e){
                reject(e);
            }
        });
    }

    getUserDetails = () => this.getData(0);

    getBusinessDetails = () => this.getData(1);

    clearUserDetails = () => window[this.storageType].removeItem('user_data');

    clearBusinessDetails = () => window[this.storageType].removeItem('business_data');

    clearUserBusinessData = () => {
        this.clearUserDetails();
        this.clearBusinessDetails();
    }











}
