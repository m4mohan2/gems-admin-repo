import axios from './../shared/eaxios';

//export class LogoutService {

export let logOutHandler = () => {
    console.log('logout');
    axios
        .get('/user/logout')
        .then(res => {
            console.log(res);
            this.props.history.push('/');
        })
        .catch(err => {
            console.log(err);
        });
};

//}


