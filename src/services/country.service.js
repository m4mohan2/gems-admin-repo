/*import { Component } from 'react';
import API from './../shared/eaxios';
//import {BrowserStorage} from "./browserStorage.service";

export class CountryService extends Component{

    keyName = 'cdata_V2';
    storageType = 'localStorage';
    sessionKey = localStorage.getItem('sessionKey') || null;

    // eslint-disable-next-line no-useless-constructor
    constructor(){
        super();
        this.clearOldData();
    }

    clearOldData = () => {
        //country_data
        //countryData_v2
        window[this.storageType].removeItem('country_data');
        window[this.storageType].removeItem('countryData_v2');
    }


    callApi = () => {
        if(this.sessionKey) return API.get('/country');
        return new Promise.reject();
    }

    processData = (data) => {

        return new Promise((resolve) => {

            const countryData = {
                countries : [],
                cities : [],
                states : []
            };

            data.map(country => {
                
                countryData.countries.push({
                    id: country.id,
                    countryName: country.countryName,
                    countryCode: country.code
                });

                if(Array.isArray(country.stateRequests)) {
                    for (const k in country.stateRequests) {
                        if (country.stateRequests.hasOwnProperty(k)) {
                            const stateElement = country.stateRequests[k];
                            countryData.states.push({
                                id: stateElement.id,
                                stateName: stateElement.stateName,
                                countryId: country.id,
                                countryCode: country.code,
                                stateCode: stateElement.code
                            });

                            // City Assign
                            if (stateElement.hasOwnProperty('cityRequests')) {
                                // eslint-disable-next-line no-loop-func
                                Array.isArray(stateElement.cityRequests) && stateElement.cityRequests.map(cityElement => {
                                    countryData.cities.push({
                                        id: cityElement.id,
                                        cityName: cityElement.name,
                                        stateId: stateElement.id,
                                        stateCode: stateElement.code
                                    });
                                });
                            }
                        }
                    }
                    resolve(countryData);
                }
            });
        });
    }

    storeData = (data) => Promise.resolve(window[this.storageType] && window[this.storageType].setItem(this.keyName, JSON.stringify(data)));

    readData = () => {
        let countryData = window[this.storageType].getItem(this.keyName) || null;
        try{
            return JSON.parse(countryData);
        }catch(e){
            window[this.storageType].removeItem(this.keyName);
            countryData = null;
        }
    }

    getAllData = () => {
        console.log('I m there @@@@@@@@@@@----------------------------------------------');
        if(this.sessionKey){
            return this.callApi().then(response => {
                console.log('I m there----------------------------------------------');
                if(response.status === 200) return this.processData(response.data);
                else throw new Error('No response from the API');
            })
                .then(data => this.storeData(data)).then(() => this.readData()).catch(err => {
                    console.log('CountryService error ', err);
                });
        }//else do nothing
    }

    getData = (key=null) => {
        try{
            return new Promise(async (resolve) => {
                let countryData = this.readData();            
                if(countryData) resolve(key && countryData[key] ? countryData[key] : countryData);
                else  countryData = await this.getAllData();
                resolve(key && countryData[key] ? countryData[key] : countryData);
            });
        }catch(err){
            console.log('CountryService error ', err);
        }
    }

    // getCountryCode( id = 0) {
    //     if(!id) return [];

    //     const countries = this.getCountries();

    //     const newCountries = countries.filter( country => {
    //         return Number(country.id) === Number(id);
    //     });
    //     return newCountries.length ? newCountries[0].countryCode : ''; 
    // }

    // getStateCode( id = 0) {
    //     if(!id) return [];

    //     const states = this.getStates();

    //     const newStates = states.filter( state => {
    //         return Number(state.id) === Number(id);
    //     });
    //     return newStates.length ? newStates[0].stateCode : '';
    // }
} 
*/

import API from './../shared/eaxios';

export class CountryService {

    keyName = 'cDataV2';
    storageType = 'localStorage';

    // eslint-disable-next-line no-useless-constructor
    constructor() {
        this.clearOldData();
    }

    clearOldData = () => {
        const cDataLoadTime = window[this.storageType].getItem('cDataLoadTime') || null;
        if (cDataLoadTime) {
            const cDataCallTime = (new Date()).getTime();
            let cDataCallDiff = cDataCallTime - parseInt(cDataLoadTime);
            cDataCallDiff /= 1000;
            cDataCallDiff /= 60;
            cDataCallDiff = Math.abs(Math.round(cDataCallDiff));

            if (cDataCallDiff > 1440) {
                window[this.storageType].removeItem('cDataLoadTime');
                window[this.storageType].removeItem(this.keyName);
            }
        } else {
            window[this.storageType].removeItem('cdata_V2'); // Old Key Name
            window[this.storageType].removeItem(this.keyName);
        }
    }


    callApi = () => {
        return API.get('/country');
    }

    processData = (data) => {

        return new Promise((
            resolve, 
            //reject
        ) => {

            const countryData = {
                countries: [],
                cities: [],
                states: []
            };

            data.map(country => {

                countryData.countries.push({
                    id: country.id,
                    countryName: country.countryName,
                    countryCode: country.code
                });

                if (Array.isArray(country.stateRequests)) {
                    for (const k in country.stateRequests) {
                        if (country.stateRequests.hasOwnProperty(k)) {
                            const stateElement = country.stateRequests[k];
                            countryData.states.push({
                                id: stateElement.id,
                                stateName: stateElement.stateName,
                                countryId: country.id,
                                countryCode: country.code,
                                stateCode: stateElement.code
                            });

                            // City Assign
                            if (stateElement.hasOwnProperty('cityRequests')) {
                                // eslint-disable-next-line no-loop-func
                                Array.isArray(stateElement.cityRequests) && stateElement.cityRequests.map(cityElement => {
                                    countryData.cities.push({
                                        id: cityElement.id,
                                        cityName: cityElement.name,
                                        stateId: stateElement.id,
                                        stateCode: stateElement.code
                                    });
                                });
                            }
                        }
                    }
                    resolve(countryData);
                }
            });
        });
    }

    storeData = (data) => {
        window[this.storageType].setItem('cDataLoadTime', (new Date().getTime()));
        return Promise.resolve(window[this.storageType]
            && window[this.storageType].setItem(this.keyName, JSON.stringify(data)));
    }

    readData = () => {
        let countryData = window[this.storageType].getItem(this.keyName) || null;
        try {
            return JSON.parse(countryData);
        } catch (e) {
            window[this.storageType].removeItem(this.keyName);
            countryData = null;
        }
    }

    getAllData = () => {
        return this.callApi()
            .then(response => {
                if (response.status === 200) return this.processData(response.data);
                else throw new Error('No response from the API');
            })
            .then(data => this.storeData(data)).then(() => this.readData()).catch(err => {
                console.log('CountryService error ', err);
            });
    }

    getData = (key = null) => {
        try {
            return new Promise(async (resolve) => {
                let countryData = this.readData();
                if (countryData) resolve(key && countryData[key] ? countryData[key] : countryData);
                else countryData = await this.getAllData();
                resolve(key && countryData[key] ? countryData[key] : countryData);
            });
        } catch (err) {
            console.log('CountryService error ', err);
        }
    }
} 
