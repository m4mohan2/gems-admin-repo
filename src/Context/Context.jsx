import React,{
    useState, 
    //useEffect, 
    createContext
} from 'react';
import PropTypes from 'prop-types';
import axios from './../shared/eaxios';

let _BdataFlag=false;
const Initialbusinessdata = [{ id: 0, businessName: 'loading...' }];

export const Context = createContext();
export const Consumer = Context.Consumer;
export const ContextProvider =  props => {

    const [businesses, setBusiness] = useState(()=> { 
        axios.get('/business/uniqueBusiness')
            .then(res => { _BdataFlag=true; setBusiness(res.data);})
            .catch(err => console.log('uniqueBusiness', err));
    });

    //useEffect(()=>{},[]);

    return (
        <div>
            <Context.Provider value={{ 
                businessdata: [(_BdataFlag ? businesses: Initialbusinessdata), setBusiness]
            }}>
                {props.children}
            </Context.Provider>
        </div>
    );

};

ContextProvider.propTypes = {
    children: PropTypes.object,
};
