import React from 'react';
//import Loader from 'react-loader-spinner';
import './LoadingSpinner.scss';
import loaderImg from '../../assets/gems-loader.gif';

const LoadingSpinner2 = () => (
	<div className="loadingSpinner">
		{/* <Loader 
            type='ThreeDots'
            color='#00BFFF'
            height='100'	
            width='100'
        /> */}
		<img src={loaderImg} alt="GEMS" className="loaderImg" />
	</div>
);

export default LoadingSpinner2;
