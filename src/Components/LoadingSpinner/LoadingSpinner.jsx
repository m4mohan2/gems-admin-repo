import React from 'react';
//import Loader from 'react-loader-spinner';
import './LoadingSpinner.scss';
import loaderImg from '../../assets/gems-loader-dark.gif';

const LoadingSpinner = () => (
	<div className="loadingSpinner">
		{/* <Loader 
            type='ThreeDots'
            color='#00BFFF'
            height='100'	
            width='100'
        /> */}
		<img src={loaderImg} alt="GEMS" className="loaderImg" />
	</div>
);

export default LoadingSpinner;
