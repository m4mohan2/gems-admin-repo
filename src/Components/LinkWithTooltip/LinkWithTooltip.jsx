import React from 'react';
import { Tooltip, OverlayTrigger} from 'react-bootstrap';
import PropTypes from 'prop-types';
function LinkWithTooltip({ 
    id, 
    children, 
    href, 
    tooltip 
}) {
    return (
        <OverlayTrigger
            overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
            placement="top"
            delayShow={300}
            delayHide={150}
        >
            <a href={href}>{children}</a>
        </OverlayTrigger>
    );
}

LinkWithTooltip.propTypes = {
    id: PropTypes.string,
    children: PropTypes.object,
    href: PropTypes.object,
    tooltip: PropTypes.string,
};
export default LinkWithTooltip;
