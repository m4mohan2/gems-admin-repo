import React, { Component, Fragment } from 'react';
import {
    Button,
    Modal,
} from 'react-bootstrap';
import PropTypes from 'prop-types';
class ModalComponent extends Component {
    constructor(props, context) {
        super(props, context);

        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);

        this.state = {
            show: false,
        };
    }

    handleClose() {
        this.setState({ show: false });
    }

    handleShow() {
        this.setState({ show: true });
    }

    render() {
        console.log('from modal', this.props.show);
        return (
            <Fragment>
                
                <Modal
                    show={this.props.show}
                    onHide={this.props.onHide}
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                >
                    <Modal.Header closeButton>
                        <Modal.Title>Modal heading</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Woohoo, you are reading this text in a modal!</Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.handleClose}>
                            Close
                        </Button>
                        <Button variant="primary" onClick={this.handleClose}>
                            Save Changes
                        </Button>
                    </Modal.Footer>
                </Modal>
            </Fragment>
        );
    }
}
ModalComponent.propTypes = {
    onHide: PropTypes.func,
    show: PropTypes.bool
};
export default ModalComponent;
