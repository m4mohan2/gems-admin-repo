import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

const SortingOrder = (props) => {
    //console.log('Sorting props', props);
    return (
        <Fragment>
            <div
                className={(props.propertyName === props.propName) && (props.orderBy === props.propOrder) ? 'activeSorting' : null}
                onClick={() => props.onClickSort(props.propertyName, props.orderBy)}
            >
                <span className={`glyphicon glyphicon-triangle-${props.directionIcon}`} aria-hidden="true">&nbsp;</span>
            </div>
        </Fragment>
    );
};

SortingOrder.propTypes = {
    propertyName: PropTypes.string,
    orderBy: PropTypes.bool,
    onClickSort: PropTypes.func,
    directionIcon: PropTypes.string,
    propName: PropTypes.string,
    propOrder: PropTypes.bool,
};

export default SortingOrder;
