import React from 'react';
import LogoTop from '../../assets/logo-white.png';

const logo = () => (
	<img src={LogoTop} alt="Logo" />
);

export default logo;
