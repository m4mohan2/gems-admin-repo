import React, { memo } from 'react';

const BusinessName = ({ businessName }) => (
    console.log('businessName component'),
    
    businessName.map( i => (
        <React.Fragment key={i.id} >
            <option value={i.id}> {i.businessName}</option>
        </React.Fragment>
    )
    )
);

export default memo(BusinessName);
