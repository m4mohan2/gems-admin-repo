/* eslint-disable react/prop-types */
import React, { Fragment } from 'react';

const LineAddress = ({ addressLine }) => {
    console.log('##############################addressLine#####################################', addressLine);
    // const ENABLE_ADDRESS_LINE =
    // process.env.REACT_APP_ENABLE_ADDRESS_LINE &&
    // process.env.REACT_APP_ENABLE_ADDRESS_LINE;
  
    // if (ENABLE_ADDRESS_LINE && ENABLE_ADDRESS_LINE === 'true') {
    //     console.log(' ENABLE_ADDRESS_LINE ### TRUE');
    return (
        <Fragment>
            {addressLine.addressLine1 && addressLine.addressLine1 !== '' ? (
                <Fragment>
                    {addressLine.addressLine1}
                    <br />
                </Fragment>
            ) : null}
            {addressLine.addressLine2 && addressLine.addressLine2 !== '' ? (
                <Fragment>{addressLine.addressLine2}, </Fragment>
            ) : null}
            {addressLine.addressLine3 && addressLine.addressLine3 !== '' ? (
                <Fragment>{addressLine.addressLine3}, </Fragment>
            ) : null}
            {addressLine.addressLine4 && addressLine.addressLine4 !== '' ? (
                <Fragment>{addressLine.addressLine4}, </Fragment>
            ) : null}
            {addressLine.addressLine5 && addressLine.addressLine5 !== '' ? (
                <Fragment>{addressLine.addressLine5}, </Fragment>
            ) : null}
        </Fragment>
    );
    // } else {
    //     return <Fragment>{addressLine.address}</Fragment>;
    // }
};

export default LineAddress;
