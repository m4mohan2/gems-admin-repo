/* eslint-disable */
import React from 'react';
import NumberFormat from 'react-number-format';

export default ({ value }) => {
    return (
        value < 0 ? (<NumberFormat
            value={Math.abs(value)}
            displayType={'text'}
            thousandSeparator={true}
            prefix={'$'}
            decimalScale={2}
            fixedDecimalScale={true}
            thousandsGroupStyle={'thousand'}
            renderText={value => <span>({value})</span>}
        />) : (<NumberFormat
            value={value}
            displayType={'text'}
            thousandSeparator={true}
            prefix={'$'}
            decimalScale={2}
            fixedDecimalScale={true}
            thousandsGroupStyle={'thousand'}
            renderText={value => <span>{value}</span>}
        />)
    
    );
};
