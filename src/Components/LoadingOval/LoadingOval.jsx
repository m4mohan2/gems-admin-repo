import Loader from 'react-loader-spinner';
import React from 'react';
import './LoadingOval.scss';

const LoadingOval = () => (
	<div className="loadingOval">
		<Loader
			type="Oval"
			color='#fff'
			height={15}
			width={15}
		/>
	</div>
);

export default LoadingOval;
