
export const formatDate = (date, showTime = false, separator = '-') => {
    if (date instanceof Date && !isNaN(date)) {
        // Year
        const year = date.getFullYear();
        // Month
        let month = date.getMonth() + 1;
        month = month < 10 ? '0' + month : month;
        // Day
        let day = date.getDate();
        day = day < 10 ? '0' + day : day;

        let formatedDate = year + separator + month + separator + day;
        if (showTime) {
            formatedDate +=
                date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
        }

        return formatedDate;
    } else {
        throw new Error('Invalid date format given');
    }
};
