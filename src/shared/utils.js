import { AES, enc } from 'crypto-js';

/**
 * AES Encryption
 * @param { string } str 
 */
export const aesEncrypt = ( str ) => {
    if( !str ) return false;

    const bytes = AES.encrypt(str, process.env.REACT_APP_OAUTH_SKEY);
    return bytes.toString();
};

/**
 * AES Decryption
 * @param { string } str 
 */
export const aesDecrypt = ( str ) => {
    if( !str ) return false;

    const bytes = AES.decrypt(str, process.env.REACT_APP_OAUTH_SKEY);
    return bytes.toString( enc.Utf8 );
};
