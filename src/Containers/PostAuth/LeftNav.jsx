/* eslint-disable no-unused-vars */
import React, { Component, useRef } from 'react';
import PropTypes from 'prop-types';
import { Image, Collapse } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
//import Business from './../../assets/business.png';
//import Home from './../../assets/home.svg';
//import Transactions from './../../assets/transactions.png';
//import Vendor from './../../assets/vendor.png';
//import paymentFiles from './../../assets/paymentFiles.png';
//import businessArchive from './../../assets/businessArchive.png';
//import Bank from './../../assets/bank.png';
//import BankHolidays from './../../assets/bankholiday.png';
import { logOutHandler } from './../../services/logout.service';
import { connect } from 'react-redux';
import { logout } from './../../redux/actions/auth';
import { handelUser } from './../../redux/actions/username';
import { IoIosRemove } from 'react-icons/io';
class leftNav extends Component {
	state = {
		facility: []
	}

	componentDidMount() {
		this.setState({ facility: this.props.facility });
	}

	logOutHandler = () => {
		this.props.handleLogout();
	}


	render() {
		// console.log(this.state.facility);
		return (
			<ul className="list-unstyled components">
				<li>
					<NavLink to="/dashboard/home">
						<i className="fa fa-home" aria-hidden="true"></i>
						<span className="textLink">Dashboard</span>
					</NavLink>
				</li>
				{this.state.facility.length !== 0 && this.state.facility[1].status === '1' ?
					<li>
						<NavLink to="/dashboard/users">
							<i className="fa fa-user-circle-o" aria-hidden="true"></i>
							<span className="textLink">Gemster</span>
						</NavLink>
					</li> : ''}
				{this.state.facility.length !== 0 && this.state.facility[2].status === '1' ?
					<li>
						<NavLink to="/dashboard/gems">
							<i className="fa fa-diamond" aria-hidden="true"></i>
							<span className="textLink">Gems</span>
						</NavLink>
					</li> : ''}
				{this.state.facility.length !== 0 && this.state.facility[3].status === '1' ?
					<li>
						<NavLink to="/dashboard/coupon">
							<i className="fa fa-tags" aria-hidden="true"></i>
							<span className="textLink">Coupon</span>
						</NavLink>
					</li>
					: ''}
				{/* {this.state.facility.length !== 0 && this.state.facility[4].status === '1' ?
					<li>
						<NavLink to="/dashboard/gems-category">
							<i className="fa fa-list-ul" aria-hidden="true"></i>
							<span className="textLink">Gems Category</span>
						</NavLink>
					</li>
					: ''} */}
				{this.state.facility.length !== 0 && this.state.facility[4].status === '1' ?
					<li>
						<NavLink to="/dashboard/category">
							<i className="fa fa-list-ul" aria-hidden="true"></i>
							<span className="textLink">Category</span>
						</NavLink>
					</li>
					: ''}
				{this.state.facility.length !== 0 && this.state.facility[14].status === '1' ?
					<li>
						<NavLink to="/dashboard/subcategory">
							<i className="fa fa-list-ul" aria-hidden="true"></i>
							<span className="textLink">Subcategory</span>
						</NavLink>
					</li>
					: ''}
				{this.state.facility.length !== 0 && this.state.facility[15].status === '1' ?
					<li>
						<NavLink to="/dashboard/claimlist">
							<i className="fa fa-users" aria-hidden="true"></i>
							<span className="textLink">Claim List</span>
						</NavLink>
					</li> : ''}
				{this.state.facility.length !== 0 && this.state.facility[0].status === '1' ?
					<li>
						<NavLink to="/dashboard/role">
							<i className="fa fa-user-circle-o" aria-hidden="true"></i>
							<span className="textLink">Role Permission</span>
						</NavLink>
					</li>
					: ''}
				{this.state.facility.length !== 0 && this.state.facility[5].status === '1' ?
					<li>
						<NavLink to="/dashboard/cms">
							<i className="fa fa-object-group" aria-hidden="true"></i>
							<span className="textLink">CMS</span>
						</NavLink>
					</li>
					: ''}
				{this.state.facility.length !== 0 && this.state.facility[6].status === '1' ?
					<li>
						<NavLink to="/dashboard/news">
							<i className="fa fa-newspaper-o" aria-hidden="true"></i>
							<span className="textLink">News</span>
						</NavLink>
					</li>
					: ''}
				{this.state.facility.length !== 0 && this.state.facility[7].status === '1' ?
					<li>
						<NavLink to="/dashboard/homepage">
							<i className="fa fa-home" aria-hidden="true"></i>
							<span className="textLink">Website Home</span>
						</NavLink>
					</li>
					: ''}
				{this.state.facility.length !== 0 && this.state.facility[8].status === '1' ?
					<li>
						<NavLink to="/dashboard/testimonial">
							<i className="fa fa-commenting" aria-hidden="true"></i>
							<span className="textLink">Testimonial</span>
						</NavLink>
					</li>
					: ''}
				{this.state.facility.length !== 0 && this.state.facility[9].status === '1' ?
					<li>
						<NavLink to="/dashboard/faq">
							<i className="fa fa-question-circle" aria-hidden="true"></i>
							<span className="textLink">Faq</span>
						</NavLink>
					</li>
					: ''}
				{this.state.facility.length !== 0 && this.state.facility[10].status === '1' ?
					<li>
						<NavLink to="/dashboard/services">
							<i className="fa fa-wrench" aria-hidden="true"></i>
							<span className="textLink">Services</span>
						</NavLink>
					</li>
					: ''}
				{this.state.facility.length !== 0 && this.state.facility[11].status === '1' ?
					<li>
						<NavLink to="/dashboard/license">
							<i className="fa fa-user-plus" aria-hidden="true"></i>
							<span className="textLink">Membership License</span>
						</NavLink>
					</li>
					: ''}
				{this.state.facility.length !== 0 && this.state.facility[12].status === '1' ?
					<li>
						<NavLink to="/dashboard/providers">
							<i className="fa fa-cogs" aria-hidden="true"></i>
							<span className="textLink">Providers</span>
						</NavLink>
					</li>
					: ''}
				{/* {this.state.facility.length !== 0 && this.state.facility[2].status === '1' ?
					<li>
						<NavLink to="/dashboard/samplechart">
							<i className="fa fa-bar-chart" aria-hidden="true"></i>
							<span className="textLink">Sample Chart</span>
						</NavLink>
					</li>
					: ''} */}
				{this.state.facility.length !== 0 && this.state.facility[13].status === '1' ?
					<li>
						<NavLink to="/dashboard/audittrails">
							<i className="fa fa-cogs" aria-hidden="true"></i>
							<span className="textLink">AuditTrails</span>
						</NavLink>
					</li>
					: ''}
				<li>
					<NavLink to="/login" onClick={this.logOutHandler}>
						<i className="fa fa-sign-out" aria-hidden="true"></i>
						<span className="textLink">Logout</span>
					</NavLink>
				</li>
			</ul>
		);
	}

}

const mapStateToPros = state => {
	return {
		facility: state.auth.facility
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		handleLogout: () => dispatch(logout()),
		handelUser: (username) => dispatch(handelUser(username))
	};
};
leftNav.propTypes = {
	handleLogout: PropTypes.func,
	handelUser: PropTypes.func,
	facility: PropTypes.any
};
export default connect(mapStateToPros, mapDispatchToProps)(leftNav);

