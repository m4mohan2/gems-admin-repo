import React, { Component } from 'react';
import Moment from 'react-moment';
import moment from 'moment';

class CouponDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    static getDerivedStateFromProps(props, state) {
        if (!state.couponDetails) {
            return {
                ...props
            };
        }
    }

    render() {
        const initialValues = { ...this.props };
        return (
            <table responsive>
                <tr>
                    <td width="30%"><b>Coupon Code</b></td>
                    <td width="10%">:</td>
                    <td width="60%">{initialValues.details.coupon}</td>
                </tr>
                <tr>
                    <td width="30%"><b>Coupon Type</b></td>
                    <td width="10%">:</td>
                    <td width="60%">{initialValues.details.coupon_type === '0' ? 'Discount' : 'Complimentary Reward'}</td>
                </tr>
                <tr>
                    <td width="30%"><b>Description</b></td>
                    <td width="10%">:</td>
                    <td width="60%">{initialValues.details.description}</td>
                </tr>
                {initialValues.details.coupon_type === '0' &&
                    <React.Fragment>
                        <tr>
                            <td width="30%"><b>Amount ($)</b></td>
                            <td width="10%">:</td>
                            <td width="60%">{initialValues.details.amount}</td>
                        </tr>
                        <tr>
                            <td width="30%"><b>Amount Type</b></td>
                            <td width="10%">:</td>
                            <td width="60%">{initialValues.details.amount_type}</td>
                        </tr>
                    </React.Fragment>
                }
                <tr>
                    <td width="30%"><b>Start Date</b></td>
                    <td width="10%">:</td>
                    <td width="60%"><Moment format="MMMM Do, YYYY HH:mm:ss">{initialValues.details.start_date}</Moment></td>
                </tr>
                <tr>
                    <td width="30%"><b>End Date</b></td>
                    <td width="10%">:</td>
                    <td width="60%"><Moment format="MMMM Do, YYYY HH:mm:ss">{initialValues.details.end_date}</Moment></td>
                </tr>
                {initialValues.details.assignuser &&
                    <React.Fragment>
                        <tr>
                            <td width="30%"><b>Assigned User Name</b></td>
                            <td width="10%">:</td>
                            <td width="60%">{initialValues.details.assignuser.first_name}&nbsp;{initialValues.details.assignuser.last_name}</td>
                        </tr>
                        <tr>
                            <td width="30%"><b>Email</b></td>
                            <td width="10%">:</td>
                            <td width="60%">{initialValues.details.assignuser_email.email}</td>
                        </tr>
                        <tr>
                            <td width="30%"><b>Assigned Date</b></td>
                            <td width="10%">:</td>
                            <td width="60%"><Moment format="MMMM Do, YYYY HH:mm:ss">{initialValues.details.assign_info[0].created_at}</Moment></td>
                        </tr>
                        {initialValues.details.assignuser.redeem_date && <tr>
                            <td width="30%"><b>Redeem Date</b></td>
                            <td width="10%">:</td>
                            <td width="60%"><Moment format="MMMM Do, YYYY HH:mm:ss">{initialValues.details.assignuser.redeem_date}</Moment></td>
                        </tr>}
                    </React.Fragment>
                }
                <tr>
                    <td width="30%"><b>Status</b></td>
                    <td width="10%">:</td>
                    <td width="60%">{initialValues.details.active_status === 0 ||
                        moment(initialValues.details.end_date).toDate() < new Date() ||
                        initialValues.details.assignuser !== null && initialValues.details.assignuser.is_redeem === 'Yes' ? 'Inactive' : 'Active'}</td>
                </tr>
                <tr>
                    <td width="30%"><b>Created Date</b></td>
                    <td width="10%">:</td>
                    <td width="60%"><Moment format="MMMM Do, YYYY HH:mm:ss">{initialValues.details.created_at}</Moment></td>
                </tr>
                <tr>
                    <td width="30%"><b>Updated Date</b></td>
                    <td width="10%">:</td>
                    <td width="60%"><Moment format="MMMM Do, YYYY HH:mm:ss">{initialValues.details.updated_at}</Moment></td>
                </tr>
                <tr>
                    <td width="30%"><b>Created By</b></td>
                    <td width="10%">:</td>
                    <td width="60%">{initialValues.details.creator.first_name}&nbsp;{initialValues.details.creator.last_name}</td>
                </tr>
                <tr>
                    <td width="30%"><b>Creater Email</b></td>
                    <td width="10%">:</td>
                    <td width="60%">{initialValues.details.creator_email.email}</td>
                </tr>
            </table>
        );
    }
}

export default CouponDetail;