import React, { Component, Fragment } from 'react';
import axios from '../../../../../shared/eaxios';
import * as AppConst from './../../../../../common/constants';
import { Button, Col, FormControl, FormGroup, Image, Modal, Row } from 'react-bootstrap';
import { Field, Form, Formik } from 'formik';
import SuccessIco from '../../../../../assets/success-ico.png';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import PropTypes from 'prop-types';
// import Select from 'react-select';

class RedeemCoupon extends Component {
    state = {
        showConfirMmsg: false,
        errorMessge: null,
        addRedeemCouponLoader: false,
        addErrorMessge: null,
        coupon_id: '',
        coupon: '',
        user: [],
        selectedOption: '',
        assignUser: '',
        editAssignCoupon: false,
        successData: '',
        assignuserdetails: ''
    };

    static getDerivedStateFromProps(props, state) {
        if (!state.couponData) {
            return {
                ...props
            };
        }
    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handleConfirmCouponClose = () => {
        this.setState({ showConfirMmsg: false });
    };

    handleSubmit = () => {
        this.setState({
            addRedeemCouponLoader: true,
        });
        let newValue = {
            coupon: this.state.coupon,
            user_id: this.state.assignUser.user_id
        };
        axios
            .post(AppConst.APIURL + '/api/redeemCoupon', newValue)
            .then(res => {
                this.setState({ successData: res });
                if (res.data.success === 'false') {
                    this.setState({
                        addRedeemCouponLoader: false,
                        addErrorMessge: res.data.message,
                    });
                    setTimeout(() => {
                        this.setState({ addErrorMessge: null });
                        this.props.handelRedeemCouponModalClose();
                    }, 5000);
                } else {
                    this.props.handelRedeemCouponModalClose();
                    this.props.handleRedeemCouponConfirMmsg();
                }
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    addRedeemCouponLoader: false,
                    addErrorMessge: errorMsg,
                });
                setTimeout(() => {
                    this.setState({ addErrorMessge: null });
                }, 5000);
            });
    };

    componentDidMount() {
        const initialValues = { ...this.props };
        this.setState({
            coupon_id: initialValues.id,
            coupon: initialValues.coupon,
            assignUser: initialValues.assignuser_email,
            assignuserdetails: initialValues.assignuser,
        });
    }

    handleChange = (selectedOption) => {
        this.setState({ selectedOption, addErrorMessge: null });
    }

    handleeditCouponEnable = () => {
        this.setState({
            assignUser: ''
        });
    }


    render() {
        return (
            <div className="addcustomerSec">
                <div className="boxBg p-35">
                    {this.state.addErrorMessge ? (
                        <div className="alert alert-danger" role="alert">
                            {this.state.addErrorMessge}
                        </div>
                    ) : null}
                    <Row>
                        <Col sm={12}>
                            {this.state.addRedeemCouponLoader ? <LoadingSpinner /> : null}
                        </Col>
                    </Row>
                    <Row className="show-grid">
                        <Col xs={12} className="brd-right">
                            <Formik
                                onSubmit={this.handleSubmit}
                            >
                                {({
                                    values,
                                }) => {
                                    return (
                                        <Form>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Coupon<span className="required">*</span></label>
                                                        <Field
                                                            name="coupon"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder=""
                                                            value={this.state.coupon ? this.state.coupon : values.coupon}
                                                            readOnly
                                                        />
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>{this.state.assignuserdetails &&
                                                            this.state.assignuserdetails.is_redeem === 'No' ? 'Select User' : 'Assigned User'}
                                                            <span className="required">*</span>
                                                        </label>
                                                        <Field
                                                            name="coupon"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder=""
                                                            value={this.state.assignUser.email}
                                                            readOnly
                                                        />
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    &nbsp;
                                                </Col>
                                            </Row>
                                            <Row className="show-grid text-center">
                                                <Col xs={12} md={12}>
                                                    <Fragment>
                                                        {this.state.assignuserdetails &&
                                                            this.state.assignuserdetails.is_redeem === 'No' ?
                                                            <Fragment>
                                                                <Button
                                                                    onClick={this.props.handelRedeemCouponModalClose}
                                                                    className="but-gray border-0 mr-2">
                                                                    Cancel
                                                                </Button>
                                                                <Button type="submit" className="blue-btn ml-2 border-0">
                                                                    Redeem
                                                                </Button>
                                                            </Fragment> :
                                                            <Button
                                                                onClick={this.props.handelRedeemCouponModalClose}
                                                                className="but-gray border-0 mr-2">
                                                                Back
                                                        </Button>
                                                        }
                                                    </Fragment>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={12}>
                                                    <p style={{ paddingTop: '10px' }}><span className="required">*</span> {this.state.assignuserdetails &&
                                                        this.state.assignuserdetails.is_redeem === 'No' ? 'These fields are required.' : 'This coupon is already redeem.'}</p>
                                                </Col>
                                            </Row>
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Col>
                    </Row>
                </div>

                {/*======  confirmation popup  ===== */}
                <Modal
                    show={this.state.showConfirMmsg}
                    onHide={this.handleConfirmCouponClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Coupon has been successfully redeem</h5>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handleConfirmCouponClose}
                            className="but-gray" > Done
                        </Button>
                    </Modal.Footer>
                </Modal>

            </div>
        );
    }
}

RedeemCoupon.propTypes = {
    handleAddConfirMmsg: PropTypes.func,
    businessId: PropTypes.number,
    token: PropTypes.any,
    handelRedeemCouponModalClose: PropTypes.func,
    handleRedeemCouponConfirMmsg: PropTypes.func,
};

export default RedeemCoupon;