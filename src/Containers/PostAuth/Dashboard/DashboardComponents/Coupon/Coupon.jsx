import React, { Component, Fragment } from 'react';
import axios from '../../../../../shared/eaxios';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import { Table, Row, Col, Modal, Image, Button } from 'react-bootstrap';
import SuccessIco from '../../../../../assets/success-ico.png';
import Pagination from 'react-js-pagination';
import EditCoupon from './EditCoupon';
import AssignCoupon from './AssignCoupon';
import AddCoupon from './AddCoupon';
import * as AppConst from './../../../../../common/constants';
import Moment from 'react-moment';
import moment from 'moment';
import { Link } from 'react-router-dom';
import refreshIcon from './../../../../../assets/refreshIcon.png';
import CouponDetails from './CouponDetail';
import RedeemCoupon from './RedeemCoupon';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class Coupon extends Component {
    state = {
        activePage: 1,
        totalCount: 0,
        itemPerPage: 10,
        sort: 1,
        field: null,
        fetchErrorMsg: null,
        couponList: [],
        loading: true,
        sortingActiveID: 1,
        statusChange: false,
        statuserrorMsg: null,
        couponId: null,
        status: null,
        couponSearchKey: '',
        viewAssignModel: false,
        assignCouponConfirMmsg: false,
        couponDetails: '',
        viewCouponDetails: false,
        viewRedeemCouponModel: false,
        redeemCouponConfirMmsg: false,
        loadingForCouponDetail: false,
        permission: []
    }

    _isMounted = false;

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handelAddModal = () => {
        this.setState({
            addModal: true,
        });
    }

    handelAddModalClose = () => {
        this.setState({
            addModal: false,
        });
    }

    sortingActive = (id) => {
        this.setState({
            sortingActiveID: id
        });
    }

    fetchCoupon = (sort = this.state.sort, field = this.state.field) => {
        this.setState({
            loading: true,
            sort: sort,
            field: field
        }, () => {
            axios.get(AppConst.APIURL +
                `/api/couponList?pageSize=${this.state.itemPerPage}&page=${this.state.sort}&coupon=${this.state.couponSearchKey}`)
                .then(res => {
                    const couponList = res.data.data.data;
                    const totalCount = res.data.data.total;
                    if (this._isMounted) {
                        this.setState({
                            couponList,
                            totalCount: totalCount,
                            loading: false
                        });
                    }
                })
                .catch(e => {
                    let errorMsg = this.displayError(e);
                    this.setState({
                        fetchErrorMsg: errorMsg,
                        loading: false
                    });
                    setTimeout(() => {
                        this.setState({ fetchErrorMsg: null });
                    }, 5000);
                });
        });
    }

    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber });
        this.fetchCoupon(pageNumber > 0 ? pageNumber : 0, this.state.field);
    };

    handleChangeItemPerPage = (e) => {
        this.setState({ itemPerPage: e.target.value },
            () => {
                this.fetchCoupon();
            });
    }

    resetPagination = () => {
        this.setState({ activePage: 1 });
    }

    rePagination = () => {
        this.setState({ activePage: 0 });
    }

    handleAddConfirMmsg = () => {
        this.setState({
            addConfirMmsg: true,
        }, () => {
            this.fetchCoupon();
        });
    }

    handleAddConfirMmsgClose = () => {
        this.setState({
            addConfirMmsg: false,
        });
    }

    handleEditConfirMmsg = () => {
        this.setState({
            viewEditModal: false,
            editConfirMmsg: true
        }, () => {
            this.fetchCoupon();
        });
    }

    handleAssignCouponConfirMmsg = () => {
        this.setState({
            viewAssignModel: false,
            assignCouponConfirMmsg: true
        }, () => {
            this.fetchCoupon();
        });
    }

    handleEditConfirMmsgClose = () => {
        this.setState({
            editConfirMmsg: false
        });
    }

    handleAssignCouponConfirMmsgClose = () => {
        this.setState({
            assignCouponConfirMmsg: false
        });
    }

    handelviewEditModal = (data) => {
        this.setState({
            viewEditModal: true,
            couponData: data
        });
    }

    handelAssignModal = (data) => {
        this.setState({
            viewAssignModel: true,
            couponData: data
        });
    }

    handelviewEditModalClose = () => {
        this.setState({
            viewEditModal: false,
        });
    }

    handelAssignModalClose = () => {
        this.setState({
            viewAssignModel: false,
        });
    }

    componentDidMount() {
        this._isMounted = true;
        const facility = this.props.facility;
        const permission = [];
        facility.map(data => {
            data.name === 'coupon' && data.permission.length !== 0 && permission.push(...data.permission);
        });
        this.setState({ permission }, () => {
            permission[1].status === true &&
                this.fetchCoupon();
        });
    }

    componentWillMount() {
        this._isMounted = false;
    }

    handelDeleteModal = (id) => {
        this.setState({
            deleteCoupon: true,
            couponId: id
        });
    }

    handleHide = () => {
        this.setState({
            statusChange: false,
            statuserrorMsg: null
        });
    }

    handleDeleteHide = () => {
        this.setState({
            deleteCoupon: false,
            deleteerrorMsg: null
        });
    }

    handleDelete(id) {
        axios.delete(AppConst.APIURL + `/api/deleteCoupon/${id}`)
            .then(res => {
                this.setState({
                    couponId: null,
                    successMessage: res.data.message,
                    deleteConfirMmsg: true
                });
                this.handleDeleteHide();
                this.fetchCoupon();
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    deleteerrorMsg: errorMsg,
                    loading: false
                });
                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);
            });
    }

    handleDeleteChangedClose = () => {
        this.setState({
            deleteConfirMmsg: false,
            successMessage: null
        });
    }

    handelStatusModal = (id, status) => {
        this.setState({
            statusChange: true,
            couponId: id,
            status: status
        });

    }

    handleStatus(id, status) {
        let updateArray = {
            'status': status
        };
        axios.put(AppConst.APIURL + `/api/statusChange/${id}`, updateArray)
            .then(res => {
                this.setState({
                    couponId: null,
                    status: null,
                    successMessage: res.data.message,
                    statusConfirMmsg: true
                });
                this.handleHide();
                this.fetchCoupon();
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    statuserrorMsg: errorMsg,
                    loading: false
                });
                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);
            });
    }

    handleStatusChangedClose = () => {
        this.setState({
            statusConfirMmsg: false,
            successMessage: null
        });
    }

    resetSearch = () => {
        this.setState({ couponSearchKey: '' });
        this.fetchCoupon();
    }

    handleChangeForSearch = (e) => {
        this.setState({ couponSearchKey: e.target.value.trim() });
    }

    handelSearch = () => {
        this.fetchCoupon();
    }

    handleCouponDetail = (code) => {
        this.setState({ loadingForCouponDetail: true });
        axios.get(AppConst.APIURL + `/api/couponDetails/${code}`)
            .then(response => {
                this.setState({
                    couponDetails: response.data,
                    viewCouponDetails: true,
                    loadingForCouponDetail: false
                });
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    fetchErrorMsg: errorMsg,
                });
                setTimeout(() => {
                    this.setState({ fetchErrorMsg: null });
                }, 5000);
            });
    }

    handleCouponDetailClose = () => {
        this.setState({ viewCouponDetails: false });
    }

    handleRedeemCoupon = (data) => {
        this.setState({
            viewRedeemCouponModel: true,
            couponData: data
        });
    }

    handelRedeemCouponModalClose = () => {
        this.setState({
            viewRedeemCouponModel: false,
        });
    }

    handleRedeemCouponConfirMmsg = () => {
        this.setState({
            viewRedeemCouponModel: false,
            redeemCouponConfirMmsg: true
        });
        this.fetchCoupon();
    }

    handleRedeemCouponConfirMmsgClose = () => {
        this.setState({ redeemCouponConfirMmsg: false });
    }

    render() {
        return (
            <div className="dashboardInner businessOuter pt-3">
                {this.state.permission[1] && this.state.permission[1].status === true ?
                    <Fragment>
                        {this.state.loadingForCouponDetail === true ? <LoadingSpinner /> : ''}
                        {this.state.permission[0] && this.state.permission[0].status === true &&
                            <Row className="pt-3">
                                <Col sm={6} md={6}>
                                </Col>
                                <Col sm={6} md={6}>
                                    <button className="btn btn-primary float-right" onClick={() => this.handelAddModal()}>Add Coupon</button>
                                </Col>
                            </Row>
                        }
                        <Row className="pt-3">
                            <Col sm={12} md={12}>
                                <div className="searchBox">
                                    <div className="row">
                                        <div className="col-12 col-md-4">
                                            <input className="form-control" type="text" placeholder="Enter coupon code here..." value={this.state.couponSearchKey} onChange={this.handleChangeForSearch} />
                                        </div>
                                        <div className="col-12 col-md-4">
                                            <button
                                                type="button"
                                                className="btn btn-primary"
                                                onClick={() => this.handelSearch()}
                                            >Search</button>
                                            <Link to="#" className='ml-3 mt-2'>
                                                <Image src={refreshIcon} onClick={() => this.resetSearch()} />
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                        <Row className="show-grid pt-1">
                            <Col sm={12} md={12}>
                                <div className="boxBg">
                                    <Table responsive hover>
                                        <thead className="theaderBg">
                                            <tr>
                                                <th>Coupon</th>
                                                <th>Coupon Type</th>
                                                <th>Amount</th>
                                                <th>Amount Type</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                                <th>Staus</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.loading ? (<tr><td colSpan={12}><LoadingSpinner /></td></tr>) :
                                                this.state.couponList.length > 0 ?
                                                    (this.state.couponList.map(coupon => (
                                                        <tr key={coupon.id}>
                                                            <td>{coupon.coupon}</td>
                                                            <td>{coupon.coupon_type === '1' ? 'Complimentary Reward' : 'Discount'}</td>
                                                            <td>{coupon.coupon_type === '1' ? '-' : coupon.amount}</td>
                                                            <td>{coupon.coupon_type === '1' ? '-' : coupon.amount_type}</td>
                                                            <td><Moment format="MMM Do, YYYY HH:mm:ss">{coupon.start_date}</Moment></td>
                                                            <td><Moment format="MMM Do, YYYY HH:mm:ss">{coupon.end_date}</Moment></td>
                                                            <td>{
                                                                (coupon.assignuser !== null) && coupon.assignuser.is_redeem === 'Yes' || moment(coupon.end_date).toDate() < new Date()
                                                                    ? <i className="fa fa-circle red" aria-hidden="true" title="Inactive"></i>
                                                                    : coupon.active_status === 1
                                                                        ? <i className="fa fa-circle green" aria-hidden="true" title="Active"
                                                                            onClick={() => this.state.permission[2] && this.state.permission[2].status === true && this.handelStatusModal(coupon.id, '0')}></i>
                                                                        : <i className="fa fa-circle red" aria-hidden="true" title="Inactive"
                                                                            onClick={() => this.state.permission[2] && this.state.permission[2].status === true && this.handelStatusModal(coupon.id, '1')}></i>
                                                            }
                                                                {coupon.assignuser === null ?
                                                                    <i className="fa fa-tasks red" aria-hidden="true" title="Coupon not assigned"></i>
                                                                    : <i className="fa fa-tasks green" aria-hidden="true" title="Coupon assigned"></i>
                                                                }
                                                                {coupon.assignuser !== null ?
                                                                    coupon.assignuser.is_redeem === 'No' ?
                                                                        <i className="fa fa-tags red" aria-hidden="true" title="Coupon not redeem"></i> :
                                                                        <i className="fa fa-tags green" aria-hidden="true" title="Coupon redeem"></i> :
                                                                    <i className="fa fa-tags red" aria-hidden="true" title="Coupon not redeem"></i>
                                                                }
                                                            </td>
                                                            <td>
                                                                {coupon.assignuser && coupon.assignuser.is_redeem === 'Yes' ?
                                                                    <React.Fragment>
                                                                        <i className="fa fa-tags ml-3" aria-hidden="true" title="View Coupon Details" onClick={() => this.handleCouponDetail(coupon.coupon)}></i>
                                                                &nbsp;
                                                                {this.state.permission[3] && this.state.permission[3].status === true &&
                                                                            <i className="fa fa-trash-o red" aria-hidden="true" title="Delete" onClick={() => this.handelDeleteModal(coupon.id)}></i>
                                                                        }
                                                                    </React.Fragment> :
                                                                    <React.Fragment>
                                                                        {moment(coupon.end_date).toDate() < new Date() ? '' :
                                                                            coupon.assignuser && this.state.permission[4] && this.state.permission[4].status === true &&
                                                                            <i className="fa fa-gift" aria-hidden="true" title="Redeem Coupon" onClick={() => this.handleRedeemCoupon(coupon)}></i>
                                                                        }
                                                                &nbsp;
                                                                {moment(coupon.end_date).toDate() < new Date() ? '' :
                                                                            this.state.permission[5] && this.state.permission[5].status === true && <i className="fa fa-tasks" aria-hidden="true" title="Assign Coupon" onClick={() => this.handelAssignModal(coupon)}></i>
                                                                        }
                                                                &nbsp;
                                                                <i className="fa fa-tags" aria-hidden="true" title="View Coupon Details" onClick={() => this.handleCouponDetail(coupon.coupon)}></i>
                                                                &nbsp;
                                                                {moment(coupon.end_date).toDate() < new Date() ? '' :
                                                                            <i className="fa fa-eye" aria-hidden="true" title="View" onClick={() => this.handelviewEditModal(coupon)}></i>
                                                                        }
                                                                &nbsp;
                                                                {this.state.permission[3] && this.state.permission[3].status === true &&
                                                                            <i className="fa fa-trash-o red" aria-hidden="true" title="Delete" onClick={() => this.handelDeleteModal(coupon.id)}></i>
                                                                        }
                                                                    </React.Fragment>
                                                                }
                                                            </td>
                                                        </tr>
                                                    )))
                                                    : this.state.fetchErrorMsg ? null : (<tr>
                                                        <td colSpan={12}>
                                                            <p className="text-center">No records found</p>
                                                        </td>
                                                    </tr>)
                                            }
                                        </tbody>
                                    </Table>
                                </div>
                            </Col>
                        </Row>
                        {this.state.totalCount ? (
                            <Row>
                                <Col md={4} className="d-flex flex-row mt-20">
                                    <span className="mr-2 mt-2 font-weight-500">Items per page</span>
                                    <select
                                        id={this.state.itemPerPage}
                                        className="form-control truncatefloat-left w-90"
                                        onChange={this.handleChangeItemPerPage}
                                        value={this.state.itemPerPage}>
                                        <option value='10'>10</option>
                                        <option value='50'>50</option>
                                        <option value='100'>100</option>
                                        <option value='150'>150</option>
                                        <option value='200'>200</option>
                                        <option value='250'>250</option>
                                    </select>
                                </Col>
                                <Col md={8}>
                                    <div className="paginationOuter text-right">
                                        <Pagination
                                            activePage={this.state.activePage}
                                            itemsCountPerPage={this.state.itemPerPage}
                                            totalItemsCount={this.state.totalCount}
                                            onChange={this.handlePageChange}
                                        />
                                    </div>
                                </Col>
                            </Row>
                        ) : null}
                    </Fragment>
                    : <h5 className="text-center p-3">You do not have any permission to view this content.</h5>}

                {/*========================= Modal for Status change =====================*/}
                <Modal
                    show={this.state.statusChange}
                    onHide={this.handleHide}
                    dialogClassName="modal-90w"
                    aria-labelledby="example-custom-modal-styling-title"
                >
                    <Modal.Body>
                        <div className="m-auto text-center">
                            <h6 className="mb-3 text-dark">Do you want to change this status?</h6>
                        </div>
                        {this.state.statuserrorMsg ? <div className="alert alert-danger my-3 text-center col-12" role="alert">{this.state.statuserrorMsg}</div> : null}
                        <div className="m-auto text-center">
                            <button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.handleHide()}>Return</button>
                            {this.state.statuserrorMsg == null ? <button className="btn btn-danger" onClick={() => this.handleStatus(this.state.couponId, this.state.status)}>Confirm</button> : null}
                        </div>

                    </Modal.Body>
                </Modal>

                {/*====== Status change confirmation popup  ===== */}
                <Modal
                    show={this.state.statusConfirMmsg}
                    onHide={this.handleStatusChangedClose}
                    className="payOptionPop"
                >
                    <Modal.Body className="text-center">
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>{this.state.successMessage}</h5>
                            </Col>
                        </Row>
                        <Button
                            onClick={this.handleStatusChangedClose}
                            className="but-gray mt-3"
                        >
                            Return
                        </Button>
                    </Modal.Body>
                </Modal>

                {/* Add Coupon Modal */}
                <Modal show={this.state.addModal}
                    onHide={this.handelAddModalClose}
                    className="right full noPadding slideModal" >
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body className="">
                        <div className="modalHeader">
                            <Row>
                                <Col md={9}>
                                    <h1>Add Coupon</h1>
                                </Col>
                            </Row>
                        </div>
                        <div className="modalBody content-body noTabs">
                            <AddCoupon
                                handleAddConfirMmsg={this.handleAddConfirMmsg}
                                handelAddModalClose={this.handelAddModalClose}
                            />
                        </div>
                    </Modal.Body>
                </Modal>

                {/*======  Add confirmation popup  ===== */}
                <Modal
                    show={this.state.addConfirMmsg}
                    onHide={this.handleAddConfirMmsgClose}
                    className="payOptionPop">
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Coupon has been successfully Generated</h5>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <Button
                                    onClick={this.handleAddConfirMmsgClose}
                                    className="but-gray">
                                    Return
                                </Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                {/* Edit Coupon Modal */}
                <Modal
                    show={this.state.viewEditModal}
                    onHide={this.handelviewEditModalClose}
                    className="right full noPadding slideModal"
                >
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body className="">
                        <div className="modalBody content-body noTabs">
                            <EditCoupon
                                {...this.state.couponData}
                                handelviewEditModalClose={this.handelviewEditModalClose}
                                handleEditConfirMmsg={this.handleEditConfirMmsg} />
                        </div>
                    </Modal.Body>
                </Modal>

                {/*====== Edit confirmation popup  ===== */}
                <Modal
                    show={this.state.editConfirMmsg}
                    onHide={this.handleEditConfirMmsgClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Coupon has been successfully edited</h5>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <Button
                                    onClick={this.handleEditConfirMmsgClose}
                                    className="but-gray"
                                >
                                    Return
                                </Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                {/*========================= Modal for Delete Coupon =====================*/}
                <Modal
                    show={this.state.deleteCoupon}
                    onHide={this.handleHide}
                    dialogClassName="modal-90w"
                    aria-labelledby="example-custom-modal-styling-title" >
                    <Modal.Body>
                        <div className="m-auto text-center">
                            <h6 className="mb-3 text-dark">Do you want to delete this Coupon?</h6>
                        </div>
                        {this.state.deleteerrorMsg ? <div className="alert alert-danger my-3 text-center col-12" role="alert">{this.state.deleteerrorMsg}</div> : null}
                        <div className="m-auto text-center">
                            <button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.handleDeleteHide()}>Return</button>
                            {this.state.deleteerrorMsg == null ? <button className="btn btn-danger" onClick={() => this.handleDelete(this.state.couponId)}>Confirm</button> : null}
                        </div>
                    </Modal.Body>
                </Modal>

                {/*====== Delete confirmation popup  ===== */}
                <Modal
                    show={this.state.deleteConfirMmsg}
                    onHide={this.handleDeleteChangedClose}
                    className="payOptionPop" >
                    <Modal.Body className="text-center">
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>{this.state.successMessage}</h5>
                            </Col>
                        </Row>
                        <Button
                            onClick={this.handleDeleteChangedClose}
                            className="but-gray mt-3"
                        >
                            Return
                        </Button>
                    </Modal.Body>
                </Modal>

                {/* Add Assign Coupon Modal */}
                <Modal show={this.state.viewAssignModel}
                    onHide={this.handelAssignModalClose}
                    className="right full noPadding slideModal" >
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body className="">
                        <div className="modalHeader">
                            <Row>
                                <Col md={9}>
                                    <h1>Assign Coupon</h1>
                                </Col>
                            </Row>
                        </div>
                        <div className="modalBody content-body noTabs">
                            <AssignCoupon
                                {...this.state.couponData}
                                handelAssignModalClose={this.handelAssignModalClose}
                                handleAssignCouponConfirMmsg={this.handleAssignCouponConfirMmsg} />
                        </div>
                    </Modal.Body>
                </Modal>

                {/*====== Assign Coupon confirmation popup  ===== */}
                <Modal
                    show={this.state.assignCouponConfirMmsg}
                    onHide={this.handleAssignCouponConfirMmsgClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Coupon has been successfully assigned</h5>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <Button
                                    onClick={this.handleAssignCouponConfirMmsgClose}
                                    className="but-gray"
                                >
                                    Return
                                </Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                {/* View coupon details Modal */}
                <Modal
                    show={this.state.viewCouponDetails}
                    onHide={this.handleCouponDetailClose}
                    className="right half noPadding slideModal"
                >
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body className="">
                        <div className="modalHeader">
                            <Row>
                                <Col md={9}>
                                    <h1>View Coupon Details</h1>
                                </Col>
                            </Row>
                        </div>
                        <div className="modalBody content-body noTabs">
                            <CouponDetails
                                {...this.state.couponDetails}
                                handleCouponDetailClose={this.handleCouponDetailClose}
                            />
                        </div>
                    </Modal.Body>
                </Modal>

                {/* Redeem Coupon Modal */}
                <Modal show={this.state.viewRedeemCouponModel}
                    onHide={this.handelRedeemCouponModalClose}
                    className="right full noPadding slideModal" >
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body className="">
                        <div className="modalHeader">
                            <Row>
                                <Col md={9}>
                                    <h1>Redeem Coupon</h1>
                                </Col>
                            </Row>
                        </div>
                        <div className="modalBody content-body noTabs">
                            <RedeemCoupon
                                {...this.state.couponData}
                                handelRedeemCouponModalClose={this.handelRedeemCouponModalClose}
                                handleRedeemCouponConfirMmsg={this.handleRedeemCouponConfirMmsg} />
                        </div>
                    </Modal.Body>
                </Modal>

                {/*====== Redeem Coupon Modal confirmation popup  ===== */}
                <Modal
                    show={this.state.redeemCouponConfirMmsg}
                    onHide={this.handleRedeemCouponConfirMmsgClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Coupon has been successfully redeem</h5>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <Button
                                    onClick={this.handleRedeemCouponConfirMmsgClose}
                                    className="but-gray"
                                >
                                    Return
                                </Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

            </div>
        );
    }
}

const mapStateToPros = state => {
    return {
        facility: state.auth.facility
    };
};
Coupon.propTypes = {
    facility: PropTypes.any
};
export default connect(mapStateToPros, null)(Coupon);