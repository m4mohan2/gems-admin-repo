import React, { Component, Fragment } from 'react';
//import * as Yup from 'yup';
import axios from '../../../../../shared/eaxios';
import * as AppConst from './../../../../../common/constants';
import { Button, Col, FormControl, FormGroup, Image, Modal, Row } from 'react-bootstrap';
import { Field, Form, Formik } from 'formik';
import SuccessIco from '../../../../../assets/success-ico.png';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import PropTypes from 'prop-types';
import Select from 'react-select';

class AssignCoupon extends Component {
    state = {
        showConfirMmsg: false,
        errorMessge: null,
        addAssignCouponLoader: false,
        addErrorMessge: null,
        coupon_id: '',
        coupon: '',
        user: [],
        userInfo: [],
        selectedOption: '',
        assignUser: '',
        editAssignCoupon: false,
        successData: '',
        assignuserdetails: ''
    };

    static getDerivedStateFromProps(props, state) {
        if (!state.couponData) {
            return {
                ...props
            };
        }
    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handleConfirmReviewClose = () => {
        this.setState({ showConfirMmsg: false });
    };

    handleConfirmReviewShow = () => {
        this.setState({ showConfirMmsg: true });
    };

    handleSubmit = () => {
        if (this.state.selectedOption === '' || this.state.selectedOption === null || this.state.selectedOption === undefined) {
            this.setState({ addErrorMessge: 'Please select user' });
        } else {
            this.setState({
                addAssignCouponLoader: true,
            });
            let newValue = {
                coupon_id: this.state.coupon_id,
                user_id: this.state.selectedOption.value
            };
            axios
                .post(AppConst.APIURL + '/api/assignCoupon', newValue)
                .then(res => {
                    this.setState({ successData: res });
                    this.props.handelAssignModalClose();
                    this.props.handleAssignCouponConfirMmsg();
                }).catch(e => {
                    let errorMsg = this.displayError(e);
                    this.setState({
                        addAssignCouponLoader: false,
                        addErrorMessge: errorMsg,
                    });
                    setTimeout(() => {
                        this.setState({ deleteErrorMessge: null });
                    }, 5000);
                });
        }
    };

    componentDidMount() {
        this.setState({ addAssignCouponLoader: true });
        const initialValues = { ...this.props };
        this.setState({
            coupon_id: initialValues.id,
            coupon: initialValues.coupon,
            assignUser: initialValues.assignuser_email,
            assignuserdetails: initialValues.assignuser
        });
        axios.get(AppConst.APIURL + '/api/userLists')
            .then(res => {
                this.setState({ addAssignCouponLoader: false, user: res.data.data });
            });
    }

    handleChange = (selectedOption) => {
        this.setState({ selectedOption, addErrorMessge: null });
        let userId = {
            user_id: selectedOption.value
        };
        axios.post(AppConst.APIURL + '/api/userInfo',userId)
            .then(res => {
                //console.log(res.data.data);
                this.setState({ userInfo: res.data.data });
            });
    console.log(this.state.userInfo);
    }

    handleeditCouponEnable = () => {
        this.setState({
            assignUser: ''
        });
    }


    render() {
        const options = [];
        this.state.user.map(data => {
            options.push({ value: data.id, label: data.email });
        });
        const colourStyles = {
            // control: styles => ({ ...styles, backgroundColor: '#313131', color: 'white' }),
            // input: styles => ({ ...styles, color: 'white' }),
            option: (provided) => ({
                ...provided,
                color: 'black',
            }),

        };
        return (
            <div className="addcustomerSec">
                <div className="boxBg p-35">
                    {this.state.addErrorMessge ? (
                        <div className="alert alert-danger" role="alert">
                            {this.state.addErrorMessge}
                        </div>
                    ) : null}
                    <Row>
                        <Col sm={12}>
                            {this.state.addAssignCouponLoader ? <LoadingSpinner /> : null}
                        </Col>
                    </Row>
                    <Row className="show-grid">
                        <Col xs={12} className="brd-right">
                            <Formik
                                onSubmit={this.handleSubmit}
                            >
                                {({
                                    values,
                                }) => {
                                    return (
                                        <Form>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Coupon<span className="required">*</span></label>
                                                        <Field
                                                            name="coupon"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder=""
                                                            value={this.state.coupon ? this.state.coupon : values.coupon}
                                                            readOnly
                                                        />
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>{this.state.assignUser ? 'Assigned User' : 'Select User'}<span className="required">*</span></label>
                                                        {this.state.assignUser ?
                                                            <Field
                                                                name="coupon"
                                                                type="text"
                                                                className={'form-control'}
                                                                autoComplete="nope"
                                                                placeholder=""
                                                                value={this.state.assignUser.email}
                                                                readOnly
                                                            /> :
                                                            <Select
                                                                className=""
                                                                classNamePrefix="select"
                                                                isClearable={true}
                                                                isSearchable={true}
                                                                name="user_id"
                                                                options={options}
                                                                styles={colourStyles}
                                                                onChange={this.handleChange}
                                                                theme={
                                                                    theme => ({
                                                                        ...theme,
                                                                        borderRadius: 0,
                                                                        colors: {
                                                                            ...theme.colors,
                                                                            primary25: '#0186cf',
                                                                            primary: 'green',
                                                                        },
                                                                    })}
                                                            />}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    {(this.state.userInfo.first_name)?'Name: '+ this.state.userInfo.first_name+' '+this.state.userInfo.last_name : ''}<br></br>
                                                    {(this.state.userInfo.address)? 'Address: '+this.state.userInfo.address : ''}
                                                </Col>
                                            </Row>
                                            <Row className="show-grid text-center">
                                                <Col xs={12} md={12}>
                                                    <Fragment>
                                                        {this.state.assignUser ?
                                                            this.state.assignuserdetails.is_redeem === 'Yes' ? <Button
                                                                onClick={this.props.handelAssignModalClose}
                                                                className="but-gray border-0 mr-2">
                                                                Back
                                                                </Button> : <Fragment>
                                                                    {/* <Button className="blue-btn border-0" onClick={this.handleeditCouponEnable}>
                                                                        Edit
                                                                </Button> */}
                                                                    <Button
                                                                        onClick={this.props.handelAssignModalClose}
                                                                        className="but-gray border-0 mr-2">
                                                                        Back
                                                                </Button>
                                                                </Fragment> : <Fragment>
                                                                <Button
                                                                    onClick={this.props.handelAssignModalClose}
                                                                    className="but-gray border-0 mr-2">
                                                                    Cancel
                                                                </Button>
                                                                <Button type="submit" className="blue-btn ml-2 border-0">
                                                                    Save
                                                                </Button>
                                                            </Fragment>
                                                        }
                                                    </Fragment>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={12}>
                                                    <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                                </Col>
                                            </Row>
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Col>
                    </Row>
                </div>

                {/*======  confirmation popup  ===== */}
                <Modal
                    show={this.state.showConfirMmsg}
                    onHide={this.handleConfirmReviewClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Coupon has been successfully assigned</h5>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handleConfirmReviewClose}
                            className="but-gray" > Done
                        </Button>
                    </Modal.Footer>
                </Modal>

            </div>
        );
    }
}

AssignCoupon.propTypes = {
    handleAddConfirMmsg: PropTypes.func,
    businessId: PropTypes.number,
    token: PropTypes.any,
    handelAssignModalClose: PropTypes.func,
    handleAssignCouponConfirMmsg: PropTypes.func,
};

export default AssignCoupon;