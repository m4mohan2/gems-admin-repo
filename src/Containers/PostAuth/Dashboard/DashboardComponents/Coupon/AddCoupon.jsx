import React, { Component } from 'react';
import * as Yup from 'yup';
import axios from '../../../../../shared/eaxios';
import * as AppConst from './../../../../../common/constants';
import { Button, Col, FormControl, FormGroup, Image, Modal, Row } from 'react-bootstrap';
import { Field, Form, Formik } from 'formik';
import SuccessIco from '../../../../../assets/success-ico.png';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { connect } from 'react-redux';
import Moment from 'moment';

const initialValues = {
    coupon_type: '',
    amount: '',
    amount_type: '',
    start_date: '',
    end_date: '',
    description: '',
    active_status: ''
};

const number = /^[0-9]/;

const addCouponSchema = Yup.object().shape({
    coupon_type: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please select coupon type'),
    amount: Yup.string()
        .when('coupon_type', {
            is: '0',
            then: Yup.string()
                .strict()
                .matches(number, 'Please enter only digits')
                .required('Please enter amount'),
        }),
    amount_type: Yup.string()
        .when('coupon_type', {
            is: '0',
            then: Yup.string()
                .strict()
                .required('Please select amount type'),
        }),
    description: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter description'),
    active_status: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please select status'),
});

class AddCoupon extends Component {
    state = {
        showConfirMmsg: false,
        errorMessge: null,
        addCouponLoader: false,
        addErrorMessge: null,
        startDate: new Date(),
        endDate: '',
    };

    handleChange = (e, field) => {
        this.setState({
            [field]: e.target.value
        });
    };

    handleConfirmReviewClose = () => {
        this.setState({ showConfirMmsg: false });
    };

    handleConfirmReviewShow = () => {
        this.setState({ showConfirMmsg: true });
    };

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handleSubmit = (values, { resetForm, setSubmitting }) => {
        this.setState({
            addCouponLoader: true,
        });
        let newValue = {
            coupon_type: values.coupon_type,
            amount: values.amount,
            amount_type: values.amount_type,
            start_date: Moment(this.state.startDate).format('YYYY-MM-DD HH:mm:ss'),
            end_date: Moment(this.state.endDate).format('YYYY-MM-DD HH:mm:ss'),
            description: values.description,
            active_status: values.active_status,
            creator_id: this.props.token
        };
        axios
            .post(AppConst.APIURL + '/api/generateCoupon', newValue)
            .then(res => {
                console.log(res);
                resetForm({});
                setSubmitting(false);
                this.props.handelAddModalClose();
                this.props.handleAddConfirMmsg();
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    addCouponLoader: false,
                    addErrorMessge: errorMsg,
                });
                setTimeout(() => {
                    this.setState({ addErrorMessge: null });
                }, 5000);
            });
    };

    handleChangeStartDate = date => {
        console.log(date);
        this.setState({
            startDate: date,
            endDate: ''
        });
    }

    handleChangeEndDate = date => {
        this.setState({
            endDate: date,
        });
    }

    render() {
        return (
            <div className="addcustomerSec">
                <div className="boxBg p-35">
                    {this.state.addErrorMessge ? (
                        <div className="alert alert-danger" role="alert">
                            {this.state.addErrorMessge}
                        </div>
                    ) : null}
                    <Row>
                        <Col sm={12}>
                            {this.state.addCouponLoader ? <LoadingSpinner /> : null}
                        </Col>
                    </Row>
                    <Row className="show-grid">
                        <Col xs={12} className="brd-right">
                            <Formik
                                initialValues={initialValues}
                                validationSchema={addCouponSchema}
                                onSubmit={this.handleSubmit}
                            >
                                {({
                                    values,
                                    errors,
                                    touched,
                                    isSubmitting,
                                }) => {
                                    return (
                                        <Form>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Coupon Type<span className="required">*</span></label>
                                                        <Field
                                                            name="coupon_type"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >
                                                            <option value="" defaultValue="">Select Coupon Type</option>
                                                            <option value="0" key="0">Discount</option>
                                                            <option value="1" key="1">Complimentary Reward</option>
                                                        </Field>
                                                        {errors.coupon_type && touched.coupon_type ? (
                                                            <span className="errorMsg ml-3">{errors.coupon_type}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                {values.coupon_type === '0' || values.coupon_type === '' ?
                                                    <Col xs={12} md={12}>
                                                        <FormGroup controlId="formControlsTextarea">
                                                            <label>Amount ($)<span className="required">*</span></label>
                                                            <Field
                                                                name="amount"
                                                                type="text"
                                                                className={'form-control'}
                                                                autoComplete="nope"
                                                                placeholder="Enter Amount"
                                                                value={values.amount || ''}
                                                            />
                                                            {errors.amount && touched.amount ? (
                                                                <span className="errorMsg ml-3">{errors.amount}</span>
                                                            ) : null}
                                                            <FormControl.Feedback />
                                                        </FormGroup>
                                                    </Col> : ''}
                                                {values.coupon_type === '0' || values.coupon_type === '' ?
                                                    <Col xs={12} md={12}>
                                                        <FormGroup controlId="formControlsTextarea">
                                                            <label>Amount Type<span className="required">*</span></label>
                                                            <Field
                                                                name="amount_type"
                                                                component="select"
                                                                className={'form-control'}
                                                                autoComplete="nope"
                                                                placeholder="select"
                                                            >
                                                                <option value="" defaultValue="">Select Amount Type</option>
                                                                <option value="Flat" key="0">Flat</option>
                                                                <option value="Percent" key="1">Percent</option>
                                                            </Field>
                                                            {errors.amount_type && touched.amount_type ? (
                                                                <span className="errorMsg ml-3">{errors.amount_type}</span>
                                                            ) : null}
                                                            <FormControl.Feedback />
                                                        </FormGroup>
                                                    </Col> : ''}

                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Start Date<span className="required">*</span></label>
                                                        <DatePicker
                                                            selected={this.state.startDate}
                                                            onChange={this.handleChangeStartDate}
                                                            className='form-control'
                                                            minDate={new Date()}
                                                            required
                                                            timeInputLabel="Time:"
                                                            dateFormat="dd-MM-yyyy HH:mm:ss"
                                                            showTimeInput
                                                            showMonthDropdown
                                                            showYearDropdown
                                                        />
                                                        {errors.start_date && touched.start_date ? (
                                                            <span className="errorMsg ml-3">{errors.start_date}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>End Date<span className="required">*</span></label>
                                                        <DatePicker
                                                            selected={this.state.endDate}
                                                            onChange={this.handleChangeEndDate}
                                                            className='form-control'
                                                            minDate={this.state.startDate}
                                                            minTime={this.state.startDate}
                                                            required
                                                            timeInputLabel="Time:"
                                                            dateFormat="dd-MM-yyyy HH:mm:ss"
                                                            showTimeInput
                                                            showMonthDropdown
                                                            showYearDropdown
                                                        />
                                                        {errors.end_date && touched.end_date ? (
                                                            <span className="errorMsg ml-3">{errors.end_date}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Description<span className="required">*</span></label>
                                                        <Field
                                                            name="description"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter Description"
                                                            value={values.description || ''}
                                                        />
                                                        {errors.description && touched.description ? (
                                                            <span className="errorMsg ml-3">{errors.description}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <span>Status <span className="required">*</span></span>
                                                        <Field
                                                            name="active_status"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >
                                                            <option value="" defaultValue="">Select Status</option>
                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        {errors.active_status && touched.active_status ? (
                                                            <span className="errorMsg ml-3">{errors.active_status}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    &nbsp;
                                                </Col>
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12} className="text-center">
                                                    <Button className="blue-btn" type="submit" disabled={isSubmitting}>
                                                        Save
                                                    </Button>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={12}>
                                                    <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                                </Col>
                                            </Row>
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Col>
                    </Row>
                </div>

                {/*======  confirmation popup  ===== */}
                <Modal
                    show={this.state.showConfirMmsg}
                    onHide={this.handleConfirmReviewClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Coupon has been successfully added</h5>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handleConfirmReviewClose}
                            className="but-gray" > Done
                        </Button>
                    </Modal.Footer>
                </Modal>

            </div>
        );
    }
}

AddCoupon.propTypes = {
    handleAddConfirMmsg: PropTypes.func,
    businessId: PropTypes.number,
    token: PropTypes.any,
    handelAddModalClose: PropTypes.func,
};

const mapStateToProps = (state) => {
    return {
        token: state.auth.token,
    };
};

export default connect(mapStateToProps)(AddCoupon);
