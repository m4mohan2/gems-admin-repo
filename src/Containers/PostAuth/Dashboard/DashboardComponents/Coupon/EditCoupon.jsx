import React, { Component, Fragment } from 'react';
import * as Yup from 'yup';
import axios from '../../../../../shared/eaxios';
import * as AppConst from './../../../../../common/constants';
import { Button, Col, FormControl, FormGroup, Row } from 'react-bootstrap';
import { Field, Form, Formik } from 'formik';
import PropTypes from 'prop-types';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import Moment from 'moment';
import { connect } from 'react-redux';

const number = /^[0-9]/;

const editCouponSchema = Yup.object().shape({
    coupon_type: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please select coupon type'),
    amount: Yup.string()
        .when('coupon_type', {
            is: '0',
            then: Yup.string()
                .strict()
                .matches(number, 'Please enter only digits')
                .required('Please enter amount').nullable(),
        }),
    amount_type: Yup.string()
        .when('coupon_type', {
            is: '0',
            then: Yup.string()
                .strict()
                .required('Please select amount type').nullable(),
        }),
    description: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter description'),
    active_status: Yup.string()
        .required('Please select status')
});

class EditCoupon extends Component {
    state = {
        showCoupon: false,
        errorMessge: null,
        editCouponEnable: false,
        disabled: false,
        editCouponLoader: false,
        editErrorMessge: false,
        startDate: '',
        endDate: '',
        permission: []
    };

    static getDerivedStateFromProps(props, state) {
        if (!state.couponData) {
            return {
                ...props
            };
        }
    }

    handleeditCouponEnable = () => {
        this.setState({
            editCouponEnable: true,
            disabled: true
        });
    }

    handleEditCouponDisable = () => {
        this.setState({
            editCouponEnable: false,
            disabled: false
        });
        this.props.onReload(this.state.couponData);
    }

    handleCloseService = () => {
        this.props.onClick();
    };

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handleSubmit = (values, { setSubmitting }) => {
        this.setState({
            editCouponLoader: true,
        });
        let newValue = {
            coupon_type: values.coupon_type,
            amount: values.amount,
            amount_type: values.amount_type,
            start_date: Moment(this.state.startDate).format('YYYY-MM-DD HH:mm:ss'),
            end_date: Moment(this.state.endDate).format('YYYY-MM-DD HH:mm:ss'),
            description: values.description,
            active_status: values.active_status
        };
        const couponData = {};
        Object.assign(couponData, newValue);
        axios
            .put(AppConst.APIURL + `/api/updateCoupon/${values.id}`, newValue)
            .then(res => {
                console.log('Service details get data', res.data);
                setSubmitting(false);
                this.setState({
                    editCouponLoader: false,
                    editCouponEnable: false,
                    disabled: false,
                    couponData
                }, () => {
                    this.props.handleEditConfirMmsg();
                });
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    editCouponLoader: false,
                    editErrorMessge: errorMsg,
                });

                setTimeout(() => {
                    this.setState({ deleteErrorMessge: null });
                }, 1000);

            });
    };

    componentDidMount() {
        const initialValues = { ...this.props };
        this.setState({
            startDate: Moment(initialValues.start_date).toDate(),
            endDate: Moment(initialValues.end_date).toDate()
        });
        const facility = this.props.facility;
        const permission = [];
        facility.map(data => {
            data.name === 'coupon' && data.permission.length !== 0 && permission.push(...data.permission);
        });
        this.setState({ permission });
    }

    handleChangeStartDate = date => {
        console.log(date);
        this.setState({
            startDate: date,
            endDate: date
        });
    }

    handleChangeEndDate = date => {
        this.setState({
            endDate: date
        });
    }

    render() {
        const initialValues = { ...this.props };
        const values = {
            id: initialValues.id,
            coupon_type: initialValues.coupon_type,
            amount: initialValues.amount,
            amount_type: initialValues.amount_type,
            description: initialValues.description,
            active_status: initialValues.active_status
        };
        const {
            disabled
        } = this.state;

        return (

            <div className="addcustomerSec">
                <div className="boxBg p-35">
                    <Row>
                        <Col sm={12}>
                            {this.state.editCouponLoader ? <LoadingSpinner /> : null}
                        </Col>
                    </Row>
                    <Row className="show-grid">
                        <Col xs={12} md={12}>
                            <div className="sectionTitle">
                                <h2>{this.state.editCouponEnable !== true ? 'View' : 'Edit'} Coupon</h2>
                            </div>
                        </Col>
                        <Col xs={12} className="brd-right">
                            <Formik
                                initialValues={values}
                                validationSchema={editCouponSchema}
                                onSubmit={this.handleSubmit}
                                enableReinitialize={true} >
                                {({
                                    values,
                                    errors,
                                    touched,
                                    isSubmitting,
                                }) => {
                                    return (
                                        <Form className={disabled === false ? ('hideRequired') : null}>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Coupon Type<span className="required">*</span></label>
                                                        <Field
                                                            name="coupon_type"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >
                                                            <option value="" defaultValue="">Select Coupon Type</option>
                                                            <option value="0" key="0">Discount</option>
                                                            <option value="1" key="1">Complimentary Reward</option>
                                                        </Field>
                                                        {errors.coupon_type && touched.coupon_type ? (
                                                            <span className="errorMsg ml-3">{errors.coupon_type}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                {values.coupon_type === '0' || values.coupon_type === '' ?
                                                    <Col xs={12} md={12}>
                                                        <FormGroup controlId="formControlsTextarea">
                                                            <label>Amount ($)<span className="required">*</span></label>
                                                            <Field
                                                                name="amount"
                                                                type="text"
                                                                className={'form-control'}
                                                                autoComplete="nope"
                                                                placeholder="Enter Amount"
                                                                value={values.amount || ''}
                                                                disabled={disabled === false ? 'disabled' : ''}
                                                            />
                                                            {errors.amount && touched.amount ? (
                                                                <span className="errorMsg ml-3">{errors.amount}</span>
                                                            ) : null}
                                                            <FormControl.Feedback />
                                                        </FormGroup>
                                                    </Col> : ''}

                                                {values.coupon_type === '0' || values.coupon_type === '' ?
                                                    <Col xs={12} md={12}>
                                                        <FormGroup controlId="formControlsTextarea">
                                                            <label>Amount Type<span className="required">*</span></label>
                                                            <Field
                                                                name="amount_type"
                                                                component="select"
                                                                className={'form-control'}
                                                                autoComplete="nope"
                                                                placeholder="select"
                                                                disabled={disabled === false ? 'disabled' : ''}
                                                            >
                                                                <option value="" defaultValue="">Select Amount Type</option>
                                                                <option value="Flat" key="0">Flat</option>
                                                                <option value="Percent" key="1">Percent</option>
                                                            </Field>
                                                            {errors.amount_type && touched.amount_type ? (
                                                                <span className="errorMsg ml-3">{errors.amount_type}</span>
                                                            ) : null}
                                                            <FormControl.Feedback />
                                                        </FormGroup>
                                                    </Col> : ''}

                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Start Date<span className="required">*</span></label>
                                                        <DatePicker
                                                            selected={this.state.startDate}
                                                            onChange={this.handleChangeStartDate}
                                                            className='form-control'
                                                            minDate={new Date()}
                                                            required
                                                            timeInputLabel="Time:"
                                                            dateFormat="dd-MM-yyyy HH:mm:ss"
                                                            showTimeInput
                                                            showMonthDropdown
                                                            showYearDropdown
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.start_date && touched.start_date ? (
                                                            <span className="errorMsg ml-3">{errors.start_date}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>End Date<span className="required">*</span></label>
                                                        <DatePicker
                                                            selected={this.state.endDate}
                                                            onChange={this.handleChangeEndDate}
                                                            className='form-control'
                                                            minDate={this.state.startDate}
                                                            required
                                                            timeInputLabel="Time:"
                                                            dateFormat="dd-MM-yyyy HH:mm:ss"
                                                            showTimeInput
                                                            showMonthDropdown
                                                            showYearDropdown
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.end_date && touched.end_date ? (
                                                            <span className="errorMsg ml-3">{errors.end_date}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Description<span className="required">*</span></label>
                                                        <Field
                                                            name="description"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter Description"
                                                            value={values.description || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.description && touched.description ? (
                                                            <span className="errorMsg ml-3">{errors.description}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <span><h3>Status <span className="required">*</span></h3></span>
                                                        <Field
                                                            name="active_status"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >
                                                            <option value="">Select Status</option>
                                                            <option value="1" key="1">Active </option>
                                                            <option value="0" key="0">Inactive </option>
                                                        </Field>
                                                        {errors.active_status && touched.active_status ? (
                                                            <span className="errorMsg ml-3">{errors.active_status}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    &nbsp;
                                                </Col>
                                            </Row>
                                            <Row>&nbsp;</Row>
                                            {this.state.permission[2] && this.state.permission[2].status === true &&
                                                <Row className="show-grid text-center">
                                                    <Col xs={12} md={12}>
                                                        <Fragment>
                                                            {this.state.editCouponEnable !== true ? (
                                                                <Fragment>
                                                                    <Button className="blue-btn border-0" onClick={this.handleeditCouponEnable}>
                                                                        Edit
                                                                </Button>
                                                                </Fragment>) :
                                                                (<Fragment>
                                                                    <Button
                                                                        onClick={this.props.handelviewEditModalClose}
                                                                        className="but-gray border-0 mr-2">
                                                                        Cancel
                                                                </Button>
                                                                    <Button type="submit" className="blue-btn ml-2 border-0" disabled={isSubmitting}>
                                                                        Save
                                                                </Button>
                                                                </Fragment>)
                                                            }
                                                        </Fragment>
                                                    </Col>
                                                </Row>}
                                            {disabled === false ? null : (<Fragment>
                                                <Row>
                                                    <Col md={12}>
                                                        <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                                    </Col>
                                                </Row>
                                            </Fragment>)}
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

const mapStateToPros = state => {
    return {
        facility: state.auth.facility
    };
};

EditCoupon.propTypes = {
    globState: PropTypes.object,
    onClickAction: PropTypes.func,
    onReload: PropTypes.func,
    onClick: PropTypes.func,
    handelviewEditModalClose: PropTypes.func,
    handleEditConfirMmsg: PropTypes.func,
    facility: PropTypes.any
};

export default connect(mapStateToPros, null)(EditCoupon);