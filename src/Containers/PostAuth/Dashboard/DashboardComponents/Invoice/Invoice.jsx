import React, { Component } from 'react';
import axios from '../../../../../shared/eaxios';
import {
    Table,
    Image,
    Row,
    Col,

} from 'react-bootstrap';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import NumberFormat from 'react-number-format';
import { Link } from 'react-router-dom';
import refreshIcon from './../../../../../assets/refreshIcon.png';
import Pagination from 'react-js-pagination';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { invoiceDescription } from './../../../../../redux/actions/invoice';



class Invoice extends Component {

    state = {
        invoiceLists: [],
        activePage: 1,
        totalCount: 0,
        itemPerPage: 250,
        invoiceListsLoading: false,
        errorMessge:null,
        paymentMethod :'',
        searchKey:''

    }    
    _isMounted = false;

    displayError = (e) => {
        console.log('displayError', e.data.message);
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber });
        this.handleGetInvoice(pageNumber > 0 ? pageNumber - 1 : 0);
    };

    handleChangeItemPerPage = (e) => {
        this.setState({ itemPerPage: e.target.value },
            () => {
                this.handleGetInvoice(this.state.activePage > 0 ? this.state.activePage - 1 : 0);
            });
    }

    resetPagination = () => {
        this.setState({ activePage: 1 });
    }

    

    businessAdd = () => {
        this.setState({
            showBusinessModal: true,
        }, () => { });
    }

    handleGetInvoice = (since = 0) => {
        this.setState({
            invoiceListsLoading:true,
        }),
        axios
            .get(
                `invoice/list?key=${this.state.searchKey}&paymentMethod=${this.state.paymentMethod}&direction=true&prop&since=${since}&limit=20`
            )
            .then(res => {
                console.log(res);
                const totalCount = res.data.total;
                if (this._isMounted){
                    this.setState({
                        invoiceLists: res.data.entries,
                        invoiceListsLoading: false,
                        totalCount
                    },()=>{
                        console.log('this.state.invoiceLists', this.state.invoiceLists);
                    });
                }


            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    errorMessge: errorMsg,
                    invoiceListsLoading: false
                });

            });
    }

    

    handleChange = (e) => {
        this.setState({ paymentMethod: e.target.value },
            () => {
                this.handleGetInvoice(0);
                //this.resetPagination();
            });
    }

    keyPress = (e) => {
        /*
        if (e.keyCode == 13) {
            this.setState(
                {
                    searchKey: e.target.value
                },
                ()=>{
                    this.handleGetInvoice();
                }
            );
            
                
        }
        */


        this.setState(
            {
                searchKey: e.target.value.trim()
            },
            () => {
            }
        );

    }

    handelSearch = () => {
        this.handleGetInvoice(0);
    }
    resetSearch = () => {
        this.props.onClickAction('');
        this.setState({
            searchKey: '',
            paymentMethod: ''
        },()=>{
            this.handleGetInvoice(0);
            this.resetPagination();
        });
        
    }

    componentDidMount(){
        console.log('did mount invoice---------------', this.state.globalState);
        this._isMounted = true;
        
        if (this.props.globalState.invoice.description !==''){
            this.setState({
                searchKey: this.props.globalState.invoice.description
            },()=>{
                this.handelSearch();
            });
        } else{
            this.handleGetInvoice(0);
        }
    }
    componentDidUpdate(){
        console.log('did update invoice');

    }

    componentWillUnmount() {
        this._isMounted = false;

    }

    


    render() {
        return (
            <div className="dashboardInner businessOuter">
                <div className="row my-3">
                    <div className="col-8">
                        <select
                            id={this.state.selectedBusiness}
                            className="form-control truncate pr-35 float-left w-210 mr-3"
                            onChange={this.handleChange}
                            value={this.state.paymentMethod}>
                            <option value=''>Select Payment Mode</option>
                            <option value='CHECK'> Check </option>
                            <option value='ACH'>ACH</option>
                            <option value='VC'>Virtual Card</option>

                        </select>

                        <input 
                            type="text" 
                            className="w-210 form-control float-left" 
                            placeholder="Type to search" 
                            onChange={this.keyPress} 
                            value={this.state.searchKey} 
                        />
                        <button
                            type="button"
                            className="btn ml-3 search-btn text-black float-left cus-mt-3"
                            onClick={() => this.handelSearch()}
                        >
                            Search
                        </button>

                        <Link to="#" className='ml-2'>
                            <Image className="mt-2" src={refreshIcon} onClick={() => this.resetSearch()} />
                        </Link>

                    </div>
                </div>
                <div className="boxBg">
                    <Table responsive hover>
                        <thead className="theaderBg">
                            <tr>
                                {/* <th></th> */}
                                <th>
                                    <span className="float-left pr-2">Status</span>
                                </th>
                                <th>
                                    <span className="float-left pr-2">Name</span>
                                </th>
                                <th>
                                    <span className="float-left pr-2">Invoice No.</span>
                                    
                                </th>
                                <th>

                                    <span className="float-left pr-2">payment Method</span>
                                    
                                </th>
                                <th>
                                    <span className="float-left pr-2">Date Created</span>
                                    
                                </th>
                                
                                <th>
                                    <span className="float-left pr-2">Due Date</span>
                                </th>
                                <th className="text-right">
                                    <span>Amount</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>

                            {this.state.invoiceListsLoading ? (<tr>
                                <td colSpan={12}>
                                    <LoadingSpinner />
                                </td>
                            </tr>) :
                                this.state.invoiceLists.length > 0 ? (
                                    this.state.invoiceLists.map(invoiceList => (
                                        <tr
                                            key={invoiceList.id}
                                        >
                                            <td>{invoiceList.invoiceState}</td>
                                            <td>{invoiceList.customerVendor.companyName}</td>
                                            <td>{invoiceList.invoiceNumber}</td>
                                            <td>{invoiceList.paymentMethod}</td>
                                            <td>{invoiceList.invoiceDate}</td>
                                            <td>{invoiceList.invoiceDueDate}</td>
                                            <td className="text-right">
                                                <NumberFormat
                                                    value={invoiceList.totalAmount}
                                                    displayType={'text'}
                                                    thousandSeparator={true}
                                                    fractionSize={2}
                                                    prefix={'$'}
                                                    decimalScale={2}
                                                    fixedDecimalScale={true}
                                                    thousandsGroupStyle={'thousand'}
                                                    renderText={value => <span>{value}</span>}
                                                />
                                            </td>
                                        </tr>
                                    ))
                                )
                                    :
                                    this.state.errorMessge ? <tr>
                                        <td colSpan={6}>
                                            <p className="text-center">{this.state.errorMessge}</p>
                                        </td>
                                    </tr> : (
                                        <tr>
                                            <td colSpan={6}>
                                                <p className="text-center">No records found</p>
                                            </td>
                                        </tr>
                                    )
                            }

                        </tbody>
                    </Table>
                </div>
                {this.state.totalCount ? (
                    <Row className="px-3">
                        <Col md={4} className="d-flex flex-row mt-20">
                            <span className="mr-2 mt-2 font-weight-500">Items per page</span>
                            <select
                                id={this.state.itemPerPage}
                                className="form-control truncatefloat-left w-90"
                                onChange={this.handleChangeItemPerPage}
                                value={this.state.itemPerPage}>
                                <option value='50'>50</option>
                                <option value='100'>100</option>
                                <option value='150'>150</option>
                                <option value='200'>200</option>
                                <option value='250'>250</option>

                            </select>
                        </Col>
                        <Col md={8}>
                            <div className="paginationOuter text-right">
                                <Pagination
                                    activePage={this.state.activePage}
                                    itemsCountPerPage={this.state.itemPerPage}
                                    totalItemsCount={this.state.totalCount}
                                    onChange={this.handlePageChange}
                                />
                            </div>
                        </Col>
                    </Row>
                ) : null}

            </div>
        );
    }
}

Invoice.propTypes = {

    onClickAction: PropTypes.func,
    globalState: PropTypes.object,

};

const mapStateToProps = state => {
    return {
        globalState: state
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onClickAction: data => dispatch(invoiceDescription(data)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Invoice);
