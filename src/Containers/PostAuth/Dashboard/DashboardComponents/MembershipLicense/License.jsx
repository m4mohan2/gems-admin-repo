import React, { Component, Fragment } from 'react';
import axios from '../../../../../shared/eaxios';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import { Table, Row, Col, Modal, Image, Button } from 'react-bootstrap';
import { FaCaretDown } from 'react-icons/fa';
import { FaCaretUp } from 'react-icons/fa';
import SuccessIco from '../../../../../assets/success-ico.png';
import Pagination from 'react-js-pagination';
import EditLicense from './EditLicense';
import AddLicense from './AddLicense';
import * as AppConst from './../../../../../common/constants';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

class License extends Component {
	state = {
		activePage: 1,
		totalCount: 0,
		itemPerPage: 10,
		sort: 1,
		field: null,
		fetchErrorMsg: null,
		licenseList: [],
		loading: true,
		sortingActiveID: 1,
		statusChange: false,
		statuserrorMsg: null,
		licenseId: null,
		status: null,
		servicesLists: [],
		permission: []
	}

	_isMounted = false;

	displayError = (e) => {
		let errorMessge = '';
		try {
			errorMessge = e.data.message ? e.data.message : e.data.error_description;
		} catch (e) {
			errorMessge = 'Access is denied!';
		}
		return errorMessge;
	}

	handelAddModal = () => {
		this.setState({
			addModal: true,
		});
	}

	handelAddModalClose = () => {
		this.setState({
			addModal: false,
		});
	}

	sortingActive = (id) => {
		this.setState({
			sortingActiveID: id
		});
	}

	fetchMemberLicense = (sort = this.state.sort, field = this.state.field) => {
		this.setState({
			loading: true,
			sort: sort,
			field: field
		}, () => {
			axios.get(AppConst.APIURL +
				`/api/listPlan?pageSize=${this.state.itemPerPage}&page=${this.state.sort}&searchKey=${this.state.serviceSearchKey}`)
				.then(res => {
					const licenseList = res.data.data.data;
					const totalCount = res.data.data.total;
					if (this._isMounted) {
						this.setState({
							licenseList,
							totalCount: totalCount,
							loading: false
						});
					}
				})
				.catch(e => {
					let errorMsg = this.displayError(e);
					this.setState({
						fetchErrorMsg: errorMsg,
						loading: false
					});
					setTimeout(() => {
						this.setState({ fetchErrorMsg: null });
					}, 5000);
				});
		});
	}

	handlePageChange = pageNumber => {
		this.setState({ activePage: pageNumber });
		this.fetchMemberLicense(pageNumber > 0 ? pageNumber : 0, this.state.field);
	};

	handleChangeItemPerPage = (e) => {
		this.setState({ itemPerPage: e.target.value },
			() => {
				this.fetchMemberLicense();
			});
	}

	resetPagination = () => {
		this.setState({ activePage: 1 });
	}

	rePagination = () => {
		this.setState({ activePage: 0 });
	}

	handleAddConfirMmsg = () => {
		this.setState({
			addConfirMmsg: true,
		}, () => {
			this.fetchMemberLicense();
		});
	}

	handleAddConfirMmsgClose = () => {
		this.setState({
			addConfirMmsg: false,
		});
	}

	handleEditConfirMmsg = () => {
		this.setState({
			viewEditModal: false,
			editConfirMmsg: true
		}, () => {
			this.fetchMemberLicense();
		});
	}

	handleEditConfirMmsgClose = () => {
		this.setState({
			editConfirMmsg: false
		});
	}

	handelviewEditModal = (data) => {
		this.setState({
			viewEditModal: true,
			licenseData: data
		});
	}

	handelviewEditModalClose = () => {
		this.setState({
			viewEditModal: false,
		});
	}

	componentDidMount() {
		this._isMounted = true;

		axios.get(AppConst.APIURL + '/api/listService')
			.then(res => {
				const servicesLists = res.data.data.data;
				this.setState({ servicesLists });
			});

		const facility = this.props.facility;
		const permission = [];
		facility.map(data => {
			data.name === 'membershipLicense' && data.permission.length !== 0 && permission.push(...data.permission);
		});
		this.setState({ permission }, () => {
			permission[0].status === true &&
				this.fetchMemberLicense();
		});
	}

	componentWillMount() {
		this._isMounted = false;
	}

	handelDeleteModal = (id) => {
		this.setState({
			deleteService: true,
			licenseId: id
		});
	}

	handleHide = () => {
		this.setState({
			statusChange: false,
			statuserrorMsg: null
		});
	}

	handleDeleteHide = () => {
		this.setState({
			deleteService: false,
			deleteerrorMsg: null
		});
	}

	handleDelete(id) {
		axios.delete(AppConst.APIURL + `/api/deletePlan/${id}`)
			.then(res => {
				console.log(res);
				this.setState({
					licenseId: null,
					successMessage: 'Record deleted successfully',
					deleteConfirMmsg: true
				});
				this.handleDeleteHide();
				this.fetchMemberLicense();
			})
			.catch(e => {
				let errorMsg = this.displayError(e);
				this.setState({
					deleteerrorMsg: errorMsg,
					loading: false
				});
				setTimeout(() => {
					this.setState({ errorMessge: null });
				}, 5000);
			});
	}

	handleDeleteChangedClose = () => {
		this.setState({
			deleteConfirMmsg: false,
			successMessage: null
		});
	}

	handelStatusModal = (id, status) => {
		this.setState({
			statusChange: true,
			licenseId: id,
			status: status
		});

	}

	handleStatus(id, status) {
		let updateArray = {
			'status': status
		};
		axios.put(AppConst.APIURL + `/api/statusUpdatePlan/${id}`, updateArray)
			.then(res => {
				console.log('--------------res------', res);
				this.setState({
					licenseId: null,
					status: null,
					successMessage: 'Status successfully changed',
					statusConfirMmsg: true
				});
				this.handleHide();
				this.fetchMemberLicense();
			})
			.catch(e => {
				let errorMsg = this.displayError(e);
				this.setState({
					statuserrorMsg: errorMsg,
					loading: false
				});
				setTimeout(() => {
					this.setState({ errorMessge: null });
				}, 5000);
			});
	}

	handleStatusChangedClose = () => {
		this.setState({
			statusConfirMmsg: false,
			successMessage: null
		});
	}

	render() {
		return (
			<div className="dashboardInner businessOuter pt-3">
				{this.state.permission[0] && this.state.permission[0].status === true ?
					<Fragment>
						<Row className="pt-3">
							<Col sm={12} md={12} className="text-right">
								{/*<button className="btn btn-primary float-right" onClick={() => this.handelAddModal()}>Add Membership License</button>*/}
							</Col>
						</Row>
						<Row className="show-grid pt-3">
							<Col sm={12} md={12}>
								<div className="boxBg">
									<Table responsive hover>
										<thead className="theaderBg">
											<tr>
												<th>
													<span className="float-left pr-2">License Name </span>
													<span className="d-flex flex-column-reverse sortingFontSize">
														<button className="custom-btn-focus sortingArrow">
															<FaCaretDown
																className={'cursorPointer ' + (this.state.sortingActiveID == 8 ? 'activeColor' : '')}
																onClick={() => {
																	this.fetchMemberLicense(0, '', false, 'license_name');
																	this.sortingActive(8);
																	this.setState({ activePage: 1 });
																}} />
														</button>
														<button className="custom-btn-focus sortingArrow">
															<FaCaretUp
																className={'cursorPointer ' + (this.state.sortingActiveID == 7 ? 'activeColor' : '')}
																onClick={() => {
																	this.fetchMemberLicense(0, '', true, 'license_name');
																	this.sortingActive(7);
																	this.setState({ activePage: 1 });
																}} />
														</button>
													</span>
												</th>
												<th>
													<span className="float-left pr-2"> License Interval </span>
													<span className="d-flex flex-column-reverse sortingFontSize">
														<button className="custom-btn-focus sortingArrow">
															<FaCaretDown
																className={'cursorPointer ' + (this.state.sortingActiveID == 10 ? 'activeColor' : '')}
																onClick={() => {
																	this.fetchMemberLicense(0, '', false, 'license_validity');
																	this.sortingActive(10);
																	this.setState({ activePage: 1 });
																}} />
														</button>
														<button className="custom-btn-focus sortingArrow">
															<FaCaretUp
																className={'cursorPointer ' + (this.state.sortingActiveID == 9 ? 'activeColor' : '')}
																onClick={() => {
																	this.fetchMemberLicense(0, '', true, 'license_validity');
																	this.sortingActive(9);
																	this.setState({ activePage: 1 });
																}} />
														</button>
													</span>
												</th>
												<th>
													<span className="float-left pr-2">Price </span>
													<span className="d-flex flex-column-reverse sortingFontSize">
														<button className="custom-btn-focus sortingArrow">
															<FaCaretDown
																className={'cursorPointer ' + (this.state.sortingActiveID == 12 ? 'activeColor' : '')}
																onClick={() => {
																	this.fetchMemberLicense(0, '', false, 'price');
																	this.sortingActive(12);
																	this.setState({ activePage: 1 });
																}} />
														</button>
														<button className="custom-btn-focus sortingArrow">
															<FaCaretUp
																className={'cursorPointer ' + (this.state.sortingActiveID == 11 ? 'activeColor' : '')}
																onClick={() => {
																	this.fetchMemberLicense(0, '', true, 'price');
																	this.sortingActive(11);
																	this.setState({ activePage: 1 });
																}} />
														</button>
													</span>
												</th>
												{/*<th>
											<span className="float-left pr-2">Free Period Validity </span>
											<span className="d-flex flex-column-reverse sortingFontSize">
												<button className="custom-btn-focus sortingArrow">
													<FaCaretDown
														className={'cursorPointer ' + (this.state.sortingActiveID == 12 ? 'activeColor' : '')}
														onClick={() => {
															this.fetchMemberLicense(0, '', false, 'free_period_validity');
															this.sortingActive(14);
															this.setState({ activePage: 1 });
														}} />
												</button>
												<button className="custom-btn-focus sortingArrow">
													<FaCaretUp
														className={'cursorPointer ' + (this.state.sortingActiveID == 11 ? 'activeColor' : '')}
														onClick={() => {
															this.fetchMemberLicense(0, '', true, 'free_period_validity');
															this.sortingActive(13);
															this.setState({ activePage: 1 });
														}} />
												</button>
											</span>
										</th>*/}
												<th>Paid Services</th>
												{/*<th>Free Services</th>*/}
												<th>Staus</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											{this.state.loading ? (<tr><td colSpan={12}><LoadingSpinner /></td></tr>) :
												this.state.licenseList.length > 0 ?
													(this.state.licenseList.map(license => (
														<tr key={license.id}>
															<td>{license.plan_name}</td>
															<td className='text-capitalize'>{license.plan_peroid}</td>
															<td>{license.plan_price} {license.plan_currency.toUpperCase()}</td>
															{/*<td>{license.plan_free_peroid}</td>*/}
															<td>{
																license.plan_paid_services.map((data) => {
																	return data.service && <p key={data.id} style={{ marginBottom: '0', paddingBottom: '0' }}>{this.state.servicesLists.map(service => { return service.status === 1 && service.id === data.id ? data.value : ''; })}</p>;
																})}</td>
															{/*<td>{
														license.plan_free_services.map(data => {
															return data.service && <p key={data.id} style={{ marginBottom: '0', paddingBottom: '0' }}>{this.state.servicesLists.map(service => { return service.status === 1 && service.id === data.id ? data.value : ''; })}</p>;
														})}</td>*/}
															<td>
																{license.status === 1
																	? <i className="fa fa-circle green" aria-hidden="true" title="Active"
																		onClick={() => this.state.permission[1] && this.state.permission[1].status === true && this.handelStatusModal(license.id, '0')}></i>
																	: <i className="fa fa-circle red" aria-hidden="true" title="Inactive"
																		onClick={() => this.state.permission[1] && this.state.permission[1].status === true && this.handelStatusModal(license.id, '1')}></i>
																}
															</td>
															<td>
																<i className="fa fa-eye" aria-hidden="true" title="View" onClick={() => this.handelviewEditModal(license)}></i>&nbsp;
																{this.state.permission[2] && this.state.permission[2].status === true ?
																	<i className="fa fa-trash-o red" aria-hidden="true" title="Delete" onClick={() => this.handelDeleteModal(license.id)}></i>
																	: ''}
															</td>
														</tr>
													)))
													: this.state.fetchErrorMsg ? null : (<tr>
														<td colSpan={12}>
															<p className="text-center">No records found</p>
														</td>
													</tr>)
											}
										</tbody>
									</Table>
								</div>
							</Col>
						</Row>
						{this.state.totalCount ? (
							<Row>
								<Col md={4} className="d-flex flex-row mt-20">
									<span className="mr-2 mt-2 font-weight-500">Items per page</span>
									<select
										id={this.state.itemPerPage}
										className="form-control truncatefloat-left w-90"
										onChange={this.handleChangeItemPerPage}
										value={this.state.itemPerPage}>
										<option value='10'>10</option>
										<option value='50'>50</option>
										<option value='100'>100</option>
										<option value='150'>150</option>
										<option value='200'>200</option>
										<option value='250'>250</option>
									</select>
								</Col>
								<Col md={8}>
									<div className="paginationOuter text-right">
										<Pagination
											activePage={this.state.activePage}
											itemsCountPerPage={this.state.itemPerPage}
											totalItemsCount={this.state.totalCount}
											onChange={this.handlePageChange}
										/>
									</div>
								</Col>
							</Row>
						) : null}
					</Fragment>
					: <h5 className="text-center p-3">You do not have any permission to view this content.</h5>}

				{/*========================= Modal for Status change =====================*/}
				<Modal
					show={this.state.statusChange}
					onHide={this.handleHide}
					dialogClassName="modal-90w"
					aria-labelledby="example-custom-modal-styling-title"
				>
					<Modal.Body>
						<div className="m-auto text-center">
							<h6 className="mb-3 text-dark">Do you want to change this status?</h6>
						</div>
						{this.state.statuserrorMsg ? <div className="alert alert-danger my-3 text-center col-12" role="alert">{this.state.statuserrorMsg}</div> : null}
						<div className="m-auto text-center">
							<button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.handleHide()}>Return</button>
							{this.state.statuserrorMsg == null ? <button className="btn btn-danger" onClick={() => this.handleStatus(this.state.licenseId, this.state.status)}>Confirm</button> : null}
						</div>

					</Modal.Body>
				</Modal>

				{/*====== Status change confirmation popup  ===== */}
				<Modal
					show={this.state.statusConfirMmsg}
					onHide={this.handleStatusChangedClose}
					className="payOptionPop"
				>
					<Modal.Body className="text-center">
						<Row>
							<Col md={12} className="text-center">
								<Image src={SuccessIco} />
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<h5>{this.state.successMessage}</h5>
							</Col>
						</Row>
						<Button
							onClick={this.handleStatusChangedClose}
							className="but-gray mt-3"
						>
							Return
                        </Button>
					</Modal.Body>
				</Modal>

				{/* Add License Modal */}
				<Modal show={this.state.addModal}
					onHide={this.handelAddModalClose}
					className="right full noPadding slideModal" >
					<Modal.Header closeButton></Modal.Header>
					<Modal.Body className="">
						<div className="modalHeader">
							<Row>
								<Col md={9}>
									<h1>Add Member License</h1>
								</Col>
							</Row>
						</div>
						<div className="modalBody content-body noTabs">
							<AddLicense
								handleAddConfirMmsg={this.handleAddConfirMmsg}
								handelAddModalClose={this.handelAddModalClose}
							/>
						</div>
					</Modal.Body>
				</Modal>

				{/*======  Add confirmation popup  ===== */}
				<Modal
					show={this.state.addConfirMmsg}
					onHide={this.handleAddConfirMmsgClose}
					className="payOptionPop">
					<Modal.Body>
						<Row>
							<Col md={12} className="text-center">
								<Image src={SuccessIco} />
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<h5>Record has been successfully Added</h5>
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<Button
									onClick={this.handleAddConfirMmsgClose}
									className="but-gray">
									Return
                                </Button>
							</Col>
						</Row>
					</Modal.Body>
				</Modal>

				{/* Edit License Modal */}
				<Modal
					show={this.state.viewEditModal}
					onHide={this.handelviewEditModalClose}
					className="right full noPadding slideModal"
				>
					<Modal.Header closeButton></Modal.Header>
					<Modal.Body className="">
						<div className="modalBody content-body noTabs">
							<EditLicense
								{...this.state.licenseData}
								handelviewEditModalClose={this.handelviewEditModalClose}
								handleEditConfirMmsg={this.handleEditConfirMmsg} />
						</div>
					</Modal.Body>
				</Modal>

				{/*====== Edit confirmation popup  ===== */}
				<Modal
					show={this.state.editConfirMmsg}
					onHide={this.handleEditConfirMmsgClose}
					className="payOptionPop"
				>
					<Modal.Body>
						<Row>
							<Col md={12} className="text-center">
								<Image src={SuccessIco} />
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<h5>Record has been successfully edited</h5>
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<Button
									onClick={this.handleEditConfirMmsgClose}
									className="but-gray"
								>
									Return
                                </Button>
							</Col>
						</Row>
					</Modal.Body>
				</Modal>

				{/*========================= Modal for Delete License =====================*/}
				<Modal
					show={this.state.deleteService}
					onHide={this.handleHide}
					dialogClassName="modal-90w"
					aria-labelledby="example-custom-modal-styling-title" >
					<Modal.Body>
						<div className="m-auto text-center">
							<h6 className="mb-3 text-dark">Do you want to delete this License?</h6>
						</div>
						{this.state.deleteerrorMsg ? <div className="alert alert-danger my-3 text-center col-12" role="alert">{this.state.deleteerrorMsg}</div> : null}
						<div className="m-auto text-center">
							<button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.handleDeleteHide()}>Return</button>
							{this.state.deleteerrorMsg == null ? <button className="btn btn-danger" onClick={() => this.handleDelete(this.state.licenseId)}>Confirm</button> : null}
						</div>
					</Modal.Body>
				</Modal>

				{/*====== Delete confirmation popup  ===== */}
				<Modal
					show={this.state.deleteConfirMmsg}
					onHide={this.handleDeleteChangedClose}
					className="payOptionPop" >
					<Modal.Body className="text-center">
						<Row>
							<Col md={12} className="text-center">
								<Image src={SuccessIco} />
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<h5>{this.state.successMessage}</h5>
							</Col>
						</Row>
						<Button
							onClick={this.handleDeleteChangedClose}
							className="but-gray mt-3"
						>
							Return
                        </Button>
					</Modal.Body>
				</Modal>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		facility: state.auth.facility
	};
};

License.propTypes = {
	facility: PropTypes.any
};

export default connect(mapStateToProps, null)(License);