import React, { Component, Fragment } from 'react';
import * as Yup from 'yup';
import axios from '../../../../../shared/eaxios';
import * as AppConst from './../../../../../common/constants';
import { Button, Col, FormControl, FormGroup, Row } from 'react-bootstrap';
import { Field, Form, Formik } from 'formik';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { FormCheckbox } from 'shards-react';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';

const editLicenseSchema = Yup.object().shape({
    plan_name: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter name')
        .max(40, 'maximum characters length 40'),
    plan_peroid: Yup.string()
        .required('Please select license interval')
        .strict(),
    plan_price: Yup.number()
        .required('Please enter license price')
        .strict(),
    plan_free_peroid: Yup.string()
        .strict(),
    plan_interval_count: Yup.string()
        .required('Please select license interval count'),
    plan_currency: Yup.string()
        .required('Please select license currency'),
    status: Yup.string()
        .required('Please select status')
});

class EditLicense extends Component {
    state = {
        showLicense: false,
        errorMessge: null,
        editLicenseEnable: false,
        disabled: false,
        editLicenseLoader: false,
        editErrorMessge: false,
        plan_paid_services: [],
        plan_free_services: [],
        paidServicePrice: 0,
        freeServicePrice: 0,
        plan_paid_services_list_api: [],
        plan_free_services_list_api: [],
        editServiceLoader: false,
        currencyCodes: [],
        permission: []
    };

    static getDerivedStateFromProps(props, state) {
        if (!state.licenseData) {
            return {
                ...props
            };
        }
    }


    handleeditLicenseEnable = () => {
        this.setState({
            editLicenseEnable: true,
            disabled: true
        });
    }

    handleEditLicenseDisable = () => {
        this.setState({
            editLicenseEnable: false,
            disabled: false
        });
        this.props.onReload(this.state.licenseData);
    }

    handleCloseLicense = () => {
        this.props.onClick();
    };

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handleSubmit = values => {
        this.setState({
            editLicenseLoader: true,
        });
        let newValue = {
            plan_name: values.plan_name,
            plan_peroid: values.plan_peroid,
            plan_price: values.plan_price,
            plan_free_peroid: values.plan_free_peroid,
            plan_paid_services: this.state.plan_paid_services_list_api,
            plan_free_services: this.state.plan_free_services_list_api,
            status: values.status,
            plan_interval_count: values.plan_interval_count,
            plan_currency: values.plan_currency,
        };
        console.log(newValue);
        const licenseData = {};
        Object.assign(licenseData, newValue);
        axios
            .put(AppConst.APIURL + `/api/editPlan/${values.id}`, newValue)
            .then(res => {
                console.log('License details get data', res.data);
                this.setState({
                    editLicenseLoader: false,
                    editLicenseEnable: false,
                    disabled: false,
                    licenseData
                }, () => {
                    this.props.handleEditConfirMmsg();
                });
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    editLicenseLoader: false,
                    editErrorMessge: errorMsg,
                });

                setTimeout(() => {
                    this.setState({ deleteErrorMessge: null });
                }, 1000);

            });
    };

    handleCheckboxChangePaidService(e, service, price) {
        if (this.state.disabled === true) {
            let stateCopy = Object.assign({}, this.state.plan_paid_services_list_api);
            stateCopy[service].service = !stateCopy[service].service;
            let paidServicePrice = 0;
            if (stateCopy[service].service === true)
                paidServicePrice = this.state.paidServicePrice + price;
            else if (stateCopy[service].service === false)
                paidServicePrice = this.state.paidServicePrice - price;
            this.setState({ stateCopy, paidServicePrice });
        }
    }

    handleCheckboxChangeFreeService(e, service, price) {
        if (this.state.disabled === true) {
            let stateCopy = Object.assign({}, this.state.plan_free_services_list_api);
            stateCopy[service].service = !stateCopy[service].service;
            let freeServicePrice = 0;
            if (stateCopy[service].service === true)
                freeServicePrice = this.state.freeServicePrice + price;
            else if (stateCopy[service].service === false)
                freeServicePrice = this.state.freeServicePrice - price;
            this.setState({ stateCopy, freeServicePrice });
        }
    }

    componentDidMount = () => {
        const facility = this.props.facility;
        const permission = [];
        facility.map(data => {
            data.name === 'membershipLicense' && data.permission.length !== 0 && permission.push(...data.permission);
        });
        this.setState({ permission });

        let cc = require('currency-codes');
        this.setState({ editServiceLoader: true, currencyCodes: cc.data });
        const initialValues = { ...this.props };
        const plan_paid_services = initialValues.plan_paid_services;
        const plan_free_services = initialValues.plan_free_services;

        axios.get(AppConst.APIURL + '/api/listService')
            .then(res => {
                const servicesLists = res.data.data.data;
                const plan_paid_services_list_api = [];
                const plan_free_services_list_api = [];
                let paidServicePrice = 0;
                let freeServicePrice = 0;
                servicesLists.map(data => {
                    if (data.status === 1) {
                        plan_paid_services_list_api.push({
                            'service': plan_paid_services.find(element => { return element.id === data.id && element.service; }) ? true : false, 'id': data.id, 'value': data.service_name, 'price': data.service_price
                        });

                        plan_paid_services.find(element => {
                            return element.id === data.id ? element.service === true ? paidServicePrice = paidServicePrice + element.price : '' : false;
                        });

                        plan_free_services_list_api.push({
                            'service': plan_free_services.find(element => { return element.id === data.id && element.service; }) ? true : false, 'id': data.id, 'value': data.service_name, 'price': data.service_price
                        });

                        plan_free_services.find(element => {
                            return element.id === data.id ? element.service === true ? freeServicePrice = freeServicePrice + element.price : '' : false;
                        });
                    }
                });
                this.setState({
                    plan_free_services: plan_free_services,
                    plan_paid_services: plan_paid_services,
                    paidServicePrice,
                    freeServicePrice,
                    plan_paid_services_list_api,
                    plan_free_services_list_api,
                    editServiceLoader: false
                });
            });
    }

    render() {
        const initialValues = { ...this.props };
        const values = {
            id: initialValues.id,
            plan_name: initialValues.plan_name,
            plan_peroid: initialValues.plan_peroid,
            plan_price: initialValues.plan_price,
            plan_free_peroid: initialValues.plan_free_peroid,
            status: initialValues.status,
            plan_interval_count: initialValues.plan_interval_count,
            plan_currency: initialValues.plan_currency
        };
        const {
            disabled
        } = this.state;

        return (
            <div className="addcustomerSec">
                <div className="boxBg p-35">
                    <Row>
                        <Col sm={12}>
                            {this.state.editServiceLoader ? <LoadingSpinner /> : null}
                        </Col>
                    </Row>
                    <Row className="show-grid">
                        <Col xs={12} md={12}>
                            <div className="sectionTitle mb-4">
                                <h2>{this.state.editLicenseEnable !== true ? 'View' : 'Edit'} Membership License</h2>
                            </div>
                        </Col>
                        <Col xs={12} className="brd-right">
                            <Formik
                                initialValues={values}
                                validationSchema={editLicenseSchema}
                                onSubmit={this.handleSubmit}
                                enableReinitialize={true} >
                                {({
                                    values,
                                    errors,
                                    touched,
                                }) => {
                                    return (
                                        <Form className={disabled === false ? ('hideRequired') : null}>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>License Name <span className="required">*</span></h3></label>
                                                        <Field
                                                            name="plan_name"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter license name"
                                                            value={values.plan_name || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.plan_name && touched.plan_name ? (
                                                            <span className="errorMsg ml-3">{errors.plan_name}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>License Interval<span className="required">*</span></h3></label>
                                                        <Field
                                                            name="plan_peroid"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Select plan period"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >
                                                            <option value="">Select</option>
                                                            <option value="day" key="0">Day</option>
                                                            <option value="week" key="1">Week</option>
                                                            <option value="month" key="2">Month</option>
                                                            <option value="year" key="3">Year</option>
                                                        </Field>
                                                        {errors.plan_peroid && touched.plan_peroid ? (
                                                            <span className="errorMsg ml-3">{errors.plan_peroid}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>License Interval Count<span className="required">*</span></h3></label>
                                                        <Field
                                                            name="plan_interval_count"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >
                                                            <option value="">Select</option>
                                                            <option value="1" key="1">1</option>
                                                            <option value="2" key="2">2</option>
                                                            <option value="3" key="3">3</option>
                                                            <option value="4" key="4">4</option>
                                                            <option value="5" key="5">5</option>
                                                            <option value="6" key="6">6</option>
                                                        </Field>
                                                        {errors.plan_interval_count && touched.plan_interval_count ? (
                                                            <span className="errorMsg ml-3">{errors.plan_interval_count}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Paid Services</label>
                                                        <br />
                                                        {this.state.plan_paid_services_list_api.map((data, index) => {
                                                            return <FormCheckbox
                                                                disabled={disabled === false ? 'disabled' : ''}
                                                                key={index}
                                                                inline
                                                                checked={data.service}
                                                                onChange={e => this.handleCheckboxChangePaidService(e, index, data.price)}
                                                            >
                                                                {data.value}
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Estimated Paid Price</label>
                                                        <Field
                                                            type="text"
                                                            className={'form-control w-25'}
                                                            autoComplete="nope"
                                                            value={this.state.paidServicePrice}
                                                            disabled={'disabled'}
                                                        />
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                {/* <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Free Services <span className="required">*</span></label>
                                                        <br />
                                                        {this.state.plan_free_services_list_api.map((data, index) => {
                                                            return <FormCheckbox
                                                                disabled={disabled === false ? 'disabled' : ''}
                                                                key={index}
                                                                inline
                                                                checked={data.service}
                                                                onChange={e => this.handleCheckboxChangeFreeService(e, index, data.price)}
                                                            >
                                                                {data.value}
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Estimated Free Price</label>
                                                        <Field
                                                            type="text"
                                                            className={'form-control w-25'}
                                                            autoComplete="nope"
                                                            value={this.state.freeServicePrice}
                                                            disabled={'disabled'}
                                                        />
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>*/}

                                                {/*<FormGroup controlId="formControlsTextarea">
                                                    <label><h3>Free Period<span className="required">*</span></h3></label>
                                                    <Field
                                                        name="plan_free_peroid"
                                                        type="text"
                                                        className={'form-control'}
                                                        autoComplete="nope"
                                                        placeholder="Enter plan free period"
                                                        value={values.plan_free_peroid || ''}
                                                        disabled={disabled === false ? 'disabled' : ''}
                                                    />
                                                    {errors.plan_free_peroid && touched.plan_free_peroid ? (
                                                        <span className="errorMsg ml-3">{errors.plan_free_peroid}</span>
                                                    ) : null}
                                                    <FormControl.Feedback />
                                                </FormGroup>
                                                */}


                                                <Col xs={3} md={3}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>License Currency<span className="required">*</span></h3></label>
                                                        <Field
                                                            name="plan_currency"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >
                                                            <option value="">Select currency</option>
                                                            {this.state.currencyCodes.map(data => {
                                                                return data.code !== 'XTS' && data.code !== 'XXX' && <option value={data.code.toLowerCase()} key={data.code}>{data.currency} ({data.code})</option>;
                                                            })}
                                                        </Field>
                                                        {errors.plan_currency && touched.plan_currency ? (
                                                            <span className="errorMsg ml-3">{errors.plan_currency}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={2} md={2}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>License Price<span className="required">*</span></h3></label>
                                                        <Field
                                                            name="plan_price"
                                                            type="number"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter license price"
                                                            value={values.plan_price || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.plan_price && touched.plan_price ? (
                                                            <span className="errorMsg ml-3">{errors.plan_price}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Status <span className="required">*</span></h3></label>
                                                        <Field
                                                            name="status"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >
                                                            <option value=""> Select Status</option>
                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive </option>
                                                        </Field>
                                                        {errors.status && touched.status ? (
                                                            <span className="errorMsg ml-3">{errors.status}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>&nbsp;</Col>
                                            </Row>
                                            <Row>&nbsp;</Row>
                                            {this.state.permission[1] && this.state.permission[1].status === true &&
                                                <Row className="show-grid text-center">
                                                    <Col xs={12} md={12}>
                                                        <Fragment>
                                                            {this.state.editLicenseEnable !== true ? (
                                                                <Fragment>
                                                                    <Button className="blue-btn border-0" onClick={this.handleeditLicenseEnable}>
                                                                        Edit
                                                    </Button>
                                                                </Fragment>) :
                                                                (<Fragment>
                                                                    <Button
                                                                        onClick={this.props.handelviewEditModalClose}
                                                                        className="but-gray border-0 mr-2">
                                                                        Cancel
                                                    </Button>
                                                                    <Button type="submit" className="blue-btn ml-2 border-0">
                                                                        Save
                                                    </Button>
                                                                </Fragment>)
                                                            }
                                                        </Fragment>
                                                    </Col>
                                                </Row>}
                                            {disabled === false ? null : (<Fragment>
                                                <Row>
                                                    <Col md={12}>
                                                        <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                                    </Col>
                                                </Row>
                                            </Fragment>)}
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        facility: state.auth.facility
    };
};
EditLicense.propTypes = {
    globState: PropTypes.object,
    onClickAction: PropTypes.func,
    onReload: PropTypes.func,
    onClick: PropTypes.func,
    handelviewEditModalClose: PropTypes.func,
    handleEditConfirMmsg: PropTypes.func,
    facility: PropTypes.any
};

export default connect(mapStateToProps, null)(EditLicense);