import React, { Component } from 'react';
import * as Yup from 'yup';
import axios from '../../../../../shared/eaxios';
import * as AppConst from './../../../../../common/constants';
import { Button, Col, FormControl, FormGroup, Image, Modal, Row } from 'react-bootstrap';
import { Field, Form, Formik } from 'formik';
import SuccessIco from '../../../../../assets/success-ico.png';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import PropTypes from 'prop-types';
import { FormCheckbox } from 'shards-react';

const initialValues = {
    plan_name: '',
    plan_peroid: '',
    plan_price: '',
    plan_free_peroid: '',
    status: ''
};

const addServiceSchema = Yup.object().shape({
    plan_name: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter name')
        .max(40, 'maximum characters length 40'),
    plan_peroid: Yup.string()
        .trim('Please remove whitespace')
        .strict(),
    plan_price: Yup.number()
        .strict()
        .required('Please enter service price'),
    plan_free_peroid: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter service price'),
    status: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please select status')
});

class AddLicense extends Component {
    state = {
        showConfirMmsg: false,
        servicesLists: [],
        errorMessge: null,
        addServiceLoader: false,
        addErrorMessge: null,
        plan_paid_services: [],
        plan_free_services: [],
        paidServicePrice: 0,
        freeServicePrice: 0
    };

    componentDidMount = () => {
        this.setState({ addServiceLoader: true });
        axios.get(AppConst.APIURL + '/api/listService')
            .then(res => {
                const servicesLists = res.data.data.data;
                const plan_paid_services = [];
                const plan_free_services = [];
                servicesLists.map(data => {
                    if (data.status === 1) {
                        plan_paid_services.push({ 'service': false, 'id': data.id, 'value': data.service_name, 'price': data.service_price });
                        plan_free_services.push({ 'service': false, 'id': data.id, 'value': data.service_name, 'price': data.service_price });
                    }
                });
                this.setState({
                    plan_paid_services,
                    plan_free_services,
                    addServiceLoader: false
                });
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                console.log(errorMsg);
            });
    }

    handleChange = (e, field) => {
        this.setState({
            [field]: e.target.value
        });
    };

    handleConfirmReviewClose = () => {
        this.setState({ showConfirMmsg: false });
    };

    handleConfirmReviewShow = () => {
        this.setState({ showConfirMmsg: true });
    };

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handleSubmit = (values, { resetForm }) => {
        this.setState({
            addServiceLoader: true,
        });
        const formData = new window.FormData();
        for (const key in values) {
            if (values.hasOwnProperty(key)) {
                formData.append(key, values[key]);
            }
        }

        let newValue = {
            plan_name: values.plan_name,
            plan_peroid: values.plan_peroid,
            plan_price: values.plan_price,
            plan_free_peroid: values.plan_free_peroid,
            plan_paid_services: JSON.stringify(this.state.plan_paid_services),
            plan_free_services: JSON.stringify(this.state.plan_free_services),
            status: values.status
        };
        console.log(newValue);
        const config = {
            headers: {
                //sessionKey: localStorage.getItem("sessionKey"),
                'Content-Type': 'application/json'
            }
        };
        axios
            .post(AppConst.APIURL + 'api/createPlan', newValue, config)
            .then(res => {
                console.log(res);
                resetForm({});
                this.props.handelAddModalClose();
                this.props.handleAddConfirMmsg();
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    addServiceLoader: false,
                    addErrorMessge: errorMsg,
                });
                setTimeout(() => {
                    this.setState({ deleteErrorMessge: null });
                }, 5000);
            });
    };

    handleCheckboxChangePaidService(e, service, price) {
        let stateCopy = Object.assign({}, this.state.plan_paid_services);
        stateCopy[service].service = !stateCopy[service].service;
        let paidServicePrice = 0;
        if (stateCopy[service].service === true)
            paidServicePrice = this.state.paidServicePrice + price;
        else if (stateCopy[service].service === false)
            paidServicePrice = this.state.paidServicePrice - price;
        this.setState({
            stateCopy,
            paidServicePrice
        });
    }

    handleCheckboxChangeFreeService(e, service, price) {
        let stateCopy = Object.assign({}, this.state.plan_free_services);
        stateCopy[service].service = !stateCopy[service].service;
        let freeServicePrice = 0;
        if (stateCopy[service].service === true)
            freeServicePrice = this.state.freeServicePrice + price;
        else if (stateCopy[service].service === false)
            freeServicePrice = this.state.freeServicePrice - price;
        this.setState({
            stateCopy,
            freeServicePrice
        });
    }

    render() {
        return (
            <div className="addcustomerSec">
                <div className="boxBg p-35">
                    {this.state.addErrorMessge ? (
                        <div className="alert alert-danger" role="alert">
                            {this.state.addErrorMessge}
                        </div>
                    ) : null}
                    <Row>
                        <Col sm={12}>
                            {this.state.addServiceLoader ? <LoadingSpinner /> : null}
                        </Col>
                    </Row>
                    <Row className="show-grid">
                        <Col xs={12} className="brd-right">
                            <Formik
                                initialValues={initialValues}
                                validationSchema={addServiceSchema}
                                onSubmit={this.handleSubmit}
                            >
                                {({
                                    values,
                                    errors,
                                    touched,
                                }) => {
                                    return (
                                        <Form>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>License Name <span className="required">*</span></label>
                                                        <Field
                                                            name="plan_name"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter License Name"
                                                            value={values.plan_name || ''}
                                                        />
                                                        {errors.plan_name && touched.plan_name ? (
                                                            <span className="errorMsg ml-3">{errors.plan_name}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>License Validity (in Days)<span className="required">*</span></label>
                                                        <Field
                                                            name="plan_peroid"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter License Validity"
                                                            value={values.plan_peroid || ''}
                                                        />
                                                        {errors.plan_peroid && touched.plan_peroid ? (
                                                            <span className="errorMsg ml-3">{errors.plan_peroid}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Free Period (in Days)<span className="required">*</span></label>
                                                        <Field
                                                            name="plan_free_peroid"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter Free Period Validity"
                                                            value={values.plan_free_peroid || ''}
                                                        />
                                                        {errors.plan_free_peroid && touched.plan_free_peroid ? (
                                                            <span className="errorMsg ml-3">{errors.plan_free_peroid}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Paid Services <span className="required">*</span></label>
                                                        <br />
                                                        {this.state.plan_paid_services.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.service}
                                                                onChange={e => this.handleCheckboxChangePaidService(e, index, data.price)}
                                                            >
                                                                {data.value}
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Estimated Price ($)</label>
                                                        <Field
                                                            type="text"
                                                            className={'form-control w-25'}
                                                            autoComplete="nope"
                                                            value={this.state.paidServicePrice}
                                                            disabled={'disabled'}
                                                        />
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Free Services <span className="required">*</span></label>
                                                        <br />
                                                        {this.state.plan_free_services.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.service}
                                                                onChange={e => this.handleCheckboxChangeFreeService(e, index, data.price)}
                                                            >
                                                                {data.value}
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Price ($)</label>
                                                        <Field
                                                            type="text"
                                                            className={'form-control w-25'}
                                                            autoComplete="nope"
                                                            value={this.state.freeServicePrice}
                                                            disabled={'disabled'}
                                                        />
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Plan Price <span className="required">*</span></label>
                                                        <Field
                                                            name="plan_price"
                                                            type="number"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter Plan Price"
                                                            value={values.plan_price || ''}
                                                        />
                                                        {errors.plan_price && touched.plan_price ? (
                                                            <span className="errorMsg ml-3">{errors.plan_price}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <span>Status <span className="required">*</span></span>
                                                        <Field
                                                            name="status"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >
                                                            <option value="" defaultValue="">Select Status</option>
                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        {errors.status && touched.status ? (
                                                            <span className="errorMsg ml-3">{errors.status}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    &nbsp;
                                                </Col>
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12} className="text-center">
                                                    <Button className="blue-btn" type="submit">
                                                        Save
                                                    </Button>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={12}>
                                                    <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                                </Col>
                                            </Row>
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Col>
                    </Row>
                </div>

                {/*======  confirmation popup  ===== */}
                <Modal
                    show={this.state.showConfirMmsg}
                    onHide={this.handleConfirmReviewClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Service has been successfully added</h5>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handleConfirmReviewClose}
                            className="but-gray" > Done
                        </Button>
                    </Modal.Footer>
                </Modal>

            </div>
        );
    }
}

AddLicense.propTypes = {
    handleAddConfirMmsg: PropTypes.func,
    businessId: PropTypes.number,
    handelAddModalClose: PropTypes.func,
};

export default AddLicense;
