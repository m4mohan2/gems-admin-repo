import React, { Component, Fragment } from 'react';
import * as Yup from 'yup';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import axios from '../../../../../shared/eaxios';
import * as AppConst from './../../../../../common/constants';
import draftToHtml from 'draftjs-to-html';
import { Button, Col, FormControl, FormGroup, Row } from 'react-bootstrap';
import { Field, Form, Formik } from 'formik';
import { Editor } from 'react-draft-wysiwyg';
import PropTypes from 'prop-types';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import htmlToDraft from 'html-to-draftjs';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import { connect } from 'react-redux';

const editServiceSchema = Yup.object().shape({
    service_name: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter name')
        .max(40, 'maximum characters length 40'),
    service_desc: Yup.string()
        .trim('Please remove whitespace')
        .strict(),
    service_price: Yup.number()
        .strict(),
    status: Yup.string()
        // .strict()
        .required('Please select status')
});

class EditServices extends Component {
    state = {
        showService: false,
        errorMessge: null,
        editServiceEnable: false,
        disabled: false,
        editServiceLoader: false,
        editErrorMessge: false,
        editorStateForDesc: EditorState.createEmpty(),
        permission: []
    };

    static getDerivedStateFromProps(props, state) {
        if (!state.serviceData) {
            return {
                ...props
            };
        }
    }

    handleEditServiceEnable = () => {
        this.setState({
            editServiceEnable: true,
            disabled: true
        });
    }

    handleEditServiceDisable = () => {
        this.setState({
            editServiceEnable: false,
            disabled: false
        });
        this.props.onReload(this.state.serviceData);
    }

    handleCloseService = () => {
        this.props.onClick();
    };

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handleSubmit = (values, { setSubmitting }) => {
        this.setState({
            editServiceLoader: true,
        });
        let newValue = {
            service_name: values.service_name,
            service_desc: draftToHtml(convertToRaw(this.state.editorStateForDesc.getCurrentContent())).trim(),
            service_price: values.service_price,
            status: values.status
        };
        const serviceData = {};
        Object.assign(serviceData, newValue);
        axios
            .put(AppConst.APIURL + `/api/editService/${values.id}`, newValue)
            .then(res => {
                console.log('Service details get data', res.data);
                setSubmitting(false);
                this.setState({
                    editServiceLoader: false,
                    editServiceEnable: false,
                    disabled: false,
                    serviceData
                }, () => {
                    this.props.handleEditConfirMmsg();
                });
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    editServiceLoader: false,
                    editErrorMessge: errorMsg,
                });

                setTimeout(() => {
                    this.setState({ deleteErrorMessge: null });
                }, 1000);

            });
    };

    componentDidMount() {
        const facility = this.props.facility;
        const permission = [];
        facility.map(data => {
            data.name === 'services' && data.permission.length !== 0 && permission.push(...data.permission);
        });
        this.setState({ permission });

        const initialValues = { ...this.props };
        const contentBlockDesc = htmlToDraft(initialValues.service_desc.trim());
        if (contentBlockDesc) {
            const contentStateDescription = ContentState.createFromBlockArray(contentBlockDesc.contentBlocks);
            const descriptionContent = EditorState.createWithContent(contentStateDescription);
            this.setState({ editorStateForDesc: descriptionContent });
        }
    }

    onEditorStateChangeDescription = (editorStateForDesc) => {
        this.setState({
            editorStateForDesc,
        });
    };

    render() {
        const initialValues = { ...this.props };
        const values = {
            id: initialValues.id,
            service_name: initialValues.service_name,
            service_desc: initialValues.service_desc,
            service_price: Number(initialValues.service_price),
            status: initialValues.status,
        };
        const {
            disabled
        } = this.state;

        return (

            <div className="addcustomerSec">
                <div className="boxBg p-35">
                    <Row>
                        <Col sm={12}>
                            {this.state.editServiceLoader ? <LoadingSpinner /> : null}
                        </Col>
                    </Row>
                    <Row className="show-grid">
                        <Col xs={12} md={12}>
                            <div className="sectionTitle">
                                <h2>{this.state.editServiceEnable !== true ? 'View' : 'Edit'} Service</h2>
                            </div>
                        </Col>
                        <Col xs={12} className="brd-right">
                            <Formik
                                initialValues={values}
                                validationSchema={editServiceSchema}
                                onSubmit={this.handleSubmit}
                                enableReinitialize={true} >
                                {({
                                    values,
                                    errors,
                                    touched,
                                    isSubmitting,
                                }) => {
                                    return (
                                        <Form className={disabled === false ? ('hideRequired') : null}>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Name <span className="required">*</span></h3></label>
                                                        <Field
                                                            name="service_name"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter service name"
                                                            value={values.service_name || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.service_name && touched.service_name ? (
                                                            <span className="errorMsg ml-3">{errors.service_name}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Description</h3>
                                                        </label>
                                                        <Editor
                                                            editorState={this.state.editorStateForDesc}
                                                            toolbarHidden={disabled === false ? true : false}
                                                            wrapperClassName="demo-wrapper"
                                                            editorClassName="form-control rounded-0 mt-n2"
                                                            onEditorStateChange={this.onEditorStateChangeDescription}
                                                            placeholder="Enter description"
                                                            readOnly={disabled === false ? 'readOnly' : ''}
                                                            toolbarClassName="text-body"
                                                            handlePastedText={() => false}
                                                            toolbar={{
                                                                options: ['inline', 'blockType', 'fontSize', 'list', 'textAlign', 'link', 'embedded', 'emoji', 'image', 'remove', 'history'],
                                                                image: { alt: { present: true, mandatory: false } }
                                                            }}
                                                        />
                                                        {errors.service_desc && touched.service_desc ? (
                                                            <span className="errorMsg ml-3">{errors.service_desc}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Service Price ($)<span className="required">*</span></h3></label>
                                                        <Field
                                                            name="service_price"
                                                            type="number"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter service price"
                                                            value={values.service_price || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.service_price && touched.service_price ? (
                                                            <span className="errorMsg ml-3">{errors.service_price}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <span><h3>Status <span className="required">*</span></h3></span>
                                                        <Field
                                                            name="status"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >
                                                            <option value="">
                                                                Select Status
                                            </option>
                                                            <option value="1" key="1">
                                                                Active
                                            </option>
                                                            <option value="0" key="0">
                                                                Inactive
                                            </option>
                                                        </Field>
                                                        {errors.status && touched.status ? (
                                                            <span className="errorMsg ml-3">{errors.status}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    &nbsp;
                                </Col>
                                            </Row>
                                            <Row>&nbsp;</Row>
                                            {this.state.permission[2] && this.state.permission[2].status === true &&
                                                <Row className="show-grid text-center">
                                                    <Col xs={12} md={12}>
                                                        <Fragment>
                                                            {this.state.editServiceEnable !== true ? (
                                                                <Fragment>
                                                                    <Button className="blue-btn border-0" onClick={this.handleEditServiceEnable}>
                                                                        Edit
                                                    </Button>
                                                                </Fragment>) :
                                                                (<Fragment>
                                                                    <Button
                                                                        onClick={this.props.handelviewEditModalClose}
                                                                        className="but-gray border-0 mr-2">
                                                                        Cancel
                                                    </Button>
                                                                    <Button type="submit" className="blue-btn ml-2 border-0" disabled={isSubmitting}>
                                                                        Save
                                                    </Button>
                                                                </Fragment>)
                                                            }
                                                        </Fragment>
                                                    </Col>
                                                </Row>}
                                            {disabled === false ? null : (<Fragment>
                                                <Row>
                                                    <Col md={12}>
                                                        <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                                    </Col>
                                                </Row>
                                            </Fragment>)}
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        facility: state.auth.facility
    };
};
EditServices.propTypes = {
    globState: PropTypes.object,
    onClickAction: PropTypes.func,
    onReload: PropTypes.func,
    onClick: PropTypes.func,
    handelviewEditModalClose: PropTypes.func,
    handleEditConfirMmsg: PropTypes.func,
    facility: PropTypes.any
};

export default connect(mapStateToProps, null)(EditServices);