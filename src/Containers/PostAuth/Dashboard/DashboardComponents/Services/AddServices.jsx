import React, { Component } from 'react';
import * as Yup from 'yup';
import { EditorState, convertToRaw } from 'draft-js';
import axios from '../../../../../shared/eaxios';
import * as AppConst from './../../../../../common/constants';
import draftToHtml from 'draftjs-to-html';
import { Button, Col, FormControl, FormGroup, Image, Modal, Row } from 'react-bootstrap';
import { Field, Form, Formik } from 'formik';
import SuccessIco from '../../../../../assets/success-ico.png';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import { Editor } from 'react-draft-wysiwyg';
import PropTypes from 'prop-types';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

const initialValues = {
    service_name: '',
    service_desc: '',
    service_price: '',
    status: ''
};

const addServiceSchema = Yup.object().shape({
    service_name: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter name')
        .max(40, 'maximum characters length 40'),
    service_desc: Yup.string()
        .trim('Please remove whitespace')
        .strict(),
    service_price: Yup.number()
        .strict()
        .required('Please enter service price'),
    status: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please select status')
});

class AddBusinessCustomers extends Component {
    state = {
        showConfirMmsg: false,
        errorMessge: null,
        addServiceLoader: false,
        addErrorMessge: null,
        descriptionContent: EditorState.createEmpty(),
    };

    onEditorStateChangeDescription = (descriptionContent) => {
        this.setState({
            descriptionContent,
        });
    };

    handleChange = (e, field) => {
        this.setState({
            [field]: e.target.value
        });
    };

    handleConfirmReviewClose = () => {
        this.setState({ showConfirMmsg: false });
    };

    handleConfirmReviewShow = () => {
        this.setState({ showConfirMmsg: true });
    };

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handleSubmit = (values, { resetForm, setSubmitting }) => {
        this.setState({
            addServiceLoader: true,
        });
        const formData = new window.FormData();
        for (const key in values) {
            if (values.hasOwnProperty(key)) {
                formData.append(key, values[key]);
            }
        }
        let newValue = {
            service_name: values.service_name,
            service_desc: draftToHtml(convertToRaw(this.state.descriptionContent.getCurrentContent())).trim(),
            service_price: values.service_price,
            status: values.status
        };
        const config = {
            headers: {
                //sessionKey: localStorage.getItem("sessionKey"),
                'Content-Type': 'application/json'
            }
        };
        axios
            .post(AppConst.APIURL + 'api/createService', newValue, config)
            .then(res => {
                console.log(res);
                resetForm({});
                setSubmitting(false);
                this.props.handelAddModalClose();
                this.props.handleAddConfirMmsg();
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    addServiceLoader: false,
                    addErrorMessge: errorMsg,
                });
                setTimeout(() => {
                    this.setState({ deleteErrorMessge: null });
                }, 5000);
            });
    };

    render() {
        return (
            <div className="addcustomerSec">
                <div className="boxBg p-35">
                    {this.state.addErrorMessge ? (
                        <div className="alert alert-danger" role="alert">
                            {this.state.addErrorMessge}
                        </div>
                    ) : null}
                    <Row>
                        <Col sm={12}>
                            {this.state.addServiceLoader ? <LoadingSpinner /> : null}
                        </Col>
                    </Row>
                    <Row className="show-grid">
                        <Col xs={12} className="brd-right">
                            <Formik
                                initialValues={initialValues}
                                validationSchema={addServiceSchema}
                                onSubmit={this.handleSubmit}
                            >
                                {({
                                    values,
                                    errors,
                                    touched,
                                    isSubmitting,
                                }) => {
                                    return (
                                        <Form>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Name <span className="required">*</span></label>
                                                        <Field
                                                            name="service_name"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter Name"
                                                            value={values.service_name || ''}
                                                        />
                                                        {errors.service_name && touched.service_name ? (
                                                            <span className="errorMsg ml-3">{errors.service_name}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Description</label>
                                                        <div className='editor'>
                                                            <Editor
                                                                editorState={this.state.descriptionContent}
                                                                wrapperClassName="demo-wrapper"
                                                                editorClassName="form-control rounded-0 mt-n2"
                                                                onEditorStateChange={this.onEditorStateChangeDescription}
                                                                placeholder="Enter description"
                                                                toolbarClassName="text-body"
                                                                handlePastedText={() => false}
                                                                toolbar={{
                                                                    options: ['inline', 'blockType', 'fontSize', 'list', 'textAlign', 'link', 'embedded', 'emoji', 'image', 'remove', 'history'],
                                                                    image: { alt: { present: true, mandatory: false } }
                                                                }}
                                                            />
                                                        </div>
                                                        {errors.service_desc && touched.service_desc ? (
                                                            <span className="errorMsg ml-3">{errors.service_desc}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Service Price ($)<span className="required">*</span></label>
                                                        <Field
                                                            name="service_price"
                                                            type="number"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter Service Price"
                                                            value={values.service_price || ''}
                                                        />
                                                        {errors.service_price && touched.service_price ? (
                                                            <span className="errorMsg ml-3">{errors.service_price}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <span>Status <span className="required">*</span></span>
                                                        <Field
                                                            name="status"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >
                                                            <option value="" defaultValue="">Select Status</option>
                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        {errors.status && touched.status ? (
                                                            <span className="errorMsg ml-3">{errors.status}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    &nbsp;
                                                </Col>
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12} className="text-center">
                                                    <Button className="blue-btn" type="submit" disabled={isSubmitting}>
                                                        Save
                                                    </Button>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={12}>
                                                    <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                                </Col>
                                            </Row>
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Col>
                    </Row>
                </div>

                {/*======  confirmation popup  ===== */}
                <Modal
                    show={this.state.showConfirMmsg}
                    onHide={this.handleConfirmReviewClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Service has been successfully added</h5>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handleConfirmReviewClose}
                            className="but-gray" > Done
                        </Button>
                    </Modal.Footer>
                </Modal>

            </div>
        );
    }
}

AddBusinessCustomers.propTypes = {
    handleAddConfirMmsg: PropTypes.func,
    businessId: PropTypes.number,
    handelAddModalClose: PropTypes.func,
};

export default AddBusinessCustomers;
