import React, { Component } from 'react';
import './Home.scss';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import * as am4maps from '@amcharts/amcharts4/maps';
import am4geodata_worldLow from '@amcharts/amcharts4-geodata/worldLow';
import axios from '../../../../../shared/eaxios';
import * as AppConst from './../../../../../common/constants';
import Moment from 'moment';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import { Pie, HorizontalBar } from 'react-chartjs-2';
import NoImage from '../../../../../assets/no_image.png';
import ReactTooltip from 'react-tooltip';

am4core.useTheme(am4themes_animated);

class Home extends Component {
    state = {
        loading: false,
        errorMessge: null,
        popularGemsList: [],
        genderWiseList: [],
        audienceCountryWiseList: [],
        fromDate: Moment(new Date()).subtract(1, 'months').format('yyyy-MM-DD'),
        toDate: Moment(new Date()).format('yyyy-MM-DD'),
        geodata: [],
        popularGemsLoader: false,
        demographicLoader: false,
        chartErrorMessge: '',
        lastSelectedDays: '30',
        totalRecordList: '',
        activeUserList: [],
    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Unknown error!';
        }
        return errorMessge;
    }

    pieChart = (data) => {
        let chart = am4core.create('piediv', am4charts.PieChart);
        chart.data = data;
        let pieSeries = chart.series.push(new am4charts.PieSeries());
        pieSeries.dataFields.value = 'percent';
        pieSeries.dataFields.category = 'gender';

        pieSeries.labels.template.disabled = true;
        pieSeries.ticks.template.disabled = true;

        pieSeries.slices.template.tooltipText = '';
        chart.legend = new am4charts.Legend();
        chart.legend.labels.template.fill = am4core.color('white');
        chart.legend.valueLabels.template.fill = am4core.color('white');
    }

    demographicChart = () => {
        let chart = am4core.create('demographicdiv', am4maps.MapChart);

        // Set map definition
        chart.geodata = am4geodata_worldLow;

        // Set projection
        chart.projection = new am4maps.projections.Miller();

        // Create map polygon series
        let polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

        // Make map load polygon (like country names) data from GeoJSON
        polygonSeries.useGeodata = true;

        // Configure series
        let polygonTemplate = polygonSeries.mapPolygons.template;
        polygonTemplate.tooltipText = '{name}: {value}';
        polygonTemplate.fill = am4core.color('#74B266');

        // Create hover state and set alternative fill color
        let hs = polygonTemplate.states.create('hover');
        hs.properties.fill = am4core.color('#367B25');

        // Remove Antarctica
        polygonSeries.exclude = ['AQ'];

        polygonSeries.heatRules.push({
            'property': 'fill',
            'target': polygonSeries.mapPolygons.template,
            'min': am4core.color('#F5F9F2'),
            'max': am4core.color('#3D7F05')
        });

        polygonSeries.data = this.state.geodata;

        // Bind 'fill' property to 'fill' key in data
        polygonTemplate.propertyFields.fill = 'fill';

    }

    popularGems = (fromdate, todate) => {
        this.setState({ popularGemsLoader: true });
        axios.get(AppConst.APIURL + `/api/popularGems?from_date=${fromdate}&to_date=${todate}`)
            .then(res => {
                this.setState({ popularGemsList: res.data.message, popularGemsLoader: false });
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    errorMessge: errorMsg,
                    popularGemsLoader: false
                });
                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);
            });
    }

    audienceCountryWise = (fromdate, todate) => {
        axios.get(AppConst.APIURL + `/api/adminAudienceCountryWise?from_date=${fromdate}&to_date=${todate}`)
            .then(res => {
                const geodata = [];
                res.data.message.map((data) => {
                    return geodata.push({ id: data.iso2, value: data.gems_comment_count + data.gems_like_count + data.gems_share_count });
                });
                this.setState({ audienceCountryWiseList: res.data.message, geodata });
                this.demographicChart();
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    errorMessge: errorMsg,
                });
                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);
            });
    }

    engagementGenderWise = (fromdate, todate) => {
        axios.get(AppConst.APIURL + `/api/adminEngagementGenderWise?from_date=${fromdate}&to_date=${todate}`)
            .then(res => {
                this.setState({ genderWiseList: res.data.message });
                this.pieChart(res.data.message);
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    errorMessge: errorMsg,
                    loading: false
                });
                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);
            });
    }

    totalRecord = (fromdate, todate) => {
        this.setState({ loading: true });
        axios.get(AppConst.APIURL + `/api/totalRecords?from_date=${fromdate}&to_date=${todate}`)
            .then(res => {
                const totalRecordList = res.data.message;
                this.setState({
                    totalRecordList
                });
                this.setState({ loading: false });
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    errorMessge: errorMsg,
                    loading: false
                });
                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);
            });
    }

    activeUser = (fromdate, todate) => {
        this.setState({ loading: true });
        axios.get(AppConst.APIURL + `/api/activeUser?from_date=${fromdate}&to_date=${todate}`)
            .then(res => {
                const activeUserList = res.data.message;
                this.setState({
                    activeUserList
                });
                this.setState({ loading: false });
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    errorMessge: errorMsg,
                    loading: false
                });
                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);
            });
    }

    componentDidMount() {
        this.popularGems(this.state.fromDate, this.state.toDate);
        this.totalRecord(this.state.fromDate, this.state.toDate);
        this.activeUser(this.state.fromDate, this.state.toDate);
        this.audienceCountryWise(this.state.fromDate, this.state.toDate);
        this.engagementGenderWise(this.state.fromDate, this.state.toDate);
    }

    handleLastDaysChange = (e) => {
        const toDate = Moment(new Date()).format('yyyy-MM-DD');
        const fromDate = Moment(new Date()).subtract(e.target.value, 'days').format('yyyy-MM-DD');
        this.setState({ lastSelectedDays: e.target.value, fromDate, toDate });
        this.popularGems(fromDate, toDate);
        this.totalRecord(fromDate, toDate);
        this.activeUser(fromDate, toDate);
        this.audienceCountryWise(fromDate, toDate);
        this.engagementGenderWise(fromDate, toDate);
    }

    render() {
        const optionsForPie = {
            labels: ['Claim Approved', 'Claim Pending', 'Claim Rejected'],
            datasets: [
                {
                    label: 'Count',
                    backgroundColor: [
                        '#14A76C',
                        '#F8E981',
                        '#F76C6C'
                    ],
                    hoverBackgroundColor: [
                        '#138457',
                        '#B4A960',
                        '#B14B4B'
                    ],
                    data: [this.state.totalRecordList && this.state.totalRecordList.claim_approved,
                    this.state.totalRecordList && this.state.totalRecordList.claim_pending,
                    this.state.totalRecordList && this.state.totalRecordList.claim_rejected]
                }
            ]
        };
        const gemNameList = this.state.popularGemsList && this.state.popularGemsList.map(data => {
            return data.name;
        });
        const chartdata = this.state.popularGemsList && this.state.popularGemsList.map(data => {
            return data.saved_lists_count + data.total_comment_count + data.total_like_count + data.total_share_count + data.total_view_count;
        });
        const optionsForBar = {
            labels: gemNameList,
            datasets: [
                {
                    label: 'Total',
                    backgroundColor: [
                        '#24305E',
                        '#A64AC9',
                        '#A8D0E6',
                        '#F76C6C',
                        '#F8E9A1'
                    ],
                    borderColor: [
                        '#24305E',
                        '#A64AC9',
                        '#A8D0E6',
                        '#F76C6C',
                        '#F8E9A1'
                    ],
                    borderWidth: 2,
                    fontColor: '#5A5757',
                    data: chartdata
                }
            ]
        };

        return (
            <div className='dashboardInner businessOuter pt-3' >
                {this.state.loading ? <LoadingSpinner /> :
                    <React.Fragment>

                        <div className='row'>
                            <div className='col-lg-10 col-md-10 col-sm-8 col-xs-6'></div>
                            <div className='col-lg-2 col-md-2 col-sm-4 col-xs-6'>
                                <select value={this.state.lastSelectedDays} onChange={this.handleLastDaysChange} className='form-control'>
                                    <option value='30'>Last 30 days</option>
                                    <option value='60'>Last 60 days</option>
                                    <option value='90'>Last 90 days</option>
                                    <option value='120'>Last 120 days</option>
                                    <option value='150'>Last 150 days</option>
                                    <option value='180'>Last 180 days</option>
                                    <option value='210'>Last 210 days</option>
                                    <option value='240'>Last 240 days</option>
                                    <option value='270'>Last 270 days</option>
                                    <option value='300'>Last 300 days</option>
                                    <option value='330'>Last 330 days</option>
                                    <option value='360'>Last 360 days</option>
                                </select>
                            </div>
                        </div>

                        <div className='row mt-3'>
                            <div className='col text-center'>
                                <div className='w-50 rounded'
                                    style={{ backgroundColor: '#487E8B', padding: '6px', margin: 'auto' }}>
                                    Total Users
                                </div>
                                {/* <button className='btn-sm btn-primary' disabled>Total Users</button> */}
                                <h6 className='mt-3'>{this.state.totalRecordList && this.state.totalRecordList.user_count}</h6>
                            </div>
                            <div className='col text-center'>
                                <div className='rounded'
                                    style={{ backgroundColor: '#487E8B', padding: '6px', margin: 'auto', maxWidth: '200px' }}>
                                    Total Providers
                                </div>
                                <h6 className='mt-3 ml-5'>{this.state.totalRecordList && this.state.totalRecordList.provider_count}</h6>
                            </div>
                            <div className='col text-center'>
                                <div className='w-50 rounded'
                                    style={{ backgroundColor: '#487E8B', padding: '6px', margin: 'auto' }}>
                                    Total Sales
                                </div>
                                <h6 className='mt-3 ml-2'>{this.state.totalRecordList && this.state.totalRecordList.sales_currency} {this.state.totalRecordList && this.state.totalRecordList.sales_count}</h6>
                            </div>
                        </div>

                        <div className='row mt-5'>
                            <div className='col-lg-7 col-md-7 col-sm-12 col-xs-12'>
                                <button className='btn-sm btn-secondary' disabled>Trending GEMS based on User Engagement</button>
                                <div className="mt-3">
                                    <HorizontalBar
                                        data={optionsForBar}
                                        type={'horizontalBar'}
                                        options={{
                                            responsive: true,
                                            maintainAspectRatio: true,
                                            legend: {
                                                display: false,
                                                position: 'right'
                                            },
                                            scales: {
                                                xAxes: [{
                                                    ticks: {
                                                        beginAtZero: true,
                                                        min: 0
                                                    },
                                                    gridLines: {
                                                        color: '#747978',
                                                        zeroLineColor: '#747978'
                                                    },
                                                }],
                                                yAxes: [{
                                                    gridLines: {
                                                        color: '#747978',
                                                        zeroLineColor: '#747978'
                                                    },
                                                    ticks: {
                                                        fontColor: '#CCC',
                                                        callback: function (label) {
                                                            if (/\s/.test(label)) {
                                                                return label.split('-');
                                                            } else {
                                                                return label;
                                                            }
                                                        }
                                                    },
                                                }]
                                            }
                                        }}
                                    />
                                </div>
                            </div>
                            <div className='col-lg-5 col-md-5 col-sm-12 col-xs-12'>
                                <button className='btn-sm btn-secondary' disabled>Active User Profiles</button>
                                <div className='mt-3'>
                                    {this.state.activeUserList && this.state.activeUserList.map((data, index) => {
                                        return data.profile_pic ?
                                            <React.Fragment>
                                                <img
                                                    key={index}
                                                    src={AppConst.UPLOADURL + '/profile/' + data.profile_pic}
                                                    className='rounded-circle m-3'
                                                    style={{ height: '80px', width: '80px', cursor: 'pointer' }}
                                                    data-tip={data.first_name + ' ' + data.last_name}>
                                                </img>
                                                <ReactTooltip place="bottom" type="info" effect="float" />
                                            </React.Fragment> :
                                            <React.Fragment>
                                                <img
                                                    key={index}
                                                    src={NoImage}
                                                    className='rounded-circle m-3'
                                                    style={{ height: '80px', width: '80px', cursor: 'pointer' }}
                                                    data-tip={data.first_name + ' ' + data.last_name}>
                                                </img>
                                                <ReactTooltip place="bottom" type="info" effect="float" />
                                            </React.Fragment>;
                                    })}
                                </div>
                            </div>
                        </div>

                        <div className='row mt-5'>
                            <div className='col-12'>
                                <button className='btn-sm btn-secondary' disabled>Engagement Audience</button>
                            </div>
                        </div>
                        <div className='mt-3'>
                            <div className='row'>
                                <div className='col-md-7 col-sm-12'>
                                    <div id='demographicdiv' style={{ width: '100%', height: '300px' }}></div>
                                </div>
                                <div className='col-md-5 col-sm-12'>
                                    <div id='piediv' style={{ width: '100%', height: '300px' }}></div>
                                </div>
                            </div>
                        </div>

                        <div className='row mt-5'>
                            <div className='col-12'>
                                <button className='btn-sm btn-secondary' disabled>Claim Requests</button>
                            </div>
                        </div>
                        <div className='mt-3'>
                            <div className='row'>
                                <div className='col-lg-7 col-md-7 col-sm-12 col-xs-12'>
                                    <div className='row mt-5'>
                                        <div className='col-3'>
                                            <div className="rounded" style={{ border: '1px solid #44318D', padding: '3px', backgroundColor: '#44318D', textAlign: 'center' }}>{this.state.totalRecordList && this.state.totalRecordList.total_claim} Claim <br />Requests</div>
                                        </div>
                                        <div className='col-3'>
                                            <div className="rounded" style={{ border: '1px solid #14A76C', padding: '3px', backgroundColor: '#14A76C', textAlign: 'center' }}>{this.state.totalRecordList && this.state.totalRecordList.claim_approved} <br />Approved</div>
                                        </div>
                                        <div className='col-3'>
                                            <div className="rounded" style={{ border: '1px solid #F8E981', padding: '3px', backgroundColor: '#F8E981', textAlign: 'center', color: '#444948' }}>{this.state.totalRecordList && this.state.totalRecordList.claim_pending} <br />Pending</div>
                                        </div>
                                        <div className='col-3'>
                                            <div className="rounded" style={{ border: '1px solid #F76C6C', padding: '3px', backgroundColor: '#F76C6C', textAlign: 'center' }}>{this.state.totalRecordList && this.state.totalRecordList.claim_rejected} <br />Rejected</div>
                                        </div>
                                    </div>
                                </div>
                                <div className='col-lg-5 col-md-5 col-sm-12 col-xs-12 mt-3'>
                                    <Pie
                                        data={optionsForPie}
                                        options={{
                                            legend: {
                                                display: true,
                                                position: 'right'
                                            }
                                        }}
                                    />
                                </div>
                            </div>
                        </div>



                    </React.Fragment>}
            </div >
        );
    }
}

export default Home;
