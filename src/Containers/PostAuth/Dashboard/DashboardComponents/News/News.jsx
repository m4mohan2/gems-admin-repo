import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import axios from '../../../../../shared/eaxios';
import PropTypes from 'prop-types';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import { Table, Row, Modal, Col, Image, Button } from 'react-bootstrap';
import { FaCaretDown } from 'react-icons/fa';
import { FaCaretUp } from 'react-icons/fa';
import SuccessIco from '../../../../../assets/success-ico.png';
import Pagination from 'react-js-pagination';
import EditBusinessCustomers from './EditNews';
import AddNews from './AddNews';
import * as AppConst from './../../../../../common/constants';
import ReactHtmlParser from 'react-html-parser';
import 'font-awesome/css/font-awesome.min.css';

class News extends Component {

	state = {
		activePage: 1,
		totalCount: 0,
		itemPerPage: 10,
		sort: 1,
		field: null,
		fetchErrorMsg: null,
		newsList: [],
		loading: true,
		sortingActiveID: 1,
		bankId: null,
		status: null,
		permission: []
	}

	_isMounted = false;

	displayError = (e) => {
		let errorMessge = '';
		try {
			errorMessge = e.data.message ? e.data.message : e.data.error_description;
		} catch (e) {
			errorMessge = 'Access is denied!';
		}
		return errorMessge;
	}

	sortingActive = (id) => {
		this.setState({
			sortingActiveID: id
		});
	}

	fetchNewsList = (sort = this.state.sort, field = this.state.field) => {
		this.setState({
			loading: true,
			sort: sort,
			field: field
		}, () => {
			axios
				.get(
					AppConst.APIURL + `/api/newsListAdmin?&pageSize=${this.state.itemPerPage}&page=${this.state.sort}&searchKey=${this.state.serviceSearchKey}`
				)
				.then(res => {
					console.log(res);
					const newsList = res.data.data;
					const totalCount = res.data.total;
					if (this._isMounted) {
						this.setState({
							newsList,
							totalCount: totalCount,
							loading: false
						});
					}
				})
				.catch(e => {
					let errorMsg = this.displayError(e);
					this.setState({
						fetchErrorMsg: errorMsg,
						loading: false
					}, () => {
						console.log('this.state.fetchErrorMsg', this.state.fetchErrorMsg);
					});
					setTimeout(() => {
						this.setState({ fetchErrorMsg: null });
					}, 5000);
				});
		});
	}


	handlePageChange = pageNumber => {
		this.setState({ activePage: pageNumber });
		this.fetchNewsList(pageNumber > 0 ? pageNumber : 0, this.state.field);
	};

	handleChangeItemPerPage = (e) => {
		this.setState({ itemPerPage: e.target.value },
			() => {
				this.fetchNewsList();
			});
	}

	resetPagination = () => {
		this.setState({ activePage: 1 });
	}

	rePagination = () => {
		this.setState({ activePage: 0 });

	}

	handelAddModal = () => {
		this.setState({
			addModal: true,
		});
	}

	handelAddModalClose = () => {
		this.setState({
			addModal: false,
		});
	}

	handleAddConfirMmsg = () => {
		this.setState({
			addConfirMmsg: true,
		}, () => {
			this.fetchNewsList();
		});
	}

	handleAddConfirMmsgClose = () => {
		this.setState({
			addConfirMmsg: false,
		});
	}

	handleEditConfirMmsg = () => {
		this.setState({
			viewEditModal: false,
			editConfirMmsg: true
		}, () => {
			this.fetchNewsList();
		});
	}

	handleEditConfirMmsgClose = () => {
		this.setState({
			editConfirMmsg: false
		});
	}

	handelviewEditModal = (data) => {
		this.setState({
			viewEditModal: true,
			vendorData: data
		}, () => {
			console.log('this.state.viewEditData@@', this.state.vendorData);
		});
	}

	handelviewEditModalClose = () => {
		this.setState({
			viewEditModal: false,
		});
	}

	handelStatusModal = (id, status) => {
		this.setState({
			statusChange: true,
			bankId: id,
			status: status
		});
	}

	handleStatusChangedClose = () => {
		this.setState({
			statusConfirMmsg: false,
			successMessage: null
		});
	}

	componentDidMount() {
		this._isMounted = true;
		const facility = this.props.facility;
		const permission = [];
		facility.map(data => {
			data.name === 'news' && data.permission.length !== 0 && permission.push(...data.permission);
		});
		this.setState({ permission }, () => {
			permission[1].status === true &&
				this.fetchNewsList();
		});
	}

	componentWillMount() {
		this._isMounted = false;
	}

	handleHide = () => {
		this.setState({
			statusChange: false,
			statuserrorMsg: null
		});
	}

	handleStatus(id, status) {

		let updateArray = {
			'status': status
		};

		axios.put(AppConst.APIURL + `/api/newsStatusUpdate/${id}`, updateArray)
			.then(res => {
				console.log('--------------res------', status);
				console.log('--------------res------', res);
				this.setState({
					bankId: null,
					status: null,
					successMessage: 'Status successfully changed',
					statusConfirMmsg: true
				});
				this.handleHide();
				this.fetchNewsList();
			}

			)
			.catch(e => {
				let errorMsg = this.displayError(e);
				this.setState({
					statuserrorMsg: errorMsg,
					loading: false


				});
				setTimeout(() => {
					this.setState({ errorMessge: null });
				}, 5000);
			});
	}

	handelDeleteModal = (id) => {
		this.setState({
			deleteGem: true,
			id: id
		});
	}

	handleDeleteHide = () => {
		this.setState({
			deleteGem: false,
			deleteerrorMsg: null
		});
	}

	handleDelete(id) {
		axios.delete(AppConst.APIURL + `/api/deleteNews/${id}`)
			.then(res => {
				console.log('--------------res------', res);
				this.setState({
					id: null,
					successMessage: 'Record deleted successfully',
					deleteConfirMmsg: true
				});
				this.handleDeleteHide();
				this.fetchNewsList();
			}

			)
			.catch(e => {
				let errorMsg = this.displayError(e);
				this.setState({
					deleteerrorMsg: errorMsg,
					loading: false


				});
				setTimeout(() => {
					this.setState({ errorMessge: null });
				}, 5000);
			});

	}

	handleDeleteChangedClose = () => {
		this.setState({
			deleteConfirMmsg: false,
			successMessage: null
		});
	}

	render() {
		return (
			<div className="dashboardInner businessOuter pt-3">
				{this.state.permission[1] && this.state.permission[1].status === true ?
					<Fragment>
						{this.state.permission[0] && this.state.permission[0].status === true ?
							<Row className="pt-3">
								<Col sm={6} md={6}>
								</Col>
								<Col sm={6} md={6}>
									<button className="btn btn-primary float-right" onClick={() => this.handelAddModal()}>Add NEWS</button>
								</Col>
							</Row> : ''}
						<Row className="show-grid pt-3">
							<Col sm={12} md={12}>
								<div className="boxBg">
									<Table responsive hover>
										<thead className="theaderBg">
											<tr>

												<th>
													<span className="float-left pr-2">
														Title
                                            </span>
													<span className="d-flex flex-column-reverse sortingFontSize">
														<button className="custom-btn-focus sortingArrow">
															<FaCaretDown
																className={'cursorPointer ' + (this.state.sortingActiveID == 8 ? 'activeColor' : '')}
																onClick={() => {
																	this.fetchNewsList(0, '', false, 'title');
																	this.sortingActive(8);
																	this.setState({ activePage: 1 });
																}} />
														</button>
														<button className="custom-btn-focus sortingArrow">
															<FaCaretUp
																className={'cursorPointer ' + (this.state.sortingActiveID == 7 ? 'activeColor' : '')}
																onClick={() => {
																	this.fetchNewsList(0, '', true, 'title');
																	this.sortingActive(7);
																	this.setState({ activePage: 1 });
																}} />
														</button>
													</span>
												</th>
												<th>
													<span className="float-left pr-2">
														Short Description
                                            </span>
													<span className="d-flex flex-column-reverse sortingFontSize">
														<button className="custom-btn-focus sortingArrow">
															<FaCaretDown
																className={'cursorPointer ' + (this.state.sortingActiveID == 10 ? 'activeColor' : '')}
																onClick={() => {
																	this.fetchNewsList(0, '', false, 'createdDate');
																	this.sortingActive(10);
																	this.setState({ activePage: 1 });
																}} />
														</button>
														<button className="custom-btn-focus sortingArrow">
															<FaCaretUp
																className={'cursorPointer ' + (this.state.sortingActiveID == 9 ? 'activeColor' : '')}
																onClick={() => {
																	this.fetchNewsList(0, '', true, 'createdDate');
																	this.sortingActive(9);
																	this.setState({ activePage: 1 });
																}} />
														</button>
													</span>
												</th>
												<th className="center">Staus</th>
												<th className="center">Action</th>
											</tr>
										</thead>
										<tbody>
											{this.state.loading ? (<tr>
												<td colSpan={12}>
													<LoadingSpinner />
												</td>
											</tr>) :
												this.state.newsList.length > 0 ? (
													this.state.newsList.map(news => (

														<tr key={news.id}>

															<td>{news.title}</td>
															<td>{ReactHtmlParser(news.short_description)}</td>
															<td className="center">
																{
																	news.status === 1
																		? <i className="fa fa-circle green" aria-hidden="true" title="Active"
																			onClick={() => this.state.permission[2] && this.state.permission[2].status === true && this.handelStatusModal(news.id, '0')}></i>
																		: <i className="fa fa-circle red" aria-hidden="true" title="Inactive"
																			onClick={() => this.state.permission[2] && this.state.permission[2].status === true && this.handelStatusModal(news.id, '1')}></i>
																}
															</td>
															<td className="center"><i className="fa fa-eye" aria-hidden="true" title="View" onClick={() => this.handelviewEditModal(news)}></i>
                                                    &nbsp;
													{this.state.permission[3] && this.state.permission[3].status === true ?
																	<i className="fa fa-trash-o red" aria-hidden="true" title="Delete" onClick={() => this.handelDeleteModal(news.id)} ></i>
																	: ''}
															</td>
														</tr>


													))
												)
													:
													this.state.fetchErrorMsg ? null : (
														<tr>
															<td colSpan={12}>
																<p className="text-center">No records found</p>
															</td>
														</tr>
													)
											}
										</tbody>
									</Table>
								</div>
							</Col>
						</Row>

						{this.state.totalCount ? (
							<Row>
								<Col md={4} className="d-flex flex-row mt-20">
									<span className="mr-2 mt-2 font-weight-500">Items per page</span>
									<select
										id={this.state.itemPerPage}
										className="form-control truncatefloat-left w-90"
										onChange={this.handleChangeItemPerPage}
										value={this.state.itemPerPage}>
										<option value='10'>10</option>
										<option value='25'>25</option>
										<option value='50'>50</option>
										<option value='100'>100</option>
									</select>
								</Col>
								<Col md={8}>
									<div className="paginationOuter text-right">
										<Pagination
											activePage={this.state.activePage}
											itemsCountPerPage={this.state.itemPerPage}
											totalItemsCount={this.state.totalCount}
											onChange={this.handlePageChange}
										/>
									</div>
								</Col>
							</Row>
						) : null}
					</Fragment>
					: <h5 className="text-center p-3">You do not have any permission to view this content.</h5>}



				{/* Add News Modal */}
				<Modal
					show={this.state.addModal}
					onHide={this.handelAddModalClose}
					className="right full noPadding slideModal"
				>
					<Modal.Header closeButton></Modal.Header>
					<Modal.Body className="">
						<div className="modalHeader">
							<Row>
								<Col md={9}>
									<h1>Add NEWS</h1>
								</Col>
							</Row>
						</div>
						<div className="modalBody content-body noTabs">
							<AddNews
								handleAddConfirMmsg={this.handleAddConfirMmsg}
								//businessId={this.props.globalState.business.businessData.id}
								handelAddModalClose={this.handelAddModalClose}
							/>
						</div>
					</Modal.Body>

				</Modal>

				{/* Edit News Modal */}
				<Modal
					show={this.state.viewEditModal}
					onHide={this.handelviewEditModalClose}
					className="right full noPadding slideModal"
				>
					<Modal.Header closeButton></Modal.Header>
					<Modal.Body className="">
						<div className="modalHeader">
							<Row>
								<Col md={9}>
									<h1>Edit News</h1>
								</Col>
							</Row>
						</div>
						<div className="modalBody content-body noTabs">
							<EditBusinessCustomers
								{...this.state.vendorData}
								handelviewEditModalClose={this.handelviewEditModalClose}
								handleEditConfirMmsg={this.handleEditConfirMmsg} />
						</div>
					</Modal.Body>

				</Modal>

				{/*====== Edit confirmation popup  ===== */}
				<Modal
					show={this.state.editConfirMmsg}
					onHide={this.handleEditConfirMmsgClose}
					className="payOptionPop"
				>
					<Modal.Body>
						<Row>
							<Col md={12} className="text-center">
								<Image src={SuccessIco} />
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<h5>Record has been successfully edited</h5>
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<Button
									onClick={this.handleEditConfirMmsgClose}
									className="but-gray"
								>
									Return
                                </Button>
							</Col>
						</Row>
					</Modal.Body>
				</Modal>

				{/*======  Add confirmation popup  ===== */}
				<Modal
					show={this.state.addConfirMmsg}
					onHide={this.handleAddConfirMmsgClose}
					className="payOptionPop"
				>
					<Modal.Body>
						<Row>
							<Col md={12} className="text-center">
								<Image src={SuccessIco} />
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<h5>Record has been successfully Added</h5>
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<Button
									onClick={this.handleAddConfirMmsgClose}
									className="but-gray"
								>
									Return
                                </Button>
							</Col>
						</Row>
					</Modal.Body>
				</Modal>

				{/*========================= Modal for Status change =====================*/}
				<Modal
					show={this.state.statusChange}
					onHide={this.handleHide}
					dialogClassName="modal-90w"
					aria-labelledby="example-custom-modal-styling-title"
				>
					<Modal.Body>
						<div className="m-auto text-center">
							<h6 className="mb-3 text-dark">Do you want to change this status?</h6>
						</div>
						{this.state.statuserrorMsg ? <div className="alert alert-danger my-3 text-center col-12" role="alert">{this.state.statuserrorMsg}</div> : null}
						<div className="m-auto text-center">
							<button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.handleHide()}>Return</button>
							{this.state.statuserrorMsg == null ? <button className="btn btn-danger" onClick={() => this.handleStatus(this.state.bankId, this.state.status)}>Confirm</button> : null}
						</div>

					</Modal.Body>
				</Modal>

				{/*====== Status change confirmation popup  ===== */}
				<Modal
					show={this.state.statusConfirMmsg}
					onHide={this.handleStatusChangedClose}
					className="payOptionPop"
				>
					<Modal.Body className="text-center">
						<Row>
							<Col md={12} className="text-center">
								<Image src={SuccessIco} />
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<h5>{this.state.successMessage}</h5>
							</Col>
						</Row>
						<Button
							onClick={this.handleStatusChangedClose}
							className="but-gray mt-3"
						>
							Return
                        </Button>
					</Modal.Body>
				</Modal>

				{/*========================= Modal for Delete Gem =====================*/}
				<Modal
					show={this.state.deleteGem}
					onHide={this.handleHide}
					dialogClassName="modal-90w"
					aria-labelledby="example-custom-modal-styling-title"
				>
					<Modal.Body>
						<div className="m-auto text-center">
							<h6 className="mb-3 text-dark">Do you want to delete this Gem?</h6>
						</div>
						{this.state.deleteerrorMsg ? <div className="alert alert-danger my-3 text-center col-12" role="alert">{this.state.deleteerrorMsg}</div> : null}
						<div className="m-auto text-center">
							<button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.handleDeleteHide()}>Return</button>
							{this.state.deleteerrorMsg == null ? <button className="btn btn-danger" onClick={() => this.handleDelete(this.state.id)}>Confirm</button> : null}
						</div>

					</Modal.Body>
				</Modal>

				{/*====== Delete Gem confirmation popup  ===== */}
				<Modal
					show={this.state.deleteConfirMmsg}
					onHide={this.handleDeleteChangedClose}
					className="payOptionPop"
				>
					<Modal.Body className="text-center">
						<Row>
							<Col md={12} className="text-center">
								<Image src={SuccessIco} />
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<h5>{this.state.successMessage}</h5>
							</Col>
						</Row>
						<Button
							onClick={this.handleDeleteChangedClose}
							className="but-gray mt-3"
						>
							Return
                        </Button>
					</Modal.Body>

				</Modal>


			</div>
		);
	}

}

const mapStateToProps = state => {
	return {
		globalState: state,
		facility: state.auth.facility
	};
};

News.propTypes = {
	globalState: PropTypes.object,
	facility: PropTypes.any
};

export default connect(mapStateToProps, null)(News);