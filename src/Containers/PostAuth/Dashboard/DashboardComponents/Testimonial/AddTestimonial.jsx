import { Field, Form, Formik } from 'formik';
import React, { Component } from 'react';
import {
    Button,
    Col,
    FormControl,
    FormGroup,
    Image,
    Modal,
    Row
} from 'react-bootstrap';
//import InputMask from 'react-input-mask';
import * as Yup from 'yup';
import axios from '../../../../../shared/eaxios';
import SuccessIco from '../../../../../assets/success-ico.png';
//import './../../customer/add-customer/AddCustomer.scss';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import PropTypes from 'prop-types';
import * as AppConst from './../../../../../common/constants';

//RichTextToolBar
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';

const initialValues = {
    name: '',
    star: '',
    description: '',
    status: '',
    selectedFile: null
};

const addCustomerSchema = Yup.object().shape({
    name: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter name')
        .max(40, 'maximum characters length 40'),
    star: Yup.string()
        .required('Please select star')
        .trim('Please remove whitespace')
        .strict(),
    description: Yup.string()
        .trim('Please remove whitespace')
        .strict(),
    status: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please select status')
});

class AddTestimonial extends Component {
    state = {
        showConfirMmsg: false,
        errorMessge: null,
        addVendorLoader: false,
        addErrorMessge: null,
        descriptionContent: EditorState.createEmpty(),
    };



    onEditorStateChangeDescription = (descriptionContent) => {
        this.setState({
            descriptionContent,
        });
    };

    handleChange = (e, field) => {
        this.setState({
            [field]: e.target.value
        });
    };

    handleConfirmReviewClose = () => {
        this.setState({ showConfirMmsg: false });
    };

    handleConfirmReviewShow = () => {
        this.setState({ showConfirMmsg: true });
    };

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (err) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handleSubmit = (values, { resetForm }) => {
        this.setState({
            addVendorLoader: true,
        });
        console.log('values 1 ', values, this.state.states);
        const formData = new window.FormData();
        for (const key in values) {
            if (values.hasOwnProperty(key)) {
                formData.append(key, values[key]);
            }
        }
        let newValue = {
            name: values.name,
            star: values.star,
            description: draftToHtml(convertToRaw(this.state.descriptionContent.getCurrentContent())).trim(),
            status: values.status
        };
        console.log('Final values ', newValue);
        // const config = {
        //     headers: {
        //         //sessionKey: localStorage.getItem("sessionKey"),
        //         'Content-Type': 'application/json'
        //     }
        // };

        formData.append(
            'image',
            this.state.selectedFile,
            //this.state.selectedFile.name 
        );
        formData.append('name', newValue.name);
        formData.append('star', newValue.star);
        formData.append('description', newValue.description);
        formData.append('status', newValue.status);

        axios
            .post(AppConst.APIURL + '/api/addTestimonial', formData)
            .then(res => {
                console.log(res);
                console.log('Vendor data', res.data);
                resetForm({});
                //this.setState({ showConfirMmsg: true });
                this.props.handelAddModalClose();
                this.props.handleAddConfirMmsg();
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    addVendorLoader: false,
                    addErrorMessge: errorMsg,
                });
                setTimeout(() => {
                    this.setState({ deleteErrorMessge: null });
                }, 5000);
            });
    };

    onFileChange = event => {
        // Update the state 
        this.setState({ selectedFile: event.target.files[0] });
    };

    render() {
        return (
            <div className="addcustomerSec">
                <div className="boxBg p-35">
                    {this.state.addErrorMessge ? (
                        <div className="alert alert-danger" role="alert">
                            {this.state.addErrorMessge}
                        </div>
                    ) : null}
                    <Row>
                        <Col sm={12}>
                            {this.state.addVendorLoader ? <LoadingSpinner /> : null}
                        </Col>
                    </Row>
                    <Row className="show-grid">
                        <Col xs={12} className="brd-right">
                            <Formik
                                initialValues={initialValues}
                                validationSchema={addCustomerSchema}
                                onSubmit={this.handleSubmit}
                            >
                                {({
                                    values,
                                    errors,
                                    touched,
                                    //isSubmitting,
                                    //handleChange,
                                    //handleBlur,
                                    //setFieldValue
                                }) => {
                                    return (
                                        <Form>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Name <span className="required">*</span></label>
                                                        <Field
                                                            name="name"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter name"
                                                            value={values.name || ''}
                                                        />
                                                        {errors.name && touched.name ? (
                                                            <span className="errorMsg ml-3">{errors.name}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Description
                                                        </label>
                                                        <div className='editor'>
                                                            <Editor
                                                                editorState={this.state.descriptionContent}
                                                                wrapperClassName="demo-wrapper"
                                                                editorClassName="form-control rounded-0 mt-n2"
                                                                onEditorStateChange={this.onEditorStateChangeDescription}
                                                                placeholder="Enter description"
                                                                toolbarClassName="text-body"
                                                                handlePastedText={() => false}
                                                                toolbar={{
                                                                    options: ['inline', 'blockType', 'fontSize', 'list', 'textAlign', 'link', 'embedded', 'emoji', 'image', 'remove', 'history'],
                                                                    image: { alt: { present: true, mandatory: false } }
                                                                }}
                                                            />
                                                        </div>

                                                        {errors.description && touched.description ? (
                                                            <span className="errorMsg ml-3">{errors.description}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <span>Star <span className="required">*</span></span>
                                                        <Field
                                                            name="star"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >
                                                            <option value="" defaultValue="5">
                                                                Select Star
                                                            </option>
                                                            <option value="1" key="1">
                                                                1
                                                            </option>
                                                            <option value="2" key="2">
                                                                2
                                                            </option>
                                                            <option value="3" key="3">
                                                                3
                                                            </option>
                                                            <option value="4" key="4">
                                                                4
                                                            </option>
                                                            <option value="5" key="5">
                                                                5
                                                            </option>
                                                        </Field>
                                                        {errors.status && touched.status ? (
                                                            <span className="errorMsg ml-3">{errors.star}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <span>Status <span className="required">*</span></span>
                                                        <Field
                                                            name="status"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >
                                                            <option value="" defaultValue="">
                                                                Select Status
                                                            </option>
                                                            <option value="1" key="1">
                                                                Active
                                                            </option>
                                                            <option value="0" key="0">
                                                                Inactive
                                                            </option>
                                                        </Field>
                                                        {errors.status && touched.status ? (
                                                            <span className="errorMsg ml-3">{errors.status}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <span>Image</span>
                                                        <input id="image" name="image" type="file" className="form-control" onChange={this.onFileChange} />
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    &nbsp;
                                                </Col>
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12} className="text-center">
                                                    <Button className="blue-btn" type="submit">
                                                        Save
                                                    </Button>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={12}>
                                                    <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                                </Col>
                                            </Row>
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Col>
                    </Row>
                </div>

                {/*======  confirmation popup  ===== */}
                <Modal
                    show={this.state.showConfirMmsg}
                    onHide={this.handleConfirmReviewClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Vendor has been successfully added</h5>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handleConfirmReviewClose}
                            className="but-gray"
                        >
                            Done
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

AddTestimonial.propTypes = {
    handleAddConfirMmsg: PropTypes.func,
    businessId: PropTypes.number,
    handelAddModalClose: PropTypes.func,
};

export default AddTestimonial;