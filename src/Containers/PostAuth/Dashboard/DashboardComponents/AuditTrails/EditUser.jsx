/* eslint-disable no-useless-escape */
import { Field, Form, Formik } from 'formik';
import React, { Component, Fragment } from 'react';
import {
    Button,
    Col,
    FormControl,
    FormGroup,
    Row,
    //Image
} from 'react-bootstrap';

import * as Yup from 'yup';

import axios from '../../../../../shared/eaxios';
import * as AppConst from './../../../../../common/constants';
//import Dropzone from 'react-dropzone';
//import UploadIcon from './../../../../../assets/uploading.png';
//import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import PropTypes from 'prop-types';

let modifiedObject = {};
//Formik and Yup validation
const editVendorSchema = Yup.object().shape({

    first_name: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter first name').nullable(),
    last_name: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter last name').nullable(),
    address: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter address').nullable(),
    gender: Yup.string()
        .strict()
        .required('Please select gender').nullable()


});

class EditUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            uploadFile: null,
            showVendor: false,
            errorMessge: null,
            editVendorEnable: false,
            disabled: false,
            editVendorLoader: false,
            editErrorMessge: false
        };
    }

    static getDerivedStateFromProps(props, state) {
        console.log(' getDerivedStateFromProps : ', props, state);
        if (!state.vendorData) {
            return {
                ...props
            };
        }
    }

    handleEditVendorEnable = () => {
        this.setState({
            editVendorEnable: true,
            disabled: true
        });
    }

    handleEditVendorDisable = () => {
        this.setState({
            editVendorEnable: false,
            disabled: false
        });
        this.props.onReload(this.state.vendorData);
    }

    handleCloseVendor = () => {
        this.props.onClick();
    };


    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }

        return errorMessge;
    }



    //after editing form submit
    handleSubmit = values => {
        this.setState({
            editVendorLoader: true,
        });
        //console.log('handleSubmit', values);

        let newValue = {
            first_name: values.first_name,
            last_name: values.last_name,
            address: values.address,
            gender: values.gender,
            //profile_pic:this.state.uploadFile
        };
        console.log('new Value submitted 2', newValue);
        // const config = {
        //     headers: {
        //         'Content-Type': 'multipart/form-data',
        //     }
        // };

        const vendorData = {};
        Object.assign(vendorData, newValue);
        //console.log('vendorData', vendorData);
        //let formData = new window.FormData();
        //let queryString = values.id + '?first_name=' + values.first_name + '&last_name=' + values.last_name + '&address=' + values.address + '&gender=' + values.gender;
        //formData.append('profile_pic', this.state.uploadFile);
        // formData.append('first_name', values.first_name);
        // formData.append('last_name', values.last_name);
        // formData.append('address', values.address);
        // formData.append('gender', values.gender);
        //console.log('--------------------');

        axios
            .put(AppConst.APIURL + `/api/updateSelectedUser/${values.user_id}`, newValue)
            .then(res => {
                console.log('Vendor details get response', res);
                console.log('Vendor details get data', res.data);
                this.setState({
                    editVendorLoader: false,
                    editVendorEnable: false,
                    disabled: false,
                    vendorData
                }, () => {
                    this.props.handleEditConfirMmsg();
                });
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    editVendorLoader: false,
                    editErrorMessge: errorMsg,
                });

                setTimeout(() => {
                    this.setState({ deleteErrorMessge: null });
                }, 5000);

            });
    };


    handelMod = (getObject) => {
        console.log('calling..', getObject);
        if (getObject) {
            getObject.first_name && (getObject.first_name = getObject.first_name.trim());
            getObject.last_name && (getObject.last_name = getObject.last_name.trim());
            getObject.address && (getObject.address = getObject.address.trim());
        }
        return getObject;
    }


    onDrop = async files => {

        this.setState({
            files,
            dropzoneActive: false,
            isDrop: true
        });

        let reader = '';
        if (files.length > 0) {
            console.log('image details', files[0]);


            // FILE SIZE RESTRICTION
            if (files[0]['size'] > 600000) {
                this.setState({
                    uploadErrorMessage: 'File size should be less than 600KB',
                    isDrop: false,
                });
                setTimeout(() => {
                    this.setState({ uploadErrorMessage: null });
                }, 5000);
            }
            else if ((files[0]['type'] !== 'image/png') && (files[0]['type'] !== 'image/jpeg')) {
                this.setState({
                    uploadErrorMessage: 'Please upload jpeg/png file',
                    isDrop: false,
                });
                setTimeout(() => {
                    this.setState({ uploadErrorMessage: null });
                }, 5000);
            }


            else {

                reader = new window.FileReader();
                reader.readAsDataURL(files[0]);

                reader.onload = () => {
                    let urlString = reader.result;

                    this.setState({
                        uploadLoader: false,
                        imageURL: urlString,
                        uploadFile: files[0]
                    });
                };


            }

        }

    };

    render() {
        const initialValues = { ...this.props };
        console.log('initial value', initialValues);

        modifiedObject = this.handelMod(initialValues.user_detail);
        console.log('MOD value', modifiedObject);

        const {

            disabled
        } = this.state;


        return (
            <Formik
                initialValues={modifiedObject}
                validationSchema={editVendorSchema}
                onSubmit={this.handleSubmit}
                enableReinitialize={true}
            >
                {({
                    values,
                    errors,
                    touched,
                    //isSubmitting,
                    //handleChange,
                    //setFieldValue,
                    //handleBlur
                }) => {
                    return (
                        <Form className={disabled === false ? ('hideRequired') : null}>

                            <Row className="show-grid">

                                <Col xs={12} md={12}>
                                    <FormGroup controlId="formControlsTextarea">
                                        <label><h3>Email <span className="required">*</span></h3></label>
                                        <Field
                                            name="Email"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={initialValues.email || ''}
                                            disabled="disabled"
                                        />
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>

                                <Col xs={12} md={12}>
                                    <FormGroup controlId="formControlsTextarea">
                                        <label><h3>First Name <span className="required">*</span></h3></label>
                                        <Field
                                            name="first_name"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.first_name || ''}
                                            disabled={disabled === false ? 'disabled' : ''}
                                        />
                                        {errors.first_name && touched.first_name ? (
                                            <span className="errorMsg ml-3">{errors.first_name}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                                <Col xs={12} md={12}>
                                    <FormGroup controlId="formControlsTextarea">
                                        <label><h3>Last Name <span className="required">*</span></h3></label>
                                        <Field
                                            name="last_name"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.last_name || ''}
                                            disabled={disabled === false ? 'disabled' : ''}
                                        />
                                        {errors.last_name && touched.last_name ? (
                                            <span className="errorMsg ml-3">{errors.last_name}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                                <Col xs={12} md={12}>
                                    <FormGroup controlId="formControlsTextarea">
                                        <label><h3>Address <span className="required">*</span></h3></label>
                                        <Field
                                            name="address"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.address || ''}
                                            disabled={disabled === false ? 'disabled' : ''}
                                        />
                                        {errors.address && touched.address ? (
                                            <span className="errorMsg ml-3">{errors.address}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>

                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formControlsTextarea">
                                        <label><h3>Gender <span className="required">*</span></h3></label>
                                        <Field
                                            name="gender"
                                            component="select"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="select"
                                            value={values.gender || ''}
                                            disabled={disabled === false ? 'disabled' : ''}

                                        >
                                            <option value="">
                                                Select Gender
                                            </option>
                                            <option value="Male" key="1">
                                                Male
                                            </option>
                                            <option value="Female" key="0">
                                                Female
                                            </option>
                                        </Field>
                                        {errors.gender && touched.gender ? (
                                            <span className="errorMsg ml-3">{errors.gender}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                                {/* <Col md={6} className="text-center">
                                    <div className="form-group">
                                        <Dropzone                                          
                                            onDrop={this.onDrop}
                                        >
                                            {({ getRootProps, getInputProps }) => (
                                                <section>
                                                    <div {...getRootProps()}>
                                                        <input {...getInputProps()} />
                                                        <Fragment>
                                                            <div className="uploadWrap">
                                                                {this.state.uploadLoader ? (
                                                                    <LoadingSpinner />
                                                                ) : ((this.state.imageURL == null || this.state.imageURL == '') ?
                                                                    (<Image
                                                                        src={UploadIcon}
                                                                        alt="upload"
                                                                    />)
                                                                    :
                                                                    (<Image height={250} src={this.state.imageURL} />)
                                                                    )}


                                                                <p className="sm-txt-blue text-center">
                                                                    Please upload only jpg/png file
                                                                </p>
                                                            </div>
                                                        </Fragment>
                                                        <button type="button" className="btn btn-primary btn-position">
                                                            Upload logo
                                                        </button>
                                                    </div>
                                                </section>
                                            )}
                                        </Dropzone>
                                        {this.state.uploadErrorMessage ? <p className="text-danger"><span>*</span>{this.state.uploadErrorMessage}</p> : null}
                                    </div>
                                </Col> */}
                            </Row>

                            <Row className="show-grid">
                                <Col xs={12} md={12}>
                                    &nbsp;
                                </Col>
                            </Row>
                            <Row>&nbsp;</Row>
                            <Row className="show-grid text-center">
                                <Col xs={12} md={12}>

                                    <Fragment>
                                        {
                                            this.state.editVendorEnable !== true ? (
                                                <Fragment>
                                                    <Button className="blue-btn border-0" onClick={this.handleEditVendorEnable}>
                                                        Edit
                                                    </Button>
                                                </Fragment>
                                            ) : (
                                                    <Fragment>
                                                        <Button
                                                            onClick={this.props.handelviewEditModalClose}
                                                            className="but-gray border-0 mr-2"
                                                        >
                                                            Cancel
                                                    </Button>
                                                        <Button type="submit" className="blue-btn ml-2 border-0">
                                                            Save
                                                    </Button>
                                                    </Fragment>)
                                        }
                                    </Fragment>

                                </Col>
                            </Row>
                            {disabled === false ? null : (<Fragment>
                                <Row>
                                    <Col md={12}>
                                        <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                    </Col>
                                </Row>
                            </Fragment>)}
                        </Form>
                    );
                }}
            </Formik>
        );
    }
}

EditUser.propTypes = {
    globState: PropTypes.object,
    onClickAction: PropTypes.func,
    onReload: PropTypes.func,
    //onSuccess: PropTypes.func,
    onClick: PropTypes.func,
    handelviewEditModalClose: PropTypes.func,
    handleEditConfirMmsg: PropTypes.func,

};

export default EditUser;
