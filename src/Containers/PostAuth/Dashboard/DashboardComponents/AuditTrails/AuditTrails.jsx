import React, { Component, Fragment } from 'react';
import axios from '../../../../../shared/eaxios';
import { Table, Row, Col, Image, FormGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import refreshIcon from './../../../../../assets/refreshIcon.png';
import Pagination from 'react-js-pagination';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import * as AppConst from './../../../../../common/constants';
import Moment from 'moment';
import DatePicker from 'react-datepicker';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

class AuditTrails extends Component {
    state = {
        dataLists: [],
        activePage: 1,
        totalCount: 0,
        itemPerPage: 10,
        loading: false,
        errorMessge: null,
        BankSearchKey: '',
        statusChange: false,
        userId: null,
        status: null,
        sort: 1,
        field: 'bankname',
        statusConfirMmsg: false,
        statuserrorMsg: null,
        sortingActiveID: 1,
        AddEditModal: false,
        Bankname: '',
        uploadLoader: false,
        imageURL: null,
        uploadErrorMessage: null,
        uploadFile: null,
        bankNameError: null,
        successMessage: null,
        BankAddloader: false,
        selectedUserId: null,
        modalState: null,
        addEditBankError: false,
        userList: [],
        userid: '',
        startDate: '',
        endDate: '',
        errorMessageForSearch: '',
        permission: []
    };

    keyPress = (e) => {
        this.setState({
            BankSearchKey: e.target.value.trim(),
        });
    }

    handelSearch = () => {
        this.fetchUserList();
    }

    resetSearch = () => {
        this.setState({
            startDate: '', endDate: '', userid: ''
        }, () => {
            this.fetchUserList();
        });
    }

    sortingActive = (id) => {
        this.setState({
            sortingActiveID: id
        }, () => { console.log('this.state.sortingActiveID', this.state.sortingActiveID); });
    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Unknown error!';
        }
        return errorMessge;
    }

    handleHide = () => {
        this.setState({
            statusChange: false,
            statuserrorMsg: null
        });
    }

    handleHideInfluencer = () => {
        this.setState({
            setInfluencer: false,
            statuserrorMsg: null
        });
    }

    handelStatusModal = (id, status) => {
        this.setState({
            statusChange: true,
            userId: id,
            status: status
        });
    }

    handelInfluencerModal = (id, status) => {
        this.setState({
            setInfluencer: true,
            userId: id,
            status: status
        });
    }

    onChangeBankName = (e) => {

        let str = e.target.value.trim();
        this.setState({
            bankNameError: null,
            Bankname: str
        });
    }

    _isMounted = false;

    fetchUserList = (
        sort = this.state.sort,
        field = this.state.field,
    ) => {

        this.setState({
            loading: true,
            sort: sort,
            field: field
        }, () => {
            axios
                .get(
                    AppConst.APIURL + `/api/getLogList?&pageSize=${this.state.itemPerPage}&page=${this.state.sort}&userid=${this.state.userid}&startDate=${this.state.startDate && Moment(this.state.startDate).format('yyyy-MM-DD')}&endDate=${this.state.endDate && Moment(this.state.endDate).format('yyyy-MM-DD')}`
                )
                .then(res => {
                    const dataLists = res.data.data.data;
                    const totalCount = res.data.data.total;
                    if (this._isMounted && dataLists) {
                        this.setState({
                            dataLists: dataLists,
                            totalCount: totalCount,
                            loading: false
                        }, () => { });
                    }

                })
                .catch(e => {
                    let errorMsg = this.displayError(e);
                    this.setState({
                        errorMessge: errorMsg,
                        loading: false
                    });
                    setTimeout(() => {
                        this.setState({ errorMessge: null });
                    }, 5000);

                });

        });

    };

    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber });
        this.fetchUserList(pageNumber > 0 ? pageNumber : 0, this.state.field);
        //console.log('pageNumber',pageNumber);
        //console.log('sort',this.state.sort);
    };

    handleChangeItemPerPage = (e) => {
        this.setState({ sort: 1 });
        this.setState({ itemPerPage: e.target.value },
            () => {
                this.fetchUserList();
            });
    }

    resetPagination = () => {
        this.setState({ activePage: 1 });
    }

    componentDidMount() {
        this._isMounted = true;
        axios.get(AppConst.APIURL + '/api/auditUserList')
            .then(res => {
                this.setState({ userList: res.data.data });
            });

        const facility = this.props.facility;
        const permission = [];
        facility.map(data => {
            data.name === 'audittrail' && data.permission.length !== 0 && permission.push(...data.permission);
        });
        this.setState({ permission }, () => {
            permission[0].status === true &&
                this.fetchUserList();
        });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    handleStatus(id, status, col) {
        console.log(status);
        let updateStatus = {
            'active_status': status
        };

        let updateInflu = {
            'is_influencer': status
        };

        const dataArr = col === 'setInfluencer' ? updateInflu : updateStatus;

        const dataUrl = col === 'setInfluencer' ? `/api/influenceUpdateUser/${id}` : `/api/statusUpdateUser/${id}`;

        axios.put(AppConst.APIURL + dataUrl, dataArr)
            .then(res => {
                console.log('--------------res------', status);
                console.log('--------------res------', res);
                this.setState({
                    userId: null,
                    status: null,
                    successMessage: 'Status successfully changed',
                    statusConfirMmsg: true
                });
                this.handleHide();
                this.handleHideInfluencer();
                this.fetchUserList();
            }

            )
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    statuserrorMsg: errorMsg,
                    loading: false


                });
                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);
            });

    }

    handleStatusChangedClose = () => {
        this.setState({
            statusConfirMmsg: false,
            successMessage: null
        });
    }

    handelAddEditModalClose = () => {
        this.setState({
            AddEditModal: false,
            imageURL: null,
            bankNameError: null,
            modalState: null,
            Bankname: '',
            selectedUserId: null,
            uploadErrorMessage: null,
            uploadFile: null
        });
    }

    handelAddEditBankModal = (param, name, id, url) => {
        console.log('param, name, id, url', param, name, id, url);
        this.setState({
            AddEditModal: true,
            modalState: param
        });
        if (param === 'edit') {
            this.setState({
                Bankname: name,
                selectedUserId: id,
                imageURL: url
            });
        }

    }

    handelviewEditModal = (data) => {
        this.setState({
            viewEditModal: true,
            vendorData: data
        }, () => {
            console.log('this.state.viewEditData@@', this.state.vendorData);
        });
    }

    handelviewEditModalClose = () => {
        this.setState({
            viewEditModal: false,
        });
    }

    handleEditConfirMmsg = () => {
        this.setState({
            viewEditModal: false,
            editConfirMmsg: true
        }, () => {
            this.fetchUserList();
        });
    }

    handleEditConfirMmsgClose = () => {

        this.setState({
            editConfirMmsg: false
        });
    }

    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value, errorMessageForSearch: '' });
    }

    handleStartDate = (date) => {
        if (date > this.state.endDate) {
            this.setState({ startDate: date, endDate: '', errorMessageForSearch: '' });
        } else {
            this.setState({ startDate: date, errorMessageForSearch: '' });
        }
    }

    handleEndDate = date => {
        this.setState({ endDate: date, errorMessageForSearch: '' });
    }

    handleSearchSubmit = (e) => {
        e.preventDefault();
        if (this.state.userid === '' && this.state.startDate === '' && this.state.endDate === '') {
            this.setState({ errorMessageForSearch: 'Any one field is required to search the Activities Logged.' });
        } else {
            this.fetchUserList();
        }
    }

    render() {
        return (
            <div className="dashboardInner businessOuter pt-3">
                {this.state.permission[0] && this.state.permission[0].status === true ?
                    <Fragment>
                        <form onSubmit={this.handleSearchSubmit}>
                            <div className="searchBox">
                                <div className="row">
                                    <div className="col-12 col-md-3">
                                        <select className="form-control" name="userid" value={this.state.userid} onChange={this.handleChange}>
                                            <option key="" value="">Select User</option>
                                            {this.state.userList.map(data => {
                                                return <option key={data.id} value={data.id}>{data.user_detail.first_name + ' ' + data.user_detail.last_name}</option>;
                                            })}
                                        </select>
                                    </div>
                                    <div className="col-12 col-md-3">
                                        <FormGroup controlId="formControlsTextarea">
                                            <DatePicker
                                                autoComplete='off'
                                                name='startDate'
                                                selected={this.state.startDate}
                                                onChange={this.handleStartDate}
                                                className='form-control'
                                                maxDate={new Date()}
                                                required
                                                dateFormat="dd-MM-yyyy"
                                                showMonthDropdown
                                                showYearDropdown
                                                placeholderText="Start date"
                                            />
                                        </FormGroup>
                                    </div>
                                    <div className="col-12 col-md-3">
                                        <FormGroup controlId="formControlsTextarea">
                                            <DatePicker
                                                autoComplete='off'
                                                name='endDate'
                                                selected={this.state.endDate}
                                                onChange={this.handleEndDate}
                                                className='form-control'
                                                maxDate={new Date()}
                                                minDate={this.state.startDate}
                                                required
                                                dateFormat="dd-MM-yyyy"
                                                showMonthDropdown
                                                showYearDropdown
                                                placeholderText="End date"
                                            />
                                        </FormGroup>
                                    </div>
                                    <div className="col-12 col-md-3">
                                        <input type="submit" value="Search" className="btn btn-primary" />
                                        <Link to="#" className='ml-3 mt-2'>
                                            <Image src={refreshIcon} onClick={() => this.resetSearch()} />
                                        </Link>
                                    </div>
                                </div>
                                {this.state.errorMessageForSearch &&
                                    <div className="text-success text-center mt-3">
                                        {this.state.errorMessageForSearch}
                                    </div>}
                            </div>
                        </form>

                        {this.state.errorMessge
                            ?
                            <div className="alert alert-danger col-12" role="alert">
                                ERROR : {this.state.errorMessge}
                            </div>
                            :
                            null
                        }


                        <div className="boxBg">
                            <Table responsive hover>
                                <thead className="theaderBg">
                                    <tr>
                                        <th>Page</th>
                                        <th>Activity</th>
                                        <th>Action On</th>
                                        <th>User</th>
                                        <th>IP Address</th>
                                        <th>Activity Date</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    {this.state.loading ? (<tr>
                                        <td colSpan={12}>
                                            <LoadingSpinner />
                                        </td>
                                    </tr>) :
                                        this.state.dataLists.length > 0 ? (
                                            this.state.dataLists.map((userList, index) => (
                                                <tr key={index}>

                                                    <td>{userList.page}</td>
                                                    <td>{userList.pageDesc}</td>
                                                    <td>{
                                                        userList.action_on
                                                            ? userList.action_on
                                                            : userList.email
                                                    }</td>
                                                    <td>{userList.first_name} {userList.last_name}</td>
                                                    <td>
                                                        {
                                                            userList.ip_address
                                                                ? userList.ip_address
                                                                : 'N/A'
                                                        }
                                                    </td>
                                                    <td>
                                                        {Moment(userList.created_at).format('MMMM Do, YYYY')}
                                                        {/* <Moment format="MMMM Do, YYYY">{userList.created_at}</Moment> */}
                                                    </td>
                                                </tr>
                                            ))
                                        )
                                            :
                                            this.state.errorMessge ? <tr>
                                                <td colSpan={12}>
                                                    <p className="text-center">{this.state.errorMessge}</p>
                                                </td>
                                            </tr> : (
                                                    <tr>
                                                        <td colSpan={12}>
                                                            <p className="text-center">No records found</p>
                                                        </td>
                                                    </tr>
                                                )
                                    }

                                </tbody>
                            </Table>

                        </div>
                        {this.state.totalCount ? (
                            <Row>
                                <Col md={4} className="d-flex flex-row mt-20">
                                    <span className="mr-2 mt-2 font-weight-500">Items per page</span>
                                    <select
                                        id={this.state.itemPerPage}
                                        className="form-control truncatefloat-left w-90"
                                        onChange={this.handleChangeItemPerPage}
                                        value={this.state.itemPerPage}>
                                        <option value='10'>10</option>
                                        <option value='50'>50</option>
                                        <option value='100'>100</option>
                                        <option value='200'>200</option>
                                        <option value='300'>300</option>
                                        <option value='500'>500</option>
                                    </select>
                                </Col>
                                <Col md={8}>
                                    <div className="paginationOuter text-right">
                                        <Pagination
                                            activePage={this.state.activePage}
                                            itemsCountPerPage={this.state.itemPerPage}
                                            totalItemsCount={this.state.totalCount}
                                            onChange={this.handlePageChange}
                                        />
                                    </div>
                                </Col>
                            </Row>
                        ) : null}
                    </Fragment>
                    : <h5 className="text-center p-3">You do not have any permission to view this content.</h5>}

            </div>



        );
    }

}

const mapStateToProps = state => {
    return {
        facility: state.auth.facility
    };
};

AuditTrails.propTypes = {
    facility: PropTypes.any
};

export default connect(mapStateToProps, null)(AuditTrails);

