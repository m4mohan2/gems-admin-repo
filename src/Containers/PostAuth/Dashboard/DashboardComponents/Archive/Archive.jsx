import React,{Component} from 'react';
import axios from '../../../../../shared/eaxios';
import {
    Table,
    //Dropdown,
    Row,
    //Modal,
    Col,
    //Image,
    //Button
} from 'react-bootstrap';
import { FaCaretDown } from 'react-icons/fa';
import { FaCaretUp } from 'react-icons/fa';
import Pagination from 'react-js-pagination';
//import { Link } from 'react-router-dom';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
//import { FiMenu } from 'react-icons/fi';


class Archive extends Component {

    state = {
        businessLists: [],
        editVendor: [],
        showBusinessModal: false,
        successMessage: null,
        activePage: 1,
        totalCount: 0,
        itemPerPage: 20,
        showConfirMmsg: false,
        showAddBusinessConfirMmsg: false,
        DeleteBusinessModal: false,
        businessArchiveConfirMmsg: false,
        errorMessge: null,
        errMsg: null,
        showDeleteMmsg: false,
        showDeleteConfirm: false,
        loading: true,
        editBusinessLoading: false,
        editBusinessUI: false,
        selectedBusinessName: '',
        businessFilter: '',
        sortingActiveID: 1,
        key: 'bi',
        editBusiness: {},
        dropzoneActive: false,
        businessDeleteLoader: false,
        archiveConfirm: true,
        selectedBusiness: 'all'
    };



    _isMounted = false;

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Unknown error!';
        }

        this.setState({
            errorMessge: errorMessge
        });

        setTimeout(() => {
            this.setState({ errorMessge: null });
        }, 5000);
    }
    
    sortingActive = (id) => {
        this.setState({
            sortingActiveID: id
        }, () => { console.log('this.state.sortingActiveID', this.state.sortingActiveID); });
    }

    fetchArchiveList = (    
        since = 0, 
        //searchKey = '', 
        //filter = '', 
        sort = '', 
        field = '', 
        //custFilter = ''
    ) => {

        this.setState({
            loading: true
        }, () => {

            axios
                .get(

                    // `business/list?since=${since}&limit=${this.state.itemPerPage}&id=${searchKey}&key=${filter}&direction=${sort}&prop=${field}&custFilter=${custFilter}`
                    `business/inactive/list?since=${since}&limit=${this.state.itemPerPage}&isCustomer=true&prop=${field}&direction=${sort}`
                )
                .then(res => {
                    console.log('res.data-----------------', res.data);
                    const businessLists = res.data.entries;
                    const totalCount = res.data.total;
                    if (this._isMounted && businessLists) {
                        this.setState({
                            businessLists: businessLists,
                            totalCount: totalCount,
                            loading: false
                        },()=>{
                            console.log('business list-----------------------', this.state.businessLists);
                        });
                    }

                    
                })
                .catch(e => {
                    let errorMsg = this.displayError(e);
                    this.setState({
                        errorMessge: errorMsg,
                        loading: false
                    });

                });

        });

    };

    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber });
        this.fetchArchiveList(pageNumber > 0 ? pageNumber - 1 : 0);
    };

    resetPagination = () => {
        this.setState({ activePage: 1 });
    }

    rePagination = () => {
        this.setState({ activePage: 0 });

    }


    componentDidMount(){
        this._isMounted = true;
        this.fetchArchiveList();
    }
    
    componentWillUnmount() {
        this._isMounted = false;
    }




    render(){
        return (
            <div className="dashboardInner businessOuter">
                {this.state.errorMessge
                    ?
                    <div className="alert alert-danger col-12" role="alert">
                        ERROR : {this.state.errorMessge}
                    </div>
                    :
                    null
                }
                
                <div className="boxBg">

                    <Table responsive hover>
                        <thead className="theaderBg">
                            <tr>
                                <th>
                                    <span className="float-left pr-2">Business Name </span>
                                    <span className="d-flex flex-column-reverse sortingFontSize">
                                        <button className="custom-btn-focus">
                                            <FaCaretDown
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 2 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.fetchArchiveList(0, false, 'business');
                                                    this.sortingActive(2);
                                                }} />
                                        </button>
                                        <button className="custom-btn-focus">
                                            <FaCaretUp
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 1 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.fetchArchiveList(0, true, 'business');
                                                    this.sortingActive(1);
                                                }} />
                                        </button>
                                    </span>
                                </th>
                                <th>
                                    <span className="float-left pr-2">Business Tax ID</span>
                                    <span className="d-flex flex-column-reverse sortingFontSize">
                                        <button className="custom-btn-focus">
                                            <FaCaretDown
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 4 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.fetchArchiveList(0,  false, 'businessNumber');
                                                    this.sortingActive(4);
                                                }} />
                                        </button>
                                        <button className="custom-btn-focus">
                                            <FaCaretUp
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 3 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.fetchArchiveList(0, true, 'businessNumber');
                                                    this.sortingActive(3);
                                                }} />
                                        </button>
                                    </span>
                                </th>
                                <th>

                                    <span className="float-left pr-2">Phone No</span>
                                    <span className="d-flex flex-column-reverse sortingFontSize">
                                        <button className="custom-btn-focus">
                                            <FaCaretDown
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 6 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.fetchArchiveList(0, false, 'businessPhone');
                                                    this.sortingActive(6);
                                                }} />
                                        </button>
                                        <button className="custom-btn-focus">
                                            <FaCaretUp
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 5 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.fetchArchiveList(0, true, 'businessPhone');
                                                    this.sortingActive(5);
                                                }} />
                                        </button>
                                    </span>
                                </th>
                                <th>
                                    <span className="float-left pr-2">Email ID</span>
                                    <span className="d-flex flex-column-reverse sortingFontSize">
                                        <button className="custom-btn-focus">
                                            <FaCaretDown
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 8 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.fetchArchiveList(0, false, 'businessEmail');
                                                    this.sortingActive(8);
                                                }} />
                                        </button>
                                        <button className="custom-btn-focus">
                                            <FaCaretUp
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 7 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.fetchArchiveList(0, true, 'businessEmail');
                                                    this.sortingActive(7);
                                                }} />
                                        </button>
                                    </span>
                                </th>
                                {/* <th>
                                    Vendor Count

                                </th>
                                <th>Invoice Count</th> */}
                                <th>Is Customer</th>
                            </tr>
                        </thead>
                        <tbody>

                            {this.state.loading ? (<tr>
                                <td colSpan={12}>
                                    <LoadingSpinner />
                                </td>
                            </tr>) :
                                this.state.businessLists.length > 0 ? (
                                    this.state.businessLists.map(businessList => (
                                        <tr key={businessList.id} >
                                            
                                            <td>{businessList.businessName}</td>
                                            <td>{businessList.businessNumber}</td>
                                            <td>{businessList.businessPhone}</td>
                                            <td>{businessList.businessEmail}</td>
                                            {/* <td align="center" onClick={() => this.handelState(businessList.id)}><Link className="btn btn-primary text-white" to="vendor">{businessList.vendorCount}</Link></td>
                                            <td align="center"><Link className="btn btn-secondary text-white" to="#">{businessList.invoiceCount}</Link></td> */}
                                            <td align="center">{businessList.isCustomer === true ? <span>Yes</span> : <span>No</span>}</td>
                                        </tr>
                                    ))
                                )
                                    :
                                    this.state.errorMessge ? <tr>
                                        <td colSpan={6}>
                                            <p className="text-center">{this.state.errorMessge}</p>
                                        </td>
                                    </tr> : (
                                        <tr>
                                            <td colSpan={6}>
                                                <p className="text-center">No records found</p>
                                            </td>
                                        </tr>
                                    )
                            }

                        </tbody>
                    </Table>

                </div>
                {this.state.totalCount ? (
                    <Row>
                        <Col md={12}>
                            <div className="paginationOuter text-right">
                                <Pagination
                                    activePage={this.state.activePage}
                                    itemsCountPerPage={this.state.itemPerPage}
                                    totalItemsCount={this.state.totalCount}
                                    onChange={this.handlePageChange}
                                />
                            </div>
                        </Col>
                    </Row>
                ) : null}




            </div>
        );
    }

}

export default Archive;
