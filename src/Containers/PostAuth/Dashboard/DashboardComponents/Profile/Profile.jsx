import { Field, Form, Formik } from 'formik';
import React, { Component, Fragment } from 'react';
import { Button, Col, FormControl, FormGroup, Row } from 'react-bootstrap';
//import InputMask from 'react-input-mask';
import * as Yup from 'yup';
//import { UserBusiness } from "../../../services/userBusiness.service";
//import SuccessIco from './../../../../../assets/success-ico.png';
import axios from './../../../../../shared/eaxios';
import LoadingSpinner from './../../../../../Components/LoadingSpinner/LoadingSpinner';
import { handelUser } from './../../../../../redux/actions/username';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as AppConst from './../../../../../common/constants';
import './Profile.scss';
//import { indexOf } from 'lodash';
import Moment from 'moment';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

const yourDetailsSchema = Yup.object().shape({
    first_name: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter First Name').nullable(),
    last_name: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter Last Name').nullable(),
    // email: Yup
    //     .string()
    //     .trim('Please remove whitespace')
    //     .strict()
    //     .email('Email must be valid')
    //     .required('Please enter Email ID'),
    // phoneNumber: Yup
    //     .string()
    //     .trim('Please remove whitespace')
    //     .strict()
    //     .min(14, 'Phone number must be 10 digit'),
    gender: Yup
        .string()
        .trim('Please remove whitespace')
        .strict().nullable(),
    // dob: Yup
    //     .string()
    //     .trim('Please remove whitespace')
    //     .strict().nullable()
});

class Profile extends Component {
    state = {
        errorMessge: null,
        userDetails: '',
        showSuccessModal: false,
        userRole: [],
        editLoader: false,
        loaderDetails: false,
        disabled: false,
        selectedFile: null,
        result: '',
        countryList: [],
        stateList: [],
        cityList: [],
        countrySelected: '',
        stateSelected: '',
        dateOfBirth: ''
    };

    componentDidMount() {
        axios.get(AppConst.APIURL + '/api/countries')
            .then(response => {
                const countryList = response.data.data.map(data => {
                    return data;
                });
                this.setState({ countryList });
            });
    }

    handleSuccessClose = () => {
        this.setState({ showSuccessModal: false });
    };

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error;
        } catch (e) {
            errorMessge = 'Unknown error!';
        }
        this.setState({
            errorMessge: errorMessge,
            loaderDetails: false
        });
        setTimeout(() => {
            this.setState({ errorMessge: null });
        }, 5000);
    }

    handleSubmit = (values, { resetForm, setSubmitting }) => {
        this.setState({
            editLoader: true
        });
        const formData = new window.FormData();
        // for (const key in values) {
        //     if (values.hasOwnProperty(key)) {
        //         formData.append(key, values[key]);
        //     }
        // }
        formData.append('first_name', values.first_name);
        formData.append('last_name', values.last_name);
        formData.append('dob', Moment(this.state.dateOfBirth).format('YYYY-MM-DD'));
        //formData.append('dob', this.state.dateOfBirth);
        formData.append('gender', values.gender);
        formData.append('city', values.city);
        formData.append('state', this.state.stateSelected);
        formData.append('country', this.state.countrySelected);
        formData.append('biography', values.biography);
        formData.append('profile_pic', this.state.selectedFile);

        axios.post(AppConst.APIURL + '/api/updateUser', formData)
            .then(res => {
                setSubmitting(false);
                this.setState({
                    editLoader: false,
                    disabled: false,
                    editCouponEnable: false,
                    result: res
                });
                let user = {};
                user.firstName = res.data.user.first_name;
                user.lastName = res.data.user.last_name;
                this.props.handelUser(user);
                this.props.handleProfileConfirMmsg();
                resetForm({});
            }).catch(this.displayError);

        // if (values.password && !values.confirmPassword) {
        //     actions.setErrors({ confirmPassword: 'Please enter password again' });
        // } else {
        //     this.setState({
        //         editLoader: true
        //     });

        //     // let phno = updateArray.phoneNumber.substring(3).split('-').join('');
        //     // updateArray.phoneNumber = phno;
        //     //updateArray.roles = ["ADMIN_USER"];
        //     //console.log('Update profile ', updateArray);
        //     axios.put(AppConst.APIURL + '/user/updateProfile', updateArray).then(res => {
        //         this.setState({
        //             editLoader: false
        //         });
        //         let username = {};
        //         username.first_name = res.data.first_name;
        //         username.last_name = res.data.last_name;
        //         this.props.handelUser(username);
        //         // sessionStorage.setItem('first_name', res.data.first_name);
        //         // sessionStorage.setItem('last_name', res.data.last_name);
        //         console.log('User update Response ', res);
        //         resetForm({});
        //         this.setState({ showSuccessModal: true });
        //     })
        //         .catch(this.displayError);
        // }
    }

    // PERMISION
    // userPermission = () => {
    //     const user = new UserBusiness();
    //     user.getPermissions().then(arr => {
    //         this.setState({ userRole: arr })
    //         console.log('Permission array ', this.state.userRole);
    //     });
    // }

    // USER DETAILS
    getUserDetails = () => {
        this.setState({
            loaderDetails: true
        }, () => {
            axios
                .post(AppConst.APIURL + '/api/profileDetails')
                .then(res => {
                    if (res.data.success) {
                        //res.data.phoneNumber = this.maskPhoneNumber(res.data.phoneNumber); //'+1 045-287-17777';
                        this.setState({
                            values: res.data.user,
                            userDetails: res.data.user.user_detail,
                            countrySelected: res.data.user.user_detail.country,
                            stateSelected: res.data.user.user_detail.state,
                            dateOfBirth: res.data.user.user_detail.dob && Moment(res.data.user.user_detail.dob).toDate()
                        });
                        let params = {
                            'id': res.data.user.user_detail.country,
                        };
                        axios.post(AppConst.APIURL + '/api/getStates', params)
                            .then(res => {
                                this.setState({ stateList: res.data.data });
                            });
                        let paramsCity = {
                            'id': res.data.user.user_detail.state,
                        };
                        axios.post(AppConst.APIURL + '/api/getCities', paramsCity)
                            .then(res => {
                                this.setState({ cityList: res.data.data, loaderDetails: false, });
                            });
                    }
                })
                .catch(this.displayError);
        });
    }

    componentWillMount() {
        this.getUserDetails();
        // this.userPermission();
    }

    handleeditCouponEnable = () => {
        this.setState({
            editCouponEnable: true,
            disabled: true
        });
    }

    onFileChange = event => {
        this.setState({ selectedFile: event.target.files[0] });
    };

    handleState = e => {
        this.setState({ countrySelected: e.target.value, stateList: [], cityList: [] });
        let params = {
            'id': e.target.value,
        };
        axios.post(AppConst.APIURL + '/api/getStates', params)
            .then(res => {
                this.setState({ stateList: res.data.data });
            });
    }

    handleCity = e => {
        this.setState({ stateSelected: e.target.value, cityList: [] });
        let params = {
            'id': e.target.value,
        };
        axios.post(AppConst.APIURL + '/api/getCities', params)
            .then(res => {
                const cityList = res.data.data;
                this.setState({ cityList });
            });
    }

    handleDOB = date => {
        this.setState({ dateOfBirth: date });
    }

    render() {
        const {
            disabled
        } = this.state;
        const values = {
            first_name: this.state.userDetails.first_name,
            last_name: this.state.userDetails.last_name,
            // dob: this.state.userDetails.dob && Moment(this.state.userDetails.dob).toDate(),
            gender: this.state.userDetails.gender,
            city: this.state.userDetails.city,
            state: this.state.userDetails.state,
            country: this.state.userDetails.country,
            biography: this.state.userDetails.biography,
            profile_pic: this.state.userDetails.profile_pic,
        };
        return (
            <div className="myProfileOuterModal">
                <div className="boxBgModal">
                    <Row>
                        <Col sm={12}>
                            {this.state.editLoader ? <LoadingSpinner /> : null}
                        </Col>
                    </Row>
                    <Formik
                        initialValues={values}
                        validationSchema={yourDetailsSchema}
                        onSubmit={this.handleSubmit}
                        enableReinitialize={true}
                    >
                        {({
                            values,
                            errors,
                            touched,
                            isSubmitting,
                            // handleChange,
                            // handleBlur
                        }) => {
                            return (
                                <Form>
                                    <Row>
                                        <Col sm={12}>
                                            {this.state.errorMessge ? (
                                                <div className="alert alert-danger" role="alert">
                                                    {this.state.errorMessge}
                                                </div>
                                            ) : null}
                                        </Col>
                                    </Row>

                                    {this.state.loaderDetails === true ? (
                                        <Row>
                                            <Col md={12}>
                                                <LoadingSpinner />
                                            </Col>
                                        </Row>)
                                        : (
                                            <Fragment>
                                                <Row className={'mb-3'}>
                                                    <Col md={6}>
                                                        <h4> Your Details</h4>
                                                        <p style={{ paddingTop: '0', fontSize: '13px', fontWeight: '300' }}><span className="required">*</span> These fields are required.</p>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col xs={12} md={12}>
                                                        <Row>
                                                            <Col xs={12} sm={6} md={6}>
                                                                <FormGroup controlId="formBasicText">
                                                                    <label>First Name <span className="required">*</span></label>
                                                                    <Field
                                                                        name="first_name"
                                                                        type="text"
                                                                        value={values.first_name || ''}
                                                                        className={'form-control'}
                                                                        autoComplete="nope"
                                                                        placeholder="Enter"
                                                                        disabled={disabled === false ? 'disabled' : ''}
                                                                    />
                                                                    {errors.first_name && touched.first_name ?
                                                                        <span className="errorMsg">{errors.first_name}</span>
                                                                        : null}
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>

                                                            <Col xs={12} sm={6} md={6}>
                                                                <FormGroup controlId="formBasicText">
                                                                    <label>Last Name <span className="required">*</span></label>
                                                                    <Field
                                                                        name="last_name"
                                                                        type="text"
                                                                        value={values.last_name || ''}
                                                                        className={'form-control'}
                                                                        autoComplete="nope"
                                                                        placeholder="Enter"
                                                                        disabled={disabled === false ? 'disabled' : ''}
                                                                    />
                                                                    {errors.last_name && touched.last_name ? <span className="errorMsg">{errors.last_name}</span> : null}
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>

                                                            <Col xs={12} sm={6} md={6}>
                                                                <FormGroup controlId="formBasicText">
                                                                    <label>Date of Birth <span className="required">*</span></label>
                                                                    {/* <Field
                                                                        name="dob"
                                                                        type="date"
                                                                        value={values.dob || ''}
                                                                        className={'form-control'}
                                                                        autoComplete="nope"
                                                                        placeholder="Enter"
                                                                        disabled={disabled === false ? 'disabled' : ''}
                                                                    /> */}
                                                                    <DatePicker
                                                                        name='dob'
                                                                        selected={this.state.dateOfBirth}
                                                                        onChange={this.handleDOB}
                                                                        className='form-control'
                                                                        maxDate={new Date(Moment(new Date()).subtract(18, 'years').toISOString())}
                                                                        required
                                                                        dateFormat="dd-MM-yyyy"
                                                                        showMonthDropdown
                                                                        showYearDropdown
                                                                        placeholderText="DOB"
                                                                        autoComplete="off"
                                                                        disabled={disabled === false ? 'disabled' : ''}
                                                                    />
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>

                                                            <Col xs={12} sm={6} md={6}>
                                                                <FormGroup controlId="formBasicText">
                                                                    <label>Gender<span className="required">*</span></label>
                                                                    <Field
                                                                        name="gender"
                                                                        component="select"
                                                                        className={'form-control'}
                                                                        autoComplete="nope"
                                                                        placeholder="select"
                                                                        disabled={disabled === false ? 'disabled' : ''}
                                                                    >
                                                                        <option value="">Select Gender</option>
                                                                        <option value="Male" key="1">Male </option>
                                                                        <option value="Female" key="0">Female </option>
                                                                    </Field>
                                                                    {errors.gender && touched.gender ? (
                                                                        <span className="errorMsg ml-3">{errors.gender}</span>
                                                                    ) : null}
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>

                                                            <Col xs={12} sm={6} md={6}>
                                                                <FormGroup controlId="formBasicText">
                                                                    <label>Country</label>
                                                                    <Field
                                                                        name="Country"
                                                                        component="select"
                                                                        className={'form-control'}
                                                                        autoComplete="nope"
                                                                        placeholder="select"
                                                                        disabled={disabled === false ? 'disabled' : ''}
                                                                        value={this.state.countrySelected}
                                                                        onChange={this.handleState}
                                                                    >
                                                                        <option value="">Select Country</option>
                                                                        {this.state.countryList.map(data => {
                                                                            return <option value={data.id} key={data.id}>{data.name}</option>;
                                                                        })}
                                                                    </Field>
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>

                                                            <Col xs={12} sm={6} md={6}>
                                                                <FormGroup controlId="formBasicText">
                                                                    <label>State</label>
                                                                    <Field
                                                                        name="State"
                                                                        component="select"
                                                                        className={'form-control'}
                                                                        autoComplete="nope"
                                                                        placeholder="select"
                                                                        disabled={disabled === false ? 'disabled' : ''}
                                                                        onChange={this.handleCity}
                                                                        value={this.state.stateSelected}
                                                                    >
                                                                        <option value="">Select State</option>
                                                                        {this.state.stateList.map(data => {
                                                                            return <option value={data.id} key={data.id}>{data.name}</option>;
                                                                        })}
                                                                    </Field>
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>

                                                            <Col xs={12} sm={6} md={6}>
                                                                <FormGroup controlId="formBasicText">
                                                                    <label>City</label>
                                                                    <Field
                                                                        name="city"
                                                                        component="select"
                                                                        className={'form-control'}
                                                                        autoComplete="nope"
                                                                        placeholder="select"
                                                                        disabled={disabled === false ? 'disabled' : ''}
                                                                    >
                                                                        <option value="">Select City</option>
                                                                        {this.state.cityList.map(data => {
                                                                            return <option value={data.id} key={data.id}>{data.name}</option>;
                                                                        })}
                                                                    </Field>
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>

                                                            <Col xs={12} sm={6} md={6}>
                                                                <FormGroup controlId="formBasicText">
                                                                    <label>Profile Pic</label>
                                                                    <input
                                                                        name="selectedFile"
                                                                        type="file"
                                                                        className={'form-control'}
                                                                        autoComplete="nope"
                                                                        placeholder="Enter"
                                                                        disabled={disabled === false ? 'disabled' : ''}
                                                                        onChange={this.onFileChange}
                                                                    />
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>

                                                            <Col xs={12} sm={12} md={12}>
                                                                <FormGroup controlId="formBasicText">
                                                                    <label>Biography</label>
                                                                    <Field
                                                                        name="biography"
                                                                        type="text"
                                                                        component="textarea"
                                                                        value={values.biography || ''}
                                                                        className={'form-control'}
                                                                        autoComplete="nope"
                                                                        placeholder="Enter"
                                                                        disabled={disabled === false ? 'disabled' : ''}
                                                                    />
                                                                    {errors.biography && touched.biography ?
                                                                        <span className="errorMsg">{errors.biography}</span>
                                                                        : null}
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>

                                                            {values.profile_pic &&
                                                                <Row className="show-grid">
                                                                    <Col xs={12} md={12}>
                                                                        <img height="200" className="ml-3 rounded" src={AppConst.UPLOADURL + '/profile/' + values.profile_pic}></img>
                                                                    </Col>
                                                                </Row>
                                                            }
                                                            {/* <Col xs={12} sm={6} md={6}>
                                                                <FormGroup controlId="formBasicText">
                                                                    <label>Email </label>
                                                                    <Field
                                                                        disabled
                                                                        name="email"
                                                                        type="text"
                                                                        value={values.email}
                                                                        className={'form-control'}
                                                                        autoComplete="off"
                                                                        placeholder="Enter"

                                                                    />
                                                                    {errors.email && touched.email ? <span className="errorMsg">{errors.email}</span> : null}
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col> */}

                                                            {/* <Col xs={12} sm={6} md={6}>
                                                                <FormGroup controlId="formBasicText">
                                                                    <label>Phone</label>
                                                                    <InputMask
                                                                        mask="1 999-999-9999"
                                                                        maskChar={null}
                                                                        type="tel"
                                                                        name="phoneNumber"
                                                                        className={`form-control ${values.phoneNumber && 'input-elem-filled'}`}
                                                                        value={values.phoneNumber}
                                                                        onChange={handleChange}
                                                                        onBlur={handleBlur}
                                                                        placeholder="Enter"
                                                                        disabled={disabled === false ? 'disabled' : ''}
                                                                    />
                                                                    {errors.phoneNumber && touched.phoneNumber ? (
                                                                        <span className="errorMsg">{errors.phoneNumber}</span>
                                                                    ) : null}
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col> */}
                                                        </Row>

                                                        {/* <Row>
                                                            <Col xs={12} sm={6} md={6}>
                                                                <FormGroup controlId="formBasicText">
                                                                    <label>Department </label>
                                                                    <Field
                                                                        name="gender"
                                                                        value={values.gender}
                                                                        type="text"
                                                                        className={'form-control'}
                                                                        autoComplete="nope"
                                                                        placeholder="Enter"
                                                                        disabled={disabled === false ? 'disabled' : ''}
                                                                    />
                                                                    {errors.gender && touched.gender ? <span className="errorMsg">{errors.gender}</span> : null}
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>

                                                            <Col xs={12} sm={6} md={6}>
                                                                <FormGroup controlId="formBasicText">
                                                                    <label>Job Title </label>
                                                                    <Field
                                                                        name="dob"
                                                                        value={values.dob}
                                                                        type="text"
                                                                        className={'form-control'}
                                                                        autoComplete="nope"
                                                                        placeholder="Enter"
                                                                        disabled={disabled === false ? 'disabled' : ''}
                                                                    />
                                                                    {errors.dob && touched.dob ? <span className="errorMsg">{errors.dob}</span> : null}
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>
                                                        </Row> */}

                                                        {/* {this.props.disabled === true ? 
                                                        <Row>
                                                            <Col xs={12} sm={6} md={6}>
                                                                <FormGroup controlId="formBasicText">
                                                                    <label>Password </label>
                                                                    <Field
                                                                        name="password"
                                                                        type="password"
                                                                        className={'form-control'}
                                                                        autoComplete="off"
                                                                        value={values.password || ''}
                                                                        disabled={disabled === false ? 'disabled' : ''}
                                                                    />
                                                                    {errors.password && touched.password ? <span className="errorMsg">{errors.password}</span> : null}
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>

                                                            <Col xs={12} sm={6} md={6}>
                                                                <FormGroup controlId="formBasicText">
                                                                    <label>Confirm Password </label>
                                                                    <Field
                                                                        name="confirmPassword"
                                                                        type="password"
                                                                        className={'form-control'}
                                                                        autoComplete="off"
                                                                        value={values.confirmPassword || ''}
                                                                        disabled={disabled === false ? 'disabled' : ''}
                                                                    />
                                                                    {errors.confirmPassword && touched.confirmPassword ? <span className="errorMsg">{errors.confirmPassword}</span> : null}
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>

                                                            <Col xs={12} sm={4}  md={4}>
                                                        <FormGroup controlId="formBasicText">
                                                        <label>User Type</label>
                                                        <Field name="userType" value="Test" type="text" disabled className={`form-control`} autoComplete="off" placeholder="Enter"/>
                                                        {errors.userType && touched.userType ? <span className="errorMsg">{errors.userType}</span> : null}
                                                        <FormControl.Feedback />
                                                        </FormGroup>
                                                        </Col>

                                                        </Row>
                                                            : null} */}
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col md={12}>&nbsp;</Col>
                                                </Row>

                                                {/* {this.state.userRole.indexOf('USER_READ') !== -1 ? (<Fragment> */}
                                                {/* <Row className={'text-center'}>
                                                <Col md={12}>
                                                    {
                                                        this.props.disabled === true ?
                                                            <Button className="but-gray mr-2" type="button" onClick={this.props.onCancle}>Cancel</Button> : null
                                                    }
                                                    <Button className="blue-btn ml-2" type="submit">Save</Button>
                                                </Col>
                                            </Row> */}
                                                {/* </Fragment>) : null} */}
                                                <Row className="show-grid text-center">
                                                    <Col xs={12} md={12}>
                                                        <Fragment>
                                                            {this.state.editCouponEnable !== true ? (
                                                                <Fragment>
                                                                    <Button className="blue-btn border-0" onClick={this.handleeditCouponEnable}>
                                                                        Edit
                                                                </Button>
                                                                </Fragment>) :
                                                                (<Fragment>
                                                                    <Button
                                                                        onClick={this.props.handelProfileClose}
                                                                        className="but-gray border-0 mr-2">
                                                                        Cancel
                                                                </Button>
                                                                    <Button type="submit" className="blue-btn ml-2 border-0" disabled={isSubmitting}>
                                                                        Save
                                                                </Button>
                                                                </Fragment>)
                                                            }
                                                        </Fragment>
                                                    </Col>
                                                </Row>
                                            </Fragment>
                                        )
                                    }
                                </Form>);
                        }}
                    </Formik>


                    {/* <Modal
                        show={this.state.showSuccessModal}
                        onHide={this.handleSuccessClose}
                        className="payOptionPop"
                    >
                        <Modal.Body>
                            <Row>
                                <Col md={12} className="text-center">
                                    <Image src={SuccessIco} />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12} className="text-center my-3">
                                    <h5>
                                        Profile has been updated successfully
                                    </h5>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12} className="text-center">
                                    <Button
                                        onClick={this.handleSuccessClose}
                                        className="but-gray"
                                    >
                                        Done
                                    </Button>
                                </Col>
                            </Row>
                        </Modal.Body>
                    </Modal> */}

                </div>
            </div>
        );
    }
}

Profile.propTypes = {
    disabled: PropTypes.bool,
    onCancle: PropTypes.func,
    handelUser: PropTypes.func,
    handelProfileClose: PropTypes.func,
    handleProfileConfirMmsg: PropTypes.func,
};

const mapDispatchToProps = dispatch => {
    return {
        handelUser: (user) => dispatch(handelUser(user))
    };
};

export default connect(null, mapDispatchToProps)(Profile);
