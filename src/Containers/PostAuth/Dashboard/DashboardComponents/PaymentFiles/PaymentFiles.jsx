import React, { Component } from 'react';
import {
    //Row,
    //Col,
    //FormGroup,
    //FormControl,
    //Button,
    Tabs,
    Tab,
//Image
} from 'react-bootstrap';
import ACHFiles from './ACHFiles';
import CheckFiles from './CheckFiles';
import './PaymentFiles.scss';


class PaymentFiles extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            key: 'ACH files',
            

        };
    }

    
    
    


    render(){
        return(
            <div className=''>
                <Tabs
                    id="controlled-tab-example"
                    activeKey={this.state.key}
                    onSelect={key => this.setState({ key })}
                    className="cusTabControl px-3 mainMenuTab "
                >
                    <Tab eventKey="ACH files" title="ACH Files">
                        {this.state.key === 'ACH files' ? <ACHFiles /> : null}
                        
                    </Tab>
                    <Tab eventKey="Check files" title="Check Files">
                        {this.state.key === 'Check files' ? <CheckFiles /> : null}
                    </Tab>
                    
                </Tabs>

            </div>
        );
    }
}

export default PaymentFiles;
