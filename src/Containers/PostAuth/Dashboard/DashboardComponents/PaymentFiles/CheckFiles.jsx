import React, { Component } from 'react';
import {
    Table,
    Row,
    Col
} from 'react-bootstrap';
import axios from '../../../../../shared/eaxios';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import NumberFormat from 'react-number-format';
import Pagination from 'react-js-pagination';



class CheckFiles extends Component {

    state= {
        LoadingSpinner: false,
        CheckFileLists: [],
        errorMessge: null,
        activePage: 1,
        totalCount: 0,
        itemPerPage: 250,
        cpbusinesslist: [],
        ProcessingBank: ''
    }

    _isMounted = false;

    componentDidMount() {
        this._isMounted = true;
        this.getAllAchFile();
        this.getcpforbusiness(0);

    }

    componentWillUnmount() {
        this._isMounted = false;

    }

    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber });
        this.getAllAchFile(pageNumber > 0 ? pageNumber - 1 : 0);
    };

    handleChangeItemPerPage = (e) => {
        this.setState({ itemPerPage: e.target.value },
            () => {
                this.getAllAchFile(this.state.activePage > 0 ? this.state.activePage - 1 : 0);
            });
    }

    resetPagination = () => {
        this.setState({ activePage: 1 });
    }

    getAllAchFile = (since = 0) => {
        this.setState({
            LoadingSpinner: true
        });
        axios
            .get(`/checkPayment/getAllCheckFile?since=${since}&limit=${this.state.itemPerPage}&processingBank=${this.state.ProcessingBank}`)
            .then(res => {
                if (this._isMounted) {
                    this.setState({
                        CheckFileLists: res.data.entries,
                        totalCount: res.data.total,
                        LoadingSpinner: false
                    }, ()=>{
                        
                    });
                }

            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    errorMessge: errorMsg,
                    LoadingSpinner: false
                });

            });
    }
    ProcessingBankOnchange = (e) => {
        console.log(e.target.value);
        this.setState({
            ProcessingBank: e.target.value
        },()=>{
            this.getAllAchFile();
        });
       

    }
    getcpforbusiness = (businessId) => {
        console.log('businessId', businessId);
        this.setState({
            ProcessingBank: ''
        }, () => {

            axios
                .get(
                    `/centralPayment/${businessId}`

                )
                .then(res => {
                    console.log('getcpforbusiness', res);
                    this.setState({
                        cpbusinesslist: res.data
                    });

                })
                .catch(e => {
                    console.log(e);
                });
        });



    }

    render() {
        console.log('ach files', this.state.CheckFileLists);
        return (
            <div className='p-3'>
                <div className='row my-3'>
                    <div className="col-md-12">
                        <select className="form-control businessDropdown float-left truncate pr-35 ml-2" onChange={this.ProcessingBankOnchange} value={this.state.ProcessingBank}>
                            <option value=''> Select Processing Bank </option>
                            {
                                this.state.cpbusinesslist.map(cpbusiness => (
                                    <option key={cpbusiness.bankOption}
                                        value={cpbusiness.bankOption}
                                    >
                                        {cpbusiness.bankOption}
                                    </option>
                                )
                                )
                            }
                        </select>
                    </div>
                </div>
                <div className="boxBg">
                    <div className="row show-grid">
                        <div className="col-sm-12">

                            <Table responsive hover className='bg-white'>
                                <thead className="theaderBg">
                                    <tr>
                                        {/* <th>
                                <span className="float-left pr-2">Business Name </span>                            
                            </th> */}
                                        <th>
                                            <span className="float-left pr-2">Check File Name </span>
                                        </th>
                                        <th>
                                            <span className="float-right ">Creation Date</span>
                                        </th>
                                        <th>
                                            <span className="float-right ">Processing Bank</span>
                                        </th>
                                        <th>
                                            <span className="float-right ">Total Amount</span>
                                        </th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>


                                    {this.state.LoadingSpinner ? (<tr>
                                        <td colSpan={12}>
                                            <LoadingSpinner />
                                        </td>
                                    </tr>) :
                                        this.state.CheckFileLists.length > 0 ? (
                                            this.state.CheckFileLists.map(CheckFileList => (
                                                <tr key={CheckFileList.id} >
                                                    {/* <td>
                                            {CheckFileList.businessName}

                                        </td> */}
                                                    <td>
                                                        {CheckFileList.fileName}

                                                    </td>
                                                    <td align="right">
                                                        {CheckFileList.createdDate}

                                                    </td>
                                                    <td align="right">
                                                        {CheckFileList.processingBank}

                                                    </td>
                                                    <td align="right">
                                                        <NumberFormat
                                                            value={CheckFileList.totalAmount}
                                                            displayType={'text'}
                                                            thousandSeparator={true}
                                                            fractionSize={2}
                                                            prefix={'$'}
                                                            decimalScale={2}
                                                            fixedDecimalScale={true}
                                                            thousandsGroupStyle={'thousand'}
                                                            renderText={value => <span>{value}</span>}
                                                        />

                                                    </td>
                                                    <td align="right">
                                                        <a rel="noopener noreferrer" target="_blank" href={CheckFileList.downloadUrl} download>
                                                            <button className="btn btn-primary">Download</button>
                                                        </a>
                                                    </td>
                                                </tr>
                                            ))
                                        )
                                            :
                                            this.state.errorMessge ? <tr>
                                                <td colSpan={6}>
                                                    <p className="text-center">{this.state.errorMessge}</p>
                                                </td>
                                            </tr> : (
                                                <tr>
                                                    <td colSpan={6}>
                                                        <p className="text-center">No records found</p>
                                                    </td>
                                                </tr>
                                            )
                                    }

                                </tbody>
                            </Table>

                        </div>
                    </div>
                </div>
                
                
                {this.state.totalCount ? (
                    <Row className="px-3">
                        <Col md={4} className="d-flex flex-row mt-20">
                            <span className="mr-2 mt-2 font-weight-500">Items per page</span>
                            <select
                                id={this.state.itemPerPage}
                                className="form-control truncatefloat-left w-90"
                                onChange={this.handleChangeItemPerPage}
                                value={this.state.itemPerPage}>
                                <option value='50'>50</option>
                                <option value='100'>100</option>
                                <option value='150'>150</option>
                                <option value='200'>200</option>
                                <option value='250'>250</option>

                            </select>
                        </Col>
                        <Col md={8}>
                            <div className="paginationOuter text-right">
                                <Pagination
                                    activePage={this.state.activePage}
                                    itemsCountPerPage={this.state.itemPerPage}
                                    totalItemsCount={this.state.totalCount}
                                    onChange={this.handlePageChange}
                                />
                            </div>
                        </Col>
                    </Row>
                ) : null}
            </div>
        );
    }
}

export default CheckFiles;
