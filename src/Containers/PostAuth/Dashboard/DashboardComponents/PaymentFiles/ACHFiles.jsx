import React, { Component } from 'react';
import {
    Table,
    Image,
    Row,
    Col
} from 'react-bootstrap';
import refreshIcon from './../../../../../assets/refreshIcon.png';
import axios from './../../../../../shared/eaxios';
import LoadingSpinner from './../../../../../Components/LoadingSpinner/LoadingSpinner';
import NumberFormat from 'react-number-format';
import Pagination from 'react-js-pagination';
//import DownloadLink from 'react-download-link';
import { Link } from 'react-router-dom';

// import { saveAs } from 'file-saver';


// const getBaseName = (path) => {
//     return path.split('/').reverse()[0];
// };

class ACHFiles extends Component {

    state = {
        LoadingSpinner: false,
        achFileLists: [],
        errorMessge: null,
        downloadLink: '',
        selectedPaymentMode: '',
        businessSearchKey: '',
        activePage: 1,
        totalCount: 0,
        itemPerPage: 250,
        cpbusinesslist: [],
        ProcessingBank: ''
    }

    _isMounted = false;

    componentDidMount() {
        this._isMounted = true;
        this.getAllAchFile();
        this.getcpforbusiness(0);
    }

    componentWillUnmount() {
        this._isMounted = false;

    }

    handelDownload = (param) => {
        console.log('param', param);

        // window.open(param);


        // window.location.href = param;
    }

    getAllAchFile = (since = 0) => {
        this.setState({
            LoadingSpinner: true
        });
        axios
            .get(`/achPayment/getAllAchFile?since=${since}&limit=${this.state.itemPerPage}&type=${this.state.selectedPaymentMode}&searchKey=${this.state.businessSearchKey}&processingBank=${this.state.ProcessingBank}`)
            .then(res => {
                if (this._isMounted) {
                    this.setState({
                        achFileLists: res.data.entries,
                        totalCount: res.data.total,
                        LoadingSpinner: false
                    }, () => {

                    });
                }

            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    errorMessge: errorMsg,
                    LoadingSpinner: false
                });

            });
    }

    // downloadLink = ( path ) => {
    //     saveAs( path, getBaseName(path));
    // }

    handleChange = (e) => {

        this.setState({ selectedPaymentMode: e.target.value },
            () => {
                console.log('this.state.value', this.state.selectedPaymentMode);
                this.getAllAchFile(0);
            });
    }

    keyPress = (e) => {
        /*if (e.keyCode == 13) {
            console.log('Entered trace no', e.target.value);    
        } */
        this.setState(
            {
                businessSearchKey: e.target.value.trim()
            },
            () => {
            }
        );

    }

    handelSearch = () => {
        this.getAllAchFile(0);
    }

    resetSearch = () => {

        this.setState({
            selectedPaymentMode: '',
            businessSearchKey: '',
            activePage: 1
        });
        this.getAllAchFile(0);
    }

    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber });
        this.getAllAchFile(pageNumber > 0 ? pageNumber - 1 : 0);
    };

    handleChangeItemPerPage = (e) => {
        this.setState({ itemPerPage: e.target.value },
            () => {
                this.getAllAchFile(this.state.activePage > 0 ? this.state.activePage - 1 : 0);
            });
    }


    resetPagination = () => {
        this.setState({ activePage: 1 });
    }
    getcpforbusiness = (businessId) => {
        console.log('businessId', businessId);
        this.setState({
            ProcessingBank: ''
        }, () => {

            axios
                .get(
                    `/centralPayment/${businessId}`

                )
                .then(res => {
                    console.log('getcpforbusiness', res);
                    this.setState({
                        cpbusinesslist: res.data
                    });

                })
                .catch(e => {
                    console.log(e);
                });
        });



    }
    ProcessingBankOnchange = (e) => {
        console.log(e.target.value);
        this.setState({
            ProcessingBank: e.target.value
        });


    }

    render() {
        console.log('ach files', this.state.achFileLists);
        return (
            <div className='p-3'>
                <div className='row my-3'>
                    <div className="col-md-12">
                        <select
                            id={this.state.selectedPaymentMode}
                            className="form-control truncate pr-35 float-left w-210"
                            onChange={this.handleChange}
                            value={this.state.selectedPaymentMode}>
                            <option value=''>All Transaction Type</option>
                            <option value='CR'> CR </option>
                            <option value='DR'> DR </option>
                        </select>
                        <select className="form-control businessDropdown float-left truncate pr-35 ml-2" onChange={this.ProcessingBankOnchange} value={this.state.ProcessingBank}>
                            <option value=''> Select Processing Bank </option>
                            {
                                this.state.cpbusinesslist.map(cpbusiness => (
                                    <option key={cpbusiness.bankOption}
                                        value={cpbusiness.bankOption}
                                    >
                                        {cpbusiness.bankOption}
                                    </option>
                                )
                                )
                            }
                        </select>
                        <input type="text" className="w-210 form-control float-left ml-2" placeholder="Search by Business Name" onChange={this.keyPress} value={this.state.businessSearchKey} />
                        <button
                            type="button"
                            className="btn ml-3 search-btn text-black float-left cus-mt-3"
                            onClick={() => this.handelSearch()}
                        >
                            Search
                        </button>
                        <Link to="#" className='ml-2'>
                            <Image
                                className="mt-2"
                                src={refreshIcon}
                                onClick={() => this.resetSearch()}
                            />
                        </Link>
                    </div>
                </div>
                <div className="boxBg">
                    <div className="row show-grid">
                        <div className="col-sm-12">

                            <Table responsive hover className='bg-white achCol'>
                                <thead className="theaderBg">
                                    <tr>
                                        <th>
                                            <span className="float-left pr-2">Business Name </span>
                                        </th>
                                        <th>
                                            <span className="float-left pr-2">ACH File Name </span>
                                        </th>
                                        <th>
                                            <span className="float-right">Processing Date</span>
                                        </th>
                                        <th>
                                            <span className="float-right">Creation Date</span>
                                        </th>
                                        <th>
                                            <span className="float-left pr-2">Transaction Type</span>
                                        </th>
                                        <th>
                                            <span className="float-left pr-2">Processing Bank</span>
                                        </th>
                                        <th>
                                            <span className="float-right">Total Amount</span>
                                        </th>

                                        <th>

                                        </th>
                                    </tr>
                                </thead>
                                <tbody>


                                    {this.state.LoadingSpinner ? (<tr>
                                        <td colSpan={12}>
                                            <LoadingSpinner />
                                        </td>
                                    </tr>) :
                                        this.state.achFileLists.length > 0 ? (
                                            this.state.achFileLists.map(achFileList => (
                                                <tr key={achFileList.id} >
                                                    <td>
                                                        {achFileList.businessName}

                                                    </td>
                                                    <td >
                                                        <div className="achFileWrap">{achFileList.achFileName}</div>
                                                    </td>
                                                    <td align="right">
                                                        {achFileList.processingDate}

                                                    </td>
                                                    <td align="right">
                                                        {achFileList.creationDate}
                                                    </td>
                                                    <td>
                                                        {achFileList.transactionType}
                                                    </td>
                                                    <td>
                                                        {achFileList.processingBank}
                                                    </td>

                                                    <td align="right">
                                                        <NumberFormat
                                                            value={achFileList.totalAmount}
                                                            displayType={'text'}
                                                            thousandSeparator={true}
                                                            fractionSize={2}
                                                            prefix={'$'}
                                                            decimalScale={2}
                                                            fixedDecimalScale={true}
                                                            thousandsGroupStyle={'thousand'}
                                                            renderText={value => <span>{value}</span>}
                                                        />

                                                    </td>
                                                    <td align="right">
                                                        {/* <DownloadLink
                                                label="Save"
                                                filename="https://devachprocess.s3.us-west-2.amazonaws.com/ACH/20190214_Epayrails_CBKC_ORIGACH_File9.txt"
                                                exportFile={() => 'My cached data'}
                                            /> */}
                                                        <a href={achFileList.downloadUrl} download target={'_blank'}>
                                                            <button className="btn btn-primary">Download</button>
                                                        </a>
                                                        {/* <a href="#" download onClick={() => this.downloadLink('https://devachprocess.s3.us-west-2.amazonaws.com/ACH/20190214_Epayrails_CBKC_ORIGACH_File9.txt')}>link</a> */}
                                                        {/* <button className="btn btn-primary" onClick={() => this.handelDownload(achFileList.achFileName)}>Download</button> */}
                                                    </td>
                                                </tr>
                                            ))
                                        )
                                            :
                                            this.state.errorMessge ? <tr>
                                                <td colSpan={6}>
                                                    <p className="text-center">{this.state.errorMessge}</p>
                                                </td>
                                            </tr> : (
                                                <tr>
                                                    <td colSpan={6}>
                                                        <p className="text-center">No records found</p>
                                                    </td>
                                                </tr>
                                            )
                                    }

                                </tbody>
                            </Table>

                        </div>
                    </div>
                </div>

                {this.state.totalCount ? (
                    <Row className="px-3">
                        <Col md={4} className="d-flex flex-row mt-20">
                            <span className="mr-2 mt-2 font-weight-500">Items per page</span>
                            <select
                                id={this.state.itemPerPage}
                                className="form-control truncatefloat-left w-90"
                                onChange={this.handleChangeItemPerPage}
                                value={this.state.itemPerPage}>
                                <option value='50'>50</option>
                                <option value='100'>100</option>
                                <option value='150'>150</option>
                                <option value='200'>200</option>
                                <option value='250'>250</option>

                            </select>
                        </Col>
                        <Col md={8}>
                            <div className="paginationOuter text-right">
                                <Pagination
                                    activePage={this.state.activePage}
                                    itemsCountPerPage={this.state.itemPerPage}
                                    totalItemsCount={this.state.totalCount}
                                    onChange={this.handlePageChange}
                                />
                            </div>
                        </Col>
                    </Row>
                ) : null}

            </div>
        );
    }
}

export default ACHFiles;
