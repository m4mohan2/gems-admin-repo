import React, { Component } from 'react';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import * as am4maps from '@amcharts/amcharts4/maps';
import am4geodata_worldLow from '@amcharts/amcharts4-geodata/worldLow';
import am4themes_dataviz from '@amcharts/amcharts4/themes/dataviz';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import axios from 'axios';
// import Moment from 'react-moment';
am4core.useTheme(am4themes_dataviz);
am4core.useTheme(am4themes_animated);

// import { ComposableMap, Geographies, Geography } from 'react-simple-maps';
// import { scaleQuantile } from 'd3-scale';
// import { csv } from 'd3-fetch';

class SampleChart extends Component {
    state = {
        days: 10,
        startDate: 0,
        endDate: 30,
        dataCsv: [],
        covidData: [],
        from: '',
        to: '',
        countryData: []
    };

    heatMap = () => {
        let targetSVG = 'M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z';

        // Create map instance
        let chart = am4core.create('chartdiv', am4maps.MapChart);

        // Set map definition
        chart.geodata = am4geodata_worldLow;

        // Set projection
        chart.projection = new am4maps.projections.Miller();

        // Create map polygon series
        let polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

        // Exclude Antartica
        polygonSeries.exclude = ['AQ'];

        // Make map load polygon (like country names) data from GeoJSON
        polygonSeries.useGeodata = true;

        // Configure series
        let polygonTemplate = polygonSeries.mapPolygons.template;
        polygonTemplate.strokeOpacity = 0.5;
        polygonTemplate.nonScalingStroke = true;

        // create capital markers
        let imageSeries = chart.series.push(new am4maps.MapImageSeries());

        // define template
        let imageSeriesTemplate = imageSeries.mapImages.template;
        let circle = imageSeriesTemplate.createChild(am4core.Sprite);
        circle.scale = 0.4;
        circle.fill = new am4core.InterfaceColorSet().getFor('alternativeBackground');
        circle.path = targetSVG;
        // what about scale...

        // set propertyfields
        imageSeriesTemplate.propertyFields.latitude = 'latitude';
        imageSeriesTemplate.propertyFields.longitude = 'longitude';

        imageSeriesTemplate.horizontalCenter = 'middle';
        imageSeriesTemplate.verticalCenter = 'middle';
        imageSeriesTemplate.align = 'center';
        imageSeriesTemplate.valign = 'middle';
        imageSeriesTemplate.width = 8;
        imageSeriesTemplate.height = 8;
        imageSeriesTemplate.nonScaling = true;
        imageSeriesTemplate.tooltipText = 'Country: {title} Confirmed: {confirmed} Recovered: {recovered} Dead: {dead}';
        imageSeriesTemplate.fill = am4core.color('#000');
        imageSeriesTemplate.background.fillOpacity = 0;
        imageSeriesTemplate.background.fill = am4core.color('#ffffff');
        imageSeriesTemplate.setStateOnChildren = true;
        imageSeriesTemplate.states.create('hover');

        imageSeries.data = this.state.countryData;


    }

    lineChart = (start, end) => {

        let chart = am4core.create('linechart', am4charts.XYChart);
        chart.responsive.enabled = true;

        // Increase contrast by taking evey second color
        chart.colors.step = 2;

        // Add data
        chart.data = this.state.covidData.slice(start, end);

        // Create axes
        let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.renderer.minGridDistance = 50;

        // Create series
        function createAxisAndSeries(field, name, opposite, bullet) {
            let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            if (chart.yAxes.indexOf(valueAxis) != 0) {
                valueAxis.syncWithAxis = chart.yAxes.getIndex(0);
            }

            let series = chart.series.push(new am4charts.LineSeries());
            series.dataFields.valueY = field;
            series.dataFields.dateX = 'date';
            series.strokeWidth = 2;
            series.yAxis = valueAxis;
            series.name = name;
            series.tooltipText = '{name}: [bold]{valueY}[/]';
            series.tensionX = 0.8;
            series.showOnInit = true;

            let interfaceColors = new am4core.InterfaceColorSet();

            switch (bullet) {
                case 'triangle': {
                    bullet = series.bullets.push(new am4charts.Bullet());
                    bullet.width = 12;
                    bullet.height = 12;
                    bullet.horizontalCenter = 'middle';
                    bullet.verticalCenter = 'middle';

                    let triangle = bullet.createChild(am4core.Triangle);
                    triangle.stroke = interfaceColors.getFor('background');
                    triangle.strokeWidth = 2;
                    triangle.direction = 'top';
                    triangle.width = 12;
                    triangle.height = 12;
                    break;
                }
                case 'rectangle': {
                    bullet = series.bullets.push(new am4charts.Bullet());
                    bullet.width = 10;
                    bullet.height = 10;
                    bullet.horizontalCenter = 'middle';
                    bullet.verticalCenter = 'middle';

                    let rectangle = bullet.createChild(am4core.Rectangle);
                    rectangle.stroke = interfaceColors.getFor('background');
                    rectangle.strokeWidth = 2;
                    rectangle.width = 10;
                    rectangle.height = 10;
                    break;
                }
                default:
                    bullet = series.bullets.push(new am4charts.CircleBullet());
                    bullet.circle.stroke = interfaceColors.getFor('background');
                    bullet.circle.strokeWidth = 2;
                    break;
            }

            valueAxis.renderer.line.strokeOpacity = 1;
            valueAxis.renderer.line.strokeWidth = 2;
            valueAxis.renderer.line.stroke = series.stroke;
            valueAxis.renderer.labels.template.fill = series.stroke;
            valueAxis.renderer.opposite = opposite;
        }

        createAxisAndSeries('active', 'Active', false, 'circle');
        createAxisAndSeries('confirmed', 'Confirmed', true, 'triangle');
        createAxisAndSeries('deaths', 'Deaths', true, 'rectangle');
        createAxisAndSeries('recovered', 'Recovered', true, 'triangle');

        // Add legend
        chart.legend = new am4charts.Legend();

        // Add cursor
        chart.cursor = new am4charts.XYCursor();

        // generate some random data, quite different range
        // function generateChartData() {
        //     let chartData = [];
        //     let firstDate = new Date();
        //     firstDate.setDate(firstDate.getDate() - days);
        //     firstDate.setHours(0, 0, 0, 0);

        //     let visits = 1600;
        //     let hits = 2900;
        //     let views = 8700;
        //     let likes = 5500;

        //     for (let i = 0; i < 10; i++) {
        //         // we create date objects here. In your data, you can have date strings
        //         // and then set format of your dates using chart.dataDateFormat property,
        //         // however when possible, use date objects, as this will speed up chart rendering.
        //         let newDate = new Date(firstDate);
        //         newDate.setDate(newDate.getDate() + i);

        //         visits += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10);
        //         hits += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10);
        //         views += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10);
        //         likes += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10);

        //         chartData.push({
        //             date: newDate,
        //             visits: visits,
        //             hits: hits,
        //             views: views,
        //             likes: likes
        //         });
        //     }
        //     return chartData;
        // }
    }

    componentDidMount() {
        axios.get('https://www.trackcorona.live/api/countries')
            .then(response => {
                let countryData = [];
                for (let i = 0; i < response.data.data.length; i++) {
                    countryData.push({ title: response.data.data[i].location, latitude: response.data.data[i].latitude, longitude: response.data.data[i].longitude, confirmed: response.data.data[i].confirmed, dead: response.data.data[i].dead, recovered: response.data.data[i].recovered });
                }
                this.setState({ countryData });
                this.heatMap();
            });

        // csv('/Unemployement.csv').then(counties => {
        //     console.log(counties);
        //     this.setState({ dataCsv: counties });
        // });
        const getHeatMapData = () => {
            return [
                { id: 'AP', state: 'Andhra Pradesh', value: 34 },
                { id: 'AR', state: 'Arunachal Pradesh', value: 45 },
                { id: 'AS', state: 'Assam', value: 3 },
                { id: 'BR', state: 'Bihar', value: 23 },
                { id: 'CT', state: 'Chhattisgarh', value: 36 },
                { id: 'GA', state: 'Goa', value: 21 },
                { id: 'GJ', state: 'Gujarat', value: 22 },
                { id: 'HR', state: 'Haryana', value: 14 },
                { id: 'HP', state: 'Himachal Pradesh', value: 24 },
                { id: 'JH', state: 'Jharkhand', value: 26 },
                { id: 'KA', state: 'Karnataka', value: 27 },
                { id: 'KL', state: 'Kerala', value: 31 },
                { id: 'MP', state: 'Madhya Pradesh', value: 25 },
                { id: 'MH', state: 'Maharashtra', value: 13 },
                { id: 'MN', state: 'Manipur', value: 36 },
                { id: 'ML', state: 'Meghalaya', value: 59 },
                { id: 'MZ', state: 'Mizoram', value: 65 },
                { id: 'NL', state: 'Nagaland', value: 59 },
                { id: 'OR', state: 'Odisha', value: 59 },
                { id: 'PB', state: 'Punjab', value: 42 },
                { id: 'RJ', state: 'Rajasthan', value: 31 },
                { id: 'SK', state: 'Sikkim', value: 25 },
                { id: 'TN', state: 'Tamil Nadu', value: 55 },
                { id: 'TG', state: 'Telangana', value: 33 },
                { id: 'TR', state: 'Tripura', value: 22 },
                { id: 'UT', state: 'Uttarakhand', value: 11 },
                { id: 'UP', state: 'Uttar Pradesh', value: 15 },
                { id: 'WB', state: 'West Bengal', value: 17 },
                { id: 'WB', state: 'West Bengal', value: 17 },
                { id: 'AN', state: 'Andaman and Nicobar Islands', value: 65 },
                { id: 'CH', state: 'Chandigarh', value: 34 },
                { id: 'DN', state: 'Dadra and Nagar Haveli', value: 19 },
                { id: 'DD', state: 'Daman and Diu', value: 20 },
                { id: 'DL', state: 'Delhi', value: 59 },
                { id: 'JK', state: 'Jammu and Kashmir', value: 25 },
                { id: 'LA', state: 'Ladakh', value: 36 },
                { id: 'LD', state: 'Lakshadweep', value: 23 },
                { id: 'PY', state: 'Puducherry', value: 32 }
            ];
        };
        this.setState({ dataCsv: getHeatMapData() });
        axios.get('https://api.covid19api.com/country/india')
            .then(response => {
                let covidData = [];
                for (let i = 0; i < response.data.length; i++) {
                    covidData.push({ date: response.data[i].Date, active: response.data[i].Active, confirmed: response.data[i].Confirmed, deaths: response.data[i].Deaths, recovered: response.data[i].Recovered });
                }
                this.setState({ covidData, from: response.data[0].Date, to: response.data[30].Date });
                this.lineChart(this.state.startDate, this.state.endDate);
            });

    }

    handleChangeForPrev = () => {
        let startDate = this.state.startDate;
        let endDate = this.state.endDate;
        if (startDate > 0) {
            endDate = startDate;
            startDate = startDate - 30;
            this.setState({ startDate, endDate });
            this.lineChart(startDate, endDate);
        }
    }

    handleChangeForNext = () => {
        let startDate = this.state.startDate;
        let endDate = this.state.endDate;
        if (endDate < this.state.covidData.length) {
            startDate = endDate;
            endDate = endDate + 30;
            this.setState({ startDate, endDate });
            this.lineChart(startDate, endDate);
        }
    }

    render() {

        // const geoUrl = 'https://rawgit.com/Anujarya300/bubble_maps/master/data/geography-data/india.topo.json';
        // const colorScale = scaleQuantile()
        //     .domain(this.state.dataCsv.map(d => d.value))
        //     .range([
        //         '#ffedea',
        //         '#ffcec5',
        //         '#ffad9f',
        //         '#ff8a75',
        //         '#ff5533',
        //         '#e2492d',
        //         '#be3d26',
        //         '#9a311f',
        //         '#782618'
        //     ]);
        // const PROJECTION_CONFIG = {
        //     scale: 350,
        //     center: [78.9629, 22.5937]
        // };

        return (
            // <ComposableMap projection='geoMercator' width={600} height={220} projectionConfig={PROJECTION_CONFIG}>
            //     <Geographies geography={geoUrl}>
            //         {({ geographies }) =>
            //             geographies.map(geo => {
            //                 const cur = this.state.dataCsv.find(s => s.id === geo.id);
            //                 return (
            //                     <Geography
            //                         key={geo.rsmKey}
            //                         geography={geo}
            //                         fill={cur ? colorScale(cur.value) : '#EEE'}
            //                     />
            //                 );
            //             })
            //         }
            //     </Geographies>
            // </ComposableMap>
            <div className='w-100 h-75 bg-light'>
                <div className='row ml-3 mr-3'>
                    <div className='col-md-3'>
                        <button type='button' className='btn btn-sm btn-primary' onClick={this.handleChangeForPrev}><i className='fa fa-angle-left'></i></button>
                    </div>
                    <div className='col-md-6 text-center text-body'>
                        <h4 className=''>Covid-19 Data India</h4>
                        {/* <Moment format='DD-MM-YYYY'>{this.state.from}</Moment>
                        &nbsp;To&nbsp;
                        <Moment format='DD-MM-YYYY'>{this.state.to}</Moment> */}
                    </div>
                    <div className='col-md-3 text-right'>
                        <button type='button' className='btn btn-sm btn-primary' onClick={this.handleChangeForNext}><i className='fa fa-angle-right'></i></button>
                    </div>
                </div>
                <div id='linechart' className='w-100 h-75'></div>
                <div id='chartdiv' className='w-100 h-50 mt-1 bg-light'></div>
            </div>

        );
    }
}

export default SampleChart;
