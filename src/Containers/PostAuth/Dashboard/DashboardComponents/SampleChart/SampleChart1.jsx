import React, { Component } from 'react';
import { Bar, Line, Pie } from 'react-chartjs-2';

class SampleChart extends Component {

    render() {
        const optionsForBar = {
            labels: ['Likes', 'Comments', 'Views',
                'Shares', 'Favorites'],
            datasets: [
                {
                    label: 'Total',
                    backgroundColor: [
                        '#B21F00',
                        '#C9DE00',
                        '#2FDE00',
                        '#00A6B4',
                        '#6800B4'
                    ],
                    borderColor: [
                        '#B21F00',
                        '#C9DE00',
                        '#2FDE00',
                        '#00A6B4',
                        '#6800B4'
                    ],
                    borderWidth: 2,
                    fontColor: '#5A5757',
                    data: [65, 59, 80, 81, 76]
                }
            ]
        };

        let weeks = [];
        for (let i = 1; i < 53; i++) {
            weeks.push('Week-' + i);
        }

        console.log(weeks);

        const optionsForLine = {
            labels: weeks,
            datasets: [
                {
                    label: 'Likes',
                    fill: false,
                    lineTension: 0.5,
                    backgroundColor: 'rgba(75,192,192,1)',
                    borderColor: 'rgba(0,0,0,1)',
                    borderWidth: 1,
                    data: [65, 59, 80, 81, 77, 65, 88, 80, 81, 98, 89, 90, 99, 87, 85, 65, 78, 80, 81, 77, 65, 88, 80, 81, 98, 89, 90, 99, 87, 85, 65, 89, 80, 81, 77, 65, 88, 80, 81, 98, 89, 90, 99, 87, 85, 65, 89, 80, 81, 77, 65, 88]
                }
            ]
        };

        const optionsForPie = {
            labels: ['Likes', 'Comments', 'Views',
                'Shares', 'Favorites'],
            datasets: [
                {
                    label: 'Count',
                    backgroundColor: [
                        '#B21F00',
                        '#C9DE00',
                        '#2FDE00',
                        '#00A6B4',
                        '#6800B4'
                    ],
                    hoverBackgroundColor: [
                        '#501800',
                        '#4B5000',
                        '#175000',
                        '#003350',
                        '#35014F'
                    ],
                    data: [65, 59, 80, 81, 56]
                }
            ]
        };

        return (
            <div className="d-flex align-items-center justify-content-center" >
                {/* <div className="engraved">Coming Soon....</div> */}
                < div className="container" >
                    <div className="row">
                        <div className="col-md-12 mt-5 mb-5 bg-light">
                            <Bar
                                data={optionsForBar}
                                options={{
                                    title: {
                                        display: true,
                                        text: 'Total Likes, Comment, Views, Share, and Favorites.',
                                        fontSize: 20
                                    },
                                    legend: {
                                        display: false,
                                        position: 'right'
                                    }
                                }}
                            />
                        </div>
                        <div className="col-md-12 mt-5 mb-5 bg-light">
                            <Line
                                data={optionsForLine}
                                options={{
                                    title: {
                                        display: true,
                                        text: 'Weekly Likes',
                                        fontSize: 20
                                    },
                                    legend: {
                                        display: true,
                                        position: 'right'
                                    }
                                }}
                            />
                        </div>
                        <div className="col-md-12 mt-5 mb-5 bg-light">
                            <Pie
                                data={optionsForPie}
                                options={{
                                    title: {
                                        display: true,
                                        text: 'Total Likes, Comment, Views, Share, and Favorites.',
                                        fontSize: 20
                                    },
                                    legend: {
                                        display: true,
                                        position: 'right'
                                    }
                                }}
                            />
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

export default SampleChart;
