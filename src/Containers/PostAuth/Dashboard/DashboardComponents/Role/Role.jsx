import React, { Component, Fragment } from 'react';
import axios from '../../../../../shared/eaxios';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import { Table, Row, Col, Modal, Image, Button } from 'react-bootstrap';
import SuccessIco from '../../../../../assets/success-ico.png';
import Pagination from 'react-js-pagination';
import EditRole from './EditRole';
import AddRole from './AddRole';
import * as AppConst from './../../../../../common/constants';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class Role extends Component {
    state = {
        activePage: 1,
        totalCount: 0,
        itemPerPage: 10,
        sort: 1,
        field: null,
        fetchErrorMsg: null,
        roleLists: [],
        loading: true,
        sortingActiveID: 1,
        statusChange: false,
        statuserrorMsg: null,
        roleId: null,
        status: null,
        roleSearchKey: null,
        permission: []
    }

    _isMounted = false;

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handelAddModal = () => {
        this.setState({
            addModal: true,
        });
    }

    handelAddModalClose = () => {
        this.setState({
            addModal: false,
        });
    }

    sortingActive = (id) => {
        this.setState({
            sortingActiveID: id
        });
    }

    fetchRole = (sort = this.state.sort, field = this.state.field) => {
        this.setState({
            loading: true, sort: sort, field: field
        }, () => {
            axios.get(AppConst.APIURL +
                `/api/getRole?pageSize=${this.state.itemPerPage}&page=${this.state.sort}&searchKey=${this.state.roleSearchKey}`)
                .then(res => {
                    const roleLists = res.data.data;
                    const totalCount = res.data.totalCount;
                    if (this._isMounted) {
                        this.setState({
                            roleLists,
                            totalCount: totalCount,
                            loading: false
                        });
                    }
                })
                .catch(e => {
                    let errorMsg = this.displayError(e);
                    this.setState({
                        fetchErrorMsg: errorMsg,
                        loading: false
                    });
                    setTimeout(() => {
                        this.setState({ fetchErrorMsg: null });
                    }, 5000);
                });
        });
    }

    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber });
        this.fetchRole(pageNumber > 0 ? pageNumber : 0, this.state.field);
    };

    handleChangeItemPerPage = (e) => {
        this.setState({ itemPerPage: e.target.value },
            () => {
                this.fetchRole();
            });
    }

    handleAddConfirMmsg = () => {
        this.setState({
            addConfirMmsg: true,
        }, () => {
            this.fetchRole();
        });
    }

    handleAddConfirMmsgClose = () => {
        this.setState({
            addConfirMmsg: false,
        });
    }

    handleEditConfirMmsg = () => {
        this.setState({
            viewEditModal: false,
            editConfirMmsg: true
        }, () => {
            this.fetchRole();
        });
    }

    handleEditConfirMmsgClose = () => {
        this.setState({
            editConfirMmsg: false
        });
    }

    handelviewEditModal = (data) => {
        this.setState({
            viewEditModal: true,
            roleData: data
        });
    }

    handelviewEditModalClose = () => {
        this.setState({
            viewEditModal: false,
        });
    }

    componentDidMount() {
        this._isMounted = true;
        const facility = this.props.facility;
        const permission = [];
        facility.map(data => {
            data.name === 'role' && data.permission.length !== 0 && permission.push(...data.permission);
        });
        this.setState({ permission }, () => {
            permission[1].status === true &&
                this.fetchRole();
        });
    }

    componentWillMount() {
        this._isMounted = false;
    }

    handelDeleteModal = (id) => {
        this.setState({
            deleteRole: true,
            roleId: id
        });
    }

    handleHide = () => {
        this.setState({
            statusChange: false,
            statuserrorMsg: null
        });
    }

    handleDeleteHide = () => {
        this.setState({
            deleteRole: false,
            deleteerrorMsg: null
        });
    }

    handleDelete(id) {
        axios.delete(AppConst.APIURL + `/api/deleteRolePermission/${id}`)
            .then(res => {
                console.log(res);
                this.setState({
                    roleId: null,
                    successMessage: 'Role removed successfully',
                    deleteConfirMmsg: true
                });
                this.handleDeleteHide();
                this.fetchRole();
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    deleteerrorMsg: errorMsg,
                    loading: false
                });
                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);
            });
    }

    handleDeleteChangedClose = () => {
        this.setState({
            deleteConfirMmsg: false,
            successMessage: null
        });
    }

    handelStatusModal = (id, status) => {
        this.setState({
            statusChange: true,
            roleId: id,
            status: status
        });

    }

    handleStatus(id, status) {
        let updateArray = {
            'status': status
        };
        axios.put(AppConst.APIURL + `/api/statusUpdateService/${id}`, updateArray)
            .then(res => {
                console.log('--------------res------', res);
                this.setState({
                    roleId: null,
                    status: null,
                    successMessage: 'Status successfully changed',
                    statusConfirMmsg: true
                });
                this.handleHide();
                this.fetchRole();
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    statuserrorMsg: errorMsg,
                    loading: false
                });
                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);
            });
    }

    handleStatusChangedClose = () => {
        this.setState({
            statusConfirMmsg: false,
            successMessage: null
        });
    }

    render() {
        return (
            <div className="dashboardInner businessOuter pt-3">
                {this.state.permission[1] && this.state.permission[1].status === true ?
                    <Fragment>
                        {this.state.permission[0] && this.state.permission[0].status === true &&
                            <Row className="pt-3">
                                <Col sm={6} md={6}></Col>
                                <Col sm={6} md={6}>
                                    <button className="btn btn-primary float-right" onClick={() => this.handelAddModal()}>Add Role Permissions</button>
                                </Col>
                            </Row>}
                        <Row className="show-grid pt-3">
                            <Col sm={12} md={12}>
                                <div className="boxBg">
                                    <Table responsive hover>
                                        <thead className="theaderBg">
                                            <tr>
                                                <th>Role Name</th>
                                                <th>Description</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.loading ? (<tr><td colSpan={12}><LoadingSpinner /></td></tr>) :
                                                this.state.roleLists.length > 0 ?
                                                    (this.state.roleLists.map(role => (
                                                        <tr key={role.id}>
                                                            <td>{role.roleName}</td>
                                                            <td>{role.roleDescription}</td>
                                                            <td>
                                                                <i className="fa fa-eye" aria-hidden="true" title="View" onClick={() => this.handelviewEditModal(role)}></i>
                                                                &nbsp;
                                                                {this.state.permission[3] && this.state.permission[3].status === true &&
                                                                    <i className="fa fa-trash-o red" aria-hidden="true" title="Delete" onClick={() => this.handelDeleteModal(role.id)}></i>
                                                                }
                                                            </td>
                                                        </tr>
                                                    )))
                                                    : this.state.fetchErrorMsg ? null : (<tr>
                                                        <td colSpan={12}>
                                                            <p className="text-center">No records found</p>
                                                        </td>
                                                    </tr>)
                                            }
                                        </tbody>
                                    </Table>
                                </div>
                            </Col>
                        </Row>
                        {this.state.totalCount ? (
                            <Row>
                                <Col md={4} className="d-flex flex-row mt-20">
                                    <span className="mr-2 mt-2 font-weight-500">Items per page</span>
                                    <select
                                        id={this.state.itemPerPage}
                                        className="form-control truncatefloat-left w-90"
                                        onChange={this.handleChangeItemPerPage}
                                        value={this.state.itemPerPage}>
                                        <option value='10'>10</option>
                                        <option value='50'>50</option>
                                        <option value='100'>100</option>
                                        <option value='150'>150</option>
                                        <option value='200'>200</option>
                                        <option value='250'>250</option>
                                    </select>
                                </Col>
                                <Col md={8}>
                                    <div className="paginationOuter text-right">
                                        <Pagination
                                            activePage={this.state.activePage}
                                            itemsCountPerPage={this.state.itemPerPage}
                                            totalItemsCount={this.state.totalCount}
                                            onChange={this.handlePageChange}
                                        />
                                    </div>
                                </Col>
                            </Row>
                        ) : null}
                    </Fragment>
                    : <h5 className="text-center p-3">You do not have any permission to view this content.</h5>}
                {/*========================= Modal for Status change =====================*/}
                <Modal
                    show={this.state.statusChange}
                    onHide={this.handleHide}
                    dialogClassName="modal-90w"
                    aria-labelledby="example-custom-modal-styling-title"
                >
                    <Modal.Body>
                        <div className="m-auto text-center">
                            <h6 className="mb-3 text-dark">Do you want to change this status?</h6>
                        </div>
                        {this.state.statuserrorMsg ? <div className="alert alert-danger my-3 text-center col-12" role="alert">{this.state.statuserrorMsg}</div> : null}
                        <div className="m-auto text-center">
                            <button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.handleHide()}>Return</button>
                            {this.state.statuserrorMsg == null ? <button className="btn btn-danger" onClick={() => this.handleStatus(this.state.roleId, this.state.status)}>Confirm</button> : null}
                        </div>

                    </Modal.Body>
                </Modal>

                {/*====== Status change confirmation popup  ===== */}
                <Modal
                    show={this.state.statusConfirMmsg}
                    onHide={this.handleStatusChangedClose}
                    className="payOptionPop"
                >
                    <Modal.Body className="text-center">
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>{this.state.successMessage}</h5>
                            </Col>
                        </Row>
                        <Button
                            onClick={this.handleStatusChangedClose}
                            className="but-gray mt-3"
                        >
                            Return
                        </Button>
                    </Modal.Body>
                </Modal>

                {/* Add Role Modal */}
                <Modal show={this.state.addModal}
                    onHide={this.handelAddModalClose}
                    className="right full noPadding slideModal" >
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body className="">
                        <div className="modalHeader">
                            <Row>
                                <Col md={9}>
                                    <h1>Add Role Permissions</h1>
                                </Col>
                            </Row>
                        </div>
                        <div className="modalBody content-body noTabs">
                            <AddRole
                                handleAddConfirMmsg={this.handleAddConfirMmsg}
                                handelAddModalClose={this.handelAddModalClose}
                            />
                        </div>
                    </Modal.Body>
                </Modal>

                {/*======  Add confirmation popup  ===== */}
                <Modal
                    show={this.state.addConfirMmsg}
                    onHide={this.handleAddConfirMmsgClose}
                    className="payOptionPop">
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Record has been successfully Added</h5>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <Button
                                    onClick={this.handleAddConfirMmsgClose}
                                    className="but-gray">
                                    Return
                                </Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                {/* Edit Role Modal */}
                <Modal
                    show={this.state.viewEditModal}
                    onHide={this.handelviewEditModalClose}
                    className="right full noPadding slideModal"
                >
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body className="">
                        <div className="modalBody content-body noTabs">
                            <EditRole
                                {...this.state.roleData}
                                handelviewEditModalClose={this.handelviewEditModalClose}
                                handleEditConfirMmsg={this.handleEditConfirMmsg} />
                        </div>
                    </Modal.Body>
                </Modal>

                {/*====== Edit confirmation popup  ===== */}
                <Modal
                    show={this.state.editConfirMmsg}
                    onHide={this.handleEditConfirMmsgClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Record has been successfully edited</h5>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <Button
                                    onClick={this.handleEditConfirMmsgClose}
                                    className="but-gray"
                                >
                                    Return
                                </Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                {/*========================= Modal for Delete Role =====================*/}
                <Modal
                    show={this.state.deleteRole}
                    onHide={this.handleHide}
                    dialogClassName="modal-90w"
                    aria-labelledby="example-custom-modal-styling-title" >
                    <Modal.Body>
                        <div className="m-auto text-center">
                            <h6 className="mb-3 text-dark">Do you want to delete this Role?</h6>
                        </div>
                        {this.state.deleteerrorMsg ? <div className="alert alert-danger my-3 text-center col-12" role="alert">{this.state.deleteerrorMsg}</div> : null}
                        <div className="m-auto text-center">
                            <button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.handleDeleteHide()}>Return</button>
                            {this.state.deleteerrorMsg == null ? <button className="btn btn-danger" onClick={() => this.handleDelete(this.state.roleId)}>Confirm</button> : null}
                        </div>
                    </Modal.Body>
                </Modal>

                {/*====== Delete confirmation popup  ===== */}
                <Modal
                    show={this.state.deleteConfirMmsg}
                    onHide={this.handleDeleteChangedClose}
                    className="payOptionPop" >
                    <Modal.Body className="text-center">
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>{this.state.successMessage}</h5>
                            </Col>
                        </Row>
                        <Button
                            onClick={this.handleDeleteChangedClose}
                            className="but-gray mt-3"
                        >
                            Return
                        </Button>
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}

const mapStateToPros = state => {
    return {
        facility: state.auth.facility
    };
};
Role.propTypes = {
    facility: PropTypes.any
};
export default connect(mapStateToPros, null)(Role);