import React, { Component, Fragment } from 'react';
import * as Yup from 'yup';
import axios from '../../../../../shared/eaxios';
import * as AppConst from './../../../../../common/constants';
import { Button, Col, FormControl, FormGroup, Row } from 'react-bootstrap';
import { Field, Form, Formik } from 'formik';
import PropTypes from 'prop-types';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import { FormCheckbox } from 'shards-react';
import { connect } from 'react-redux';

const editRoleSchema = Yup.object().shape({
    roleName: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter role name')
        .max(40, 'maximum characters length 40'),
    roleDescription: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .max(250, 'maximum characters length 250')
});

class EditRole extends Component {
    state = {
        errorMessge: null,
        editRoleEnable: false,
        disabled: false,
        editRoleLoader: false,
        editErrorMessge: false,

        role: [],
        gemster: [],
        gems: [],
        coupon: [],
        category: [],
        cms: [],
        news: [],
        websiteHome: [],
        testimonials: [],
        faq: [],
        services: [],
        membershipLicense: [],
        providers: [],
        audittrail: [],
        subCategory: [],
        claimList: [],

        rolePermission: [],
    };

    static getDerivedStateFromProps(props, state) {
        if (!state.roleData) {
            return {
                ...props
            };
        }
    }

    handleEditRoleEnable = () => {
        this.setState({
            editRoleEnable: true,
            disabled: true
        });
    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handleSubmit = (values, { setSubmitting }) => {
        this.setState({
            editRoleLoader: true,
        });
        const facility = [{ id: null, name: 'role', name_key: null, permission: this.state.role, status: values.roleStatus, url: 'role' },
        { id: null, name: 'gemster', name_key: null, permission: this.state.gemster, status: values.gemsterStatus, url: 'gemster' },
        { id: null, name: 'gems', name_key: null, permission: this.state.gems, status: values.gemsStatus, url: 'gems' },
        { id: null, name: 'coupon', name_key: null, permission: this.state.coupon, status: values.couponStatus, url: 'coupon' },
        { id: null, name: 'category', name_key: null, permission: this.state.category, status: values.categoryStatus, url: 'category' },
        { id: null, name: 'cms', name_key: null, permission: this.state.cms, status: values.cmsStatus, url: 'cms' },
        { id: null, name: 'news', name_key: null, permission: this.state.news, status: values.newsStatus, url: 'news' },
        { id: null, name: 'websiteHome', name_key: null, permission: this.state.websiteHome, status: values.websiteHomeStatus, url: 'websiteHome' },
        { id: null, name: 'testimonials', name_key: null, permission: this.state.testimonials, status: values.testimonialsStatus, url: 'testimonials' },
        { id: null, name: 'faq', name_key: null, permission: this.state.faq, status: values.faqStatus, url: 'faq' },
        { id: null, name: 'services', name_key: null, permission: this.state.services, status: values.servicesStatus, url: 'services' },
        { id: null, name: 'membershipLicense', name_key: null, permission: this.state.membershipLicense, status: values.membershipLicenseStatus, url: 'membershipLicense' },
        { id: null, name: 'providers', name_key: null, permission: this.state.providers, status: values.providersStatus, url: 'providers' },
        { id: null, name: 'audittrail', name_key: null, permission: this.state.audittrail, status: values.auditTrailStatus, url: 'audittrail' },
        { id: null, name: 'subcategory', name_key: null, permission: this.state.subCategory, status: values.subCategoryStatus, url: 'subcategory' },
        { id: null, name: 'claimlist', name_key: null, permission: this.state.claimList, status: values.claimListStatus, url: 'claimlist' }];

        let newValue = {
            roleName: values.roleName,
            roleDescription: values.roleDescription,
            facility: facility
        };
        axios
            .put(AppConst.APIURL + `/api/editRolePermission/${values.id}`, newValue)
            .then(res => {
                console.log('Service details get data', res.data);
                setSubmitting(false);
                this.setState({
                    editRoleLoader: false,
                    editRoleEnable: false,
                    disabled: false,
                }, () => {
                    this.props.handleEditConfirMmsg();
                });
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    editRoleLoader: false,
                    editErrorMessge: errorMsg,
                });

                setTimeout(() => {
                    this.setState({ deleteErrorMessge: null });
                }, 1000);

            });
    };

    componentDidMount() {
        const rolefacility = this.props.roleFacility;
        const rolePermission = [];
        rolefacility.map(data => {
            data.name === 'role' && data.permission.length !== 0 && rolePermission.push(...data.permission);
        });
        this.setState({ rolePermission });

        const initialValues = { ...this.props };
        const facility = initialValues.facility;
        this.setState({
            role: facility[0].permission,
            gemster: facility[1].permission,
            gems: facility[2].permission,
            coupon: facility[3].permission,
            category: facility[4].permission,
            cms: facility[5].permission,
            news: facility[6].permission,
            websiteHome: facility[7].permission,
            testimonials: facility[8].permission,
            faq: facility[9].permission,
            services: facility[10].permission,
            membershipLicense: facility[11].permission,
            providers: facility[12].permission,
            audittrail: facility[13].permission,
            subCategory: facility[14] && facility[14].permission,
            claimList: facility[15] && facility[15].permission,
        });
    }

    handleCheckboxChange(e, status, name) {
        if (this.state.disabled === true) {
            let stateCopy = Object.assign({}, name);
            stateCopy[status].status = !stateCopy[status].status;
            this.setState({ stateCopy });
        }
    }

    render() {
        const initialValues = { ...this.props };
        const values = {
            id: initialValues.id,
            roleName: initialValues.roleName,
            roleDescription: initialValues.roleDescription,

            roleStatus: initialValues.facility[0].status,
            gemsterStatus: initialValues.facility[1].status,
            gemsStatus: initialValues.facility[2].status,
            couponStatus: initialValues.facility[3].status,
            categoryStatus: initialValues.facility[4].status,
            cmsStatus: initialValues.facility[5].status,
            newsStatus: initialValues.facility[6].status,
            websiteHomeStatus: initialValues.facility[7].status,
            testimonialsStatus: initialValues.facility[8].status,
            faqStatus: initialValues.facility[9].status,
            servicesStatus: initialValues.facility[10].status,
            membershipLicenseStatus: initialValues.facility[11].status,
            providersStatus: initialValues.facility[12].status,
            auditTrailStatus: initialValues.facility[13].status,
            subCategoryStatus: initialValues.facility[14] && initialValues.facility[14].status,
            claimListStatus: initialValues.facility[15] && initialValues.facility[15].status,
        };
        const { disabled } = this.state;

        return (

            <div className="addcustomerSec">
                <div className="boxBg p-35">
                    <Row>
                        <Col sm={12}>
                            {this.state.editRoleLoader ? <LoadingSpinner /> : null}
                        </Col>
                    </Row>
                    <Row className="show-grid">
                        <Col xs={12} md={12}>
                            <div className="sectionTitle">
                                <h2>{this.state.editRoleEnable !== true ? 'View' : 'Edit'} Role Permissions</h2>
                            </div>
                        </Col>
                        <Col xs={12} className="brd-right">
                            <Formik
                                initialValues={values}
                                validationSchema={editRoleSchema}
                                onSubmit={this.handleSubmit}
                                enableReinitialize={true} >
                                {({
                                    values,
                                    errors,
                                    touched,
                                    isSubmitting,
                                }) => {
                                    return (
                                        <Form className={disabled === false ? ('hideRequired') : null}>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Role Name<span className="required">*</span></h3></label>
                                                        <Field
                                                            name="roleName"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter role name"
                                                            value={values.roleName || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.roleName && touched.roleName ? (
                                                            <span className="errorMsg ml-3">{errors.roleName}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Role Description<span className="required">*</span></h3></label>
                                                        <Field
                                                            name="roleDescription"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter role description"
                                                            value={values.roleDescription || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.roleDescription && touched.roleDescription ? (
                                                            <span className="errorMsg ml-3">{errors.roleDescription}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={12} md={12}>
                                                    <h4 className="mt-3 mb-4">Role and Permissions :</h4>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Role</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="roleStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >
                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.role.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.role)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Gemster</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="gemsterStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.gemster.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.gemster)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Gems</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="gemsStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.gems.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.gems)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Coupon</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="couponStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.coupon.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.coupon)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Category</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="categoryStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.category.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.category)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>CMS</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="cmsStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.cms.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.cms)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>News</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="newsStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.news.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.news)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Website Home</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="websiteHomeStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.websiteHome.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.websiteHome)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Testimonials</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="testimonialsStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.testimonials.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.testimonials)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>FAQ</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="faqStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.faq.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.faq)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Services</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="servicesStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.services.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.services)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Membership License</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="membershipLicenseStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.membershipLicense.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.membershipLicense)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Providers</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="providersStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.providers.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.providers)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Audit Trail</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="auditTrailStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.audittrail.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.audittrail)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Subcategory</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="subCategoryStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.subCategory.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.subCategory)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Claim List</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="claimListStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.claimList.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.claimList)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>


                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>&nbsp;</Col>
                                            </Row>
                                            <Row>&nbsp;</Row>
                                            {this.state.rolePermission[2] && this.state.rolePermission[2].status === true ?
                                                <Row className="show-grid text-center">
                                                    <Col xs={12} md={12}>
                                                        <Fragment>
                                                            {this.state.editRoleEnable !== true ? (
                                                                <Fragment>
                                                                    <Button className="blue-btn border-0" onClick={this.handleEditRoleEnable}>
                                                                        Edit
                                                                </Button>
                                                                </Fragment>) :
                                                                (<Fragment>
                                                                    <Button
                                                                        onClick={this.props.handelviewEditModalClose}
                                                                        className="but-gray border-0 mr-2">
                                                                        Cancel
                                                                </Button>
                                                                    <Button type="submit" className="blue-btn ml-2 border-0" disabled={isSubmitting}>
                                                                        Save
                                                                </Button>
                                                                </Fragment>)
                                                            }
                                                        </Fragment>
                                                    </Col>
                                                </Row> : ''}
                                            {disabled === false ? null : (<Fragment>
                                                <Row>
                                                    <Col md={12}>
                                                        <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                                    </Col>
                                                </Row>
                                            </Fragment>)}
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

const mapStateToPros = state => {
    return {
        roleFacility: state.auth.facility
    };
};

EditRole.propTypes = {
    globState: PropTypes.object,
    onClickAction: PropTypes.func,
    onReload: PropTypes.func,
    onClick: PropTypes.func,
    handelviewEditModalClose: PropTypes.func,
    handleEditConfirMmsg: PropTypes.func,
    roleFacility: PropTypes.any
};

export default connect(mapStateToPros, null)(EditRole);