import React, { Component } from 'react';
import * as Yup from 'yup';
import axios from '../../../../../shared/eaxios';
import * as AppConst from './../../../../../common/constants';
import { Button, Col, FormControl, FormGroup, Image, Modal, Row } from 'react-bootstrap';
import { Field, Form, Formik } from 'formik';
import SuccessIco from '../../../../../assets/success-ico.png';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import PropTypes from 'prop-types';
import { FormCheckbox } from 'shards-react';

const initialValues = {
    roleName: '',
    roleDescription: '',
    roleStatus: '1',
    gemsterStatus: '1',
    gemsStatus: '1',
    couponStatus: '1',
    categoryStatus: '1',
    cmsStatus: '1',
    newsStatus: '1',
    websiteHomeStatus: '1',
    testimonialsStatus: '1',
    faqStatus: '1',
    servicesStatus: '1',
    membershipLicenseStatus: '1',
    providersStatus: '1',
    auditTrailStatus: '1',
    subCategoryStatus: '1',
    claimListStatus: '1',
};

const addRoleSchema = Yup.object().shape({
    roleName: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter role name')
        .max(40, 'maximum characters length 40'),
    roleDescription: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .max(250, 'maximum characters length 250')
});

const role = [{ id: null, permissionName: 'create', status: false },
{ id: null, permissionName: 'view', status: false },
{ id: null, permissionName: 'edit', status: false },
{ id: null, permissionName: 'delete', status: false }];

const gemster = [{ id: null, permissionName: 'view', status: false },
{ id: null, permissionName: 'edit', status: false },
{ id: null, permissionName: 'export', status: false }];

const gems = [{ id: null, permissionName: 'chart', status: false },
{ id: null, permissionName: 'view', status: false },
{ id: null, permissionName: 'edit', status: false },
{ id: null, permissionName: 'delete', status: false }];

const coupon = [{ id: null, permissionName: 'create', status: false },
{ id: null, permissionName: 'view', status: false },
{ id: null, permissionName: 'edit', status: false },
{ id: null, permissionName: 'delete', status: false },
{ id: null, permissionName: 'redeem', status: false },
{ id: null, permissionName: 'assign', status: false }];

const category = [{ id: null, permissionName: 'create', status: false },
{ id: null, permissionName: 'view', status: false },
{ id: null, permissionName: 'edit', status: false },
{ id: null, permissionName: 'delete', status: false }];

const cms = [{ id: null, permissionName: 'view', status: false },
{ id: null, permissionName: 'edit', status: false }];

const news = [{ id: null, permissionName: 'create', status: false },
{ id: null, permissionName: 'view', status: false },
{ id: null, permissionName: 'edit', status: false },
{ id: null, permissionName: 'delete', status: false }];

const websiteHome = [{ id: null, permissionName: 'view', status: false },
{ id: null, permissionName: 'edit', status: false }];

const testimonials = [{ id: null, permissionName: 'create', status: false },
{ id: null, permissionName: 'view', status: false },
{ id: null, permissionName: 'edit', status: false },
{ id: null, permissionName: 'delete', status: false }];

const faq = [{ id: null, permissionName: 'create', status: false },
{ id: null, permissionName: 'view', status: false },
{ id: null, permissionName: 'edit', status: false },
{ id: null, permissionName: 'delete', status: false }];

const services = [{ id: null, permissionName: 'create', status: false },
{ id: null, permissionName: 'view', status: false },
{ id: null, permissionName: 'edit', status: false },
{ id: null, permissionName: 'delete', status: false }];

const membershipLicense = [{ id: null, permissionName: 'view', status: false },
{ id: null, permissionName: 'edit', status: false },
{ id: null, permissionName: 'delete', status: false }];

const providers = [{ id: null, permissionName: 'view', status: false },
{ id: null, permissionName: 'edit', status: false }];

const audittrail = [{ id: null, permissionName: 'view', status: false }];

const subCategory = [{ id: null, permissionName: 'create', status: false },
{ id: null, permissionName: 'view', status: false },
{ id: null, permissionName: 'edit', status: false },
{ id: null, permissionName: 'delete', status: false }];

const claimList = [{ id: null, permissionName: 'view', status: false },
{ id: null, permissionName: 'edit', status: false }];

class AddRole extends Component {
    state = {
        showConfirMmsg: false,
        errorMessge: null,
        addRoleLoader: false,
        addErrorMessge: null,
        role: role,
        gemster: gemster,
        gems: gems,
        coupon: coupon,
        category: category,
        cms: cms,
        news: news,
        websiteHome: websiteHome,
        testimonials: testimonials,
        faq: faq,
        services: services,
        membershipLicense: membershipLicense,
        providers: providers,
        audittrail: audittrail,
        subCategory: subCategory,
        claimList: claimList
    };

    handleChange = (e, field) => {
        this.setState({
            [field]: e.target.value
        });
    };

    handleConfirmReviewClose = () => {
        this.setState({ showConfirMmsg: false });
    };

    handleConfirmReviewShow = () => {
        this.setState({ showConfirMmsg: true });
    };

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handleSubmit = (values, { resetForm, setSubmitting }) => {
        this.setState({
            addRoleLoader: true,
        });
        const facility = [{ id: null, name: 'role', name_key: null, permission: this.state.role, status: values.roleStatus, url: 'role' },
        { id: null, name: 'gemster', name_key: null, permission: this.state.gemster, status: values.gemsterStatus, url: 'gemster' },
        { id: null, name: 'gems', name_key: null, permission: this.state.gems, status: values.gemsStatus, url: 'gems' },
        { id: null, name: 'coupon', name_key: null, permission: this.state.coupon, status: values.couponStatus, url: 'coupon' },
        { id: null, name: 'category', name_key: null, permission: this.state.category, status: values.categoryStatus, url: 'category' },
        { id: null, name: 'cms', name_key: null, permission: this.state.cms, status: values.cmsStatus, url: 'cms' },
        { id: null, name: 'news', name_key: null, permission: this.state.news, status: values.newsStatus, url: 'news' },
        { id: null, name: 'websiteHome', name_key: null, permission: this.state.websiteHome, status: values.websiteHomeStatus, url: 'websiteHome' },
        { id: null, name: 'testimonials', name_key: null, permission: this.state.testimonials, status: values.testimonialsStatus, url: 'testimonials' },
        { id: null, name: 'faq', name_key: null, permission: this.state.faq, status: values.faqStatus, url: 'faq' },
        { id: null, name: 'services', name_key: null, permission: this.state.services, status: values.servicesStatus, url: 'services' },
        { id: null, name: 'membershipLicense', name_key: null, permission: this.state.membershipLicense, status: values.membershipLicenseStatus, url: 'membershipLicense' },
        { id: null, name: 'providers', name_key: null, permission: this.state.providers, status: values.providersStatus, url: 'providers' },
        { id: null, name: 'audittrail', name_key: null, permission: this.state.audittrail, status: values.auditTrailStatus, url: 'audittrail' },
        { id: null, name: 'subcategory', name_key: null, permission: this.state.subCategory, status: values.subCategoryStatus, url: 'subcategory' },
        { id: null, name: 'claimlist', name_key: null, permission: this.state.claimList, status: values.claimListStatus, url: 'claimlist' }];

        let newValue = {
            roleName: values.roleName,
            roleDescription: values.roleDescription,
            facility: facility
        };
        axios
            .post(AppConst.APIURL + 'api/addRole', newValue)
            .then(res => {
                console.log(res);
                resetForm({});
                setSubmitting(false);
                this.props.handelAddModalClose();
                this.props.handleAddConfirMmsg();
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    addRoleLoader: false,
                    addErrorMessge: errorMsg,
                });
                setTimeout(() => {
                    this.setState({ deleteErrorMessge: null });
                }, 5000);
            });
    };

    handleCheckboxChange(e, status, name) {
        let stateCopy = Object.assign({}, name);
        stateCopy[status].status = !stateCopy[status].status;
        this.setState({ stateCopy });
    }

    render() {
        return (
            <div className="addcustomerSec">
                <div className="boxBg p-35">
                    {this.state.addErrorMessge ? (
                        <div className="alert alert-danger" role="alert">
                            {this.state.addErrorMessge}
                        </div>
                    ) : null}
                    <Row>
                        <Col sm={12}>
                            {this.state.addRoleLoader ? <LoadingSpinner /> : null}
                        </Col>
                    </Row>
                    <Row className="show-grid">
                        <Col xs={12} className="brd-right">
                            <Formik
                                initialValues={initialValues}
                                validationSchema={addRoleSchema}
                                onSubmit={this.handleSubmit}
                            >
                                {({
                                    values,
                                    errors,
                                    touched,
                                    isSubmitting,
                                }) => {
                                    return (
                                        <Form>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Role Name<span className="required">*</span></label>
                                                        <Field
                                                            name="roleName"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter role name"
                                                            value={values.roleName || ''}
                                                        />
                                                        {errors.roleName && touched.roleName ? (
                                                            <span className="errorMsg ml-3">{errors.roleName}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Role Description</label>
                                                        <Field
                                                            name="roleDescription"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter role description"
                                                            value={values.roleDescription || ''}
                                                        />
                                                        {errors.roleDescription && touched.roleDescription ? (
                                                            <span className="errorMsg ml-3">{errors.roleDescription}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={12} md={12}>
                                                    <h4 className="mt-3 mb-4">Role and Permissions :</h4>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Role</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="roleStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >
                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.role.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.role)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Gemster</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="gemsterStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.gemster.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.gemster)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Gems</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="gemsStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.gems.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.gems)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Coupon</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="couponStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.coupon.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.coupon)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Category</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="categoryStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.category.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.category)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>CMS</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="cmsStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.cms.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.cms)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>News</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="newsStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.news.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.news)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Website Home</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="websiteHomeStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.websiteHome.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.websiteHome)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Testimonials</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="testimonialsStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.testimonials.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.testimonials)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>FAQ</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="faqStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.faq.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.faq)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Services</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="servicesStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.services.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.services)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Membership License</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="membershipLicenseStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.membershipLicense.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.membershipLicense)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Providers</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="providersStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.providers.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.providers)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Audit Trail</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="auditTrailStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.audittrail.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.audittrail)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Subcategory</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="subCategoryStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.subCategory.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.subCategory)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={3} md={3}>
                                                    <h5>Claim List</h5>
                                                </Col>
                                                <Col xs={4} md={4}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <Field
                                                            name="claimListStatus"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >

                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={5} md={5}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        {this.state.claimList.map((data, index) => {
                                                            return <FormCheckbox
                                                                key={index}
                                                                inline
                                                                checked={data.status}
                                                                onChange={e => this.handleCheckboxChange(e, index, this.state.claimList)}
                                                            >
                                                                <span className='text-capitalize'>{data.permissionName}</span>
                                                            </FormCheckbox>;
                                                        })}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}> &nbsp;</Col>
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12} className="text-center">
                                                    <Button className="blue-btn" type="submit" disabled={isSubmitting}>
                                                        Save
                                                    </Button>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={12}>
                                                    <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                                </Col>
                                            </Row>
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Col>
                    </Row>
                </div>

                {/*======  confirmation popup  ===== */}
                <Modal
                    show={this.state.showConfirMmsg}
                    onHide={this.handleConfirmReviewClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Role Permissions has been successfully added</h5>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handleConfirmReviewClose}
                            className="but-gray" > Done
                        </Button>
                    </Modal.Footer>
                </Modal>

            </div>
        );
    }
}

AddRole.propTypes = {
    handleAddConfirMmsg: PropTypes.func,
    businessId: PropTypes.number,
    handelAddModalClose: PropTypes.func,
};

export default AddRole;
