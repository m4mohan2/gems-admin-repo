/* eslint-disable react/jsx-key */
import React, { Fragment, Component } from 'react';
import {
    Consumer
} from './../../../../../Context/Context';
import {
    //Grid, 
    Col,
    Row,
    Table,
    Modal,
    Image,
    Button,
    Collapse,
    Tabs,
    Tab

} from 'react-bootstrap';
import LoadingSpinner from './../../../../../Components/LoadingSpinner/LoadingSpinner';
import NumberFormat from 'react-number-format';

import './Transaction.scss';
import Pagination from 'react-js-pagination';
import DatePicker from 'react-date-picker';
import SuccessIco from './../../../../../assets/success-ico.png';
import axios from './../../../../../shared/eaxios';
//import { JsLogger } from './../../../../../services/jsLogger.service';
import refreshIcon from './../../../../../assets/refreshIcon.png';
import { Link } from 'react-router-dom';
import { FaDownload } from 'react-icons/fa';
import BusinessName from './../../../../../Components/Dashboard/BusinessName';
import { FiMaximize2, FiX } from 'react-icons/fi';
import InvoicePayment from './InvoicePayment';
import QuickbooksOnline from './QuickbooksOnline';
import moment from 'moment';
import { saveAs } from 'file-saver';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { TiLink } from 'react-icons/ti';
import InvoiceView from './InvoiceView';

//const InvoicePayment = React.lazy(() => import('./InvoicePayment'));
//let log = new JsLogger;

class Transaction extends Component {

    state = {
        checkedBankId: [],
        checkAll: false,
        open: false,
        currentId: null,
        fundingTransactions: [],
        banks: [],
        transactionLoader: false,
        bankLoader: false,
        errorMessageBank: '',
        errorMessageTransaction: '',
        activePage: 1,
        totalCount: 0,
        itemPerPage: 250,
        searchKey: '',
        downloadLink: '',
        fileLoader: false,
        errorFileDownloading: null,
        fromDate: '',
        fromDateUI: '',
        toDate: '',
        toDateUI: '',
        modalType: '',
        sixPrevMonthDate: '',
        dateError: null,
        statdateErr: null,
        key: 'fundingdebit',
        bankName: [{ id: 0, bankName: 'loading...' }],
        selectedBusinessName: '',
        bankSelection: true,
        selectedBankName: '',
        traceNoSearchKey: '',
        selectDateConfirm: false,
        selectPaymentDate: '',
        loading: false,
        SinglePendingAmount: '',
        SinglePendingtrace: '',
        SinglePendingUserId: '',
        modaltype: '',
        invDetailsProps: {},
        direction: '',
        propName: '',
        propOrder: true,
        showConfirMmsg: false,
        showActionModal: false,
        vendorBusinessId: '',
        invoiceNo: '',
        trasaction: {},
        traceNumber: null,
        cpbusinesslist: [],
        ProcessingBank: '',
        lastTrasactionType:this.props.type
        //showSuccessModal: false
    }

    _isMounted = false;

    showActionModal = () => {
        this.setState({
            showActionModal: true,
        });
    }

    hideActionModal = () => {
        this.setState({
            showActionModal: false
        });
    }

    handleConfirmReviewClose = () => {
        this.setState({
            showConfirMmsg: false,
        });
    };
    handleConfirmReviewShow = () => {
        this.setState({ showConfirMmsg: true });
    };

    selectPaymentDateOnchange = date => {


        this.setState(
            {
                statdateErr: null,
                selectPaymentDate: date
            },
            () => {

                console.log('====>', moment(this.state.selectPaymentDate).format('YYYY-MM-DD'));
            }
        );

    }

    handlePageChange = (pageNumber) => {
        this.setState({ activePage: pageNumber });
        this.getFundingDebitTransaction(pageNumber > 0 ? pageNumber - 1 : 0);
    }

    handleChangeItemPerPage = (e) => {
        this.setState({ itemPerPage: e.target.value },
            () => {
                this.getFundingDebitTransaction(this.state.activePage > 0 ? this.state.activePage - 1 : 0);
            });
    }

    handleSuccessClose = () => {
        this.setState({ showSuccessModal: false });
        this.handelInitialDate();
    };

    handleSuccessShow = (type) => {
        console.log(type);
        this.setState({ modalType: type, showSuccessModal: true });
    };

    handleChangeDate = (date, type) => {
        if (date != null) {
            let day = date.getDate();
            let month = date.getMonth() + +1;
            let year = date.getFullYear();
            let fullDate = day + '/' + month + '/' + year;
            if (type === 'from') {
                this.setState({
                    fromDateUI: date,
                    fromDate: fullDate
                });
            }
            else {
                this.setState({
                    toDateUI: date,
                    toDate: fullDate
                });
            }
        }
        else {
            if (type === 'from') {
                this.setState({
                    fromDateUI: '',
                    fromDate: ''
                });
            }
            else {
                this.setState({
                    toDateUI: '',
                    toDate: ''
                });
            }
        }
    }

    handelInitialDate = () => {
        let currentDate = new Date();
        let prevMonthDate = new Date(currentDate.setDate(currentDate.getDate() - 30));

        let cDate = new Date();
        let sixPrevMonthDate = new Date(cDate.setDate(cDate.getDate() - 180));

        this.setState({
            fromDateUI: prevMonthDate,
            sixPrevMonthDate: sixPrevMonthDate
        });
        this.handleChangeDate(prevMonthDate, 'from');
        this.handleChangeDate(new Date(), 'to');


    }


    subMenuOpen = (passId) => {
        let currentId = passId;
        this.setState({
            open: true,
            currentId
        });
    }

    subMenuClose = () => {
        this.setState({
            open: false,
            currentId: null
        });
    }

    handelError = (err) => {
        let errorMessage = '';
        try {
            errorMessage = err.data.message ? err.data.message : err.data.error_description;
        } catch (err) {
            errorMessage = 'No records found.';
        }
        return errorMessage;
    }

    searchTextHandel = (e) => {
        this.setState({ searchKey: e.target.value });
    }

    resetSearch = () => {
        //this.setState({ searchKey: '' });
        //this.getFundingDebitTransaction();
        this.setState({
            selectedBusinessName: '',
            selectedBankName: '',
            traceNoSearchKey: '',
            bankSelection: true,
            ProcessingBank: ''


        }, () => {
            this.getFundingDebitTransaction();
        });

    }

    handelSearch = () => {
        this.getFundingDebitTransaction();
    }




    getFundingDebitTransaction = (
        since = 0,
        //searchKey = ''
    ) => {
        this.setState({ transactionLoader: true }, () => {
            axios.get(
                `transaction/getFundingDebit?businessId=${this.state.selectedBusinessName}&bankId=${this.state.selectedBankName}&since=${since}&limit=${this.state.itemPerPage}&key=${this.state.traceNoSearchKey}&processingBank=${this.state.ProcessingBank}&type=${this.props.type}`
            )

                .then(res => {
                    console.log('getFundingDebit response', res);
                    const totalCount = res.data.total;
                    if (this._isMounted) {
                        this.setState({
                            fundingTransactions: res.data,
                            totalCount: totalCount,
                            transactionLoader: false,
                            traceNumber: res.data.traceNumber
                        });
                    }

                    console.log('debit tranction', this.state.fundingTransactions);
                })
                .catch(err => {
                    let errorMessage = this.handelError(err);
                    this.setState({
                        transactionLoader: false, errorMessageTransaction: errorMessage
                    });
                });
        });
    }

    

    businessOnchange = (e) => {
        //console.log('e.target.value', e.target.value);
        this.getcpforbusiness(e.target.value);
        this.setState(
            {
                selectedBusinessName: e.target.value,
                bankSelection: true,
                bankName: [{ id: 0, bankName: 'loading...' }],
            },
            () => {

                console.log('selectedBusinessName Id', this.state.selectedBusinessName);
                axios.get(`transaction/bank/${this.state.selectedBusinessName}`)
                    .then(res => {
                        console.log('res after selecting business banks recive', res, res.data.length);
                        if (res.data.entries.length > 0) {
                            if (this._isMounted) {
                                this.setState({
                                    bankName: res.data.entries,
                                    bankSelection: false
                                }, () => {
                                    console.log('this.state.bankName----- transaction', this.state.bankName);
                                });
                            }
                        }


                    })
                    .catch(err => {
                        console.log('error', err);
                    });
            }



        );
    }

    bankOnchange = (e) => {
        this.setState(
            { selectedBankName: e.target.value },
            () => {
                console.log('selectedBankName', this.state.selectedBankName);
            }
        );
    }

    keyPress = (e) => {
        /*
        if (e.keyCode == 13) {
            console.log('Entered trace no', e.target.value);
            this.setState(
                {
                    traceNoSearchKey: e.target.value
                },
                ()=>{
                    this.getFundingDebitTransaction();
                }
            );
            
                
        }
        */

        console.log('Entered trace no', e.target.value);
        this.setState(
            {
                traceNoSearchKey: e.target.value.trim()
            }
        );

    }


    componentDidMount() {
        console.log('+++++++++++ transaction mount',this.props.type);
        this._isMounted = true;
        //this.getBank();
        this.getFundingDebitTransaction();
        this.handelInitialDate();

    }

    componentDidUpdate(prevProps){
        // console.log('transaction update ++++++++++++++', this.props);
        console.log('transaction update prevProps.type++++++++++++++', prevProps.type);
        console.log('transaction update this.props.type++++++++++++++', this.props.type);
        if (prevProps.type != this.props.type){
            this.getFundingDebitTransaction();
            this.setState({
                lastTrasactionType: this.props.type
            }, () => { console.log('last transaction type ++++++++++++++++++++', this.state.lastTrasactionType);});
        }
        
    }


    componentWillUnmount() {
        this._isMounted = false;
    }


    handleDateModalOpen = () => {
        this.setState({
            selectDateConfirm: true
        });
    }
    handleDateModalClose = () => {
        this.setState({
            selectDateConfirm: false,
            selectPaymentDate: '',
            loading: false,
            dateError: null,
            statdateErr: null
        });
    }



    pendingAction = (data) => {
        console.log('got Funding debit data', data);
        this.setState({
            selectDateConfirm: true,
            SinglePendingAmount: data.amount,
            SinglePendingtrace: data.traceNumber,
            SinglePendingUserId: data.transactionRequests[0].userId
        }, () => {

        });
    }


    pendingConfirm = () => {

        if (this.state.selectPaymentDate == '') {
            this.setState({
                statdateErr: 'Please select a date'
            });
        }

        else {

            let val = {
                'traceNumber': this.state.SinglePendingtrace,
                'amount': this.state.SinglePendingAmount,
                'paymentDate': moment(this.state.selectPaymentDate).format('YYYY-MM-DD')
            };
            //console.log('pendingConfirm working', val);

            this.setState({
                loading: true,
            }, () => {
                // axios.put(`achPayment/confirm/${this.state.SinglePendingUserId}`, val)
                axios.put('achPayment/confirm', val)
                    .then(res => {
                        console.log(res.data);
                        this.getFundingDebitTransaction();
                        this.setState({
                            loading: false,
                            showConfirMmsg: true
                        }, () => {
                            this.handleDateModalClose();
                        });
                    })
                    .catch(err => {
                        let errorMessage = '';
                        try {
                            errorMessage = err.data.message ? err.data.message : err.data.error_description;
                        } catch (err) {
                            errorMessage = 'No records found.';
                        }
                        this.setState({
                            loading: false, errorMsg: errorMessage
                        });
                        setTimeout(() => {
                            this.setState({ errorMsg: null });
                        }, 2500);
                    });
            });

        }


    }


    handleSuccessShow = (type) => {
        console.log(type);
        this.setState({ modalType: type, showSuccessModal: true });
    };

    handleSuccessClose = () => {
        this.setState({ showSuccessModal: false });
        this.handelInitialDate();
    };




    handleChangeDate = (date, type) => {
        if (date != null) {
            let day = date.getDate();
            let month = date.getMonth() + +1;
            let year = date.getFullYear();
            let fullDate = day + '/' + month + '/' + year;
            if (type === 'from') {
                this.setState({
                    fromDateUI: date,
                    fromDate: fullDate
                });
            }
            else {
                this.setState({
                    toDateUI: date,
                    toDate: fullDate
                });
            }
        }
        else {
            if (type === 'from') {
                this.setState({
                    fromDateUI: '',
                    fromDate: ''
                });
            }
            else {
                this.setState({
                    toDateUI: '',
                    toDate: ''
                });
            }
        }
    }

    handelGotoINvoice = (param, id, obj, traceNumber) => {
        obj['type'] = this.props.type;
        console.log('param, id, obj, traceNumber---------------------', param, id, obj, traceNumber);
        //this.props.onClickAction(param);
        //this.props.history.push('/dashboard/invoice');
        

        this.setState({
            showActionModal: true,
            vendorBusinessId: id,
            invoiceNo: param,
            trasaction: obj,
            traceNumber
        }
        // , () => this.props.onClickAction(this.state.vendorBusinessId)
        );

    }

    getFundingDebitCSV = () => {

        let d = new Date();
        let n = (d.getTime()) / 1000;
        let t = Math.floor(n);

        //console.log('||||', this.state.fromDate, '||||', this.state.fromDateUI, '||||', this.state.toDate);
        let since = 0;
        if (this.state.fromDate != '' && this.state.fromDateUI != '' && this.state.toDate != '' && this.state.toDateUI != '') {
            this.setState({ fileLoader: true }, () => {
                axios.get(`/transaction/fundingTransaction/download?fromDate=${this.state.fromDate}&toDate=${this.state.toDate}&businessId=${this.state.selectedBusinessName}&bankId=${this.state.selectedBankName}&since=${since}&limit=${this.state.itemPerPage}&key=${this.state.traceNoSearchKey}&type=${this.props.type}`)
                    .then(response => {


                        this.setState({
                            fileLoader: false
                        });

                        let blob = new window.Blob([response.data], { type: 'text/plain;charset=utf-8' });
                        saveAs(blob, `Funding Debit(Transaction)-list-${t}.csv`);
                        this.handleSuccessShow('success');
                    })
                    .catch(err => {
                        let errorMessage = this.handelError(err);
                        this.setState({
                            fileLoader: false, errorFileDownloading: errorMessage
                        });
                        setTimeout(() => {
                            this.setState({ errorFileDownloading: null });
                        }, 5000);
                    });
            });
        }
        else {
            this.setState({
                dateError: 'Please enter valid date.'
            });
            setTimeout(() => {
                this.setState({
                    dateError: ''
                });
            }, 2500);

        }


    }
    ProcessingBankOnchange = (e) => {
        console.log(e.target.value);
        this.setState({
            ProcessingBank: e.target.value
        });
       

    }
    getcpforbusiness = (businessId) => {
        console.log('businessId', businessId);
        this.setState({
            ProcessingBank: ''
        }, () => {

            axios
                .get(
                    `/centralPayment/${businessId}`

                )
                .then(res => {
                    console.log('getcpforbusiness', res);
                    this.setState({
                        cpbusinesslist: res.data
                    });

                })
                .catch(e => {
                    console.log(e);
                });
        });



    }

    render() {
        const {
            open,
            fundingTransactions,

        } = this.state;
        const { type }=this.props;

        return (
            <div className="">

                <Tabs
                    id="controlled-tab-example"
                    activeKey={this.state.key}
                    onSelect={key => this.setState({ key })}
                    className="cusTabControl px-3 mainMenuTab"
                >
                    <Tab eventKey="fundingdebit" title="Funding Debit">

                        <div className="transactionHistory fundingDebit px-3 pb-3 bg-light">
                            <div className="row py-4">
                                <div className="col-sm-12">
                                    <form className="form-inline float-left">
                                        <select className="form-control businessDropdown col-2 truncate pr-35" onChange={this.businessOnchange} value={this.state.selectedBusinessName}>
                                            <option value=''> Select a Business Name </option>
                                            <Consumer>
                                                {(value) => <BusinessName businessName={value.businessdata[0]} />}
                                            </Consumer>
                                        </select>
                                        <select className="form-control businessDropdown col-2 truncate pr-35 ml-2" onChange={this.ProcessingBankOnchange} value={this.state.ProcessingBank}>
                                            <option value=''> Select Processing Bank </option>
                                            {
                                                this.state.cpbusinesslist.map(cpbusiness => (
                                                    <option key={cpbusiness.bankOption}
                                                        value={cpbusiness.bankOption}
                                                    >
                                                        {cpbusiness.bankOption}
                                                    </option>
                                                )
                                                )
                                            }
                                        </select>

                                        <select className="form-control bankDropDown truncate pr-35 ml-2" onChange={this.bankOnchange} value={this.state.selectedBankName} disabled={this.state.bankSelection}>
                                            <option value=''> Select Customer Bank </option>
                                            {
                                                this.state.bankName.map(i => (
                                                    <React.Fragment key={i.id} >
                                                        <option value={i.id}> {i.bankName}</option>
                                                    </React.Fragment>
                                                )
                                                )
                                            }
                                        </select>

                                        <input
                                            placeholder='Enter Trace Number'
                                            value={this.state.traceNoSearchKey}
                                            className='form-control float-right ml-2 invoiceIco traceNo'
                                            onChange={this.keyPress}
                                        //onKeyDown={this.keyPress}
                                        />

                                        <button
                                            type="button"
                                            className="btn ml-2 search-btn text-black"
                                            onClick={() => this.handelSearch()}
                                        >
                                            Search
                                        </button>

                                        <Link to="#" className='ml-2'>
                                            <Image src={refreshIcon} onClick={() => this.resetSearch()} />
                                        </Link>

                                    </form>

                                    <Button type="button" className="btn btn-dark float-right" onClick={() => this.handleSuccessShow('download')}><FaDownload /> Download</Button>

                                </div>

                            </div>



                            <div className="transactionInner">

                                <div className="faSearchPanel topSearchInvoice">

                                </div>

                                <div className="boxBg">
                                    <Row className="show-grid">
                                        <Col sm={12} md={12}>
                                            <Table responsive hover>
                                                <thead className="theaderBg">
                                                    <tr>
                                                        <th className="text-left" width="100">Status</th>
                                                        <th>Business Name</th>
                                                        <th>Transaction Date</th>
                                                        <th>Payment Date</th>
                                                        <th className="text-right">Withdrawals</th>
                                                        <th className="pl-4">Processing Bank</th>
                                                        <th width=''></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {
                                                        this.state.transactionLoader ?
                                                            <tr>
                                                                <td colSpan={7} style={{ textAlign: 'center' }}>
                                                                    <LoadingSpinner />
                                                                </td>
                                                            </tr>
                                                            :
                                                            fundingTransactions.entries.length > 0 ?
                                                                fundingTransactions.entries.map((data, k) => (
                                                                    <Fragment key={k}>
                                                                        <tr key={'data' + k} id={'data' + k}>
                                                                            <td className="text-left">{data.isProcessed && data.isProcessed === true ? <div className="text-primary complete">Complete</div> : <span onClick={() => this.pendingAction(data)} className="border-0 statusInfo pending">Pending</span>}</td>
                                                                            <td>{data.businessName}</td>
                                                                            <td>{data.transactionDate ? data.transactionDate : null}</td>
                                                                            <td>{data.paymentDate ? data.paymentDate : null}</td>
                                                                            <td className="text-right">
                                                                                <NumberFormat
                                                                                    value={data.amount}
                                                                                    displayType={'text'}
                                                                                    thousandSeparator={true}
                                                                                    fractionSize={2}
                                                                                    prefix={'$'}
                                                                                    decimalScale={2}
                                                                                    fixedDecimalScale={true}
                                                                                    thousandsGroupStyle={'thousand'}
                                                                                    renderText={value => <span>{value}</span>}
                                                                                />
                                                                            </td>
                                                                            <td className="pl-4">{data.processingBank}</td>
                                                                            {/* <td width='4%'></td> */}
                                                                            <td width='' className="text-right">
                                                                                {
                                                                                    data.transactionRequests.length > 0 ?
                                                                                        this.state.currentId !== 'subData' + k ?
                                                                                            <Button
                                                                                                onClick={() => this.subMenuOpen('subData' + k)}
                                                                                                aria-controls="collapse-area"
                                                                                                aria-expanded={open}
                                                                                                className="btnTranShowHide"
                                                                                                id={'data' + k}
                                                                                            >

                                                                                                <span className='glyphicon glyphicon-resize-full' aria-hidden="true"><FiMaximize2 /></span>
                                                                                            </Button>
                                                                                            :
                                                                                            <Button
                                                                                                onClick={() => this.subMenuClose()}
                                                                                                aria-controls="collapse-area"
                                                                                                aria-expanded={open}
                                                                                                className="btnTranShowHide"
                                                                                                id={'data' + k}
                                                                                            >
                                                                                                <span className='glyphicon glyphicon glyphicon-remove' aria-hidden="true"><FiX /></span>
                                                                                            </Button>
                                                                                        :
                                                                                        null
                                                                                }

                                                                            </td>
                                                                        </tr>

                                                                        <tr key={'subData' + k} id={'subData' + k} className="transactionDetailsRow">
                                                                            <td colSpan='8' style={{ padding: '0' }}>
                                                                                <Collapse in={this.state.currentId === 'subData' + k ? this.state.open : null}>
                                                                                    <div className="transactionDetails">
                                                                                        <Table responsive hover>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <th className="fundingDebitHeader">Funding Debit</th>
                                                                                                    {/* <th className="text-left">Status</th> */}
                                                                                                    <th>Invoice No.</th>
                                                                                                    <th>Description</th>
                                                                                                    <th>{type == 'AR' ? 'Customer Name' : 'Vendor Name'}</th>
                                                                                                    <th className="text-right">$ Amount</th>
                                                                                                    <th className="text-right">Reference</th>
                                                                                                    <th className="text-right">Date</th>
                                                                                                    <th width="50"></th>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                            <tbody>
                                                                                                {
                                                                                                    data.transactionRequests.map((transactions, k) => (
                                                                                                        <tr key={k}>
                                                                                                            {
                                                                                                                k === 0 ?
                                                                                                                    <td rowSpan={data.transactionRequests.length} className="fundingDebitCol">
                                                                                                                        <div className="tracnoSec">
                                                                                                                            <h2>Trace Number:</h2>
                                                                                                                            <h3>{data.traceNumber}</h3>
                                                                                                                        </div>
                                                                                                                        <div className="debitSec">
                                                                                                                            <h2>Debited Amount:</h2>
                                                                                                                            <h4>
                                                                                                                                <NumberFormat
                                                                                                                                    value={data.amount}
                                                                                                                                    displayType={'text'}
                                                                                                                                    thousandSeparator={true}
                                                                                                                                    fractionSize={2}
                                                                                                                                    prefix={'$'}
                                                                                                                                    decimalScale={2}
                                                                                                                                    fixedDecimalScale={true}
                                                                                                                                    thousandsGroupStyle={'lakh'}
                                                                                                                                    renderText={value => <span>{value}</span>}
                                                                                                                                />
                                                                                                                            </h4>
                                                                                                                        </div>
                                                                                                                    </td>
                                                                                                                    :
                                                                                                                    null
                                                                                                            }
                                                                                                            {/* <td className="text-left">{transactions.status === true ? <div className="text-primary complete">Complete</div> : <span className="pending">Pending</span>}</td> */}
                                                                                                            {transactions.transactionType === 'TRAN_CHRG_CHECKFILE' || transactions.transactionType === 'TRAN_CHRG_SUBS'
                                                                                                                || transactions.transactionType === 'WEX_DEBIT_CHECK' ? (
                                                                                                                    <td>  {transactions.invoiceNumber}</td>

                                                                                                                ) : (
                                                                                                                    <td onClick={() => this.handelGotoINvoice(transactions.description, transactions.vendorBusinessId, transactions, data.traceNumber)} className="pointer"><TiLink /> {transactions.invoiceNumber}</td>

                                                                                                                )

                                                                                                            }
                                                                                                            <td>{transactions.description}</td>
                                                                                                            <td>{transactions.customerVendorName}</td>
                                                                                                            <td className="text-right">
                                                                                                                <NumberFormat
                                                                                                                    value={transactions.amount}
                                                                                                                    displayType={'text'}
                                                                                                                    thousandSeparator={true}
                                                                                                                    fractionSize={2}
                                                                                                                    prefix={'$'}
                                                                                                                    decimalScale={2}
                                                                                                                    fixedDecimalScale={true}
                                                                                                                    thousandsGroupStyle={'lakh'}
                                                                                                                    renderText={value => <span>{value}</span>}
                                                                                                                />
                                                                                                            </td>
                                                                                                            <td className="text-right">{transactions.referenceNo}</td>
                                                                                                            <td className="text-right">{transactions.transactionDate}</td>
                                                                                                            <td width="50"></td>
                                                                                                        </tr>
                                                                                                    ))
                                                                                                }
                                                                                            </tbody>
                                                                                        </Table>
                                                                                    </div>
                                                                                </Collapse>

                                                                            </td>
                                                                        </tr>

                                                                    </Fragment>
                                                                ))
                                                                :
                                                                this.state.errorMessageTransaction ?
                                                                    (
                                                                        <tr>
                                                                            <td colSpan={7} style={{ textAlign: 'center' }}>
                                                                                {this.state.errorMessageTransaction}
                                                                            </td>
                                                                        </tr>
                                                                    )
                                                                    :
                                                                    (
                                                                        <tr>
                                                                            <td colSpan={7} style={{ textAlign: 'center' }}>
                                                                                No records found.
                                                                            </td>
                                                                        </tr>
                                                                    )
                                                    }
                                                </tbody>
                                            </Table>
                                        </Col>
                                    </Row>
                                    {this.state.totalCount ? (
                                        <Row className="px-3">
                                            <Col md={4} className="d-flex flex-row mt-20">
                                                <span className="mr-2 mt-2 font-weight-500">Items per page</span>
                                                <select
                                                    id={this.state.itemPerPage}
                                                    className="form-control truncatefloat-left w-90"
                                                    onChange={this.handleChangeItemPerPage}
                                                    value={this.state.itemPerPage}>
                                                    <option value='50'>50</option>
                                                    <option value='100'>100</option>
                                                    <option value='150'>150</option>
                                                    <option value='200'>200</option>
                                                    <option value='250'>250</option>

                                                </select>
                                            </Col>
                                            <Col md={8}>
                                                <div className="paginationOuter text-right">
                                                    <Pagination
                                                        activePage={this.state.activePage}
                                                        itemsCountPerPage={this.state.itemPerPage}
                                                        totalItemsCount={this.state.totalCount}
                                                        onChange={this.handlePageChange}
                                                    />
                                                </div>
                                            </Col>
                                        </Row>
                                    ) : null}
                                </div>

                            </div>

                            {/* Download success modal */}

                            <Modal
                                show={this.state.showSuccessModal}
                                onHide={this.handleSuccessClose}
                                className="payOptionPop"
                            >
                                <Modal.Body>
                                    {this.state.modalType === 'success' ?
                                        <Fragment>
                                            <Row>
                                                <Col md="12" className="text-center">
                                                    <Image src={SuccessIco} />
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md="12" className="text-center">
                                                    <h5>
                                                        Successfully downloaded.
                                                    </h5>
                                                </Col>
                                            </Row>
                                        </Fragment>
                                        :
                                        <Fragment>
                                            <Row className="text-center">
                                                <div className="text-center mb-3 w-100">
                                                    <h3 className='pl-3'>
                                                        Please select a transaction period
                                                    </h3>
                                                </div>


                                            </Row>
                                            <br />
                                            <Row>
                                                <Col sm={12} md={12}>
                                                    {
                                                        this.state.fileLoader ?
                                                            <LoadingSpinner />
                                                            :
                                                            this.state.errorFileDownloading ?
                                                                <div className="alert alert-danger">
                                                                    {this.state.errorFileDownloading}
                                                                </div>
                                                                : null
                                                    }
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md="6" sm="12">
                                                    <h5>From date</h5>
                                                    <DatePicker
                                                        selected={this.state.fromDateUI}
                                                        value={this.state.fromDateUI}
                                                        onChange={(value) => this.handleChangeDate(value, 'from')}
                                                        minDate={this.state.sixPrevMonthDate}
                                                        maxDate={new Date()}
                                                    />
                                                </Col>
                                                <Col md="6" sm="12">
                                                    <h5>To date</h5>
                                                    <DatePicker
                                                        selected={this.state.toDateUI}
                                                        value={this.state.toDateUI}
                                                        onChange={(value) => this.handleChangeDate(value, 'to')}
                                                        minDate={this.state.sixPrevMonthDate}
                                                        maxDate={new Date()}
                                                    />
                                                </Col>
                                            </Row>
                                        </Fragment>
                                    }
                                </Modal.Body>
                                <div className="my-3 text-center">
                                    <Button
                                        onClick={this.handleSuccessClose}
                                        className="btn btn-darkBlue mr-3"
                                    >
                                        Return
                                    </Button>
                                    {this.state.modalType != 'success' ?
                                        <Button
                                            onClick={this.getFundingDebitCSV}
                                            className="btn btn-primary"
                                        >
                                            Download
                                        </Button>
                                        : null}
                                </div>
                            </Modal>

                            {/* ========== choose date modal =============== */}

                            <Modal
                                show={this.state.selectDateConfirm}
                                onHide={this.handleDateModalClose}
                            >

                                <Modal.Body>
                                    <div className="text-center mb-3">
                                        <h5>Select Payment Date</h5>
                                    </div>
                                    {this.state.errorMsg ? <div className="alert alert-danger">{this.state.errorMsg}</div> : null}
                                    {/* <Row>
                            <Col md={12} className="text-center"> */}


                                    {this.state.loading ? <LoadingSpinner /> : <DatePicker
                                        onChange={this.selectPaymentDateOnchange}
                                        value={this.state.selectPaymentDate}
                                        className="selectPaymentDateCustom"
                                    />}

                                    {this.state.statdateErr ? <div className='text-danger'>{this.state.statdateErr}</div> : null}
                                    {/* </Col>
                        </Row> */}

                                    {/* <Moment format="YYYY/MM/DD">{this.state.selectPaymentDate}</Moment> */}
                                    <div className="my-3 text-center">
                                        <Button
                                            onClick={this.handleDateModalClose}
                                            className="btn btn-darkBlue mr-3"
                                        >
                                            Return
                                        </Button>
                                        <Button
                                            onClick={this.pendingConfirm}
                                            className="btn btn-primary"
                                        >
                                            Confirm
                                        </Button>

                                    </div>
                                </Modal.Body>

                            </Modal>


                        </div>


                    </Tab>

                    <Tab eventKey="invoicepayment" title="Invoice Payment">
                        <div className="invoicepayment">
                            {this.state.key === 'invoicepayment' ? <InvoicePayment  {...this.props} /> : null}
                        </div>
                    </Tab>
                    <Tab eventKey="quickbooks" title="QB Failed Sync">
                        <div className="quickbooks">
                            {this.state.key === 'quickbooks' ? <QuickbooksOnline {...this.props} businessNames={this.state.businessName} /> : null}
                        </div>
                    </Tab>     
                </Tabs>

                {/* status changing successfull massage */}
                <Modal
                    show={this.state.showConfirMmsg}
                    onHide={this.handleConfirmReviewClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Funding debit payment has been successfully confirmed</h5>
                            </Col>
                        </Row>
                        <div className="text-center">
                            <Button
                                onClick={this.handleConfirmReviewClose}
                                className="but-gray"
                            >
                                Done
                            </Button>
                        </div>

                    </Modal.Body>
                </Modal>


                {/* invoice view*/}
                <Modal
                    show={this.state.showActionModal}
                    onHide={this.hideActionModal}
                    className="right full noPadding slideModal"
                >
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body>
                        <InvoiceView
                            id={this.state.vendorBusinessId}
                            searchKey={this.state.invoiceNo}
                            transactionObj={this.state.trasaction}
                            traceNumber={this.state.traceNumber}
                            transactionType={this.state.lastTrasactionType}
                        />
                    </Modal.Body>

                </Modal>

            </div>



        );
    }
}

Transaction.propTypes = {
    // onClickAction: PropTypes.func,
    history: PropTypes.object,
    type: PropTypes.string,

};

const mapStateToProps = state => {
    return {
        globalState: state
    };
};

// const mapDispatchToProps = dispatch => {
//     return {
//         onClickAction: data => dispatch(invoiceDescription(data)),
//     };
// };



export default connect(mapStateToProps)(Transaction);
