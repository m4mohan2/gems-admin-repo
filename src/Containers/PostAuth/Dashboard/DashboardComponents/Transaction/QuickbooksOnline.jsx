/* eslint-disable no-unused-vars */
import React,{Component} from 'react';
import axios from './../../../../../shared/eaxios';
import LoadingSpinner from './../../../../../Components/LoadingSpinner/LoadingSpinner';
import Pagination from 'react-js-pagination';
import BusinessName from './../../../../../Components/Dashboard/BusinessName';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import refreshIcon from './../../../../../assets/refreshIcon.png';
import {
    Consumer
} from './../../../../../Context/Context';
import {
    Image, 
    Button,
    Row, 
    Col
} from 'react-bootstrap';
class QuickbooksOnline extends Component {

    state = {
        qbOnlineDataLoader:false,
        qbonlinedata:[],
        activePage: 1,
        totalCount: 0,
        itemPerPage: 250,
        searchKey: '',
        qbError:null,
        UpdateQBaccountingErr:null,
        UpdateQBaccountingLoader:false,
        selectedBusinessName:'',
        updateID:null

    }

    _isMounted = false;

    componentDidMount(){
        //_isMounted = true;
        this.getqbOnlineData();

    }

    componentDidUpdate(prevProps) {

        if (prevProps.type != this.props.type) {
            this.getqbOnlineData();
        }

    }
    
    handlePageChange = (pageNumber) => {
        this.setState({ activePage: pageNumber });
        this.getqbOnlineData(pageNumber > 0 ? pageNumber - 1 : 0);
    }

    handleChangeItemPerPage = (e) => {
        this.setState({ itemPerPage: e.target.value },
            () => {
                this.getqbOnlineData(this.state.activePage > 0 ? this.state.activePage - 1 : 0);
            });
    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }

        return errorMessge;
    }

    keyPress = (e) => {
      
        this.setState(
            {
                searchKey: e.target.value.trim()
            }
        );

    }

    businessOnchange = (e) => {
        this.setState(
            {
                selectedBusinessName: e.target.value,
            },()=>{
                this.getqbOnlineData();
            });
    }
    
    resetSearch = () => {
        
        this.setState({
            selectedBusinessName: '',
            searchKey:''
        }, () => {
            this.getqbOnlineData();
        });

    }

    handelSearch = () => {
        this.getqbOnlineData();
    }
        
    getqbOnlineData = (
        since = 0
    ) => {
        this.setState({
            qbOnlineDataLoader: true
        },()=>{
            axios.get(
                `transaction/accountingTransaction?businessId=${this.state.selectedBusinessName}&key=${this.state.searchKey}&since=${since}&limit=${this.state.itemPerPage}&type=${this.props.type}`
            )
                .then(res => {
                    console.log('getFundingDebit response', res);

                    this.setState({
                        qbonlinedata: res.data.entries,
                        totalCount: res.data.total,
                        qbOnlineDataLoader:false
                    });
                    
                })
                .catch(err=>{
                    console.log(err);
                    let qbError = this.displayError(err);
                    this.setState({
                        qbError,
                        qbOnlineDataLoader: false
                    });

                });


                    
        });

    };

    UpdateQBaccountingTransac=(id)=>{
        this.setState({
            UpdateQBaccountingLoader:true,
            updateID: id
        }, () => axios.put(`transaction/update/accountingTransaction/${id}`)
            .then(() => {
                this.getqbOnlineData();
                this.setState({
                    UpdateQBaccountingLoader: false,
                    updateID:null
                });
            })
            .catch(err => {
                console.log(err);
                let UpdateQBaccountingErr = this.displayError(err);
                this.setState({
                    UpdateQBaccountingErr,
                    UpdateQBaccountingLoader: false,
                    updateID:null
                });

            })
        );
        
    }
    
    render(){
        const {
            qbOnlineDataLoader,
            qbonlinedata,
            qbError,
            UpdateQBaccountingLoader,
            updateID

        } = this.state;
        const { businessNames} =this.props;

        return (
            <div>
                {   qbOnlineDataLoader 
                    ? 
                    <LoadingSpinner/> 
                    :
                    <div className="p-3">
                     

                        <div className="row pb-3">
                            <div className="col-sm-12">
                                <form className="form-inline float-left">
                                    <select className="form-control businessDropdown col-2 truncate pr-35" onChange={this.businessOnchange} value={this.state.selectedBusinessName}>
                                        <option value=''> Select a Business Name </option>
                                        <Consumer>
                                            {(value) => <BusinessName businessName={value.businessdata[0]} />}
                                        </Consumer>
                                    </select>

                                    <input
                                        placeholder='Enter Vendor Name'
                                        value={this.state.searchKey}
                                        className='form-control float-right ml-2 invoiceIco traceNo'
                                        onChange={this.keyPress}
                                    />

                                    <button
                                        type="button"
                                        className="btn ml-2 search-btn text-black"
                                        onClick={() => this.handelSearch()}
                                    >
                                        Search
                                    </button>

                                    <Link to="#" className='ml-2'>
                                        <Image src={refreshIcon} onClick={() => this.resetSearch()} />
                                    </Link>
                                </form>
                            </div>
                        </div>

                        <div className="boxBg">
                            <div className="show-grid row">
                                <div className="col-md-12 col-sm-12">
                                    <table className="table table-hover">
                                        <thead className="theaderBg">
                                            <tr>
                                                <th className="text-left" width="100">Processing Status</th>
                                                <th className="text-left" width="100">Business Name</th>
                                                <th className="text-left" width="100">Vendor Name</th>
                                                <th className="text-left" width="100">Payment Method</th>
                                                <th className="text-left" width="100">Invoice Number</th>
                                                <th className="text-left" width="100">Transaction Date</th>
                                                <th className="text-right" width="50">&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                qbonlinedata.length > 0
                                                    ?
                                                    qbonlinedata.map(qb => (
                                                        <tr key={qb.id}>
                                                            <td className={'font-weight-normal ' + (qb.processingStatus === 'STOP' && 'text-warning')}>{qb.processingStatus}</td>
                                                            <td>{qb.businessName}</td>
                                                            <td>{qb.customerVendorName}</td>
                                                            <td>{qb.paymentMethod.replace(/_/g,' ')}</td>
                                                            <td>{qb.invoiceNumber}</td>
                                                            <td>{qb.transactionDate}</td>
                                                            <td className="text-right">
                                                                <Button 
                                                                    variant="primary" 
                                                                    size="sm"
                                                                    className="btn-cus-update"
                                                                    onClick={() => this.UpdateQBaccountingTransac(qb.id)}
                                                                >
                                                                    {UpdateQBaccountingLoader && (updateID === qb.id) ? 'Syncing...' :'ReSync'}
                                                                </Button>
                                                            </td>
                                                        </tr>

                                                    ))
                                                    : qbError ?
                                                        (
                                                            <tr>
                                                                <td colSpan={7} style={{ textAlign: 'center' }}>
                                                                    {qbError}
                                                                </td>
                                                            </tr>
                                                        )
                                                        :
                                                        (
                                                            <tr>
                                                                <td colSpan={7} style={{ textAlign: 'center' }}>
                                                                    No records found.
                                                                </td>
                                                            </tr>
                                                        )
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        {this.state.totalCount ? (
                            <Row className="px-3">
                                <Col md={4} className="d-flex flex-row mt-20">
                                    <span className="mr-2 mt-2 font-weight-500">Items per page</span>
                                    <select
                                        id={this.state.itemPerPage}
                                        className="form-control truncatefloat-left w-90"
                                        onChange={this.handleChangeItemPerPage}
                                        value={this.state.itemPerPage}>
                                        <option value='50'>50</option>
                                        <option value='100'>100</option>
                                        <option value='150'>150</option>
                                        <option value='200'>200</option>
                                        <option value='250'>250</option>

                                    </select>
                                </Col>
                                <Col md={8}>
                                    <div className="paginationOuter text-right">
                                        <Pagination
                                            activePage={this.state.activePage}
                                            itemsCountPerPage={this.state.itemPerPage}
                                            totalItemsCount={this.state.totalCount}
                                            onChange={this.handlePageChange}
                                        />
                                    </div>
                                </Col>
                            </Row>
                        ) : null}                
                    </div>
                }
            </div>
        );
    }
        
    

}

QuickbooksOnline.propTypes ={
    businessNames: PropTypes.object,
    type: PropTypes.string,

};
export default QuickbooksOnline;
