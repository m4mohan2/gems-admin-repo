/* eslint-disable no-unused-vars */
import React, {
    Component,
    //Fragment
} from 'react';
import axios from '../../../../../shared/eaxios';
import PropTypes from 'prop-types';
import './InvoiceView.scss';
import NumberFormat from 'react-number-format';

import {
    //Button, 
    //Col, 
    //Row, 
    //Table, 
    Collapse,
    //Modal, 
    //Image 
} from 'react-bootstrap';
import LoadingSpinner from './../../../../../Components/LoadingSpinner/LoadingSpinner';
import LineAddress from './../../../../../Components/LineAddress/LineAddress';

class InvoiceView extends Component {

    state = {
        invoiceData: {},
        businessData: {},
        invoiceDataLoader: false,
        businessDataLoader: false,
        transDetailsLoader: false,
        businessErrorMessge: null,
        transactionErrorMessge: null,
        errorMessge: null,
        searchKey: '',
        businessId: null,
        transactionObj: {},
        payOptions: true,
        auditrails: false,
        invoiceID: null,
        transDetails: []

    }
    _isMounted = false;


    componentDidMount() {
        console.log('this.props--------', this.props);
        this._isMounted = true;
        this.setState({
            businessId: this.props.transactionObj.vendorBusinessId,
            searchKey: this.props.searchKey,
            transactionObj: this.props.transactionObj,
            traceNumber: this.props.traceNumber
        }, () => {
            this.handelgetInvoice();
            this.handelGetBusiness();
        });
    }

    componentWillUnmount() {
        this._isMounted = false;


    }

    displayError = (e) => {
        console.log('displayError', e.data.message);
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handelGetTransaction = () => {
        this.setState({
            transDetailsLoader: true
        });
        axios
            .get(
                `transaction/list/${this.state.invoiceID}`
            )
            .then(res => {
                if (this._isMounted) {
                    this.setState({
                        transDetails: res.data,
                        transDetailsLoader: false
                    }, () => {

                    });
                }


            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    transactionErrorMessge: errorMsg,
                    transDetailsLoader: false
                });

            });
    }

    handelGetBusiness = () => {
        this.setState({
            businessDataLoader: true
        });
        axios
            .get(
                `business/${this.state.businessId}`
            )
            .then(res => {
                if (this._isMounted) {
                    this.setState({
                        businessData: res.data,
                        businessDataLoader: false,
                    }, () => {
                        console.log('this.state.businessData', this.state.businessData);
                    });
                }


            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    businessErrorMessge: errorMsg,
                    businessDataLoader: false
                });

            });
    }

    handelgetInvoice = () => {

        this.setState({
            invoiceDataLoader: true,
        }),
        axios
            .get(
                // `invoice/list?key=${this.state.searchKey}&paymentMethod=&direction=true&prop&since=&limit=20&id=${this.state.businessId}`
                `invoice/${this.props.transactionObj.invoiceId}`
                // http://localhost:7090/admin/invoice/15894
            )
            .then(res => {
                console.log(res);
                if (this._isMounted) {
                    this.setState({
                        invoiceData: res.data,
                        invoiceDataLoader: false,
                        invoiceID: res.data.id
                    }, () => {
                        console.log('this.state.invoiceData', this.state.invoiceData);
                        this.handelGetTransaction();
                    });
                }


            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    errorMessge: errorMsg,
                    invoiceDataLoader: false
                });

            });

    }


    render() {
        const {
            invoiceData,
            businessData,
            //transactionObj,
            transDetails
        } = this.state;

        const addressDetails = {
            addressLine: invoiceData.customerVendor
        };
        return (
            <div className="overflow-hidden">
                <div className="invoiceMasterHeader">
                    <div className="row p-3 bg-white  m-0">
                        <div className="col-md-4">
                            <h1>Invoice</h1>
                        </div>
                        <div className="text-center col-md-4"></div>
                        <div className="col-md-4">
                            <div className="actionButtons"></div>
                        </div>
                    </div>
                </div>


                <div className="invoiceMasterBody content-body" >
                    <div className="row">
                        <div className="col-sm-9">
                            <div className='content-block bg-white'>
                                <div className="invAddressAmount">
                                    <div className="row">
                                        {
                                            this.state.invoiceDataLoader ? <LoadingSpinner /> :
                                                <div className="col-sm-4 print-billTo">
                                                    <h3>Remit To</h3>
                                                    {
                                                        this.props.transactionType==='AP'
                                                            ? 
                                                            <div>
                                                                <h4>{invoiceData.customerVendor && invoiceData.customerVendor.companyName}</h4>
                                                                {invoiceData.customerVendor && invoiceData.customerVendor.countryCode &&
                                                                    <div>
                                                                        <p>
                                                                            {/* {invoiceData.customerVendor && invoiceData.customerVendor.address}<br /> */}
                                                                            <LineAddress {...addressDetails} />
                                                                            {invoiceData.customerVendor && invoiceData.customerVendor.cityName ? <span>{invoiceData.customerVendor.cityName}, &nbsp;</span> : null}
                                                                            <span>{invoiceData.customerVendor && invoiceData.customerVendor.stateCode}</span> &nbsp; <span>{invoiceData.customerVendor && invoiceData.customerVendor.zipCode}</span><br />
                                                                            <span>{invoiceData.customerVendor.countryCode && invoiceData.customerVendor.countryCode === 'US' ? <span><br />USA</span> :
                                                                                invoiceData.customerVendor.countryCode === 'CANADA' ? <span><br />CANADA</span> : null
                                                                            }</span>

                                                                        </p>
                                                                    </div>
                                                                }
                                                                {invoiceData.customerVendor && invoiceData.customerVendor.phoneNumber ? <p> Phone: {invoiceData.customerVendor.phoneNumber} </p> : null}
                                                            </div>
                                                            : 
                                                            <div>
                                                                <h4>{businessData.businessName && businessData.businessName}</h4>
                                                                {
                                                                    businessData.businessCountryCode &&
                                                                    <div>
                                                                        <p>
                                                                            {businessData.businessAddress1 && businessData.businessAddress1} <br />
                                                                            <span>{businessData.cityName && businessData.cityName ? <span>{businessData.cityName}, &nbsp;</span> : null}</span>
                                                                            <span>{businessData.businessStateCode && businessData.businessStateCode}</span> &nbsp; <span>{businessData.businessZipcode && businessData.businessZipcode}</span><br />
                                                                            <span>{businessData.businessCountryCode && businessData.businessCountryCode === 'US' ? <span><br />USA</span> :
                                                                                businessData.businessCountryCode === 'CANADA' ? <span><br />CANADA</span> : null
                                                                            }</span>

                                                                        </p>
                                                                    </div>
                                                                }
                                                                <p>{businessData.businessPhone && <p> Phone: {businessData.businessPhone} </p>}</p>

                                                            </div>
                                                    }
                                                    
                                                </div>

                                        }
                                        {
                                            this.state.businessDataLoader ? <LoadingSpinner /> :
                                                <div className="col-sm-4 print-billTo">
                                                    <h3>Bill To</h3>
                                                    {
                                                        this.props.transactionType === 'AP'
                                                            ?
                                                            <div>
                                                                <h4>{businessData.businessName && businessData.businessName}</h4>
                                                                {
                                                                    businessData.businessCountryCode &&
                                                                    <div>
                                                                        <p>
                                                                            {businessData.businessAddress1 && businessData.businessAddress1} <br />
                                                                            <span>{businessData.cityName && businessData.cityName ? <span>{businessData.cityName}, &nbsp;</span> : null}</span>
                                                                            <span>{businessData.businessStateCode && businessData.businessStateCode}</span> &nbsp; <span>{businessData.businessZipcode && businessData.businessZipcode}</span><br />
                                                                            <span>{businessData.businessCountryCode && businessData.businessCountryCode === 'US' ? <span><br />USA</span> :
                                                                                businessData.businessCountryCode === 'CANADA' ? <span><br />CANADA</span> : null
                                                                            }</span>

                                                                        </p>
                                                                    </div>
                                                                }
                                                                <p>{businessData.businessPhone && <p> Phone: {businessData.businessPhone} </p>}</p>

                                                            </div>
                                                            : 
                                                            <div>
                                                                <h4>{invoiceData.customerVendor && invoiceData.customerVendor.companyName}</h4>
                                                                {invoiceData.customerVendor && invoiceData.customerVendor.countryCode &&
                                                                    <div>
                                                                        <p>
                                                                            {/* {invoiceData.customerVendor && invoiceData.customerVendor.address}<br /> */}
                                                                            <LineAddress {...addressDetails} />
                                                                            {invoiceData.customerVendor && invoiceData.customerVendor.cityName ? <span>{invoiceData.customerVendor.cityName}, &nbsp;</span> : null}
                                                                            <span>{invoiceData.customerVendor && invoiceData.customerVendor.stateCode}</span> &nbsp; <span>{invoiceData.customerVendor && invoiceData.customerVendor.zipCode}</span><br />
                                                                            <span>{invoiceData.customerVendor.countryCode && invoiceData.customerVendor.countryCode === 'US' ? <span><br />USA</span> :
                                                                                invoiceData.customerVendor.countryCode === 'CANADA' ? <span><br />CANADA</span> : null
                                                                            }</span>

                                                                        </p>
                                                                    </div>
                                                                }
                                                                {invoiceData.customerVendor && invoiceData.customerVendor.phoneNumber ? <p> Phone: {invoiceData.customerVendor.phoneNumber} </p> : null}
                                                            </div>
                                                    }        
                                                </div>
                                        }

                                        <div className="col-sm-4">
                                            <div className="row mt-3 pt-3">
                                                <div className="col-sm-6">
                                                    <h4>Invoice No</h4>
                                                </div>
                                                <div className="col-sm-6">
                                                    <h4>{invoiceData.invoiceNumber && invoiceData.invoiceNumber}</h4>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-6">
                                                    Invoice Date
                                                </div>
                                                <div className="col-sm-6">
                                                    {invoiceData.invoiceDate && invoiceData.invoiceDate}
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-6">
                                                    Due Date
                                                </div>
                                                <div className="col-sm-6">
                                                    {invoiceData.invoiceDueDate && invoiceData.invoiceDueDate}
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-6">
                                                    P.O.
                                                </div>
                                                <div className="col-sm-6">
                                                    {invoiceData.invoicePO && invoiceData.invoicePO}
                                                </div>
                                            </div>

                                            <div className="row">
                                                <div className="col-sm-6">
                                                    Terms
                                                </div>
                                                <div className="col-sm-6">
                                                    {invoiceData.term && invoiceData.term}
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                                {invoiceData.invoiceItems && invoiceData.invoiceItems.length > 0
                                    ?

                                    <div className="row my-3">
                                        <div className="col-sm-12">
                                            <div id="item-details" className="invoiceTable">
                                                <table className="table tableBorder">
                                                    <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th className="text-right">Unit Price</th>
                                                            <th width="8%">&nbsp;</th>
                                                            <th>Quantity</th>
                                                            <th className="text-right">Subtotal</th>
                                                            <th width="4%">&nbsp;</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {
                                                            invoiceData.invoiceItems.map(invoiceItem => (
                                                                <tr key={invoiceItem.id}>
                                                                    <td>
                                                                        <h6>{invoiceItem.name}</h6>
                                                                        <p>{invoiceItem.description}</p>
                                                                    </td>
                                                                    <td className="text-right">
                                                                        <span>
                                                                            <NumberFormat
                                                                                value={invoiceItem.unitPrice}
                                                                                displayType={'text'}
                                                                                thousandSeparator={true}
                                                                                fractionSize={2}
                                                                                prefix={'$'}
                                                                                decimalScale={2}
                                                                                fixedDecimalScale={true}
                                                                                thousandsGroupStyle={'thousand'}
                                                                                renderText={value => <span>{value}</span>}
                                                                            />
                                                                        </span>
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                    <td>{invoiceItem.quantity}</td>
                                                                    <td className="text-right">
                                                                        <span>
                                                                            <NumberFormat
                                                                                value={invoiceItem.subtotal}
                                                                                displayType={'text'}
                                                                                thousandSeparator={true}
                                                                                fractionSize={2}
                                                                                prefix={'$'}
                                                                                decimalScale={2}
                                                                                fixedDecimalScale={true}
                                                                                thousandsGroupStyle={'thousand'}
                                                                                renderText={value => <span>{value}</span>}
                                                                            />
                                                                        </span>
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                            ))
                                                        }
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    :
                                    null
                                }


                                <div className="row my-3">
                                    <div className="col-sm-12 text-right">
                                        {console.log('+++++++++++ this.props.transactionType ++++++++++++++++++', this.props.transactionType)}
                                        {this.props.transactionType == 'AP' ? <span className="companyNameRblock">Vendor Discount</span> : <span className="companyNameRblock">Customer Discount</span>}
                                        : 
                                        <span>
                                            {
                                                invoiceData.vendorDiscount &&
                                            <NumberFormat
                                                value={invoiceData.vendorDiscount}
                                                displayType={'text'}
                                                thousandSeparator={true}
                                                fractionSize={2}
                                                prefix={'$'}
                                                decimalScale={2}
                                                fixedDecimalScale={true}
                                                thousandsGroupStyle={'thousand'}
                                                renderText={value => <span>{value}</span>}
                                            />
                                            }
                                        </span>
                                    </div>
                                </div>
                                <div className="row my-3">
                                    <div className="col-sm-12 text-right"><span className="companyNameRblock">Negotiated Discount</span>: <span>
                                        {
                                            invoiceData.negotiatedDiscount &&
                                            <NumberFormat
                                                value={invoiceData.negotiatedDiscount}
                                                displayType={'text'}
                                                thousandSeparator={true}
                                                fractionSize={2}
                                                prefix={'$'}
                                                decimalScale={2}
                                                fixedDecimalScale={true}
                                                thousandsGroupStyle={'thousand'}
                                                renderText={value => <span>{value}</span>}
                                            />
                                        }</span></div>
                                </div>
                                <div className="row my-3">
                                    <div className="col-sm-12 text-right"><span className="companyNameRblock">Total Tax</span>: <span>
                                        {
                                            invoiceData.totalTax &&
                                            <NumberFormat
                                                value={invoiceData.totalTax}
                                                displayType={'text'}
                                                thousandSeparator={true}
                                                fractionSize={2}
                                                prefix={'$'}
                                                decimalScale={2}
                                                fixedDecimalScale={true}
                                                thousandsGroupStyle={'thousand'}
                                                renderText={value => <span>{value}</span>}
                                            />
                                        }</span></div>
                                </div>


                                <div className="row my-3">
                                    <div className="col-sm-12 text-right"><span className="companyNameRblock">Total Amount</span>: <span>
                                        {
                                            invoiceData.totalAmount &&
                                            <NumberFormat
                                                value={invoiceData.totalAmount}
                                                displayType={'text'}
                                                thousandSeparator={true}
                                                fractionSize={2}
                                                prefix={'$'}
                                                decimalScale={2}
                                                fixedDecimalScale={true}
                                                thousandsGroupStyle={'thousand'}
                                                renderText={value => <span>{value}</span>}
                                            />
                                        }</span></div>
                                </div>
                                

                                {invoiceData.comment
                                    &&
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="companyNameRblock">Comments</div>
                                            <div className="comment">{invoiceData.comment}</div>
                                        </div>
                                    </div>

                                }
                            </div>

                        </div>
                        <div className="col-sm-3">



                            <div className="content-block rightContentBlock">
                                <div className="invDisplayRightBlock collapaseOuter">
                                    <h3 onClick={() => this.setState({ payOptions: !this.state.payOptions })}
                                        aria-controls="payoption-detals"
                                        aria-expanded={this.state.payOptions}
                                        className={this.state.payOptions === false ? 'collapas-show' : 'collapas-hide'}
                                    >
                                        Transaction Details
                                    </h3>
                                    <Collapse in={this.state.payOptions}>
                                        <div id="transaction-details" className="collapeBlock transaction-details-height">


                                            {this.state.transDetailsLoader ?
                                                <LoadingSpinner /> :
                                                transDetails.map(transDetail => (

                                                    <React.Fragment key={transDetail.id}>
                                                        {/* {transDetail.checkDetails || transDetail.achDetails || (transDetail.transactionType ==='CREDIT_MEMO')
                                                            ?  */}
                                                        <React.Fragment>
                                                            <div className="row mb-2 pb-2">
                                                                <div className="col-sm-8">


                                                                    {
                                                                        transDetail.transactionType === 'ACH_DEBIT_CHECK' ?
                                                                            <span className="companyNameRblock">Funding Debit [ACH]</span>
                                                                            : transDetail.transactionType === 'WEX_CREDIT_CHECK' ?
                                                                                <span className="companyNameRblock">Invoice Payment [Virtual Credit Card]</span>
                                                                                : transDetail.transactionType === 'ACH_CREDIT_CHECK' ?
                                                                                    <span className="companyNameRblock">Invoice Payment [ACH]</span>
                                                                                    : transDetail.transactionType === 'CHECK_CREDIT' ?
                                                                                        <span className="companyNameRblock">Invoice Payment [Check]</span>
                                                                                        : transDetail.transactionType === 'TRAN_CHRG_ACH' ?
                                                                                            <span className="companyNameRblock">Transaction Charge [ACH]</span>
                                                                                            : transDetail.transactionType === 'TRAN_CHRG_CHECK' ?
                                                                                                <span className="companyNameRblock">Transaction Charge [ACH]</span>
                                                                                                : transDetail.transactionType === 'CREDIT_MEMO' ?
                                                                                                    <span className="companyNameRblock">Credit Memo</span>
                                                                                                    : transDetail.transactionType === 'AR_ACH_CREDIT_CHECK' ?
                                                                                                        <span className="companyNameRblock"> Invoice Payment[ACH]</span>
                                                                                                        : transDetail.transactionType === 'AR_ACH_DEBIT_CHECK' ?
                                                                                                            <span className="companyNameRblock"> Funding Credit[ACH]</span>
                                                                                                            : transDetail.transactionType === 'CARD_PAYMENT' ?
                                                                                                                <span className="companyNameRblock"> Invoice Payment[CARD]</span>
                                                                                                                : null
                                                                    }
                                                                    <div>
                                                                        {transDetail.achDetails && transDetail.transactionType != '' ?
                                                                            `Trace No: ${transDetail.achDetails.traceNumber && transDetail.achDetails.traceNumber}`
                                                                            :
                                                                            transDetail.checkDetails && transDetail.transactionType != '' ?
                                                                                `Check No: ${transDetail.checkDetails.checkNumber && transDetail.checkDetails.checkNumber}`
                                                                                : null
                                                                        }
                                                                    </div>

                                                                    <div>
                                                                        {transDetail.checkDetails && transDetail.checkDetails.checkNumber ?
                                                                            transDetail.checkDetails.paymentDate
                                                                            :
                                                                            transDetail.achDetails && transDetail.achDetails.paymentDate ?
                                                                                transDetail.achDetails.paymentDate
                                                                                : null

                                                                        }
                                                                        {
                                                                            transDetail.status == 'UNPAID'
                                                                                ? <div className="statusInfo pending">Pending</div>
                                                                                : <div className="text-primary complete">Complete</div>

                                                                        }
                                                                        {/* {   
                                                                            transDetail.status == 'UNPAID' ? <span>Pending</span> 
                                                                                : 
                                                                                transDetail.checkDetails && transDetail.checkDetails.status ?
                                                                                    <div>Payment Status: {transDetail.checkDetails.status}</div>
                                                                                    :
                                                                                    transDetail.achDetails && transDetail.achDetails.status ?
                                                                                        <div>Payment Status: {transDetail.achDetails.status}</div>
                                                                                        : 
                                                                                        null
                                                                        } */}
                                                                    </div>



                                                                </div>
                                                                <div className="col-sm-4 text-right">
                                                                    {transDetail.transactionType != '' ? <NumberFormat
                                                                        value={transDetail.amount}
                                                                        displayType={'text'}
                                                                        thousandSeparator={true}
                                                                        fractionSize={2}
                                                                        prefix={'$'}
                                                                        decimalScale={2}
                                                                        fixedDecimalScale={true}
                                                                        thousandsGroupStyle={'thousand'}
                                                                        renderText={value => <span>{value}</span>}
                                                                    /> : null}


                                                                </div>

                                                            </div>

                                                        </React.Fragment>

                                                        {/* : null

                                                         } */}
                                                    </React.Fragment>

                                                ))}
                                        </div>
                                    </Collapse>
                                </div>
                                <div className="invDisplayRightBlock collapaseOuter">
                                    <h3 onClick={() => this.setState({ auditrails: !this.state.auditrails })}
                                        aria-controls="payoption-detals"
                                        aria-expanded={this.state.auditrails}
                                        className={this.state.auditrails === false ? 'collapas-show' : 'collapas-hide'}
                                    >
                                        Audit Trail
                                    </h3>
                                    <Collapse in={this.state.auditrails}>
                                        <div id="transaction-details" className="collapeBlock">

                                            {invoiceData.auditTrails && invoiceData.auditTrails.length > 0
                                                ?
                                                invoiceData.auditTrails.map(auditTrail => (
                                                    <div className='row py-3' key={auditTrail.id}>
                                                        <div className="col-sm-6">
                                                            <div className="companyNameRblock">{auditTrail.userName}</div>
                                                            <div>{auditTrail.activityDate}</div>
                                                        </div>
                                                        <div className="col-sm-6 text-right">
                                                            <span className={`statusRblock ${auditTrail.invoiceState === 'SUBMITTED' ? 'submit' :
                                                                auditTrail.invoiceState === 'UPLOADED' ? 'uploaded' :
                                                                    auditTrail.invoiceState === 'VOIDED' ? 'voided' :
                                                                        auditTrail.invoiceState === 'REVIEWED' ? 'reviewed' :
                                                                            auditTrail.invoiceState === 'APPROVED' ? 'approve' :
                                                                                auditTrail.invoiceState === 'SUBMITTED' ? 'submit' :
                                                                                    auditTrail.invoiceState === 'PAID' ? 'paid' :
                                                                                        auditTrail.invoiceState === 'PARTIALLY_PAID' ? 'paid' :
                                                                                            auditTrail.invoiceState === 'RECEIVED' ? 'paid' : null}`
                                                            }

                                                            >
                                                                {auditTrail.invoiceState.replace(/_/g, ' ')}
                                                            </span>
                                                        </div>
                                                    </div>
                                                ))
                                                :
                                                null
                                            }
                                        </div>
                                    </Collapse>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

InvoiceView.propTypes = {

    id: PropTypes.number,
    searchKey: PropTypes.string,
    transactionObj: PropTypes.object,
    traceNumber: PropTypes.string,
    transactionType:PropTypes.string,

};

export default InvoiceView;
