import React,{Component} from 'react';
import {
    Consumer
} from './../../../../../Context/Context'; 
import axios from './../../../../../shared/eaxios';
import LoadingSpinner from './../../../../../Components/LoadingSpinner/LoadingSpinner';
import Pagination from 'react-js-pagination';
import BusinessName from './../../../../../Components/Dashboard/BusinessName';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import refreshIcon from './../../../../../assets/refreshIcon.png';
import {
    Image, 
    //Button
} from 'react-bootstrap';
class CardPayment extends Component {

    state = {
        cardPayDataLoader:false,
        cardPaydata:[],
        activePage: 1,
        totalCount: 0,
        itemPerPage: 20,
        searchKey: '',
        cpError:null,
        UpdateCPaccountingErr:null,
        UpdateCPaccountingLoader:false,
        selectedBusinessName:'',
        updateID:null

    }

    _isMounted = false;

    componentDidMount(){
        //_isMounted = true;
        this.getCardPayData();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.type != this.props.type) {
            this.getCardPayData();
        }

    }
    
    


    handlePageChange = (pageNumber) => {
        this.setState({ activePage: pageNumber });
        this.getCardPayData(pageNumber > 0 ? pageNumber - 1 : 0);
    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }

        return errorMessge;
    }

    keyPress = (e) => {
      
        this.setState(
            {
                searchKey: e.target.value.trim()
            }
        );

    }

    businessOnchange = (e) => {
        this.setState(
            {
                selectedBusinessName: e.target.value,
            },()=>{
                this.getCardPayData();
            });
    }
    
    resetSearch = () => {
        
        this.setState({
            selectedBusinessName: '',
            searchKey:''
        }, () => {
            this.getCardPayData();
        });

    }

    handelSearch = () => {
        this.getCardPayData();
    }
        
    getCardPayData = (
        since = 0
    ) => {
        this.setState({
            cardPayDataLoader: true
        },()=>{
            axios.get(
                `transaction/card?businessId=${this.state.selectedBusinessName}&key=${this.state.searchKey}&since=${since}&limit=${this.state.itemPerPage}&type=${this.props.type}`
            )
                .then(res => {
                    console.log('getCard Pay Data ----------------------- ', res);

                    this.setState({
                        cardPaydata: res.data.entries,
                        totalCount: res.data.total,
                        cardPayDataLoader:false
                    });
                    
                })
                .catch(err=>{
                    console.log(err);
                    let cpError = this.displayError(err);
                    this.setState({
                        cpError,
                        cardPayDataLoader: false
                    });

                });


                    
        });

    };

    UpdateCPaccountingTransac=(id)=>{
        this.setState({
            UpdateCPaccountingLoader:true,
            updateID: id
        }, () => axios.put(`transaction/update/accountingTransaction/${id}`)
            .then(() => {
                this.getCardPayData();
                this.setState({
                    UpdateCPaccountingLoader: false,
                    updateID:null
                });
            })
            .catch(err => {
                console.log(err);
                let UpdateCPaccountingErr = this.displayError(err);
                this.setState({
                    UpdateCPaccountingErr,
                    UpdateCPaccountingLoader: false,
                    updateID:null
                });

            })
        );
        
    }
    
    render(){
        const {
            cardPayDataLoader,
            cardPaydata,
            cpError,
            // UpdateCPaccountingLoader,
            // updateID

        } = this.state;
        // eslint-disable-next-line no-unused-vars
        const { businessNames} =this.props;

        return (
            <div>
                {   cardPayDataLoader 
                    ? 
                    <LoadingSpinner/> 
                    :
                    <div className="p-3">

                        <div className="row pb-3">
                            <div className="col-sm-12">
                                <form className="form-inline float-left">
                                    <select className="form-control businessDropdown col-2 truncate pr-35" onChange={this.businessOnchange} value={this.state.selectedBusinessName}>
                                        <option value=''> Select a Business Name </option>
                                        <Consumer>
                                            {(value) => <BusinessName businessName={value.businessdata[0]} />}
                                        </Consumer>
                                    </select>

                                    {/* <input
                                        placeholder='Enter Vendor Name'
                                        value={this.state.searchKey}
                                        className='form-control float-right ml-2 invoiceIco traceNo'
                                        onChange={this.keyPress}
                                    /> */}

                                    <button
                                        type="button"
                                        className="btn ml-2 search-btn text-black"
                                        onClick={() => this.handelSearch()}
                                    >
                                        Search
                                    </button>

                                    <Link to="#" className='ml-2'>
                                        <Image src={refreshIcon} onClick={() => this.resetSearch()} />
                                    </Link>
                                </form>
                            </div>
                        </div>

                        <div className="boxBg">
                            <div className="show-grid row">
                                <div className="col-md-12 col-sm-12">
                                    <table className="table table-hover">
                                        <thead className="theaderBg">
                                            <tr>
                                                <th className="text-left" width="100">Processing Status</th>
                                                <th className="text-left" width="100">Business Name</th>
                                                <th className="text-left" width="100">Customer Name</th>
                                                <th className="text-left" width="100">Payment Method</th>
                                                <th className="text-left" width="100">Invoice Number</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                cardPaydata.length > 0
                                                    ?
                                                    cardPaydata.map(qb => (
                                                        <tr key={qb.id}>
                                                            <td className={'font-weight-normal ' + (qb.processingStatus === 'STOP' && 'text-warning')}>{qb.processingStatus}</td>
                                                            <td>{qb.businessName}</td>
                                                            <td>{qb.customerVendorName}</td>
                                                            <td>{qb.paymentMethod.replace(/_/g,' ')}</td>
                                                            <td>{qb.invoiceNumber}</td>
                                                        </tr>

                                                    ))
                                                    : cpError ?
                                                        (
                                                            <tr>
                                                                <td colSpan={7} style={{ textAlign: 'center' }}>
                                                                    {cpError}
                                                                </td>
                                                            </tr>
                                                        )
                                                        :
                                                        (
                                                            <tr>
                                                                <td colSpan={7} style={{ textAlign: 'center' }}>
                                                                    No records found.
                                                                </td>
                                                            </tr>
                                                        )
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        {this.state.totalCount ? (
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="paginationOuter text-right mr-3">
                                        <Pagination
                                            activePage={this.state.activePage}
                                            itemsCountPerPage={this.state.itemPerPage}
                                            totalItemsCount={this.state.totalCount}
                                            onChange={this.handlePageChange}
                                        />
                                    </div>
                                </div>
                            </div>
                        ) : null}                   
                    </div>}
            </div>
        );
    }
        
    

}

CardPayment.propTypes ={
    businessNames: PropTypes.object,
    type: PropTypes.string,

};
export default CardPayment;
