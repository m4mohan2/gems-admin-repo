/* eslint-disable no-unused-vars */
import React, { Fragment } from 'react';
import {
    //Grid, 
    Col,
    Row,
    Table,
    Modal,
    Image,
    Button,
    Collapse,
    Tabs,
    Tab
    //ButtonToolbar 
} from 'react-bootstrap';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import NumberFormat from 'react-number-format';
import './Transaction.scss';
import Pagination from 'react-js-pagination';
import DatePicker from 'react-date-picker';
import SuccessIco from './../../../../../assets/success-ico.png';
import axios from '../../../../../shared/eaxios';
//import { JsLogger } from '../../../../../services/jsLogger.service';
import refreshIcon from './../../../../../assets/refreshIcon.png';
import { Link } from 'react-router-dom';
import { FaDownload } from 'react-icons/fa';
import BusinessName from '../../../../../Components/Dashboard/BusinessName';
import {
    Consumer
} from './../../../../../Context/Context';
import { FiMaximize2, FiX } from 'react-icons/fi';
import InvoicePaymentCheck from './InvoicePaymentCheck';
import InvoicePaymentVC from './InvoicePaymentVC';
import CardPayment from './CardPayment';
import moment from 'moment';
//let log = new JsLogger;
import { saveAs } from 'file-saver';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { invoiceDescription } from './../../../../../redux/actions/invoice';
import { TiLink } from 'react-icons/ti';
import InvoiceView from './InvoiceView';

//const InvoicePaymentCheck = React.lazy(() => import('./InvoicePaymentCheck'));



class InvoicePayment extends React.Component {
   
    state = {
        checkedBankId: [],
        checkAll: false,
        open: false,
        currentId: null,
        fundingTransactions: [],
        banks: [],
        transactionLoader: false,
        bankLoader: false,
        errorMessageBank: '',
        errorMessageTransaction: '',
        activePage: 1,
        totalCount: 0,
        itemPerPage: 250,
        searchKey: '',
        downloadLink: '',
        fileLoader: false,
        errorFileDownloading: '',
        fromDate: '',
        fromDateUI: '',
        toDate: '',
        toDateUI: '',
        modalType: '',
        sixPrevMonthDate: '',
        dateError: '',
        bankName: [{ id: 0, bankName: 'loading...' }],
        selectedBusinessName: '',
        bankSelection: true,
        selectedBankName: '',
        vendorSelection: true,
        traceNoSearchKey: '',
        key: 'ACH',
        vendorName: [{ id: 0, companyName: 'loading...' }],
        selectedVendorName: '',
        selectDateConfirm: false,
        selectPaymentDate: '',
        loading: false,
        SinglePendingAmount: '',
        SinglePendingtrace: '',
        SinglePendingUserId:'',
        direction: '',
        //propName: '',
        ErrorAch: null,
        showConfirMmsgACH:false,
        showConfirMmsg: false,
        businessRefId:null,
        showActionModal: false,
        statdateErr:null

    }

    _isMounted = false;

    showActionModal = () => {
        this.setState({
            showActionModal: true,
        });
    }

    hideActionModal = () => {
        this.setState({
            showActionModal: false
        });
    }

    handleConfirmReviewClose = () => {
        this.setState({
            showConfirMmsg: false,
        });
    };
    handleConfirmReviewShow = () => {
        this.setState({ showConfirMmsg: true });
    };

    handleConfirmACHClose = () => {
        this.setState({
            showConfirMmsgACH: false,
        });
    };
    handleConfirmACHShow = () => {
        this.setState({ showConfirMmsgACH: true });
    };

    selectPaymentDateOnchange = date => {

        this.setState(
            { statdateErr: null, selectPaymentDate: date },
            () => {

                console.log('====>', moment(this.state.selectPaymentDate).format('YYYY-MM-DD'));
            }
        );

    } 

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }

        return errorMessge;
    }

    handelGenerateACH = () => {
        this.setState({
            loading:true,
        });
        axios
            .post(
                'achPayment/generateAch'
            )
            .then(res => {
                console.log('res.data for handel Generate ACH', res.data);

                this.setState({
                    showConfirMmsgACH:true,
                });
                

            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    ErrorAch: errorMsg,
                    loading: false
                }, () => {
                    console.log('this.state.ErrorAch', this.state.ErrorAch);
                });

                setTimeout(() => {
                    this.setState({ ErrorAch: null });
                }, 5000);
            });
    }

    handlePageChange = (pageNumber) => {
        this.setState({ activePage: pageNumber });
        this.getFundingDebitTransaction(pageNumber > 0 ? pageNumber - 1 : 0);
    }

    handleChangeItemPerPage = (e) => {
        this.setState({ itemPerPage: e.target.value },
            () => {
                this.getFundingDebitTransaction(this.state.activePage > 0 ? this.state.activePage - 1 : 0);
            });
    }

    handleSuccessClose = () => {
        this.setState({ showSuccessModal: false });
        this.handelInitialDate();
    };

    handleSuccessShow = (type) => {
        console.log(type);
        this.setState({ modalType: type, showSuccessModal: true });
    };

    handleChangeDate = (date, type) => {
        if (date != null) {
            let day = date.getDate();
            let month = date.getMonth() + +1;
            let year = date.getFullYear();
            let fullDate = day + '/' + month + '/' + year;
            if (type === 'from') {
                this.setState({
                    fromDateUI: date,
                    fromDate: fullDate
                });
            }
            else {
                this.setState({
                    toDateUI: date,
                    toDate: fullDate
                });
            }
        }
        else {
            if (type === 'from') {
                this.setState({
                    fromDateUI: '',
                    fromDate: ''
                });
            }
            else {
                this.setState({
                    toDateUI: '',
                    toDate: ''
                });
            }
        }
    }

    handelInitialDate = () => {
        let currentDate = new Date();
        let prevMonthDate = new Date(currentDate.setDate(currentDate.getDate() - 30));

        let cDate = new Date();
        let sixPrevMonthDate = new Date(cDate.setDate(cDate.getDate() - 180));

        this.setState({
            fromDateUI: prevMonthDate,
            sixPrevMonthDate: sixPrevMonthDate
        });
        this.handleChangeDate(prevMonthDate, 'from');
        this.handleChangeDate(new Date(), 'to');
    }


    subMenuOpen = (passId) => {
        let currentId = passId;
        this.setState({
            open: true,
            currentId
        });
    }

    subMenuClose = () => {
        this.setState({
            open: false,
            currentId: null
        });
    }

    handelError = (err) => {
        let errorMessage = '';
        try {
            errorMessage = err.data.message ? err.data.message : err.data.error_description;
        } catch (err) {
            errorMessage = 'No records found.';
        }
        return errorMessage;
    }

    searchTextHandel = (e) => {
        this.setState({ searchKey: e.target.value.trim() });
    }

    resetSearch = () => {
        
        this.setState({
            selectedBusinessName: '',
            selectedBankName: '',
            selectedVendorName: '',
            traceNoSearchKey: '',
            bankSelection: true,
            vendorSelection:true
        });
        this.getFundingDebitTransaction();
    }

    handelSearch = () => {
        this.getFundingDebitTransaction();
    }


    getFundingDebitTransaction = (
        since = 0,
        //searchKey = ''
    ) => {
        this.setState({ transactionLoader: true }, () => {
            axios.get(
                `transaction/invoicePayment/ach?businessId=${this.state.selectedBusinessName}&bankId=${this.state.selectedBankName}&vendorId=${this.state.selectedVendorName}&key=${this.state.traceNoSearchKey}&since=${since}&limit=${this.state.itemPerPage}&type=${this.props.type}`
            )

                .then(res => {
                    console.log('ACH response', res);
                    const totalCount = res.data.total;
                    if (this._isMounted) {
                        this.setState({
                            fundingTransactions: res.data,
                            totalCount: totalCount,
                            transactionLoader: false
                        });
                    }
                    
                    console.log('debit tranction', this.state.fundingTransactions);
                })
                .catch(err => {
                    let errorMessage = this.handelError(err);
                    this.setState({
                        transactionLoader: false, errorMessageTransaction: errorMessage
                    });
                });
        });
    }

    getFundingDebitCSV = () => {
        
        let d = new Date();
        let n = (d.getTime()) / 1000;
        let t = Math.floor(n);

        console.log('||||', this.state.fromDate, '||||', this.state.fromDateUI, '||||', this.state.toDate, '||||', this.state.toDateUI ) ;
        let since = 0;

        if (this.state.fromDate != '' && this.state.fromDateUI != '' && this.state.toDate != '' && this.state.toDateUI != '') {
            this.setState({ fileLoader: true }, () => {
                axios.get(`/transaction/invoicePayment/download/ach?fromDate=${this.state.fromDate}&toDate=${this.state.toDate}&businessId=${this.state.selectedBusinessName}&bankId=${this.state.selectedBankName}&since=${since}&limit=${this.state.itemPerPage}&key=${this.state.traceNoSearchKey}&type=${this.props.type}`)
                    .then(response => {

                        this.setState({
                            fileLoader: false
                        });

                        let blob = new window.Blob([response.data], { type: 'text/plain;charset=utf-8' });
                        saveAs(blob, `Invoice Payment ACH (Transaction)-list-${t}.csv`);
                        this.handleSuccessShow('success');
                    })
                    .catch(err => {
                        let errorMessage = this.handelError(err);
                        this.setState({
                            fileLoader: false, errorFileDownloading: errorMessage
                        });
                        setTimeout(() => {
                            this.setState({ errorFileDownloading: null });
                        }, 5000);
                    });
            });
        }
        else {
            this.setState({
                dateError: 'Please enter valid date.'
            });
            setTimeout(() => {
                this.setState({
                    dateError: ''
                });
            }, 2500);

        }
    }

    

    businessOnchange = (e) => {
        
        this.setState(
            { 
                selectedBusinessName: e.target.value,
                vendorName: [{ id: 0, companyName: 'loading...' }],
                vendorSelection: true,
                bankSelection: true,
                bankName: [{ id: 0, bankName: 'loading...' }], 
            },
            () => {
                console.log('selectedBusinessName Id', this.state.selectedBusinessName);
                // axios.get(`transaction/getBank?businessId=${this.state.selectedBusinessName}`)
                //     .then(res => {
                //         console.log('res after selecting business banks recive', res, res.data.length);
                //         if (res.data.length > 0) {
                //             if (this._isMounted) {
                //                 this.setState({
                //                     bankName: res.data,
                //                     bankSelection: false
                //                 }, () => {
                //                     console.log('this.state.bankName----- transaction', this.state.bankName);
                //                 });
                //             }
                //         }


                //     })
                //     .catch(err => {
                //         console.log('error', err);
                //     });

                let list = this.props.type === 'AR' ? 'customerlist' : 'vendorlist';

                axios.get(`customerVendor/${list}/${this.state.selectedBusinessName}`)

                    .then(res => {
                        console.log('vendor name comes after sending bi', res);
                        if (res.data.entries.length > 0) {
                            if (this._isMounted) {
                                this.setState({
                                    vendorName: res.data.entries,
                                    vendorSelection: false
                                }, () => {
                                    console.log('this.state.vendorName----- transaction', this.state.vendorName);
                                });
                            }
                        }


                    })
                    .catch(err => {
                        console.log('error', err);
                    });
            }



        );
    }

    bankOnchange = (e) => {
        this.setState(
            { selectedBankName: e.target.value },
            () => {
                console.log('selectedBankName', this.state.selectedBankName);
            }
        );
    }

    keyPress = (e) => {
        /*
        if (e.keyCode == 13) {
            console.log('Entered trace no', e.target.value);
            this.setState(
                {
                    traceNoSearchKey: e.target.value
                },
                ()=>{
                    this.getFundingDebitTransaction();
                }
            );
            
                
        }
        */

        //console.log('Entered trace no', e.target.value);
        this.setState(
            {
                traceNoSearchKey: e.target.value.trim()
            }
        );

    }

    handelGetBanks = (param) =>{
        axios.get(`transaction/bank/${param}`)
            .then(res => {
                console.log('res after selecting business banks recive', res, res.data.length);
                if (res.data.entries.length > 0) {
                    if (this._isMounted) {
                        this.setState({
                            bankName: res.data.entries,
                            bankSelection: false
                        }, () => {
                            console.log('this.state.bankName----- transaction', this.state.bankName);
                        });
                    }
                }


            })
            .catch(err => {
                console.log('error', err);
            });

    }

    vendorOnchange = (e) => {
        this.state.vendorName.map(i => {
            
        
        
            if (i.id == e.target.value) {
                this.setState({
                    businessRefId: i.businessRefId
                }, () => {
                    this.handelGetBanks(this.state.businessRefId);

                    
                });
            }

        });

        this.setState(
            { selectedVendorName: e.target.value },
            () => {


                

            }
        );
    }



    pendingAction = (data) => {
        console.log('got ACH data', data);
        this.setState({
            selectDateConfirm: true,
            SinglePendingAmount: data.amount,
            SinglePendingtrace: data.traceNumber,
            SinglePendingUserId: data.transactionRequests[0].userId
        });
    }

    
    pendingConfirm = () =>{

        if (this.state.selectPaymentDate == '') {
            this.setState({
                statdateErr: 'Please select a date'
            });
        }

        else {
            let val = {
                'traceNumber': this.state.SinglePendingtrace,
                'amount': this.state.SinglePendingAmount,
                'paymentDate': moment(this.state.selectPaymentDate).format('YYYY-MM-DD')
            };

            this.setState({
                loading: true,
            }, () => {
                axios.put('achPayment/confirm', val)
                    //axios.put(`achPayment/confirm/${this.state.SinglePendingUserId}`, val)  
                    .then(res => {
                        console.log(res.data);
                        this.getFundingDebitTransaction();
                        this.setState({
                            loading: false,
                            showConfirMmsg: true,
                        }, () => {
                            this.handleDateModalClose();
                        });
                    })
                    .catch(err => {
                        let errorMessage = '';
                        try {
                            errorMessage = err.data.message ? err.data.message : err.data.error_description;
                        } catch (err) {
                            errorMessage = 'No records found.';
                        }
                        this.setState({
                            loading: false, errorMsg: errorMessage
                        });
                        setTimeout(() => {
                            this.setState({ errorMsg: null });
                        }, 2500);
                    });
            });
        }
        
        
        
    }

    /*handelGotoINvoice = (param) => {
        console.log('invoice props', this.props);
        this.props.onClickAction(param);
        
        this.props.history.push('/dashboard/invoice');
    }*/
    handelGotoINvoice = (param, id, obj, traceNumber) => {
        
        this.setState({
            showActionModal: true,
            vendorBusinessId: id,
            invoiceNo: param,
            trasaction: obj,
            traceNumber
        }, () => this.props.onClickAction(this.state.vendorBusinessId));

    }


    componentDidMount() {
        
        console.log('++++++++++++++ this.props.type invoice mount ++++++++++++', this.props);
        this._isMounted = true;
        this.getFundingDebitTransaction();
        this.handelInitialDate();
    }

    componentDidUpdate(prevProps) {
        console.log('++++++++++++++ this.props.type invoice update ++++++++++++', this.props);
        if (prevProps.type != this.props.type) {
            this.getFundingDebitTransaction();
        }

    }

    componentWillUnmount() {
        this._isMounted = false;
    }



    /*************pending action started***********/



    handleDateModalOpen = () => {
        this.setState({
            selectDateConfirm: true
        });
    }
    handleDateModalClose = () => {
        this.setState({
            selectDateConfirm: false,
            selectPaymentDate: '',
            loading: false,
            statdateErr: null

        });
    }

    
   

    /*****************pending action end***********/

    render() {
        const { 
            open, 
            fundingTransactions, 
        } = this.state;
        return (
            <Tabs
                id="controlled-tab"
                activeKey={this.state.key}
                onSelect={key => this.setState({ key })}
                className="subTab px-4"
            >
                <Tab eventKey="ACH" title="ACH">
                    
                    <div className="transactionHistory fundingDebit px-3 pb-3 bg-light">
                        {this.state.ErrorAch ? <div className="alert alert-danger my-3">{this.state.ErrorAch}</div>:null}
                        <div className="row py-4">
                            <div className="col-sm-12 d-flex justify-content-between">
                                <form className="form-inline">
                                    <select className="form-control businessDropdown truncate pr-35" onChange={this.businessOnchange} value={this.state.selectedBusinessName}>
                                        <option value=''> Select a Business Name </option>
                                        <Consumer>
                                            {(value) => <BusinessName businessName={value.businessdata[0]} /> }
                                        </Consumer>
                                    </select>

                                    <select 
                                        className="form-control vendorDropDown truncate pr-35 ml-2" 
                                        onChange={this.vendorOnchange} 
                                        value={this.state.selectedVendorName} 
                                        disabled={this.state.vendorSelection}
                                    >
                                        <option value=''> {this.props.type === 'AR' ? 'Select a customer' : 'Select a Vendor'} </option>
                                        {
                                            this.state.vendorName.map(i => (
                                                <React.Fragment key={i.id} >
                                                    <option value={i.id}> {i.companyName}</option>
                                                </React.Fragment>
                                            )
                                            )
                                        }
                                    </select>

                                    <select 
                                        className="form-control bankDropDown truncate pr-35 ml-2" 
                                        onChange={this.bankOnchange} 
                                        value={this.state.selectedBankName} 
                                        disabled={this.state.bankSelection}
                                    >
                                        <option value=''> Select Vendor Bank </option>
                                        {
                                            this.state.bankName.map(i => (
                                                <React.Fragment key={i.id} >
                                                    <option value={i.id}> {i.bankName}</option>
                                                </React.Fragment>
                                            )
                                            )
                                        }
                                    </select>

                                   

                                    <input
                                        placeholder='Enter Trace Number'
                                        value={this.state.traceNoSearchKey}
                                        className='form-control float-right ml-2 invoiceIco traceNo'
                                        onChange={this.keyPress}
                                    />
                                    <button
                                        type="button"
                                        className="btn ml-2 search-btn text-black"
                                        onClick={() => this.handelSearch()}
                                    >
                                            Search
                                    </button>

                                    <Link to="#" className='ml-2'>
                                        <Image src={refreshIcon} onClick={() => this.resetSearch()} />
                                    </Link>
                                </form>
                                {/* <button 
                                    className="btn btn-primary"
                                    onClick={() => this.handelGenerateACH()}
                                >
                                    Generate ACH
                                </button> */}
                                <Button type="button" className="btn btn-dark float-right ml-3" onClick={() => this.handleSuccessShow('download')}><FaDownload /> Download</Button>                                    
                            </div>
                        </div>


                        <div className="transactionInner">

                            <div className="faSearchPanel topSearchInvoice">
                                    
                            </div>
                            <div className="boxBg">
                                <Row className="show-grid">
                                    <Col sm={12} md={12}>
                                        <Table responsive hover>
                                            <thead className="theaderBg">
                                                <tr>
                                                    <th className="text-left" width="100">Status</th>
                                                    <th>Business Name</th>
                                                    <th>Transaction Date</th>
                                                    <th>Payment Date</th>
                                                    <th className="text-right">Withdrawals</th>
                                                    {/* <th className="pl-4">Funding Bank</th> */}
                                                    <th width='12%'></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    this.state.transactionLoader ?
                                                        <tr>
                                                            <td colSpan={7} style={{ textAlign: 'center' }}>
                                                                <LoadingSpinner />
                                                            </td>
                                                        </tr>
                                                        :
                                                        fundingTransactions.entries.length > 0 ?
                                                            fundingTransactions.entries.map((data, k) => (
                                                                <Fragment key={k}>
                                                                    <tr key={'data' + k} id={'data' + k}>
                                                                        <td className="text-left">{data.isProcessed && data.isProcessed === true ? <div className="text-primary complete">Complete</div> : <span onClick={() => this.pendingAction(data)} className="border-0 statusInfo pending">Pending</span>}</td>
                                                                        <td>{data.businessName}</td>
                                                                        <td>{data.transactionDate ? data.transactionDate : null}</td>
                                                                        <td>{data.paymentDate ? data.paymentDate : null}</td>                                                                        
                                                                        <td className="text-right">
                                                                            <NumberFormat
                                                                                value={data.amount}
                                                                                displayType={'text'}
                                                                                thousandSeparator={true}
                                                                                fractionSize={2}
                                                                                prefix={'$'}
                                                                                decimalScale={2}
                                                                                fixedDecimalScale={true}
                                                                                thousandsGroupStyle={'thousand'}
                                                                                renderText={value => <span>{value}</span>}
                                                                            />
                                                                        </td>
                                                                        {/* <td className="pl-4">{data.bankName}</td> */}
                                                                        <td width='12%' className="text-right">
                                                                            {
                                                                                data.transactionRequests.length > 0 ?
                                                                                    this.state.currentId !== 'subData' + k ?
                                                                                        <Button
                                                                                            onClick={() => this.subMenuOpen('subData' + k)}
                                                                                            aria-controls="collapse-area"
                                                                                            aria-expanded={open}
                                                                                            className="btnTranShowHide"
                                                                                            id={'data' + k}
                                                                                        >

                                                                                            <span className='glyphicon glyphicon-resize-full' aria-hidden="true"><FiMaximize2 /></span>
                                                                                        </Button>
                                                                                        :
                                                                                        <Button
                                                                                            onClick={() => this.subMenuClose()}
                                                                                            aria-controls="collapse-area"
                                                                                            aria-expanded={open}
                                                                                            className="btnTranShowHide"
                                                                                            id={'data' + k}
                                                                                        >
                                                                                            <span className='glyphicon glyphicon glyphicon-remove' aria-hidden="true"><FiX /></span>
                                                                                        </Button>
                                                                                    :
                                                                                    null
                                                                            }

                                                                        </td>
                                                                    </tr>

                                                                    <tr key={'subData' + k} id={'subData' + k} className="transactionDetailsRow">
                                                                        <td colSpan='8' style={{ padding: '0' }}>
                                                                            <Collapse in={this.state.currentId === 'subData' + k ? this.state.open : null}>
                                                                                <div className="transactionDetails">
                                                                                    <Table responsive hover>
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <th className="fundingDebitHeader">ACH Payment</th>
                                                                                                <th>Invoice No.</th>
                                                                                                <th>Description</th>
                                                                                                <th>{this.props.type == 'AR' ? 'Customer Name' : 'Vendor Name'}</th>
                                                                                                <th className="text-right">$ Amount</th>
                                                                                                <th className="text-right">Reference</th>
                                                                                                <th className="text-right">Date</th>
                                                                                                <th width="50"></th>
                                                                                                
                                                                                            </tr>
                                                                                        </tbody>
                                                                                        <tbody>
                                                                                                
                                                                                            {
                                                                                                data.transactionRequests.map((transactions, k) => (
                                                                                                    <tr key={k}>
                                                                                                        {
                                                                                                            k === 0 ?
                                                                                                                <td rowSpan={data.transactionRequests.length} className="fundingDebitCol">
                                                                                                                    <div className="tracnoSec">
                                                                                                                        <h2>Trace Number:</h2>
                                                                                                                        <h3>{data.traceNumber}</h3>
                                                                                                                    </div>
                                                                                                                    <div className="debitSec">
                                                                                                                        <h2>Credited Amount:</h2>
                                                                                                                        <h4>
                                                                                                                            <NumberFormat
                                                                                                                                value={data.amount}
                                                                                                                                displayType={'text'}
                                                                                                                                thousandSeparator={true}
                                                                                                                                fractionSize={2}
                                                                                                                                prefix={'$'}
                                                                                                                                decimalScale={2}
                                                                                                                                fixedDecimalScale={true}
                                                                                                                                thousandsGroupStyle={'lakh'}
                                                                                                                                renderText={value => <span>{value}</span>}
                                                                                                                            />
                                                                                                                        </h4>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                                :
                                                                                                                null
                                                                                                        }
                                                                                                        {/* <td onClick={() => this.handelGotoINvoice(transactions.description)} className="pointer"><TiLink /> {transactions.description}</td> */}
                                                                                                        <td onClick={() => this.handelGotoINvoice(transactions.description, transactions.vendorBusinessId, transactions, data.traceNumber)} className="pointer"><TiLink /> {transactions.invoiceNumber}</td>
                                                                                                        <td>{transactions.description}</td>
                                                                                                        <td>{transactions.customerVendorName}</td>
                                                                                                        <td className="text-right">
                                                                                                            <NumberFormat
                                                                                                                value={transactions.amount}
                                                                                                                displayType={'text'}
                                                                                                                thousandSeparator={true}
                                                                                                                fractionSize={2}
                                                                                                                prefix={'$'}
                                                                                                                decimalScale={2}
                                                                                                                fixedDecimalScale={true}
                                                                                                                thousandsGroupStyle={'lakh'}
                                                                                                                renderText={value => <span>{value}</span>}
                                                                                                            />
                                                                                                        </td>
                                                                                                        <td className="text-right">{transactions.referenceNo}</td>
                                                                                                        <td className="text-right">{transactions.transactionDate}</td>
                                                                                                        <td width="50"></td>
                                                                                                        
                                                                                                    </tr>
                                                                                                ))
                                                                                            }
                                                                                        </tbody>
                                                                                    </Table>
                                                                                        
                                                                                </div>
                                                                            </Collapse>

                                                                        </td>
                                                                    </tr>

                                                                </Fragment>
                                                            ))
                                                            :
                                                            this.state.errorMessageTransaction ?
                                                                (
                                                                    <tr>
                                                                        <td colSpan={7} style={{ textAlign: 'center' }}>
                                                                            {this.state.errorMessageTransaction}
                                                                        </td>
                                                                    </tr>
                                                                )
                                                                :
                                                                (
                                                                    <tr>
                                                                        <td colSpan={7} style={{ textAlign: 'center' }}>
                                                                                No records found.
                                                                        </td>
                                                                    </tr>
                                                                )
                                                }
                                            </tbody>
                                        </Table>
                                    </Col>
                                </Row>
                                {this.state.totalCount ? (
                                    <Row className="px-3">
                                        <Col md={4} className="d-flex flex-row mt-20">
                                            <span className="mr-2 mt-2 font-weight-500">Items per page</span>
                                            <select
                                                id={this.state.itemPerPage}
                                                className="form-control truncatefloat-left w-90"
                                                onChange={this.handleChangeItemPerPage}
                                                value={this.state.itemPerPage}>
                                                <option value='50'>50</option>
                                                <option value='100'>100</option>
                                                <option value='150'>150</option>
                                                <option value='200'>200</option>
                                                <option value='250'>250</option>

                                            </select>
                                        </Col>
                                        <Col md={8}>
                                            <div className="paginationOuter text-right">
                                                <Pagination
                                                    activePage={this.state.activePage}
                                                    itemsCountPerPage={this.state.itemPerPage}
                                                    totalItemsCount={this.state.totalCount}
                                                    onChange={this.handlePageChange}
                                                />
                                            </div>
                                        </Col>
                                    </Row>
                                ) : null}
                            </div>

                        </div>



                        <Modal
                            show={this.state.showSuccessModal}
                            onHide={this.handleSuccessClose}
                            className="payOptionPop"
                        >
                            <Modal.Body>
                                {this.state.modalType === 'success' ?
                                    <Fragment>
                                        <Row>
                                            <Col md="12" className="text-center">
                                                <Image src={SuccessIco} />
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md="12" className="text-center">
                                                <h5>
                                                        Successfully downloaded.
                                                </h5>
                                            </Col>
                                        </Row>
                                    </Fragment>
                                    :
                                    <Fragment>
                                        <Row className="text-center">
                                            <div className="text-center mb-3 w-100">
                                                <h3 className='pl-3'>
                                                        Please select a transaction period
                                                </h3>
                                            </div>
                                        </Row>
                                        <br />
                                        <Row>
                                            <Col sm={12} md={12}>
                                                {
                                                    this.state.fileLoader ?
                                                        <LoadingSpinner />
                                                        :
                                                        this.state.errorFileDownloading ?
                                                            <div className="alert alert-danger">
                                                                {this.state.errorFileDownloading}
                                                            </div>
                                                            : null
                                                }
                                            </Col>
                                        </Row>
                                        {
                                            this.state.dateError != '' ?
                                                <Row>
                                                    <Col sm={12} md={12} className="alert alert-danger btn-darkBlue">
                                                        {this.state.dateError}
                                                    </Col>
                                                </Row>
                                                : null
                                        }
                                        <Row>
                                            <Col md="6" sm="12">
                                                <h5>From date</h5>
                                                <DatePicker
                                                    selected={this.state.fromDateUI}
                                                    value={this.state.fromDateUI}
                                                    onChange={(value) => this.handleChangeDate(value, 'from')}
                                                    dateFormat="YYYY-MM-DD"
                                                    minDate={this.state.sixPrevMonthDate}
                                                    maxDate={new Date()}
                                                />
                                            </Col>
                                            <Col md="6" sm="12">
                                                <h5>To date</h5>
                                                <DatePicker
                                                    selected={this.state.toDateUI}
                                                    value={this.state.toDateUI}
                                                    onChange={(value) => this.handleChangeDate(value, 'to')}
                                                    dateFormat='YYYY/MM/DD'
                                                    minDate={this.state.sixPrevMonthDate}
                                                    maxDate={new Date()}
                                                />
                                            </Col>
                                        </Row>
                                    </Fragment>
                                }
                            </Modal.Body>
                            <div className="my-3 text-center">
                                <Button
                                    onClick={this.handleSuccessClose}
                                    className="btn btn-darkBlue mr-3"
                                >
                                        Return
                                </Button>
                                {this.state.modalType != 'success' ?
                                    <Button
                                        onClick={this.getFundingDebitCSV}
                                        className="btn btn-primary"
                                    >
                                            Download
                                    </Button>
                                    : null}
                            </div>
                        </Modal>


                        {/* generate ACH success */}     
                        <Modal
                            show={this.state.showConfirMmsgACH}
                            onHide={this.handleConfirmACHClose}
                            className="payOptionPop"
                        >
                            <Modal.Body>
                                <Row>
                                    <Col md={12} className="text-center">
                                        <Image src={SuccessIco} />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={12} className="text-center">
                                        <h5>ACH has been Successfully Generated</h5>
                                    </Col>
                                </Row>
                                <div className="text-center">
                                    <Button
                                        onClick={this.handleConfirmACHClose}
                                        className="but-gray"
                                    >
                                        Done
                                    </Button>
                                </div>

                            </Modal.Body>
                        </Modal>   

                        {/* ========== choose date modal =============== */}

                        <Modal
                            show={this.state.selectDateConfirm}
                            onHide={this.handleDateModalClose}
                        >
                            <Modal.Body>
                                <div className="text-center mb-3">
                                    <h5>Select Payment Date</h5>
                                </div>
                                {this.state.errorMsg ? <div className="alert alert-danger">{this.state.errorMsg}</div> : null}
                                {/* <Row>
                            <Col md={12} className="text-center"> */}
                                <DatePicker
                                    onChange={this.selectPaymentDateOnchange}
                                    value={this.state.selectPaymentDate}
                                    className="selectPaymentDateCustom"
                                />
                                {this.state.statdateErr ? <div className='text-danger'>{this.state.statdateErr}</div> : null}
                                {this.state.loading ? <LoadingSpinner /> : null}
                                {/* </Col>
                        </Row> */}

                                {/* <Moment format="YYYY/MM/DD">{this.state.selectPaymentDate}</Moment> */}
                            </Modal.Body>
                            <div className="my-3 text-center">
                                <Button
                                    onClick={this.handleDateModalClose}
                                    className="btn btn-darkBlue mr-3"
                                >
                                    Return
                                </Button>
                                <Button
                                    onClick={this.pendingConfirm}
                                    className="btn btn-primary"
                                >
                                    Confirm
                                </Button>
                            </div>
                        </Modal>
        
                        {/* status changing successfull massage */}
                        <Modal
                            show={this.state.showConfirMmsg}
                            onHide={this.handleConfirmReviewClose}
                            className="payOptionPop"
                        >
                            <Modal.Body>
                                <Row>
                                    <Col md={12} className="text-center">
                                        <Image src={SuccessIco} />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={12} className="text-center">
                                        <h5>ACH has been successfully confirmed</h5>
                                    </Col>
                                </Row>
                                <div className="text-center">
                                    <Button
                                        onClick={this.handleConfirmReviewClose}
                                        className="but-gray"
                                    >
                                        Done
                                    </Button>
                                </div>

                            </Modal.Body>
                        </Modal>  

                        {/* invoice view*/}
                        <Modal
                            show={this.state.showActionModal}
                            onHide={this.hideActionModal}
                            className="right full noPadding slideModal"
                        >
                            <Modal.Header closeButton></Modal.Header>
                            <Modal.Body>
                                <InvoiceView 
                                    id={this.state.vendorBusinessId} 
                                    searchKey={this.state.invoiceNo} 
                                    transactionObj={this.state.trasaction} 
                                    traceNumber={this.state.traceNumber} 
                                    transactionType={this.props.type}
                                />
                            </Modal.Body>

                        </Modal>  


                    </div>


                </Tab>
                {this.props.type !== 'AR' &&
                    <Tab eventKey="Check" title="Check">
                        <InvoicePaymentCheck {...this.props}/>
                    </Tab>
                }
                
                {this.props.type !== 'AR' &&
                    <Tab eventKey="VC" title={'Virtual Credit Card'}>
                        <InvoicePaymentVC  {...this.props} />
                    </Tab>        
                }
                {this.props.type == 'AR' &&
                    <Tab eventKey="CP" title={'Card Payment'}>
                        <CardPayment businessNames={this.state.businessName} {...this.props} />
                    </Tab>
                }
            </Tabs>
        );
    }
}


InvoicePayment.propTypes = {

    onClickAction: PropTypes.func,
    history: PropTypes.object,
    type: PropTypes.string,

};

const mapStateToProps = state => {
    return {
        globalState: state
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onClickAction: data => dispatch(invoiceDescription(data)),
    };
};



export default connect(mapStateToProps, mapDispatchToProps)(InvoicePayment);
