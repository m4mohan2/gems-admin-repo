import React, { Suspense, lazy, Fragment } from 'react';
import Loader from './../../../../../Components/Loader/Loader';
import { Route, Switch, Redirect } from 'react-router-dom';

const Transaction = lazy(() => import('./Transaction'));

const TransactionRoute = () => {
    return (
        <Suspense fallback={<Fragment><Loader /></Fragment>}>
            <Switch>
                <Route path="/dashboard/transaction/ap" render={(props) => <Transaction type={'AP'} {...props} />}></Route>
                <Route path="/dashboard/transaction/ar" render={(props) => <Transaction type={'AR'} {...props} />}></Route>
                <Redirect from="/dashboard/transaction" to="/dashboard/transaction/ar" />
            </Switch>
        </Suspense>
    );
};

export default TransactionRoute;
