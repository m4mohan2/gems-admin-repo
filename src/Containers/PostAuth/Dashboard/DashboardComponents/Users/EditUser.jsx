import { Field, Form, Formik } from 'formik';
import React, { Component, Fragment } from 'react';
import { Button, Col, FormControl, FormGroup, Row } from 'react-bootstrap';
import * as Yup from 'yup';
import axios from '../../../../../shared/eaxios';
import * as AppConst from './../../../../../common/constants';
import PropTypes from 'prop-types';
import Moment from 'moment';
import LoadingSpinner from './../../../../../Components/LoadingSpinner/LoadingSpinner';
import { connect } from 'react-redux';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

const editUserSchema = Yup.object().shape({
    first_name: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter first name').nullable(),
    last_name: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter last name').nullable(),
    address: Yup.string()
        .trim('Please remove whitespace')
        .required('Please enter address')
        .strict()
        .max(40, 'maximum characters length 40').nullable(),
    gender: Yup.string()
        .strict()
        .required('Please select gender').nullable(),
    // dob: Yup
    //     .string()
    //     .required('Please enter dob')
    //     .strict().nullable()
});

class EditUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            uploadFile: null,
            showVendor: false,
            errorMessge: null,
            editUserEnable: false,
            disabled: false,
            editUserLoader: false,
            editErrorMessge: false,
            countryList: [],
            stateList: [],
            cityList: [],
            countrySelected: '',
            stateSelected: '',
            selectedFile: '',
            uploadErrorMessage: '',
            permission: [],
            dateOfBirth: ''
        };
    }

    handleCountry = () => {
        axios.get(AppConst.APIURL + '/api/countries')
            .then(response => {
                const countryList = response.data.data.map(data => {
                    return data;
                });
                this.setState({ countryList, editUserLoader: false });
            });
    }

    handleState = id => {
        let params = {
            'id': id,
        };
        axios.post(AppConst.APIURL + '/api/getStates', params)
            .then(res => {
                this.setState({ stateList: res.data.data });
            });
    }

    handleCity = id => {
        let params = {
            'id': id,
        };
        axios.post(AppConst.APIURL + '/api/getCities', params)
            .then(res => {
                const cityList = res.data.data;
                this.setState({ cityList });
            });
    }

    componentDidMount() {
        const facility = this.props.facility;
        const permission = [];
        facility.map(data => {
            data.name === 'gemster' && data.permission.length !== 0 && permission.push(...data.permission);
        });
        this.setState({ permission });

        const values = { ...this.props };
        this.setState({
            editUserLoader: true,
            countrySelected: values.country !== 'null' || values.country !== null || values.country !== '' ? values.country : '',
            stateSelected: values.state !== 'null' || values.state !== null || values.state !== '' ? values.state : '',
            dateOfBirth: values.dob && Moment(values.dob).toDate()
        });
        this.handleCountry();
        values.country !== 'null' || values.country !== null || values.country !== '' ? this.handleState(values.country) : '';
        values.state !== 'null' || values.state !== null || values.state !== '' ? this.handleCity(values.state) : '';
    }

    handleChange = (event) => {
        if (event.target.name === 'Country') {
            this.setState({ countrySelected: event.target.value });
            this.handleState(event.target.value);
        }
        else if (event.target.name === 'State') {
            this.setState({ stateSelected: event.target.value });
            this.handleCity(event.target.value);
        }
    }

    static getDerivedStateFromProps(props, state) {
        if (!state.vendorData) {
            return {
                ...props
            };
        }
    }

    handleEditUserEnable = () => {
        this.setState({
            editUserEnable: true,
            disabled: true
        });
    }

    handleEditUserDisable = () => {
        this.setState({
            editUserEnable: false,
            disabled: false
        });
        this.props.onReload(this.state.vendorData);
    }

    handleCloseVendor = () => {
        this.props.onClick();
    };

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handleSubmit = (values, { setSubmitting }) => {
        if (this.state.uploadErrorMessage !== '') {
            setSubmitting(false);
        }
        else {
            this.setState({
                editUserLoader: true,
            });
            const formData = new window.FormData();
            formData.append('first_name', values.first_name);
            formData.append('last_name', values.last_name);
            formData.append('address', values.address);
            formData.append('gender', values.gender);
            formData.append('profile_pic', this.state.selectedFile);
            formData.append('dob', Moment(this.state.dateOfBirth).format('YYYY-MM-DD'));
            formData.append('active_status', values.active_status);
            formData.append('country', this.state.countrySelected);
            formData.append('state', this.state.stateSelected);
            formData.append('city', values.city);
            formData.append('biography', values.biography);
            formData.append('phone', values.phone);
            formData.append('zip', values.zip);
            formData.append('user_id', values.id);

            axios
                .post(AppConst.APIURL + '/api/updateUser', formData)
                .then(res => {
                    setSubmitting(false);
                    console.log('Vendor details get data', res.data);
                    this.setState({
                        editUserLoader: false,
                        editUserEnable: false,
                        disabled: false,
                    }, () => {
                        this.props.handleEditConfirMmsg();
                        this.props.handelviewEditModalClose();
                    });
                })
                .catch(e => {
                    let errorMsg = this.displayError(e);
                    this.setState({
                        editUserLoader: false,
                        editErrorMessge: errorMsg,
                    });
                    setTimeout(() => {
                        this.setState({ deleteErrorMessge: null });
                    }, 5000);
                });
        }
    };

    onDrop = async files => {
        console.log(files);
        this.setState({
            files,
            dropzoneActive: false,
            isDrop: true
        });
        let reader = '';
        if (files.length > 0) {
            console.log('image details', files[0]);

            // FILE SIZE RESTRICTION
            if (files[0]['size'] > 600000) {
                this.setState({
                    uploadErrorMessage: 'File size should be less than 600KB',
                    isDrop: false,
                });
                setTimeout(() => {
                    this.setState({ uploadErrorMessage: null });
                }, 5000);
            }
            else if ((files[0]['type'] !== 'image/png') && (files[0]['type'] !== 'image/jpeg')) {
                this.setState({
                    uploadErrorMessage: 'Please upload jpeg/png file',
                    isDrop: false,
                });
                setTimeout(() => {
                    this.setState({ uploadErrorMessage: null });
                }, 5000);
            }
            else {
                reader = new window.FileReader();
                reader.readAsDataURL(files[0]);
                reader.onload = () => {
                    let urlString = reader.result;
                    this.setState({
                        uploadLoader: false,
                        imageURL: urlString,
                        uploadFile: files[0]
                    });
                };
            }
        }
    };

    onFileChange = event => {
        this.setState({ uploadErrorMessage: '' });
        if (event.target.files.length !== 0) {
            if (event.target.files[0].size > 600000) {
                this.setState({
                    uploadErrorMessage: 'File size should be less than 600KB',
                    isDrop: false,
                });
            }
            else if ((event.target.files[0].type !== 'image/png') && (event.target.files[0].type !== 'image/jpeg')) {
                this.setState({
                    uploadErrorMessage: 'Please upload jpeg/png file',
                    isDrop: false,
                });
            }
            else {
                this.setState({ selectedFile: event.target.files[0], uploadErrorMessage: '' });
            }
        }
    };

    handleDOB = date => {
        this.setState({ dateOfBirth: date });
    }

    render() {
        const initialValues = { ...this.props };
        const values = {
            id: initialValues.id,
            first_name: initialValues.first_name ? initialValues.first_name.trim() : '',
            last_name: initialValues.last_name ? initialValues.last_name.trim() : '',
            address: initialValues.address ? initialValues.address.trim() : '',
            gender: initialValues.gender,
            // dob: initialValues.dob && Moment(initialValues.dob).format('yyyy-MM-DD'),
            profile_pic: initialValues.profile_pic,
            biography: initialValues.biography && initialValues.biography,
            phone: initialValues.phone && initialValues.phone,
            zip: initialValues.zip,
            city: initialValues.city !== 'null' ? initialValues.city : '',
            active_status: initialValues.active_status
        };

        const { disabled } = this.state;
        return (
            <div className="addcustomerSec">
                <div className="boxBg p-35">
                    <Row className="show-grid">
                        <Col xs={12} md={12}>
                            <div className="sectionTitle">
                                <h2>{this.state.editUserEnable !== true ? 'View' : 'Edit'} Gemster</h2>
                            </div>
                        </Col>
                        <Col xs={12} className="brd-right">
                            <Formik
                                initialValues={values}
                                validationSchema={editUserSchema}
                                onSubmit={this.handleSubmit}
                                enableReinitialize={true}
                            >
                                {({
                                    values,
                                    errors,
                                    touched,
                                    isSubmitting,
                                }) => {
                                    return (
                                        <Form className={disabled === false ? ('hideRequired') : null}>
                                            {this.state.editUserLoader === true ? <LoadingSpinner /> : ''}
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Email <span className="required">*</span></h3></label>
                                                        <Field
                                                            name="email"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={initialValues.email || ''}
                                                            disabled="disabled"
                                                        />
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>First Name <span className="required">*</span></h3></label>
                                                        <Field
                                                            name="first_name"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.first_name || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.first_name && touched.first_name ? (
                                                            <span className="errorMsg ml-3">{errors.first_name}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Last Name <span className="required">*</span></h3></label>
                                                        <Field
                                                            name="last_name"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.last_name || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.last_name && touched.last_name ? (
                                                            <span className="errorMsg ml-3">{errors.last_name}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Address <span className="required">*</span></h3></label>
                                                        <Field
                                                            name="address"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.address || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.address && touched.address ? (
                                                            <span className="errorMsg ml-3">{errors.address}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Gender <span className="required">*</span></h3></label>
                                                        <Field
                                                            name="gender"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            value={values.gender || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >
                                                            <option value="">Select Gender</option>
                                                            <option value="Male" key="1"> Male</option>
                                                            <option value="Female" key="0">Female</option>
                                                        </Field>
                                                        {errors.gender && touched.gender ? (
                                                            <span className="errorMsg ml-3">{errors.gender}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} sm={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Date of Birth<span className="required">*</span></h3></label>
                                                        {/* <Field
                                                            name="dob"
                                                            type="date"
                                                            value={values.dob || ''}
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.dob && touched.dob ? (
                                                            <span className="errorMsg ml-3">{errors.dob}</span>
                                                        ) : null} */}
                                                        <DatePicker
                                                            name='dob'
                                                            selected={this.state.dateOfBirth}
                                                            onChange={this.handleDOB}
                                                            className='form-control'
                                                            maxDate={new Date(Moment(new Date()).subtract(18, 'years').toISOString())}
                                                            required
                                                            dateFormat="dd-MM-yyyy"
                                                            showMonthDropdown
                                                            showYearDropdown
                                                            placeholderText="DOB"
                                                            autoComplete="off"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} sm={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Country<span className="required">*</span></h3></label>
                                                        <Field
                                                            required
                                                            name="Country"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                            value={this.state.countrySelected}
                                                            onChange={this.handleChange}
                                                        >
                                                            <option value="">Select Country</option>
                                                            {this.state.countryList.map(data => {
                                                                return <option value={data.id} key={data.id}>{data.name}</option>;
                                                            })}
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} sm={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>State</h3></label>
                                                        <Field
                                                            name="State"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                            onChange={this.handleChange}
                                                            value={this.state.stateSelected}
                                                        >
                                                            <option value="">Select State</option>
                                                            {this.state.stateList.map(data => {
                                                                return <option value={data.id} key={data.id}>{data.name}</option>;
                                                            })}
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} sm={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>City</h3></label>
                                                        <Field
                                                            name="city"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >
                                                            <option value="">Select City</option>
                                                            {this.state.cityList.map(data => {
                                                                return <option value={data.id} key={data.id}>{data.name}</option>;
                                                            })}
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} sm={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Profile Pic</h3></label>
                                                        <input
                                                            name="selectedFile"
                                                            type="file"
                                                            onChange={this.onFileChange}
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {this.state.uploadErrorMessage && <span className="errorMsg ml-3">{this.state.uploadErrorMessage}</span>}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} sm={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Biography</label>
                                                        <Field
                                                            name="biography"
                                                            type="text"
                                                            component="textarea"
                                                            value={values.biography || ''}
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.biography && touched.biography ?
                                                            <span className="errorMsg">{errors.biography}</span>
                                                            : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Phone</h3></label>
                                                        <Field
                                                            name="phone"
                                                            type="number"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.phone || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.phone && touched.phone ? (
                                                            <span className="errorMsg ml-3">{errors.phone}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>ZIP</h3></label>
                                                        <Field
                                                            name="zip"
                                                            type="number"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.zip || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.zip && touched.zip ? (
                                                            <span className="errorMsg ml-3">{errors.zip}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                {values.profile_pic &&
                                                    <Row className="show-grid">
                                                        <Col xs={12} md={12}>
                                                            <img height="200" className="ml-3 rounded" src={AppConst.UPLOADURL + '/profile/' + values.profile_pic}></img>
                                                        </Col>
                                                    </Row>
                                                }


                                                {/* <Col md={6} className="text-center">
                                    <div className="form-group">
                                        <Dropzone                                          
                                            onDrop={this.onDrop}
                                        >
                                            {({ getRootProps, getInputProps }) => (
                                                <section>
                                                    <div {...getRootProps()}>
                                                        <input {...getInputProps()} />
                                                        <Fragment>
                                                            <div className="uploadWrap">
                                                                {this.state.uploadLoader ? (
                                                                    <LoadingSpinner />
                                                                ) : ((this.state.imageURL == null || this.state.imageURL == '') ?
                                                                    (<Image
                                                                        src={UploadIcon}
                                                                        alt="upload"
                                                                    />)
                                                                    :
                                                                    (<Image height={250} src={this.state.imageURL} />)
                                                                    )}


                                                                <p className="sm-txt-blue text-center">
                                                                    Please upload only jpg/png file
                                                                </p>
                                                            </div>
                                                        </Fragment>
                                                        <button type="button" className="btn btn-primary btn-position">
                                                            Upload logo
                                                        </button>
                                                    </div>
                                                </section>
                                            )}
                                        </Dropzone>
                                        {this.state.uploadErrorMessage ? <p className="text-danger"><span>*</span>{this.state.uploadErrorMessage}</p> : null}
                                    </div>
                                </Col> */}
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>&nbsp;</Col>
                                            </Row>
                                            <Row>&nbsp;</Row>
                                            {this.state.permission[1] && this.state.permission[1].status === true &&
                                                <Row className="show-grid text-center">
                                                    <Col xs={12} md={12}>
                                                        <Fragment>
                                                            {this.state.editUserEnable !== true ?
                                                                <Button className="blue-btn border-0"
                                                                    onClick={this.handleEditUserEnable}>
                                                                    Edit
                                                            </Button> :
                                                                <Fragment>
                                                                    <Button onClick={this.props.handelviewEditModalClose}
                                                                        className="but-gray border-0 mr-2">
                                                                        Cancel
                                                                </Button>
                                                                    <Button type="submit"
                                                                        className="blue-btn ml-2 border-0"
                                                                        disabled={isSubmitting}>
                                                                        Save
                                                                </Button>
                                                                </Fragment>
                                                            }
                                                        </Fragment>
                                                    </Col>
                                                </Row>}
                                            {disabled === false ? null : (<Fragment>
                                                <Row>
                                                    <Col md={12}>
                                                        <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                                    </Col>
                                                </Row>
                                            </Fragment>)}
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Col>
                    </Row>
                </div>
            </div>

        );
    }
}

const mapStateToPros = state => {
    return {
        facility: state.auth.facility
    };
};

EditUser.propTypes = {
    globState: PropTypes.object,
    onClickAction: PropTypes.func,
    onReload: PropTypes.func,
    onClick: PropTypes.func,
    handelviewEditModalClose: PropTypes.func,
    handleEditConfirMmsg: PropTypes.func,
    facility: PropTypes.any
};

export default connect(mapStateToPros, null)(EditUser);
