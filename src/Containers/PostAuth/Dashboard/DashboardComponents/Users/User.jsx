import React, { Component, Fragment } from 'react';
import axios from '../../../../../shared/eaxios';
import {
    Table,
    Row,
    Modal,
    Col,
    Image,
    Button,
} from 'react-bootstrap';
import Pagination from 'react-js-pagination';
import { Link } from 'react-router-dom';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import refreshIcon from './../../../../../assets/refreshIcon.png';
import SuccessIco from './../../../../../assets/success-ico.png';
import Dropzone from 'react-dropzone';
import UploadIcon from './../../../../../assets/uploading.png';
import * as AppConst from './../../../../../common/constants';
import EditBusinessCustomers from './EditUser';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

//import ReactExport from 'react-data-export';
//const ExcelFile = ReactExport.ExcelFile;
//const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
//const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;


class User extends Component {

    state = {
        userLists: [],

        activePage: 1,
        totalCount: 0,
        itemPerPage: 10,

        loading: false,
        errorMessge: null,

        searchString: '',
        statusChange: false,

        userId: null,
        status: null,

        sort: 1,
        field: 'bankname',

        statusConfirMmsg: false,
        statuserrorMsg: null,
        sortingActiveID: 1,
        AddEditModal: false,
        Bankname: '',
        uploadLoader: false,
        imageURL: null,
        uploadErrorMessage: null,
        uploadFile: null,
        bankNameError: null,
        successMessage: null,
        BankAddloader: false,
        selectedUserId: null,
        modalState: null,
        addEditBankError: false,

        permission: []
    };

    keyPress = (e) => {
        this.setState({
            searchString: e.target.value.trim(),
        });
    }

    handelSearch = () => {
        this.fetchUserList();
    }

    resetSearch = () => {
        this.setState({
            searchString: ''
        }, () => {
            this.fetchUserList();
        });
    }

    sortingActive = (id) => {
        this.setState({
            sortingActiveID: id
        }, () => { console.log('this.state.sortingActiveID', this.state.sortingActiveID); });
    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Unknown error!';
        }
        return errorMessge;
    }

    handleHide = () => {
        this.setState({
            statusChange: false,
            statuserrorMsg: null
        });
    }

    handleHideInfluencer = () => {
        this.setState({
            setInfluencer: false,
            statuserrorMsg: null
        });
    }

    handelStatusModal = (id, status) => {
        this.setState({
            statusChange: true,
            userId: id,
            status: status
        });

    }

    handelInfluencerModal = (id, status) => {
        this.setState({
            setInfluencer: true,
            userId: id,
            status: status
        });

    }

    onChangeBankName = (e) => {

        let str = e.target.value.trim();
        this.setState({
            bankNameError: null,
            Bankname: str
        });

    }

    _isMounted = false;



    fetchUserList = (
        sort = this.state.sort,
        field = this.state.field,
    ) => {

        this.setState({
            loading: true,
            sort: sort,
            field: field
        }, () => {

            axios
                .get(
                    AppConst.APIURL + `/api/alluser?&pageSize=${this.state.itemPerPage}&page=${this.state.sort}&user_type=1&searchString=${this.state.searchString}`
                )
                .then(res => {
                    const userLists = res.data.data.data;
                    const totalCount = res.data.data.total;
                    if (this._isMounted && userLists) {
                        this.setState({
                            userLists: userLists,
                            totalCount: totalCount,
                            loading: false
                        }, () => { });
                    }

                })
                .catch(e => {
                    let errorMsg = this.displayError(e);
                    this.setState({
                        errorMessge: errorMsg,
                        loading: false
                    });
                    setTimeout(() => {
                        this.setState({ errorMessge: null });
                    }, 5000);

                });

        });

    };

    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber });
        this.fetchUserList(pageNumber > 0 ? pageNumber : 0, this.state.field);
        //console.log('pageNumber',pageNumber);
        //console.log('sort',this.state.sort);
    };

    handleChangeItemPerPage = (e) => {
        this.setState({ sort: 1 });
        this.setState({ itemPerPage: e.target.value },
            () => {
                this.fetchUserList();
            });
    }

    resetPagination = () => {
        this.setState({ activePage: 1 });
    }

    componentDidMount() {
        this._isMounted = true;
        const facility = this.props.facility;
        const permission = [];
        facility.map(data => {
            data.name === 'gemster' && data.permission.length !== 0 && permission.push(...data.permission);
        });
        this.setState({ permission }, () => {
            permission[0].status === true &&
                this.fetchUserList();
        });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    handleStatus(id, status, col) {
        console.log(status);
        let updateStatus = {
            'active_status': status
        };

        let updateInflu = {
            'is_influencer': status
        };

        const dataArr = col === 'setInfluencer' ? updateInflu : updateStatus;

        const dataUrl = col === 'setInfluencer' ? `/api/influenceUpdateUser/${id}` : `/api/statusUpdateUser/${id}`;

        axios.put(AppConst.APIURL + dataUrl, dataArr)
            .then(res => {
                console.log('--------------res------', status);
                console.log('--------------res------', res);
                this.setState({
                    userId: null,
                    status: null,
                    successMessage: 'Status successfully changed',
                    statusConfirMmsg: true
                });
                this.handleHide();
                this.handleHideInfluencer();
                this.fetchUserList();
            }

            )
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    statuserrorMsg: errorMsg,
                    loading: false


                });
                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);
            });

    }

    handleStatusChangedClose = () => {
        this.setState({
            statusConfirMmsg: false,
            successMessage: null
        });
    }

    handelAddEditModalClose = () => {
        this.setState({
            AddEditModal: false,
            imageURL: null,
            bankNameError: null,
            modalState: null,
            Bankname: '',
            selectedUserId: null,
            uploadErrorMessage: null,
            uploadFile: null
        });
    }

    handelAddEditBankModal = (param, name, id, url) => {
        console.log('param, name, id, url', param, name, id, url);
        this.setState({
            AddEditModal: true,
            modalState: param
        });
        if (param === 'edit') {
            this.setState({
                Bankname: name,
                selectedUserId: id,
                imageURL: url
            });
        }

    }

    handelEditUser = () => {
        const config = {
            headers: {
                'Content-Type': 'multipart/form-data',
            }
        };

        let formData = new window.FormData();
        formData.append('file', this.state.uploadFile);

        axios
            .put(`/banklist/update/${this.state.selectedUserId}`, formData, config)
            .then(res => {
                this.handelAddEditModalClose();
                console.log(res);
                if (this._isMounted) {
                    this.setState({
                        BankAddloader: false,
                        successMessage: 'Bank logo has been successfully Edited',
                        statusConfirMmsg: true,
                        activePage: 1

                    }, () => { this.fetchUserList(0, '', ''); });
                }

            })
            .catch(err => {
                let errorMessage = this.displayError(err);
                this.setState({
                    uploadLoader: false,
                    addEditBankError: errorMessage
                });
                setTimeout(() => {
                    this.setState({ addEditBankError: null });
                }, 5000);
            });

    }


    handelAddBank = () => {
        console.log('this.state.uploadFile', this.state.uploadFile);
        console.log('this.state.Bankname', this.state.Bankname);

        if (this.state.Bankname != null && this.state.Bankname != '') {
            if (this.state.uploadFile != null) {

                this.setState({
                    BankAddloader: true
                });

                const config = {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    }
                };

                let formData = new window.FormData();
                formData.append('file', this.state.uploadFile);
                formData.append('bankName', this.state.Bankname);

                axios
                    .post('/banklist/add', formData, config)
                    .then(res => {
                        this.handelAddEditModalClose();
                        console.log(res);
                        if (this._isMounted) {
                            this.setState({
                                uploadLoader: false,
                                BankAddloader: false,
                                successMessage: 'User has been successfully Added',
                                statusConfirMmsg: true,
                            }, () => this.fetchUserList());
                        }

                    })
                    .catch(err => {
                        let errorMessage = this.displayError(err);
                        console.log(err);
                        this.setState({
                            uploadLoader: false,
                            BankAddloader: false,
                            addEditBankError: errorMessage
                        });


                        setTimeout(() => {
                            this.setState({ addEditBankError: null });
                        }, 5000);
                    });

            } else {
                this.setState({
                    uploadErrorMessage: ' Please upload a logo',
                });
            }

        } else {
            this.setState({
                bankNameError: '* Please enter a bank name'
            });
        }

    }

    onDrop = async files => {

        this.setState({
            files,
            dropzoneActive: false,
            isDrop: true
        });

        let reader = '';
        if (files.length > 0) {
            console.log('image details', files[0]);


            // FILE SIZE RESTRICTION
            if (files[0]['size'] > 600000) {
                this.setState({
                    uploadErrorMessage: 'File size should be less than 600KB',
                    isDrop: false,
                });
                setTimeout(() => {
                    this.setState({ uploadErrorMessage: null });
                }, 5000);
            } else if ((files[0]['type'] !== 'image/png') && (files[0]['type'] !== 'image/jpeg')) {
                this.setState({
                    uploadErrorMessage: 'Please upload jpeg/png file',
                    isDrop: false,
                });
                setTimeout(() => {
                    this.setState({ uploadErrorMessage: null });
                }, 5000);
            } else {

                reader = new window.FileReader();
                reader.readAsDataURL(files[0]);

                reader.onload = () => {
                    let urlString = reader.result;

                    this.setState({
                        uploadLoader: false,
                        imageURL: urlString,
                        uploadFile: files[0]
                    });
                };


            }

        }

    };

    handelviewEditModal = (data) => {
        this.setState({
            viewEditModal: true,
            vendorData: data
        }, () => {
            console.log('this.state.viewEditData@@', this.state.vendorData);
        });
    }

    handelviewEditModalClose = () => {
        this.setState({
            viewEditModal: false,
        });
    }

    handleEditConfirMmsg = () => {
        this.setState({
            viewEditModal: false,
            editConfirMmsg: true
        }, () => {
            this.fetchUserList();
        });
    }

    handleEditConfirMmsgClose = () => {

        this.setState({
            editConfirMmsg: false
        });
    }

    download(data) {
        let a = document.createElement('a');
        a.href = 'data:attachment/csv,' + data;
        a.terget = '_blank';
        a.download = 'gemlist.csv';
        document.body.appendChild(a);
        a.click();
    }

    objectToCsv(data) {
        const csvRows = [];
        const headers = Object.keys(data[0]);

        const lbl = ['First Name', 'Last Name', 'Email', 'Gender', 'Dob', 'Address'];
        csvRows.push(lbl.join(','));

        for (const row of data) {
            const values = headers.map(header => {
                const escaped = ('' + row[header]).replace(/"/g, '\\"');
                return `"${escaped}"`;
            });
            csvRows.push(values.join(','));
        }
        //console.log('csvRows',csvRows);
        return csvRows.join('\n');
    }

    async getReport() {
        axios
            .get(
                AppConst.APIURL + '/api/alluser?&export=true&user_type=1'
            )
            .then(res => {
                const json = res.data.data;
                //console.log('=====================',json);

                if (json.length > 0) {

                    const data = json.map(row => ({
                        first_name: row.first_name,
                        last_name: row.last_name,
                        email: row.email,
                        gender: row.gender,
                        dob: row.dob,
                        address: row.address
                    }));

                    const csvData = this.objectToCsv(data);
                    //console.log(csvData);

                    this.download(csvData);
                }
            });
    }

    async exportCsv() {
        const jsonurl = AppConst.APIURL + '/api/alluser?&export=true';
        const response = await fetch(jsonurl);
        const json = await response.json();
        const re = json.data;

        let csvRow = [];
        let A = [
            ['id', 'email']
        ];
        //let re = this.state.userLists;

        for (let item = 0; item < re.length; item++) {
            //A.push(re[item].first_name, re[item].email);
        }

        console.log('A=>', A);

        for (let i = 0; i < A.length; ++i) {
            csvRow.push(A[i].join(','));
        }


        console.log('csvRow', csvRow);

        const csvString = A.join('%0A');

        console.log('csvString', csvString);


        let a = document.createElement('a');
        a.href = 'data:attachment/csv, ' + csvString;
        a.terget = '_blank';
        a.download = 'gemlist.csv';
        document.body.appendChild(a);
        a.click();
    }

    render() {
        return (
            <div className="dashboardInner businessOuter pt-3">
                {this.state.permission[0] && this.state.permission[0].status === true ?
                    <Fragment>
                        {this.state.errorMessge
                            ?
                            <div className="alert alert-danger col-12" role="alert">
                                ERROR : {this.state.errorMessge}
                            </div>
                            : null
                        }
                        {this.state.permission[2] && this.state.permission[2].status === true ?
                            <Row>
                                <Col md={12} className="text-right mb-20">
                                    <Button variant="success" onClick={() => this.getReport()}>Export</Button>
                                </Col>
                            </Row> : ''
                        }
                        <div className="searchBox">
                            <div className="row">
                                <div className="col-12 col-md-4">
                                    <input className="form-control" type="text" placeholder="Search" value={this.state.searchString} onChange={this.keyPress} />
                                </div>
                                <div className="col-12 col-md-4">
                                    <button
                                        type="button"
                                        className="btn btn-primary"
                                        onClick={() => this.handelSearch()}
                                    >Search</button>
                                    <Link to="#" className='ml-3 mt-2'>
                                        <Image src={refreshIcon} onClick={() => this.resetSearch()} />
                                    </Link>
                                </div>
                            </div>
                        </div>
                        <div className="boxBg">
                            <Table responsive hover>
                                <thead className="theaderBg">
                                    <tr>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Verified</th>
                                        <th>Influencer</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.loading ? (<tr>
                                        <td colSpan={12}>
                                            <LoadingSpinner />
                                        </td>
                                    </tr>) :
                                        this.state.userLists.length > 0 ? (
                                            this.state.userLists.map(userList => (
                                                <tr key={userList.id}>
                                                    <td>{userList.first_name}</td>
                                                    <td>{userList.last_name}</td>
                                                    <td>{userList.email}</td>
                                                    <td>{userList.address}</td>
                                                    <td>{userList.is_verified === 1 ? 'Yes' : 'No'}</td>
                                                    <td>
                                                        {userList.is_influencer === 1
                                                            ? <i className="fa fa-circle green" aria-hidden="true" title="Yes"
                                                                onClick={() => this.state.permission[1] && this.state.permission[1].status === true && this.handelInfluencerModal(userList.id, '0')}></i>
                                                            : <i className="fa fa-circle red" aria-hidden="true" title="No"
                                                                onClick={() => this.state.permission[1] && this.state.permission[1].status === true && this.handelInfluencerModal(userList.id, '1')}></i>
                                                        }
                                                    </td>
                                                    <td>
                                                        {userList.active_status === 1
                                                            ? <i className="fa fa-circle green" aria-hidden="true" title="Active"
                                                                onClick={() => this.state.permission[1] && this.state.permission[1].status === true && this.handelStatusModal(userList.id, '0')}></i>
                                                            : <i className="fa fa-circle red" aria-hidden="true" title="Inactive"
                                                                onClick={() => this.state.permission[1] && this.state.permission[1].status === true && this.handelStatusModal(userList.id, '1')}></i>
                                                        }
                                                    </td>
                                                    <td>
                                                        <i className="fa fa-eye" aria-hidden="true" title="View" onClick={() => this.handelviewEditModal(userList)}></i>

                                                    </td>
                                                </tr>
                                            ))
                                        )
                                            :
                                            this.state.errorMessge ? <tr>
                                                <td colSpan={12}>
                                                    <p className="text-center">{this.state.errorMessge}</p>
                                                </td>
                                            </tr> : (<tr>
                                                <td colSpan={12}>
                                                    <p className="text-center">No records found</p>
                                                </td>
                                            </tr>)}
                                </tbody>
                            </Table>
                        </div>
                        {this.state.totalCount ? (
                            <Row>
                                <Col md={4} className="d-flex flex-row mt-20">
                                    <span className="mr-2 mt-2 font-weight-500">Items per page</span>
                                    <select
                                        id={this.state.itemPerPage}
                                        className="form-control truncatefloat-left w-90"
                                        onChange={this.handleChangeItemPerPage}
                                        value={this.state.itemPerPage}>
                                        <option value='10'>10</option>
                                        <option value='25'>25</option>
                                        <option value='50'>50</option>
                                        <option value='100'>100</option>
                                    </select>
                                </Col>
                                <Col md={8}>
                                    <div className="paginationOuter text-right">
                                        <Pagination
                                            activePage={this.state.activePage}
                                            itemsCountPerPage={this.state.itemPerPage}
                                            totalItemsCount={this.state.totalCount}
                                            onChange={this.handlePageChange}
                                        />
                                    </div>
                                </Col>
                            </Row>
                        ) : null}
                    </Fragment>
                    : <h5 className="text-center p-3">You do not have any permission to view this content.</h5>}
                {/*========================= Modal for Status change =====================*/}
                <Modal
                    show={this.state.statusChange}
                    onHide={this.handleHide}
                    dialogClassName="modal-90w"
                    aria-labelledby="example-custom-modal-styling-title"
                >
                    <Modal.Body>
                        <div className="m-auto text-center">
                            <h6 className="mb-3">Do you want to change this user status?</h6>
                        </div>
                        {this.state.statuserrorMsg ? <div className="alert alert-danger my-3 text-center col-12" role="alert">{this.state.statuserrorMsg}</div> : null}
                        <div className="m-auto text-center">
                            <button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.handleHide()}>Return</button>
                            {this.state.statuserrorMsg == null ? <button className="btn btn-danger" onClick={() => this.handleStatus(this.state.userId, this.state.status, 'statusChange')}>Confirm</button> : null}
                        </div>

                    </Modal.Body>
                </Modal>
                {/*========================= Modal for set user as Influencer =====================*/}
                <Modal
                    show={this.state.setInfluencer}
                    onHide={this.handleHideInfluencer}
                    dialogClassName="modal-90w"
                    aria-labelledby="example-custom-modal-styling-title"
                >
                    <Modal.Body>
                        <div className="m-auto text-center">
                            <h6 className="mb-3">Do you want to change this user as Influencer?</h6>
                        </div>
                        {this.state.statuserrorMsg ? <div className="alert alert-danger my-3 text-center col-12" role="alert">{this.state.statuserrorMsg}</div> : null}
                        <div className="m-auto text-center">
                            <button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.handleHideInfluencer()}>Return</button>
                            {this.state.statuserrorMsg == null ? <button className="btn btn-danger" onClick={() => this.handleStatus(this.state.userId, this.state.status, 'setInfluencer')}>Confirm</button> : null}
                        </div>

                    </Modal.Body>
                </Modal>

                {/*====== Status change confirmation popup  ===== */}
                <Modal
                    show={this.state.statusConfirMmsg}
                    onHide={this.handleStatusChangedClose}
                    className="payOptionPop"
                >
                    <Modal.Body className="text-center">
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>{this.state.successMessage}</h5>
                            </Col>
                        </Row>
                        <Button
                            onClick={this.handleStatusChangedClose}
                            className="but-gray mt-3"
                        >
                            Return
                        </Button>
                    </Modal.Body>

                </Modal>

                {/*====== Bank Add Edit Modal  ===== */}
                <Modal
                    show={this.state.AddEditModal}
                    onHide={this.handelAddEditModalClose}
                    // className="payOptionPop"
                    // size="lg"
                    className="right half noPadding slideModal"
                >
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body className="text-center">
                        <div>

                            <div className="modalHeader">
                                <Row>
                                    <Col md={12} className="text-left">
                                        <h1>{this.state.modalState === 'add' ? 'Add User' : 'Edit User'}</h1>
                                    </Col>
                                </Row>
                            </div> <div className="modalBody content-body noTabs" > {
                                this.state.modalState === 'add' ?
                                    <div>



                                        {
                                            this.state.addEditBankError
                                            &&
                                            <div className="row">
                                                <div className="col-12">
                                                    <div className="alert alert-danger" role="alert">{this.state.addEditBankError}</div>
                                                </div>
                                            </div>

                                        }


                                        {this.state.BankAddloader === true ? <LoadingSpinner /> :
                                            <div className="">
                                                <Row>
                                                    <Col md={6} className="text-center">
                                                        <div className="form-group">
                                                            <Dropzone

                                                                onDrop={this.onDrop}
                                                            >
                                                                {({ getRootProps, getInputProps }) => (
                                                                    <section>
                                                                        <div {...getRootProps()}>
                                                                            <input {...getInputProps()} />
                                                                            <Fragment>
                                                                                <div className="uploadWrap">
                                                                                    {this.state.uploadLoader ? (
                                                                                        <LoadingSpinner />
                                                                                    ) : ((this.state.imageURL == null || this.state.imageURL == '') ?
                                                                                        (<Image
                                                                                            src={UploadIcon}
                                                                                            alt="upload"
                                                                                        />)
                                                                                        :
                                                                                        (<Image height={250} src={this.state.imageURL} />)
                                                                                        )}


                                                                                    <p className="sm-txt-blue text-center">
                                                                                        Please upload only jpg/png file
                                                                                    </p>
                                                                                </div>
                                                                            </Fragment>
                                                                            <button type="button" className="btn btn-primary btn-position">
                                                                                Upload logo
                                                                            </button>
                                                                        </div>
                                                                    </section>
                                                                )}
                                                            </Dropzone>

                                                            {this.state.uploadErrorMessage ? <div><p className="text-danger mt-3"><span>*</span>{this.state.uploadErrorMessage}</p></div> : null}
                                                        </div>
                                                    </Col>
                                                    <Col md={6} className="text-center">
                                                        <div className="form-group">
                                                            <input type="text" className="form-control" onChange={(e) => this.onChangeBankName(e)} placeholder="Enter a Bank Name" />
                                                            <div className="text-danger text-left">{this.state.bankNameError}</div>
                                                        </div>
                                                    </Col>
                                                </Row>

                                                <Button
                                                    onClick={this.handelAddEditModalClose}
                                                    className="but-gray mt-3"
                                                >
                                                    Return
                                                </Button>
                                                <Button
                                                    onClick={() => this.handelAddBank()}
                                                    className="btn btn-primary ml-3 mt-3"
                                                >
                                                    Submit
                                                </Button>
                                            </div>
                                        }
                                    </div> :
                                    <div>


                                        {this.state.BankAddloader === true ? <LoadingSpinner /> :
                                            <div>
                                                <Row>
                                                    <Col md={6} className="text-center">
                                                        <div className="form-group">
                                                            <Dropzone
                                                                //onDrop={acceptedFiles => console.log(acceptedFiles)}
                                                                onDrop={this.onDrop}
                                                            >
                                                                {({ getRootProps, getInputProps }) => (
                                                                    <section>
                                                                        <div {...getRootProps()}>
                                                                            <input {...getInputProps()} />
                                                                            <Fragment>
                                                                                <div className="uploadWrap">
                                                                                    {this.state.uploadLoader ? (
                                                                                        <LoadingSpinner />
                                                                                    ) : ((this.state.imageURL == null || this.state.imageURL == '') ?
                                                                                        (<Image
                                                                                            src={UploadIcon}
                                                                                            alt="upload"
                                                                                        />)
                                                                                        :
                                                                                        (<Image height={250} src={this.state.imageURL} />)
                                                                                        )}


                                                                                    <p className="sm-txt-blue text-center">
                                                                                        Please upload only jpg/png file
                                                                                    </p>
                                                                                </div>
                                                                            </Fragment>
                                                                            <button type="button" className="btn btn-primary btn-position">
                                                                                Upload logo
                                                                            </button>
                                                                        </div>
                                                                    </section>
                                                                )}
                                                            </Dropzone>
                                                            {/* <input type="file" className="form-control" placeholder="Upload Bank logo"/> */}
                                                            {this.state.uploadErrorMessage ? <p className="text-danger"><span>*</span>{this.state.uploadErrorMessage}</p> : null}
                                                        </div>
                                                    </Col>
                                                    <Col md={6} className="text-center">
                                                        <div className="form-group"><input type="text" className="form-control" onChange={(e) => this.onChangeBankName(e)} placeholder="Enter a Bank Name" />
                                                            <div className="text-danger text-left">{this.state.bankNameError}</div>
                                                            <h5>Bank Name: {this.state.Bankname}</h5>
                                                        </div>
                                                        <div className="form-group">
                                                            <input type="text" className="form-control" onChange={(e) => this.onChangeBankName(e)} placeholder="Enter a Bank Name" />
                                                            <div className="text-danger text-left">{this.state.bankNameError}</div>
                                                        </div>
                                                    </Col>
                                                </Row>

                                                <Button
                                                    onClick={this.handelAddEditModalClose}
                                                    className="but-gray mt-3"
                                                >
                                                    Return
                                                </Button>
                                                <Button
                                                    onClick={() => this.handelEditUser()}
                                                    className="btn btn-primary ml-3 mt-3"
                                                >
                                                    Submit
                                                </Button>
                                            </div>
                                        }
                                    </div>
                            } </div>



                        </div>
                    </Modal.Body>

                </Modal>

                { /* Edit Cms Modal */}
                <Modal
                    show={this.state.viewEditModal}
                    onHide={this.handelviewEditModalClose}
                    className="right full noPadding slideModal"
                >
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body className="">
                        {/* <div className="modalHeader">
                            <Row>
                                <Col md={9}>
                                    <h1>Edit Gemster</h1>
                                </Col>
                            </Row>
                        </div> */}
                        <div className="modalBody content-body noTabs">
                            <EditBusinessCustomers
                                {...this.state.vendorData}
                                handelviewEditModalClose={this.handelviewEditModalClose}
                                handleEditConfirMmsg={this.handleEditConfirMmsg} />
                        </div>
                    </Modal.Body>
                </Modal>

                <Modal
                    show={this.state.editConfirMmsg}
                    onHide={this.handleEditConfirMmsgClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>User Profile updated successfully</h5>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <Button
                                    onClick={this.handleEditConfirMmsgClose}
                                    className="but-gray"
                                >
                                    Return
                                </Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

            </div>
        );
    }

}

const mapStateToPros = state => {
    return {
        facility: state.auth.facility
    };
};
User.propTypes = {
    facility: PropTypes.any
};
export default connect(mapStateToPros, null)(User);

