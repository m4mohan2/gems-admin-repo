import React, { Component } from 'react';
import axios from '../../../../../shared/eaxios';
import * as AppConst from './../../../../../common/constants';
import refreshIcon from './../../../../../assets/refreshIcon.png';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import PropTypes from 'prop-types';
import { Col, Row, Image, FormGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Moment from 'moment';
import { Line } from 'react-chartjs-2';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

class Chart extends Component {
    state = {
        chartLoader: false,
        chartErrorMessge: '',
        gemsId: '',
        fromDate: new Date(Moment(new Date()).subtract(1, 'months').toISOString()),
        toDate: new Date(),
        likeData: [],
        viewData: [],
        commentData: [],
        shareData: [],
        saveData: [],
    };

    static getDerivedStateFromProps(props, state) {
        if (!state.gemsData) {
            return {
                ...props
            };
        }
    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    componentDidMount() {
        const gemsData = { ...this.props };
        const gemsId = gemsData.id;
        this.setState({ gemsId });
        this.handleAnalysis(gemsId, Moment(this.state.fromDate).format('yyyy-MM-DD'), Moment(this.state.toDate).format('yyyy-MM-DD'));
    }

    handleAnalysis = (id, fromDate, toDate) => {
        this.setState({ chartLoader: true });
        axios.get(AppConst.APIURL + `api/gemsAnalysisByLike/${id}?from_date=${fromDate}&to_date=${toDate}`)
            .then(res => {
                this.setState({ likeData: res.data });
            });
        axios.get(AppConst.APIURL + `api/gemsAnalysisByView/${id}?from_date=${fromDate}&to_date=${toDate}`)
            .then(res => {
                this.setState({ viewData: res.data });
            });
        axios.get(AppConst.APIURL + `api/gemsAnalysisByComment/${id}?from_date=${fromDate}&to_date=${toDate}`)
            .then(res => {
                this.setState({ commentData: res.data });
            });
        axios.get(AppConst.APIURL + `api/gemsAnalysisBySave/${id}?from_date=${fromDate}&to_date=${toDate}`)
            .then(res => {
                this.setState({ saveData: res.data });
            });
        axios.get(AppConst.APIURL + `api/gemsAnalysisByShare/${id}?from_date=${fromDate}&to_date=${toDate}`)
            .then(res => {
                this.setState({ shareData: res.data, chartLoader: false });
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    chartLoader: false,
                    chartErrorMessge: errorMsg,
                });
            });
    }

    handleSearchSubmit = (e) => {
        e.preventDefault();
        if (this.state.fromDate === '' || this.state.toDate === '') {
            this.setState({ chartErrorMessge: 'Please enter from date and to date to get the chart' });
        } else {
            this.handleAnalysis(this.state.gemsId, Moment(this.state.fromDate).format('yyyy-MM-DD'), Moment(this.state.toDate).format('yyyy-MM-DD'));
        }
    }

    handleFromDate = date => {
        if (date > this.state.toDate) {
            this.setState({ fromDate: date, toDate: '', chartErrorMessge: '' });
        } else {
            this.setState({ fromDate: date, chartErrorMessge: '' });
        }
    }

    handleToDate = date => {
        this.setState({ toDate: date, chartErrorMessge: '' });
    }

    resetSearch = () => {
        const fromDate = new Date(Moment(new Date()).subtract(1, 'months').toISOString());
        const toDate = new Date();
        this.setState({ fromDate, toDate }, () => {
            this.handleAnalysis(this.state.gemsId, Moment(this.state.fromDate).format('yyyy-MM-DD'), Moment(this.state.toDate).format('yyyy-MM-DD'));
        });
    }

    render() {
        let dates = [];
        let totalLike = [];
        let countLike = 0;
        for (let i = 0; i < this.state.likeData.length; i++) {
            dates.push(Moment(this.state.likeData[i].date).format('Do MMM'));
            totalLike.push(this.state.likeData[i].total);
            countLike = countLike + this.state.likeData[i].total;
        }
        const optionsForLike = {
            labels: dates,
            datasets: [
                {
                    label: 'Likes',
                    fill: false,
                    lineTension: 0.5,
                    backgroundColor: '#24305E',
                    borderColor: '#F8E9A1',
                    borderWidth: 2,
                    data: totalLike,
                }
            ]
        };

        let viewdates = [];
        let viewtotalLike = [];
        let countView = 0;
        for (let i = 0; i < this.state.viewData.length; i++) {
            viewdates.push(Moment(this.state.viewData[i].date).format('Do MMM'));
            viewtotalLike.push(this.state.viewData[i].total);
            countView = countView + this.state.viewData[i].total;
        }
        const optionsForView = {
            labels: viewdates,
            datasets: [
                {
                    label: 'Views',
                    fill: false,
                    lineTension: 0.5,
                    backgroundColor: '#24305E',
                    borderColor: '#A64AC9',
                    borderWidth: 2,
                    data: viewtotalLike,
                }
            ]
        };

        let commentdates = [];
        let commenttotalLike = [];
        let countComment = 0;
        for (let i = 0; i < this.state.commentData.length; i++) {
            commentdates.push(Moment(this.state.commentData[i].date).format('Do MMM'));
            commenttotalLike.push(this.state.commentData[i].total);
            countComment = countComment + this.state.commentData[i].total;
        }
        const optionsForComment = {
            labels: commentdates,
            datasets: [
                {
                    label: 'Comment',
                    fill: false,
                    lineTension: 0.5,
                    backgroundColor: '#24305E',
                    borderColor: '#F76C6C',
                    borderWidth: 2,
                    data: commenttotalLike,
                }
            ]
        };

        let sharedates = [];
        let sharetotalLike = [];
        let countShare = 0;
        for (let i = 0; i < this.state.shareData.length; i++) {
            sharedates.push(Moment(this.state.shareData[i].date).format('Do MMM'));
            sharetotalLike.push(this.state.shareData[i].total);
            countShare = countShare + this.state.shareData[i].total;
        }
        const optionsForShare = {
            labels: sharedates,
            datasets: [
                {
                    label: 'Share',
                    fill: false,
                    lineTension: 0.5,
                    backgroundColor: '#24305E',
                    borderColor: '#248B74',
                    borderWidth: 2,
                    data: sharetotalLike,
                }
            ]
        };

        let savedates = [];
        let savetotalLike = [];
        let countSave = 0;
        for (let i = 0; i < this.state.saveData.length; i++) {
            savedates.push(Moment(this.state.saveData[i].date).format('Do MMM'));
            savetotalLike.push(this.state.saveData[i].total);
            countSave = countSave + this.state.saveData[i].total;
        }
        const optionsForSave = {
            labels: savedates,
            datasets: [
                {
                    label: 'Save',
                    fill: false,
                    lineTension: 0.5,
                    backgroundColor: '#24305E',
                    borderColor: 'orange',
                    borderWidth: 2,
                    data: savetotalLike,
                }
            ]
        };
        return (
            <div className="addcustomerSec">
                <Row>
                    <Col sm={12}>
                        {this.state.chartLoader ? <LoadingSpinner /> : null}
                    </Col>
                </Row>
                <form onSubmit={this.handleSearchSubmit}>
                    <div className="searchBox">
                        <div className="row">
                            <div className="col-md-4">
                                <FormGroup controlId="formControlsTextarea">
                                    <DatePicker
                                        name='fromDate'
                                        selected={this.state.fromDate}
                                        onChange={this.handleFromDate}
                                        className='form-control'
                                        maxDate={new Date()}
                                        required
                                        dateFormat="dd-MM-yyyy"
                                        showMonthDropdown
                                        showYearDropdown
                                        placeholderText="From date"
                                    />
                                </FormGroup>
                            </div>
                            <div className="col-md-4">
                                <FormGroup controlId="formControlsTextarea">
                                    <DatePicker
                                        name='toDate'
                                        selected={this.state.toDate}
                                        onChange={this.handleToDate}
                                        className='form-control'
                                        maxDate={new Date()}
                                        minDate={this.state.fromDate}
                                        required
                                        dateFormat="dd-MM-yyyy"
                                        showMonthDropdown
                                        showYearDropdown
                                        placeholderText="To date"
                                    />
                                </FormGroup>
                            </div>
                            <div className="col-md-4">
                                <input type="submit" value="Search" className="btn btn-primary" />
                                <Link to="#" className='ml-3 mt-2'>
                                    <Image src={refreshIcon} onClick={() => this.resetSearch()} />
                                </Link>
                            </div>
                        </div>
                        {this.state.chartErrorMessge ? (
                            <div className="text-center bold mt-3 text-success">
                                {this.state.chartErrorMessge}
                            </div>
                        ) : null}
                    </div>
                </form>
                <Row className="mb-3">
                    <Col sm={6} xs={12} className="">
                        <Line
                            data={optionsForLike}
                            options={{
                                responsive: true,
                                maintainAspectRatio: true,
                                title: {
                                    display: true,
                                    text: 'Likes: ' + countLike,
                                    fontSize: 16,
                                    fontColor: 'white'
                                },
                                legend: {
                                    display: true,
                                    position: 'right',
                                    fontColor: 'white'
                                },
                                scales: {
                                    xAxes: [{
                                        ticks: {
                                            beginAtZero: true,
                                            min: 0,
                                            fontColor: '#BAC2C5',
                                        },
                                        gridLines: {
                                            color: '#BAC2C5',
                                            zeroLineColor: '#BAC2C5'
                                        },
                                    }],
                                    yAxes: [{
                                        gridLines: {
                                            color: '#BAC2C5',
                                            zeroLineColor: '#BAC2C5'
                                        },
                                        ticks: {
                                            stepSize: 1,
                                            fontColor: '#BAC2C5',
                                        },
                                    }]
                                }
                            }}
                        />
                    </Col>
                    <Col sm={6} xs={12} className="">
                        <Line
                            data={optionsForView}
                            options={{
                                title: {
                                    display: true,
                                    text: 'View: ' + countView,
                                    fontSize: 16,
                                    fontColor: 'white'
                                },
                                legend: {
                                    display: true,
                                    position: 'right'
                                },
                                scales: {
                                    xAxes: [{
                                        ticks: {
                                            beginAtZero: true,
                                            min: 0,
                                            fontColor: '#BAC2C5',
                                        },
                                        gridLines: {
                                            color: '#BAC2C5',
                                            zeroLineColor: '#BAC2C5'
                                        },
                                    }],
                                    yAxes: [{
                                        gridLines: {
                                            color: '#BAC2C5',
                                            zeroLineColor: '#BAC2C5'
                                        },
                                        ticks: {
                                            stepSize: 1,
                                            fontColor: '#BAC2C5',
                                        },
                                    }]
                                }
                            }}
                        />
                    </Col>
                </Row>
                <Row className="mb-3">
                    <Col sm={6} xs={12} className="">
                        <Line
                            data={optionsForSave}
                            options={{
                                title: {
                                    display: true,
                                    text: 'Save: ' + countSave,
                                    fontSize: 16,
                                    fontColor: 'white'
                                },
                                legend: {
                                    display: true,
                                    position: 'right'
                                },
                                scales: {
                                    xAxes: [{
                                        ticks: {
                                            beginAtZero: true,
                                            min: 0,
                                            fontColor: '#BAC2C5',
                                        },
                                        gridLines: {
                                            color: '#BAC2C5',
                                            zeroLineColor: '#BAC2C5'
                                        },
                                    }],
                                    yAxes: [{
                                        gridLines: {
                                            color: '#BAC2C5',
                                            zeroLineColor: '#BAC2C5'
                                        },
                                        ticks: {
                                            stepSize: 1,
                                            fontColor: '#BAC2C5',
                                        },
                                    }]
                                }
                            }}
                        />
                    </Col>
                    <Col sm={6} xs={12} className="">
                        <Line
                            data={optionsForShare}
                            options={{
                                title: {
                                    display: true,
                                    text: 'Share: ' + countShare,
                                    fontSize: 16,
                                    fontColor: 'white'
                                },
                                legend: {
                                    display: true,
                                    position: 'right'
                                },
                                scales: {
                                    xAxes: [{
                                        ticks: {
                                            beginAtZero: true,
                                            min: 0,
                                            fontColor: '#BAC2C5',
                                        },
                                        gridLines: {
                                            color: '#BAC2C5',
                                            zeroLineColor: '#BAC2C5'
                                        },
                                    }],
                                    yAxes: [{
                                        gridLines: {
                                            color: '#BAC2C5',
                                            zeroLineColor: '#BAC2C5'
                                        },
                                        ticks: {
                                            stepSize: 1,
                                            fontColor: '#BAC2C5',
                                        },
                                    }]
                                }
                            }}
                        />
                    </Col>
                </Row>
                <Row className="mb-3">
                    <Col sm={12} xs={12} className="">
                        <Line
                            data={optionsForComment}
                            options={{
                                title: {
                                    display: true,
                                    text: 'Comment: ' + countComment,
                                    fontSize: 16,
                                    fontColor: 'white'
                                },
                                legend: {
                                    display: true,
                                    position: 'right'
                                },
                                scales: {
                                    xAxes: [{
                                        ticks: {
                                            beginAtZero: true,
                                            min: 0,
                                            fontColor: '#BAC2C5',
                                        },
                                        gridLines: {
                                            color: '#BAC2C5',
                                            zeroLineColor: '#BAC2C5'
                                        },
                                    }],
                                    yAxes: [{
                                        gridLines: {
                                            color: '#BAC2C5',
                                            zeroLineColor: '#BAC2C5'
                                        },
                                        ticks: {
                                            stepSize: 1,
                                            fontColor: '#BAC2C5',
                                        },
                                    }]
                                }
                            }}
                        />
                    </Col>
                </Row>
            </div>
        );
    }
}

Chart.propTypes = {
    gemsId: PropTypes.any,
};

export default Chart;