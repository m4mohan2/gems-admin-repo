/* eslint-disable no-useless-escape */
//import { Formik } from 'formik';
import React, { Component } from 'react';

//import * as Yup from 'yup';
//import axios from '../../../../../shared/eaxios';
//import * as AppConst from './../../../../../common/constants';
import PropTypes from 'prop-types';

//RichTextToolBar
//import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import * as AppConst from './../../../../../common/constants';

class EditGems extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showVendor: false,
            errorMessge: null,
            editVendorEnable: false,
            disabled: false,
            editVendorLoader: false,
            editErrorMessge: false,
            editorStateForShortDesc: EditorState.createEmpty(),
            editorStateForDesc: EditorState.createEmpty()
        };
    }

    static getDerivedStateFromProps(props, state) {
        console.log(' getDerivedStateFromProps : ', props, state);
        if (!state.vendorData) {
            return {
                ...props
            };
        }
    }

    handleEditVendorEnable = () => {
        this.setState({
            editVendorEnable: true,
            disabled: true
        });
    }

    handleEditVendorDisable = () => {
        this.setState({
            editVendorEnable: false,
            disabled: false
        });
        this.props.onReload(this.state.vendorData);
    }

    handleCloseVendor = () => {
        this.props.onClick();
    };


    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }



    onEditorStateChangeShortDescription = (editorStateForShortDesc) => {
        this.setState({
            editorStateForShortDesc,
        });
    };

    onEditorStateChangeDescription = (editorStateForDesc) => {
        this.setState({
            editorStateForDesc,
        });
    };


    render() {
        const initialValues = { ...this.props };
        return (

                            <table responsive>
                                <tr>
                                    <td width="30%"><b>Name</b></td>
                                    <td width="10%">:</td>
                                    <td width="60%">{initialValues.name}</td>
                                </tr>
                                <tr>
                                    <td width="30%"><b>Latitude</b></td>
                                    <td width="10%">:</td>
                                    <td width="60%">{initialValues.lat}</td>
                                </tr>
                                <tr>
                                    <td width="30%"><b>Longitude</b></td>
                                    <td width="10%">:</td>
                                    <td width="60%">{initialValues.lng}</td>
                                </tr>
                                <tr>
                                    <td width="30%"><b>Phone No</b></td>
                                    <td width="10%">:</td>
                                    <td width="60%">{initialValues.phone_no}</td>
                                </tr>                                
                                <tr>
                                    <td width="30%"><b>Status</b></td>
                                    <td width="10%">:</td>
                                    <td width="60%">{initialValues.status==1 ? 'Active' : 'Inactive' }</td>
                                </tr>
                                <tr>
                                    <td width="30%"><b>Created By</b></td>
                                    <td width="10%">:</td>
                                    <td width="60%">{initialValues.created_by_user_detail.first_name } {initialValues.created_by_user_detail.last_name }</td>
                                </tr>
                                <tr>
                                    <td width="30%" valign="top"><b>Category</b></td>
                                    <td width="10%" valign="top">:</td>
                                    <td width="60%" valign="top">{initialValues.category_detail.length > 0 ? (initialValues.category_detail.map(catList => (
                                        <div key={catList.id}>{catList.category_name}<br></br></div> 
                                        ) )) : ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="30%" valign="top"><b>Note</b></td>
                                    <td width="10%" valign="top">:</td>
                                    <td width="60%" valign="top">{initialValues.note}</td>
                                </tr>
                                <tr>
                                    <td width="30%" valign="top"><b>Image</b></td>
                                    <td width="10%" valign="top">:</td>
                                    <td width="60%" valign="top">{initialValues.images.length > 0 ? (initialValues.images.map(imageList => (
                                        <div key={imageList.id}>{
                                            imageList.source == 'image_file' 
                                            ? <img width="40%" height="40%" src={AppConst.UPLOADURL + 'gems/' + imageList.image}></img> 
                                            : <img width="40%" height="40%" src={imageList.image}></img>
                                        }<br></br>
                                        </div> 
                                        ) )) : ''}
                                    </td>
                                </tr>

                            </table>                        
        );
    }
}

EditGems.propTypes = {
    globState: PropTypes.object,
    onClickAction: PropTypes.func,
    onReload: PropTypes.func,
    //onSuccess: PropTypes.func,
    onClick: PropTypes.func,
    handelviewEditModalClose: PropTypes.func,
    handleEditConfirMmsg: PropTypes.func,

};

export default EditGems;