import React, { Component, Fragment } from 'react';
import axios from '../../../../../shared/eaxios';
import {
	Table,
	Row,
	Modal,
	Col,
	Image,
	Button,
} from 'react-bootstrap';
// import { FaCaretDown } from 'react-icons/fa';
// import { FaCaretUp } from 'react-icons/fa';
import Pagination from 'react-js-pagination';
//import { Link } from 'react-router-dom';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
//import refreshIcon from './../../../../../assets/refreshIcon.png';
import SuccessIco from './../../../../../assets/success-ico.png';
import Dropzone from 'react-dropzone';
import UploadIcon from './../../../../../assets/uploading.png';
import * as AppConst from './../../../../../common/constants';
import EditBusinessCustomers from './gem-details';
import { Link } from 'react-router-dom';
import refreshIcon from './../../../../../assets/refreshIcon.png';

import GooglePlacesAutocomplete from 'react-google-places-autocomplete';
import { geocodeByAddress, getLatLng } from 'react-google-places-autocomplete';
import Chart from './Chart';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class Bank extends Component {

	state = {
		gemsLists: [],
		activePage: 1,
		totalCount: 0,
		itemPerPage: 10,
		loading: false,
		errorMessge: null,
		BankSearchKey: '',
		statusChange: false,
		bankId: null,
		status: null,
		sort: 1,
		field: 'bankname',
		statusConfirMmsg: false,
		statuserrorMsg: null,
		sortingActiveID: 1,
		AddEditModal: false,
		Bankname: '',
		uploadLoader: false,
		imageURL: null,
		uploadErrorMessage: null,
		uploadFile: null,
		bankNameError: null,
		successMessage: null,
		BankAddloader: false,
		selectedBankId: null,
		modalState: null,
		addEditBankError: false,
		location: '',
		gemsName: '',
		errorMessageForSearch: '',
		latitude: '',
		longitude: '',
		latitudeForSearch: '',
		longitudeForSearch: '',
		viewChartModel: false,
		permission: [],
		statusForSearch: '',
	};

	resetSearch = () => {
		this.setState({ statusForSearch: '', location: '', gemsName: '', latitude: '', longitude: '', activePage: 1, errorMessageForSearch: '', sort: 1 }, () => {
			this.fetchBankList();
		});
	}

	displayError = (e) => {
		let errorMessge = '';
		try {
			errorMessge = e.data.message ? e.data.message : e.data.error_description;
		} catch (e) {
			errorMessge = 'Unknown error!';
		}
		return errorMessge;
	}

	handleHide = () => {
		this.setState({
			statusChange: false,
			statuserrorMsg: null
		});
	}

	handleDeleteHide = () => {
		this.setState({
			deleteGem: false,
			deleteerrorMsg: null
		});
	}

	handelStatusModal = (id, status) => {
		this.setState({
			statusChange: true,
			bankId: id,
			status: status
		});
	}

	handelDeleteModal = (id) => {
		this.setState({
			deleteGem: true,
			gemsId: id
		});
	}

	onChangeBankName = (e) => {
		let str = e.target.value.trim();
		this.setState({
			bankNameError: null,
			Bankname: str
		});
	}

	_isMounted = false;

	fetchBankList = (sort = this.state.sort, field = this.state.field,) => {
		this.setState({
			loading: true,
			sort: sort,
			field: field
		}, () => {
			axios.get(AppConst.APIURL + `/api/gemList?gems_name=${this.state.gemsName}&latitude=${this.state.latitude}&longitude=${this.state.longitude}&pageSize=${this.state.itemPerPage}&page=${this.state.sort}&status=${this.state.statusForSearch}`)
				.then(res => {
					const gemsLists = res.data.data;
					const totalCount = res.data.total;
					if (this._isMounted && gemsLists) {
						this.setState({
							gemsLists: gemsLists,
							totalCount: totalCount,
							loading: false,
						}, () => {
						});
					}
				})
				.catch(e => {
					let errorMsg = this.displayError(e);
					this.setState({
						errorMessge: errorMsg,
						loading: false
					});
					setTimeout(() => {
						this.setState({ errorMessge: null });
					}, 5000);
				});
		});
	};

	handlePageChange = pageNumber => {
		this.setState({ activePage: pageNumber });
		this.fetchBankList(pageNumber > 0 ? pageNumber : 0, this.state.field);
	};

	handleChangeItemPerPage = (e) => {
		this.setState({ itemPerPage: e.target.value },
			() => {
				this.fetchBankList();
			});
	}

	resetPagination = () => {
		this.setState({ activePage: 1 });
	}

	componentDidMount() {
		this._isMounted = true;
		const facility = this.props.facility;
		const permission = [];
		facility.map(data => {
			data.name === 'gems' && data.permission.length !== 0 && permission.push(...data.permission);
		});
		this.setState({ permission }, () => {
			permission[1].status === true &&
				this.fetchBankList();
		});
	}

	componentWillUnmount() {
		this._isMounted = false;
	}

	handleStatus(id, status) {

		let updateArray = {
			'status': status
		};

		axios.put(AppConst.APIURL + `/api/statusUpdateGem/${id}`, updateArray)
			.then(res => {
				console.log('--------------res------', status);
				console.log('--------------res------', res);
				this.setState({
					bankId: null,
					status: null,
					successMessage: 'Status successfully changed',
					statusConfirMmsg: true
				});
				this.handleHide();
				this.fetchBankList();
			}

			)
			.catch(e => {
				let errorMsg = this.displayError(e);
				this.setState({
					statuserrorMsg: errorMsg,
					loading: false


				});
				setTimeout(() => {
					this.setState({ errorMessge: null });
				}, 5000);
			});

	}

	handleStatusChangedClose = () => {
		this.setState({
			statusConfirMmsg: false,
			successMessage: null
		});
	}

	handleDelete(id) {
		axios.delete(AppConst.APIURL + `/api/deleteGem/${id}`)
			.then(res => {
				console.log('--------------res------', res);
				this.setState({
					gemsId: null,
					successMessage: 'Record deleted successfully',
					deleteConfirMmsg: true
				});
				this.handleDeleteHide();
				this.fetchBankList();
			}

			)
			.catch(e => {
				let errorMsg = this.displayError(e);
				this.setState({
					deleteerrorMsg: errorMsg,
					loading: false


				});
				setTimeout(() => {
					this.setState({ errorMessge: null });
				}, 5000);
			});

	}

	handleDeleteChangedClose = () => {
		this.setState({
			deleteConfirMmsg: false,
			successMessage: null
		});
	}

	handelAddEditModalClose = () => {
		this.setState({
			AddEditModal: false,
			imageURL: null,
			bankNameError: null,
			modalState: null,
			Bankname: '',
			selectedBankId: null,
			uploadErrorMessage: null,
			uploadFile: null
		});
	}

	handelAddEditBankModal = (param, name, id, url) => {
		console.log('param, name, id, url', param, name, id, url);
		this.setState({
			AddEditModal: true,
			modalState: param
		});
		if (param === 'edit') {
			this.setState({
				Bankname: name,
				selectedBankId: id,
				imageURL: url
			});
		}

	}

	handelEditBank = () => {
		const config = {
			headers: {
				'Content-Type': 'multipart/form-data',
			}
		};

		let formData = new window.FormData();
		formData.append('file', this.state.uploadFile);

		axios
			.put(`/banklist/update/${this.state.selectedBankId}`, formData, config)
			.then(res => {
				this.handelAddEditModalClose();
				console.log(res);
				if (this._isMounted) {
					this.setState({
						BankAddloader: false,
						successMessage: 'Logo has been successfully Edited',
						statusConfirMmsg: true,
						activePage: 1

					}, () => { this.fetchBankList(0, '', ''); });
				}

			})
			.catch(err => {
				let errorMessage = this.displayError(err);
				this.setState({
					uploadLoader: false,
					addEditBankError: errorMessage
				});
				setTimeout(() => {
					this.setState({ addEditBankError: null });
				}, 5000);
			});

	}


	handelAddBank = () => {
		console.log('this.state.uploadFile', this.state.uploadFile);
		console.log('this.state.Bankname', this.state.Bankname);

		if (this.state.Bankname != null && this.state.Bankname != '') {
			if (this.state.uploadFile != null) {

				this.setState({
					BankAddloader: true
				});

				const config = {
					headers: {
						'Content-Type': 'multipart/form-data',
					}
				};

				let formData = new window.FormData();
				formData.append('file', this.state.uploadFile);
				formData.append('bankName', this.state.Bankname);

				axios
					.post('/banklist/add', formData, config)
					.then(res => {
						this.handelAddEditModalClose();
						console.log(res);
						if (this._isMounted) {
							this.setState({
								uploadLoader: false,
								BankAddloader: false,
								successMessage: 'Record has been successfully Added',
								statusConfirMmsg: true,
							}, () => this.fetchBankList());
						}

					})
					.catch(err => {
						let errorMessage = this.displayError(err);
						console.log(err);
						this.setState({
							uploadLoader: false,
							BankAddloader: false,
							addEditBankError: errorMessage
						});


						setTimeout(() => {
							this.setState({ addEditBankError: null });
						}, 5000);
					});

			}
			else {
				this.setState({
					uploadErrorMessage: ' Please upload a logo',
				});
			}

		}
		else {
			this.setState({
				bankNameError: '* Please enter a bank name'
			});
		}

	}

	onDrop = async files => {

		this.setState({
			files,
			dropzoneActive: false,
			isDrop: true
		});

		let reader = '';
		if (files.length > 0) {
			console.log('image details', files[0]);


			// FILE SIZE RESTRICTION
			if (files[0]['size'] > 600000) {
				this.setState({
					uploadErrorMessage: 'File size should be less than 600KB',
					isDrop: false,
				});
				setTimeout(() => {
					this.setState({ uploadErrorMessage: null });
				}, 5000);
			}
			else if ((files[0]['type'] !== 'image/png') && (files[0]['type'] !== 'image/jpeg')) {
				this.setState({
					uploadErrorMessage: 'Please upload jpeg/png file',
					isDrop: false,
				});
				setTimeout(() => {
					this.setState({ uploadErrorMessage: null });
				}, 5000);
			}


			else {

				reader = new window.FileReader();
				reader.readAsDataURL(files[0]);

				reader.onload = () => {
					let urlString = reader.result;

					this.setState({
						uploadLoader: false,
						imageURL: urlString,
						uploadFile: files[0]
					});
				};


			}

		}

	};

	handelviewEditModal = (id) => {
		axios
			.get(AppConst.APIURL + `/api/gemsDetails/${id}`)
			.then(res => {
				console.log('gems Details@@', res.data.message);
				this.setState({
					viewEditModal: true,
					vendorData: res.data.message
				}, () => {
					console.log('this.state.viewEditData@@', this.state.vendorData);
				});

			})
			.catch(err => {
				let errorMessage = this.displayError(err);
				this.setState({
					uploadLoader: false,
					addEditBankError: errorMessage
				});
				setTimeout(() => {
					this.setState({ addEditBankError: null });
				}, 5000);
			});




		// this.setState({
		//     viewEditModal: true,
		//     vendorData: data
		// }, () => {
		//     console.log('this.state.viewEditData@@', this.state.vendorData);
		// });
	}

	handelviewEditModalClose = () => {
		this.setState({
			viewEditModal: false,
		});
	}

	handleEditConfirMmsg = () => {
		this.setState({
			viewEditModal: false,
			editConfirMmsg: true
		}, () => {
			this.fetchUserList();
		});
	}

	handleEditConfirMmsgClose = () => {

		this.setState({
			editConfirMmsg: false
		});
	}

	handleSearchSubmit = (e) => {
		e.preventDefault();
		if (this.state.gemsName === '' && this.state.location === '' && this.state.statusForSearch === '') {
			this.setState({ errorMessageForSearch: 'Any one field is required to search Gems.' });
		} else {
			this.setState({ loading: true, sort: 1 }, () => {
				axios.get(AppConst.APIURL + `/api/gemList?gems_name=${this.state.gemsName}&latitude=${this.state.latitudeForSearch}&longitude=${this.state.longitudeForSearch}&pageSize=${this.state.itemPerPage}&page=${this.state.sort}&status=${this.state.statusForSearch}`)
					.then(response => {
						this.setState({
							gemsLists: response.data.data,
							totalCount: response.data.total,
							loading: false,
							latitude: this.state.latitudeForSearch,
							longitude: this.state.longitudeForSearch,
							latitudeForSearch: '',
							longitudeForSearch: '',
							location: '',
							activePage: response.data.current_page
						});
					});
			});
		}
	}

	handleChange = (event) => {
		this.setState({ [event.target.name]: event.target.value, errorMessageForSearch: '' });
	}

	handleLngLon = (description) => {
		geocodeByAddress(description)
			.then(results => getLatLng(results[0]))
			.then(({ lat, lng }) =>
				this.setState({
					latitudeForSearch: lat,
					longitudeForSearch: lng,
					location: description,
					errorMessageForSearch: ''
				})
			);
	}

	handelChartModal = (data) => {
		this.setState({
			viewChartModel: true,
			gemsData: data,
			gemsName: data.name
		});
	}

	handelChartModalClose = () => {
		this.setState({
			viewChartModel: false,
			gemsName: ''
		});
	}

	render() {
		return (
			<div className="dashboardInner businessOuter pt-3">
				{this.state.permission[1] && this.state.permission[1].status === true ?
					<Fragment>
						<form onSubmit={this.handleSearchSubmit}>
							<div className="searchBox">
								<div className="row">
									<div className="col-12 col-md-3">
										<input className="form-control" type="text" name="gemsName" value={this.state.gemsName} onChange={this.handleChange} placeholder="Enter gems name" />
									</div>
									<div className="col-12 col-md-3">
										<GooglePlacesAutocomplete
											placeholder='Location'
											inputClassName={'form-control'}
											suggestionsClassNames={
												{
													container: 'locationDrop',
													suggestion: 'custom-suggestion-classname cursorPointer',
													suggestionActive: 'custom-suggestions-classname--active',
												}
											}
											onSelect={({ description }) => (this.handleLngLon(description))}
											initialValue={this.state.location}
											loader={<LoadingSpinner />}
										/>
									</div>
									<div className="col-12 col-md-3">
										<select className="form-control" name="statusForSearch" value={this.state.statusForSearch} onChange={this.handleChange}>
											<option key="" value="">Approve Status</option>
											<option value="1" key="1">Active</option>
											<option value="0" key="0">Inactive</option>
										</select>
									</div>
									<div className="col-12 col-md-3">
										<input type="submit" value="Search" className="btn btn-primary" />
										<Link to="#" className='ml-3 mt-2'>
											<Image src={refreshIcon} onClick={() => this.resetSearch()} />
										</Link>
									</div>
								</div>
								{this.state.errorMessageForSearch &&
									<div className="text-success text-center mt-3">
										{this.state.errorMessageForSearch}
									</div>}
							</div>
						</form>

						{this.state.errorMessge
							?
							<div className="alert alert-danger col-12" role="alert">
								ERROR : {this.state.errorMessge}
							</div>
							:
							null
						}
						<div className="boxBg">
							<Table responsive hover>
								<thead className="theaderBg">
									<tr>
										<th>Gems Name</th>
										<th>Address</th>
										<th>Phone No</th>
										<th>Note</th>
										<th className="center">Approve Status</th>
										<th className="center">Action</th>
									</tr>
								</thead>
								<tbody>
									{this.state.loading ? (<tr>
										<td colSpan={12}>
											<LoadingSpinner />
										</td>
									</tr>) :
										this.state.gemsLists.length > 0 ? (
											this.state.gemsLists.map(gemList => (
												<tr key={gemList.id}>
													<td>{gemList.name}</td>
													<td>{gemList.address}</td>
													<td>{gemList.phone_no}</td>
													<td>{(gemList.note != null) ? gemList.note.substring(0, 50) : ''}</td>
													<td className="center">
														{gemList.status === 1
															? <i className="fa fa-circle green" aria-hidden="true" title="Active"
																onClick={() => this.state.permission[2] && this.state.permission[2].status === true && this.handelStatusModal(gemList.id, '0')}></i>
															: <i className="fa fa-circle red" aria-hidden="true" title="Inactive"
																onClick={() => this.state.permission[2] && this.state.permission[2].status === true && this.handelStatusModal(gemList.id, '1')}></i>
														}
													</td>
													<td className="center">
														{this.state.permission[0] && this.state.permission[0].status === true &&
															<i className="fa fa-bar-chart" aria-hidden="true" title="View" onClick={() => this.handelChartModal(gemList)}></i>
														}
												&nbsp;
															<i className="fa fa-eye" aria-hidden="true" title="View" onClick={() => this.handelviewEditModal(gemList.id)}></i>

												&nbsp;
												{this.state.permission[3] && this.state.permission[3].status === true &&
															<i className="fa fa-trash-o red" aria-hidden="true" title="Delete" onClick={() => this.handelDeleteModal(gemList.id)}></i>
														}
													</td>
												</tr>
											))
										)
											:
											this.state.errorMessge ? <tr>
												<td colSpan={12}>
													<p className="text-center">{this.state.errorMessge}</p>
												</td>
											</tr> : (
													<tr>
														<td colSpan={12}>
															<p className="text-center">No records found</p>
														</td>
													</tr>
												)
									}
								</tbody>
							</Table>
						</div>
						{this.state.totalCount ? (
							<Row>
								<Col md={4} className="d-flex flex-row mt-20">
									<span className="mr-2 mt-2 font-weight-500">Items per page</span>
									<select
										id={this.state.itemPerPage}
										className="form-control truncatefloat-left w-90"
										onChange={this.handleChangeItemPerPage}
										value={this.state.itemPerPage}>
										<option value='10'>10</option>
										<option value='25'>25</option>
										<option value='50'>50</option>
										<option value='100'>100</option>
									</select>
								</Col>
								<Col md={8}>
									<div className="paginationOuter text-right">
										<Pagination
											activePage={this.state.activePage}
											itemsCountPerPage={this.state.itemPerPage}
											totalItemsCount={this.state.totalCount}
											onChange={this.handlePageChange}
										/>
									</div>
								</Col>
							</Row>
						) : null}
					</Fragment>
					: <h5 className="text-center p-3">You do not have any permission to view this content.</h5>
				}
				{/*========================= Modal for Status change =====================*/}
				<Modal
					show={this.state.statusChange}
					onHide={this.handleHide}
					dialogClassName="modal-90w"
					aria-labelledby="example-custom-modal-styling-title"
				>
					<Modal.Body>
						<div className="m-auto text-center">
							<h6 className="mb-3 text-dark">Do you want to change this status?</h6>
						</div>
						{this.state.statuserrorMsg ? <div className="alert alert-danger my-3 text-center col-12" role="alert">{this.state.statuserrorMsg}</div> : null}
						<div className="m-auto text-center">
							<button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.handleHide()}>Return</button>
							{this.state.statuserrorMsg == null ? <button className="btn btn-danger" onClick={() => this.handleStatus(this.state.bankId, this.state.status)}>Confirm</button> : null}
						</div>

					</Modal.Body>
				</Modal>

				{/*====== Status change confirmation popup  ===== */}
				<Modal
					show={this.state.statusConfirMmsg}
					onHide={this.handleStatusChangedClose}
					className="payOptionPop"
				>
					<Modal.Body className="text-center">
						<Row>
							<Col md={12} className="text-center">
								<Image src={SuccessIco} />
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<h5>{this.state.successMessage}</h5>
							</Col>
						</Row>
						<Button
							onClick={this.handleStatusChangedClose}
							className="but-gray mt-3"
						>
							Return
                        </Button>
					</Modal.Body>

				</Modal>

				{/*========================= Modal for Delete Gem =====================*/}
				<Modal
					show={this.state.deleteGem}
					onHide={this.handleHide}
					dialogClassName="modal-90w"
					aria-labelledby="example-custom-modal-styling-title"
				>
					<Modal.Body>
						<div className="m-auto text-center">
							<h6 className="mb-3 text-dark">Do you want to delete this Gem?</h6>
						</div>
						{this.state.deleteerrorMsg ? <div className="alert alert-danger my-3 text-center col-12" role="alert">{this.state.deleteerrorMsg}</div> : null}
						<div className="m-auto text-center">
							<button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.handleDeleteHide()}>Return</button>
							{this.state.deleteerrorMsg == null ? <button className="btn btn-danger" onClick={() => this.handleDelete(this.state.gemsId)}>Confirm</button> : null}
						</div>

					</Modal.Body>
				</Modal>

				{/*====== Delete Gem confirmation popup  ===== */}
				<Modal
					show={this.state.deleteConfirMmsg}
					onHide={this.handleDeleteChangedClose}
					className="payOptionPop"
				>
					<Modal.Body className="text-center">
						<Row>
							<Col md={12} className="text-center">
								<Image src={SuccessIco} />
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<h5>{this.state.successMessage}</h5>
							</Col>
						</Row>
						<Button
							onClick={this.handleDeleteChangedClose}
							className="but-gray mt-3"
						>
							Return
                        </Button>
					</Modal.Body>

				</Modal>

				{/*====== Bank Add Edit Modal  ===== */}
				<Modal
					show={this.state.AddEditModal}
					onHide={this.handelAddEditModalClose}
					// className="payOptionPop"
					// size="lg"
					className="right half noPadding slideModal"
				>
					<Modal.Header closeButton></Modal.Header>
					<Modal.Body className="text-center">
						<div>

							<div className="modalHeader">
								<Row>
									<Col md={12} className="text-left">
										<h1>{this.state.modalState === 'add' ? 'Add Bank' : 'Edit Bank'}</h1>
									</Col>
								</Row>
							</div>
							<div className="modalBody content-body noTabs">
								{this.state.modalState === 'add' ?
									<div>



										{
											this.state.addEditBankError
											&&
											<div className="row">
												<div className="col-12">
													<div className="alert alert-danger" role="alert">{this.state.addEditBankError}</div>
												</div>
											</div>

										}


										{this.state.BankAddloader === true ? <LoadingSpinner /> :
											<div className="">
												<Row>
													<Col md={6} className="text-center">
														<div className="form-group">
															<Dropzone

																onDrop={this.onDrop}
															>
																{({ getRootProps, getInputProps }) => (
																	<section>
																		<div {...getRootProps()}>
																			<input {...getInputProps()} />
																			<Fragment>
																				<div className="uploadWrap">
																					{this.state.uploadLoader ? (
																						<LoadingSpinner />
																					) : ((this.state.imageURL == null || this.state.imageURL == '') ?
																						(<Image
																							src={UploadIcon}
																							alt="upload"
																						/>)
																						:
																						(<Image height={250} src={this.state.imageURL} />)
																						)}


																					<p className="sm-txt-blue text-center">
																						Please upload only jpg/png file
                                                                                    </p>
																				</div>
																			</Fragment>
																			<button type="button" className="btn btn-primary btn-position">
																				Upload logo
                                                                            </button>
																		</div>
																	</section>
																)}
															</Dropzone>

															{this.state.uploadErrorMessage ? <div><p className="text-danger mt-3"><span>*</span>{this.state.uploadErrorMessage}</p></div> : null}
														</div>
													</Col>
													<Col md={6} className="text-center">
														<div className="form-group">
															<input type="text" className="form-control" onChange={(e) => this.onChangeBankName(e)} placeholder="Enter a Bank Name" />
															<div className="text-danger text-left">{this.state.bankNameError}</div>
														</div>
													</Col>
												</Row>

												<Button
													onClick={this.handelAddEditModalClose}
													className="but-gray mt-3"
												>
													Return
                                                </Button>
												<Button
													onClick={() => this.handelAddBank()}
													className="btn btn-primary ml-3 mt-3"
												>
													Submit
                                                </Button>
											</div>
										}
									</div>
									:
									<div>


										{this.state.BankAddloader === true ? <LoadingSpinner /> :
											<div>
												<Row>
													<Col md={6} className="text-center">
														<div className="form-group">
															<Dropzone
																//onDrop={acceptedFiles => console.log(acceptedFiles)}
																onDrop={this.onDrop}
															>
																{({ getRootProps, getInputProps }) => (
																	<section>
																		<div {...getRootProps()}>
																			<input {...getInputProps()} />
																			<Fragment>
																				<div className="uploadWrap">
																					{this.state.uploadLoader ? (
																						<LoadingSpinner />
																					) : ((this.state.imageURL == null || this.state.imageURL == '') ?
																						(<Image
																							src={UploadIcon}
																							alt="upload"
																						/>)
																						:
																						(<Image height={250} src={this.state.imageURL} />)
																						)}


																					<p className="sm-txt-blue text-center">
																						Please upload only jpg/png file
                                                                                    </p>
																				</div>
																			</Fragment>
																			<button type="button" className="btn btn-primary btn-position">
																				Upload logo
                                                                            </button>
																		</div>
																	</section>
																)}
															</Dropzone>
															{/* <input type="file" className="form-control" placeholder="Upload Bank logo"/> */}
															{this.state.uploadErrorMessage ? <p className="text-danger"><span>*</span>{this.state.uploadErrorMessage}</p> : null}
														</div>
													</Col>
													<Col md={6} className="text-center">
														<div className="form-group">
															{/* <input type="text" className="form-control" onChange={(e) => this.onChangeBankName(e)} placeholder="Enter a Bank Name" />
                                                    <div className="text-danger text-left">{this.state.bankNameError}</div> */}
															<h5>Bank Name: {this.state.Bankname}</h5>
														</div>
													</Col>
												</Row>

												<Button
													onClick={this.handelAddEditModalClose}
													className="but-gray mt-3"
												>
													Return
                                                </Button>
												<Button
													onClick={() => this.handelEditBank()}
													className="btn btn-primary ml-3 mt-3"
												>
													Submit
                                                </Button>
											</div>
										}
									</div>
								}
							</div>



						</div>
					</Modal.Body>

				</Modal>

				{/* View Gems Modal */}
				<Modal
					show={this.state.viewEditModal}
					onHide={this.handelviewEditModalClose}
					className="right full noPadding slideModal"
				>
					<Modal.Header closeButton></Modal.Header>
					<Modal.Body className="">
						<div className="modalHeader">
							<Row>
								<Col md={9}>
									<h1>View Gems</h1>
								</Col>
							</Row>
						</div>
						<div className="modalBody content-body noTabs">
							<EditBusinessCustomers
								{...this.state.vendorData}
								handelviewEditModalClose={this.handelviewEditModalClose}
								handleEditConfirMmsg={this.handleEditConfirMmsg} />
						</div>
					</Modal.Body>

				</Modal>

				{/* View Chart Modal */}
				<Modal show={this.state.viewChartModel}
					onHide={this.handelChartModalClose}
					className="right full noPadding slideModal" >
					<Modal.Header closeButton></Modal.Header>
					<Modal.Body className="">
						<div className="modalHeader">
							<Row>
								<Col md={9}>
									<h1>{this.state.gemsName}</h1>
								</Col>
							</Row>
						</div>
						<div className="modalBody content-body noTabs">
							<Chart
								{...this.state.gemsData}
								handelAssignModalClose={this.handelChartModalClose}
							/>
						</div>
					</Modal.Body>
				</Modal>

			</div >
		);
	}
}

const mapStateToPros = state => {
	return {
		facility: state.auth.facility
	};
};
Bank.propTypes = {
	facility: PropTypes.any
};
export default connect(mapStateToPros, null)(Bank);
