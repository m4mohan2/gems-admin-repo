import React, { Component } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import 'font-awesome/css/font-awesome.min.css';
import * as AppConst from './../../../../../common/constants';

import productImg2 from './../../../../../assets/no_image.png';
import Moment from 'react-moment';

class GemDetails extends Component {
	state = {}
	render() {
		const initialValues = { ...this.props };
		let settings = {
			dots: true,
			infinite: true,
			speed: 500,
			slidesToShow: 1,
			slidesToScroll: 1,
		};


		return (
			<div className="gemDetailsPan">
				<div className="commonBanner">
					<div className="container">
						<div className="row">
							<div className="col-12">

							</div>
						</div>
					</div>
				</div>
				<div className="gemsDetailsContentPan">
					<div className="container">
						<div className="row">
							<div className="col-6">
								<div className="gemsDetailSlider">
									<Slider {...settings}>
									{initialValues.images.length > 0 ? (initialValues.images.map(imageList => (
										<div className="slideItem" key={imageList.id}>{
											imageList.source == 'image_file'  
											?<img src={AppConst.UPLOADURL + 'gems/' + imageList.image} alt="GEMS" />
											:<img src={ imageList.image} alt="GEMS" />}
										</div>
									))) : 
										<div className="slideItem">
											<img src={productImg2} alt="GEMS" />
										</div>
									} 
										
										
									</Slider>
								</div>
							</div>
							<div className="col-6">
								<h3>{initialValues.name}</h3>
								<label>
									{initialValues.category_detail.length > 0 ? (initialValues.category_detail.map(catList => (
                                        <span className="categoryBlock" key={catList.id}>{catList.category_name} </span> 
                                        ) )) : ''}
									</label><br></br>
									
								{initialValues.total_like > 0 ?
								<label><b><i className="fa fa-heart-o" aria-hidden="true"></i></b> {initialValues.total_like} Likes &nbsp;&nbsp;</label>
								: '' } 
								{initialValues.total_view_count > 0 ?
								<label><b><i className="fa fa-eye" aria-hidden="true"></i></b> {initialValues.total_view_count} Views &nbsp;&nbsp;</label>
								: '' } 
								{initialValues.total_share_count > 0 ?
								<label><b><i className="fa fa-share-alt" aria-hidden="true"></i></b> {initialValues.total_share_count} Shares &nbsp;&nbsp;</label>
								: '' } 
								{initialValues.total_comment_count > 0 ?
								<label><b><i className="fa fa-comment-o" aria-hidden="true"></i></b> {initialValues.total_comment_count} Comments </label>
								: '' }
								<p>{initialValues.note}</p>
								
								<div className="addressPan">
									<label><b><i className="fa fa-phone" aria-hidden="true"></i>Phone No : </b> {initialValues.phone_no ? initialValues.country_code + initialValues.phone_no : 'N/A'}</label>
									<label><b><i className="fa fa-map-marker" aria-hidden="true"></i>Address : </b>{initialValues.address}</label>
									<label><b><i className="fa fa-user" aria-hidden="true"></i>Gemmed By : </b> {initialValues.created_by_user_detail.first_name } {initialValues.created_by_user_detail.last_name }</label>						
									{initialValues.provider_name != null ?
									<label><b><i className="fa fa-user" aria-hidden="true"></i>Claimed By : </b>  {initialValues.provider_name.first_name +' '+ initialValues.provider_name.last_name }</label>
									: ''}
									<label><b><i className="fa fa-calendar" aria-hidden="true"></i>Created On : </b><Moment format="MMMM Do, YYYY">{initialValues.created_at}</Moment></label>
									<label><b><i className="fa fa-check" aria-hidden="true"></i>Status : </b> {initialValues.status==1 ? 'Active' : 'Inactive' }</label>
									
								</div>	
								
							</div>
						</div>
					</div>
				</div>



			</div>
		);
	}
}

export default GemDetails;