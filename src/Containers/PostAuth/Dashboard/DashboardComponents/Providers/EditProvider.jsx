import { Field, Form, Formik } from 'formik';
import React, { Component, Fragment } from 'react';
import { Button, Col, FormControl, FormGroup, Row, Table } from 'react-bootstrap';
import * as Yup from 'yup';
import axios from '../../../../../shared/eaxios';
import * as AppConst from './../../../../../common/constants';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

//Formik and Yup validation

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

const editVendorSchema = Yup.object().shape({
    first_name: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter first name')
        .max(40, 'maximum characters length 40'),
    last_name: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter last name')
        .max(40, 'maximum characters length 40'),
    phone: Yup.string()
        .required('Please enter phone number')
        .max(10, 'Phone number must be 10 digit')
        .matches(phoneRegExp, 'Phone number is not valid'),
    address: Yup.string()
        .trim('Please remove whitespace')
        .required('Please enter address')
        .strict()
        .max(40, 'maximum characters length 40'),
    address2: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .max(40, 'maximum characters length 40'),
    personalCountry: Yup.string().required('Please select country'),
    personalZip: Yup.string()
        .required('Please enter zip'),
    company_name: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter company name')
        .max(40, 'maximum characters length 40'),
    company_phone: Yup.string()
        .required('Please enter company phone number')
        .max(10, 'Phone number must be 10 digit')
        .matches(phoneRegExp, 'Phone number is not valid'),
    /* company_email: Yup.string()
         .email('Email must be valid')
         .trim('Please remove whitespace')
         .strict()
         .required('Please enter company email')
         .max(40, 'maximum characters length 40'),*/
    addr1: Yup.string()
        .trim('Please remove whitespace')
        .required('Please enter company address')
        .strict()
        .max(40, 'maximum characters length 40'),
    addr2: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .max(40, 'maximum characters length 40'),
    companyCountry: Yup.string().required('Please select company country'),
    companyZip: Yup.string()
        .required('Please enter zip'),
    active_status: Yup.string()
        .required('Please select status')
});

class EditUser extends Component {
    state = {
        uploadFile: null,
        showVendor: false,
        errorMessge: null,
        editVendorEnable: false,
        disabled: false,
        editVendorLoader: false,
        editErrorMessge: false,
        countryList: [],
        personalStateList: [],
        personalCityList: [],
        companyStateList: [],
        companyCityList: [],
        addServiceLoader: false,
        selectedFile: '',
        uploadErrorMessage: '',
        permission: []
    };

    /******* Provider Edit Start *********/

    componentDidMount = () => {
        const facility = this.props.facility;
        const permission = [];
        facility.map(data => {
            data.name === 'providers' && data.permission.length !== 0 && permission.push(...data.permission);
        });
        this.setState({ permission });

        this.setState({ addServiceLoader: true });
        const initialValues = { ...this.props };
        axios.get(AppConst.APIURL + '/api/countries')
            .then(res => {
                const countryList = res.data.data;
                this.setState({ countryList: countryList, addServiceLoader: false });
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                console.log(errorMsg);
            });
        this.autoStateChange('personalCountry', initialValues.user_detail.country);
        this.autoCityChange('personalState', initialValues.user_detail.state);
        this.autoStateChange('comapnyCountry', initialValues.company_detail.country);
        this.autoCityChange('companyState', initialValues.company_detail.state);
    }

    handleConfirmReviewClose = () => {
        this.setState({ showConfirMmsg: false });
    };

    handleConfirmReviewShow = () => {
        this.setState({ showConfirMmsg: true });
    };

    handleChange = (e, field) => {
        this.setState({
            [field]: e.target.value
        });
    };

    handleStateChange = (event) => {
        this.handleState(event.target.name, event.target.value);
    }

    autoStateChange = (name, id) => {
        this.handleState(name, id);
    }

    autoCityChange = (name, id) => {
        this.handleCity(name, id);
    }

    handleState = (section, id) => {
        let params = {
            'id': id,
        };
        axios
            .post(AppConst.APIURL + '/api/getStates', params)
            .then(res => {
                const stateList = res.data.data;
                if (section == 'personalCountry') {
                    this.setState({ personalStateList: stateList });
                } else {
                    this.setState({ companyStateList: stateList });
                }
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                console.log(errorMsg);
            });
    }

    handleCityChange = (event) => {
        this.handleCity(event.target.name, event.target.value);
    }

    handleCity = (section, id) => {
        let params = {
            'id': id,
        };
        axios.post(AppConst.APIURL + '/api/getCities', params).then(res => {
            const cityList = res.data.data;
            if (section == 'personalState') {
                this.setState({ personalCityList: cityList });
            } else {
                this.setState({ companyCityList: cityList });
            }
        })
            .catch(e => {
                let errorMsg = this.displayError(e);
                console.log(errorMsg);
            });
    }

    static getDerivedStateFromProps(props, state) {
        if (!state.vendorData) {
            return {
                ...props
            };
        }
    }

    handleEditVendorEnable = () => {
        this.setState({
            editVendorEnable: true,
            disabled: true
        });
    }

    handleEditVendorDisable = () => {
        this.setState({
            editVendorEnable: false,
            disabled: false
        });
        this.props.onReload(this.state.vendorData);
    }

    handleCloseVendor = () => {
        this.props.onClick();
    };

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handleSubmit = (values, { setSubmitting }) => {
        if (this.state.uploadErrorMessage !== '') {
            setSubmitting(false);
        }
        else {
            this.setState({
                editVendorLoader: true,
            });
            let newValue = {
                first_name: values.first_name,
                last_name: values.last_name,
                phone: values.phone,
                email: values.email,
                address: values.address,
                address2: values.address2,
                personalCountry: values.personalCountry,
                personalState: values.personalState,
                personalCity: values.personalCity,
                personalZip: values.personalZip,
                active_status: values.active_status,
                company_name: values.company_name,
                company_phone: values.company_phone,
                //company_email: values.company_email,
                addr1: values.addr1,
                addr2: values.addr2,
                companyCountry: values.companyCountry,
                companyState: values.companyState,
                companyCity: values.companyCity,
                companyZip: values.companyZip,
                claim: values.claim,
                hiddenGemId: values.hiddenGemId
            };

            const formData = new window.FormData();
            formData.append('first_name', values.first_name);
            formData.append('last_name', values.last_name);
            formData.append('phone', values.phone);
            formData.append('email', values.email);
            formData.append('address', values.address);
            formData.append('address2', values.address2);
            formData.append('personalCountry', values.personalCountry);
            formData.append('personalState', values.personalState);
            formData.append('personalCity', values.personalCity);
            formData.append('personalZip', values.personalZip);
            formData.append('active_status', values.active_status);
            formData.append('company_name', values.company_name);
            formData.append('company_phone', values.company_phone);
            formData.append('addr1', values.addr1);
            formData.append('addr2', values.addr2);
            formData.append('companyCountry', values.companyCountry);
            formData.append('companyState', values.companyState);
            formData.append('companyCity', values.companyCity);
            formData.append('companyZip', values.companyZip);
            formData.append('profile_pic', this.state.selectedFile);
            formData.append('claim', values.claim);
            formData.append('hiddenGemId', values.hiddenGemId);

            const vendorData = {};
            Object.assign(vendorData, newValue);
            axios
                .post(AppConst.APIURL + `/api/updateProvider/${values.id}`, formData)
                .then(res => {
                    console.log('Vendor details get response', res);
                    setSubmitting(false);
                    this.setState({
                        editVendorLoader: false,
                        editVendorEnable: false,
                        disabled: false,
                        vendorData
                    }, () => {
                        this.props.handleEditConfirMmsg();
                    });
                })
                .catch(e => {
                    let errorMsg = this.displayError(e);
                    this.setState({
                        editVendorLoader: false,
                        editErrorMessge: errorMsg,
                    });

                    setTimeout(() => {
                        this.setState({ deleteErrorMessge: null });
                    }, 5000);
                });
        }
    };

    handelMod = (getObject) => {
        if (getObject) {
            getObject.first_name && (getObject.first_name = getObject.first_name.trim());
            getObject.last_name && (getObject.last_name = getObject.last_name.trim());
            getObject.address && (getObject.address = getObject.address.trim());
            getObject.address && (getObject.address = getObject.address.trim());
        }
        return getObject;
    }

    onDrop = async files => {
        this.setState({
            files,
            dropzoneActive: false,
            isDrop: true
        });
        let reader = '';
        if (files.length > 0) {
            // FILE SIZE RESTRICTION
            if (files[0]['size'] > 600000) {
                this.setState({
                    uploadErrorMessage: 'File size should be less than 600KB',
                    isDrop: false,
                });
                setTimeout(() => {
                    this.setState({ uploadErrorMessage: null });
                }, 5000);
            }
            else if ((files[0]['type'] !== 'image/png') && (files[0]['type'] !== 'image/jpeg')) {
                this.setState({
                    uploadErrorMessage: 'Please upload jpeg/png file',
                    isDrop: false,
                });
                setTimeout(() => {
                    this.setState({ uploadErrorMessage: null });
                }, 5000);
            }
            else {
                reader = new window.FileReader();
                reader.readAsDataURL(files[0]);
                reader.onload = () => {
                    let urlString = reader.result;
                    this.setState({
                        uploadLoader: false,
                        imageURL: urlString,
                        uploadFile: files[0]
                    });
                };
            }
        }
    };

    onFileChange = event => {
        this.setState({ uploadErrorMessage: '' });
        if (event.target.files.length !== 0) {
            if (event.target.files[0].size > 600000) {
                this.setState({
                    uploadErrorMessage: 'File size should be less than 600KB',
                    isDrop: false,
                });
            }
            else if ((event.target.files[0].type !== 'image/png') && (event.target.files[0].type !== 'image/jpeg')) {
                this.setState({
                    uploadErrorMessage: 'Please upload jpeg/png file',
                    isDrop: false,
                });
            }
            else {
                this.setState({ selectedFile: event.target.files[0], uploadErrorMessage: '' });
            }
        }
    };

    render() {
        const initialValues = { ...this.props };
        const values = {
            id: initialValues.id,
            first_name: initialValues.user_detail.first_name,
            last_name: initialValues.user_detail.last_name,
            address: initialValues.user_detail.address,
            address2: initialValues.user_detail.address2,
            phone: initialValues.user_detail.phone,
            personalCountry: initialValues.user_detail.country,
            personalState: initialValues.user_detail.state,
            personalCity: initialValues.user_detail.city,
            personalZip: initialValues.user_detail.zip,
            profile_pic: initialValues.user_detail.profile_pic,
            active_status: initialValues.active_status,
            company_name: initialValues.company_detail.company_name,
            //company_email: initialValues.company_detail.company_email,
            company_phone: initialValues.company_detail.company_phone,
            addr1: initialValues.company_detail.addr1,
            addr2: initialValues.company_detail.addr2,
            companyCountry: initialValues.company_detail.country,
            companyState: initialValues.company_detail.state,
            companyCity: initialValues.company_detail.city,
            companyZip: initialValues.company_detail.zip,
            company_documents: initialValues.company_documents,
            claim: initialValues.gem_status.action,
            hiddenGemId: initialValues.gem_status.gems_id,
            gem_detail: initialValues.gem_detail,
        };
        const {
            disabled
        } = this.state;
        return (
            <div className="addcustomerSec">
                <div className="boxBg p-35">
                    <Row>
                        <Col sm={12}>
                            {this.state.addServiceLoader ? <LoadingSpinner /> : null}
                        </Col>
                    </Row>
                    <Row className="show-grid">
                        {/* <Col xs={12} md={12}>
                            <div className="sectionTitle">
                                <h2>{this.state.editVendorEnable !== true ? 'View' : 'Edit'} Details</h2>
                            </div>
                        </Col> */}
                        <Col xs={12} className="brd-right">
                            <Formik
                                initialValues={values}
                                validationSchema={editVendorSchema}
                                onSubmit={this.handleSubmit}
                                enableReinitialize={true}
                            >
                                {({
                                    values,
                                    errors,
                                    touched,
                                    isSubmitting,
                                    handleChange,
                                }) => {
                                    return (
                                        <Form className={disabled === false ? ('hideRequired') : null}>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    <div className="sectionTitle mb-4">
                                                        <h2>Company Information</h2>
                                                    </div>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Company Email <span className="required">*</span></h3></label>
                                                        <Field
                                                            name="email"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={initialValues.email || ''}
                                                            disabled="disabled"
                                                        />
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Company Name <span className="required">*</span></h3></label>
                                                        <Field
                                                            name="company_name"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.company_name || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.company_name && touched.company_name ? (
                                                            <span className="errorMsg ml-3">{errors.company_name}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Company Phone <span className="required">*</span></h3></label>
                                                        <Field
                                                            name="company_phone"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.company_phone || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.company_phone && touched.company_phone ? (
                                                            <span className="errorMsg ml-3">{errors.company_phone}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Address <span className="required">*</span></h3></label>
                                                        <Field
                                                            name="addr1"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.addr1 || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.addr1 && touched.addr1 ? (
                                                            <span className="errorMsg ml-3">{errors.addr1}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>ADDRESS2 LINE 2(optional)</h3></label>
                                                        <Field
                                                            name="addr2"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.addr2 || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.addr2 && touched.addr2 ? (
                                                            <span className="errorMsg ml-3">{errors.addr2}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <label><h3>Country <span className="required">*</span></h3></label>
                                                        <Field
                                                            name="companyCountry"
                                                            component="select"
                                                            className={`input-elem ${values.companyCountry &&
                                                                'input-elem-filled'} form-control`}
                                                            autoComplete="nope"
                                                            value={values.companyCountry || ''}
                                                            onChange={e => {
                                                                handleChange(e);
                                                                this.handleStateChange(e);
                                                            }}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >
                                                            <option value="">
                                                                {this.state.countryList.length ? 'Select Country' : 'Loading...'}
                                                            </option>
                                                            {this.state.countryList.map(country => (
                                                                <option
                                                                    key={country.id}
                                                                    value={country.id}
                                                                >
                                                                    {country.name}
                                                                </option>
                                                            ))}
                                                        </Field>
                                                        {errors.companyCountry && touched.companyCountry ? (
                                                            <span className="errorMsg ml-3">
                                                                {errors.companyCountry}
                                                            </span>
                                                        ) : null}
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <label><h3>State</h3></label>
                                                        <Field
                                                            name="companyState"
                                                            component="select"
                                                            className={`input-elem topShift ${values.companyState &&
                                                                'input-elem-filled'} form-control`}
                                                            autoComplete="nope"
                                                            value={values.companyState || ''}
                                                            onChange={e => {
                                                                handleChange(e);
                                                                this.handleCityChange(e);
                                                            }}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >
                                                            <option value="">
                                                                {this.state.companyStateList.length ? 'Select State' : 'Loading...'}
                                                            </option>
                                                            {this.state.companyStateList.map(state => (
                                                                <option value={state.id} key={state.id}>
                                                                    {state.name}
                                                                </option>
                                                            ))}
                                                        </Field>
                                                        {errors.companyState &&
                                                            touched.companyState ? (
                                                                <span className="errorMsg ml-3">
                                                                    {errors.companyState}
                                                                </span>
                                                            ) : null}
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <label><h3>City</h3></label>
                                                        <Field
                                                            component="select"
                                                            name="companyCity"
                                                            placeholder="select"
                                                            className={`input-elem topShift ${values.companyCity &&
                                                                'input-elem-filled'} form-control`}
                                                            value={values.companyCity || ''}
                                                            onChange={e => {
                                                                handleChange(e);
                                                            }}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >
                                                            <option value="">
                                                                {this.state.companyCityList.length ? 'Select City' : 'Loading...'}
                                                            </option>
                                                            {this.state.companyCityList.map(city => (
                                                                <option value={city.id} key={city.id}>
                                                                    {city.name}
                                                                </option>
                                                            ))}
                                                        </Field>
                                                        {errors.companyCity && touched.companyCity ? (
                                                            <span className="errorMsg ml-3">
                                                                {errors.companyCity}
                                                            </span>
                                                        ) : null}
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <label><h3>Zip <span className="required">*</span></h3></label>
                                                    <FormGroup controlId="formBasicText">
                                                        <Field
                                                            name="companyZip"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Zip"
                                                            value={values.companyZip || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.companyZip && touched.companyZip ? (
                                                            <span className="errorMsg ml-3">{errors.companyZip}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={12} md={12}>
                                                    <div className="sectionTitle mb-4 mt-4">
                                                        <h2>Personal Information</h2>
                                                        {/* <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras mollis vulputate</p> */}
                                                    </div>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>First Name <span className="required">*</span></h3></label>
                                                        <Field
                                                            name="first_name"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.first_name || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.first_name && touched.first_name ? (
                                                            <span className="errorMsg ml-3">{errors.first_name}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Last Name <span className="required">*</span></h3></label>
                                                        <Field
                                                            name="last_name"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.last_name || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.last_name && touched.last_name ? (
                                                            <span className="errorMsg ml-3">{errors.last_name}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Phone <span className="required">*</span></h3></label>
                                                        <Field
                                                            name="phone"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.phone || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.phone && touched.phone ? (
                                                            <span className="errorMsg ml-3">{errors.phone}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Address <span className="required">*</span></h3></label>
                                                        <Field
                                                            name="address"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.address || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.address && touched.address ? (
                                                            <span className="errorMsg ml-3">{errors.address}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>ADDRESS2 LINE 2(optional)</h3></label>
                                                        <Field
                                                            name="address2"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.address2 || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.address2 && touched.address2 ? (
                                                            <span className="errorMsg ml-3">{errors.address2}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <label><h3>Country<span className="required">*</span></h3></label>
                                                        <Field
                                                            name="personalCountry"
                                                            component="select"
                                                            className={`input-elem ${values.personalCountry &&
                                                                'input-elem-filled'} form-control`}
                                                            autoComplete="nope"
                                                            value={values.personalCountry || ''}
                                                            onChange={e => {
                                                                handleChange(e);
                                                                this.handleStateChange(e);
                                                            }}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >
                                                            <option value="">
                                                                {this.state.countryList.length ? 'Select Country' : 'Loading...'}
                                                            </option>
                                                            {this.state.countryList.map(country => (
                                                                <option
                                                                    key={country.id}
                                                                    value={country.id}
                                                                >
                                                                    {country.name}
                                                                </option>
                                                            ))}
                                                        </Field>
                                                        {errors.personalCountry && touched.personalCountry ? (
                                                            <span className="errorMsg ml-3">
                                                                {errors.personalCountry}
                                                            </span>
                                                        ) : null}
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <label><h3>State</h3></label>
                                                        <Field
                                                            name="personalState"
                                                            component="select"
                                                            className={`input-elem topShift ${values.personalState &&
                                                                'input-elem-filled'} form-control`}
                                                            autoComplete="nope"
                                                            value={values.personalState || ''}
                                                            onChange={e => {
                                                                handleChange(e);
                                                                this.handleCityChange(e);
                                                            }}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >
                                                            <option value="">
                                                                {this.state.personalStateList.length ? 'Select Sate' : 'Loading...'}
                                                            </option>
                                                            {this.state.personalStateList.map(state => (
                                                                <option value={state.id} key={state.id}>
                                                                    {state.name}
                                                                </option>
                                                            ))}
                                                        </Field>
                                                        {errors.personalState &&
                                                            touched.personalState ? (
                                                                <span className="errorMsg ml-3">
                                                                    {errors.personalState}
                                                                </span>
                                                            ) : null}
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <label><h3>City</h3></label>
                                                        <Field
                                                            component="select"
                                                            name="personalCity"
                                                            placeholder="select"
                                                            className={`input-elem topShift ${values.personalCity &&
                                                                'input-elem-filled'} form-control`}
                                                            value={values.personalCity || ''}
                                                            onChange={e => {
                                                                handleChange(e);
                                                            }}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >
                                                            <option value="">
                                                                {this.state.personalCityList.length ? 'Select City' : 'Loading...'}
                                                            </option>
                                                            {this.state.personalCityList.map(city => (
                                                                <option value={city.id} key={city.id}>
                                                                    {city.name}
                                                                </option>
                                                            ))}
                                                        </Field>
                                                        {errors.personalCity && touched.personalCity ? (
                                                            <span className="errorMsg ml-3">
                                                                {errors.personalCity}
                                                            </span>
                                                        ) : null}
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <label><h3>Zip <span className="required">*</span></h3></label>
                                                        <Field
                                                            name="personalZip"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.personalZip || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.personalZip && touched.personalZip ? (
                                                            <span className="errorMsg ml-3">{errors.personalZip}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} sm={6} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <label><h3>Profile Pic</h3></label>
                                                        <input
                                                            name="selectedFile"
                                                            type="file"
                                                            onChange={this.onFileChange}
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {this.state.uploadErrorMessage && <span className="errorMsg ml-3">{this.state.uploadErrorMessage}</span>}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                {values.profile_pic &&
                                                    <Col xs={12} md={12}>
                                                        <img height="150" width="150" className="ml-3 rounded" src={AppConst.UPLOADURL + '/profile/' + values.profile_pic}></img>
                                                    </Col>
                                                }

                                                <Col xs={12} md={12}>
                                                    <div className="sectionTitle">
                                                        <h2>Company Document(s)</h2>
                                                        <p></p>
                                                    </div>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <div className="addressPan">
                                                        {values.company_documents.length > 0 ?
                                                            values.company_documents.map((eachfile) => (
                                                                <Col key={eachfile.id} sm="4">
                                                                    <label><a href={AppConst.FILE_BASE_URL + eachfile.file_path} target="_blank" rel="noopener noreferrer" title="Download File "><b> {eachfile.org_file_name}</b> </a></label>
                                                                </Col>
                                                            )) : 'N/A'}
                                                    </div>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <label><h3>Status <span className="required">*</span></h3></label>
                                                        <Field
                                                            name="active_status"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >
                                                            <option value="">Select Status</option>
                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        {errors.active_status && touched.active_status ? (
                                                            <span className="errorMsg ml-3">{errors.active_status}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <label><h3>Claim Status<span className="required">*</span></h3></label>
                                                        <Field
                                                            name="claim"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            value={values.claim || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >
                                                            <option value="0" key="0">Pending</option>
                                                            <option value="1" key="1">Approve</option>
                                                            <option value="2" key="2">Reject</option>
                                                        </Field>
                                                        <Field
                                                            name="hiddenGemId"
                                                            component="hidden"
                                                            autoComplete="nope"
                                                            value={values.hiddenGemId || ''}
                                                        ></Field>
                                                        {errors.claim && touched.claim ? (
                                                            <span className="errorMsg ml-3">{errors.claim}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <div className="sectionTitle">
                                                        <h2>Gem Information</h2>
                                                        <p></p>
                                                    </div>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <div className="boxBg">
                                                        <Table responsive hover>
                                                            <thead className="theaderBg">
                                                                <tr>
                                                                    <th>Gems Name</th>
                                                                    <th>Address</th>
                                                                    <th>Status</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                {values.gem_detail.length !== 0 && values.gem_detail.map((data, i) => {
                                                                    return <tr key={i}>
                                                                        <td>{data.name}</td>
                                                                        <td>{data.address}</td>
                                                                        <td>{data.action === 0 ? 'Pending' : data.action === 1 ? 'Approved' : 'Rejected'}</td>
                                                                    </tr>;
                                                                })}
                                                            </tbody>
                                                        </Table>
                                                    </div>
                                                </Col>

                                                {/* <Col xs={12} md={12}>
                                                    <div className="addressPan">

                                                        <Col sm="12">
                                                            <label><b><i className="fa fa-diamond" aria-hidden="true"></i></b>&nbsp;{values.gem_detail !== null ? values.gem_detail.name : ''}</label>
                                                        </Col>
                                                        <Col sm="12">
                                                            <label>&nbsp;<b><i className="fa fa-map-marker" aria-hidden="true"></i></b>&nbsp;&nbsp;:{values.gem_detail.address}</label>
                                                        </Col>

                                                    </div>
                                                </Col> */}

                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={6} md={12}>
                                                    &nbsp;
                                                </Col>
                                            </Row>
                                            <Row>&nbsp;</Row>
                                            {this.state.permission[1] && this.state.permission[1].status === true &&
                                                <Row className="show-grid text-center">
                                                    <Col xs={12} md={12}>
                                                        <Fragment>
                                                            {
                                                                this.state.editVendorEnable !== true ? (
                                                                    <Fragment>
                                                                        <Button className="blue-btn border-0" onClick={this.handleEditVendorEnable}>Edit</Button>
                                                                    </Fragment>
                                                                ) : (
                                                                        <Fragment>
                                                                            <Button onClick={this.props.handelviewEditModalClose} className="but-gray border-0 mr-2"> Cancel </Button>
                                                                            <Button type="submit" className="blue-btn ml-2 border-0" disabled={isSubmitting}>Save</Button>
                                                                        </Fragment>
                                                                    )
                                                            }
                                                        </Fragment>
                                                    </Col>
                                                </Row>}
                                            {disabled === false ? null : (<Fragment>
                                                <Row>
                                                    <Col md={12}>
                                                        <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                                    </Col>
                                                </Row>
                                            </Fragment>)}
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        facility: state.auth.facility
    };
};

EditUser.propTypes = {
    globState: PropTypes.object,
    onClickAction: PropTypes.func,
    onReload: PropTypes.func,
    //onSuccess: PropTypes.func,
    onClick: PropTypes.func,
    handelviewEditModalClose: PropTypes.func,
    handleEditConfirMmsg: PropTypes.func,
    facility: PropTypes.any
};

export default connect(mapStateToProps, null)(EditUser);