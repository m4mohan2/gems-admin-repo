import { Field, Form, Formik } from 'formik';
import React, { Component } from 'react';
import {
    Button,
    Col,
    FormControl,
    FormGroup,
    Image,
    Modal,
    Row
} from 'react-bootstrap';
//import InputMask from 'react-input-mask';
import * as Yup from 'yup';
import axios from '../../../../../shared/eaxios';
import SuccessIco from '../../../../../assets/success-ico.png';
//import './../../customer/add-customer/AddCustomer.scss';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import PropTypes from 'prop-types';
import * as AppConst from './../../../../../common/constants';

//RichTextToolBar
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';

const initialValues = {
    question: '',
    category_id: '',
    answer: '',
    status: ''
};

const addCustomerSchema = Yup.object().shape({
    question: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter question'),
        //.max(40, 'maximum characters length 40'),
    category_id: Yup.string()
        .required('Please select type')
        .trim('Please remove whitespace'),
    //.strict(),
    answer: Yup.string()
        .trim('Please remove whitespace')
        .strict(),
    status: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please select status')
});

class AddFaq extends Component {
    state = {
        showConfirMmsg: false,
        errorMessge: null,
        addVendorLoader: false,
        addErrorMessge: null,
        answerContent: EditorState.createEmpty(),
    };



    onEditorStateChangeAnswer = (answerContent) => {
        this.setState({
            answerContent,
        });
    };

    handleChange = (e, field) => {
        this.setState({
            [field]: e.target.value
        });
    };

    handleConfirmReviewClose = () => {
        this.setState({ showConfirMmsg: false });
    };

    handleConfirmReviewShow = () => {
        this.setState({ showConfirMmsg: true });
    };

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_answer;
        } catch (err) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handleSubmit = (values, { resetForm }) => {
        this.setState({
            addVendorLoader: true,
        });
        console.log('values 1 ', values, this.state.states);
        const formData = new window.FormData();
        for (const key in values) {
            if (values.hasOwnProperty(key)) {
                formData.append(key, values[key]);
            }
        }
        let newValue = {
            question: values.question,
            category_id: values.category_id,
            answer: draftToHtml(convertToRaw(this.state.answerContent.getCurrentContent())).trim(),
            status: values.status
        };
        console.log('Final values ', newValue);
        const config = {
            headers: {
                //sessionKey: localStorage.getItem("sessionKey"),
                'Content-Type': 'application/json'
            }
        };
        axios
            .post(AppConst.APIURL + '/api/addFaq', newValue, config)
            .then(res => {
                console.log(res);
                console.log('Vendor data', res.data);
                resetForm({});
                //this.setState({ showConfirMmsg: true });
                this.props.handelAddModalClose();
                this.props.handleAddConfirMmsg();
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    addVendorLoader: false,
                    addErrorMessge: errorMsg,
                });
                setTimeout(() => {
                    this.setState({ deleteErrorMessge: null });
                }, 5000);
            });
    };

    render() {
        return (
            <div className="addcustomerSec">
                <div className="boxBg p-35">
                    {this.state.addErrorMessge ? (
                        <div className="alert alert-danger" role="alert">
                            {this.state.addErrorMessge}
                        </div>
                    ) : null}
                    <Row>
                        <Col sm={12}>
                            {this.state.addVendorLoader ? <LoadingSpinner /> : null}
                        </Col>
                    </Row>
                    <Row className="show-grid">
                        <Col xs={12} className="brd-right">
                            <Formik
                                initialValues={initialValues}
                                validationSchema={addCustomerSchema}
                                onSubmit={this.handleSubmit}
                            >
                                {({
                                    values,
                                    errors,
                                    touched,
                                    //isSubmitting,
                                    //handleChange,
                                    //handleBlur,
                                    //setFieldValue
                                }) => {
                                    return (
                                        <Form>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Question <span className="required">*</span></label>
                                                        <Field
                                                            name="question"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter question"
                                                            value={values.question || ''}
                                                        />
                                                        {errors.question && touched.question ? (
                                                            <span className="errorMsg ml-3">{errors.question}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Answer
                                                        </label>
                                                        <div className='editor'>
                                                            <Editor
                                                                editorState={this.state.answerContent}
                                                                wrapperClassName="demo-wrapper"
                                                                editorClassName="form-control rounded-0 mt-n2"
                                                                onEditorStateChange={this.onEditorStateChangeAnswer}
                                                                placeholder="Enter description"
                                                                toolbarClassName="text-body"
                                                                handlePastedText={() => false}
                                                                toolbar={{
                                                                    options: ['inline', 'blockType', 'fontSize', 'list', 'textAlign', 'link', 'embedded', 'emoji', 'image', 'remove', 'history'],
                                                                    image: { alt: { present: true, mandatory: false } }
                                                                }}
                                                            />
                                                        </div>

                                                        {errors.answer && touched.answer ? (
                                                            <span className="errorMsg ml-3">{errors.answer}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <span>Type <span className="required">*</span></span>
                                                        <Field
                                                            name="category_id"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >
                                                            <option value="" defaultValue="5">
                                                                Select Type
                                                            </option>
                                                            <option value="1" key="1">
                                                                Using Gems
                                                            </option>
                                                            <option value="2" key="2">
                                                                Account
                                                            </option>
                                                            <option value="3" key="3">
                                                                Other
                                                            </option>
                                                        </Field>
                                                        {errors.category_id && touched.category_id ? (
                                                            <span className="errorMsg ml-3">{errors.category_id}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <span>Status <span className="required">*</span></span>
                                                        <Field
                                                            name="status"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >
                                                            <option value="" defaultValue="">
                                                                Select Status
                                                            </option>
                                                            <option value="1" key="1">
                                                                Active
                                                            </option>
                                                            <option value="0" key="0">
                                                                Inactive
                                                            </option>
                                                        </Field>
                                                        {errors.status && touched.status ? (
                                                            <span className="errorMsg ml-3">{errors.status}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    &nbsp;
                                                </Col>
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12} className="text-center">
                                                    <Button className="blue-btn" type="submit">
                                                        Save
                                                    </Button>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={12}>
                                                    <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                                </Col>
                                            </Row>
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Col>
                    </Row>
                </div>

                {/*======  confirmation popup  ===== */}
                <Modal
                    show={this.state.showConfirMmsg}
                    onHide={this.handleConfirmReviewClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Vendor has been successfully added</h5>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handleConfirmReviewClose}
                            className="but-gray"
                        >
                            Done
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

AddFaq.propTypes = {
    handleAddConfirMmsg: PropTypes.func,
    businessId: PropTypes.number,
    handelAddModalClose: PropTypes.func,
};

export default AddFaq;