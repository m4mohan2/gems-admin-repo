/* eslint-disable no-useless-escape */
import { Field, Form, Formik } from 'formik';
import React, { Component, Fragment } from 'react';
import {
    Button,
    Col,
    FormControl,
    FormGroup,
    Row
} from 'react-bootstrap';
import * as Yup from 'yup';
import axios from '../../../../../shared/eaxios';
import * as AppConst from './../../../../../common/constants';
import PropTypes from 'prop-types';

//RichTextToolBar
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import { connect } from 'react-redux';

//let modifiedObject = {};
//Formik and Yup validation
const editVendorSchema = Yup.object().shape({
    question: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter question'),
    answer: Yup.string()
        .trim('Please remove whitespace')
        .strict()
});

class EditFaq extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showVendor: false,
            errorMessge: null,
            editVendorEnable: false,
            disabled: false,
            editVendorLoader: false,
            editErrorMessge: false,
            editorForAnswer: EditorState.createEmpty(),
            permission: []
        };
    }

    static getDerivedStateFromProps(props, state) {
        console.log(' getDerivedStateFromProps : ', props, state);
        if (!state.vendorData) {
            return {
                ...props
            };
        }
    }

    handleEditVendorEnable = () => {
        this.setState({
            editVendorEnable: true,
            disabled: true
        });
    }

    handleEditVendorDisable = () => {
        this.setState({
            editVendorEnable: false,
            disabled: false
        });
        this.props.onReload(this.state.vendorData);
    }

    handleCloseVendor = () => {
        this.props.onClick();
    };


    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    //after editing form submit
    handleSubmit = values => {
        this.setState({
            editVendorLoader: true,
        });
        console.log('handleSubmit', values);

        let newValue = {
            question: values.question,
            category_id: values.category_id,
            answer: draftToHtml(convertToRaw(this.state.editorForAnswer.getCurrentContent())).trim(),
            status: values.status
        };
        console.log('new Value submitted 2', newValue);

        const vendorData = {};
        Object.assign(vendorData, newValue);
        console.log('vendorData', vendorData);
        axios
            .put(AppConst.APIURL + `/api/updateFaq/${values.id}`, newValue)
            .then(res => {
                console.log('Vendor details get response', res);
                console.log('Vendor details get data', res.data);
                this.setState({
                    editVendorLoader: false,
                    editVendorEnable: false,
                    disabled: false,
                    vendorData
                }, () => {
                    this.props.handleEditConfirMmsg();
                });
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    editVendorLoader: false,
                    editErrorMessge: errorMsg,
                });

                setTimeout(() => {
                    this.setState({ deleteErrorMessge: null });
                }, 1000);

            });
    };

    componentDidMount() {
        const facility = this.props.facility;
        const permission = [];
        facility.map(data => {
            data.name === 'faq' && data.permission.length !== 0 && permission.push(...data.permission);
        });
        this.setState({ permission });

        const initialValues = { ...this.props };
        const contentBlockDesc = htmlToDraft(initialValues.answer.trim());
        if (contentBlockDesc) {
            const contentStateDescription = ContentState.createFromBlockArray(contentBlockDesc.contentBlocks);
            const descriptionContent = EditorState.createWithContent(contentStateDescription);
            this.setState({ editorForAnswer: descriptionContent });
        }
    }

    onEditorStateChangeAnswer = (editorForAnswer) => {
        this.setState({
            editorForAnswer,
        });
    };

    render() {
        const initialValues = { ...this.props };
        console.log('initial value', initialValues);
        // modifiedObject = this.handelMod(initialValues);
        // console.log('MOD value', modifiedObject);
        const values = {
            id: initialValues.id,
            question: initialValues.question,
            answer: initialValues.answer,
            category_id: initialValues.category_id,
            status: initialValues.status,
        };
        const {
            disabled
        } = this.state;

        return (
            <Formik
                initialValues={values}
                validationSchema={editVendorSchema}
                onSubmit={this.handleSubmit}
                enableReinitialize={true}
            >
                {({
                    values,
                    errors,
                    touched,
                    //isSubmitting,
                    //handleChange,
                    //setFieldValue,
                    //handleBlur
                }) => {
                    return (
                        <Form className={disabled === false ? ('hideRequired') : null}>
                            <Row className="show-grid">
                                <Col xs={12} md={12}>
                                    <FormGroup controlId="formControlsTextarea">
                                        <label><h3>Question <span className="required">*</span></h3></label>
                                        <Field
                                            name="question"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.question || ''}
                                            disabled={disabled === false ? 'disabled' : ''}
                                        />
                                        {errors.question && touched.question ? (
                                            <span className="errorMsg ml-3">{errors.question}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>

                                <Col xs={12} md={12}>
                                    <FormGroup controlId="formControlsTextarea">
                                        <label><h3>Answer</h3>
                                        </label>
                                        <Editor
                                            editorState={this.state.editorForAnswer}
                                            toolbarHidden={disabled === false ? true : false}
                                            wrapperClassName="demo-wrapper"
                                            editorClassName="form-control rounded-0 mt-n2"
                                            onEditorStateChange={this.onEditorStateChangeAnswer}
                                            placeholder="Enter answer"
                                            readOnly={disabled === false ? 'readOnly' : ''}
                                            handlePastedText={() => false}
                                            toolbarClassName="text-body"
                                            toolbar={{
                                                options: ['inline', 'blockType', 'fontSize', 'list', 'textAlign', 'link', 'embedded', 'emoji', 'image', 'remove', 'history'],
                                                image: { alt: { present: true, mandatory: false } }
                                            }}
                                        />
                                        {/* <Field
                                            name="answer"
                                            component="textarea"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.answer || ''}
                                            disabled={disabled === false ? 'disabled' : ''}
                                        /> */}
                                        {errors.answer && touched.answer ? (
                                            <span className="errorMsg ml-3">{errors.answer}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>

                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formControlsTextarea">
                                        <span><h3>Type <span className="required">*</span></h3></span>
                                        <Field
                                            name="category_id"
                                            component="select"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="select"
                                            disabled={disabled === false ? 'disabled' : ''}
                                        >
                                            <option value="">
                                                Select Type
                                            </option>
                                            <option value="1" key="1">
                                                Using Gems
                                            </option>
                                            <option value="2" key="2">
                                                Account
                                            </option>
                                            <option value="3" key="3">
                                                Other
                                            </option>
                                        </Field>
                                        {errors.category_id && touched.category_id ? (
                                            <span className="errorMsg ml-3">{errors.category_id}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>

                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formControlsTextarea">
                                        <span><h3>Status <span className="required">*</span></h3></span>
                                        <Field
                                            name="status"
                                            component="select"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="select"
                                            disabled={disabled === false ? 'disabled' : ''}
                                        >
                                            <option value="">
                                                Select Status
                                            </option>
                                            <option value="1" key="1">
                                                Active
                                            </option>
                                            <option value="0" key="0">
                                                Inactive
                                            </option>
                                        </Field>
                                        {errors.status && touched.status ? (
                                            <span className="errorMsg ml-3">{errors.status}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row className="show-grid">
                                <Col xs={12} md={12}>
                                    &nbsp;
                                </Col>
                            </Row>
                            <Row>&nbsp;</Row>
                            {this.state.permission[2] && this.state.permission[2].status === true &&
                                <Row className="show-grid text-center">
                                    <Col xs={12} md={12}>
                                        <Fragment>
                                            {
                                                this.state.editVendorEnable !== true ? (
                                                    <Fragment>
                                                        <Button className="blue-btn border-0" onClick={this.handleEditVendorEnable}>
                                                            Edit
                                                    </Button>
                                                    </Fragment>
                                                ) : (
                                                        <Fragment>
                                                            <Button
                                                                onClick={this.props.handelviewEditModalClose}
                                                                className="but-gray border-0 mr-2"
                                                            >
                                                                Cancel
                                                    </Button>
                                                            <Button type="submit" className="blue-btn ml-2 border-0">
                                                                Save
                                                    </Button>
                                                        </Fragment>)
                                            }
                                        </Fragment>
                                    </Col>
                                </Row>}
                            {disabled === false ? null : (<Fragment>
                                <Row>
                                    <Col md={12}>
                                        <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                    </Col>
                                </Row>
                            </Fragment>)}
                        </Form>
                    );
                }}
            </Formik>
        );
    }
}
const mapStateToProps = state => {
    return {
        globalState: state,
        facility: state.auth.facility
    };
};
EditFaq.propTypes = {
    globState: PropTypes.object,
    onClickAction: PropTypes.func,
    onReload: PropTypes.func,
    //onSuccess: PropTypes.func,
    onClick: PropTypes.func,
    handelviewEditModalClose: PropTypes.func,
    handleEditConfirMmsg: PropTypes.func,
    facility: PropTypes.any
};

export default connect(mapStateToProps, null)(EditFaq);