import { Field, Form, Formik } from 'formik';
import React, { Component } from 'react';
import { Button, Col, FormControl, FormGroup, Image, Modal, Row } from 'react-bootstrap';
import * as Yup from 'yup';
import axios from '../../../../../shared/eaxios';
import SuccessIco from '../../../../../assets/success-ico.png';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import PropTypes from 'prop-types';
import * as AppConst from './../../../../../common/constants';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';

//RichTextToolBar
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';

const initialValues = {
    category_name: '',
    status: ''
};

const addCategorySchema = Yup.object().shape({
    category_name: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter category name'),
    status: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please select status')
});

class AddCategory extends Component {
    state = {
        showConfirMmsg: false,
        errorMessge: null,
        addCategoryLoader: false,
        addErrorMessge: null,
        categoryDescription: EditorState.createEmpty(),
        subCategoryList: [],
        subCategory: [],
        subCategoryError: null,
    };

    onEditorStateChangeDesc = (categoryDescription) => {
        this.setState({ categoryDescription });
    };

    handleChange = (e, field) => {
        this.setState({ [field]: e.target.value });
    };

    handleConfirmReviewClose = () => {
        this.setState({ showConfirMmsg: false });
    };

    handleConfirmReviewShow = () => {
        this.setState({ showConfirMmsg: true });
    };

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_answer;
        } catch (err) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handleSubmit = (values, { resetForm, setSubmitting }) => {
        if (this.state.subCategory.length === 0) {
            this.setState({ subCategoryError: 'Please select sub category' });
            setSubmitting(false);
        } else {
            this.setState({ addCategoryLoader: true });
            setSubmitting(true);
            let newValue = {
                category_name: values.category_name,
                description: draftToHtml(convertToRaw(this.state.categoryDescription.getCurrentContent())).trim(),
                subcategory: this.state.subCategory,
                status: values.status
            };
            axios.post(AppConst.APIURL + '/api/addCategory', newValue)
                .then(res => {
                    console.log(res);
                    resetForm({});
                    setSubmitting(false);
                    this.setState({ showConfirMmsg: true });
                    this.props.handelAddModalClose();
                    this.props.handleAddConfirMmsg();
                }).catch(e => {
                    let errorMsg = this.displayError(e);
                    this.setState({
                        addCategoryLoader: false,
                        addErrorMessge: errorMsg,
                    });
                    setTimeout(() => {
                        this.setState({ addErrorMessge: null });
                    }, 5000);
                });
        }
    };

    componentDidMount() {
        this.setState({ addCategoryLoader: true });
        axios.get(AppConst.APIURL + '/api/subcategoryList')
            .then(res => {
                this.setState({ subCategoryList: res.data, addCategoryLoader: false });
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    addCategoryLoader: false,
                    addErrorMessge: errorMsg,
                });
                setTimeout(() => {
                    this.setState({ deleteErrorMessge: null });
                }, 5000);
            });
    }

    handleSelect = (data) => {
        this.setState({ subCategory: data, subCategoryError: null });
    }

    render() {
        const colourStyles = {
            option: (provided) => ({
                ...provided,
                color: 'black',
            }),
        };
        const animatedComponents = makeAnimated();
        return (
            <div className='addcustomerSec'>
                <div className='boxBg p-35'>
                    {this.state.addErrorMessge ? (
                        <div className='alert alert-danger' role='alert'>
                            {this.state.addErrorMessge}
                        </div>
                    ) : null}
                    <Row>
                        <Col sm={12}>
                            {this.state.addCategoryLoader ? <LoadingSpinner /> : null}
                        </Col>
                    </Row>
                    <Row className='show-grid'>
                        <Col xs={12} className='brd-right'>
                            <Formik
                                initialValues={initialValues}
                                validationSchema={addCategorySchema}
                                onSubmit={this.handleSubmit}
                            >
                                {({
                                    values,
                                    errors,
                                    touched,
                                    isSubmitting,
                                }) => {
                                    return (
                                        <Form>
                                            <Row className='show-grid'>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId='formControlsTextarea'>
                                                        <label>Category name <span className='required'>*</span></label>
                                                        <Field
                                                            name='category_name'
                                                            type='text'
                                                            className={'form-control'}
                                                            autoComplete='nope'
                                                            placeholder='Enter Category name'
                                                            value={values.category_name || ''}
                                                        />
                                                        {errors.category_name && touched.category_name ? (
                                                            <span className='errorMsg ml-3'>{errors.category_name}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId='formControlsTextarea'>
                                                        <label>Description
                                                        </label>
                                                        <div className='editor'>
                                                            <Editor
                                                                editorState={this.state.categoryDescription}
                                                                wrapperClassName='demo-wrapper'
                                                                editorClassName='form-control rounded-0 mt-n2'
                                                                onEditorStateChange={this.onEditorStateChangeDesc}
                                                                placeholder='Enter description'
                                                                toolbarClassName='text-body'
                                                                handlePastedText={() => false}
                                                                toolbar={{
                                                                    options: ['inline', 'blockType', 'fontSize', 'list', 'textAlign', 'link', 'embedded', 'emoji', 'image', 'remove', 'history'],
                                                                    image: { alt: { present: true, mandatory: false } }
                                                                }}
                                                            />
                                                        </div>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId='formControlsTextarea'>
                                                        <label>Sub Category<span className='required'>*</span></label>
                                                        <Select
                                                            components={animatedComponents}
                                                            closeMenuOnSelect={false}
                                                            classNamePrefix="select"
                                                            isMulti
                                                            isClearable={true}
                                                            isSearchable={true}
                                                            name="sub_category"
                                                            options={this.state.subCategoryList}
                                                            styles={colourStyles}
                                                            onChange={this.handleSelect}
                                                            theme={
                                                                theme => ({
                                                                    ...theme,
                                                                    borderRadius: 0,
                                                                    colors: {
                                                                        ...theme.colors,
                                                                        primary25: '#0186cf',
                                                                        primary: 'green',
                                                                    },
                                                                })}
                                                        />
                                                        {this.state.subCategoryError ? (
                                                            <span className='errorMsg ml-3'>{this.state.subCategoryError}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId='formControlsTextarea'>
                                                        <label>Status<span className='required'>*</span></label>
                                                        <Field
                                                            name='status'
                                                            component='select'
                                                            className={'form-control'}
                                                            autoComplete='nope'
                                                            placeholder='select'
                                                        >
                                                            <option value='' defaultValue=''>Select Status</option>
                                                            <option value='1' key='1'>Active</option>
                                                            <option value='0' key='0'>Inactive</option>
                                                        </Field>
                                                        {errors.status && touched.status ? (
                                                            <span className='errorMsg ml-3'>{errors.status}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row className='show-grid'>
                                                <Col xs={12} md={12}>&nbsp;</Col>
                                            </Row>
                                            <Row className='show-grid'>
                                                <Col xs={12} md={12} className='text-center'>
                                                    <Button className='blue-btn' type='submit' disabled={isSubmitting}>Save</Button>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={12}>
                                                    <p style={{ paddingTop: '10px' }}><span className='required'>*</span> These fields are required.</p>
                                                </Col>
                                            </Row>
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Col>
                    </Row>
                </div>

                {/*======  confirmation popup  ===== */}
                <Modal
                    show={this.state.showConfirMmsg}
                    onHide={this.handleConfirmReviewClose}
                    className='payOptionPop'
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className='text-center'>
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className='text-center'>
                                <h5>Category has been successfully added</h5>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handleConfirmReviewClose}
                            className='but-gray'
                        >
                            Done
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

AddCategory.propTypes = {
    handleAddConfirMmsg: PropTypes.func,
    businessId: PropTypes.number,
    handelAddModalClose: PropTypes.func,
};

export default AddCategory;