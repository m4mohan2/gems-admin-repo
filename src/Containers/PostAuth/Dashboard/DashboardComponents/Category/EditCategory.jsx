import { Field, Form, Formik } from 'formik';
import React, { Component, Fragment } from 'react';
import { Button, Col, FormControl, FormGroup, Row } from 'react-bootstrap';
import * as Yup from 'yup';
import axios from '../../../../../shared/eaxios';
import * as AppConst from './../../../../../common/constants';
import PropTypes from 'prop-types';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import { connect } from 'react-redux';

//RichTextToolBar
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';

const editVendorSchema = Yup.object().shape({
    category_name: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter category name'),
    status: Yup.string()
        .required('Please select status')
});

class EditCategory extends Component {
    state = {
        errorMessge: null,
        editCategoryEnable: false,
        disabled: false,
        editCategoryLoader: false,
        editErrorMessge: null,
        editorForDesc: EditorState.createEmpty(),
        subCategoryList: [],
        subCategory: [],
        subCategoryError: null,
        permission: []
    };

    static getDerivedStateFromProps(props, state) {
        if (!state.vendorData) {
            return {
                ...props
            };
        }
    }

    handleEditCategoryEnable = () => {
        this.setState({
            editCategoryEnable: true,
            disabled: true
        });
    }

    handleEditCategoryDisable = () => {
        this.setState({
            editCategoryEnable: false,
            disabled: false
        });
        this.props.onReload(this.state.vendorData);
    }

    handleCloseVendor = () => {
        this.props.onClick();
    };

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handleSubmit = (values, { setSubmitting }) => {
        if (this.state.subCategory.length === 0) {
            this.setState({ subCategoryError: 'Please select sub category' });
            setSubmitting(false);
        } else {
            this.setState({ editCategoryLoader: true });
            setSubmitting(true);
            let newValue = {
                category_name: values.category_name,
                description: draftToHtml(convertToRaw(this.state.editorForDesc.getCurrentContent())).trim(),
                subcategory: this.state.subCategory,
                status: values.status
            };
            axios
                .put(AppConst.APIURL + `/api/updateCategory/${values.id}`, newValue)
                .then(res => {
                    console.log('Vendor details get response', res);
                    setSubmitting(false);
                    this.setState({
                        editCategoryLoader: false,
                        editCategoryEnable: false,
                        disabled: false,
                    }, () => {
                        this.props.handleEditConfirMmsg();
                    });
                })
                .catch(e => {
                    let errorMsg = this.displayError(e);
                    this.setState({
                        editCategoryLoader: false,
                        editErrorMessge: errorMsg,
                    });
                    setTimeout(() => {
                        this.setState({ editErrorMessge: null });
                    }, 1000);
                });
        }
    };

    componentDidMount() {
        const facility = this.props.facility;
        const permission = [];
        facility.map(data => {
            data.name === 'category' && data.permission.length !== 0 && permission.push(...data.permission);
        });
        this.setState({ permission });

        const initialValues = { ...this.props };
        const contentBlockDesc = htmlToDraft(initialValues.description.trim());
        if (contentBlockDesc) {
            const contentStateDescription = ContentState.createFromBlockArray(contentBlockDesc.contentBlocks);
            const descriptionContent = EditorState.createWithContent(contentStateDescription);
            this.setState({ editorForDesc: descriptionContent });
        }
        this.setState({ editCategoryLoader: true });
        axios.get(AppConst.APIURL + '/api/subcategoryList')
            .then(res => {
                const subCategory = [];
                res.data.map(sdata => {
                    initialValues.subcategory.map(ss => {
                        return ss.subcategory_id === sdata.value && subCategory.push(sdata);
                    });
                });
                this.setState({ subCategoryList: res.data, editCategoryLoader: false, subCategory });
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    editCategoryLoader: false,
                    editErrorMessge: errorMsg,
                });
                setTimeout(() => {
                    this.setState({ editErrorMessge: null });
                }, 5000);
            });

    }

    onEditorStateChangeAnswer = (editorForDesc) => {
        this.setState({
            editorForDesc,
        });
    };

    handleSelect = (data) => {
        this.setState({ subCategory: data, subCategoryError: null });
    }

    render() {
        const initialValues = { ...this.props };
        const values = {
            id: initialValues.id,
            category_name: initialValues.category_name,
            description: initialValues.description,
            status: initialValues.status,
        };
        const { disabled } = this.state;
        const colourStyles = {
            option: (provided) => ({
                ...provided,
                color: 'black',
            }),
        };
        const animatedComponents = makeAnimated();
        return (
            <div className="addcustomerSec">
                <div className="boxBg p-35">
                    {this.state.editErrorMessge ? (
                        <div className='alert alert-danger' role='alert'>
                            {this.state.editErrorMessge}
                        </div>
                    ) : null}
                    <Row>
                        <Col sm={12}>
                            {this.state.editCategoryLoader ? <LoadingSpinner /> : null}
                        </Col>
                    </Row>
                    <Row className="show-grid">
                        <Col xs={12} md={12}>
                            <div className="sectionTitle mb-5">
                                <h2>{this.state.editCategoryEnable !== true ? 'View' : 'Edit'} Category</h2>
                            </div>
                        </Col>
                        <Col xs={12} className="brd-right">
                            <Formik
                                initialValues={values}
                                validationSchema={editVendorSchema}
                                onSubmit={this.handleSubmit}
                                enableReinitialize={true}
                            >
                                {({
                                    values,
                                    errors,
                                    touched,
                                    isSubmitting
                                }) => {
                                    return (
                                        <Form className={disabled === false ? ('hideRequired') : null}>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Category Name <span className="required">*</span></h3></label>
                                                        <Field
                                                            name="category_name"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.category_name || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.category_name && touched.category_name ? (
                                                            <span className="errorMsg ml-3">{errors.category_name}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Description</h3>
                                                        </label>
                                                        <Editor
                                                            editorState={this.state.editorForDesc}
                                                            toolbarHidden={disabled === false ? true : false}
                                                            wrapperClassName="demo-wrapper"
                                                            editorClassName="form-control rounded-0 mt-n2"
                                                            onEditorStateChange={this.onEditorStateChangeAnswer}
                                                            placeholder="Enter answer"
                                                            readOnly={disabled === false ? 'readOnly' : ''}
                                                            handlePastedText={() => false}
                                                            toolbarClassName="text-body"
                                                            toolbar={{
                                                                options: ['inline', 'blockType', 'fontSize', 'list', 'textAlign', 'link', 'embedded', 'emoji', 'image', 'remove', 'history'],
                                                                image: { alt: { present: true, mandatory: false } }
                                                            }}
                                                        />
                                                        {errors.answer && touched.answer ? (
                                                            <span className="errorMsg ml-3">{errors.answer}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId='formControlsTextarea'>
                                                        <label>Sub Category<span className='required'>*</span></label>
                                                        <Select
                                                            components={animatedComponents}
                                                            value={this.state.subCategory}
                                                            closeMenuOnSelect={false}
                                                            classNamePrefix="select"
                                                            isMulti
                                                            isClearable={true}
                                                            isSearchable={true}
                                                            name="sub_category"
                                                            options={this.state.subCategoryList}
                                                            styles={colourStyles}
                                                            onChange={this.handleSelect}
                                                            isDisabled={disabled === false ? 'Yes' : ''}
                                                            theme={
                                                                theme => ({
                                                                    ...theme,
                                                                    borderRadius: 0,
                                                                    colors: {
                                                                        ...theme.colors,
                                                                        primary25: '#0186cf',
                                                                        primary: 'green',
                                                                    },
                                                                })}
                                                        />
                                                        {this.state.subCategoryError ? (
                                                            <span className='errorMsg ml-3'>{this.state.subCategoryError}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Status<span className='required'>*</span></label>
                                                        <Field
                                                            name="status"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        >
                                                            <option value="">Select Status</option>
                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        {errors.status && touched.status ? (
                                                            <span className="errorMsg ml-3">{errors.status}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>&nbsp;</Col>
                                            </Row>
                                            <Row>&nbsp;</Row>
                                            {this.state.permission[2] && this.state.permission[2].status === true &&
                                                <Row className="show-grid text-center">
                                                    <Col xs={12} md={12}>
                                                        <Fragment>
                                                            {this.state.editCategoryEnable !== true ?
                                                                <Fragment>
                                                                    <Button className="blue-btn border-0" onClick={this.handleEditCategoryEnable}>
                                                                        Edit
                                                </Button>
                                                                </Fragment>
                                                                :
                                                                <Fragment>
                                                                    <Button onClick={this.props.handelviewEditModalClose} className="but-gray border-0 mr-2">
                                                                        Cancel
                                                </Button>
                                                                    <Button type="submit" className="blue-btn ml-2 border-0" disabled={isSubmitting}>
                                                                        Save
                                                </Button>
                                                                </Fragment>}
                                                        </Fragment>
                                                    </Col>
                                                </Row>}
                                            {disabled === false ? null : (<Fragment>
                                                <Row>
                                                    <Col md={12}>
                                                        <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                                    </Col>
                                                </Row>
                                            </Fragment>)}
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        facility: state.auth.facility
    };
};

EditCategory.propTypes = {
    globState: PropTypes.object,
    onClickAction: PropTypes.func,
    onReload: PropTypes.func,
    //onSuccess: PropTypes.func,
    onClick: PropTypes.func,
    handelviewEditModalClose: PropTypes.func,
    handleEditConfirMmsg: PropTypes.func,
    facility: PropTypes.any
};

export default connect(mapStateToProps, null)(EditCategory);