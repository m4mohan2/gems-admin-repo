import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import axios from '../../../../../shared/eaxios';
import PropTypes from 'prop-types';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import { Table, Row, Modal, Col, Image, Button } from 'react-bootstrap';
import SuccessIco from '../../../../../assets/success-ico.png';
import Pagination from 'react-js-pagination';
import EditCategory from './EditCategory';
import AddCategory from './AddCategory';
import * as AppConst from './../../../../../common/constants';
import ReactHtmlParser from 'react-html-parser';
import 'font-awesome/css/font-awesome.min.css';

class Category extends Component {

	state = {
		activePage: 1,
		totalCount: 0,
		itemPerPage: 10,
		sort: 1,
		field: null,
		fetchErrorMsg: null,
		categoryList: [],
		loading: true,
		sortingActiveID: 1,
		bankId: null,
		status: null,
		permission: []
	}

	_isMounted = false;

	displayError = (e) => {
		let errorMessge = '';
		try {
			errorMessge = e.data.message ? e.data.message : e.data.error_description;
		} catch (e) {
			errorMessge = 'Access is denied!';
		}
		return errorMessge;
	}

	sortingActive = (id) => {
		this.setState({
			sortingActiveID: id
		});
	}

	fetchCategoryList = (sort = this.state.sort, field = this.state.field) => {
		this.setState({
			loading: true,
			sort: sort,
			field: field
		}, () => {
			axios
				.get(
					AppConst.APIURL + `/api/categoryListAdmin?&pageSize=${this.state.itemPerPage}&page=${this.state.sort}&searchKey=${this.state.serviceSearchKey}`
				)
				.then(res => {
					const categoryList = res.data.data;
					const totalCount = res.data.total;
					if (this._isMounted) {
						this.setState({
							categoryList,
							totalCount: totalCount,
							loading: false
						});
					}
				})
				.catch(e => {
					let errorMsg = this.displayError(e);
					this.setState({
						fetchErrorMsg: errorMsg,
						loading: false
					}, () => {
						console.log('this.state.fetchErrorMsg', this.state.fetchErrorMsg);
					});
					setTimeout(() => {
						this.setState({ fetchErrorMsg: null });
					}, 5000);
				});
		});
	}

	handlePageChange = pageNumber => {
		this.setState({ activePage: pageNumber });
		this.fetchCategoryList(pageNumber > 0 ? pageNumber : 0, this.state.field);
	};

	handleChangeItemPerPage = (e) => {
		this.setState({ itemPerPage: e.target.value },
			() => {
				this.fetchCategoryList();
			});
	}

	resetPagination = () => {
		this.setState({ activePage: 1 });
	}

	rePagination = () => {
		this.setState({ activePage: 0 });

	}

	handelAddModal = () => {
		this.setState({
			addModal: true,
		});
	}

	handelAddModalClose = () => {
		this.setState({
			addModal: false,
		});
	}

	handleAddConfirMmsg = () => {
		this.setState({
			addConfirMmsg: true,
		}, () => {
			this.fetchCategoryList();
		});
	}

	handleAddConfirMmsgClose = () => {
		this.setState({
			addConfirMmsg: false,
		});
	}

	handleEditConfirMmsg = () => {
		this.setState({
			viewEditModal: false,
			editConfirMmsg: true
		}, () => {
			this.fetchCategoryList();
		});
	}

	handleEditConfirMmsgClose = () => {
		this.setState({
			editConfirMmsg: false
		});
	}

	handelviewEditModal = (data) => {
		this.setState({
			viewEditModal: true,
			vendorData: data
		}, () => {
			console.log('this.state.viewEditData@@', this.state.vendorData);
		});
	}

	handelviewEditModalClose = () => {
		this.setState({
			viewEditModal: false,
		});
	}

	handelStatusModal = (id, status) => {
		this.setState({
			statusChange: true,
			bankId: id,
			status: status
		});
	}

	handleStatusChangedClose = () => {
		this.setState({
			statusConfirMmsg: false,
			successMessage: null
		});
	}

	componentDidMount() {
		this._isMounted = true;
		const facility = this.props.facility;
		const permission = [];
		facility.map(data => {
			data.name === 'category' && data.permission.length !== 0 && permission.push(...data.permission);
		});
		this.setState({ permission }, () => {
			permission[1].status === true &&
				this.fetchCategoryList();
		});
	}

	componentWillMount() {
		this._isMounted = false;
	}

	handleHide = () => {
		this.setState({
			statusChange: false,
			statuserrorMsg: null
		});
	}

	handleStatus(id, status) {

		let updateArray = {
			'status': status
		};

		axios.put(AppConst.APIURL + `/api/statusUpdateCategory/${id}`, updateArray)
			.then(res => {
				console.log('--------------res------', status);
				console.log('--------------res------', res);
				this.setState({
					bankId: null,
					status: null,
					successMessage: 'Status successfully changed',
					statusConfirMmsg: true
				});
				this.handleHide();
				this.fetchCategoryList();
			}

			)
			.catch(e => {
				let errorMsg = this.displayError(e);
				this.setState({
					statuserrorMsg: errorMsg,
					loading: false


				});
				setTimeout(() => {
					this.setState({ errorMessge: null });
				}, 5000);
			});
	}

	handelDeleteModal = (id) => {
		this.setState({
			deleteGem: true,
			id: id
		});
	}

	handleDeleteHide = () => {
		this.setState({
			deleteGem: false,
			deleteerrorMsg: null
		});
	}

	handleDelete(id) {
		axios.delete(AppConst.APIURL + `/api/deleteCategory/${id}`)
			.then(res => {
				console.log('--------------res------', res);
				this.setState({
					id: null,
					successMessage: 'Record deleted successfully',
					deleteConfirMmsg: true
				});
				this.handleDeleteHide();
				this.fetchCategoryList();
			}

			)
			.catch(e => {
				let errorMsg = this.displayError(e);
				this.setState({
					deleteerrorMsg: errorMsg,
					loading: false


				});
				setTimeout(() => {
					this.setState({ errorMessge: null });
				}, 5000);
			});

	}

	handleDeleteChangedClose = () => {
		this.setState({
			deleteConfirMmsg: false,
			successMessage: null
		});
	}

	render() {
		return (
			<div className="dashboardInner businessOuter pt-3">
				{this.state.permission[1] && this.state.permission[1].status === true ?
					<Fragment>
						{this.state.permission[0] && this.state.permission[0].status === true &&
							<Row className="pt-3">
								<Col sm={6} md={6}>
								</Col>
								<Col sm={6} md={6}>
									<button className="btn btn-primary float-right" onClick={() => this.handelAddModal()}>Add Category</button>
								</Col>
							</Row>}
						<Row className="show-grid pt-3">
							<Col sm={12} md={12}>
								<div className="boxBg">
									<Table responsive hover>
										<thead className="theaderBg">
											<tr>
												<th>Name</th>
												<th>Description</th>
												<th className="center">Staus</th>
												<th className="center">Action</th>
											</tr>
										</thead>
										<tbody>
											{this.state.loading ? (<tr>
												<td colSpan={12}>
													<LoadingSpinner />
												</td>
											</tr>) :
												this.state.categoryList.length > 0 ? (
													this.state.categoryList.map(category => (

														<tr key={category.id}>

															<td>{category.category_name}</td>
															<td>{ReactHtmlParser(category.description)}</td>
															<td className="center">
																{
																	category.status === 1
																		? <i className="fa fa-circle green" aria-hidden="true" title="Active"
																			onClick={() => this.state.permission[2] && this.state.permission[2].status === true && this.handelStatusModal(category.id, '0')}></i>
																		: <i className="fa fa-circle red" aria-hidden="true" title="Inactive"
																			onClick={() => this.state.permission[2] && this.state.permission[2].status === true && this.handelStatusModal(category.id, '1')}></i>
																}
															</td>
															<td className="center">
																<i className="fa fa-eye" aria-hidden="true" title="View" onClick={() => this.handelviewEditModal(category)}></i>

																{this.state.permission[3] && this.state.permission[3].status === true &&
																	<i className="fa fa-trash-o red ml-2" aria-hidden="true" title="Delete" onClick={() => this.handelDeleteModal(category.id)}></i>
																}
															</td>
														</tr>
													)))
													:
													this.state.fetchErrorMsg ? null : (
														<tr>
															<td colSpan={12}>
																<p className="text-center">No records found</p>
															</td>
														</tr>
													)
											}
										</tbody>
									</Table>
								</div>
							</Col>
						</Row>

						{this.state.totalCount ? (
							<Row>
								<Col md={4} className="d-flex flex-row mt-20">
									<span className="mr-2 mt-2 font-weight-500">Items per page</span>
									<select
										id={this.state.itemPerPage}
										className="form-control truncatefloat-left w-90"
										onChange={this.handleChangeItemPerPage}
										value={this.state.itemPerPage}>
										<option value='10'>10</option>
										<option value='25'>25</option>
										<option value='50'>50</option>
										<option value='100'>100</option>
									</select>
								</Col>
								<Col md={8}>
									<div className="paginationOuter text-right">
										<Pagination
											activePage={this.state.activePage}
											itemsCountPerPage={this.state.itemPerPage}
											totalItemsCount={this.state.totalCount}
											onChange={this.handlePageChange}
										/>
									</div>
								</Col>
							</Row>
						) : null}
					</Fragment>
					: <h5 className="text-center p-3">You do not have any permission to view this content.</h5>
				}

				{/* Add Category Modal */}
				<Modal
					show={this.state.addModal}
					onHide={this.handelAddModalClose}
					className="right full noPadding slideModal"
				>
					<Modal.Header closeButton></Modal.Header>
					<Modal.Body className="">
						<div className="modalHeader">
							<Row>
								<Col md={9}>
									<h1>Add Category</h1>
								</Col>
							</Row>
						</div>
						<div className="modalBody content-body noTabs">
							<AddCategory
								handleAddConfirMmsg={this.handleAddConfirMmsg}
								//businessId={this.props.globalState.business.businessData.id}
								handelAddModalClose={this.handelAddModalClose}
							/>
						</div>
					</Modal.Body>

				</Modal>

				{/* Edit Category Modal */}
				<Modal
					show={this.state.viewEditModal}
					onHide={this.handelviewEditModalClose}
					className="right full noPadding slideModal"
				>
					<Modal.Header closeButton></Modal.Header>
					<Modal.Body className="">
						<div className="modalBody content-body noTabs">
							<EditCategory
								{...this.state.vendorData}
								handelviewEditModalClose={this.handelviewEditModalClose}
								handleEditConfirMmsg={this.handleEditConfirMmsg} />
						</div>
					</Modal.Body>

				</Modal>

				{/*====== Edit confirmation popup  ===== */}
				<Modal
					show={this.state.editConfirMmsg}
					onHide={this.handleEditConfirMmsgClose}
					className="payOptionPop"
				>
					<Modal.Body>
						<Row>
							<Col md={12} className="text-center">
								<Image src={SuccessIco} />
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<h5>Record has been successfully edited</h5>
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<Button
									onClick={this.handleEditConfirMmsgClose}
									className="but-gray"
								>
									Return
                                </Button>
							</Col>
						</Row>
					</Modal.Body>
				</Modal>

				{/*======  Add confirmation popup  ===== */}
				<Modal
					show={this.state.addConfirMmsg}
					onHide={this.handleAddConfirMmsgClose}
					className="payOptionPop"
				>
					<Modal.Body>
						<Row>
							<Col md={12} className="text-center">
								<Image src={SuccessIco} />
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<h5>Record has been successfully Added</h5>
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<Button
									onClick={this.handleAddConfirMmsgClose}
									className="but-gray"
								>
									Return
                                </Button>
							</Col>
						</Row>
					</Modal.Body>
				</Modal>

				{/*========================= Modal for Status change =====================*/}
				<Modal
					show={this.state.statusChange}
					onHide={this.handleHide}
					dialogClassName="modal-90w"
					aria-labelledby="example-custom-modal-styling-title"
				>
					<Modal.Body>
						<div className="m-auto text-center">
							<h6 className="mb-3 text-dark">Do you want to change this status?</h6>
						</div>
						{this.state.statuserrorMsg ? <div className="alert alert-danger my-3 text-center col-12" role="alert">{this.state.statuserrorMsg}</div> : null}
						<div className="m-auto text-center">
							<button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.handleHide()}>Return</button>
							{this.state.statuserrorMsg == null ? <button className="btn btn-danger" onClick={() => this.handleStatus(this.state.bankId, this.state.status)}>Confirm</button> : null}
						</div>

					</Modal.Body>
				</Modal>

				{/*====== Status change confirmation popup  ===== */}
				<Modal
					show={this.state.statusConfirMmsg}
					onHide={this.handleStatusChangedClose}
					className="payOptionPop"
				>
					<Modal.Body className="text-center">
						<Row>
							<Col md={12} className="text-center">
								<Image src={SuccessIco} />
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<h5>{this.state.successMessage}</h5>
							</Col>
						</Row>
						<Button
							onClick={this.handleStatusChangedClose}
							className="but-gray mt-3"
						>
							Return
                        </Button>
					</Modal.Body>

				</Modal>

				{/*========================= Modal for Delete Gem =====================*/}
				<Modal
					show={this.state.deleteGem}
					onHide={this.handleHide}
					dialogClassName="modal-90w"
					aria-labelledby="example-custom-modal-styling-title"
				>
					<Modal.Body>
						<div className="m-auto text-center">
							<h6 className="mb-3 text-dark">Do you want to delete this Gem?</h6>
						</div>
						{this.state.deleteerrorMsg ? <div className="alert alert-danger my-3 text-center col-12" role="alert">{this.state.deleteerrorMsg}</div> : null}
						<div className="m-auto text-center">
							<button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.handleDeleteHide()}>Return</button>
							{this.state.deleteerrorMsg == null ? <button className="btn btn-danger" onClick={() => this.handleDelete(this.state.id)}>Confirm</button> : null}
						</div>

					</Modal.Body>
				</Modal>

				{/*====== Delete Gem confirmation popup  ===== */}
				<Modal
					show={this.state.deleteConfirMmsg}
					onHide={this.handleDeleteChangedClose}
					className="payOptionPop"
				>
					<Modal.Body className="text-center">
						<Row>
							<Col md={12} className="text-center">
								<Image src={SuccessIco} />
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<h5>{this.state.successMessage}</h5>
							</Col>
						</Row>
						<Button
							onClick={this.handleDeleteChangedClose}
							className="but-gray mt-3"
						>
							Return
                        </Button>
					</Modal.Body>

				</Modal>


			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		globalState: state,
		facility: state.auth.facility
	};
};

Category.propTypes = {
	globalState: PropTypes.object,
	facility: PropTypes.any
};

export default connect(mapStateToProps, null)(Category);