import { Field, Form, Formik } from 'formik';
import React, { Component, Fragment } from 'react';
import { Button, Col, FormControl, FormGroup, Row } from 'react-bootstrap';
import * as Yup from 'yup';
import axios from '../../../../../shared/eaxios';
import * as AppConst from './../../../../../common/constants';
import PropTypes from 'prop-types';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import { connect } from 'react-redux';

const editVendorSchema = Yup.object().shape({
    subcategory_name: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter subcategory name')
});

class EditSubcategory extends Component {
    state = {
        errorMessge: null,
        editSubcategoryEnable: false,
        disabled: false,
        editSubcategoryLoader: false,
        editErrorMessge: null,
        permission: []
    };

    static getDerivedStateFromProps(props, state) {
        if (!state.vendorData) {
            return {
                ...props
            };
        }
    }

    handleEditSubcategoryEnable = () => {
        this.setState({
            editSubcategoryEnable: true,
            disabled: true
        });
    }

    handleEditSubcategoryDisable = () => {
        this.setState({
            editSubcategoryEnable: false,
            disabled: false
        });
        this.props.onReload(this.state.vendorData);
    }

    handleCloseVendor = () => {
        this.props.onClick();
    };

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handleSubmit = (values, { setSubmitting }) => {
        this.setState({ editSubcategoryLoader: true });
        let newValue = {
            name: values.subcategory_name,
        };
        axios
            .put(AppConst.APIURL + `/api/updateSubcategory/${values.id}`, newValue)
            .then(res => {
                console.log('Vendor details get response', res);
                setSubmitting(false);
                this.setState({
                    editSubcategoryLoader: false,
                    editSubcategoryEnable: false,
                    disabled: false,
                }, () => {
                    this.props.handleEditConfirMmsg();
                });
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    editSubcategoryLoader: false,
                    editErrorMessge: errorMsg,
                });
                setTimeout(() => {
                    this.setState({ editErrorMessge: null });
                }, 1000);
            });

    };

    componentDidMount() {
        const facility = this.props.facility;
        const permission = [];
        facility.map(data => {
            data.name === 'subcategory' && data.permission.length !== 0 && permission.push(...data.permission);
        });
        this.setState({ permission });
    }

    render() {
        const initialValues = { ...this.props };
        console.log(initialValues);
        const values = {
            id: initialValues.id,
            subcategory_name: initialValues.name,
        };
        const { disabled } = this.state;

        return (
            <div className="addcustomerSec">
                <div className="boxBg p-35">
                    {this.state.editErrorMessge ? (
                        <div className='alert alert-danger' role='alert'>
                            {this.state.editErrorMessge}
                        </div>
                    ) : null}
                    <Row>
                        <Col sm={12}>
                            {this.state.editSubcategoryLoader ? <LoadingSpinner /> : null}
                        </Col>
                    </Row>
                    <Row className="show-grid">
                        <Col xs={12} md={12}>
                            <div className="sectionTitle mb-5">
                                <h2>{this.state.editSubcategoryEnable !== true ? 'View' : 'Edit'} Subcategory</h2>
                            </div>
                        </Col>
                        <Col xs={12} className="brd-right">
                            <Formik
                                initialValues={values}
                                validationSchema={editVendorSchema}
                                onSubmit={this.handleSubmit}
                                enableReinitialize={true}
                            >
                                {({
                                    values,
                                    errors,
                                    touched,
                                    isSubmitting
                                }) => {
                                    return (
                                        <Form className={disabled === false ? ('hideRequired') : null}>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label><h3>Subcategory Name <span className="required">*</span></h3></label>
                                                        <Field
                                                            name="subcategory_name"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.subcategory_name || ''}
                                                            disabled={disabled === false ? 'disabled' : ''}
                                                        />
                                                        {errors.subcategory_name && touched.subcategory_name ? (
                                                            <span className="errorMsg ml-3">{errors.subcategory_name}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>&nbsp;</Col>
                                            </Row>
                                            <Row>&nbsp;</Row>
                                            {this.state.permission[2] && this.state.permission[2].status === true &&
                                                <Row className="show-grid text-center">
                                                    <Col xs={12} md={12}>
                                                        <Fragment>
                                                            {this.state.editSubcategoryEnable !== true ?
                                                                <Fragment>
                                                                    <Button className="blue-btn border-0" onClick={this.handleEditSubcategoryEnable}>
                                                                        Edit
                                                                </Button>
                                                                </Fragment>
                                                                :
                                                                <Fragment>
                                                                    <Button onClick={this.props.handelviewEditModalClose} className="but-gray border-0 mr-2">
                                                                        Cancel
                                                                </Button>
                                                                    <Button type="submit" className="blue-btn ml-2 border-0" disabled={isSubmitting}>
                                                                        Save
                                                                </Button>
                                                                </Fragment>}
                                                        </Fragment>
                                                    </Col>
                                                </Row>}
                                            {disabled === false ? null : (<Fragment>
                                                <Row>
                                                    <Col md={12}>
                                                        <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                                    </Col>
                                                </Row>
                                            </Fragment>)}
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        facility: state.auth.facility
    };
};

EditSubcategory.propTypes = {
    globState: PropTypes.object,
    onClickAction: PropTypes.func,
    onReload: PropTypes.func,
    //onSuccess: PropTypes.func,
    onClick: PropTypes.func,
    handelviewEditModalClose: PropTypes.func,
    handleEditConfirMmsg: PropTypes.func,
    facility: PropTypes.any
};
export default connect(mapStateToProps, null)(EditSubcategory);