import { Field, Form, Formik } from 'formik';
import React, { Component } from 'react';
import { Button, Col, FormControl, FormGroup, Image, Modal, Row } from 'react-bootstrap';
import * as Yup from 'yup';
import axios from '../../../../../shared/eaxios';
import SuccessIco from '../../../../../assets/success-ico.png';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import PropTypes from 'prop-types';
import * as AppConst from './../../../../../common/constants';

const initialValues = {
    subcategory_name: '',
    status: ''
};

const addSubcategorySchema = Yup.object().shape({
    subcategory_name: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter subcategory name'),
    status: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please select status')
});

class AddSubcategory extends Component {
    state = {
        showConfirMmsg: false,
        errorMessge: null,
        addSubcategoryLoader: false,
        addErrorMessge: null,
    };

    handleConfirmReviewClose = () => {
        this.setState({ showConfirMmsg: false });
    };

    handleConfirmReviewShow = () => {
        this.setState({ showConfirMmsg: true });
    };

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_answer;
        } catch (err) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handleSubmit = (values, { resetForm, setSubmitting }) => {
        this.setState({ addSubcategoryLoader: true });
        let newValue = {
            name: values.subcategory_name,
            status: values.status
        };
        axios.post(AppConst.APIURL + '/api/addSubcategory', newValue)
            .then(res => {
                console.log(res);
                resetForm({});
                setSubmitting(false);
                this.setState({ showConfirMmsg: true });
                this.props.handelAddModalClose();
                this.props.handleAddConfirMmsg();
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    addSubcategoryLoader: false,
                    addErrorMessge: errorMsg,
                });
                setTimeout(() => {
                    this.setState({ addErrorMessge: null });
                }, 5000);
            });

    };


    render() {

        return (
            <div className='addcustomerSec'>
                <div className='boxBg p-35'>
                    {this.state.addErrorMessge ? (
                        <div className='alert alert-danger' role='alert'>
                            {this.state.addErrorMessge}
                        </div>
                    ) : null}
                    <Row>
                        <Col sm={12}>
                            {this.state.addSubcategoryLoader ? <LoadingSpinner /> : null}
                        </Col>
                    </Row>
                    <Row className='show-grid'>
                        <Col xs={12} className='brd-right'>
                            <Formik
                                initialValues={initialValues}
                                validationSchema={addSubcategorySchema}
                                onSubmit={this.handleSubmit}
                            >
                                {({
                                    values,
                                    errors,
                                    touched,
                                    isSubmitting,
                                }) => {
                                    return (
                                        <Form>
                                            <Row className='show-grid'>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId='formControlsTextarea'>
                                                        <label>Subcategory name <span className='required'>*</span></label>
                                                        <Field
                                                            name='subcategory_name'
                                                            type='text'
                                                            className={'form-control'}
                                                            autoComplete='nope'
                                                            placeholder='Enter subcategory name'
                                                            value={values.subcategory_name || ''}
                                                        />
                                                        {errors.subcategory_name && touched.subcategory_name ? (
                                                            <span className='errorMsg ml-3'>{errors.subcategory_name}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <span><h3>Status<span className="required">*</span></h3></span>
                                                        <Field
                                                            name="status"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >
                                                            <option value="" defaultValue="">Select Status</option>
                                                            <option value="1" key="1">Active</option>
                                                            <option value="0" key="0">Inactive</option>
                                                        </Field>
                                                        {errors.status && touched.status ? (
                                                            <span className="errorMsg ml-3">{errors.status}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                            </Row>
                                            <Row className='show-grid'>
                                                <Col xs={12} md={12}>&nbsp;</Col>
                                            </Row>
                                            <Row className='show-grid'>
                                                <Col xs={12} md={12} className='text-center'>
                                                    <Button className='blue-btn' type='submit' disabled={isSubmitting}>Save</Button>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={12}>
                                                    <p style={{ paddingTop: '10px' }}><span className='required'>*</span> These fields are required.</p>
                                                </Col>
                                            </Row>
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Col>
                    </Row>
                </div>

                {/*======  confirmation popup  ===== */}
                <Modal
                    show={this.state.showConfirMmsg}
                    onHide={this.handleConfirmReviewClose}
                    className='payOptionPop'
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className='text-center'>
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className='text-center'>
                                <h5>Subcategory has been successfully added</h5>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handleConfirmReviewClose}
                            className='but-gray'
                        >
                            Done
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

AddSubcategory.propTypes = {
    handleAddConfirMmsg: PropTypes.func,
    businessId: PropTypes.number,
    handelAddModalClose: PropTypes.func,
};

export default AddSubcategory;