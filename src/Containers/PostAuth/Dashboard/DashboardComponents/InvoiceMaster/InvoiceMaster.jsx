import React, { Component } from 'react';
import { Button, Col, Row, Table, Collapse } from 'react-bootstrap';
import './invoiceMaster.scss';

class InvoiceMaster extends Component {

    state = {
        openTransactionDetails: false,
        openAuditTrail: false
    }

    render() {
        const { openTransactionDetails, openAuditTrail } = this.state;

        return (
            <div className="invoicMasterWrap">
                <div className="invoiceMasterHeader">
                    <Row>
                        <Col md={3}>
                            <h1>Invoice</h1>
                        </Col>
                        <Col md={7} className="text-center">
                            <Button className="approve-btn" >
                                Review
                            </Button>
                        </Col>
                        <Col md={2}>
                            <button>
                                <span className="glyphicon glyphicon-trash" aria-hidden="true"></span>
                            </button>
                        </Col>
                    </Row>
                </div>

                <div className="invoiceMasterBody content-body">
                    <Row>
                        <Col md={9}>
                            <div className="content-block">
                                <div className="invAddressAmount">
                                    <Row>
                                        <Col md={4}>
                                            <div className="print-billTo">
                                                <h3>REMIT To</h3>
                                                <h4>ZURN INDUSTRIES</h4>
                                                <p><span>1074  Richison Drive</span>
                                                    USA</p>
                                                <p>Phone: 1 414-643-3000</p>
                                            </div>
                                        </Col>
                                        <Col md={4}>
                                            <div className="print-billTo">
                                                <h3>REMIT To</h3>
                                                <h4>ZURN INDUSTRIES</h4>
                                                <p><span>1074  Richison Drive</span>
                                                    USA</p>
                                                <p>Phone: 1 414-643-3000</p>
                                            </div>
                                        </Col>
                                        <Col md={4}>
                                            <div className="print-invoice-details">
                                                <h3>&nbsp;</h3>
                                                <Table className="leftColBlack">
                                                    <tr>
                                                        <td width="48%"><h4>Invoice No</h4></td>
                                                        <td width="50%"><h4>2222222222</h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Invoice Date</td>
                                                        <td><span>2019-11-01</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Due Date</td>
                                                        <td><span>2019-11-01</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>P.O.</td>
                                                        <td><span>	20</span></td>
                                                    </tr>

                                                    <tr>
                                                        <td>Terms</td>
                                                        <td><span>02</span></td>
                                                    </tr>
                                                </Table>
                                            </div>
                                        </Col>
                                    </Row>
                                </div>
                                <Row>
                                    &nbsp;
                                </Row>
                                <Row>
                                    <Col md={12}>
                                        <div id="item-details" className="invoiceTable">
                                            <Table>
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th className="text-right">Unit Price</th>
                                                        <th width="8%">&nbsp;</th>
                                                        <th>Quantity</th>
                                                        <th className="text-right">Subtotal</th>
                                                        <th width="4%">&nbsp;</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <h5>gold</h5>
                                                            <p><span className="itemDescriptionPrint">gold ring</span></p>
                                                        </td>
                                                        <td className="text-right">
                                                            $1.00
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>1</td>
                                                        <td className="text-right">
                                                            $1.00
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </tbody>
                                            </Table>
                                        </div>
                                    </Col>
                                </Row>
                            </div>
                        </Col>

                        <Col md={3}>
                            <div className="content-block rightContentBlock">
                                <div className="invDisplayRightBlock collapaseOuter">
                                    <h3 onClick={() => this.setState({ openTransactionDetails: !openTransactionDetails })}
                                        aria-controls="transaction-details"
                                        aria-expanded={openTransactionDetails}
                                        className={openTransactionDetails === false ? 'collapas-show' : 'collapas-hide'}
                                        style={{ marginBottom: '0' }}
                                    >
                                        Transaction Details
                                    </h3>

                                    <Collapse in={this.state.openTransactionDetails}>
                                        <div id="transaction-details">
                                            <Table responsive>
                                                <tbody>
                                                    <tr className="transDetailsFirstRow">
                                                        <td>
                                                            <span className="companyNameRblock">
                                                                Funding Debit [ACH]
                                                            </span>
                                                        </td>
                                                        <td className="text-right">
                                                            <span className="transactionAmount">
                                                                $0.00
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr className="numberDateRow">
                                                        <td colSpan={2}>
                                                            <span>Trace No #:000000</span>
                                                            <span className="invPaymentDate">12/01/2019</span>
                                                            <span className="due">
                                                                Pending
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </Table>
                                        </div>
                                    </Collapse>
                                </div>

                                <div className="invDisplayRightBlock collapaseOuter">
                                    <h3 onClick={() => this.setState({ openAuditTrail: !openAuditTrail })}
                                        aria-controls="audit-details"
                                        aria-expanded={openAuditTrail}
                                        className={openAuditTrail === false ? 'collapas-show' : 'collapas-hide'}
                                        style={{ marginBottom: '0' }}
                                    >
                                        Audit Trail
                                    </h3>

                                    <Collapse in={this.state.openAuditTrail}>
                                        <div id="audit-details">
                                            <Table>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <span className="companyNameRblock">David Doe</span>
                                                            <br></br>
                                                            2019/01/12
                                                        </td>
                                                        <td className="text-right">
                                                            <span className="statusRblock submit">SUBMITTED</span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </Table>
                                        </div>
                                    </Collapse>
                                </div>


                            </div>
                        </Col>
                    </Row>
                </div >
            </div >
        );
    }
}

export default InvoiceMaster;
