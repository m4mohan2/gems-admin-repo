import React, { Component, Fragment } from 'react';
import Moment from 'react-moment';
import axios from '../../../../../shared/eaxios';
import * as AppConst from './../../../../../common/constants';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';

class ClaimDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            userDetails: '',
            fetchErrorMsg: null
        };
    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    static getDerivedStateFromProps(props, state) {
        if (!state.claimDetails) {
            return {
                ...props
            };
        }
    }

    handleUserDetails = (userId) => {
        this.setState({ loading: true });
        axios.get(AppConst.APIURL + `/api/providerInfo?user_id=${userId}`)
            .then(response => {
                this.setState({
                    userDetails: response.data.user,
                    loading: false
                });
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    fetchErrorMsg: errorMsg,
                    loading: false
                });
                setTimeout(() => {
                    this.setState({ fetchErrorMsg: null });
                }, 5000);
            });
    }

    componentDidMount() {
        const initialValues = { ...this.props };
        this.handleUserDetails(initialValues.user_id);
    }

    render() {
        const initialValues = { ...this.props };
        const userDetails = this.state.userDetails;
        return (
            <Fragment>
                {this.state.fetchErrorMsg ? (
                    <div className="alert alert-danger" role="alert">
                        {this.state.fetchErrorMsg}
                    </div>
                ) : null}
                {this.state.loading ? <LoadingSpinner /> :
                    <table>
                        <tbody>
                            <tr>
                                <td width="30%"><b>Claim Status</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{initialValues.action === 0 ? 'Pending' : initialValues.action === 1 ? 'Approved' : 'Rejected'}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Claim Created Date</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{initialValues.created_at && <Moment format="MMMM Do, YYYY HH:mm:ss">{initialValues.created_at}</Moment>}</td>
                            </tr>

                            <tr>
                                <td><h5 className='mb-3 mt-3'>Provider Details</h5></td>
                            </tr>
                            <tr>
                                <td width="30%"><b>First Name</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{userDetails.user_detail && userDetails.user_detail.first_name}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Last Name</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{userDetails.user_detail && userDetails.user_detail.last_name}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Phone Number</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{userDetails.user_detail && userDetails.user_detail.phone}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Email</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{initialValues.provider.email}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Address</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{userDetails.user_detail && userDetails.user_detail.address} {userDetails.user_detail && userDetails.user_detail.address2}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Country</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{userDetails.user_country && userDetails.user_country.name}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>State</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{userDetails.user_state && userDetails.user_state.name}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>City</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{initialValues.user_city && initialValues.user_city.name}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>ZIP</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{userDetails.user_detail && userDetails.user_detail.zip}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Status</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{userDetails.active_status === 1 ? 'Active' : 'Inactive'}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Created Date</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{userDetails.user_detail && userDetails.user_detail.created_at && <Moment format="MMMM Do, YYYY HH:mm:ss">{userDetails.user_detail && userDetails.user_detail.created_at}</Moment>}</td>
                            </tr>

                            <tr>
                                <td><h5 className='mb-3 mt-3'>Company Details</h5></td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Company Name</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{userDetails.company_detail && userDetails.company_detail.company_name}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Company Phone</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{userDetails.company_detail && userDetails.company_detail.company_phone}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Company Email</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{userDetails.company_detail && userDetails.company_detail.company_email}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Company Address</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{userDetails.company_detail && userDetails.company_detail.addr1} {userDetails.company_detail && userDetails.company_detail.addr2}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Company Country</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{userDetails.company_country && userDetails.company_country.name}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Company State</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{userDetails.company_state && userDetails.company_state.name}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Company City</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{initialValues.company_city && initialValues.company_city.name}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Company ZIP</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{userDetails.company_detail && userDetails.company_detail.zip}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Created Date</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{userDetails.company_detail && userDetails.company_detail.created_at && <Moment format="MMMM Do, YYYY HH:mm:ss">{userDetails.company_detail && userDetails.company_detail.created_at}</Moment>}</td>
                            </tr>

                            <tr>
                                <td><h5 className='mb-3 mt-3'>Gems Details</h5></td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Gems Name</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{initialValues.gems.name}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Address</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{initialValues.gems.address}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Phone Number</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{initialValues.gems.country_code} {initialValues.gems.phone_no}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Status</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{initialValues.gems.status === 1 ? 'Active' : 'Inactive'}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Created Date</b></td>
                                <td width="10%">:</td>
                                <td width="60%">{initialValues.gems.created_at && <Moment format="MMMM Do, YYYY HH:mm:ss">{initialValues.gems.created_at}</Moment>}</td>
                            </tr>
                        </tbody>
                    </table>
                }
            </Fragment>
        );
    }
}

export default ClaimDetail;