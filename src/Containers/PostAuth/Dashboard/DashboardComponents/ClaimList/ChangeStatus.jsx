import React, { Component } from 'react';
import axios from '../../../../../shared/eaxios';
import * as AppConst from './../../../../../common/constants';
import { Button, Col, FormControl, FormGroup, Image, Modal, Row } from 'react-bootstrap';
import { Field, Form, Formik } from 'formik';
import SuccessIco from '../../../../../assets/success-ico.png';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import PropTypes from 'prop-types';

class ChangeStatus extends Component {
    state = {
        showConfirMmsg: false,
        errorMessge: null,
        loader: false,
        errorMessage: null,
        successData: '',
    };

    static getDerivedStateFromProps(props, state) {
        if (!state.claimData) {
            return {
                ...props
            };
        }
    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handleChangeStatusClose = () => {
        this.setState({ showConfirMmsg: false });
    };

    handleSubmit = (values, { setSubmitting }) => {
        this.setState({
            loader: true,
        });
        axios
            .get(AppConst.APIURL + `/api/changeClaimStatus/${values.id}?action=${values.action}`)
            .then(res => {
                setSubmitting(false);
                this.setState({
                    loader: false,
                    successData: res
                }, () => {
                    this.props.handelChangeStatusModalClose();
                    this.props.handleChangeStatusConfirMmsg();
                });
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    loader: false,
                    errorMessage: errorMsg,
                });
                setTimeout(() => {
                    this.setState({ errorMessage: null });
                }, 5000);
            });
    };

    render() {
        const initialValues = { ...this.props };
        const values = {
            id: initialValues.id,
            providerEmail: initialValues.provider.email,
            action: initialValues.action,
        };
        return (
            <div className="addcustomerSec">
                <div className="boxBg p-35">
                    {this.state.errorMessage ? (
                        <div className="alert alert-danger" role="alert">
                            {this.state.errorMessage}
                        </div>
                    ) : null}
                    <Row>
                        <Col sm={12}>
                            {this.state.loader ? <LoadingSpinner /> : null}
                        </Col>
                    </Row>
                    <Row className="show-grid">
                        <Col xs={12} className="brd-right">
                            <Formik
                                initialValues={values}
                                onSubmit={this.handleSubmit}
                                enableReinitialize={true}
                            >
                                {({
                                    values,
                                    isSubmitting,
                                }) => {
                                    return (
                                        <Form>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Provider<span className="required">*</span></label>
                                                        <Field
                                                            name="providerEmail"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder=""
                                                            value={values.providerEmail || ''}
                                                            readOnly
                                                        />
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={12}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <span><h3>Status<span className="required">*</span></h3></span>
                                                        <Field
                                                            name="action"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                        >
                                                            <option value="0" key="0">Pending</option>
                                                            <option value="1" key="1">Approved</option>
                                                            <option value="2" key="2">Rejected</option>
                                                        </Field>
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    &nbsp;
                                                </Col>
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12} className="text-center">
                                                    <Button className="blue-btn" type="submit" disabled={isSubmitting}>
                                                        Save
                                                    </Button>
                                                </Col>
                                            </Row>
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Col>
                    </Row>
                </div>

                {/*======  confirmation popup  ===== */}
                <Modal
                    show={this.state.showConfirMmsg}
                    onHide={this.handleChangeStatusClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Status has been successfully changed</h5>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handleChangeStatusClose}
                            className="but-gray" > Done
                        </Button>
                    </Modal.Footer>
                </Modal>

            </div>
        );
    }
}

ChangeStatus.propTypes = {
    handelChangeStatusModalClose: PropTypes.func,
    handleChangeStatusConfirMmsg: PropTypes.func,
};

export default ChangeStatus;