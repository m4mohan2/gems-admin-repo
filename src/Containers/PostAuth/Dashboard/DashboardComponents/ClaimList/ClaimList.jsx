import React, { Component, Fragment } from 'react';
import axios from '../../../../../shared/eaxios';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import { Table, Row, Col, Modal, Image, Button } from 'react-bootstrap';
import SuccessIco from '../../../../../assets/success-ico.png';
import Pagination from 'react-js-pagination';
import * as AppConst from './../../../../../common/constants';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ChangeStatus from './ChangeStatus';
import ClaimDetail from './ClaimDetail';
import { Link } from 'react-router-dom';
import refreshIcon from './../../../../../assets/refreshIcon.png';

class ClaimList extends Component {
    state = {
        activePage: 1,
        totalCount: 0,
        itemPerPage: 10,
        sort: 1,
        field: null,
        fetchErrorMsg: null,
        loading: true,
        statusChange: false,
        statuserrorMsg: null,
        claimList: [],
        claimStatusValue: null,
        serviceSearchKey: '',
        claimData: '',
        permission: [],
        claimDetails: '',
        status: '',
        errorMessageForSearch: '',
        claimId: null,
        statusForChange: '',
    }

    _isMounted = false;

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    fetchClaimList = (sort = this.state.sort, field = this.state.field) => {
        this.setState({
            loading: true,
            sort: sort,
            field: field
        }, () => {
            axios.get(AppConst.APIURL +
                `/api/claimList?pageSize=${this.state.itemPerPage}&page=${this.state.sort}&action=${this.state.status}`)
                .then(res => {
                    const claimList = res.data.message.data;
                    const totalCount = res.data.message.total;
                    if (this._isMounted) {
                        this.setState({
                            claimList,
                            totalCount: totalCount,
                            loading: false
                        });
                    }
                })
                .catch(e => {
                    let errorMsg = this.displayError(e);
                    this.setState({
                        fetchErrorMsg: errorMsg,
                        loading: false
                    });
                    setTimeout(() => {
                        this.setState({ fetchErrorMsg: null });
                    }, 5000);
                });
        });
    }

    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber });
        this.fetchClaimList(pageNumber > 0 ? pageNumber : 0, this.state.field);
    };

    handleChangeItemPerPage = (e) => {
        this.setState({ itemPerPage: e.target.value },
            () => {
                this.fetchClaimList();
            });
    }

    resetPagination = () => {
        this.setState({ activePage: 1 });
    }

    rePagination = () => {
        this.setState({ activePage: 0 });
    }

    componentDidMount() {
        this._isMounted = true;
        const facility = this.props.facility;
        const permission = [];
        facility.map(data => {
            data.name === 'claimlist' && data.permission.length !== 0 && permission.push(...data.permission);
        });
        this.setState({ permission }, () => {
            permission[1].status === true &&
                this.fetchClaimList();
        });
    }

    componentWillMount() {
        this._isMounted = false;
    }

    handleChangeStatus = (data) => {
        this.setState({
            viewStatusChangeModel: true,
            claimData: data
        });
    }

    handelChangeStatusModalClose = () => {
        this.setState({
            viewStatusChangeModel: false,
        });
    }

    handleChangeStatusConfirMmsg = () => {
        this.setState({
            viewStatusChangeModel: false,
            changeStatusConfirMmsg: true
        });
        this.fetchClaimList();
    }

    handleChangeStatusConfirMmsgClose = () => {
        this.setState({ changeStatusConfirMmsg: false });
    }

    handleClaimDetail = (data) => {
        this.setState({
            viewClaimDetails: true,
            claimDetails: data
        });
    }

    handleClaimDetailClose = () => {
        this.setState({ viewClaimDetails: false });
    }

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value, errorMessageForSearch: '' });
    }

    handleSearchSubmit = (e) => {
        e.preventDefault();
        if (this.state.status === '') {
            this.setState({ errorMessageForSearch: 'Status is required to search claim list.' });
        } else {
            this.setState({ sort: 1, activePage: 1 }, () => {
                this.fetchClaimList();
            });
        }
    }

    resetSearch = () => {
        this.setState({
            status: '', errorMessageForSearch: '', sort: 1, activePage: 1
        }, () => {
            this.fetchClaimList();
        });
    }

    handelStatusModal = (id, status) => {
        this.setState({
            statusChange: true,
            claimId: id,
            statusForChange: status
        });
    }

    handleHide = () => {
        this.setState({
            statusChange: false,
            statuserrorMsg: null
        });
    }

    handleStatus(id, status) {
        this.setState({
            loader: true,
        });
        axios
            .get(AppConst.APIURL + `/api/changeClaimStatus/${id}?action=${status}`)
            .then(res => {
                this.setState({
                    claimId: null,
                    statusForChange: '',
                    successMessage: res.data.message,
                    statusConfirMmsg: true,
                    loader: false,
                });
                this.handleHide();
                this.fetchClaimList();
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    loader: false,
                    statuserrorMsg: errorMsg,
                });
                setTimeout(() => {
                    this.setState({ statuserrorMsg: null });
                }, 5000);
            });
    }

    handleStatusChangedClose = () => {
        this.setState({
            statusConfirMmsg: false,
            successMessage: null
        });
    }

    render() {
        return (
            <div className="dashboardInner businessOuter pt-3">
                {this.state.permission[0] && this.state.permission[0].status === true ?
                    <Fragment>

                        <form onSubmit={this.handleSearchSubmit}>
                            <div className="searchBox">
                                <div className="row">
                                    <div className="col-12 col-md-3">
                                        <select className="form-control" name="status" value={this.state.status} onChange={this.handleChange}>
                                            <option key="" value="">Select status</option>
                                            <option value="0" key="0">Pending</option>
                                            <option value="1" key="1">Approved</option>
                                            <option value="2" key="2">Rejected</option>
                                        </select>
                                    </div>
                                    <div className="col-12 col-md-3">
                                        <input type="submit" value="Search" className="btn btn-primary" />
                                        <Link to="#" className='ml-3 mt-2'>
                                            <Image src={refreshIcon} onClick={() => this.resetSearch()} />
                                        </Link>
                                    </div>
                                </div>
                                {this.state.errorMessageForSearch &&
                                    <div className="text-success text-center mt-3">
                                        {this.state.errorMessageForSearch}
                                    </div>}
                            </div>
                        </form>

                        <Row className="show-grid pt-3">
                            <Col sm={12} md={12}>
                                <div className="boxBg">
                                    <Table responsive hover>
                                        <thead className="theaderBg">
                                            <tr>
                                                <th>Gems Name</th>
                                                <th>Provider</th>
                                                <th>Company Name</th>
                                                <th>Document</th>
                                                <th className='text-center'>Status</th>
                                                <th className='text-center'>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.loading ? (<tr><td colSpan={12}><LoadingSpinner /></td></tr>) :
                                                this.state.claimList.length > 0 ?
                                                    (this.state.claimList.map(claim => (
                                                        <tr key={claim.id}>
                                                            <td>{claim.gems.name}</td>
                                                            <td>{claim.provider.email}</td>
                                                            <td>{claim.company_detail.company_name}</td>
                                                            <td>{claim.company_documents.length !== 0 ?
                                                                claim.company_documents.map(data => {
                                                                    return <p key={data.id} style={{ marginBottom: '0', paddingBottom: '0' }} className='text-primary'>
                                                                        <a href={AppConst.FILE_BASE_URL + data.file_path}
                                                                            target="_blank" rel="noopener noreferrer" title="Download File">
                                                                            {data.org_file_name}
                                                                        </a>
                                                                    </p>;
                                                                }) : '-'}
                                                            </td>
                                                            <td className='text-center'>{claim.action === 0 ?
                                                                <i className="fa fa-clock-o" aria-hidden="true" title="Pending" onClick={() => this.state.permission[1] && this.state.permission[1].status === true && this.handelStatusModal(claim.id, claim.action)}></i> :
                                                                claim.action === 1 ? <i className="fa fa-circle green" aria-hidden="true" title="Approved" onClick={() => this.state.permission[1] && this.state.permission[1].status === true && this.handelStatusModal(claim.id, claim.action)}></i> :
                                                                    <i className="fa fa-circle red" aria-hidden="true" title="Rejected" onClick={() => this.state.permission[1] && this.state.permission[1].status === true && this.handelStatusModal(claim.id, claim.action)}></i>
                                                            }</td>
                                                            <td className='text-center'>
                                                                <i className="fa fa-tags ml-3" aria-hidden="true" title="View Details" onClick={() => this.handleClaimDetail(claim)}></i>
                                                                {/* {this.state.permission[1] && this.state.permission[1].status === true ?
                                                                    <i className="fa fa-pencil-square-o ml-2" aria-hidden="true" title="Change Status" onClick={() => this.handleChangeStatus(claim)}></i>
                                                                    : '-'} */}
                                                            </td>
                                                        </tr>
                                                    )))
                                                    : this.state.fetchErrorMsg ? null : (<tr>
                                                        <td colSpan={12}>
                                                            <p className="text-center">No records found</p>
                                                        </td>
                                                    </tr>)
                                            }
                                        </tbody>
                                    </Table>
                                </div>
                            </Col>
                        </Row>
                        {this.state.totalCount ? (
                            <Row>
                                <Col md={4} className="d-flex flex-row mt-20">
                                    <span className="mr-2 mt-2 font-weight-500">Items per page</span>
                                    <select
                                        id={this.state.itemPerPage}
                                        className="form-control truncatefloat-left w-90"
                                        onChange={this.handleChangeItemPerPage}
                                        value={this.state.itemPerPage}>
                                        <option value='10'>10</option>
                                        <option value='50'>50</option>
                                        <option value='100'>100</option>
                                        <option value='150'>150</option>
                                        <option value='200'>200</option>
                                        <option value='250'>250</option>
                                    </select>
                                </Col>
                                <Col md={8}>
                                    <div className="paginationOuter text-right">
                                        <Pagination
                                            activePage={this.state.activePage}
                                            itemsCountPerPage={this.state.itemPerPage}
                                            totalItemsCount={this.state.totalCount}
                                            onChange={this.handlePageChange}
                                        />
                                    </div>
                                </Col>
                            </Row>
                        ) : null}
                    </Fragment>
                    : <h5 className="text-center p-3">You do not have any permission to view this content.</h5>}
                {/* Change Status Modal */}
                <Modal show={this.state.viewStatusChangeModel}
                    onHide={this.handelChangeStatusModalClose}
                    className="right full noPadding slideModal" >
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body className="">
                        <div className="modalHeader">
                            <Row>
                                <Col md={9}>
                                    <h1>Change Status</h1>
                                </Col>
                            </Row>
                        </div>
                        <div className="modalBody content-body noTabs">
                            <ChangeStatus
                                {...this.state.claimData}
                                handelChangeStatusModalClose={this.handelChangeStatusModalClose}
                                handleChangeStatusConfirMmsg={this.handleChangeStatusConfirMmsg} />
                        </div>
                    </Modal.Body>
                </Modal>

                {/*====== Change Status Modal confirmation popup  ===== */}
                <Modal
                    show={this.state.changeStatusConfirMmsg}
                    onHide={this.handleChangeStatusConfirMmsgClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Status has been successfully changed</h5>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <Button
                                    onClick={this.handleChangeStatusConfirMmsgClose}
                                    className="but-gray"
                                >
                                    Return
                                </Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                {/* View claim details Modal */}
                <Modal
                    show={this.state.viewClaimDetails}
                    onHide={this.handleClaimDetailClose}
                    className="right half noPadding slideModal"
                >
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body className="">
                        <div className="modalHeader">
                            <Row>
                                <Col md={9}>
                                    <h1>View Claim Details</h1>
                                </Col>
                            </Row>
                        </div>
                        <div className="modalBody content-body noTabs">
                            <ClaimDetail
                                {...this.state.claimDetails}
                                handleClaimDetailClose={this.handleClaimDetailClose}
                            />
                        </div>
                    </Modal.Body>
                </Modal>

                {/*========================= Modal for Status change =====================*/}
                <Modal
                    show={this.state.statusChange}
                    onHide={this.handleHide}
                    dialogClassName="modal-90w"
                    aria-labelledby="example-custom-modal-styling-title"
                >
                    <Modal.Body>
                        {this.state.loader ? <LoadingSpinner /> : null}
                        <div className="m-auto text-center">
                            <h6 className="mb-3 text-dark">Do you want to change this status?</h6>
                        </div>

                        <div className="m-5">
                            <select className="form-control rounded shadow" name="statusForChange" value={this.state.statusForChange} onChange={this.handleChange}>
                                <option value="0" key="0">Pending</option>
                                <option value="1" key="1">Approved</option>
                                <option value="2" key="2">Rejected</option>
                            </select>
                        </div>
                        {this.state.statuserrorMsg ? <div className="alert alert-danger my-3 text-center col-12" role="alert">{this.state.statuserrorMsg}</div> : null}
                        <div className="m-auto text-center">
                            <button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.handleHide()}>Return</button>
                            {this.state.statuserrorMsg == null ? <button className="btn btn-danger" onClick={() => this.handleStatus(this.state.claimId, this.state.statusForChange)}>Confirm</button> : null}
                        </div>

                    </Modal.Body>
                </Modal>

                {/*====== Status change confirmation popup  ===== */}
                <Modal
                    show={this.state.statusConfirMmsg}
                    onHide={this.handleStatusChangedClose}
                    className="payOptionPop"
                >
                    <Modal.Body className="text-center">
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>{this.state.successMessage}</h5>
                            </Col>
                        </Row>
                        <Button
                            onClick={this.handleStatusChangedClose}
                            className="but-gray mt-3"
                        >
                            Return
                        </Button>
                    </Modal.Body>
                </Modal>

            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        facility: state.auth.facility
    };
};

ClaimList.propTypes = {
    facility: PropTypes.any
};

export default connect(mapStateToProps, null)(ClaimList);