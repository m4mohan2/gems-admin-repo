/* eslint-disable no-useless-escape */
import { Field, Form, Formik } from 'formik';
import React, { Component, Fragment } from 'react';
import {
    Button,
    Col,
    FormControl,
    FormGroup,
    Row
} from 'react-bootstrap';
import * as Yup from 'yup';
import axios from '../../../../../shared/eaxios';
import * as AppConst from './../../../../../common/constants';
import PropTypes from 'prop-types';

//RichTextToolBar
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import { connect } from 'react-redux';

//let modifiedObject = {};
//Formik and Yup validation
const editVendorSchema = Yup.object().shape({
    title: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter title'),
    //.max(40, 'maximum characters length 40'),
    short_description: Yup.string()
        // .trim('Please remove whitespace')
        .strict(),
    description: Yup.string()
        // .trim('Please remove whitespace')
        .strict(),
    status: Yup.string()
        // .strict()
        .required('Please select status')
});

class EditCms extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showVendor: false,
            errorMessge: null,
            editVendorEnable: false,
            disabled: false,
            editVendorLoader: false,
            editErrorMessge: false,
            editorStateForShortDesc: EditorState.createEmpty(),
            editorStateForDesc: EditorState.createEmpty(),
            permission: []
        };
    }

    static getDerivedStateFromProps(props, state) {
        if (!state.vendorData) {
            return {
                ...props
            };
        }
    }

    handleEditVendorEnable = () => {
        this.setState({
            editVendorEnable: true,
            disabled: true
        });
    }

    handleEditVendorDisable = () => {
        this.setState({
            editVendorEnable: false,
            disabled: false
        });
        this.props.onReload(this.state.vendorData);
    }

    handleCloseVendor = () => {
        this.props.onClick();
    };


    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    //after editing form submit
    handleSubmit = values => {
        this.setState({
            editVendorLoader: true,
        });
        console.log('handleSubmit', values);

        let newValue = {
            title: values.title,
            short_description: draftToHtml(convertToRaw(this.state.editorStateForShortDesc.getCurrentContent())).trim(),
            description: draftToHtml(convertToRaw(this.state.editorStateForDesc.getCurrentContent())).trim(),
            status: values.status
        };

        const vendorData = {};
        Object.assign(vendorData, newValue);
        console.log('vendorData', vendorData);
        axios
            .put(AppConst.APIURL + `/api/updateCms/${values.id}`, newValue)
            .then(res => {
                console.log('Vendor details get response', res);
                this.setState({
                    editVendorLoader: false,
                    editVendorEnable: false,
                    disabled: false,
                    vendorData
                }, () => {
                    this.props.handleEditConfirMmsg();
                });
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    editVendorLoader: false,
                    editErrorMessge: errorMsg,
                });

                setTimeout(() => {
                    this.setState({ deleteErrorMessge: null });
                }, 1000);

            });
    };


    // handelMod = (getObject) => {
    //     console.log('calling..', getObject);
    //     if (getObject) {

    //         getObject.title && (getObject.title = getObject.title.trim());
    //         getObject.short_description && (getObject.short_description = getObject.short_description.trim());
    //         getObject.description && (getObject.description = getObject.description.trim());
    //     }
    //     return getObject;
    // }

    componentDidMount() {
        const facility = this.props.facility;
        const permission = [];
        facility.map(data => {
            data.name === 'cms' && data.permission.length !== 0 && permission.push(...data.permission);
        });
        this.setState({ permission });

        const initialValues = { ...this.props };
        const contentBlockShortdesc = htmlToDraft(initialValues.short_description.trim());
        const contentBlockDesc = htmlToDraft(initialValues.description.trim());
        if (contentBlockShortdesc && contentBlockDesc) {
            const contentStateShortdescription = ContentState.createFromBlockArray(contentBlockShortdesc.contentBlocks);
            const contentStateDescription = ContentState.createFromBlockArray(contentBlockDesc.contentBlocks);
            const shortDescription = EditorState.createWithContent(contentStateShortdescription);
            const descriptionContent = EditorState.createWithContent(contentStateDescription);
            this.setState({ editorStateForShortDesc: shortDescription, editorStateForDesc: descriptionContent });
        }
    }

    onEditorStateChangeShortDescription = (editorStateForShortDesc) => {
        this.setState({
            editorStateForShortDesc,
        });
    };

    onEditorStateChangeDescription = (editorStateForDesc) => {
        this.setState({
            editorStateForDesc,
        });
    };


    render() {
        const initialValues = { ...this.props };
        // console.log('initial value', initialValues);
        // modifiedObject = this.handelMod(initialValues);
        // console.log('MOD value', modifiedObject);
        const values = {
            id: initialValues.id,
            title: initialValues.title.trim(),
            short_description: initialValues.short_description.trim(),
            description: initialValues.description.trim(),
            status: initialValues.status,
        };
        const {
            disabled
        } = this.state;

        return (
            <Formik
                initialValues={values}
                validationSchema={editVendorSchema}
                onSubmit={this.handleSubmit}
                enableReinitialize={true}
            >
                {({
                    values,
                    errors,
                    touched,
                    //isSubmitting,
                    //handleChange,
                    //setFieldValue,
                    //handleBlur
                }) => {
                    return (
                        <Form className={disabled === false ? ('hideRequired') : null}>
                            <Row className="show-grid">
                                <Col xs={12} md={12}>
                                    <FormGroup controlId="formControlsTextarea">
                                        <label><h3>Title <span className="required">*</span></h3></label>
                                        <Field
                                            name="title"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.title || ''}
                                            disabled={disabled === false ? 'disabled' : ''}
                                        />
                                        {errors.title && touched.title ? (
                                            <span className="errorMsg ml-3">{errors.title}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                                <Col xs={12} md={12}>
                                    <FormGroup controlId="formControlsTextarea">
                                        <label><h3>Short Description</h3>
                                        </label>
                                        <div className='editor'>
                                            <Editor
                                                editorState={this.state.editorStateForShortDesc}
                                                toolbarHidden={disabled === false ? true : false}
                                                wrapperClassName="demo-wrapper"
                                                editorClassName="form-control rounded-0 mt-n2"
                                                onEditorStateChange={this.onEditorStateChangeShortDescription}
                                                placeholder="Enter short description"
                                                readOnly={disabled === false ? 'readOnly' : ''}
                                                toolbarClassName="text-body"
                                                handlePastedText={() => false}
                                                toolbar={{
                                                    options: ['inline', 'blockType', 'fontSize', 'list', 'textAlign', 'link', 'embedded', 'emoji', 'image', 'remove', 'history'],
                                                    image: { alt: { present: true, mandatory: false } }
                                                }}
                                            />
                                        </div>
                                        {/* <Field
                                            name="short_description"
                                            component="textarea"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.short_description || ''}
                                            disabled={disabled === false ? 'disabled' : ''}
                                        /> */}
                                        {errors.short_description && touched.short_description ? (
                                            <span className="errorMsg ml-3">{errors.short_description}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>

                                <Col xs={12} md={12}>
                                    <FormGroup controlId="formControlsTextarea">
                                        <label><h3>Description</h3>
                                        </label>
                                        <Editor
                                            editorState={this.state.editorStateForDesc}
                                            toolbarHidden={disabled === false ? true : false}
                                            wrapperClassName="demo-wrapper"
                                            editorClassName="form-control rounded-0 mt-n2"
                                            onEditorStateChange={this.onEditorStateChangeDescription}
                                            placeholder="Enter description"
                                            readOnly={disabled === false ? 'readOnly' : ''}
                                            toolbarClassName="text-body"
                                            handlePastedText={() => false}
                                            toolbar={{
                                                options: ['inline', 'blockType', 'fontSize', 'list', 'textAlign', 'link', 'embedded', 'emoji', 'image', 'remove', 'history'],
                                                image: { alt: { present: true, mandatory: false } }
                                            }}
                                        />
                                        {/* <Field
                                            name="description"
                                            component="textarea"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.description || ''}
                                            disabled={disabled === false ? 'disabled' : ''}
                                        /> */}
                                        {errors.description && touched.description ? (
                                            <span className="errorMsg ml-3">{errors.description}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>

                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formControlsTextarea">
                                        <span><h3>Status <span className="required">*</span></h3></span>
                                        <Field
                                            name="status"
                                            component="select"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="select"
                                            disabled={disabled === false ? 'disabled' : ''}
                                        >
                                            <option value="">
                                                Select Status
                                            </option>
                                            <option value="1" key="1">
                                                Active
                                            </option>
                                            <option value="0" key="0">
                                                Inactive
                                            </option>
                                        </Field>
                                        {errors.status && touched.status ? (
                                            <span className="errorMsg ml-3">{errors.status}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row className="show-grid">
                                <Col xs={12} md={12}>
                                    &nbsp;
                                </Col>
                            </Row>
                            <Row>&nbsp;</Row>
                            {this.state.permission[1] && this.state.permission[1].status === true &&
                                <Row className="show-grid text-center">
                                    <Col xs={12} md={12}>
                                        <Fragment>
                                            {
                                                this.state.editVendorEnable !== true ? (
                                                    <Fragment>
                                                        <Button className="blue-btn border-0" onClick={this.handleEditVendorEnable}>
                                                            Edit
                                                    </Button>
                                                    </Fragment>
                                                ) : (
                                                        <Fragment>
                                                            <Button
                                                                onClick={this.props.handelviewEditModalClose}
                                                                className="but-gray border-0 mr-2"
                                                            >
                                                                Cancel
                                                    </Button>
                                                            <Button type="submit" className="blue-btn ml-2 border-0">
                                                                Save
                                                    </Button>
                                                        </Fragment>)
                                            }
                                        </Fragment>
                                    </Col>
                                </Row>}
                            {disabled === false ? null : (<Fragment>
                                <Row>
                                    <Col md={12}>
                                        <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                    </Col>
                                </Row>
                            </Fragment>)}
                        </Form>
                    );
                }}
            </Formik>
        );
    }
}

const mapStateToProps = state => {
    return {
        facility: state.auth.facility
    };
};


EditCms.propTypes = {
    globState: PropTypes.object,
    onClickAction: PropTypes.func,
    onReload: PropTypes.func,
    //onSuccess: PropTypes.func,
    onClick: PropTypes.func,
    handelviewEditModalClose: PropTypes.func,
    handleEditConfirMmsg: PropTypes.func,
    facility: PropTypes.any
};

export default connect(mapStateToProps, null)(EditCms);