import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import axios from '../../../../../shared/eaxios';
import PropTypes from 'prop-types';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import { Table, Row, Modal, Col, Image, Button } from 'react-bootstrap';
import { FaCaretDown } from 'react-icons/fa';
import { FaCaretUp } from 'react-icons/fa';
import SuccessIco from '../../../../../assets/success-ico.png';
import Pagination from 'react-js-pagination';
import EditBusinessCustomers from './EditCms';
import AddBusinessCustomers from './AddCms';
import * as AppConst from './../../../../../common/constants';
import ReactHtmlParser from 'react-html-parser';

class Cms extends Component {

	state = {
		activePage: 1,
		totalCount: 0,
		itemPerPage: 10,
		sort: 1,
		field: null,
		fetchErrorMsg: null,
		cmsList: [],
		loading: true,
		sortingActiveID: 1,
		permission: []
	}

	_isMounted = false;

	displayError = (e) => {
		let errorMessge = '';
		try {
			errorMessge = e.data.message ? e.data.message : e.data.error_description;
		} catch (e) {
			errorMessge = 'Access is denied!';
		}
		return errorMessge;
	}

	sortingActive = (id) => {
		this.setState({
			sortingActiveID: id
		});
	}

	fetchCmsList = (sort = this.state.sort, field = this.state.field) => {
		this.setState({
			loading: true,
			sort: sort,
			field: field
		}, () => {
			axios
				.get(
					AppConst.APIURL + `/api/cmsList?&pageSize=${this.state.itemPerPage}&page=${this.state.sort}&searchKey=${this.state.serviceSearchKey}`
				)
				.then(res => {
					const cmsList = res.data;
					const totalCount = res.data.total;
					if (this._isMounted) {
						this.setState({
							cmsList,
							totalCount: totalCount,
							loading: false
						});
					}
				})
				.catch(e => {
					let errorMsg = this.displayError(e);
					this.setState({
						fetchErrorMsg: errorMsg,
						loading: false
					}, () => {
						console.log('this.state.fetchErrorMsg', this.state.fetchErrorMsg);
					});
					setTimeout(() => {
						this.setState({ fetchErrorMsg: null });
					}, 5000);
				});
		});
	}


	handlePageChange = pageNumber => {
		this.setState({ activePage: pageNumber });
		this.fetchCmsList(pageNumber > 0 ? pageNumber : 0, this.state.field);
	};

	handleChangeItemPerPage = (e) => {
		this.setState({ itemPerPage: e.target.value },
			() => {
				this.fetchCmsList();
			});
	}

	resetPagination = () => {
		this.setState({ activePage: 1 });
	}

	rePagination = () => {
		this.setState({ activePage: 0 });

	}

	handelAddModal = () => {
		this.setState({
			addModal: true,
		});
	}

	handelAddModalClose = () => {
		this.setState({
			addModal: false,
		});
	}

	handleAddConfirMmsg = () => {
		this.setState({
			addConfirMmsg: true,
		}, () => {
			this.fetchCmsList();
		});
	}

	handleAddConfirMmsgClose = () => {
		this.setState({
			addConfirMmsg: false,
		});
	}

	handleEditConfirMmsg = () => {
		this.setState({
			viewEditModal: false,
			editConfirMmsg: true
		}, () => {
			this.fetchCmsList();
		});
	}

	handleEditConfirMmsgClose = () => {
		this.setState({
			editConfirMmsg: false
		});
	}

	handelviewEditModal = (data) => {
		this.setState({
			viewEditModal: true,
			vendorData: data
		});
	}

	handelviewEditModalClose = () => {
		this.setState({
			viewEditModal: false,
		});
	}

	componentDidMount() {
		this._isMounted = true;
		const facility = this.props.facility;
		const permission = [];
		facility.map(data => {
			data.name === 'cms' && data.permission.length !== 0 && permission.push(...data.permission);
		});
		this.setState({ permission }, () => {
			permission[0].status === true &&
				this.fetchCmsList();
		});
	}

	componentWillMount() {
		this._isMounted = false;
	}

	handelStatusModal = (id, status) => {
		this.setState({
			statusChange: true,
			bankId: id,
			status: status
		});

	}

	handleStatusChangedClose = () => {
		this.setState({
			statusConfirMmsg: false,
			successMessage: null
		});
	}


	handleHide = () => {
		this.setState({
			statusChange: false,
			statuserrorMsg: null
		});
	}

	handleStatus(id, status) {

		let updateArray = {
			'status': status
		};

		axios.put(AppConst.APIURL + `/api/statusUpdateCms/${id}`, updateArray)
			.then(res => {
				//console.log('--------------res------', status);
				console.log('--------------res------', res);
				this.setState({
					bankId: null,
					status: null,
					successMessage: 'Status successfully changed',
					statusConfirMmsg: true
				});
				this.handleHide();
				this.fetchCmsList();
			}

			)
			.catch(e => {
				let errorMsg = this.displayError(e);
				this.setState({
					statuserrorMsg: errorMsg,
					loading: false


				});
				setTimeout(() => {
					this.setState({ errorMessge: null });
				}, 5000);
			});
	}

	render() {
		return (
			<div className="dashboardInner businessOuter pt-3">
				{this.state.permission[0] && this.state.permission[0].status === true ?
					<Fragment>
						<Row className="pt-3">
							<Col sm={6} md={6}>
							</Col>
							<Col sm={6} md={6}>
								{/* <button className="btn btn-primary float-right" onClick={() => this.handelAddModal()}>Add CMS</button> */}
							</Col>
						</Row>
						<Row className="show-grid pt-3">
							<Col sm={12} md={12}>
								<div className="boxBg">
									<Table responsive hover>
										<thead className="theaderBg">
											<tr>

												<th>
													<span className="float-left pr-2">
														Title
                                            </span>
													<span className="d-flex flex-column-reverse sortingFontSize">
														<button className="custom-btn-focus sortingArrow">
															<FaCaretDown
																className={'cursorPointer ' + (this.state.sortingActiveID == 8 ? 'activeColor' : '')}
																onClick={() => {
																	this.fetchCmsList(0, '', false, 'title');
																	this.sortingActive(8);
																	this.setState({ activePage: 1 });
																}} />
														</button>
														<button className="custom-btn-focus sortingArrow">
															<FaCaretUp
																className={'cursorPointer ' + (this.state.sortingActiveID == 7 ? 'activeColor' : '')}
																onClick={() => {
																	this.fetchCmsList(0, '', true, 'title');
																	this.sortingActive(7);
																	this.setState({ activePage: 1 });
																}} />
														</button>
													</span>
												</th>
												<th>
													<span className="float-left pr-2">
														Short Description
                                            </span>
													<span className="d-flex flex-column-reverse sortingFontSize">
														<button className="custom-btn-focus sortingArrow">
															<FaCaretDown
																className={'cursorPointer ' + (this.state.sortingActiveID == 10 ? 'activeColor' : '')}
																onClick={() => {
																	this.fetchCmsList(0, '', false, 'createdDate');
																	this.sortingActive(10);
																	this.setState({ activePage: 1 });
																}} />
														</button>
														<button className="custom-btn-focus sortingArrow">
															<FaCaretUp
																className={'cursorPointer ' + (this.state.sortingActiveID == 9 ? 'activeColor' : '')}
																onClick={() => {
																	this.fetchCmsList(0, '', true, 'createdDate');
																	this.sortingActive(9);
																	this.setState({ activePage: 1 });
																}} />
														</button>
													</span>
												</th>
												<th className="text-center">Staus</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											{this.state.loading ? (<tr>
												<td colSpan={12}>
													<LoadingSpinner />
												</td>
											</tr>) :
												this.state.cmsList.length > 0 ? (
													this.state.cmsList.map(cms => (

														<tr key={cms.id}>

															<td>{cms.title}</td>
															<td>{ReactHtmlParser(cms.short_description)}</td>
															<td className="center">
																{
																	cms.status === 1
																		? <i className="fa fa-circle green" aria-hidden="true" title="Active" onClick={() => this.handelStatusModal(cms.id, '0')}></i>
																		: <i className="fa fa-circle red" aria-hidden="true" title="Inactive" onClick={() => this.handelStatusModal(cms.id, '1')}></i>
																}
															</td>
															<td><i className="fa fa-eye" aria-hidden="true" title="View" onClick={() => this.handelviewEditModal(cms)}></i></td>
														</tr>


													))
												)
													:
													this.state.fetchErrorMsg ? null : (
														<tr>
															<td colSpan={12}>
																<p className="text-center">No records found</p>
															</td>
														</tr>
													)
											}
										</tbody>
									</Table>
								</div>
							</Col>
						</Row>

						{this.state.totalCount ? (
							<Row className="px-3">
								<Col md={4} className="d-flex flex-row mt-20">
									<span className="mr-2 mt-2 font-weight-500">Items per page</span>
									<select
										id={this.state.itemPerPage}
										className="form-control truncatefloat-left w-90"
										onChange={this.handleChangeItemPerPage}
										value={this.state.itemPerPage}>
										<option value='10'>10</option>
										<option value='25'>25</option>
										<option value='50'>50</option>
										<option value='100'>100</option>

									</select>
								</Col>
								<Col md={8}>
									<div className="paginationOuter text-right">
										<Pagination
											activePage={this.state.activePage}
											itemsCountPerPage={this.state.itemPerPage}
											totalItemsCount={this.state.totalCount}
											onChange={this.handlePageChange}
										/>
									</div>
								</Col>
							</Row>
						) : null}
					</Fragment>
					: <h5 className="text-center p-3">You do not have any permission to view this content.</h5>}


				{/* Add Cms Modal */}
				<Modal
					show={this.state.addModal}
					onHide={this.handelAddModalClose}
					className="right full noPadding slideModal"
				>
					<Modal.Header closeButton></Modal.Header>
					<Modal.Body className="">
						<div className="modalHeader">
							<Row>
								<Col md={9}>
									<h1>Add CMS</h1>
								</Col>
							</Row>
						</div>
						<div className="modalBody content-body noTabs">
							<AddBusinessCustomers
								handleAddConfirMmsg={this.handleAddConfirMmsg}
								//businessId={this.props.globalState.business.businessData.id}
								handelAddModalClose={this.handelAddModalClose}
							/>
						</div>
					</Modal.Body>

				</Modal>

				{/* Edit Cms Modal */}
				<Modal
					show={this.state.viewEditModal}
					onHide={this.handelviewEditModalClose}
					className="right full noPadding slideModal"
				>
					<Modal.Header closeButton></Modal.Header>
					<Modal.Body className="">
						<div className="modalHeader">
							<Row>
								<Col md={9}>
									<h1>Edit Cms</h1>
								</Col>
							</Row>
						</div>
						<div className="modalBody content-body noTabs">
							<EditBusinessCustomers
								{...this.state.vendorData}
								handelviewEditModalClose={this.handelviewEditModalClose}
								handleEditConfirMmsg={this.handleEditConfirMmsg} />
						</div>
					</Modal.Body>

				</Modal>

				{/*====== Edit confirmation popup  ===== */}
				<Modal
					show={this.state.editConfirMmsg}
					onHide={this.handleEditConfirMmsgClose}
					className="payOptionPop"
				>
					<Modal.Body>
						<Row>
							<Col md={12} className="text-center">
								<Image src={SuccessIco} />
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<h5>Record has been successfully edited</h5>
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<Button
									onClick={this.handleEditConfirMmsgClose}
									className="but-gray"
								>
									Return
                                </Button>
							</Col>
						</Row>
					</Modal.Body>
				</Modal>

				{/*======  Add confirmation popup  ===== */}
				<Modal
					show={this.state.addConfirMmsg}
					onHide={this.handleAddConfirMmsgClose}
					className="payOptionPop"
				>
					<Modal.Body>
						<Row>
							<Col md={12} className="text-center">
								<Image src={SuccessIco} />
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<h5>Record has been successfully Added</h5>
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<Button
									onClick={this.handleAddConfirMmsgClose}
									className="but-gray"
								>
									Return
                                </Button>
							</Col>
						</Row>
					</Modal.Body>
				</Modal>


				{/*========================= Modal for Status change =====================*/}
				<Modal
					show={this.state.statusChange}
					onHide={this.handleHide}
					dialogClassName="modal-90w"
					aria-labelledby="example-custom-modal-styling-title"
				>
					<Modal.Body>
						<div className="m-auto text-center">
							<h6 className="mb-3 text-dark">Do you want to change this status?</h6>
						</div>
						{this.state.statuserrorMsg ? <div className="alert alert-danger my-3 text-center col-12" role="alert">{this.state.statuserrorMsg}</div> : null}
						<div className="m-auto text-center">
							<button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.handleHide()}>Return</button>
							{this.state.statuserrorMsg == null ? <button className="btn btn-danger" onClick={() => this.handleStatus(this.state.bankId, this.state.status)}>Confirm</button> : null}
						</div>

					</Modal.Body>
				</Modal>

				{/*====== Status change confirmation popup  ===== */}
				<Modal
					show={this.state.statusConfirMmsg}
					onHide={this.handleStatusChangedClose}
					className="payOptionPop"
				>
					<Modal.Body className="text-center">
						<Row>
							<Col md={12} className="text-center">
								<Image src={SuccessIco} />
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<h5>{this.state.successMessage}</h5>
							</Col>
						</Row>
						<Button
							onClick={this.handleStatusChangedClose}
							className="but-gray mt-3"
						>
							Return
                        </Button>
					</Modal.Body>

				</Modal>


			</div>
		);
	}

}

const mapStateToProps = state => {
	return {
		globalState: state,
		facility: state.auth.facility
	};
};

Cms.propTypes = {
	globalState: PropTypes.object,
	facility: PropTypes.any
};

export default connect(mapStateToProps, null)(Cms);