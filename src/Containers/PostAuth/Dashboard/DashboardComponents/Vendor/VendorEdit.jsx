import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
    Row,
    Col,
    FormGroup,
    FormControl,
    Button,
} from 'react-bootstrap';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';
import InputMask from 'react-input-mask';
import axios from './../../../../../shared/eaxios';
import { CountryService } from '../../../../../services/country.service';

//Formik and Yup validation
const editBusinessSchema = Yup.object().shape({
    companyName: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter Company Name'),
    taxNo: Yup
        .string()
        //.min(10, 'taxNo must be at least 9 characters'),
        .test(
            'taxtValidNo',
            'Please enter minimum 9 digit ',
            function (val) {
                if (val) {
                    // eslint-disable-next-line no-useless-escape
                    const rValue = val.replace(/\-/, '');
                    if ((rValue.length) < 9) {
                        return false;
                    }
                }
                return true;
            }
        ),
    //.required('Please enter business tax ID/SSN'),
    firstName: Yup
        .string()
        .trim('Please remove whitespace')
        .strict(),
    lastName: Yup
        .string()
        .trim('Please remove whitespace')
        .strict(),
    paymentMode: Yup
        .string()
        .notOneOf(['']),
    emailAddress: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .email('Email must be valid'),
    phoneNumber: Yup.string().min(14, 'Phone number must be 10 digit'),
    address: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please add an address'),
    zipCode: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .test(
            'zipCode',
            'Please enter a valid zip code',
            function (value) {
                if (value) {
                    let isValid = false;
                    // eslint-disable-next-line no-useless-escape
                    if (/^\d{5}([\-|\s]\d{1,4})?$/.test(value)) {
                        isValid = true;
                    } else if (/^[a-zA-Z0-9]{3}\s[a-zA-Z0-9]{3}$/.test(value)) {
                        let expArr = value.split(' ');

                        if (/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{3}$/.test(expArr[0]) && /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{3}$/.test(expArr[1])) {
                            isValid = true;
                        }
                    }
                    return isValid;
                }

            }
        ).required('Please enter zip code'),
    //countryCode: Yup.string(),
    countryCode: Yup.string().required('Please select a country'),
    stateCode: Yup.string()
        .trim('Please remove whitespace').required('Please select a state'),
    city: Yup.string()
        .trim('Please remove whitespace'),
    cityId: Yup.number().required('Please select a city'),
});

class VendorEdit extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            showVendor: false,
            countries: [],
            states: [],
            cities: [],
            selectedStates: [],
            selectedCities: [],
            errorMessge: null
        };
    }

    handleCloseVendor = () => {
        this.props.onClick();
    };


    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        this.setState({
            errorMessge: errorMessge
        });

        setTimeout(() => {
            this.setState({ errorMessge: null });
        }, 5000);
    }



    //after editing form submit
    handleSubmit = values => {
        console.log('values', values);

        for (const st of this.state.states) {
            if (parseInt(st.id) === parseInt(values.stateCode)) values.stateCode = st.stateCode;
            console.log(values.stateCode);
        }

        for (const country of this.state.countries) {
            if (parseInt(country.id) === parseInt(values.countryCode)) values.countryCode = country.countryCode;
            console.log(values.countryCode);
        }

        let newValue = {
            companyName: values.companyName,
            //taxNo: values.taxNo,
            taxNo: values.taxNo ? values.taxNo.split('-').join('') : null,
            firstName: values.firstName,
            lastName: values.lastName,
            emailAddress: values.emailAddress,
            phoneNumber: values.phoneNumber,
            paymentMode: values.paymentMode,
            address: values.address,
            zipCode: values.zipCode,
            //countryCode: values.countryCode,
            countryCode: values.countryCode,
            stateCode: values.stateCode,
            cityId: values.cityId,
            ...values
        };
        console.log('new Value submitted 2', newValue);
        console.log('values-----------------------------------------', values);

        let req = {
            url: `/customerVendor/vendor/update/${values.businessId}/${values.id}`,
            method: 'PUT',
            data: newValue
        };

        // let req = {
        //     url: '/customerVendor/vendor/update/4275',
        //     method: 'PUT',
        //     data: newValue
        // };

        
        axios(req)
        //axios.put(`/customerVendor/vendor/update/${values.id}`, {newValue})
            .then(res => {
                console.log('Vendor details get response', res);
                console.log('Vendor details get data', res.data);
                // this.setState({
                //   showCustomer: false,
                //   showVendor: false,
                // });
                this.props.onSuccess();
            })
            .catch(this.displayError);
    };

    componentDidMount() {
        const countryService = new CountryService;
        countryService.getData().then(({ countries, states, cities }) => { //countries, states or cities
            this.setState({ countries, states, cities }, () => {
                this.populateState({ target: { value: this.props.countryCode } });
            });
        });
    }

    populateState = e => {
        const countryCode = e.target.value;
        const states = this.state.states;
        let countryId = 0;
        for (let country of this.state.countries) if (country.countryCode === countryCode) countryId = country.id;
        const newStates = states.filter(state => Number(state.countryId) === Number(countryId));
        this.setState({ selectedStates: newStates, selectedCities: [] }, () => {
            this.populateCity({ target: { value: this.props.stateCode } });
        });
    };

    populateCity = e => {
        console.log('populate city ', e);
        const stateCode = e.target.value;
        const cities = this.state.cities;
        let stateId = 0;
        for (let st of this.state.states) if (st.stateCode === stateCode) stateId = st.id;
        const newCities = cities.filter(ct => Number(ct.stateId) === Number(stateId));
        this.setState({ selectedCities: newCities });
    };

    render() {
        // const initialValues = { ...this.props };
        //console.log("initial value", initialValues);

        let getObject = { ...this.props };
        console.log('getObject==>>=', getObject);
        //if (getObject){
        // blank space removed
        // getObject.companyName = getObject.companyName.trim();
        // getObject.address = getObject.address.trim();
        // getObject.firstName = getObject.firstName.trim();
        // getObject.lastName = getObject.lastName.trim();
        // getObject.taxNo = getObject.taxNo.trim();
        // getObject.zipCode = getObject.zipCode.trim();
        // getObject.emailAddress = getObject.emailAddress.trim();
        //}
        

        /****** phone no formatting started *****/


        if (getObject.phoneNumber !== '') {

            let formatedPhone = '';
            let formatedPhoneArray = [];

            for (let letter of getObject.phoneNumber) {
                if (isNaN(letter) === false) {
                    formatedPhoneArray.push(letter);
                }
            }

            formatedPhoneArray = formatedPhoneArray.filter(entry => entry.trim() !== '');
            if (formatedPhoneArray.length === 11) {
                formatedPhoneArray.splice(0, 1);
            }
            formatedPhoneArray.splice(0, 0, '1 ');
            formatedPhoneArray.splice(5, 0, '-');
            formatedPhoneArray.splice(9, 0, '-');

            for (let n of formatedPhoneArray) {
                formatedPhone += n;
            }

            getObject.phoneNumber = formatedPhone;

        }


        /*********** end of phone no formatting end***********/

        const modifiedObject = getObject; // modifiedObject is final object after all modification done from props data 
        console.log('modifiedObject--------------------', modifiedObject);
        const { 
            selectedCities, 
            selectedStates, 
            //selectedCountryId 
        } = this.state;

        return (
            <Formik
                initialValues={modifiedObject}
                validationSchema={editBusinessSchema}
                onSubmit={this.handleSubmit}
            >
                {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur
                }) => {
                    return (
                        <Form>
                            <Row>
                                <Col md={12}>
                                    {this.state.errorMessge ? (
                                        <div className="alert alert-danger" role="alert">
                                            {this.state.errorMessge}
                                        </div>
                                    ) : null}
                                </Col>
                            </Row>
                            <Row className="show-grid">
                                <Col xs={6} md={6}>
                                    <FormGroup controlId="formBasicText">
                                        <div>Company Name <span className="required">*</span></div>
                                        <Field
                                            name="companyName"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.companyName}
                                        />
                                        {errors.companyName && touched.companyName ? (
                                            <span className="errorMsg">{errors.companyName}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>

                                <Col xs={6} md={6}>
                                    <FormGroup controlId="formBasicText">
                                        <div>Business Tax ID/SSN</div>
                                        {/* <Field
                                            name="taxNo"
                                            type="text"
                                            className={`form-control`}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.taxNo}
                                        /> */}
                                        <InputMask
                                            mask="99-99999999"
                                            maskChar={null}
                                            type="text"
                                            name="taxNo"
                                            className={'form-control'}
                                            value={values.taxNo}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                        {errors.taxNo && touched.taxNo ? (
                                            <span className="errorMsg">{errors.taxNo}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                        {/* <HelpBlock>Validation Text</HelpBlock> */}
                                    </FormGroup>
                                </Col>
                            </Row>

                            <Row className="show-grid">
                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formControlsTextarea">
                                        <div>Company Street Address <span className="required">*</span></div>
                                        <Field
                                            name="address"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.address}
                                        />
                                        {errors.address && touched.address ? (
                                            <span className="errorMsg">{errors.address}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formBasicText">
                                        <div>Country <span className="required">*</span></div>
                                        <Field
                                            component="select"
                                            name="countryCode"
                                            placeholder="select"
                                            className="form-control"
                                            value={values.countryCode || ''}
                                            onChange={e => {
                                                handleChange(e);
                                                this.populateState(e);
                                            }}
                                        >
                                            <option value="">
                                                {this.state.countries.length ? 'Select Country' : 'Loading...'}
                                            </option>


                                            {this.state.countries.map(country => (
                                                <option key={country.id} value={country.countryCode}>
                                                    {country.countryName}
                                                </option>
                                            ))}
                                        </Field>
                                        {errors.countryCode && touched.countryCode ? (
                                            <span className="errorMsg">{errors.countryCode}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                            </Row>

                            <Row className="show-grid">
                                <Col xs={12} md={6} className="cityField">
                                    <FormGroup controlId="formBasicText">
                                        <div>State <span className="required">*</span></div>
                                        <Field
                                            component="select"
                                            name="stateCode"
                                            placeholder="select"
                                            className="form-control"
                                            value={values.stateCode || ''}
                                            onChange={e => {
                                                handleChange(e);
                                                this.populateCity(e);
                                            }}
                                            disabled={selectedStates.length ? false : true}
                                        >
                                            <option value="">Select State</option>

                                            {selectedStates.map(stName => (
                                                <option value={stName.stateCode} key={stName.id}>
                                                    {stName.stateName}
                                                </option>
                                            ))}
                                        </Field>
                                        {errors.stateCode && touched.stateCode ? (
                                            <span className="errorMsg">{errors.stateCode}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                                <Col xs={12} md={6} className="cityField">
                                    <FormGroup controlId="formBasicText">
                                        <div>City  <span className="required">*</span></div>

                                        <Field
                                            component="select"
                                            name="cityId"
                                            placeholder="select"
                                            className="form-control"
                                            value={values.cityId || ''}
                                            onChange={e => {
                                                handleChange(e);
                                            }}
                                            disabled={selectedCities.length ? false : true}
                                        >
                                            <option value="">Select City</option>

                                            {selectedCities.map(ct => (
                                                <option value={ct.id} key={ct.id}>
                                                    {ct.cityName}
                                                </option>
                                            ))}
                                        </Field>
                                        {errors.cityId && touched.cityId ? (
                                            <span className="errorMsg">{errors.cityId}</span>
                                        ) : null}


                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row className="show-grid">
                                <Col xs={12} md={12}>
                                    <FormGroup controlId="formBasicText">
                                        <div>Zip Code <span className="required">*</span></div>
                                        <Field
                                            name="zipCode"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                        />
                                        {errors.zipCode && touched.zipCode ? (
                                            <span className="errorMsg">{errors.zipCode}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                            </Row>

                            <Row className="show-grid">
                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formBasicText">
                                        <div>First Name</div>

                                        <Field
                                            name="firstName"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.firstName}
                                        />
                                        {errors.firstName && touched.firstName ? (
                                            <span className="errorMsg">{errors.firstName}</span>
                                        ) : null}

                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formBasicText">
                                        <div>Last Name</div>
                                        <Field
                                            name="lastName"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.lastName}
                                        />
                                        {errors.lastName && touched.lastName ? (
                                            <span className="errorMsg">{errors.lastName}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                            </Row>

                            <Row className="show-grid">
                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formBasicText">
                                        <div>Work Email</div>
                                        <Field
                                            name="emailAddress"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.emailAddress}
                                        />
                                        {errors.emailAddress && touched.emailAddress ? (
                                            <span className="errorMsg">{errors.emailAddress}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formBasicText">
                                        <div>Phone</div>
                                        
                                        <InputMask
                                            mask="1 999-999-9999"
                                            maskChar={null}
                                            type="tel"
                                            name="phoneNumber"
                                            className={`form-control ${values.phoneNumber &&
                                                    'input-elem-filled'}`}
                                            value={values.phoneNumber}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                        {errors.phoneNumber && touched.phoneNumber ? (
                                            <span className="errorMsg">{errors.phoneNumber}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                            </Row>

                            <Row className="show-grid">
                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formControlsSelect">
                                        <div>Payment Mode</div>

                                        <Field
                                            component="select"
                                            name="paymentMode"
                                            placeholder="select"
                                            className="form-control"
                                            value={values.paymentMode}
                                            disabled
                                        >
                                            <option value="">Select</option>
                                            <option value="ACH">ACH</option>
                                            <option value="WIRE">Wire Transfer</option>
                                            <option value="CHECK">Check</option>
                                        </Field>
                                        {errors.paymentMode && touched.paymentMode ? (
                                            <span className="errorMsg">{errors.paymentMode}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                            </Row>

                            <Row className="show-grid">
                                <Col xs={12} md={12}>
                                    &nbsp;
                                </Col>
                            </Row>
                            <Row>&nbsp;</Row>
                            <Row className="show-grid text-center">
                                <Col xs={12} md={12}>
                                    <Button
                                        onClick={this.handleCloseVendor}
                                        className="but-gray"
                                    >
                                        Return
                                    </Button>
                                    <Button type="submit" className="but-green  ml-3">
                                        Confirm
                                    </Button>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12}>
                                    <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                </Col>
                            </Row>
                        </Form>
                    );
                }}
            </Formik>
        );
    }
}

VendorEdit.propTypes = {
    onClick: PropTypes.func,
    onSuccess: PropTypes.func,
    countryCode: PropTypes.string,
    tooltip: PropTypes.object,
    stateCode: PropTypes.string,
    cityId: PropTypes.string,
};



export default VendorEdit;
