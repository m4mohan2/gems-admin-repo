/* eslint-disable react/prop-types */
import React, { Component, Fragment } from 'react';
import {
    Consumer
} from './../../../../../Context/Context';
import {
    Table,
    //ButtonToolbar, 
    //DropdownButton,
    Dropdown,
    Button,
    Row,
    Image,
    Modal,
    Col
} from 'react-bootstrap';
import './vendor.scss';
import axios from './../../../../../shared/eaxios';
import LoadingSpinner from './../../../../../Components/LoadingSpinner/LoadingSpinner';
import Pagination from 'react-js-pagination';
import BusinessName from './../../../../../Components/Dashboard/BusinessName';
import refreshIcon from './../../../../../assets/refreshIcon.png';
import { Link } from 'react-router-dom';
//import editSmIcon from './../../../../../assets/editSmIcon.png';
import SuccessIco from './../../../../../assets/success-ico.png';
//import crossSmIcon from './../../../../../assets/crossSmIcon.png';
//import LinkWithTooltip from './../../../../../Components/LinkWithTooltip/LinkWithTooltip';
import EditVendor from './VendorEdit';
import { FaDownload } from 'react-icons/fa';
//import ModalComponent from '../../../../../Components/Utilities/ModalComponent/ModalComponent';
import { saveAs } from 'file-saver';
import { FaCaretDown } from 'react-icons/fa';
import { FaCaretUp } from 'react-icons/fa';
import PropTypes from 'prop-types';
import { handelBusinessIDName } from '../../../../../redux/actions/business';
import { connect } from 'react-redux';
import { FiMenu } from 'react-icons/fi';
import DatePicker from 'react-date-picker';
//import { MdLaunch } from 'react-icons/md';


class Vendor extends Component {

    state = {
        vendorLists: [],
        editVendor: [],
        showVendor: false,
        successMessage: null,
        activePage: 1,
        totalCount: 0,
        itemPerPage: 20,
        showConfirMmsg: false,
        errorMessge: null,
        errMsg: null,
        showDeleteMmsg: false,
        showDeleteConfirm: false,
        loading: true,
        selectedBusinessName: '',
        vendorFilter:'',
        sortingActiveID:1,
        downloadLink: '',
        fileLoader: false,
        errorFileDownloading: '',
        fromDate: '',
        fromDateUI: '',
        toDate: '',
        toDateUI: '',
        modalType: '',
        sixPrevMonthDate: '',
        dateError: '',
        modaltype: '',
        invDetailsProps: {},
        direction: '',
        propName: '',
        propOrder: true,
        showSuccessModal:false,
        sort: null,
        field: null,

    };

    _isMounted = false;

    displayError = (e) => {
        console.log('displayError', e.data.message);
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        this.setState({
            errorMessge: errorMessge,
            errMsg: errorMessge
        });

        setTimeout(() => {
            this.setState({ errorMessge: null });
        }, 5000);
    }



    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber });
        this.fetchVendorList(pageNumber > 0 ? pageNumber - 1 : 0, this.state.selectedBusinessName);
    };
    handleCloseVendor = () => {
        this.setState({ showVendor: false });
        // this.fetchVendorList();
        let since = 0;
        this.fetchVendorList(since, this.state.searchInput);
    };
    editVendor = venId => {
        axios
            .get(`/customerVendor/vendor/${venId}`)
            .then(vendor => {
                if (vendor.data) {
                    const details = vendor.data;
                    console.log('vendor edit details get', details);
                    this.setState({
                        showVendor: true,
                        editVendor: details
                    });
                }
                this.resetSearch();
            })
            .catch(this.displayError);
    };
   

    componentDidMount() {
        this._isMounted = true;
        this.handelInitialDate();

        // if (this.props.globalState.business.businessID) {
        //     {
        //         this.props.globalState.business != null ? (this.setState({ selectedBusinessName: this.props.globalState.business.businessID },
        //             () => {
        //                 let since = 0;
        //                 this.fetchVendorList(since, this.state.selectedBusinessName, this.state.vendorFilter);

        //             })) : null;
        //     }
        // }

        if (this.props.globalState.business.businessID != null) {
            console.log('got props', this.props.globalState.business.businessID);
            this.setState({ selectedBusinessName: this.props.globalState.business.businessID },
                () => {
                    console.log('*****this.state.selectedBusinessName*********', this.state.selectedBusinessName);
                    let since = 0;
                    this.fetchVendorList(since, this.state.selectedBusinessName, this.state.vendorFilter);
                   
                });
        }else{
            this.fetchVendorList();
        }
        
        

    }


    componentWillUnmount() {
        this.props.onClickAction('');
        this._isMounted = false;

    }


    fetchVendorList = (
        since = 0, // Pagination strating from --1
        searchKey = '', // search by Business ID --2 ******
        filter = '', //  serch by keyword like : name, ph --3
        sort = '', // sort with a = true, d =flase --4
        field = '' // sort with field name --5
    ) => {
        this.setState({
            loading: true,

            sort: sort,
            field: field
        }, () => {

            axios
                .get(
                    
                    // `customerVendor/vendorlist?since=${since}&limit=${this.state.itemPerPage}&businessId=${searchKey}&key=${filter}&direction=${this.state.sort}&prop=${this.state.field}`
                    `customerVendor/vendorlist?since=${since}&limit=${this.state.itemPerPage}&businessId=${searchKey}&key=${filter}&direction=${this.state.sort}&prop=${this.state.field}`
                )
                .then(res => {
                    console.log('res.data-----------------', res.data);
                    const vendorLists = res.data.entries;
                    const totalCount = res.data.total;
                    if (this._isMounted) {
                        this.setState({
                            vendorLists: vendorLists,
                            totalCount: totalCount,
                            loading: false
                        });
                    }

                    console.log('vendor list-----------------------', this.state.vendorLists);
                })
                .catch(this.displayError);

        });

    };

    

    handleChange = (e) => {
        this.setState({ selectedBusinessName: e.target.value }, () => { console.log('this.state.value', this.state.selectedBusinessName); });
    }

    filterMethodChange = (e) => {
        let since = 0;
        console.log('selected filter method', e.target.value);

        this.setState({
            vendorFilter: e.target.value
        },()=>{
            this.fetchVendorList(since, this.state.selectedBusinessName, this.state.vendorFilter);
        });

    }

    handelSearch = () => {
        let since = 0;
        this.fetchVendorList(since, this.state.selectedBusinessName, this.state.vendorFilter);
    }

    resetSearch = () => {
        this.setState({ 
            //searchInput: '',
            selectedBusinessName: '',
            vendorFilter: ''
        },()=>{
            let since = 0;
            this.fetchVendorList(since, this.state.selectedBusinessName, this.state.vendorFilter);
        });
        
    }

    successMessgae = () => {
        this.setState({
            showConfirMmsg: true,
            showVendor: false
        });
        this.fetchVendorList();
        // var since = 0;
        // this.fetchVendorList(since, this.state.searchInput);
    };

    handleConfirmReviewClose = () => {
        this.setState({
            showConfirMmsg: false
        });
    };

    handlerWarningDeleteClose = () =>{
        this.setState({ showDeleteMmsg: false });
    }

    handlerWarningDelete = (venId) => {
        console.log('user', venId);
        this.setState({
            showDeleteMmsg: true,
            deleteConfirm: venId
        });

    };

    handleDeleteSuccessOpen = () => {
        this.setState({
            showDeleteConfirm: true
        });
    }
    handleDeleteSuccessClose = () => {
        this.setState({
            showDeleteConfirm: false
        });
    }

    downloadVendors=(e)=>{
        let d = new Date();
        let n = (d.getTime()) / 1000;
        let t = Math.floor(n);
        console.log('e',e);
        // this.setState({
        //     loading: true
        // }, () => {

            

        axios
            .get(
                //'/customerVendor/vendorList/download'
                `/customerVendor/vendorList/download?businessId=${this.state.selectedBusinessName}&key=${this.state.vendorFilter}&direction&prop`
                //, {responseType : 'blob'}
                    
            )
        // .then(res => {
        //     console.log('download response-----------------', res);
        //     console.log('res.data-----------------', res.data);
        //     //window.location.href = res.data;
                    
        // })
            .then(response => {
                console.log('download response-----------------', response.data);
                let blob = new window.Blob([response.data], { type: 'text/plain;charset=utf-8' });
                saveAs(blob, `vendor-list-${t}.csv`);
                /*const filename = response.headers.get('Content-Disposition').split('filename=')[1];
                    response.blob().then(blob => {
                        let url = window.URL.createObjectURL(blob);
                        let a = document.createElement('a');
                        console.log(a , url);
                        a.href = url;
                        a.download = filename;
                        a.click();
                    });*/
            })
            .catch(this.displayError);
            




        // });
    }

    deleteVendor = venId => {
        console.log(venId);
        axios
            .delete(`/customerVendor/delete/vendor/${venId}`)
            .then(() => {
                // console.log('delete vendor id', venId);
                this.fetchVendorList();
                if (this._isMounted){
                    this.setState({
                        showStatus: true,
                        successMessage: 'Vendor has been deleted successfully',
                        showDeleteMmsg: false,
                        showDeleteConfirm: true
                    });
                    setTimeout(() => {
                        this.setState({ successMessage: null });
                    }, 2500);
                }
                
            })
            .catch(this.displayError);
    };

    tableSorting = (boolan, type)=>{
        console.log('working sorting', boolan, type);
    }

    sortingActive=(id)=>{
        this.setState({
            sortingActiveID:id
        }, () => { console.log('this.state.sortingActiveID', this.state.sortingActiveID);});
    }

    handleSuccessShow = (type) => {
        console.log(type);
        this.setState({ modalType: type, showSuccessModal: true });
    };

    handleSuccessClose = () => {
        this.setState({ showSuccessModal: false });
        this.handelInitialDate();
    };

    handelInitialDate = () => {
        let currentDate = new Date();
        let prevMonthDate = new Date(currentDate.setDate(currentDate.getDate() - 30));

        let cDate = new Date();
        let sixPrevMonthDate = new Date(cDate.setDate(cDate.getDate() - 180));

        this.setState({
            fromDateUI: prevMonthDate,
            sixPrevMonthDate: sixPrevMonthDate
        });
        this.handleChangeDate(prevMonthDate, 'from');
        this.handleChangeDate(new Date(), 'to');
    }


    handleChangeDate = (date, type) => {
        if (date != null) {
            let day = date.getDate();
            let month = date.getMonth() + +1;
            let year = date.getFullYear();
            let fullDate = day + '/' + month + '/' + year;
            if (type === 'from') {
                this.setState({
                    fromDateUI: date,
                    fromDate: fullDate
                });
            }
            else {
                this.setState({
                    toDateUI: date,
                    toDate: fullDate
                });
            }
        }
        else {
            if (type === 'from') {
                this.setState({
                    fromDateUI: '',
                    fromDate: ''
                });
            }
            else {
                this.setState({
                    toDateUI: '',
                    toDate: ''
                });
            }
        }
    }

    getFundingDebitCSV =()=>{
        let d = new Date();
        let n = (d.getTime()) / 1000;
        let t = Math.floor(n);

        if (this.state.fromDate != '' && this.state.fromDateUI != '' && this.state.toDate != '' && this.state.toDateUI != '') {
            this.setState({ fileLoader: true }, () => {
                axios.get(`/transaction/fundingTransaction/download?fromDate=${this.state.fromDate}&toDate=${this.state.toDate}/businessId=${this.state.selectedBusinessName}&key=${this.state.vendorFilter}`)
                    .then(response => {

                        this.setState({
                            fileLoader: false
                        });
                        
                        let blob = new window.Blob([response.data], { type: 'text/plain;charset=utf-8' });
                        saveAs(blob,  `vendor-list-${t}.csv`);
                        this.handleSuccessShow('success');
                    })
                    .catch(err => {
                        let errorMessage = this.handelError(err);
                        this.setState({
                            fileLoader: false, errorFileDownloading: errorMessage
                        });
                        setTimeout(() => {
                            this.setState({ errorFileDownloading: null });
                        }, 5000);
                    });
            });
        }
        else {
            this.setState({
                dateError: 'Please enter valid date.'
            });
            setTimeout(() => {
                this.setState({
                    dateError: ''
                });
            }, 2500);

        }
        
         
    }

    render() {
        console.log( 'Props ', this.props );
        // console.log('props from vendors*************************', this.props.globalState.business);
        console.log('*****this.state.selectedBusinessName render*********', this.state.selectedBusinessName);
        
        return (
            <div>
                {this.state.errorMessge
                    ?
                    <div className="alert alert-danger col-12" role="alert">
                        ERROR : {this.state.errorMessge}
                    </div>
                    :
                    null
                }
                <div className="dashboardInner vendorOuter">
                    <div className="topSearch transactionSearch mb-4">
                        {/* <h4 className="mb-3">Search Vendors by Business Name </h4> */}
                        <div className="row">
                            <div className="col-sm-6">
                                <form className="form-inline">
                                    <select
                                        id={this.state.selectedBusinessName}
                                        className="form-control col-6 truncate pr-35" 
                                        onChange={this.handleChange} 
                                        value={this.state.selectedBusinessName}>
                                        <option value=''> Select a Business Name </option>
                                        <Consumer>
                                            {(value) => <BusinessName businessName={value.businessdata[0]} />}
                                        </Consumer>
                                    </select>

                                    <div className="form-group col-4">
                                        <button
                                            type="button"
                                            className="btn search-btn text-black"
                                            onClick={() => this.handelSearch()}
                                        >
                                            Search
                                        </button>
                                        <div className="form-group ml-2">
                                            <Link to="#">
                                                <Image src={refreshIcon} onClick={() => this.resetSearch()} />
                                            </Link>
                                        </div>
                                    </div>

                                </form>

                            </div>
                            <div className="col-sm-6">
                                {/* <Button type="button" className="btn btn-dark float-right ml-3" onClick={() => this.handleSuccessShow('download')}><FaDownload /> Download</Button> */}
                                <button type="button" className="btn btn-dark float-right ml-3" onClick={()=>this.downloadVendors('hi')}><FaDownload /> Download</button>
                                <form className="form-inline float-right">
                                    <select className="form-control truncate pr-35" onChange={this.filterMethodChange} value={this.state.vendorFilter}>
                                        <option value=''> Select a filter </option>
                                        <option value='ach'> Vendors with ACH </option>
                                        <option value='check'> Vendors with Check </option>
                                        <option value='noPayment'> Vendor without Payment Method </option>
                                        <option value='noEmail'>Vendor without email </option>
                                        <option value='noPhone'> Vendor without Phone Number </option>
                                    </select>
                                </form>
                                
                                
                            </div>

                        </div>

                    </div>{/*topSearch*/}



                    <div className="boxBg border p-t-15 p-b-15">
                        <Table responsive hover>
                            <thead className="theaderBg">
                                <tr>
                                    <th></th>
                                    <th>
                                        <span className="float-left pr-2">Business Name </span>
                                        <span className="d-flex flex-column-reverse sortingFontSize">
                                            <button className="custom-btn-focus sortingArrow">
                                                <FaCaretDown 
                                                    className={'cursorPointer ' + (this.state.sortingActiveID==2 ? 'activeColor' : '')} 
                                                    onClick={() => { 
                                                        this.fetchVendorList(0, '', '', false, 'business');
                                                        this.sortingActive(2);
                                                    }} /> 
                                            </button>
                                            <button className="custom-btn-focus sortingArrow">
                                                <FaCaretUp 
                                                    className={'cursorPointer ' + (this.state.sortingActiveID == 1 ? 'activeColor' : '')} 
                                                    onClick={() => { 
                                                        this.fetchVendorList(0, '', '', true, 'business');
                                                        this.sortingActive(1); 
                                                    }}/>
                                            </button>
                                        </span>
                                    </th>
                                    <th>
                                        <span className="float-left pr-2">
                                            Vendor Name 
                                        </span>
                                        <span className="d-flex flex-column-reverse sortingFontSize">
                                            <button className="custom-btn-focus sortingArrow">
                                                <FaCaretDown 
                                                    className={'cursorPointer ' + (this.state.sortingActiveID == 4 ? 'activeColor' :'')} 
                                                    onClick={() => { 
                                                        this.fetchVendorList(0, '', '', false, 'vendor');
                                                        this.sortingActive(4);
                                                    }} />
                                            </button>
                                            <button className="custom-btn-focus sortingArrow">
                                                <FaCaretUp 
                                                    className={'cursorPointer ' + (this.state.sortingActiveID == 3 ? 'activeColor' : '')}
                                                    onClick={() => { 
                                                        this.fetchVendorList(0, '', '', true, 'vendor');
                                                        this.sortingActive(3); 

                                                    }} />
                                            </button> 
                                        </span>
                                    </th>
                                    <th>
                                        <span className="float-left pr-2">
                                            Email Address
                                        </span>
                                        <span className="d-flex flex-column-reverse sortingFontSize">
                                            <button className="custom-btn-focus sortingArrow">
                                                <FaCaretDown
                                                    className={'cursorPointer ' + (this.state.sortingActiveID == 6 ? 'activeColor' : '')} 
                                                    onClick={() => { 
                                                        this.fetchVendorList(0, '', '', false, 'email');
                                                        this.sortingActive(6);
                                                    }} /> 
                                            </button>
                                            <button className="custom-btn-focus sortingArrow">
                                                <FaCaretUp 
                                                    className={'cursorPointer ' + (this.state.sortingActiveID == 5 ? 'activeColor' : '')} 
                                                    onClick={() => { 
                                                        this.fetchVendorList(0, '', '', true, 'email');
                                                        this.sortingActive(5); 
                                                    }} />
                                            </button>
                                        </span>
                                    </th>
                                    <th>
                                        <span className="float-left pr-2">
                                            Phone Number
                                        </span>
                                        {/* <span className="d-flex flex-column-reverse sortingFontSize">
                                            <button className="custom-btn-focus sortingArrow">
                                                <FaCaretDown
                                                    className={'cursorPointer ' + (this.state.sortingActiveID == 6 ? 'activeColor' : '')}
                                                    onClick={() => {
                                                        this.fetchVendorList(0, '', '', false, 'email');
                                                        this.sortingActive(6);
                                                    }} />
                                            </button>
                                            <button className="custom-btn-focus sortingArrow">
                                                <FaCaretUp
                                                    className={'cursorPointer ' + (this.state.sortingActiveID == 5 ? 'activeColor' : '')}
                                                    onClick={() => {
                                                        this.fetchVendorList(0, '', '', true, 'email');
                                                        this.sortingActive(5);
                                                    }} />
                                            </button>
                                        </span> */}
                                    </th>
                                    <th>
                                        <span className="float-left pr-2">
                                           Payment Method 
                                        </span>
                                        <span className="d-flex flex-column-reverse sortingFontSize">
                                            <button className="custom-btn-focus sortingArrow">
                                                <FaCaretDown 
                                                    className={'cursorPointer ' + (this.state.sortingActiveID == 8 ? 'activeColor' : '')} 
                                                    onClick={() => { 
                                                        this.fetchVendorList(0, '', '', false, 'payment');
                                                        this.sortingActive(8); 
                                                    }} /> 
                                            </button>
                                            <button className="custom-btn-focus sortingArrow">
                                                <FaCaretUp 
                                                    className={'cursorPointer ' + (this.state.sortingActiveID == 7 ? 'activeColor' : '')} 
                                                    onClick={() => { 
                                                        this.fetchVendorList(0, '', '', true, 'payment');
                                                        this.sortingActive(7);  
                                                    }} />
                                            </button>
                                        </span>
                                    </th>    
                                    <th>
                                        <span className="float-left pr-2">
                                            Date Added 
                                        </span>
                                        <span className="d-flex flex-column-reverse sortingFontSize">
                                            <button className="custom-btn-focus sortingArrow">
                                                <FaCaretDown 
                                                    className={'cursorPointer ' + (this.state.sortingActiveID == 10 ? 'activeColor' : '')} 
                                                    onClick={() => { 
                                                        this.fetchVendorList(0, '', '', false, 'createdDate');
                                                        this.sortingActive(10);  
                                                    }} /> 
                                            </button>
                                            <button className="custom-btn-focus sortingArrow">
                                                <FaCaretUp 
                                                    className={'cursorPointer ' + (this.state.sortingActiveID == 9 ? 'activeColor' : '')} 
                                                    onClick={() => { 
                                                        this.fetchVendorList(0, '', '', true, 'createdDate');
                                                        this.sortingActive(9);  
                                                    }} />
                                            </button>
                                        </span>
                                    </th>
                                    {/* <th>
                                    </th> */}
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.loading ? (<tr>
                                    <td colSpan={12}>
                                        <LoadingSpinner />
                                    </td>
                                </tr>) :
                                    this.state.vendorLists.length > 0 ? (
                                        this.state.vendorLists.map(vendorList => (

                                            (vendorList.archive === false ?
                                                (<tr key={vendorList.id}>
                                                    <td>
                                                        <Dropdown>

                                                            <Dropdown.Toggle className="customActionButton">
                                                                <FiMenu />
                                                            </Dropdown.Toggle>

                                                            <Dropdown.Menu>
                                                                <Dropdown.Item onClick={() => this.editVendor(vendorList.id)}>
                                                                    Edit
                                                                </Dropdown.Item>
                                                                <Dropdown.Item onClick={() => this.handlerWarningDelete(vendorList.id)}>
                                                                    Delete
                                                                </Dropdown.Item>
                                                            </Dropdown.Menu>

                                                        </Dropdown>
                                                    </td>
                                                    <td>{vendorList.businessName}</td>
                                                    <td>{vendorList.companyName}</td>
                                                    <td>{vendorList.emailAddress}</td>
                                                    <td>{vendorList.phoneNumber}</td>
                                                    <td>{vendorList.paymentMode}</td>
                                                    <td>{vendorList.createdDate}</td>
                                                    {/* <td><MdLaunch className="cursor-pointer" /></td> */}
                                                    {/* <td>
                                                        <div className="showHideItem">
                                                            <div className="d-flex justify-content-end">
                                                                <LinkWithTooltip
                                                                    tooltip="edit vendor"
                                                                    id="vendor-edit"
                                                                >
                                                                    <div className="smIconBg"
                                                                        onClick={() => this.editVendor(vendorList.id)}>
                                                                        <Image src={editSmIcon} />
                                                                    </div>
                                                                </LinkWithTooltip>
                                                                <LinkWithTooltip
                                                                    tooltip="delete vendor"
                                                                    id="vendor-del"
                                                                >
                                                                    <div className="smIconBg ml-1"
                                                                        onClick={() => this.handlerWarningDelete(vendorList.id)}>
                                                                        <Image src={crossSmIcon} />
                                                                    </div>

                                                                </LinkWithTooltip>

                                                            </div>
                                                        </div>
                                                    </td> */}
                                                </tr>) : null)


                                        ))
                                    )
                                        :
                                        this.state.errorMessge ? null : (
                                            <tr>
                                                <td colSpan={12}>
                                                    <p className="text-center">No records found</p>
                                                </td>
                                            </tr>
                                        )
                                }
                            </tbody>
                        </Table>
                    </div>
                    {this.state.totalCount ? (
                        <Row>
                            <Col md={12}>
                                <div className="paginationOuter text-right">
                                    <Pagination
                                        activePage={this.state.activePage}
                                        itemsCountPerPage={this.state.itemPerPage}
                                        totalItemsCount={this.state.totalCount}
                                        onChange={this.handlePageChange}
                                    />
                                </div>
                            </Col>
                        </Row>
                    ) : null}

                </div>

                {/*==================EDIT CUSTOMER DETAILS======================*/}
                <Modal
                    show={this.state.showVendor}
                    onHide={this.handleCloseVendor}
                    className="enterPayInfoPop"
                >
                    <Modal.Header >
                        <Modal.Title>Edit Vendor</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <EditVendor
                            {...this.state.editVendor}
                            onSuccess={this.successMessgae}
                            onClick={this.handleCloseVendor}
                        />
                    </Modal.Body>
                </Modal>

                {/*=======DELETE VENDOR APPROVE POPUP======== */}
                <Modal
                    show={this.state.showDeleteMmsg}
                    onHide={this.handlerWarningDeleteClose}
                    className="payOptionPop"
                >
                    <Modal.Body>

                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Warning!! Vendor will be permanently deleted and cannot be recovered. Are you sure ? </h5>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handlerWarningDeleteClose}
                            className="but-gray"
                        >
                            Return
                        </Button>
                        <Button
                            onClick={() => this.deleteVendor(this.state.deleteConfirm)}
                            className="btn-danger"
                        >
                            Proceed
                        </Button>
                    </Modal.Footer>
                </Modal>

                {/* ==== ussing ModalComponent service for  test ==== */}
                {/* <ModalComponent 
                    show={this.state.showDeleteMmsg}
                    onHide={this.handlerWarningDeleteClose}
                /> */}

                {/*====DELETE VENDOR SUCCESS POPUP===== */}
                <Modal
                    show={this.state.showDeleteConfirm}
                    onHide={this.handleDeleteSuccessClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Vendor has been successfully deleted</h5>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handleDeleteSuccessClose}
                            className="but-gray"
                        >
                            Return
                        </Button>
                    </Modal.Footer>
                </Modal>

                {/*======  confirmation popup  ===== */}
                <Modal
                    show={this.state.showConfirMmsg}
                    onHide={this.handleConfirmReviewClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Vendor has been successfully updated</h5>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handleConfirmReviewClose}
                            className="but-gray"
                        >
                            Return
                        </Button>
                    </Modal.Footer>
                </Modal>
                {/* ========== Download Modal ============== */} 

                <Modal
                    show={this.state.showSuccessModal}
                    onHide={this.handleSuccessClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        {this.state.modalType === 'success' ?
                            <Fragment>
                                <Row>
                                    <Col md="12" className="text-center">
                                        <Image src={SuccessIco} />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md="12" className="text-center">
                                        <h5>
                                            Successfully downloaded.
                                        </h5>
                                    </Col>
                                </Row>
                            </Fragment>
                            :
                            <Fragment>
                                <Row className="text-center">
                                    <h3 className='pl-3'>
                                        Please select period
                                    </h3>
                                </Row>
                                <br />
                                <Row>
                                    <Col sm={12} md={12}>
                                        {
                                            this.state.fileLoader ?
                                                <LoadingSpinner />
                                                :
                                                this.state.errorFileDownloading ?
                                                    <div className="alert alert-danger">
                                                        {this.state.errorFileDownloading}
                                                    </div>
                                                    : null
                                        }
                                    </Col>
                                </Row>
                                {
                                    this.state.dateError != '' ?
                                        <Row>
                                            <Col sm={12} md={12} className="alert alert-danger">
                                                {this.state.dateError}
                                            </Col>
                                        </Row>
                                        : null
                                }
                                <Row>
                                    <Col md="6" sm="12">
                                        <h5>From date</h5>
                                        <DatePicker
                                            selected={this.state.fromDateUI}
                                            value={this.state.fromDateUI}
                                            onChange={(value) => this.handleChangeDate(value, 'from')}
                                            dateFormat="YYYY-MM-DD"
                                            minDate={this.state.sixPrevMonthDate}
                                            maxDate={new Date()}
                                            className={'cusdatepickerHandel'}
                                        />
                                    </Col>
                                    <Col md="6" sm="12">
                                        <h5>To date</h5>
                                        <DatePicker
                                            selected={this.state.toDateUI}
                                            value={this.state.toDateUI}
                                            onChange={(value) => this.handleChangeDate(value, 'to')}
                                            dateFormat='YYYY/MM/DD'
                                            minDate={this.state.sixPrevMonthDate}
                                            maxDate={new Date()}
                                            className={'cusdatepickerHandel'}
                                        />
                                    </Col>
                                </Row>
                            </Fragment>
                        }
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handleSuccessClose}
                            className="but-gray btn-darkBlue"
                        >
                            Return
                        </Button>
                        {this.state.modalType != 'success' ?
                            <Button
                                onClick={this.getFundingDebitCSV}
                                className="blue-btn "
                            >
                                Download
                            </Button>
                            : null}
                    </Modal.Footer>
                </Modal>

                {/* Download success modal */}

                <Modal
                    show={this.state.showSuccessModal}
                    onHide={this.handleSuccessClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        {this.state.modalType === 'success' ?
                            <Fragment>
                                <Row>
                                    <Col md="12" className="text-center">
                                        <Image src={SuccessIco} />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md="12" className="text-center">
                                        <h5>
                                            Successfully downloaded.
                                        </h5>
                                    </Col>
                                </Row>
                            </Fragment>
                            :
                            <Fragment>
                                <Row className="text-center">
                                    <h3 className='pl-3'>
                                        Please select period
                                    </h3>
                                </Row>
                                <br />
                                <Row>
                                    <Col sm={12} md={12}>
                                        {
                                            this.state.fileLoader ?
                                                <p style={{ textAlign: 'center' }}><LoadingSpinner /></p>
                                                :
                                                this.state.errorFileDownloading ?
                                                    <div className="alert alert-danger">
                                                        {this.state.errorFileDownloading}
                                                    </div>
                                                    : null
                                        }
                                    </Col>
                                </Row>
                                {
                                    this.state.dateError != '' ?
                                        <Row>
                                            <Col sm={12} md={12} className="alert alert-danger">
                                                {this.state.dateError}
                                            </Col>
                                        </Row>
                                        : null
                                }
                                <Row>
                                    <Col md="6" sm="12">
                                        <h5>From date</h5>
                                        <DatePicker
                                            selected={this.state.fromDateUI}
                                            value={this.state.fromDateUI}
                                            onChange={(value) => this.handleChangeDate(value, 'from')}
                                            dateFormat="YYYY-MM-DD"
                                            minDate={this.state.sixPrevMonthDate}
                                            maxDate={new Date()}
                                        />
                                    </Col>
                                    <Col md="6" sm="12">
                                        <h5>To date</h5>
                                        <DatePicker
                                            selected={this.state.toDateUI}
                                            value={this.state.toDateUI}
                                            onChange={(value) => this.handleChangeDate(value, 'to')}
                                            dateFormat='YYYY/MM/DD'
                                            minDate={this.state.sixPrevMonthDate}
                                            maxDate={new Date()}
                                        />
                                    </Col>
                                </Row>
                            </Fragment>
                        }
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handleSuccessClose}
                            className="but-gray"
                        >
                            Return
                        </Button>
                        {this.state.modalType != 'success' ?
                            <Button
                                onClick={this.getFundingDebitCSV}
                                className="blue-btn "
                            >
                                Download
                            </Button>
                            : null}
                    </Modal.Footer>
                </Modal>

            </div>

        );
    }
}

const mapStateToProps = state => {
    return {
        globalState: state,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        onClickAction: data => dispatch(handelBusinessIDName(data))
    };
};




Vendor.propTypes = {
    globState: PropTypes.object,
    onClickAction: PropTypes.func,    
};

export default connect(mapStateToProps, mapDispatchToProps)(Vendor);
