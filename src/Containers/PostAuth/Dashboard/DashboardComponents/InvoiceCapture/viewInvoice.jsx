/* eslint-disable no-unused-vars */
import {
    Field,
    Form,
    Formik,
    FieldArray
} from 'formik';
import React, {
    Component,
    Fragment
} from 'react';
import {
    Button,
    Col,
    Collapse,
    FormControl,
    FormGroup,
    Image,
    Row,
    Tab,
    Table,
    Tabs,
    Modal,
    //ButtonToolbar
} from 'react-bootstrap';
//import { Link } from 'react-router-dom';
import * as Yup from 'yup';
import InvSample from './../../../../../assets/invoice-sample.png';
import { FaFilePdf } from 'react-icons/fa';
import './VerifyInvoice.scss';
import * as Fields from './InvoiceFields';
import axios from './../../../../../shared/eaxios';
import LoadingSpinner from './../../../../../Components/LoadingSpinner/LoadingSpinner';
import PropTypes from 'prop-types';
import SuccessIco from './../../../../../assets/success-ico.png';
import pdfIcon from './../../../../../assets/pdficon.png';
import { IoMdDownload } from 'react-icons/io';



const initialerReport = {
    [Fields.INVOICE_NUMBER]: false,
    [Fields.INVOICE_DATE]: false,
    [Fields.INVOICE_DUE_DATE]: false,
    [Fields.INVOICE_PO]: false,
    [Fields.TERM_ID]: false,
    [Fields.CUSTOMER_NAME]: false,
    [Fields.CUSTOMER_ADDRESS]: false,
    [Fields.CUSTOMER_PHONE_NO]: false,
    [Fields.VENDOR_NAME]: false,
    // [Fields.VENDOR_ADDRESS]: false,
    // [Fields.VENDOR_PHONE_NO]: false,
    [Fields.INVOICE_ITEMS]: false,
    [Fields.SUBTOTAL]: false,
    [Fields.TOTAL_TAX]: false,
    [Fields.AMOUNT_DUE]: false,
    [Fields.CURRENCY]: false,
    [Fields.COMMENT]: '',
    invoiceItems: [],


};

const initialValue = {
    invoiceItems: [],
    comment2: ''
};




class ViewInvoice extends Component {
    state = {
        invoiceDetails: true,
        customerVendor: true,
        lineItems: true,
        invDetails: true,
        invAdditionalNote: true,
        errorEnable: false,
        detailsLoader: false,
        detailsError: null,
        detailsList: {},
        errorReportLoader: false,
        alertMsg: false,
        errorSubmitLoader: false,
        errorReportError: null,
        errorObj: {},
        showConfirMmsg: false,
        verifyError: null,
        verifyLoader: false,
        submitStatus: null,
        confirmState: null,
        auditTrails: [],
        taxType: null

    }

    downloadurl(url) {
        setTimeout(() => {
            window.open(
                url,
                '_blank'
            );
        }, 100);
    }


    handleConfirmReviewClose = () => {
        this.setState({
            showConfirMmsg: false,
        }, () => {
            this.props.modalCloseReviewInvoice();
            this.props.getAllList();
        });
    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Unknown error!';
        }
        return errorMessge;
    }


    closeAlertMsg = () => {
        this.setState({
            alertMsg: false,
        });
    }
    openAlertMsg = () => {
        this.setState({
            alertMsg: true,
        });
    }
    cancelError = () => {
        this.setState({
            errorEnable: false,
        });
    }

    openErrorSection = () => {
        this.setState({
            errorEnable: true
        });
    }

    handleErrorReport = (values) => {
        let temp = {};
        temp = values;
        console.log('errorObj::::::::::::::::::', values);
        //temp.isActive = true;
        let tempDetails = this.state.detailsList;
        tempDetails['error']['errorJson'] = JSON.stringify(temp);
        tempDetails['error']['isActive'] = true;
        console.log('tempDetails||||||||||||||||||||||', tempDetails);
        this.setState({
            errorObj: tempDetails,
            submitStatus: 'error',
        }, () => this.openAlertMsg());


    }

    handleVerify = () => {

        this.setState({
            submitStatus: 'verify',
        }, () => this.openAlertMsg());

    }



    componentDidMount() {
        console.log('I am verify did mount', this.props);
        this.getDetailsList();
    }

    componentDidUpdate() {

    }
    componentWillUnmount() {
        console.log('I am verify componentWillUnmount');
    }

    getDetailsList = () => {
        this.setState({
            detailsLoader: true
        });


        axios
            .get(
                `invoice/${this.props.selectedID}/${this.props.selectedBusinessID}`
            )
            .then(res => {
                initialerReport['invoiceItems'] = res.data.invoiceItems;
                { initialerReport['invoiceItems'][0]['isTaxable'] ? this.setState({ taxType: true }) : this.setState({ taxType: false }); }
                const detailsList = res.data;
                detailsList['termId'] = detailsList.termRequest && detailsList.termRequest.id ? detailsList.termRequest.termName : '';
                this.setState({
                    detailsLoader: false,
                    detailsList,
                    auditTrails: res.data.auditTrails

                }, () => console.log('detailsList: res.data', res.data));
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    detailsError: errorMsg,
                    detailsLoader: false

                });

                // setTimeout(() => {
                //     this.setState({ detailsError: null });
                // }, 5000);

            });
    }

    handleVerifyConfirm = () => {
        this.setState({
            verifyLoader: true
        });




        axios
            .put(`invoice/capture/verify/${this.state.detailsList.id}/${this.props.selectedBusinessID}`, this.state.detailsList)
            .then(res => {
                console.log(res);
                this.setState({
                    verifyLoader: false,
                    showConfirMmsg: true,
                    alertMsg: false,
                    confirmState: 'verify'
                }, () => {
                    this.props.getAllList();
                });
            })
            .catch(e => {

                let errorMsg = this.displayError(e);
                this.setState({
                    verifyError: errorMsg,
                    verifyLoader: false

                }, () => {
                });

                setTimeout(() => {
                    this.setState({ verifyError: null });
                }, 5000);

            });
    }




    HandleConfirmReport = () => {
        this.setState({
            errorSubmitLoader: true,
        });


        axios
            .put(`invoice/capture/void/${this.state.detailsList.id}/${this.props.selectedBusinessID}`, this.state.errorObj)
            .then(res => {
                console.log(res);
                this.setState({
                    errorSubmitLoader: false,
                    showConfirMmsg: true,
                    alertMsg: false,
                    confirmState: 'error'
                }, () => {
                    this.props.getAllList();
                });
            })
            .catch(e => {

                let errorMsg = this.displayError(e);
                this.setState({
                    errorReportError: errorMsg,
                    errorSubmitLoader: false,


                }, () => {
                });

                setTimeout(() => {
                    this.setState({
                        errorReportError: null, alertMsg: false,
                    });
                }, 5000);

            });
    }



    handleSubmit = (values) => {
        let detailsList = this.state.detailsList;
        detailsList.invoiceState = 'UPLOADED';
        detailsList.comment = values.comment2;
        this.setState({
            detailsList
        }, () => {
            this.handleVerify();
        });

    }


    render() {
        console.log('err enable', this.state.errorEnable);
        const {
            invoiceDetails,
            customerVendor,
            lineItems,
            invDetails,
            invAdditionalNote,
            detailsList,
            detailsLoader,
            detailsError,
            errorReportError,
            errorEnable,
            verifyError,
            submitStatus,
            verifyLoader,
            errorSubmitLoader,
            auditTrails,
            taxType
        } = this.state;

        const { emailRequest, s3FileLocation, fileName, } = this.props;

        return (
            <div className="verifyInvoiceOuter invoicMasterWrap">
                <div className="invoiceMasterHeader">
                    <Row>
                        <Col md={3}>
                            <h1>Invoice</h1>
                        </Col>
                    </Row>
                </div>

                <div className="invoiceMasterBody content-body">

                    {detailsLoader ?
                        <LoadingSpinner />
                        : detailsError
                            ?
                            <div>{detailsError}</div>
                            :
                            <Formik
                                initialValues={errorEnable ? initialerReport : initialValue}
                                //validationSchema={errorEnable ? reportInvoiceSchema : submitInvoiceSchema}
                                onSubmit={errorEnable ? this.handleErrorReport : this.handleSubmit}
                                enableReinitialize
                            >
                                {({
                                    values,
                                    //errors,
                                    //touched,
                                    setFieldValue
                                }) => {
                                    return (
                                        <Form>
                                            <Row>
                                                <Col md={7}>
                                                    <div className="content-block leftContentBlock">
                                                        <Row>
                                                            <Col md={12}>
                                                                {this.props && this.props.s3FileLocation ? (
                                                                    <iframe
                                                                        src={this.props.s3FileLocation + '#toolbar=0&navpanes=0&statusbar=0&view=Fit;readonly=true; disableprint=true;'}
                                                                        width="100%"
                                                                        height="500"
                                                                        frameBorder="0"
                                                                        title="Invoice"
                                                                        allowFullScreen
                                                                    ></iframe>
                                                                ) : (
                                                                    <div className="invNotFound">
                                                                        <h5>No invoice found.</h5>
                                                                    </div>
                                                                )
                                                                }
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col md={12}>
                                                                <div className="invInfoTable">
                                                                    <div className="table-responsive">
                                                                        {!errorEnable && (
                                                                            <Table>
                                                                                <thead>
                                                                                    <tr>

                                                                                        <th width="150">
                                                                                            Name
                                                                                        </th>
                                                                                        <th width="40%">
                                                                                            Description
                                                                                        </th>
                                                                                        <th>
                                                                                            Quantity
                                                                                        </th>
                                                                                        <th>
                                                                                            Unit price
                                                                                        </th>
                                                                                        {
                                                                                            !emailRequest.isAccountingToolEnable && taxType &&
                                                                                            <th>
                                                                                                Tax Rate %
                                                                                            </th>
                                                                                        }
                                                                                        {
                                                                                            !emailRequest.isAccountingToolEnable && taxType &&
                                                                                            <th>
                                                                                                Tax Name
                                                                                            </th>
                                                                                        }

                                                                                        <th className="text-right">
                                                                                            Total Amount
                                                                                        </th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    {
                                                                                        detailsList.invoiceItems
                                                                                            ? detailsList.invoiceItems.map((list) =>
                                                                                                <tr key={list.id}>
                                                                                                    <td>
                                                                                                        {list.name}
                                                                                                    </td>
                                                                                                    <td width="40%">
                                                                                                        <p>{list.description}</p>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        {list.quantity}
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <span className="dollerInput">{parseFloat(list.unitPrice).toFixed(2)}</span>
                                                                                                    </td>
                                                                                                    {
                                                                                                        !emailRequest.isAccountingToolEnable && list.isTaxable == true &&
                                                                                                        <td>
                                                                                                            {list.taxPercent}
                                                                                                        </td>
                                                                                                    }

                                                                                                    {
                                                                                                        !emailRequest.isAccountingToolEnable && list.isTaxable &&
                                                                                                        <td>
                                                                                                            {list.taxName}
                                                                                                        </td>
                                                                                                    }
                                                                                                    <td className="text-right">
                                                                                                        {/* {list.subtotal} */}
                                                                                                        <span className="dollerInput">{parseFloat(list.subtotal).toFixed(2)}</span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            )
                                                                                            : null
                                                                                    }

                                                                                </tbody>

                                                                            </Table>
                                                                        )}

                                                                    </div>
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                </Col>
                                                <Col md={5}>
                                                    <div className="content-block rightContentBlock">
                                                        <Tabs defaultActiveKey="details" transition={false} id="noanim-tab-example">
                                                            <Tab eventKey="details" title="Details">
                                                                {!errorEnable && (
                                                                    <div>
                                                                        <div>
                                                                            <div className="invDisplayRightBlock collapaseOuter">
                                                                                <h3
                                                                                    style={{ marginBottom: '0' }}
                                                                                >
                                                                                    Invoice Details
                                                                                </h3>

                                                                                <Collapse in={invoiceDetails}>
                                                                                    <div id="audit-details">
                                                                                        <Table>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Invoice Number</span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        {detailsList[Fields.INVOICE_NUMBER]}
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Invoice Date</span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        {detailsList[Fields.INVOICE_DATE]}
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr className="">
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Invoice Due Date</span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        {detailsList[Fields.INVOICE_DUE_DATE]}
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">P.O. </span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        {detailsList[Fields.INVOICE_PO]}
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Terms</span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        {detailsList[Fields.TERM_ID]}
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </Table>
                                                                                    </div>
                                                                                </Collapse>
                                                                            </div>




                                                                            <div className="invDisplayRightBlock collapaseOuter">
                                                                                <h3>
                                                                                    Customer &amp; Vendor
                                                                                </h3>

                                                                                <Collapse in={customerVendor}>
                                                                                    <div id="audit-details">
                                                                                        <Table>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Customer Name</span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        {detailsList[Fields.CUSTOMER_NAME]}
                                                                                                    </td>
                                                                                                </tr>

                                                                                                <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Vendor Name</span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        {detailsList.customerVendor && detailsList.customerVendor[Fields.VENDOR_NAME]}
                                                                                                    </td>
                                                                                                </tr>
                                                                                                {/* <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Vendor Address</span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        {detailsList.customerVendor && detailsList.customerVendor[Fields.VENDOR_ADDRESS]}
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Vendor Phone No</span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        {detailsList.customerVendor && detailsList.customerVendor[Fields.VENDOR_PHONE_NO]}
                                                                                                    </td>
                                                                                                </tr> */}
                                                                                            </tbody>
                                                                                        </Table>
                                                                                    </div>
                                                                                </Collapse>
                                                                            </div>

                                                                            {/* <div className="invDisplayRightBlock collapaseOuter">
                                                                                <h3 >
                                                                                    Line Items
                                                                                </h3>

                                                                                <Collapse in={lineItems}>
                                                                                    <div id="audit-details">
                                                                                        <Table>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Line Item</span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        {detailsList[Fields.INVOICE_ITEMS].length}
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </Table>
                                                                                    </div>
                                                                                </Collapse>
                                                                            </div> */}


                                                                            <div className="invDisplayRightBlock collapaseOuter">
                                                                                <h3>
                                                                                    Invoice Details
                                                                                </h3>

                                                                                <Collapse in={invDetails}>
                                                                                    <div id="audit-details">
                                                                                        <Table>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Subtotal</span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        <span className="dollerInput">{parseFloat(detailsList[Fields.SUBTOTAL]).toFixed(2)}</span>

                                                                                                    </td>
                                                                                                </tr>
                                                                                                {/* {!emailRequest.isAccountingToolEnable && !taxType && */}
                                                                                                {!emailRequest.isAccountingToolEnable &&
                                                                                                    <tr>
                                                                                                        <td>

                                                                                                            <span className="companyNameRblock">Total Tax</span>
                                                                                                        </td>
                                                                                                        <td className="text-right">
                                                                                                            <span className="dollerInput">{parseFloat(detailsList[Fields.TOTAL_TAX]).toFixed(2)}</span>

                                                                                                        </td>
                                                                                                    </tr>
                                                                                                } 
                                                                                                <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Amount Due</span>
                                                                                                    </td>
                                                                                                    <td className="text-right ">
                                                                                                        <span className="dollerInput">{parseFloat(detailsList[Fields.AMOUNT_DUE]).toFixed(2)}</span>
                                                                                                    </td>
                                                                                                </tr>

                                                                                            </tbody>
                                                                                        </Table>
                                                                                    </div>
                                                                                </Collapse>
                                                                            </div>

                                                                            {/* <div className="invDisplayRightBlock collapaseOuter">
                                                                                <h3>
                                                                                    Invoice Additional Note
                                                                                </h3>

                                                                                <Collapse in={invAdditionalNote}>
                                                                                    <div id="audit-details">
                                                                                        <Table>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <Field
                                                                                                            name="comment2"
                                                                                                            component="textarea"
                                                                                                            className={'additionalNoteArea'}
                                                                                                            autoComplete="nope"
                                                                                                            placeholder="Enter"
                                                                                                            value={values.comment2 || ''}
                                                                                                            autoFocus
                                                                                                        />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </Table>
                                                                                    </div>
                                                                                </Collapse>
                                                                            </div> */}

                                                                        </div>
                                                                    </div>
                                                                )}

                                                            </Tab>
                                                            <Tab eventKey="assets" title="Assets">
                                                                <div className="invDisplayRightBlock">
                                                                    <h3>
                                                                        Original Email
                                                                    </h3>
                                                                    <div id="assets" className="invRightBlockCon">
                                                                        <h4>{emailRequest && emailRequest.emailMsgFrom}</h4>
                                                                        <p>{emailRequest && emailRequest.emailMsgSubject}</p>
                                                                    </div>
                                                                </div>

                                                                <div className="invDisplayRightBlock">
                                                                    <h3>
                                                                        Original Invoice
                                                                    </h3>
                                                                    <div id="invoicePdf" className="invRightBlockCon">
                                                                        <ul>
                                                                            <li>
                                                                                {fileName && <div> <Image src={pdfIcon} width="15" /> {fileName} &nbsp; &nbsp; <span className="link" onClick={() => this.downloadurl(s3FileLocation)} ><IoMdDownload /> Download</span></div>}
                                                                            </li>
                                                                        </ul>

                                                                    </div>
                                                                </div>
                                                            </Tab>
                                                            <Tab eventKey="comments" title="Comments">
                                                                <div className="invDisplayRightBlock">
                                                                    {/* {(auditTrails.length > 0) && auditTrails[0].comment} */}
                                                                    <ul>
                                                                        {(auditTrails.length > 0) && auditTrails.map(audit => audit.comment != '' && (<li>{audit.comment}</li>))}
                                                                    </ul>
                                                                </div>
                                                            </Tab>
                                                            <Tab eventKey="audittrails" title="Audit Trail">
                                                                <div className="invDisplayRightBlock">
                                                                    <h3>
                                                                        Audit Trail
                                                                    </h3>
                                                                    <div id="audit-details">
                                                                        <Table>
                                                                            <tbody>
                                                                                {auditTrails.length > 0 ? (auditTrails.map((audit, k) =>

                                                                                    < tr key={k}>
                                                                                        <td>
                                                                                            <span className="companyNameRblock">
                                                                                                {audit.userName}
                                                                                            </span>
                                                                                            <br />
                                                                                            {audit.activityDate}
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <span
                                                                                                className="statusRblock submit"
                                                                                            >
                                                                                                {audit.invoiceState.replace(/_/g, ' ')}
                                                                                            </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                )) : null
                                                                                }
                                                                            </tbody>
                                                                        </Table>
                                                                    </div>
                                                                </div>
                                                            </Tab>
                                                        </Tabs>

                                                    </div>
                                                </Col>
                                            </Row>
                                        </Form>
                                    );
                                }}
                            </Formik>

                    }


                </div>


            </div>
        );
    }
}

ViewInvoice.propTypes = {
    selectedID: PropTypes.number,
    selectedBusinessID: PropTypes.number,
    selectedStatus: PropTypes.string,
    s3FileLocation: PropTypes.string,
    modalCloseReviewInvoice: PropTypes.func,
    getAllList: PropTypes.func,
    fileName: PropTypes.string,
    emailRequest: PropTypes.object,


};

export default ViewInvoice;
