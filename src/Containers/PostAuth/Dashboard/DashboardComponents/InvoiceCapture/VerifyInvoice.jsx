/* eslint-disable indent */
/* eslint-disable no-unused-vars */
import {
    Field,
    Form,
    Formik,
    FieldArray
} from 'formik';
import React, {
    Component,
    Fragment
} from 'react';
import {
    Button,
    Col,
    Collapse,
    FormControl,
    FormGroup,
    Image,
    Row,
    Tab,
    Table,
    Tabs,
    Modal,
    //ButtonToolbar
} from 'react-bootstrap';
//import { Link } from 'react-router-dom';
import * as Yup from 'yup';
import InvSample from './../../../../../assets/invoice-sample.png';
import { FaFilePdf } from 'react-icons/fa';
import * as Fields from './InvoiceFields';
import axios from './../../../../../shared/eaxios';
import LoadingSpinner from './../../../../../Components/LoadingSpinner/LoadingSpinner';
import PropTypes from 'prop-types';
import SuccessIco from './../../../../../assets/success-ico.png';
import pdfIcon from './../../../../../assets/pdficon.png';
import { IoMdDownload } from 'react-icons/io';
import './VerifyInvoice.scss';


const initialerReport = {
    [Fields.INVOICE_NUMBER]: false,
    [Fields.INVOICE_DATE]: false,
    [Fields.INVOICE_DUE_DATE]: false,
    [Fields.INVOICE_PO]: false,
    [Fields.TERM_ID]: false,
    [Fields.CUSTOMER_NAME]: false,
    [Fields.CUSTOMER_ADDRESS]: false,
    [Fields.CUSTOMER_PHONE_NO]: false,
    [Fields.VENDOR_NAME]: false,
    // [Fields.VENDOR_ADDRESS]: false,
    // [Fields.VENDOR_PHONE_NO]: false,
    [Fields.INVOICE_ITEMS]: false,
    [Fields.SUBTOTAL]: false,
    [Fields.TOTAL_TAX]: false,
    [Fields.AMOUNT_DUE]: false,
    [Fields.CURRENCY]: false,
    [Fields.COMMENT]: '',
    invoiceItems: [],
    auditTrailNote: '',
    taxType: null, // TAX_LINE & TAX_INVOICE


};

const initialValue = {
    invoiceItems: [],
    auditTrailNote: ''
    //comment2: ''
};




class VerifyInvoice extends Component {
    state = {
        invoiceDetails: true,
        customerVendor: true,
        lineItems: true,
        invDetails: true,
        invAdditionalNote: true,
        invComment: true,
        errorEnable: false,
        detailsLoader: false,
        detailsError: null,
        detailsList: {},
        errorReportLoader: false,
        alertMsg: false,
        errorSubmitLoader: false,
        errorReportError: null,
        errorObj: {},
        showConfirMmsg: false,
        verifyError: null,
        verifyLoader: false,
        submitStatus: null,
        confirmState: null,
        auditTrails: [],
        errorChkLength: false,
        errorFlag: false,
        taxType: null
    }

    downloadurl(url) {
        setTimeout(() => {
            window.open(
                url,
                '_blank'
            );
        }, 100);
    }

    handleErrorChkAlert = () => {
        this.setState({
            errorChkLength: true
        });
    }

    handleErrorChkAlertClose = () => {
        this.setState({
            errorChkLength: false
        });
    }

    handleConfirmReviewClose = () => {
        this.setState({
            showConfirMmsg: false,
        }, () => {
            this.props.modalCloseReviewInvoice();
            this.props.getAllList();
        });
    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Unknown error!';
        }
        return errorMessge;
    }


    closeAlertMsg = () => {
        this.setState({
            alertMsg: false,
        });
    }
    openAlertMsg = () => {
        this.setState({
            alertMsg: true,
        });
    }
    cancelError = () => {
        this.setState({
            errorEnable: false,
        });
    }

    openErrorSection = () => {
        this.setState({
            errorEnable: true
        });
    }

    handleErrorReport = (values) => {
        console.log('values::::::::::::::::::invoiceItems', values.invoiceItems);

        const errorinv = values.invoiceItems.filter(item => item.isvalid);
        const errorChk = Object.values(values).filter(item => item == true);

        console.log('all report ::::::::::::::::::', errorinv, errorChk, errorChk.length, errorinv.length);
        if (errorChk.length === 0 && errorinv.length === 0) {
            this.setState({
                errorChkLength: true
            }, () => console.log('U r here%%%%%%%%%%%%%%'));
        } else {

            let temp = {};
            temp = values;
            console.log('errorObj::::::::::::::::::', values);
            //temp.isActive = true;
            let detailsList = this.state.detailsList;
            detailsList['termId'] = detailsList['temptermId'];
            let tempDetails = detailsList;

            //let tempDetails = this.state.detailsList;

            tempDetails.invoiceState = 'ADMIN_VOIDED';
            tempDetails['error']['errorJson'] = JSON.stringify(temp);
            tempDetails['error']['isActive'] = true;
            for (let el in tempDetails) { if (tempDetails[el] == true) { this.setState({ errorFlag: true }); } }
            tempDetails['auditTrailNote'] = values['auditTrailNote'];
            console.log('tempDetails||||||||||||||||||||||', tempDetails);
            console.log('tempDetails.error.errorJson//////////////////////', tempDetails.error.errorJson);
            this.setState({
                errorObj: tempDetails,
                submitStatus: 'error',
            }, () => this.openAlertMsg());
        }





    }

    handleVerify = () => {

        this.setState({
            submitStatus: 'verify',
        }, () => this.openAlertMsg());

    }



    componentDidMount() {
        console.log('I am verify did mount', this.props.emailRequest);
        this.getDetailsList();
    }

    componentDidUpdate() {
        console.log('I am verify did update', this.props.emailRequest);
    }
    componentWillUnmount() {
        console.log('I am verify componentWillUnmount');
    }

    getDetailsList = () => {
        this.setState({
            detailsLoader: true
        });


        axios
            .get(
                `invoice/${this.props.selectedID}/${this.props.selectedBusinessID}`
            )
            .then(res => {
                initialerReport['invoiceItems'] = res.data.invoiceItems;
                console.log('!!!!!!!!!!!!!!!!! initialerReport[invoiceItems] !!!!!!!!!!!!!!!', initialerReport['invoiceItems']);
                {
                    initialerReport['invoiceItems'][0]['isTaxable']
                        ? this.setState({ taxType: true })
                        : this.setState({ taxType: false });
                }

                initialValue.comment2 = res.data.comment;
                const detailsList = res.data;
                detailsList['termId'] = detailsList.termRequest && detailsList.termRequest.id ? detailsList.termRequest.termName : '';
                detailsList['temptermId'] = detailsList.termRequest && detailsList.termRequest.id;
                this.setState({
                    detailsLoader: false,
                    detailsList,
                    auditTrails: res.data.auditTrails

                }, () => console.log('detailsList: res.data', res.data));
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    detailsError: errorMsg,
                    detailsLoader: false

                });

                // setTimeout(() => {
                //     this.setState({ detailsError: null });
                // }, 5000);

            });
    }

    handleVerifyConfirm = () => {
        this.setState({
            verifyLoader: true
        });

        const detailsList = {...this.state.detailsList};
        detailsList['termId'] = detailsList['temptermId'];
        axios
            .put(`invoice/capture/verify/${this.state.detailsList.id}/${this.props.selectedBusinessID}`, detailsList)
            .then(res => {
                console.log(res);
                this.setState({
                    verifyLoader: false,
                    showConfirMmsg: true,
                    alertMsg: false,
                    confirmState: 'verify'
                }, () => {
                    this.props.getAllList();
                });
            })
            .catch(e => {

                let errorMsg = this.displayError(e);
                this.setState({
                    verifyError: errorMsg,
                    verifyLoader: false

                }, () => {
                });

                setTimeout(() => {
                    this.setState({ verifyError: null });
                }, 5000);

            });        


    }




    HandleConfirmReport = () => {
        this.setState({
            errorSubmitLoader: true,
        });

        console.log('*****  this.state.errorObj *****', this.state.errorObj);

        axios
            .put(`invoice/capture/void/${this.state.detailsList.id}/${this.props.selectedBusinessID}`, this.state.errorObj)
            .then(res => {
                console.log(res);
                this.setState({
                    errorSubmitLoader: false,
                    showConfirMmsg: true,
                    alertMsg: false,
                    confirmState: 'error'
                }, () => {
                    this.props.getAllList();
                });
            })
            .catch(e => {

                let errorMsg = this.displayError(e);
                this.setState({
                    errorReportError: errorMsg,
                    errorSubmitLoader: false,


                }, () => {
                });

                setTimeout(() => {
                    this.setState({
                        errorReportError: null, alertMsg: false,
                    });
                }, 5000);

            });
    }



    handleSubmit = (values) => {
        console.log('******************************************************', values);
        let detailsList = this.state.detailsList;
        //detailsList.invoiceState='UPLOADED';
        detailsList.invoiceState = 'SCANNED';
        //detailsList.comment = values.comment2;
        detailsList.auditTrailNote = values.auditTrailNote;
        detailsList.invoiceItems[0].id = null;

        this.setState({
            detailsList
        }, () => {
            this.handleVerify();
        });

    }


    render() {
        const {
            invoiceDetails,
            customerVendor,
            lineItems,
            invDetails,
            invAdditionalNote,
            detailsList,
            detailsLoader,
            detailsError,
            errorReportError,
            errorEnable,
            verifyError,
            submitStatus,
            verifyLoader,
            errorSubmitLoader,
            auditTrails,
            invComment,
            taxType
        } = this.state;

        const { emailRequest, s3FileLocation, fileName } = this.props;

        return (
            <div className="verifyInvoiceOuter invoicMasterWrap">
                <div className="invoiceMasterHeader">
                    <Row>
                        <Col md={3}>
                            <h1>Verify Invoice</h1>
                        </Col>
                    </Row>
                </div>

                <div className="invoiceMasterBody content-body">

                    {detailsLoader ?
                        <LoadingSpinner />
                        : detailsError
                            ?
                            <div>{detailsError}</div>
                            :
                            <Formik
                                initialValues={errorEnable ? initialerReport : initialValue}
                                //validationSchema={errorEnable ? reportInvoiceSchema : submitInvoiceSchema}
                                onSubmit={errorEnable ? this.handleErrorReport : this.handleSubmit}
                                enableReinitialize
                            >
                                {({
                                    values,
                                    //errors,
                                    //touched,
                                    setFieldValue
                                }) => {


                                    return (
                                        <Form>
                                            <Row>
                                                <Col md={7}>
                                                    <div className="content-block leftContentBlock">
                                                        <Row>
                                                            <Col md={12}>
                                                                {this.props && this.props.s3FileLocation ? (
                                                                    <iframe
                                                                        src={this.props.s3FileLocation + '#toolbar=0&navpanes=0&statusbar=0&view=Fit;readonly=true; disableprint=true;'}
                                                                        width="100%"
                                                                        height="500"
                                                                        frameBorder="0"
                                                                        title="Invoice"
                                                                        allowFullScreen
                                                                    ></iframe>
                                                                ) : (
                                                                        <div className="invNotFound">
                                                                            <h5>No invoice found.</h5>
                                                                        </div>
                                                                    )
                                                                }
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col md={12}>
                                                                <div className="invInfoTable">
                                                                    <div className="table-responsive">
                                                                        {!errorEnable && (
                                                                            <Table>
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th width="150">
                                                                                            Name
                                                                                    </th>

                                                                                        <th width="200">
                                                                                            Description
                                                                                    </th>
                                                                                        <th>
                                                                                            Quantity
                                                                                    </th>
                                                                                        <th>
                                                                                            Unit price
                                                                                    </th>
                                                                                        {
                                                                                            !emailRequest.isAccountingToolEnable && taxType &&
                                                                                            <th>
                                                                                                Tax Rate %
                                                                                        </th>
                                                                                        }
                                                                                        {
                                                                                            !emailRequest.isAccountingToolEnable && taxType &&
                                                                                            <th>
                                                                                                Tax Name
                                                                                        </th>
                                                                                        }

                                                                                        <th className="text-right">
                                                                                            Subtotal
                                                                                    </th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    {
                                                                                        detailsList.invoiceItems
                                                                                            ? detailsList.invoiceItems.map((list) =>
                                                                                                <tr key={list.id}>
                                                                                                    <td>
                                                                                                        {list.name}
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <p>{list.description}</p>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        {list.quantity}
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <span className="dollerInput">{parseFloat(list.unitPrice).toFixed(2)}</span>
                                                                                                    </td>
                                                                                                    {
                                                                                                        !emailRequest.isAccountingToolEnable && list.isTaxable == true &&
                                                                                                        <td>
                                                                                                            {(list.taxPercent).toFixed(2)}
                                                                                                        </td>
                                                                                                    }

                                                                                                    {
                                                                                                        !emailRequest.isAccountingToolEnable && list.isTaxable &&
                                                                                                        <td>
                                                                                                            {list.taxName}
                                                                                                        </td>
                                                                                                    }
                                                                                                    <td className="text-right">
                                                                                                        {/* {list.subtotal} */}
                                                                                                        <span className="dollerInput">{parseFloat(list.subtotal).toFixed(2)}</span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            )
                                                                                            : null
                                                                                    }

                                                                                </tbody>

                                                                            </Table>
                                                                        )}

                                                                        {errorEnable && (

                                                                            <FieldArray
                                                                                name="invoiceItems"
                                                                                render={() => (
                                                                                    <Fragment>
                                                                                        {
                                                                                            values.invoiceItems.length > 0 ?
                                                                                                <Table responsive hover>
                                                                                                    <thead className="theaderBg">
                                                                                                        <tr>
                                                                                                            <th width="6%">&nbsp;</th>
                                                                                                            <th>Name</th>
                                                                                                            <th>Description</th>
                                                                                                            <th style={{ width: '25px' }}>Quantity</th>
                                                                                                            <th style={{ width: '130px' }}>Unit Price</th>
                                                                                                            {
                                                                                                                !emailRequest.isAccountingToolEnable && taxType &&
                                                                                                                <th>
                                                                                                                    Tax Rate %
                                                                                                            </th>
                                                                                                            }
                                                                                                            {
                                                                                                                !emailRequest.isAccountingToolEnable && taxType &&
                                                                                                                <th>
                                                                                                                    Tax Name
                                                                                                            </th>
                                                                                                            }
                                                                                                            <th>Subtotal</th>
                                                                                                            <th width="6%">&nbsp;</th>
                                                                                                        </tr>
                                                                                                    </thead><tbody>
                                                                                                        {values.invoiceItems && values.invoiceItems.length ? values.invoiceItems.map((inv, index) => {
                                                                                                            inv.unitPrice = parseFloat(inv.unitPrice).toFixed(2);
                                                                                                            const tempTotal = (inv.unitPrice * inv.quantity);
                                                                                                            let percentCalc;
                                                                                                            this.state.taxType ? percentCalc = (inv.taxPercent / 100) * tempTotal : percentCalc = 0;
                                                                                                            //const subtotal = tempTotal + percentCalc;
                                                                                                            const subtotal = tempTotal;
                                                                                                            inv.subtotal = isNaN((subtotal).toFixed(2)) ? 0.00 : (subtotal).toFixed(2);
                                                                                                            //inv.subtotal = inv.unitPrice * inv.quantity;
                                                                                                            // inv.isvalid = false;
                                                                                                            inv.tax = (inv.subtotal * values.taxPercent) / 100;

                                                                                                            inv.taxPercent = parseFloat(inv.taxPercent).toFixed(2);
                                                                                                            return (
                                                                                                                <tr key={index}>
                                                                                                                    <td>
                                                                                                                        <FormGroup controlId="formBasicText">
                                                                                                                            <Field
                                                                                                                                name={`invoiceItems.${index}.isvalid`}
                                                                                                                                type="checkbox"
                                                                                                                                className={'form-control editCheckBox'}
                                                                                                                                autoComplete="nope"
                                                                                                                                // checked={invoiceItems[index.isvalid}
                                                                                                                                onChange={
                                                                                                                                    (e) => {
                                                                                                                                        setFieldValue(`invoiceItems.${index}.isvalid`, e.target.checked);
                                                                                                                                    }}
                                                                                                                                readonly="true"

                                                                                                                            />
                                                                                                                            <FormControl.Feedback />
                                                                                                                        </FormGroup>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <FormGroup controlId="formBasicText">
                                                                                                                            <Field
                                                                                                                                name={`invoiceItems.${index}.name`}
                                                                                                                                type="text"
                                                                                                                                className={'form-control'}
                                                                                                                                autoComplete="nope"
                                                                                                                                placeholder="Name"
                                                                                                                                readonly="true"
                                                                                                                            />
                                                                                                                            <FormControl.Feedback />
                                                                                                                        </FormGroup>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <FormGroup controlId="formBasicText">
                                                                                                                            <Field
                                                                                                                                name={`invoiceItems.${index}.description`}
                                                                                                                                type="text"
                                                                                                                                className={'form-control'}
                                                                                                                                autoComplete="nope"
                                                                                                                                placeholder="Description"
                                                                                                                                readonly="true"
                                                                                                                            />
                                                                                                                            <FormControl.Feedback />
                                                                                                                        </FormGroup>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <FormGroup controlId="formBasicText">
                                                                                                                            <Field
                                                                                                                                name={`invoiceItems.${index}.quantity`}
                                                                                                                                type="number"
                                                                                                                                className={'form-control'}
                                                                                                                                autoComplete="nope"
                                                                                                                                placeholder=""
                                                                                                                                min="0"
                                                                                                                                readonly="true"
                                                                                                                            />
                                                                                                                            <FormControl.Feedback />
                                                                                                                        </FormGroup>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <FormGroup controlId="formBasicText">
                                                                                                                            <Field
                                                                                                                                name={`invoiceItems.${index}.unitPrice`}
                                                                                                                                type="number"
                                                                                                                                className={'form-control dollerInput'}
                                                                                                                                autoComplete="nope"
                                                                                                                                min="0"
                                                                                                                                readonly="true"
                                                                                                                            />
                                                                                                                            <FormControl.Feedback />
                                                                                                                        </FormGroup>
                                                                                                                    </td>
                                                                                                                    {
                                                                                                                        !emailRequest.isAccountingToolEnable && taxType &&
                                                                                                                        <td>

                                                                                                                            <FormGroup controlId="formBasicText">
                                                                                                                                <Field
                                                                                                                                    name={`invoiceItems.${index}.taxPercent`}
                                                                                                                                    type="number"
                                                                                                                                    className={'form-control'}
                                                                                                                                    autoComplete="nope"
                                                                                                                                    min="0"
                                                                                                                                    readonly="true"
                                                                                                                                />
                                                                                                                                <FormControl.Feedback />
                                                                                                                            </FormGroup>
                                                                                                                        </td>

                                                                                                                    }

                                                                                                                    {
                                                                                                                        !emailRequest.isAccountingToolEnable && taxType &&
                                                                                                                        <td>
                                                                                                                            <FormGroup controlId="formBasicText">
                                                                                                                                <Field
                                                                                                                                    name={`invoiceItems.${index}.taxName`}
                                                                                                                                    type="text"
                                                                                                                                    className={'form-control'}
                                                                                                                                    autoComplete="nope"
                                                                                                                                    min="0"
                                                                                                                                    readonly="true"
                                                                                                                                />
                                                                                                                                <FormControl.Feedback />
                                                                                                                            </FormGroup>
                                                                                                                        </td>
                                                                                                                    }

                                                                                                                    <td>
                                                                                                                        <FormGroup controlId="formBasicText">
                                                                                                                            <Field
                                                                                                                                name={`invoiceItems.${index}.subtotal`}
                                                                                                                                type="text"
                                                                                                                                className={'form-control'}
                                                                                                                                autoComplete="nope"
                                                                                                                                placeholder="Sub Total"
                                                                                                                                readonly="true"
                                                                                                                            />
                                                                                                                            <FormControl.Feedback />
                                                                                                                        </FormGroup>
                                                                                                                    </td>

                                                                                                                    {/* <td>
                                                                                                                <div className="showHideItem">
                                                                                                                    <Link to="#" className="smIconBg" onClick={() => helpers.remove(index)}>
                                                                                                                        x
                                                                                                                    </Link>
                                                                                                                </div>
                                                                                                            </td> */}
                                                                                                                </tr>
                                                                                                            );
                                                                                                        }) : null}
                                                                                                    </tbody>

                                                                                                </Table>
                                                                                                : null
                                                                                        }
                                                                                    </Fragment>
                                                                                )}
                                                                            />
                                                                        )}
                                                                    </div>
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                </Col>
                                                <Col md={5}>
                                                    <div className="content-block rightContentBlock">
                                                        <Tabs defaultActiveKey="details" transition={false} id="noanim-tab-example">
                                                            <Tab eventKey="details" title="Details">
                                                                {!errorEnable && (
                                                                    <div>
                                                                        <div>
                                                                            <div className="invDisplayRightBlock collapaseOuter">
                                                                                <h3
                                                                                    style={{ marginBottom: '0' }}
                                                                                >
                                                                                    Invoice Details
                                                                                </h3>

                                                                                <Collapse in={invoiceDetails}>
                                                                                    <div id="audit-details">
                                                                                        <Table>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Invoice Number</span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        {detailsList[Fields.INVOICE_NUMBER]}
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Invoice Date</span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        {detailsList[Fields.INVOICE_DATE]}
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr className="">
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Invoice Due Date</span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        {detailsList[Fields.INVOICE_DUE_DATE]}
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">P.O. </span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        {detailsList[Fields.INVOICE_PO]}
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Terms</span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        {detailsList[Fields.TERM_ID]}
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </Table>
                                                                                    </div>
                                                                                </Collapse>
                                                                            </div>




                                                                            <div className="invDisplayRightBlock collapaseOuter">
                                                                                <h3>
                                                                                    Customer &amp; Vendor
                                                                                </h3>

                                                                                <Collapse in={customerVendor}>
                                                                                    <div id="audit-details">
                                                                                        <Table>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Customer Name</span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        {detailsList[Fields.CUSTOMER_NAME]}
                                                                                                    </td>
                                                                                                </tr>

                                                                                                <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Vendor Name</span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        {detailsList.customerVendor && detailsList.customerVendor[Fields.VENDOR_NAME]}
                                                                                                    </td>
                                                                                                </tr>
                                                                                                {/* <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Vendor Address</span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        {detailsList.customerVendor && detailsList.customerVendor[Fields.VENDOR_ADDRESS]}
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Vendor Phone No</span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        {detailsList.customerVendor && detailsList.customerVendor[Fields.VENDOR_PHONE_NO]}
                                                                                                    </td>
                                                                                                </tr> */}
                                                                                            </tbody>
                                                                                        </Table>
                                                                                    </div>
                                                                                </Collapse>
                                                                            </div>

                                                                            {/* <div className="invDisplayRightBlock collapaseOuter">
                                                                                <h3 >
                                                                                    Line Items
                                                                                </h3>

                                                                                <Collapse in={lineItems}>
                                                                                    <div id="audit-details">
                                                                                        <Table>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Line Item</span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </Table>
                                                                                    </div>
                                                                                </Collapse>
                                                                            </div> */}


                                                                            <div className="invDisplayRightBlock collapaseOuter">
                                                                                <h3>
                                                                                    Invoice Details
                                                                                </h3>

                                                                                <Collapse in={invDetails}>
                                                                                    <div id="audit-details">
                                                                                        <Table>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Subtotal</span>
                                                                                                    </td>
                                                                                                    <td className="text-right">
                                                                                                        <span className="dollerInput">{parseFloat(detailsList[Fields.SUBTOTAL]).toFixed(2)}</span>

                                                                                                    </td>
                                                                                                </tr>
                                                                                                {/* CONDITION REQ */}
                                                                                                {/* {!emailRequest.isAccountingToolEnable && !taxType && */}
                                                                                                {!emailRequest.isAccountingToolEnable &&
                                                                                                    <tr>
                                                                                                        <td>

                                                                                                            <span className="companyNameRblock">Total Tax</span>
                                                                                                        </td>
                                                                                                        <td className="text-right">
                                                                                                            <span className="dollerInput">{parseFloat(detailsList[Fields.TOTAL_TAX]).toFixed(2)}</span>

                                                                                                        </td>
                                                                                                    </tr>
                                                                                                }

                                                                                                <tr>
                                                                                                    <td>

                                                                                                        <span className="companyNameRblock">Amount Due</span>
                                                                                                    </td>
                                                                                                    <td className="text-right ">
                                                                                                        <span className="dollerInput">{parseFloat(detailsList[Fields.AMOUNT_DUE]).toFixed(2)}</span>
                                                                                                    </td>
                                                                                                </tr>

                                                                                            </tbody>
                                                                                        </Table>
                                                                                    </div>
                                                                                </Collapse>
                                                                            </div>
                                                                            {/* 
                                                                            <div className="invDisplayRightBlock collapaseOuter">
                                                                                <h3>
                                                                                    Invoice Additional Note
                                                                                </h3>

                                                                                <Collapse in={invAdditionalNote}>
                                                                                    <div id="audit-details">
                                                                                        <Table>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <Field
                                                                                                            name="comment2"
                                                                                                            component="textarea"
                                                                                                            className={'additionalNoteArea'}
                                                                                                            autoComplete="nope"
                                                                                                            placeholder="Enter"
                                                                                                            value={values.comment2 || ''}
                                                                                                            autoFocus
                                                                                                        />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </Table>
                                                                                    </div>
                                                                                </Collapse>
                                                                            </div> */}

                                                                            <div className="invDisplayRightBlock collapaseOuter">
                                                                                <h3>
                                                                                    Invoice Additional Note
                                                                                </h3>

                                                                                <Collapse in={invAdditionalNote}>
                                                                                    <div id="audit-details">
                                                                                        <Table>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    {/* <td>
                                                                                                <Field
                                                                                                    component="textarea"
                                                                                                    className={'additionalNoteArea'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="Enter"
                                                                                                    name="comment"
                                                                                                    value={detailsList.comment || ''} 
                                                                                                />
                                                                                            </td> */}

                                                                                                    <td className="text-right">
                                                                                                        {detailsList[Fields.COMMENT]}
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </Table>
                                                                                    </div>
                                                                                </Collapse>
                                                                            </div>
                                                                            {/* Verify sumit section */}
                                                                            <div className="invDisplayRightBlock collapaseOuter">
                                                                                <h3 onClick={() => this.setState({ invComment: !invComment })}
                                                                                    aria-controls="inv-note"
                                                                                    aria-expanded={invComment}
                                                                                    className={invComment === false ? 'collapas-show' : 'collapas-hide'}
                                                                                    style={{ marginBottom: '0' }}
                                                                                >
                                                                                    Comment
                                                                                </h3>
                                                                                <Collapse in={invComment}>
                                                                                    <div id="audit-details">
                                                                                        <Table>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <Field
                                                                                                            name="auditTrailNote"
                                                                                                            component="textarea"
                                                                                                            className={'additionalNoteArea'}
                                                                                                            autoComplete="nope"
                                                                                                            placeholder="Enter"
                                                                                                            value={values.auditTrailNote || ''}
                                                                                                            autoFocus
                                                                                                        />
                                                                                                        {/* {errors.auditTrailNote && touched.auditTrailNote ? (
                                                                                                    <span className="errorMsg">
                                                                                                        {errors.auditTrailNote}
                                                                                                    </span>
                                                                                                ) : null} */}
                                                                                                        <FormControl.Feedback />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </Table>
                                                                                    </div>
                                                                                </Collapse>
                                                                            </div>


                                                                        </div>
                                                                    </div>
                                                                )}
                                                                {errorEnable && (<div>
                                                                    <div className="invDisplayRightBlock collapaseOuter">
                                                                        <h3
                                                                            style={{ marginBottom: '0' }}
                                                                        >
                                                                            Invoice Details
                                                                        </h3>

                                                                        <Collapse in={invoiceDetails}>
                                                                            <div id="audit-details">
                                                                                <Table>
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <label className="customCkBox">
                                                                                                    <input
                                                                                                        type="checkbox"
                                                                                                        name={Fields.INVOICE_NUMBER}
                                                                                                        checked={values[Fields.INVOICE_NUMBER]}
                                                                                                        onChange={(e) => {
                                                                                                            setFieldValue(`${Fields.INVOICE_NUMBER}`, e.target.checked);
                                                                                                        }}
                                                                                                    />
                                                                                                    <span className="checkmark" />
                                                                                                </label>
                                                                                                <span className="companyNameRblock">Invoice Number</span>
                                                                                            </td>
                                                                                            <td className="text-right">
                                                                                                {detailsList[Fields.INVOICE_NUMBER]}
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <label className="customCkBox">
                                                                                                    <input
                                                                                                        type="checkbox"
                                                                                                        name={Fields.INVOICE_DATE}
                                                                                                        checked={values[Fields.INVOICE_DATE]}
                                                                                                        onChange={(e) => {
                                                                                                            setFieldValue(`${Fields.INVOICE_DATE}`, e.target.checked);
                                                                                                        }}
                                                                                                    />
                                                                                                    <span className="checkmark" />
                                                                                                </label>
                                                                                                <span className="companyNameRblock">Invoice Date</span>
                                                                                            </td>
                                                                                            <td className="text-right">
                                                                                                {detailsList[Fields.INVOICE_DATE]}
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr className="">
                                                                                            <td>
                                                                                                <label className="customCkBox">
                                                                                                    <input
                                                                                                        type="checkbox"
                                                                                                        name={Fields.INVOICE_DUE_DATE}
                                                                                                        checked={values[Fields.INVOICE_DUE_DATE]}
                                                                                                        onChange={(e) => {
                                                                                                            setFieldValue(`${Fields.INVOICE_DUE_DATE}`, e.target.checked);
                                                                                                        }}
                                                                                                    />
                                                                                                    <span className="checkmark" />
                                                                                                </label>
                                                                                                <span className="companyNameRblock">Invoice Due Date</span>
                                                                                            </td>
                                                                                            <td className="text-right">
                                                                                                {detailsList[Fields.INVOICE_DUE_DATE]}
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <label className="customCkBox">
                                                                                                    <input
                                                                                                        type="checkbox"
                                                                                                        name={Fields.INVOICE_PO}
                                                                                                        checked={values[Fields.INVOICE_PO]}
                                                                                                        onChange={(e) => {
                                                                                                            setFieldValue(`${Fields.INVOICE_PO}`, e.target.checked);
                                                                                                        }}
                                                                                                    />
                                                                                                    <span className="checkmark" />
                                                                                                </label>
                                                                                                <span className="companyNameRblock">P.O. </span>
                                                                                            </td>
                                                                                            <td className="text-right">
                                                                                                {detailsList[Fields.INVOICE_PO]}
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <label className="customCkBox">
                                                                                                    <input
                                                                                                        type="checkbox"
                                                                                                        name={Fields.TERM_ID}
                                                                                                        checked={values[Fields.TERM_ID]}
                                                                                                        onChange={(e) => {
                                                                                                            setFieldValue(`${Fields.TERM_ID}`, e.target.checked);
                                                                                                        }}
                                                                                                    />
                                                                                                    <span className="checkmark" />
                                                                                                </label>
                                                                                                <span className="companyNameRblock">Terms</span>
                                                                                            </td>
                                                                                            <td className="text-right">
                                                                                                {detailsList[Fields.TERM_ID]}
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </Table>
                                                                            </div>
                                                                        </Collapse>
                                                                    </div>




                                                                    <div className="invDisplayRightBlock collapaseOuter">
                                                                        <h3>
                                                                            Customer &amp; Vendor
                                                                        </h3>

                                                                        <Collapse in={customerVendor}>
                                                                            <div id="audit-details">
                                                                                <Table>
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td>
                                                                                                {/* <label className="customCkBox">
                                                                                                    <input
                                                                                                        type="checkbox"
                                                                                                        name={Fields.CUSTOMER_NAME}
                                                                                                        checked={values[Fields.CUSTOMER_NAME]}
                                                                                                        onChange={(e) => {
                                                                                                            setFieldValue(`${Fields.CUSTOMER_NAME}`, e.target.checked);
                                                                                                        }}
                                                                                                    />
                                                                                                    <span className="checkmark" />
                                                                                                </label> */}
                                                                                                <span className="companyNameRblock">Customer Name</span>
                                                                                            </td>
                                                                                            <td className="text-right">
                                                                                                {detailsList[Fields.CUSTOMER_NAME]}
                                                                                            </td>
                                                                                        </tr>
                                                                                        {/* <tr>
                                                                                            <td>
                                                                                                <label className="customCkBox">
                                                                                                    <input
                                                                                                        type="checkbox"
                                                                                                        name={Fields.CUSTOMER_ADDRESS}
                                                                                                        checked={values[Fields.CUSTOMER_ADDRESS]}
                                                                                                        onChange={(e) => {
                                                                                                            setFieldValue(`${Fields.CUSTOMER_ADDRESS}`, e.target.checked);
                                                                                                        }}
                                                                                                    />
                                                                                                    <span className="checkmark" />
                                                                                                </label>
                                                                                                <span className="companyNameRblock">Customer Address</span>
                                                                                            </td>
                                                                                            <td className="text-right">
                                                                                                {detailsList[Fields.CUSTOMER_ADDRESS]}
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <label className="customCkBox">
                                                                                                    <input
                                                                                                        type="checkbox"
                                                                                                        name={Fields.CUSTOMER_PHONE_NO}
                                                                                                        checked={values[Fields.CUSTOMER_PHONE_NO]}
                                                                                                        onChange={(e) => {
                                                                                                            setFieldValue(`${Fields.CUSTOMER_PHONE_NO}`, e.target.checked);
                                                                                                        }}
                                                                                                    />
                                                                                                    <span className="checkmark" />
                                                                                                </label>
                                                                                                <span className="companyNameRblock">Customer Phone No</span>
                                                                                            </td>
                                                                                            <td className="text-right">
                                                                                                {detailsList[Fields.CUSTOMER_PHONE_NO]}
                                                                                            </td>
                                                                                        </tr> */}
                                                                                        <tr>
                                                                                            <td>
                                                                                                <label className="customCkBox">
                                                                                                    <input
                                                                                                        type="checkbox"
                                                                                                        name={Fields.VENDOR_NAME}
                                                                                                        checked={values[Fields.VENDOR_NAME]}
                                                                                                        onChange={(e) => {
                                                                                                            setFieldValue(`${Fields.VENDOR_NAME}`, e.target.checked);
                                                                                                        }}
                                                                                                    />
                                                                                                    <span className="checkmark" />
                                                                                                </label>
                                                                                                <span className="companyNameRblock">Vendor Name</span>
                                                                                            </td>
                                                                                            <td className="text-right">
                                                                                                {detailsList.customerVendor[Fields.VENDOR_NAME]}
                                                                                            </td>
                                                                                        </tr>
                                                                                        {/* <tr>
                                                                                            <td>
                                                                                                <label className="customCkBox">
                                                                                                    <input
                                                                                                        type="checkbox"
                                                                                                        name={Fields.VENDOR_ADDRESS}
                                                                                                        checked={values[Fields.VENDOR_ADDRESS]}
                                                                                                        onChange={(e) => {
                                                                                                            setFieldValue(`${Fields.VENDOR_ADDRESS}`, e.target.checked);
                                                                                                        }}
                                                                                                    />
                                                                                                    <span className="checkmark" />
                                                                                                </label>
                                                                                                <span className="companyNameRblock">Vendor Address</span>
                                                                                            </td>
                                                                                            <td className="text-right">
                                                                                                {detailsList.customerVendor[Fields.VENDOR_ADDRESS]}
                                                                                            </td>
                                                                                        </tr> */}
                                                                                        {/* <tr>
                                                                                            <td>
                                                                                                <label className="customCkBox">
                                                                                                    <input
                                                                                                        type="checkbox"
                                                                                                        name={Fields.VENDOR_PHONE_NO}
                                                                                                        checked={values[Fields.VENDOR_PHONE_NO]}
                                                                                                        onChange={(e) => {
                                                                                                            setFieldValue(`${Fields.VENDOR_PHONE_NO}`, e.target.checked);
                                                                                                        }}
                                                                                                    />
                                                                                                    <span className="checkmark" />
                                                                                                </label>
                                                                                                <span className="companyNameRblock">Vendor Phone No</span>
                                                                                            </td>
                                                                                            <td className="text-right">
                                                                                                {detailsList.customerVendor[Fields.VENDOR_PHONE_NO]}
                                                                                            </td>
                                                                                        </tr> */}
                                                                                    </tbody>
                                                                                </Table>
                                                                            </div>
                                                                        </Collapse>
                                                                    </div>

                                                                    {/* <div className="invDisplayRightBlock collapaseOuter">
                                                                        <h3 >
                                                                            Line Items
                                                                        </h3>

                                                                        <Collapse in={lineItems}>
                                                                            <div id="audit-details">
                                                                                <Table>
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <label className="customCkBox">
                                                                                                    <input
                                                                                                        type="checkbox"
                                                                                                        name={Fields.INVOICE_ITEMS}
                                                                                                        checked={values[Fields.INVOICE_ITEMS]}
                                                                                                        onChange={(e) => {
                                                                                                            setFieldValue(`${Fields.INVOICE_ITEMS}`, e.target.checked);
                                                                                                        }}
                                                                                                    />
                                                                                                    <span className="checkmark" />
                                                                                                </label>
                                                                                                <span className="companyNameRblock">Line Item</span>
                                                                                            </td>
                                                                                            <td className="text-right">
                                                                                                {detailsList[Fields.INVOICE_ITEMS].length}
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </Table>
                                                                            </div>
                                                                        </Collapse>
                                                                    </div> */}


                                                                    <div className="invDisplayRightBlock collapaseOuter">
                                                                        <h3>
                                                                            Invoice Details
                                                                        </h3>

                                                                        <Collapse in={invDetails}>
                                                                            <div id="audit-details">
                                                                                <Table>
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <label className="customCkBox">
                                                                                                    <input
                                                                                                        type="checkbox"
                                                                                                        name={Fields.SUBTOTAL}
                                                                                                        checked={values[Fields.SUBTOTAL]}
                                                                                                        onChange={(e) => {
                                                                                                            setFieldValue(`${Fields.SUBTOTAL}`, e.target.checked);
                                                                                                        }}
                                                                                                    />
                                                                                                    <span className="checkmark" />
                                                                                                </label>
                                                                                                <span className="companyNameRblock">Subtotal</span>
                                                                                            </td>
                                                                                            <td className="text-right">
                                                                                                {parseFloat(detailsList[Fields.SUBTOTAL]).toFixed(2)}
                                                                                            </td>
                                                                                        </tr>
                                                                                        {/* {!emailRequest.isAccountingToolEnable && !taxType && */}
                                                                                        {!emailRequest.isAccountingToolEnable &&
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <label className="customCkBox">
                                                                                                        <input
                                                                                                            type="checkbox"
                                                                                                            name={Fields.TOTAL_TAX}
                                                                                                            checked={values[Fields.TOTAL_TAX]}
                                                                                                            onChange={(e) => {
                                                                                                                setFieldValue(`${Fields.TOTAL_TAX}`, e.target.checked);
                                                                                                            }}
                                                                                                        />
                                                                                                        <span className="checkmark" />
                                                                                                    </label>
                                                                                                    <span className="companyNameRblock">Total Tax</span>
                                                                                                </td>
                                                                                                <td className="text-right">
                                                                                                {parseFloat(detailsList[Fields.TOTAL_TAX]).toFixed(2)}
                                                                                                </td>
                                                                                            </tr>
                                                                                        } 
                                                                                        <tr>
                                                                                            <td>
                                                                                                <label className="customCkBox">
                                                                                                    <input
                                                                                                        type="checkbox"
                                                                                                        name={Fields.AMOUNT_DUE}
                                                                                                        checked={values[Fields.AMOUNT_DUE]}
                                                                                                        onChange={(e) => {
                                                                                                            setFieldValue(`${Fields.AMOUNT_DUE}`, e.target.checked);
                                                                                                        }}
                                                                                                    />
                                                                                                    <span className="checkmark" />
                                                                                                </label>
                                                                                                <span className="companyNameRblock">Amount Due</span>
                                                                                            </td>
                                                                                            <td className="text-right">
                                                                                                {parseFloat(detailsList[Fields.AMOUNT_DUE]).toFixed(2)}
                                                                                            </td>
                                                                                        </tr>

                                                                                    </tbody>
                                                                                </Table>
                                                                            </div>
                                                                        </Collapse>
                                                                    </div>

                                                                    <div className="invDisplayRightBlock collapaseOuter">
                                                                        <h3>
                                                                            Invoice Additional Note
                                                                        </h3>

                                                                        <Collapse in={invAdditionalNote}>
                                                                            <div id="audit-details">
                                                                                <Table>
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            {/* <td>
                                                                                                <Field
                                                                                                    component="textarea"
                                                                                                    className={'additionalNoteArea'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="Enter"
                                                                                                    name="comment"
                                                                                                    value={detailsList.comment || ''} 
                                                                                                />
                                                                                            </td> */}
                                                                                            <td>
                                                                                                <label className="customCkBox">
                                                                                                    <input
                                                                                                        type="checkbox"
                                                                                                        name={Fields.COMMENT}
                                                                                                        checked={values[Fields.COMMENT]}
                                                                                                        onChange={(e) => {
                                                                                                            setFieldValue(`${Fields.COMMENT}`, e.target.checked);
                                                                                                        }}
                                                                                                    />
                                                                                                    <span className="checkmark" />
                                                                                                </label>
                                                                                                {/* <span className="companyNameRblock">Amount Due</span> */}
                                                                                            </td>
                                                                                            <td className="text-right">
                                                                                                {detailsList[Fields.COMMENT]}
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </Table>
                                                                            </div>
                                                                        </Collapse>
                                                                    </div>


                                                                    <div className="invDisplayRightBlock collapaseOuter">
                                                                        <h3 onClick={() => this.setState({ invComment: !invComment })}
                                                                            aria-controls="inv-note"
                                                                            aria-expanded={invComment}
                                                                            className={invComment === false ? 'collapas-show' : 'collapas-hide'}
                                                                            style={{ marginBottom: '0' }}
                                                                        >
                                                                            Comment
                                                                        </h3>
                                                                        <Collapse in={invComment}>
                                                                            <div id="audit-details">
                                                                                <Table>
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <Field
                                                                                                    name="auditTrailNote"
                                                                                                    component="textarea"
                                                                                                    className={'additionalNoteArea'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="Enter"
                                                                                                    value={values.auditTrailNote || ''}
                                                                                                    autoFocus
                                                                                                />
                                                                                                {/* {errors.auditTrailNote && touched.auditTrailNote ? (
                                                                                                    <span className="errorMsg">
                                                                                                        {errors.auditTrailNote}
                                                                                                    </span>
                                                                                                ) : null} */}
                                                                                                <FormControl.Feedback />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </Table>
                                                                            </div>
                                                                        </Collapse>
                                                                    </div>


                                                                </div>

                                                                )}

                                                                {errorEnable
                                                                    ?
                                                                    (
                                                                        <div className="invButtonsWrap text-center">
                                                                            <Button type="button" className="btn-dark" onClick={() => this.cancelError()}>
                                                                                Cancel
                                                                            </Button>
                                                                            <Button type="submit" className="ml-2">
                                                                                Submit Error
                                                                            </Button>
                                                                        </div>
                                                                    )
                                                                    :
                                                                    <div className="invButtonsWrap text-center">
                                                                        <Button type="button" className="btn-dark" onClick={() => this.openErrorSection()}>
                                                                            Report Error
                                                                        </Button>
                                                                        <Button type="submit" className="ml-2">
                                                                            Submit
                                                                        </Button>
                                                                    </div>
                                                                }

                                                            </Tab>
                                                            <Tab eventKey="assets" title="Assets">
                                                                <div className="invDisplayRightBlock">
                                                                    <h3>
                                                                        Original Email
                                                                    </h3>
                                                                    <div id="assets" className="invRightBlockCon">
                                                                        <h4>{emailRequest && emailRequest.emailMsgFrom}</h4>
                                                                        <p>{emailRequest && emailRequest.emailMsgSubject}</p>
                                                                    </div>
                                                                </div>

                                                                <div className="invDisplayRightBlock">
                                                                    <h3>
                                                                        Original Invoice
                                                                    </h3>
                                                                    <div id="invoicePdf" className="invRightBlockCon">
                                                                        <ul>
                                                                            <li>
                                                                                {fileName && <div> <Image src={pdfIcon} width="15" /> {fileName} &nbsp; &nbsp; <span className="link" onClick={() => this.downloadurl(s3FileLocation)} ><IoMdDownload /> Download</span></div>}
                                                                            </li>
                                                                        </ul>

                                                                    </div>
                                                                </div>
                                                            </Tab>
                                                            <Tab eventKey="comments" title="Comments">
                                                                <div className="invDisplayRightBlock">
                                                                    {/* {(auditTrails.length > 0) && auditTrails[0].comment} */}
                                                                    <ul>
                                                                        {(auditTrails.length > 0) && auditTrails.map(audit => audit.comment != '' && (<li>{audit.comment}</li>))}
                                                                    </ul>
                                                                </div>
                                                            </Tab>
                                                            <Tab eventKey="audittrails" title="Audit Trail">
                                                                <div className="invDisplayRightBlock">
                                                                    <h3>
                                                                        Audit Trail
                                                                    </h3>
                                                                    <div id="audit-details">
                                                                        <Table>
                                                                            <tbody>
                                                                                {auditTrails.length > 0 ? (auditTrails.map((audit, k) =>

                                                                                    < tr key={k}>
                                                                                        <td>
                                                                                            <span className="companyNameRblock">
                                                                                                {audit.userName}
                                                                                            </span>
                                                                                            <br />
                                                                                            {audit.activityDate}
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <span
                                                                                                className="statusRblock submit"
                                                                                            >
                                                                                                {audit.invoiceState.replace(/_/g, ' ')}
                                                                                            </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                )) : null
                                                                                }
                                                                            </tbody>
                                                                        </Table>
                                                                    </div>
                                                                </div>
                                                            </Tab>
                                                        </Tabs>

                                                    </div>
                                                </Col>
                                            </Row>
                                        </Form>
                                    );
                                }}
                            </Formik>

                    }


                </div>
                {/*======  before action confirmation massage popup  =====*/}
                <Modal
                    show={this.state.alertMsg}
                    onHide={this.closeAlertMsg}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <div className="m-auto text-center">
                            {this.state.errorSubmitLoader ? <LoadingSpinner /> : null}
                            {this.state.verifyLoader ? <LoadingSpinner /> : null}



                            {errorReportError && <div className="my-3">{errorReportError}</div>}
                            {verifyError && <div className="my-3">{verifyError}</div>}

                            {((submitStatus == 'error') && !errorReportError && !errorSubmitLoader) &&
                                <h6 className="mb-3">
                                    <span>Do you want to submit this error report ?</span>
                                </h6>
                            }

                            {((submitStatus == 'verify') && !verifyError && !verifyLoader) &&
                                <h6 className="mb-3">
                                    <span>Do you want to submit this ?</span>
                                </h6>
                            }

                        </div>

                        <div className="m-auto text-center">
                            <button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.closeAlertMsg()}>Return</button>
                            {(submitStatus == 'error' && !errorReportError && !errorSubmitLoader) ? <button className="btn btn-primary" onClick={() => this.HandleConfirmReport()}>Confirm</button> : null}
                            {(submitStatus == 'verify' && !verifyError && !verifyLoader) ? <button className="btn btn-primary" onClick={() => this.handleVerifyConfirm()}>Confirm</button> : null}

                        </div>

                    </Modal.Body>
                </Modal>

                {/*======  confirmation popup  ===== */}

                <Modal
                    show={this.state.showConfirMmsg}
                    onHide={this.handleConfirmReviewClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <div className='row'>
                            <div className="text-center col-sm-12">
                                <image src={SuccessIco} />
                            </div>
                        </div>
                        <div className='row'>
                            <div className="text-center my-3 col-sm-12">
                                <h6>Status has been successfully updated</h6>
                                {/* {confirmState == 'error' ? <h6>Error report has been successfully updated</h6> : <h6>Status has been successfully updated</h6>}  */}
                            </div>
                        </div>
                        <div className="text-center">
                            <button
                                onClick={this.handleConfirmReviewClose}
                                className="btn btn-dark but-gray"
                            >
                                Done
                            </button>
                        </div>

                    </Modal.Body>
                </Modal>


                {/*======  block error submit  ===== */}

                <Modal
                    show={this.state.errorChkLength}
                    onHide={this.handleErrorChkAlertClose}
                    className="payOptionPop"
                >
                    <Modal.Body>

                        <div className='row'>
                            <div className="text-center my-3 col-sm-12">
                                <h6>Please select the field to report an error</h6>
                            </div>
                        </div>
                        <div className="text-center">
                            <button
                                onClick={this.handleErrorChkAlertClose}
                                className="btn btn-dark but-gray"
                            >
                                Return
                            </button>
                        </div>

                    </Modal.Body>
                </Modal>

            </div>
        );
    }
}

VerifyInvoice.propTypes = {
    selectedID: PropTypes.number,
    selectedBusinessID: PropTypes.number,
    selectedStatus: PropTypes.string,
    s3FileLocation: PropTypes.string,
    modalCloseReviewInvoice: PropTypes.func,
    getAllList: PropTypes.func,
    fileName: PropTypes.string,
    emailRequest: PropTypes.object,


};

export default VerifyInvoice;
