/* eslint-disable no-unused-vars */
import React, { Component } from 'react';
import { Table, Modal, Image, Col, Row } from 'react-bootstrap';
import VerifyInvoice from './VerifyInvoice';
import ViewInvoice from './viewInvoice';
import axios from './../../../../../shared/eaxios';
import './InvoiceCapture.scss';
import PropTypes from 'prop-types';
import LoadingSpinner from './../../../../../Components/LoadingSpinner/LoadingSpinner';
import EditInvoice from './EditInvoice';
import AddInvoice from './AddInvoice';
import refreshIcon from './../../../../../assets/refreshIcon.png';
import { Base64 } from 'js-base64';
import Pagination from 'react-js-pagination';
let moment = require('moment');




import { 

    INVOICE_STATUS_ALL, 
    INVOICE_STATUS_UNOPENED, 
    INVOICE_STATUS_PENDING_VARIFICATION, 
    INVOICE_STATUS_VERIFIED, 
    INVOICE_STATUS_ERROR_REPORTED

} from './InvoiceStatus';

class InvoiceCaptureMain extends Component {

    state = {
        errorMessge: null,
        lists: [],
        invoiceStatus: null,
        businessID:'',
        listLoader:false,
        selectedID:null,
        selectedStatus:null,
        selectedBusinessID: 0,
        emailAttachID:0,
        icSearchKey: '',
        s3FileLocation:'',
        searchKeyURI:'',
        emailRequest:'',
        fileName:'',
        activePage: 1,
        totalCount: 0,
        itemPerPage: 250,
        prevSix: [],
        slcTimeStamp: null,
        currentMonth: new Date().getMonth() + 1,
        currentYear: new Date().getFullYear(),
        

    
    }

    LastSix = () => {
        let d, m, prevSix = [];
        d = new Date();
        m = d.getMonth() + 1;
        for (let i = 6; i > 0; i--) {
            prevSix.push(new Date().setMonth(m - i));
        }
        prevSix = prevSix.reverse();
        prevSix = prevSix.map((m, i) => (

            <option key={m.toString()} value={m.toString()} >
                {i == 0 ? 'This month' : moment(m).format('MMMM')}
            </option>
        ));
        this.setState({ prevSix });
    };



    handleGetYrMo = (e) => {

        let temp = parseInt(e.target.value);
        let tempMo = new Date(temp).getMonth() + 1;
        let tempYr = new Date(temp).getFullYear();


        this.setState({
            slcTimeStamp: e.target.value,
            currentMonth: tempMo,
            currentYear: tempYr
        }, () => {
            this.getAllList();
        });
    }

    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber });

        this.getAllList(
            pageNumber > 0 ? pageNumber - 1 : 0,
            
        );
    };

    handleChangeItemPerPage = (e) => {
        this.setState({ itemPerPage: e.target.value },
            () => {
                this.getAllList(this.state.activePage > 0 ? this.state.activePage - 1 : 0,);

            });
    }


    static getDerivedStateFromProps(props, state) {
        const { match: {params: { invoiceStatus }}} = props;
        console.log( 'getDerivedStateFromProps ', props );
        if (!state.invoiceStatus) {
            return {
                invoiceStatus
            };
        } else if (state.invoiceStatus && state.invoiceStatus !== invoiceStatus) {
            return {
                invoiceStatus
            };
        }
        return null;
    }

    componentDidUpdate(prevProps) {
        console.log('componentDidUpdate ', prevProps, this.state);
        const { invoiceStatus } = this.state;
        const { match: { params: { invoiceStatus: prevInvoiceStatus } } } = prevProps;
        if (invoiceStatus && prevInvoiceStatus !== invoiceStatus) {
            console.log('Called componentDidUpdate' , this.state.invoiceStatus);
            this.getAllList();
        }
        

    }

    componentDidMount() {

        this.LastSix();
        console.log('componentDidMount --> ', this.state);
        this.getAllList();
    }

    getAllList = (since = 0,) => {
        //let currentMonth = new Date().getMonth() + 1;
        //let currentYear = new Date().getFullYear();
        
        const { 
            invoiceStatus, 
            businessID
        } = this.state;
        const inStatus = 
            invoiceStatus === INVOICE_STATUS_ALL 
                ? '' 
                : invoiceStatus === INVOICE_STATUS_UNOPENED
                    ? 'UNOPENED' 
                    : invoiceStatus === INVOICE_STATUS_PENDING_VARIFICATION
                        ? 'ADMIN_PENDING_VERIFICATION'
                        : invoiceStatus === INVOICE_STATUS_VERIFIED
                            ?'UPLOADED'
                            : invoiceStatus === INVOICE_STATUS_ERROR_REPORTED
                                ? 'ADMIN_VOIDED'
                                :'';
        this.setState({
            listLoader: true,
        }, () => {
            axios.get(
                `invoice/capture/emailList?month=${this.state.currentMonth}&year=${this.state.currentYear}&since=${since}&limit=${this.state.itemPerPage}&state=${inStatus}&prop=&key=${this.state.searchKeyURI}`    
            )
                .then(res => {
                    const totalCount = res.data.total;
                    this.setState({ 
                        lists: res.data.entries, 
                        listLoader: false,
                        totalCount,
                    });
                })
                .catch(e => {
                    console.log(e);
                    let errorMsg = this.displayError(e);
                    this.setState({
                        errorMessge: errorMsg,
                        listLoader: false
                    });

                });

        });
    }
    displayError = (e) => {
        console.log('displayError', e.data.message);
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        this.setState({
            errorMessge: errorMessge,
            errMsg: errorMessge
        });

        setTimeout(() => {
            this.setState({ errorMessge: null });
        }, 5000);
    }

    modalshowReviewInvoice = (
        id, 
        status, 
        bi, 
        emailAttachID, 
        s3FileLocation, 
        emailRequest, 
        fileName
    ) => {
        // console.log('@@@@@@@@@@@@@@@@@@@@@@ status @@@@@@@@@@@@@@@@@@@@@@@@@@@@', 
        //     id,
        //     status,
        //     bi,
        //     emailAttachID,
        //     s3FileLocation,
        //     emailRequest,
        //     fileName);
        this.setState({
            selectedID:id,
            selectedStatus: status,
            showReviewInvoice: true,
            selectedBusinessID:bi,
            emailAttachID: emailAttachID,
            s3FileLocation,
            emailRequest,
            fileName
        });
    }

    modalCloseReviewInvoice = () => {
        this.setState({
            showReviewInvoice: false,
        });
    }
    handelOnchange = (e) => {

        this.setState(
            {
                searchValue: e.target.value,
            },
            () => {
                console.log('this.state.searchValue', this.state.searchValue);
            }
        );

    }

    handlekeyPress = (e) => {

        if (e.key === 'Enter') {
            let searchKeyURI = encodeURI(this.state.searchValue);
            console.log('searchKeyURI', searchKeyURI);
            this.setState({
                searchKeyURI
            }, () => this.getAllList());
            //this.getAllList();
        }

    }

    handelSearch = () => {
        // console.log('this.state.searchValue search', this.state.searchValue);
        if (this.state.searchValue != '') {
            let searchKeyURI = encodeURI(this.state.searchValue);
            this.setState({
                searchKeyURI
            }, () => this.getAllList());
            //this.getAllList();
        }

    }
    resetSearch = () =>{
        this.setState({
            searchKeyURI:'',
            searchValue:''
        }, () => this.getAllList());
    }

    render(){
        const { 
            errorMessge,
            lists,
            //invoiceStatus,
            listLoader,
            selectedStatus,
            prevSix
        } = this.state;
        return(
            <div className="invCaptureInnerBody">
                <div className="accRecSerOuter">
                    <div className="row show-grid m-b-30">
                        <div className="col-sm-6">
                            <div className="topSearchInvoice">
                                <div className="form-inline">
                                    <input
                                       
                                        onChange={this.handelOnchange}
                                        onKeyPress={this.handlekeyPress}
                                        value={this.state.searchValue} 
                                        name="Search"
                                        type="text"
                                        className="form-control searchMain"
                                        placeholder="Search.."

                                    />
                                    <div className="form-group m-l-20">
                                        <button
                                            type="button"
                                            className="btn search-btn"
                                            onClick={() => this.handelSearch()}
                                        >
                                                    Search
                                        </button>

                                    </div>
                                    <div className="form-group m-l-20">
                                        <span to="#" className="refreshIconAc cursor-pointer" onClick={() => this.resetSearch()}>
                                            <Image src={refreshIcon} />
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <span className={'float-right mt-1 bg-img-none'}>
                                <select className='px-2 py-1' onChange={(e) => this.handleGetYrMo(e)} value={this.state.slcTimeStamp}>
                                    {prevSix}
                                </select>
                            </span>
                            
                        </div>
                        {/* <Col md={6} className="text-right">
                                    <Button
                                        onClick={this.createInvoiceHandler}
                                        className="btn btn-primary"
                                    >
                                        <span className="glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span> Create Invoice
                                    </Button>
                                    <Button
                                        className="btn btn-primary ml-1"
                                        onClick={this.handleUploadShow}>
                                        <span className="glyphicon glyphicon glyphicon-open" aria-hidden="true"></span> Upload Invoice
                                    </Button>
                                </Col> */}
                    </div>
                </div>
                <div className="boxBg">
                    <Table responsive hover>
                        <thead className="theaderBg">
                            <tr>
                                {/* <th>
                                    <label className="customCkBox">
                                        <input type="checkbox" />
                                        <span className="checkmark" />
                                    </label>
                                </th> */}
                                <th>Status </th>
                                <th>Name</th>
                                <th>Type</th>
                                <th className="">Modified By</th>
                                <th className="">Business Name</th>
                                <th className="text-right">Date Received</th>
                                <th className="text-right">Date Modified</th>
                            </tr>
                        </thead>
                        <tbody>
                            {listLoader ? (<tr>
                                <td colSpan={12}>
                                    <LoadingSpinner />
                                </td>
                            </tr>) :

                                lists.length > 0 ? (
                                    lists.map(list => (
                                        <tr key={list.id} onClick={() => this.modalshowReviewInvoice(

                                            list.invoiceId, 
                                            list.invoiceState, 
                                            list.emailRequest.companyID, 
                                            list.id, 
                                            list.s3FileLocation,
                                            list.emailRequest,
                                            list.fileName
                                        )}>
                                            {/* <td>
                                                <label className="customCkBox">
                                                    <input type="checkbox" />
                                                    <span className="checkmark" />
                                                </label>
                                            </td> */}

                                            <td >
                                                
                                                {
                                                    list.invoiceState === 'ADMIN_PENDING_VERIFICATION' 
                                                        ? <span className="statusInfo uploaded">PENDING</span> 
                                                        : list.invoiceState === 'UPLOADED' ? <span className="statusInfo reviewed">VERIFIED</span>
                                                            : list.invoiceState === 'ADMIN_VOIDED' ? <span className="statusInfo warning">ERROR REPORTED</span>
                                                                : list.invoiceState === 'SCANNED' ? <span className="statusInfo reviewed">VERIFIED</span>
                                                                    : <span className="statusInfo unopened">{list.invoiceState}</span>
                                                }
                                            </td>
                                            <td>
                                                {list.fileName}
                                            </td>
                                            <td>
                                                Invoice
                                            </td>
                                            <td>
                                                {list.userName}
                                            </td>
                                            <td>
                                                {list.emailRequest.businessName}
                                            </td>
                                            <td className="text-right">
                                                {list.createdDate}
                                            </td>
                                            <td className="text-right">
                                                {list.modifiedDate}
                                            </td>
                                        </tr>
                                    ))

                                ) : errorMessge ? null : (
                                    <tr>
                                        <td colSpan={12}>
                                            <p className="text-center">No records found</p>
                                        </td>
                                    </tr>
                                )
                    
                            }
                
                        </tbody>
                    </Table>
                </div>
                {this.state.totalCount ? (
                    <Row className="px-3">
                        <Col md={4} className="d-flex flex-row mt-20">
                            <span className="mr-2 mt-2 font-weight-500">Items per page</span>
                            <select
                                id={this.state.itemPerPage}
                                className="form-control truncatefloat-left w-90"
                                onChange={this.handleChangeItemPerPage}
                                value={this.state.itemPerPage}>
                                <option value='50'>50</option>
                                <option value='100'>100</option>
                                <option value='150'>150</option>
                                <option value='200'>200</option>
                                <option value='250'>250</option>

                            </select>
                        </Col>
                        <Col md={8}>
                            <div className="paginationOuter text-right">
                                <Pagination
                                    activePage={this.state.activePage}
                                    itemsCountPerPage={this.state.itemPerPage}
                                    totalItemsCount={this.state.totalCount}
                                    onChange={this.handlePageChange}
                                />
                            </div>
                        </Col>
                    </Row>
                ) : null}

                {/*====FOR REVIEW INVOICE====*/}
                <Modal
                    show={this.state.showReviewInvoice}
                    onHide={() => this.modalCloseReviewInvoice()}
                    className="right full noPadding"
                >
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body closeButton>
                        {selectedStatus === 'ADMIN_PENDING_VERIFICATION' && <VerifyInvoice 
                            modalCloseReviewInvoice={() => this.modalCloseReviewInvoice()} 
                            selectedID={this.state.selectedID} 
                            selectedStatus={this.state.selectedStatus} 
                            selectedBusinessID={this.state.selectedBusinessID} 
                            s3FileLocation={this.state.s3FileLocation} 
                            getAllList={this.getAllList} 
                            emailAttachID={this.state.emailAttachID}
                            emailRequest={this.state.emailRequest}
                            fileName={this.state.fileName}
                        />}
                        {selectedStatus === 'UNOPENED' && <AddInvoice 
                            modalCloseReviewInvoice={() => this.modalCloseReviewInvoice()} 
                            selectedID={this.state.selectedID} 
                            selectedStatus={this.state.selectedStatus} 
                            selectedBusinessID={this.state.selectedBusinessID} 
                            s3FileLocation={this.state.s3FileLocation} 
                            getAllList={this.getAllList} 
                            emailAttachID={this.state.emailAttachID}
                            emailRequest={this.state.emailRequest}
                            fileName={this.state.fileName}
                        />}
                        {selectedStatus === 'ADMIN_VOIDED' && <EditInvoice 
                            modalCloseReviewInvoice={() => this.modalCloseReviewInvoice()} 
                            selectedID={this.state.selectedID} 
                            selectedStatus={this.state.selectedStatus} 
                            selectedBusinessID={this.state.selectedBusinessID} 
                            s3FileLocation={this.state.s3FileLocation} 
                            getAllList={this.getAllList} 
                            emailAttachID={this.state.emailAttachID}
                            emailRequest={this.state.emailRequest}
                        />}

                        {(selectedStatus === 'UPLOADED' || selectedStatus === 'REVIEWED'|| selectedStatus === 'SCANNED') &&( <ViewInvoice
                            modalCloseReviewInvoice={() => this.modalCloseReviewInvoice()}
                            selectedID={this.state.selectedID}
                            selectedStatus={this.state.selectedStatus}
                            selectedBusinessID={this.state.selectedBusinessID}
                            s3FileLocation={this.state.s3FileLocation}
                            getAllList={this.getAllList}
                            emailAttachID={this.state.emailAttachID}
                            emailRequest={this.state.emailRequest}
                            fileName={this.state.fileName}
                        />)}
                        
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}

InvoiceCaptureMain.propTypes = {
    match: PropTypes.object,
    invoiceStatus: PropTypes.string
};

export default InvoiceCaptureMain;
