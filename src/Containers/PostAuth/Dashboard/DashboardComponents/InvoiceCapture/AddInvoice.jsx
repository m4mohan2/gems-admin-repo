/* eslint-disable */
import { ErrorMessage, Field, FieldArray, Form, Formik } from 'formik';
import PropTypes from 'prop-types';
import React, { Component, Fragment } from 'react';
import {
    Button,
    Col,
    Collapse,
    FormControl,
    FormGroup,
    Image,
    Modal,
    Row,
    Tab,
    Table,
    Tabs
} from 'react-bootstrap';
import DatePicker from 'react-date-picker';
import { IoMdDownload } from 'react-icons/io';
import { Link } from 'react-router-dom';
import * as Yup from 'yup';
import crossSmIcon from './../../../../../assets/crossSmIcon.png';
//import { pdfIcon as FaFilePdf } from 'react-icons/fa';
import pdfIcon from './../../../../../assets/pdficon.png';
import SuccessIco from './../../../../../assets/success-ico.png';
import LoadingSpinner from './../../../../../Components/LoadingSpinner/LoadingSpinner';
import { CountryService } from './../../../../../services/country.service';
import { UserBusiness } from './../../../../../services/userBusiness.service';
import axios from './../../../../../shared/eaxios';
import {formatDate} from './../../../../../shared/formatDate';
import AddBusinessVendors from './../Business/BusinessVendors/AddBusinessVendors';
import AdditemsModal from './AdditemsModal';
import './EditInvoice.scss';
import './InvoiceCapture.scss';
import './VerifyInvoice.scss';


const initialValues = {
    invoiceNumber: '',
    invoiceDuedate: '',
    invoiceDate: '',
    poNumber: '',
    terms: '',
    businessName: '',
    businessAddress1: '',
    businessPhone: '',
    vendorName: '',
    vendorAddress: '',
    vendorPhoneNo: '',
    subTotal: '0.00',
    amountDue: '0.00',
    totalTax: '0.00',
    comment: '',
    taxType: 'TAX_LINE', // TAX_LINE & TAX_INVOICE 
    // EXP
    invoiceItems: [
        {
            name: '',
            description: '',
            quantity: 1,
            unitPrice: parseFloat(0).toFixed(2),
            subtotal: 0.00,
            taxPercent: parseFloat(0).toFixed(2),
            taxName: 'Tax',
            //isTaxable: false, 
            itemMaster: null,
            showflag: true,
            expencedisable: false,
            unitDisFlag: false,
            itemRefType: 'ITEM',
            isTaxable: true
        }
    ],
    emailAttachment: {},
    errorObj: {},
    businessData: null,
    businessDataError: null,
    businessLoader: false,
    auditTrailNote: '',
    //taxOption: '',
    // taxLineItems: false,
    // taxInvoice: false,

};

const addInvoiceSchema = Yup.object().shape({
    invoiceNumber: Yup
        .string()
        .required('Please enter invoice number'),
    invoiceDuedate: Yup
        .string('Please enter invoice due date')
        .typeError('Please enter invoice due date')
        .required('Please enter invoice due date'),
    invoiceDate: Yup
        .string('Please enter invoice date')
        .typeError('Please enter invoice date')
        .required('Please enter invoice date'),
    poNumber: Yup
        .string(),
    //.required('Please enter PO number'),
    terms: Yup
        .string(),
    //.required('Please select terms'),
    businessName: Yup
        .string(),
    businessAddress1: Yup
        .string(),
    businessPhone: Yup
        .string(),
    vendorName: Yup
        .string()
        .required('Please select vendor name'),
    vendorAddress: Yup
        .string(),
    // .required('Please enter vendor address'),
    vendorPhoneNo: Yup
        .string(),
    comment: Yup
        .string(),
    auditTrailNote: Yup
        .string(),
    // .required('Please enter vendor phone no.'),
    invoiceItems: Yup.array().of(Yup.object().shape({
        name: Yup
            .string()
            .required("Please enter item name"),
        description: Yup
            .string(),
        //.required("Please enter description"),
        quantity: Yup.number()
            .required("Please enter quantity")
            .typeError('Quantity should be a number')
            .max(99999, "Quantity cannot be more than 5 digits.")
            .moreThan(0, 'Quantity should be greater than 0')
            // .test(
            //     'quantity',
            //     'Input quantity without leading zeros',
            //     (value) => {
            //         if (value) {
            //             let isValid = false;
            //             if (/^(0|[1-9][0-9]?)$/.test(value)) {
            //                 isValid = true;
            //             }
            //             return isValid;
            //         }
            //         return true;
            //     }
            // )
            ,


        unitPrice: Yup.string()
            .required("Please enter unit price")
            .typeError('Unit price should be a number')
            //.moreThan(0, 'Unit Price should be greater than 0.00')
            //.max(99999, "maximum unit price can't more than 5 digit")

            .test(//5 digit with decimal
                'unitPrice',
                'Maximum unit price cannot be more than 5 digits with two decimals.',
                function (value) {
                    if (value) {
                        let isValid = false;
                        if (/^[+]?[0-9]\d{0,4}(\.\d{1,2})?%?$/.test(value)) {
                            isValid = true;
                        }
                        return isValid;
                    }
                    return true;
                }
            ).test(//upto two decimal places
                'unitPrice',
                'Unit price should be decimal places',
                function (value) {
                    if (value) {
                        let isValid = false;
                        if (/^[0-9]*\.[0-9][0-9]$/.test(value)) {
                        //if (/^\d*(\.\d{0,2})?$/.test(value)) {
                            isValid = true;
                        }
                        return isValid;
                    }
                    return true;
                }
            ).test(//can not start with 0 but 0.01 valid
                'unitPrice',
                'Unit price can not start with 0',
                function (value) {
                    if (value) {
                        let isValid = false;
                        if (/^(([1-9][0-9]*)|((0|[1-9]\d*)(\.\d+)))$/.test(value)) {
                            isValid = true;
                        }
                        return isValid;
                    }
                    return true;
                }
            ).test(//greater than 0.00
                'unitPrice',
                'Unit Price should be greater than 0.00',
                function (value) {
                    if (value) {
                        let isValid = false;
                        if (/^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$/.test(value)) {
                            isValid = true;
                        }
                        return isValid;
                    }
                    return true;
                }

            ),

        taxPercent: Yup.string()
            //.max(100, "maximum tax percent can't more than 100")
            .typeError('Tax rate should be a number')
            //.moreThan(0, 'Tax rate should be greater than 0.00')
            .test(
                'taxPercent',
                'Tax rate upto two decimal places or invalid input',
                (value) => {
                    
                    if (value) {
                        let isValid = false;
                        //if (/^\d*(\.\d{0,2})?$/.test(value)) {
                        if (/^(?:100|\d{1,2})(?:\.\d{1,2})?$/.test(value)) {
                            isValid = true;
                        }

                        return isValid;
                    }
                    return true;
                }
        )
        .test(//can not start with 0 but 0.01 valid
            'taxPercent',
            'Tax rate can not start with 0 but you can put 0.00',
            function (value) {
                if (value) {
                    let isValid = false;
                    if (/^(([1-9][0-9]*)|((0|[1-9]\d*)(\.\d+)))$/.test(value)) {
                        isValid = true;
                    }
                    return isValid;
                }
                return true;
            }
        )
        .test(//can not start with 0 but 0.01 valid
            'taxPercent',
            // 'Tax rate cannot be greater than 100 or invalid input',
            'Tax rate cannot be greater than 100',
            function (value) {
              
                if (value) {
                    let isValid = false;
                    if (parseFloat(value) <= parseFloat(100.00)) {
                        isValid = true;
                    }
                    return isValid;
                }
                 return true;
            }
        )
        ,
        taxName: Yup
            .string()
        .required("Please enter tax name"),

        subtotal: Yup.number().required('Please enter total amount')
    })),
    subTotal: Yup
        .number()
        .required('Please enter subtotal')
        .test(
            'subTotal',
            'Subtotal upto two decimal places',
            function (value) {
                if (value) {
                    let isValid = false;
                    if (/^\d*(\.\d{0,2})?$/.test(value)) {
                        isValid = true;
                    }

                    return isValid;
                }
                return true;
            }
        ),
    totalTax: Yup
        .string()
        // .required('Please enter total tax')
        .typeError('Total tax should be number')
        .test(//upto two decimal places
            'totalTax',
            'Total tax upto two decimal places',
            function (value) {
                if (value) {
                    let isValid = false;
                    if (/^\d*(\.\d{0,2})?$/.test(value)) {
                        isValid = true;
                    }
                    return isValid;
                }
                return true;
            }
        )
        .test(//5 digit with decimal
            'totalTax',
            'Maximum total tax can not more than 5 digit with two decimal',
            function (value) {
                if (value) {
                    let isValid = false;
                    if (/^[+]?[0-9]\d{0,4}(\.\d{1,2})?%?$/.test(value)) {
                        isValid = true;
                    }
                    return isValid;
                }
                return true;
            }
        ).test(//can not start with 0 but 0.01 valid
            'totalTax',
            'Total tax can not start with 0, you can put 0.00',
            function (value) {
                if (value) {
                    let isValid = false;
                    if (/^(([1-9][0-9]*)|((0|[1-9]\d*)(\.\d+)))$/.test(value)) {
                        isValid = true;
                    }
                    return isValid;
                }
                return true;
            }
    ),
    amountDue: Yup
        .number()
        .required('Please enter due amount')
        .test(
            'amountDue',
            'Due amount upto two decimal places',
            function (value) {
                if (value) {
                    let isValid = false;
                    if (/^\d*(\.\d{0,2})?$/.test(value)) {
                        isValid = true;
                    }

                    return isValid;
                }
                return true;
            }
        )
});

const initialValuestrems = {
    termName: '',
    netDueDays: '',
    discountPercent: '',
    discountApplicableDays: ''
};
const addtremsSchema = Yup.object().shape({
    termName: Yup
        .string()
        .required('Please enter  term name'),
    netDueDays: Yup
        .number(),
    //.required('Please enter net due days'),
    discountPercent: Yup
        .number()
        // .required('Please enter discount percent')
        .test(
            'discountPercent',
            'Discount percent upto two decimal places',
            function (value) {
                if (value) {
                    let isValid = false;
                    if (/^\d*(\.\d{0,2})?$/.test(value)) {
                        isValid = true;
                    }

                    return isValid;
                }
                return true;
            }
        ),
    discountApplicableDays: Yup
        .string()
    //.required('Please enter  discount applicable days')
});




class AddInvoice extends Component {

    state = {
        errorItemMessage: "",
        errorItemMessageModal: false,
        additemConfirm: false,
        showAddVendor: false,
        showAdditemsModal: false,
        Additemsloder: false,
        showCreateTrmsModal: false,
        errorMessage: '',
        showinvoiceWarningModal: false,
        succesSubmitModal: false,
        loader: false,
        vendorLoader: false,
        termLoader: false,
        invoiceDetails: true,
        customerVendor: true,
        lineItems: true,
        invDetails: true,
        invAdditionalNote: true,
        businessDetails: {},
        vendorLists: [],
        termLists: [],
        emailAttachmentId: '',
        invoiceId: '',
        auditTrails: [],
        tremserrorMessage: '',
        trmsloader: false,
        addtrmsConfirm: false,
        itemlist: [],
        tempitemlist: [],
        key: 'expense',
        Accountslist: [],
        tempAccountslist: [],
        itemsdetails: {},
        showedititemsModal: false,
        edititemConfirm: false,
        edititemsloder: false,
        erroreditItemMessage: '',
        erroreditItemMessagemodal: false,
        itemid: '',
        newaddItemshow: true,
        seletedItemName: '',
        expeTitalId: '',
        selectedInvoiceDate: null,
        taxOption: false,
        isTaxableState: true

    }
    constructor(props) {
        super(props);
        this.getAccountsref = React.createRef();
        this.getitemref = React.createRef();
        this.getexpenceref = React.createRef()
    }

    




    cal = (nun1, num2) => {
        // console.log(nun1)
        // console.log(num2)
        if (num2 !== '') {
            const val = (parseFloat(nun1) + parseFloat(num2));
            // console.log('1')
            return parseFloat(val).toFixed(2);
        } else {
            // console.log('2')
            return parseFloat(nun1).toFixed(2);
        }

    }

    taxTypeCalculation = ({
        invoiceItems
    }, taxType, setFieldValue) => {
        setFieldValue('totalTax', (0.00).toFixed(2));
        if (taxType === 'TAX_LINE') {
            this.setState({
                isTaxableState: true
            })
            console.log(taxType, ' ### 1 invoiceItems### ', invoiceItems)
            const taxLineTotalItems = invoiceItems.map(invoice => this.getTotal(invoice.unitPrice, invoice.quantity, 0));
            console.log(taxType, ' ### 1 taxLineTotalItems ### ', taxLineTotalItems)
            const subTotal = taxLineTotalItems.reduce((tot, value) => (tot + value));
            setFieldValue('subTotal', subTotal.toFixed(2));

            const taxLineTotTaxlItems = invoiceItems.map(invoice => this.getTotalTax(invoice.unitPrice, invoice.quantity, invoice.taxPercent));
            const totalTax = taxLineTotTaxlItems.reduce((tot, value) => (tot + value));
            setFieldValue('totalTax', totalTax.toFixed(2));
           
            const amountDue = subTotal + totalTax;
            setFieldValue('amountDue', amountDue.toFixed(2));

        } else if (taxType === 'TAX_INVOICE') {
            this.setState({
                isTaxableState: false
            })
            console.log(taxType, ' ### 2 invoiceItems### ', invoiceItems);
            const taxInvoiceTotalItems = invoiceItems.map(invoice => this.getTotal(invoice.unitPrice, invoice.quantity, 0));
            console.log(taxType, ' ### 2 taxInvoiceTotalItems### ', taxInvoiceTotalItems);
            const subTotal = taxInvoiceTotalItems.reduce((tot, value) => (tot + value));
            setFieldValue('subTotal', subTotal.toFixed(2));
            // set amount due
            setFieldValue('amountDue', this.cal(subTotal.toFixed(2), 0.00));

        }
    }
    

    /* Added reuseable function */
    getTotal = (unitPrice, qty, taxPercent) => {
        unitPrice = isFinite(unitPrice) ? unitPrice : 0.00;
        qty = isFinite(qty) ? qty : 0;
        taxPercent = isFinite(taxPercent) ? taxPercent : 0;
        
        const total = parseFloat(unitPrice * qty);
        let taxAmount = 0.00;
        if (taxPercent > 0) {
            taxAmount = this.getTaxAmount(total, taxPercent)
        }

        return (total + taxAmount);
    }
    /* */
    getTaxAmount = (amount, percent) => {
        amount = isFinite(amount) ? amount : 0.00;
        percent = isFinite(percent) ? percent : 0;

        return (percent / 100) * amount;
    }

    /* */
    getTotalTax = (unitPrice, qty, taxPercent) =>{
        let totaltax=0.00;
        const tempTotal = parseFloat(unitPrice * qty);
        const percentCalc = (taxPercent / 100) * tempTotal;
        return totaltax = parseFloat(totaltax) + parseFloat(percentCalc);

    }


    settotalamount = (values, index, tag, value) => {
        const arry = values.invoiceItems
        const totalTax = values.totalTax === '' ? 0.00 : values.totalTax;
        let totalamout = 0
        if (tag === 'Quantity') {
            arry[index].quantity = isNaN(value) ? 0 : value
        } else if (tag === 'Unit price') {
            arry[index].unitPrice = isNaN(value) ? 0.00 : value
        } else if (tag === 'Tax Rate') {
            arry[index].taxPercent = isNaN(value) ? 0.00 : value
        }

        arry.map((inv, index) => {
            const tempTotal = parseFloat(inv.unitPrice * inv.quantity);
            const percentCalc = (inv.taxPercent / 100) * tempTotal;
            console.log('--------------------percentCalc------------------',percentCalc);
            //const subtotal = (inv.unitPrice * inv.quantity).toFixed(2);

            const subtotal = (parseFloat(tempTotal) + parseFloat(percentCalc)).toFixed(2);
            // const subtotal = (parseFloat(tempTotal)).toFixed(2);

            totalamout = parseFloat(totalamout) + parseFloat(subtotal);
        });
        return (parseFloat(totalTax) + parseFloat(totalamout)).toFixed(2);
    }


    showAdditemsModal = () => {
        this.setState({ showAdditemsModal: true })
    }

    hideAdditemsModal = () => {
        this.setState({ showAdditemsModal: false })
    }
    addnewitemhandleSubmit = (param) => {
        console.log(param)
        this.setState({
            Additemsloder: true
        }, () => {
            axios.post(`/autofy/saveItem/${this.props.selectedBusinessID}`, param)
                .then(res => {
                    this.getItemlist();
                    this.setState({
                        Additemsloder: false,
                        showAdditemsModal: false,
                        additemConfirm: true
                    })

                })
                .catch(err => {
                    let errorItemMessage = this.handelError(err);
                    this.setState({
                        errorItemMessage,
                        showAdditemsModal: false,
                        errorItemMessageModal: true,
                        Additemsloder: false

                    })
                }
                )

        })
        console.log(param)


    }
    closeadditemsConfirmModal = () => {
        this.setState({ additemConfirm: false })
    }
    hideAdditemsWorningModal = () => {
        this.setState({ errorItemMessageModal: false })
    }

    displayError = (err) => {
        let errorMessage = '';
        try {
            errorMessage = err.data.message ? err.data.message : err.data.error_description;
        } catch (err) {
            errorMessage = "No records found."
        }
        return errorMessage;
    }

    handelError = (err) => {
        let errorMessage = '';
        try {
            errorMessage = err.data.message ? err.data.message : err.data.error_description;
        } catch (err) {
            errorMessage = "No records found."
        }
        return errorMessage;
    }

    getDetailsList = () => {
        this.setState({
            detailsLoader: true
        });


        axios
            .get(
                `invoice/${this.props.selectedID}/${this.props.selectedBusinessID}`
            )
            .then(res => {
                let errorObj = JSON.parse(res.data.error.errorJson);
                //console.log('***************res******************', res.data);
                const getinvoiceDetails = res.data;
                initialValues['invoiceNumber'] = getinvoiceDetails.invoiceNumber;

                // initialValues['invoiceDate'] = new Date(getinvoiceDetails.invoiceDate);
                // initialValues['invoiceDuedate'] = new Date(getinvoiceDetails.invoiceDueDate);

                initialValues['invoiceDate'] = formatDate(getinvoiceDetails.invoiceDate);
                initialValues['invoiceDuedate'] = formatDate(getinvoiceDetails.invoiceDueDate);

                initialValues['poNumber'] = getinvoiceDetails.invoicePO;
                initialValues['terms'] = getinvoiceDetails.termRequest.id ? getinvoiceDetails.termRequest.id : ''
                initialValues['subTotal'] = getinvoiceDetails.subTotal.toFixed(2);
                initialValues['vendorName'] = getinvoiceDetails.customerVendorId;
                initialValues['amountDue'] = getinvoiceDetails.totalAmount.toFixed(2);
                initialValues['totalTax'] = getinvoiceDetails.totalTax.toFixed(2);




                getinvoiceDetails.invoiceItems.map((item) => {
                    delete item['id']
                    item['showflag'] = true;
                    item['expencedisable'] = false;
                    console.log('arrty', item)
                })
                initialValues['invoiceItems'] = getinvoiceDetails.invoiceItems;
                initialValues['comment'] = getinvoiceDetails.comment;
                initialValues['businessName'] = getinvoiceDetails.businessName;
                initialValues['businessAddress1'] = getinvoiceDetails.businessAddress1;
                initialValues['businessPhone'] = getinvoiceDetails.businessPhone;
                const vendors = this.state.vendorLists;
                const vendor = vendors.filter(v => {
                    return Number(v.id) === Number(initialValues['vendorName']);
                });
                initialValues['vendorAddress'] = vendor[0] && vendor[0].address;
                initialValues['vendorPhoneNo'] = vendor[0] && vendor[0].phoneNumber;
                const auditTrails = getinvoiceDetails.auditTrails;
                const emailAttachment = getinvoiceDetails.emailAttachment;

                this.setState({
                    emailAttachment,
                    auditTrails,
                    emailAttachmentId: getinvoiceDetails.emailAttachmentId,
                    //loader: false,

                });
                //initialerReport['invoiceItems'] = res.data.invoiceItems;
                this.setState({
                    detailsLoader: false,
                    detailsList: res.data,
                    errorObj: errorObj,

                }, () => console.log('######################### errorObj ###########', this.state.errorObj));


            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    detailsError: errorMsg,
                    detailsLoader: false

                });

                setTimeout(() => {
                    this.setState({ detailsError: null });
                }, 5000);

            });
    }

    deleteallrow = (value, setFieldValue) => {

        console.log(value)
        setFieldValue("invoiceItems", [])
        // setFieldValue("invoiceItems", [{ id: null, name: '', description: '', quantity: 0, unitPrice: 0, subtotal: 0, isTaxable: false, tax: null, itemMaster: '' }])
        //setFieldValue("invoiceItems", [{ id: null, name: '', description: '', quantity: 0, unitPrice: 0, subtotal: 0, isTaxable: false, tax: null, itemMaster: '', showflag: true, expencedisable: false }])
        setFieldValue("subTotal", '0.00')
        setFieldValue("totalTax", '0.00')
        setFieldValue("amountDue", '0.00')
        //setFieldValue("amountDue", this.cal(0.00, value.totalTax))

    }

    handleSubmit = (values) => {
        console.log('------------- values ---------------', values);
        values.invoiceItems = values.invoiceItems.map((item, i) => { item.isTaxable = this.state.isTaxableState; return item });
        console.log('------------- values.invoiceItems ---------------', values.invoiceItems);
        let param = {
            "emailAttachmentId": this.props.emailAttachID,
            //'emailAttachmentId': 37761,
            'customerVendorId': values.vendorName,
            'invoiceNumber': values.invoiceNumber,
            'invoicePO': values.poNumber,
            'invoiceTerm': values.terms,
            "totalTax": values.totalTax,
            "totalAmount": values.amountDue,
            "subTotal": values.subTotal,
            'invoiceDate': formatDate(values.invoiceDate),
            'invoiceDueDate': formatDate(values.invoiceDuedate),
            'invoiceState': 'ADMIN_PENDING_VERIFICATION',
            'termId': values.terms,
            'invoiceItems': values.invoiceItems,
            'comment': values.comment,
            'auditTrailNote': values.auditTrailNote,
        };
        param.isLineItemTaxEnabled = this.state.isTaxableState;
        //param.isTaxable = values.taxType === 'TAX_LINE' ? true : false ;

        console.log('invoice/capture/create/${this.props.selectedBusinessID}=======', param);

        axios.post(`invoice/capture/create/${this.props.selectedBusinessID}`, param)
            .then(res => {
                this.showsuccesSubmitModal();
                this.props.getAllList();
                //this.getDetailsList();
            })
            .catch(err => {
                let errorMessage = this.handelError(err);
                this.setState({
                    errorMessage,
                    succesSubmitModal: false,
                    showinvoiceWarningModal: true

                }, () => this.getDetailsList());
            });


    }

    getBusiness = () => {
        this.setState({
            loader: true
        }, (() => {
            const userBusiness = new UserBusiness();
            userBusiness.getBusinessDetails()
                .then(bDetails => {
                    const businessDetails = bDetails;
                    ///console.log(bDetails)
                    initialValues['customerPhNo'] = bDetails.businessPhone;
                    this.setState({
                        businessDetails
                    }, () => {

                        this.getSCCName(
                            bDetails.businessName,
                            bDetails.businessAddress1,
                            bDetails.businessCountryCode,
                            bDetails.businessStateCode,
                            bDetails.businessCityId,
                            bDetails.businessZipcode
                        );

                    });


                });
        }));

    }

    getSCCName = (businessName, Address1, countryReq, stateReq, cityReq, businessZipcode) => {

        const countryService = new CountryService();
        countryService.getData().then(({ countries, states, cities }) => { //countries, states or cities
            let countriesList = countries;
            let citiesList = cities;
            let statesList = states;
            let stateName, countryName, cityName;

            for (const country of countriesList) {
                if ((country.countryCode) === (countryReq)) countryName = country.countryName;
            }

            for (const st of statesList) {
                if ((st.stateCode) === (stateReq)) stateName = st.stateName;
            }

            for (const city of citiesList) {
                if ((city.id) === (cityReq)) cityName = city.cityName;
            }
            const { businessDetails } = this.state;
            const Country = (businessDetails.businessCountryCode === 'US' || businessDetails.businessCountryCode === 'United States') ? 'USA' :
                businessDetails.businessCountryCode === 'CANADA' ? 'CANADA' : null;
            initialValues['customerName'] = businessName;
            initialValues['customerAddress'] = Address1 + ' ' + cityName + ' ' + businessDetails.businessStateCode + ' ' +
                businessDetails.businessZipcode + Country;

            this.setState({
                loader: false
            });

        });
    }

    fetchVendorList = () => {
        this.setState({
            vendorLoader: true
        }, () => {
            axios
                .get(
                    `customerVendor/vendorlist/${this.props.selectedBusinessID}?since=0&limit=20000000&key=&direction=&prop=&businessType=Fintainium Customer`
                )
                .then(res => {
                    const vendorLists = res.data.entries;
                    //console.log('vendorLists = res.data.entries', vendorLists);
                    const vendor = vendorLists.filter(v => {
                        return Number(v.id) === Number(initialValues['vendorName']);
                    });
                    initialValues['vendorAddress'] = vendor[0] && vendor[0].address;
                    initialValues['vendorPhoneNo'] = vendor[0] && vendor[0].phoneNumber;
                    this.setState({
                        vendorLists,
                        vendorLoader: false
                    });

                })
                .catch();
        });

    };

    fetchterm = () => {
        this.setState({
            termLoader: true
        }, () => {
            axios
                .get(
                    `/term/list/${this.props.selectedBusinessID}`
                )
                .then(res => {
                    //console.log('termLists ', res);
                    const termLists = res.data;
                    this.setState({
                        termLists,
                        termLoader: false
                    });

                })
                .catch();
        });

    };

    // handleChange = (e) => {
    //     //console.log(e)
    // }

    getinvoiceDetails() {
        // console.log('getinvoiceDetails', this.state.businessDetails)
        this.setState({
            loader: true
        }, () => {
            axios
                .get(
                    `/invoice/${this.state.invoiceId}`
                )
                .then(res => {
                    const getinvoiceDetails = res.data;
                    initialValues['invoiceNumber'] = getinvoiceDetails.invoiceNumber;
                    initialValues['invoiceDate'] = new Date(getinvoiceDetails.invoiceDate);
                    initialValues['invoiceDuedate'] = new Date(getinvoiceDetails.invoiceDueDate);
                    initialValues['poNumber'] = getinvoiceDetails.invoicePO;
                    initialValues['terms'] = getinvoiceDetails.termRequest && getinvoiceDetails.termRequest.id ? getinvoiceDetails.termRequest.id : ''
                    initialValues['subTotal'] = getinvoiceDetails.totalAmount;
                    initialValues['vendorName'] = getinvoiceDetails.customerVendorId;
                    initialValues['amountDue'] = getinvoiceDetails.totalAmount + getinvoiceDetails.totalTax;
                    initialValues['totalTax'] = getinvoiceDetails.totalTax;
                    initialValues['invoiceItems'] = getinvoiceDetails.invoiceItems;
                    initialValues['comment'] = getinvoiceDetails.comment;
                    const vendors = this.state.vendorLists;
                    const vendor = vendors.filter(v => {
                        return Number(v.id) === Number(initialValues['vendorName']);
                    });
                    initialValues['vendorAddress'] = vendor[0] && vendor[0].address;
                    initialValues['vendorPhoneNo'] = vendor[0] && vendor[0].phoneNumber;
                    const auditTrails = getinvoiceDetails.auditTrails;
                    const emailAttachment = getinvoiceDetails.emailAttachment;

                    this.setState({
                        emailAttachment,
                        auditTrails,
                        emailAttachmentId: getinvoiceDetails.emailAttachmentId,
                        loader: false,

                    });

                })
                .catch();
        });

    }

    // handleChange = (e, field) => {
    //     this.setState({
    //         [field]: e.target.value
    //     });
    // };

    populatevendorvendorAddress = e => {
        const vendorId = e.target.value;
        const vendors = this.state.vendorLists;
        const vendor = vendors.filter(v => {
            return Number(v.id) === Number(vendorId);
        });
        return vendor[0] && vendor[0].address

    };

    populatevendorvendorvendorName = e => {
        const vendorId = e.target.value;
        const vendors = this.state.vendorLists;
        const vendor = vendors.filter(v => {
            return Number(v.id) === Number(vendorId);
        });
        return vendor[0] && vendor[0].id

    };

    populatevendorvendorvendorPhoneNo = e => {
        const vendorId = e.target.value;
        const vendors = this.state.vendorLists;
        const vendor = vendors.filter(v => {
            return Number(v.id) === Number(vendorId);
        });
        return vendor[0] && vendor[0].phoneNumber

    };

    downloadurl(url) {
        setTimeout(() => {
            window.open(
                url,
                '_blank'
            );
        }, 100);
    }

    closesuccesSubmitModal = () => {
        this.setState(
            { succesSubmitModal: false }, () => {
                this.props.modalCloseReviewInvoice();
            });

    }

    showsuccesSubmitModal = () => {
        this.setState({ succesSubmitModal: true });

    }

    closeinvoiceWarningModal = () => {
        this.setState({ showinvoiceWarningModal: false });

    }

    addnewTrmshandleSubmit = (value) => {
        //console.log('--------------------addnewTrmshandleSubmit-----------------', value);

        const config = {
            headers: {
                "Content-Type": "application/json"
            }
        };
        const param = {
            termName: value.termName,
            netDueDays: (value.netDueDays === '' ? 0.00 : (value.netDueDays)),
            discountPercent: (value.discountPercent === '' ? 0 : (value.discountPercent)),
            discountApplicableDays: (value.discountApplicableDays === '' ? 0 : (value.discountApplicableDays)),
            archive: false,
            isActive: true
        }

        console.log('**************************************', param);
        this.setState({
            trmsloader: true,
        }
            , () => {
                axios
                    .post(`/term/create/${this.props.selectedBusinessID}`, param, config)
                    .then(res => {
                        this.setState({
                            showCreateTrmsModal: false,
                            addtrmsConfirm: true
                        });
                    })
                    .catch(err => {
                        let tremserrorMessage = this.handelError(err);
                        this.setState({
                            tremserrorMessage,
                            trmsloader: false,

                        })
                    });
            }
        )
    }

    hideCreatetremsModal = () => {
        this.setState({ showCreateTrmsModal: false })
    }

    showCreatetremsModal = () => {
        this.setState({ showCreateTrmsModal: true })
    }

    closeaddtrmsConfirmModal = () => {
        this.setState({ addtrmsConfirm: false }, () => {
            this.fetchterm();
        })
    }

    handleGetBusiness = () => {

        this.setState({
            showBusinessModal: true,
            editBusinessLoading: true,
            editBusinessUI: true
        }, () => {

            axios
                .get(

                    `business/${this.props.selectedBusinessID}`
                )
                .then(res => {
                    console.log('editBusiness', res.data);
                    let getinvoiceDetails = res.data;
                    initialValues['businessName'] = getinvoiceDetails.businessName;
                    initialValues['businessAddress1'] = getinvoiceDetails.businessAddress1;
                    initialValues['businessPhone'] = getinvoiceDetails.businessPhone;
                    if (this._isMounted) {
                        this.setState({
                            businessData: res.data,
                            businessLoader: false
                        }, () => {
                            // initialValues = this.state.editBusiness;
                            // console.log('editBusiness', initialValues);
                        });
                    }


                })
                .catch(e => {
                    let errorMsg = this.displayError(e);
                    this.setState({
                        businessDataError: errorMsg,
                        businessLoader: false

                    });

                    setTimeout(() => {
                        this.setState({ businessDataError: null });
                    }, 5000);

                });

        });
    }

    componentDidUpdate() {
        console.log('componentDidUpdate', this.props.emailRequest);
    }

    componentWillMount() {

        // const invoiceKey = { ...this.props };
        // //console.log('componentDidMount EditInvoice', invoiceKey.invId)
        // this.setState({
        //     invoiceId: invoiceKey.invId
        // });
    }

    componentDidMount() {
        console.log('componentDidMount', this.props.emailRequest);
        this.handleGetBusiness();
        // this.getItemlist();
        // this.getAccountslist();


        this.fetchVendorList();
        // this.getBusiness();
        this.fetchterm();
        // this.getinvoiceDetails();

    }

    deletSingleRow = (index, values, setFieldValue, callback) => {
        console.log('+++++ values +++++++++', values);
        let totalAmount = 0;
        let grandsubtotal = 0;
        let totaltax = 0;
        let amountDue =0;

        if (values.invoiceItems.length) {
            values.invoiceItems.map((data, i) => {
                if (i !== index) {

                    const tempTotal = (data.unitPrice * data.quantity);
                    const percentCalc = (data.taxPercent / 100) * tempTotal;
                    //const subTotal = (parseFloat(tempTotal) + parseFloat(percentCalc)).toFixed(2);
                    //const subTotal = (data.unitPrice * data.quantity).toFixed(2);
                   const subTotal = (parseFloat(tempTotal)).toFixed(2);
                    grandsubtotal = parseFloat(grandsubtotal) + parseFloat(subTotal);
                    totaltax = parseFloat(totaltax) + parseFloat(percentCalc);

                    totalAmount = parseFloat(totalAmount) + parseFloat(subTotal);
                    amountDue = parseFloat(grandsubtotal) + parseFloat(totaltax);
                }
            });
        }

        setFieldValue('totalTax', totaltax.toFixed(2));
        setFieldValue("subTotal", totalAmount.toFixed(2));
        //setFieldValue("amountDue", this.cal(totalAmount, values.totalTax))
        setFieldValue('amountDue', amountDue.toFixed(2));
        callback(index);
    }

    setsubtotal = (arry, index, tag, value, setFieldValue) => {
        let grandsubtotal = 0;
        let totaltax=0;
        if (tag === 'Quantity') {
            arry[index].quantity = isNaN(value) ? 0 : value
        } else if (tag === 'Unit price') {
            arry[index].unitPrice = isNaN(value) ? 0.00 : value
        } 
        else if (tag === 'Tax Rate') {
            arry[index].taxPercent = isNaN(value) ? 0.00 : value
        }
        // else{
        //     arry[index].taxName = value ? '' : value
        // }
        arry.map((inv, index) => {
            const tempTotal = parseFloat(inv.unitPrice * inv.quantity);
            const percentCalc = (inv.taxPercent / 100) * tempTotal;

            //const subtotal = (inv.unitPrice * inv.quantity).toFixed(2);
            // const subtotal = (parseFloat(tempTotal) + parseFloat(percentCalc)).toFixed(2);
            const subtotal = (parseFloat(tempTotal)).toFixed(2);
            grandsubtotal = parseFloat(grandsubtotal) + parseFloat(subtotal);

            totaltax = parseFloat(totaltax) + parseFloat(percentCalc);
            const amountDue = parseFloat(grandsubtotal) + parseFloat(totaltax);
            setFieldValue('totalTax', totaltax.toFixed(2));
            console.log(':::::::::::::::::::::::::',totaltax);
            setFieldValue('amountDue', amountDue.toFixed(2));
        });
        return grandsubtotal.toFixed(2);
    }


    editItem = (index, value) => {
        console.log(index)
        console.log(value)

    }


    setdescription = (param) => {
        const item = this.state.itemlist.filter(item => Number(item.id) === Number(param));
        return item[0] && item[0].description
    }

    setItemMasterId = (param) => {
        const item = this.state.itemlist.filter(item => Number(item.id) === Number(param));
        const obj = { id: item[0].id }
        console.log('setItemMasterId', obj);
        return obj
    }

    setunitPrice = (param) => {
        const item = this.state.itemlist.filter(item => Number(item.id) === Number(param));
        // console.log(item)
        return item[0] && item[0].unitPrice.toFixed(2)
    }
    ///today add
    hideAddVendorModalHandler = () => {
        this.setState({ showAddVendor: false })
    }

    showAddVendorModalHandler = () => {
        this.setState({ showAddVendor: true })
    }

    subtotalcalculation = (param) => {
        // let  total = 0;
        // initialValues.invoiceItems.map((item) => {
        //     total = total + item.subtotal

        // });
        console.log(param)
        return param

    };

    setname = (param) => {
        const item = this.state.itemlist.filter(item => Number(item.id) === Number(param));
        return item[0] && item[0].name
    }

    setsubtotalitemwise = (arry, index, value) => {
        let grandsubtotal = 0
        arry[index].quantity = 1
        arry[index].unitPrice = isNaN(value) ? 0.00 : value.toFixed(2)
        arry.map((inv, index) => {
            const subtotal = (inv.unitPrice * inv.quantity).toFixed(2);
            grandsubtotal = parseFloat(grandsubtotal) + parseFloat(subtotal);
        });
        return grandsubtotal.toFixed(2);
    }

    settotalamountwise = (values, index, value) => {
        const arry = values.invoiceItems
        const totalTax = values.totalTax;
        let totalamout = 0
        // arry[index].quantity = 1
        arry[index].unitPrice = isNaN(value) ? 0.00 : value.toFixed(2)
        arry.map((inv, index) => {
            const subtotal = (inv.unitPrice * inv.quantity).toFixed(2);
            totalamout = parseFloat(totalamout) + parseFloat(subtotal);
        });
        return (parseFloat(totalTax) + parseFloat(totalamout)).toFixed(2);
    }

    settotalamont = (values) => {
        let totalamout = 0
        const totalTax = isNaN(values.totalTax) ? 0.00 : values.totalTax;
        values.map((inv, index) => {
            const subtotal = (inv.unitPrice * inv.quantity).toFixed(2);
            totalamout = parseFloat(totalamout) + parseFloat(subtotal);
        });
        return (parseFloat(totalTax) + parseFloat(totalamout)).toFixed(2);
    }

    expenseitemclick = (param, setFieldValue, index, values) => {
        const item = this.state.Accountslist.filter(item => Number(item.id) === Number(param));
        values.invoiceItems[index].expencedisable = true;
        setFieldValue(`invoiceItems.${index}.itemRefType`, 'EXPENSE');
        if (item && item.length > 0) {
            setFieldValue(`invoiceItems.${index}.itemMaster`, { id: item[0].id });
            setFieldValue(`invoiceItems.${index}.name`, item[0].name);
            setFieldValue(`invoiceItems.${index}.description`, item[0].description);
            setFieldValue(`invoiceItems.${index}.quantity`, 1);
            setFieldValue(`invoiceItems.${index}.unitPrice`, 0.00);
            setFieldValue(`invoiceItems.${index}.unitDisFlag`, false);///// unitDisFlag 30-01-2020
            setFieldValue("subTotal", this.setsubtotalitemwise(values.invoiceItems, index, 0));
            setFieldValue("amountDue", this.settotalamountwise(values, index, 0));

        } else {
            setFieldValue(`invoiceItems.${index}.itemMaster`, { id: '' });
            setFieldValue(`invoiceItems.${index}.name`, '');
            setFieldValue(`invoiceItems.${index}.description`, '');
            setFieldValue(`invoiceItems.${index}.quantity`, 0);
            setFieldValue(`invoiceItems.${index}.unitPrice`, 0.00);
        }

    }

    itemclick = (param, setFieldValue, index, values) => {
        const item = this.state.itemlist.filter(item => Number(item.id) === Number(param));
        values.invoiceItems[index].expencedisable = false
        setFieldValue(`invoiceItems.${index}.itemRefType`, 'ITEM');
        if (item && item.length > 0) {

            setFieldValue(`invoiceItems.${index}.itemMaster`, { id: item[0].id });
            setFieldValue(`invoiceItems.${index}.name`, item[0].itemName);
            setFieldValue(`invoiceItems.${index}.description`, item[0].description);
            setFieldValue(`invoiceItems.${index}.quantity`, 1);
            setFieldValue(`invoiceItems.${index}.unitPrice`, item[0].unitPrice.toFixed(2));
            setFieldValue(`invoiceItems.${index}.unitDisFlag`, true);///// unitDisFlag 30-01-2020
            setFieldValue("subTotal", this.setsubtotalitemwise(values.invoiceItems, index, item[0].unitPrice))
            setFieldValue("amountDue", this.settotalamountwise(values, index, item[0].unitPrice))

        } else {
            setFieldValue(`invoiceItems.${index}.itemMaster`, { id: '' });
            setFieldValue(`invoiceItems.${index}.name`, '');
            setFieldValue(`invoiceItems.${index}.description`, '');
            setFieldValue(`invoiceItems.${index}.quantity`, 0);
            setFieldValue(`invoiceItems.${index}.unitPrice`, 0.00);
        }

    }

    setitemName = (param) => {
        console.log(param)
        const item = this.state.itemlist.filter(item => item.itemName.toLowerCase() === param.toLowerCase());
        console.log('item', item)
        if (item && item.length > 0) {
            console.log('------------------------------------------', item[0]);
            return item[0].itemName
        } else {
            const itemAccount = this.state.Accountslist.filter(item => item.name.toLowerCase() === param.toLowerCase());
            console.log('itemAccount', itemAccount)
            if (itemAccount && itemAccount.length > 0) {
                return itemAccount[0].name
            }
        }
        return 'Select'
    }

    addnewitem = (index, values, setFieldValue, type) => {
        values.invoiceItems[index].showflag = false
        setFieldValue(`invoiceItems.${index}.itemMaster`, { id: '' });
        setFieldValue(`invoiceItems.${index}.itemRefType`, type);
        setFieldValue(`invoiceItems.${index}.name`, '');
        setFieldValue(`invoiceItems.${index}.description`, '');
        // EXP
        setFieldValue(`invoiceItems.${index}.quantity`, 1);
        setFieldValue(`invoiceItems.${index}.unitPrice`, 0.00);
        // EXP
        if (type !== "ITEM") {
            setFieldValue(`invoiceItems.${index}.expencedisable`, true);
        }
        setFieldValue("subTotal", this.setsubtotalitemwise(values.invoiceItems, index, 0))
        setFieldValue("amountDue", this.settotalamont(values.invoiceItems))

    }

    showitem = (param) => {
        return param
    }

    getAccountsfilterClick = () => {
        //console.log(this.getAccountsref.current.value)
        const filterValue = (this.getAccountsref.current || {}).value.toLowerCase().trim();
        if (filterValue === '') {
            const Accountslist = [...this.state.tempAccountslist]
            this.setState({
                Accountslist
            })
        } else {
            const Accountslist = [...this.state.tempAccountslist].filter(value => value.name.toLowerCase().includes(filterValue));
            this.setState({
                Accountslist
            })
        }


    }

    getExpencefilterClick = () => {
        //console.log(this.getexpenceref.current.value)
        const filterValue = (this.getexpenceref.current || {}).value.toLowerCase().trim();
        if (filterValue === '') {
            const Accountslist = [...this.state.tempAccountslist]
            this.setState({
                Accountslist
            })
        } else {
            const Accountslist = [...this.state.tempAccountslist].filter(value => value.name.toLowerCase().includes(filterValue));
            this.setState({
                Accountslist
            })
        }


    }

    getitemlistfilterClick = () => {
        // console.log(this.getitemref.current.value)
        const filterValue = (this.getitemref.current || {}).value.toLowerCase().trim();;
        if (filterValue === '') {
            const itemlist = [...this.state.tempitemlist]
            this.setState({
                itemlist
            })
        } else {
            const itemlist = [...this.state.tempitemlist].filter(value => value.itemName.toLowerCase().includes(filterValue));
            this.setState({
                itemlist
            })
        }


    }

    expenceassegingtoall = (e, values, setFieldValue) => {
        this.setState({ expeTitalId: e })
        const expenceitem = this.state.Accountslist.filter(item => item.name === e);
        const expenceitemarry = []
        for (let index in values.invoiceItems) {

            expenceitemarry.push({
                name: expenceitem[0].name,
                description: expenceitem[0].description,
                quantity: 1,
                unitPrice: 0.00,
                taxPercent: 0.00,
                subtotal: 0,
                tax: null,
                itemRefType: 'EXPENSE',
                showflag: true,
                expencedisable: true
            })
        }
        setFieldValue("invoiceItems", expenceitemarry)
        setFieldValue("subTotal", 0.00)
        setFieldValue("amountDue", values.invoiceItems.totalTax)


    }



    /*getItemlist = () => {
        this.setState({ loader: true }, () => {
            axios.get(`autofy/getItems/${this.props.selectedBusinessID}`)
                .then(res => {
                    const itemlist = res.data;
                    const tempitemlist = [...res.data]
                    this.setState({
                        itemlist,
                        tempitemlist,
                        loader: false
                    });

                })
                .catch(err => {

                });
        });
    };*/

    handleAddConfirMmsg = () => {
        this.setState({
            addConfirMmsg: true,
        }, () => {
            this.fetchVendorList();
        });

    }
    handleAddConfirMmsgClose = () => {
        this.setState({
            addConfirMmsg: false,
        });

    }

    handleInvoiceDate = (date) => {
        this.setState({
            selectedInvoiceDate: new Date(date)
        });
    }

    render() {
        console.log('initialValues', initialValues)
        const {
            loader,
            errorObj,
            auditTrails,
            emailAttachment,
            invoiceDetails,
            termLists,
            vendorLists,
            Accountslist,
            customerVendor,
            lineItems,
            invDetails,
            itemlist,
            invAdditionalNote,
            selectedInvoiceDate
        } = this.state;

        const { emailRequest, s3FileLocation, fileName } = this.props;

        console.log('selectedInvoiceDate', selectedInvoiceDate);

        return (
            <div className="verifyInvoiceOuter invoicMasterWrap">
                <div className="invoiceMasterHeader">
                    <Row>
                        <Col md={3}>
                            <h1>Add Invoice</h1>
                        </Col>
                    </Row>
                </div>

                <div className="invoiceMasterBody content-body">
                    {loader ?
                        (
                            <LoadingSpinner className="text-center" />

                        )
                        : (

                            <Formik
                                initialValues={initialValues}
                                validationSchema={addInvoiceSchema}
                                onSubmit={this.handleSubmit}
                                enableReinitialize
                            >
                                {({
                                    values,
                                    errors,
                                    setFieldValue,
                                    touched,
                                    handleChange,
                                    handleBlur
                                }) => {

                                    return (
                                        <Form noValidate>
                                            <Row>
                                                <Col md={7}>
                                                    <div className="content-block leftContentBlock">
                                                        <Row>
                                                            <Col md={12}>
                                                                {this.props && this.props.s3FileLocation ? (
                                                                    <iframe
                                                                        src={this.props.s3FileLocation + '#toolbar=0&navpanes=0&statusbar=0&view=Fit;readonly=true; disableprint=true;'}
                                                                        width="100%"
                                                                        height="500"
                                                                        frameBorder="0"
                                                                        title="Invoice"
                                                                        allowFullScreen
                                                                    ></iframe>
                                                                ) : (
                                                                        <div className="invNotFound">
                                                                            <h5>No invoice found.</h5>
                                                                        </div>
                                                                    )
                                                                }
                                                                <hr />
                                                            </Col>
                                                        </Row>



                                                        <Row>
                                                            <Col md={12}>
                                                                {
                                                                    !emailRequest.isAccountingToolEnable ? (
                                                                        <div className="row">
                                                                            <div className="col-sm-12">
                                                                                <FormGroup controlId="taxType">

                                                                                    <Field
                                                                                        name="taxType"
                                                                                        type="radio"
                                                                                        onChange={(e) => {
                                                                                            handleChange(e);
                                                                                            this.taxTypeCalculation(values, 'TAX_LINE', setFieldValue);
                                                                                        }}
                                                                                        checked={values.taxType === 'TAX_LINE'}
                                                                                        value={`TAX_LINE`}
                                                                                    /> Apply Tax to Line Items
                                                                                        &nbsp; &nbsp;
                                                                                                <Field
                                                                                        name="taxType"
                                                                                        type="radio"
                                                                                        onChange={(e) => {
                                                                                            handleChange(e);
                                                                                            this.taxTypeCalculation(values, 'TAX_INVOICE', setFieldValue);
                                                                                        }}
                                                                                        checked={values.taxType === 'TAX_INVOICE'}
                                                                                        value={`TAX_INVOICE`}

                                                                                    /> Apply Tax to Invoice


                                                                                            </FormGroup>
                                                                            </div>
                                                                        </div>
                                                                    ) : null
                                                                }
                                                                <div className="invInfoTable">
                                                                    <div className="table-responsive">

                                                                        <FieldArray
                                                                            name="invoiceItems"
                                                                            render={helpers => (
                                                                                <Fragment>

                                                                                    <Table>
                                                                                        <thead className="theaderBg">
                                                                                            <tr>
                                                                                                <th >Name&nbsp;/&nbsp;Description  <span className="required">*</span></th>
                                                                                                <th>Quantity  <span className="required">*</span></th>
                                                                                                <th >Unit Price  <span className="required">*</span></th>
                                                                                                {!emailRequest.isAccountingToolEnable && values.taxType === 'TAX_LINE' && <th width="">Tax Rate %</th>}
                                                                                                {!emailRequest.isAccountingToolEnable && values.taxType === 'TAX_LINE' && <th>Tax Name</th>}
                                                                                                <th >Subtotal  <span className="required">*</span></th>
                                                                                                <th >&nbsp;&nbsp;</th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody>

                                                                                            {values.invoiceItems && values.invoiceItems.length ? values.invoiceItems.map((inv, index) => {
                                                                                                const tempTotal = (inv.unitPrice * inv.quantity);
                                                                                                let percentCalc;

                                                                                                values.taxType === 'TAX_LINE' ? percentCalc = (inv.taxPercent / 100) * tempTotal : percentCalc = 0;
                                                                                                //const subtotal = tempTotal + percentCalc;
                                                                                                const subtotal = tempTotal;
                                                                                                inv.subtotal = isNaN((subtotal).toFixed(2)) ? 0.00 : (subtotal).toFixed(2);

                                                                                                //
                                                                                                inv.tax = (inv.subtotal * values.taxPercent) / 100;
                                                                                                inv.id = null;

                                                                                                return (
                                                                                                    <tr key={index}>
                                                                                                        <td>

                                                                                                            {/*  */}
                                                                                                            <FormGroup controlId="formBasicText" >
                                                                                                                <Field
                                                                                                                    name={`invoiceItems.${index}.name`}
                                                                                                                    type='text'
                                                                                                                    placeholder="Name"
                                                                                                                    className={`form-control`}
                                                                                                                    autoComplete="nope"
                                                                                                                />
                                                                                                                <ErrorMessage name={`invoiceItems.${index}.name`} render={(msg) => <span className="errorMsg">{msg}</span>} />
                                                                                                                <FormControl.Feedback />
                                                                                                            </FormGroup>
                                                                                                            {/*  */}

                                                                                                            <FormGroup controlId="formBasicText" style={{ display: 'none' }}>
                                                                                                                <Field
                                                                                                                    name={`invoiceItems.${index}.itemRefType`}
                                                                                                                    component="textarea"

                                                                                                                    className={`form-control`}
                                                                                                                    autoComplete="nope"
                                                                                                                />

                                                                                                                <FormControl.Feedback />
                                                                                                            </FormGroup>
                                                                                                            {values.invoiceItems[index].showflag ? (
                                                                                                                <FormGroup controlId="formBasicText" style={{ display: 'none' }}>

                                                                                                                    <Field
                                                                                                                        name={`invoiceItems.${index}.name`}
                                                                                                                        component="select"
                                                                                                                        className={`form-control`}
                                                                                                                        autoComplete="nope"
                                                                                                                        placeholder="Name"
                                                                                                                        onChange={e => {
                                                                                                                            handleChange(e);
                                                                                                                            setFieldValue(`invoiceItems.${index}.itemMaster`, this.setItemMasterId(e.target.value));
                                                                                                                            setFieldValue(`invoiceItems.${index}.description`, this.setdescription(e.target.value));
                                                                                                                            setFieldValue(`invoiceItems.${index}.unitPrice`, this.setunitPrice(e.target.value));

                                                                                                                        }}
                                                                                                                    >
                                                                                                                        <option value="" >
                                                                                                                            Select
                                                                                                                    </option>

                                                                                                                        {itemlist.length ?
                                                                                                                            itemlist.map(item => (
                                                                                                                                <option
                                                                                                                                    value={item.id}
                                                                                                                                    key={item.id}

                                                                                                                                >
                                                                                                                                    {item.itemName}
                                                                                                                                </option>
                                                                                                                            )) : null}

                                                                                                                        {Accountslist.length ?
                                                                                                                            Accountslist.map(item => (
                                                                                                                                <option
                                                                                                                                    value={item.id}
                                                                                                                                    key={item.id}

                                                                                                                                >
                                                                                                                                    {item.name}
                                                                                                                                </option>
                                                                                                                            )) : null}


                                                                                                                    </Field>
                                                                                                                    {errors.invoiceItems && errors.invoiceItems[index] && errors.invoiceItems[index].name && (Array.isArray(touched.invoiceItems) && touched.invoiceItems[index] && touched.invoiceItems[index].name) ? (
                                                                                                                        <span className="errorMsg">
                                                                                                                            {errors.invoiceItems[index].name}
                                                                                                                        </span>
                                                                                                                    ) : null}
                                                                                                                    <FormControl.Feedback />

                                                                                                                </FormGroup>
                                                                                                            ) : null
                                                                                                            }


                                                                                                            <FormGroup controlId="formBasicText">
                                                                                                                <Field
                                                                                                                    name={`invoiceItems.${index}.description`}
                                                                                                                    component="textarea"
                                                                                                                    //type="text"
                                                                                                                    className={`form-control`}
                                                                                                                    autoComplete="nope"
                                                                                                                    placeholder="Description"
                                                                                                                />
                                                                                                                {errors.invoiceItems && errors.invoiceItems[index] && errors.invoiceItems[index].description && (Array.isArray(touched.invoiceItems) && touched.invoiceItems[index] && touched.invoiceItems[index].description) ? (
                                                                                                                    <span className="errorMsg">
                                                                                                                        {errors.invoiceItems[index].description}
                                                                                                                    </span>
                                                                                                                ) : null}
                                                                                                                <FormControl.Feedback />
                                                                                                            </FormGroup>
                                                                                                            <div>
                                                                                                                {/* <Button type="button" bsStyle="primary blue-btn" onClick={() => this.editItem(values.invoiceItems[index].name)}>
                                                                                                                Edit item
                                                                                                                </Button> */}
                                                                                                            </div>
                                                                                                        </td>

                                                                                                        <td >
                                                                                                            <FormGroup controlId="formBasicText">
                                                                                                                {/* {console.log("expenseItem----", values.invoiceItems[index])} */}
                                                                                                                <Field
                                                                                                                    name={`invoiceItems.${index}.quantity`}
                                                                                                                    type="number"
                                                                                                                    className={`form-control`}
                                                                                                                    autoComplete="nope"
                                                                                                                    placeholder="Quantity"
                                                                                                                    disabled={values.invoiceItems[index].expencedisable}
                                                                                                                    min="0"
                                                                                                                    onChange={e => {
                                                                                                                        handleChange(e);
                                                                                                                        setFieldValue(`invoiceItems.${index}.quantity`, e.target.value.replace(/^.*?([1-9]+[0-9]*).*$/, '$1'))
                                                                                                                        setFieldValue("subTotal", this.setsubtotal(values.invoiceItems, index, 'Quantity', e.target.value, setFieldValue))
                                                                                                                        //setFieldValue("amountDue", this.settotalamount(values, index, 'Quantity', e.target.value))
                                                                                                                    }}
                                                                                                                />
                                                                                                                {errors.invoiceItems && errors.invoiceItems[index] && errors.invoiceItems[index].quantity && (Array.isArray(touched.invoiceItems) && touched.invoiceItems[index] && touched.invoiceItems[index].quantity) ? (
                                                                                                                    <span className="errorMsg">
                                                                                                                        {errors.invoiceItems[index].quantity}
                                                                                                                    </span>
                                                                                                                ) : null}
                                                                                                                <FormControl.Feedback />
                                                                                                            </FormGroup>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <FormGroup controlId="formBasicText">
                                                                                                                <Field
                                                                                                                    name={`invoiceItems.${index}.unitPrice`}
                                                                                                                    type="text"
                                                                                                                    className={`form-control dollerInput unitPrice`}
                                                                                                                    autoComplete="nope"
                                                                                                                    placeholder="Unit price"
                                                                                                                    min="0"
                                                                                                                    onBlur={e => {
                                                                                                                        if(e.target.value) {
                                                                                                                            const up = isNaN(parseFloat(e.target.value)) ? 0 : parseFloat(e.target.value);
                                                                                                                            setFieldValue(`invoiceItems.${index}.unitPrice`, up.toFixed(2))
                                                                                                                        } 
                                                                                                                    }}
                                                                                                                    disabled={values.invoiceItems[index].unitDisFlag}
                                                                                                                    onChange={e => {
                                                                                                                        handleChange(e);
                                                                                                                        setFieldValue("subTotal", this.setsubtotal(values.invoiceItems, index, 'Unit price', e.target.value, setFieldValue));
                                                                                                                        //setFieldValue("amountDue", this.settotalamount(values, index, 'Unit price', e.target.value))
                                                                                                                    }}
                                                                                                                />
                                                                                                                {errors.invoiceItems && errors.invoiceItems[index] && errors.invoiceItems[index].unitPrice && (Array.isArray(touched.invoiceItems) && touched.invoiceItems[index] && touched.invoiceItems[index].unitPrice) ? (
                                                                                                                    <span className="errorMsg">
                                                                                                                        {errors.invoiceItems[index].unitPrice}
                                                                                                                    </span>
                                                                                                                ) : null}
                                                                                                                <FormControl.Feedback />
                                                                                                            </FormGroup>
                                                                                                        </td>

                                                                                                        {/* tax rate start*/}
                                                                                                        {!emailRequest.isAccountingToolEnable && values.taxType === 'TAX_LINE'
                                                                                                            ? (<td >
                                                                                                                <FormGroup controlId="formBasicText">
                                                                                                                    <Field
                                                                                                                        name={`invoiceItems.${index}.taxPercent`}
                                                                                                                        type="text"
                                                                                                                        className={`form-control taxPercent`}
                                                                                                                        autoComplete="nope"
                                                                                                                        placeholder="%"
                                                                                                                        min="0"
                                                                                                                        disabled={values.invoiceItems[index].unitDisFlag}
                                                                                                                        onBlur={e => {
                                                                                                                            if(e.target.value) {
                                                                                                                                const up = isNaN(parseFloat(e.target.value)) ? 0 : parseFloat(e.target.value);
                                                                                                                                setFieldValue(`invoiceItems.${index}.taxPercent`, up.toFixed(2))
                                                                                                                            } 
                                                                                                                        }}
                                                                                                                        onChange={e => {
                                                                                                                            handleChange(e);
                                                                                                                            setFieldValue("subTotal", this.setsubtotal(values.invoiceItems, index, 'Tax Rate', e.target.value, setFieldValue));
                                                                                                                            //setFieldValue("amountDue", this.settotalamount(values, index, 'Tax Rate', e.target.value))
                                                                                                                        }}
                                                                                                                    />
                                                                                                                    {errors.invoiceItems && errors.invoiceItems[index] && errors.invoiceItems[index].taxPercent && (Array.isArray(touched.invoiceItems) && touched.invoiceItems[index] && touched.invoiceItems[index].taxPercent) ? (
                                                                                                                        <span className="errorMsg">
                                                                                                                            {errors.invoiceItems[index].taxPercent}
                                                                                                                        </span>
                                                                                                                    ) : null}
                                                                                                                    <FormControl.Feedback />
                                                                                                                </FormGroup>
                                                                                                            </td>) : null}

                                                                                                        {/* tax rate end */}

                                                                                                        {/* tax name*/}
                                                                                                        {!emailRequest.isAccountingToolEnable && values.taxType === 'TAX_LINE' ? (<td width="90">
                                                                                                            <FormGroup controlId="formBasicText">
                                                                                                                <Field
                                                                                                                    name={`invoiceItems.${index}.taxName`}
                                                                                                                    type="text"
                                                                                                                    className={`form-control taxName`}
                                                                                                                    autoComplete="nope"
                                                                                                                    placeholder=""
                                                                                                                    min="0"
                                                                                                                    disabled={values.invoiceItems[index].unitDisFlag}
                                                                                                                    onChange={e => {
                                                                                                                        handleChange(e);
                                                                                                                    }}
                                                                                                                />
                                                                                                                {errors.invoiceItems && errors.invoiceItems[index] && errors.invoiceItems[index].taxName && (Array.isArray(touched.invoiceItems) && touched.invoiceItems[index] && touched.invoiceItems[index].taxName) ? (
                                                                                                                    <span className="errorMsg">
                                                                                                                        {errors.invoiceItems[index].taxName}
                                                                                                                    </span>
                                                                                                                ) : null}
                                                                                                                <FormControl.Feedback />
                                                                                                            </FormGroup>
                                                                                                        </td>) : null}

                                                                                                        {/* tax name */}


                                                                                                        <td>
                                                                                                            <FormGroup controlId="formBasicText">
                                                                                                                <Field
                                                                                                                    name={`invoiceItems.${index}.subtotal`}
                                                                                                                    type="text"
                                                                                                                    className={`form-control dollerInput`}
                                                                                                                    autoComplete="nope"
                                                                                                                    placeholder="Sub Total"
                                                                                                                    readonly="true"
                                                                                                                />
                                                                                                                {errors.invoiceItems && errors.invoiceItems[index] && errors.invoiceItems[index].subtotal && (Array.isArray(touched.invoiceItems) && touched.invoiceItems[index] && touched.invoiceItems[index].subtotal) ? (
                                                                                                                    <span className="errorMsg">
                                                                                                                        {errors.invoiceItems[index].subtotal}
                                                                                                                    </span>
                                                                                                                ) : null}
                                                                                                                <FormControl.Feedback />
                                                                                                            </FormGroup>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <div className="showHideItem">
                                                                                                                <Link to="#" className="smIconBg"
                                                                                                                    onClick={() => this.deletSingleRow(index, values, setFieldValue, (i) => helpers.remove(i))}>
                                                                                                                    <Image src={crossSmIcon} />
                                                                                                                </Link>
                                                                                                            </div>

                                                                                                        </td>
                                                                                                    </tr>
                                                                                                );
                                                                                            }) : null}
                                                                                        </tbody>
                                                                                        {/* <tfoot>
                                                                                        <tr>
                                                                                            <td colSpan={4} className="text-center">
                                                                                           
                                                                                            </td>
                                                                                        </tr>
                                                                                     </tfoot> */}

                                                                                        <tfoot>
                                                                                            <tr>
                                                                                                <td colSpan={5} className="text-center">
                                                                                                    {/* EXP */}
                                                                                                    <Button type="button" className="mr-3" bsStyle="primary blue-btn" onClick={() => helpers.push({ name: '', description: '', quantity: 1, unitPrice: parseFloat(0).toFixed(2), taxPercent: parseFloat(0).toFixed(2), taxName: 'Tax', subtotal: 0, isTaxable: false, itemMaster: null, showflag: true, expencedisable: false, itemRefType: 'ITEM' })}>
                                                                                                        Add Line
                                                                                                </Button>
                                                                                                    {values.invoiceItems.length > 0 ? (
                                                                                                        <Button type="button" bsStyle="primary but-gray ml-5" onClick={() => this.deleteallrow(values, setFieldValue)}>
                                                                                                            Delete all rows
                                                                                                </Button>
                                                                                                    ) : null

                                                                                                    }

                                                                                                </td>
                                                                                            </tr>
                                                                                        </tfoot>
                                                                                    </Table>


                                                                                </Fragment>
                                                                            )}
                                                                        />
                                                                    </div>
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col md={12}>
                                                                <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                </Col>
                                                <Col md={5}>
                                                    <div className="content-block rightContentBlock">
                                                        <Tabs defaultActiveKey="details" transition={false} id="noanim-tab-example">
                                                            <Tab eventKey="details" title="Details">
                                                                <div className="invDisplayRightBlock collapaseOuter">
                                                                    <h3 onClick={() => this.setState({ invoiceDetails: !invoiceDetails })}
                                                                        aria-controls="invoice-details"
                                                                        aria-expanded={invoiceDetails}
                                                                        className={invoiceDetails === false ? 'collapas-show' : 'collapas-hide'}
                                                                        style={{ marginBottom: '0' }}
                                                                    >
                                                                        Invoice Details
                                                                    </h3>

                                                                    <Collapse in={invoiceDetails}>
                                                                        <div id="audit-details">
                                                                            <Table>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.invoiceNumber ? ' text-danger ' : '')}>Invoice Number <span className="text-danger">*</span></span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="invoiceNumber"
                                                                                                    type="text"
                                                                                                    className={'form-control'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="Enter"
                                                                                                    value={values.invoiceNumber || ''}
                                                                                                    autoFocus
                                                                                                />
                                                                                                <ErrorMessage name="invoiceNumber" render={(msg) => <span className="errorMsg">{msg}</span>} />

                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.invoiceDate ? ' text-danger' : '')}>Invoice Date <span className="text-danger">*</span></span>
                                                                                        </td>
                                                                                        <td className="text-right dateEditInv">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                {/* <Field
                                                                                            name="invoiceDate"
                                                                                            type="text"
                                                                                            className={'form-control'}
                                                                                            autoComplete="nope"
                                                                                            placeholder="Enter"
                                                                                            value={values.invoiceDate || ''}
                                                                                            autoFocus
                                                                                        /> */}
                                                                                                <DatePicker
                                                                                                    className={'form-control'}
                                                                                                    onChange={value => {
                                                                                                        setFieldValue('invoiceDate', value);
                                                                                                        this.handleInvoiceDate(value);

                                                                                                    }}
                                                                                                    value={values.invoiceDate || ''}
                                                                                                    // maxDate={maxDate}
                                                                                                    name="invoiceDate"
                                                                                                    showLeadingZeros
                                                                                                />
                                                                                                <div className="clearfix"></div>
                                                                                                {errors.invoiceDate && touched.invoiceDate ? (
                                                                                                    <span className="errorMsg">
                                                                                                        {errors.invoiceDate}
                                                                                                    </span>
                                                                                                ) : null}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.invoiceDueDate ? ' text-danger' : '')}>Invoice Due Date <span className="text-danger">*</span></span>
                                                                                        </td>
                                                                                        <td className="text-right dateEditInv">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <DatePicker
                                                                                                    className={'form-control'}
                                                                                                    onChange={value => {
                                                                                                        setFieldValue('invoiceDuedate', value);
                                                                                                    }}
                                                                                                    value={values.invoiceDuedate}
                                                                                                    minDate={selectedInvoiceDate ? new Date(selectedInvoiceDate) : false}
                                                                                                    name="invoiceDuedate"
                                                                                                    showLeadingZeros
                                                                                                />
                                                                                                <div className="clearfix"></div>
                                                                                                {errors.invoiceDuedate && touched.invoiceDuedate ? (
                                                                                                    <span className="errorMsg">
                                                                                                        {errors.invoiceDuedate}
                                                                                                    </span>
                                                                                                ) : null}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.invoicePO ? ' text-danger ' : '')}>P.O. </span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="poNumber"
                                                                                                    type="text"
                                                                                                    className={'form-control'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="Enter"
                                                                                                    value={values.poNumber || ''}
                                                                                                    autoFocus
                                                                                                />
                                                                                                {errors.poNumber && touched.poNumber ? (
                                                                                                    <span className="errorMsg">
                                                                                                        {errors.poNumber}
                                                                                                    </span>
                                                                                                ) : null}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.termId ? ' text-danger ' : '')}>Terms </span>
                                                                                            <span className="text-primary">
                                                                                                &nbsp;(&nbsp;<a style={{ cursor: 'pointer' }} onClick={this.showCreatetremsModal}>Add Term</a>&nbsp;)
                                                                                            </span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="terms"
                                                                                                    component="select"
                                                                                                    className={'form-control'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="select"
                                                                                                    value={values.terms || ''}
                                                                                                >
                                                                                                    <option value="" >
                                                                                                        Select
                                                                                                    </option>
                                                                                                    {termLists.length ?
                                                                                                        termLists.map(term => (
                                                                                                            <option
                                                                                                                value={term.id}
                                                                                                                key={term.id}

                                                                                                            >
                                                                                                                {term.termName}
                                                                                                            </option>
                                                                                                        )) : null}
                                                                                                </Field>

                                                                                                {errors.terms && touched.terms ? (
                                                                                                    <span className="errorMsg">{errors.terms}</span>
                                                                                                ) : null}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </Table>
                                                                        </div>
                                                                    </Collapse>
                                                                </div>


                                                                <div className="invDisplayRightBlock collapaseOuter">
                                                                    <h3 onClick={() => this.setState({ customerVendor: !customerVendor })}
                                                                        aria-controls="customer-vendor"
                                                                        aria-expanded={customerVendor}
                                                                        className={customerVendor === false ? 'collapas-show' : 'collapas-hide'}
                                                                        style={{ marginBottom: '0' }}
                                                                    >
                                                                        Customer &amp; Vendor
                                                                    </h3>

                                                                    <Collapse in={customerVendor}>
                                                                        <div id="audit-details">
                                                                            <Table>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.businessName ? ' text-danger ' : '')}>Customer Name</span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="businessName"
                                                                                                    type="text"
                                                                                                    className={'form-control'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="Enter"
                                                                                                    value={values.businessName || ''}
                                                                                                    autoFocus
                                                                                                    disabled
                                                                                                />
                                                                                                {/* {errors.businessName && touched.businessName ? (
                                                                                            <span className="errorMsg">
                                                                                                {errors.businessName}
                                                                                            </span>
                                                                                        ) : null} */}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.businessAddress1 ? ' text-danger ' : '')}>Customer Address</span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="businessAddress1"
                                                                                                    type="text"
                                                                                                    className={'form-control'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="Enter"
                                                                                                    value={values.businessAddress1 || ''}
                                                                                                    autoFocus
                                                                                                    disabled
                                                                                                />
                                                                                                {/* {errors.businessAddress1 && touched.businessAddress1 ? (
                                                                                            <span className="errorMsg">
                                                                                                {errors.businessAddress1}
                                                                                            </span>
                                                                                        ) : null} */}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.businessPhone ? ' text-danger ' : '')}>Customer Phone No</span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="businessPhone"
                                                                                                    type="text"
                                                                                                    className={'form-control'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="Enter"
                                                                                                    value={values.businessPhone || ''}
                                                                                                    autoFocus
                                                                                                    disabled
                                                                                                />
                                                                                                {/* {errors.businessPhone && touched.businessPhone ? (
                                                                                            <span className="errorMsg">
                                                                                                {errors.businessPhone}
                                                                                            </span>
                                                                                        ) : null} */}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.companyName ? ' text-danger ' : '')}>
                                                                                                Vendor Name
                                                                                                <span className="text-danger"> *</span>
                                                                                            </span>
                                                                                            {
                                                                                                (emailRequest.isAccountingToolEnable == false)
                                                                                                &&
                                                                                                <span className="text-primary">
                                                                                                    &nbsp;(&nbsp;<a style={{ cursor: 'pointer' }} onClick={this.showAddVendorModalHandler}>Add Vendor</a>&nbsp;)
                                                                                                </span>
                                                                                            }
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="vendorName"
                                                                                                    component="select"
                                                                                                    className={'form-control'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="select"
                                                                                                    value={values.vendorName}
                                                                                                    onChange={e => {
                                                                                                        handleChange(e);
                                                                                                        setFieldValue("vendorName", this.populatevendorvendorvendorName(e));
                                                                                                        setFieldValue("vendorAddress", this.populatevendorvendorAddress(e));
                                                                                                        setFieldValue("vendorPhoneNo", this.populatevendorvendorvendorPhoneNo(e));
                                                                                                    }}
                                                                                                >
                                                                                                    <option value="" selected="selected">
                                                                                                        Select
                                                                                                    </option>
                                                                                                    {vendorLists.length ?
                                                                                                        vendorLists.map(vendor => (
                                                                                                            <option
                                                                                                                value={vendor.id}
                                                                                                                key={vendor.id}

                                                                                                            >
                                                                                                                {vendor.companyName}
                                                                                                            </option>
                                                                                                        )) : null}
                                                                                                </Field>
                                                                                                <ErrorMessage name="vendorName" render={(msg) => <span className="errorMsg">{msg}</span>} />

                                                                                                {/* {errors.vendorName && touched.vendorName ? (
                                                                                                    <span className="errorMsg">{errors.vendorName}</span>
                                                                                                ) : null} */}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.address ? ' text-danger ' : '')}>Vendor Address</span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="vendorAddress"
                                                                                                    type="text"
                                                                                                    className={'form-control'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="Enter"
                                                                                                    value={values.vendorAddress || ''}
                                                                                                    disabled
                                                                                                    autoFocus
                                                                                                />
                                                                                                {/* {errors.vendorAddress && touched.vendorAddress ? (
                                                                                            <span className="errorMsg">
                                                                                                {errors.vendorAddress}
                                                                                            </span>
                                                                                        ) : null} */}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.phoneNumber ? ' text-danger ' : '')}>Vendor Phone No</span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="vendorPhoneNo"
                                                                                                    type="text"
                                                                                                    className={'form-control'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="Enter"
                                                                                                    value={values.vendorPhoneNo || ''}
                                                                                                    disabled
                                                                                                    autoFocus
                                                                                                />
                                                                                                {/* {errors.vendorPhoneNo && touched.vendorPhoneNo ? (
                                                                                            <span className="errorMsg">
                                                                                                {errors.vendorPhoneNo}
                                                                                            </span>
                                                                                        ) : null} */}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </Table>
                                                                        </div>
                                                                    </Collapse>
                                                                </div>

                                                                {/* <div className="invDisplayRightBlock collapaseOuter">
                                                                    <h3 onClick={() => this.setState({ lineItems: !lineItems })}
                                                                        aria-controls="line-items"
                                                                        aria-expanded={lineItems}
                                                                        className={lineItems === false ? 'collapas-show' : 'collapas-hide'}
                                                                        style={{ marginBottom: '0' }}
                                                                    >
                                                                        Line Items
                                                                    </h3>

                                                                    <Collapse in={lineItems}>
                                                                        <div id="audit-details">
                                                                            <Table>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className="companyNameRblock">Line Item</span>
                                                                                            <span>&nbsp;(&nbsp;<a style={{ cursor: 'pointer' }} onClick={this.showAdditemsModal}>Add Item</a>&nbsp;)</span> 
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            {initialValues['invoiceItems'].length}
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </Table>
                                                                        </div>
                                                                    </Collapse>
                                                                </div> */}


                                                                <div className="invDisplayRightBlock collapaseOuter">
                                                                    <h3 onClick={() => this.setState({ invDetails: !invDetails })}
                                                                        aria-controls="invoice-details"
                                                                        aria-expanded={invDetails}
                                                                        className={invDetails === false ? 'collapas-show' : 'collapas-hide'}
                                                                        style={{ marginBottom: '0' }}
                                                                    >
                                                                        Invoice Details
                                                                    </h3>

                                                                    <Collapse in={invDetails}>
                                                                        <div id="audit-details">
                                                                            <Table>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.totalAmount ? ' text-danger ' : '')}>Subtotal</span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="subTotal"
                                                                                                    type="number"
                                                                                                    className={'form-control dollerInput'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="Enter"
                                                                                                    value={values.subTotal || 0.00}
                                                                                                    autoFocus
                                                                                                    disabled
                                                                                                />
                                                                                                {errors.subTotal && touched.subTotal ? (
                                                                                                    <span className="errorMsg">
                                                                                                        {errors.subTotal}
                                                                                                    </span>
                                                                                                ) : null}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                    {/* {values.taxType === 'TAX_INVOICE' ? ( */}
                                                                                        <tr>
                                                                                            <td>
                                                                                                <span className={"companyNameRblock" + (errorObj && errorObj.totalTax ? ' text-danger ' : '')}>Total Tax</span>
                                                                                            </td>

                                                                                            <td className="text-right">
                                                                                                <FormGroup controlId="formBasicText">
                                                                                                    <Field
                                                                                                        name="totalTax"
                                                                                                        type="text"
                                                                                                        className={'form-control dollerInput'}
                                                                                                        autoComplete="nope"
                                                                                                        placeholder="Enter"
                                                                                                        value={values.totalTax || 0.00}
                                                                                                        onBlur={e => {
                                                                                                            if(e.target.value) {
                                                                                                                const up = isNaN(parseFloat(e.target.value)) ? 0 : parseFloat(e.target.value);
                                                                                                                setFieldValue('totalTax', up.toFixed(2))
                                                                                                            } 
                                                                                                        }}
                                                                                                        onChange={e => {
                                                                                                            handleChange(e, setFieldValue("amountDue", this.cal(values.subTotal, e.target.value)));
                                                                                                        }}
                                                                                                        disabled={ values.taxType === 'TAX_INVOICE' ? false:true}
                                                                                                    />
                                                                                                    {errors.totalTax && touched.totalTax ? (
                                                                                                        <span className="errorMsg">
                                                                                                            {errors.totalTax}
                                                                                                        </span>
                                                                                                    ) : null}
                                                                                                    <FormControl.Feedback />
                                                                                                </FormGroup>
                                                                                            </td>

                                                                                        </tr>
                                                                                    {/* ) : null} */}

                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.balanceAmount ? ' text-danger ' : '')}>Amount Due</span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="amountDue"
                                                                                                    type="number"
                                                                                                    className={'form-control dollerInput'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="Enter"
                                                                                                    value={values.amountDue || 0.00}
                                                                                                    autoFocus
                                                                                                    disabled
                                                                                                />
                                                                                                {errors.amountDue && touched.amountDue ? (
                                                                                                    <span className="errorMsg">
                                                                                                        {errors.amountDue}
                                                                                                    </span>
                                                                                                ) : null}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>

                                                                                </tbody>
                                                                            </Table>
                                                                        </div>
                                                                    </Collapse>
                                                                </div>

                                                                <div className="invDisplayRightBlock collapaseOuter">
                                                                    <h3 onClick={() => this.setState({ invAdditionalNote: !invAdditionalNote })}
                                                                        aria-controls="inv-note"
                                                                        aria-expanded={invAdditionalNote}
                                                                        className={invAdditionalNote === false ? 'collapas-show' : 'collapas-hide'}
                                                                        style={{ marginBottom: '0' }}
                                                                    >
                                                                        Invoice Additional Note
                                                                    </h3>
                                                                    <Collapse in={invAdditionalNote}>
                                                                        <div id="audit-details">
                                                                            <Table>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>

                                                                                            <Field
                                                                                                name="comment"
                                                                                                component="textarea"
                                                                                                className={'additionalNoteArea'}
                                                                                                autoComplete="nope"
                                                                                                placeholder="Enter"
                                                                                                value={values.comment || ''}
                                                                                                autoFocus
                                                                                            />
                                                                                            {errors.comment && touched.comment ? (
                                                                                                <span className="errorMsg">
                                                                                                    {errors.comment}
                                                                                                </span>
                                                                                            ) : null}
                                                                                            <FormControl.Feedback />

                                                                                            {/* <textarea className="additionalNoteArea">
                                                                                            </textarea> */}
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </Table>
                                                                        </div>
                                                                    </Collapse>
                                                                </div>
                                                                <div className="invButtonsWrap text-center">

                                                                    <Button type="submit" bsStyle="primary blue-btn ml-5" >
                                                                        Save
                                                                    </Button>

                                                                </div>
                                                            </Tab>

                                                            <Tab eventKey="assets" title="Assets">
                                                                <div className="invDisplayRightBlock">
                                                                    <h3>
                                                                        Original Email
                                                                    </h3>
                                                                    <div id="assets" className="invRightBlockCon">
                                                                        <h4>{emailRequest && emailRequest.emailMsgFrom}</h4>
                                                                        <p>{emailRequest && emailRequest.emailMsgSubject}</p>
                                                                    </div>
                                                                </div>

                                                                <div className="invDisplayRightBlock">
                                                                    <h3>
                                                                        Original Invoice
                                                                    </h3>
                                                                    <div id="invoicePdf" className="invRightBlockCon">
                                                                        <ul>
                                                                            <li>
                                                                                {fileName && <div> <Image src={pdfIcon} width="15" /> {fileName} &nbsp; &nbsp; <span className="link" onClick={() => this.downloadurl(s3FileLocation)} ><IoMdDownload /> Download</span></div>}
                                                                            </li>
                                                                        </ul>

                                                                    </div>
                                                                </div>
                                                            </Tab>
                                                            <Tab eventKey="comments" title="Comments">
                                                                <div className="invDisplayRightBlock">
                                                                    {/* <h3>
                                                                        Write a New Comment
                                                                    </h3> */}
                                                                    {/* <div id="invoicePdf" className="invRightBlockCon">
                                                                        <Field
                                                                            name="auditTrailNote"
                                                                            component="textarea"
                                                                            className={'additionalNoteArea'}
                                                                            autoComplete="nope"
                                                                            placeholder="Enter"
                                                                            value={values.auditTrailNote || ''}
                                                                            autoFocus
                                                                        />
                                                                        {errors.auditTrailNote && touched.auditTrailNote ? (
                                                                            <span className="errorMsg">
                                                                                {errors.auditTrailNote}
                                                                            </span>
                                                                        ) : null}
                                                                        <FormControl.Feedback />

                                                                    </div> */}
                                                                </div>
                                                            </Tab>
                                                            <Tab eventKey="audittrails" title="Audit Trail">
                                                                <div className="invDisplayRightBlock">
                                                                    <h3>
                                                                        Audit Trail
                                                                    </h3>
                                                                    <div id="audit-details">
                                                                        <Table>
                                                                            <tbody>
                                                                                {auditTrails.length > 0 ? (auditTrails.map((audit, k) =>

                                                                                    < tr key={k}>
                                                                                        <td>
                                                                                            <span className="companyNameRblock">
                                                                                                {audit.userName}
                                                                                            </span>
                                                                                            <br />
                                                                                            {audit.activityDate}
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <span
                                                                                                className="statusRblock submit"
                                                                                            >
                                                                                                {audit.invoiceState}
                                                                                            </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                )) : null
                                                                                }
                                                                            </tbody>
                                                                        </Table>
                                                                    </div>
                                                                </div>
                                                            </Tab>
                                                        </Tabs>
                                                    </div>
                                                </Col>
                                            </Row>
                                        </Form>
                                    );

                                }}
                            </Formik>

                        )
                    }
                    <Modal
                        show={this.state.succesSubmitModal}
                        onHide={this.closesuccesSubmitModal}
                        className="payOptionPop"
                    >
                        <Modal.Body>
                            <Row>
                                <Col md={12} className="text-center">
                                    <Image src={SuccessIco} />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12} className="text-center">
                                    {/* <h5>Are you sure you want to unlock your card ?</h5> */}
                                    <h5>Invoice added successfully </h5>
                                </Col>
                            </Row>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button
                                className="but-gray m-auto"
                                type="button"
                                disabled={loader}
                                onClick={
                                    () => { this.closesuccesSubmitModal(); }
                                }
                            >Return</Button>
                        </Modal.Footer>
                    </Modal>

                    <Modal
                        show={this.state.showinvoiceWarningModal}
                        onHide={this.closeinvoiceWarningModal}
                        className="payOptionPop"
                    >
                        <Modal.Body>
                            <Row>
                                <Col md={12} className="text-center">
                                    <p> {this.state.errorMessage}</p>
                                </Col>
                            </Row>
                        </Modal.Body>
                        <Modal.Footer>

                            <Button
                                onClick={this.closeinvoiceWarningModal}
                                className="but-gray">
                                Return
                            </Button>
                        </Modal.Footer>
                    </Modal>

                    <Modal
                        show={this.state.showinvoiceWarningModal}
                        onHide={this.closeinvoiceWarningModal}
                        className="payOptionPop"
                    >
                        <Modal.Body>
                            <Row>
                                <Col md={12} className="text-center">
                                    <p> {this.state.errorMessage}</p>
                                </Col>
                            </Row>
                        </Modal.Body>
                        <Modal.Footer>

                            <Button
                                onClick={this.closeinvoiceWarningModal}
                                className="but-gray">
                                Return
                        </Button>
                        </Modal.Footer>
                    </Modal>



                    <Modal
                        show={this.state.showCreateTrmsModal}
                        onHide={this.hideCreatetremsModal}
                        className="payOptionPop"
                    >
                        <Modal.Header>
                            <Modal.Title>Create Term</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="modalBody content-body noTabs">
                                <Row>
                                    <Col md={12}>
                                        <Formik
                                            initialValues={initialValuestrems}
                                            validationSchema={addtremsSchema}
                                            onSubmit={this.addnewTrmshandleSubmit}
                                            enableReinitialize={true}
                                        >
                                            {({
                                                values,
                                                errors,
                                                touched

                                            }) => {
                                                return (
                                                    <Form novalidate>
                                                        <Row className="show-grid">
                                                            <Col xs={6} md={12}>
                                                                <FormGroup controlId="termName">
                                                                    <label>Term Name
                                                                        <span className="required">*</span>
                                                                    </label>
                                                                    <Field
                                                                        name="termName"
                                                                        type="text"
                                                                        className={`form-control`}
                                                                        autoComplete="nope"
                                                                        placeholder="Enter"
                                                                        value={values.termName}
                                                                        autoFocus

                                                                    />
                                                                    {errors.termName && touched.termName ? (
                                                                        <span className="errorMsg">{errors.termName}</span>
                                                                    ) : null}
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col md={12}>
                                                                <p style={{ paddingTop: '20px' }}><span className="required">*</span> This field is required.</p>
                                                            </Col>
                                                        </Row>


                                                        <div className="formButtonWrap">
                                                            <Row>
                                                                <Col className="text-center">
                                                                    <Button type="button" className="but-gray mr-3" onClick={this.hideCreatetremsModal}>Cancel</Button>
                                                                    <Button type="submit" className="blue-btn  m-l-5"  >Save</Button>
                                                                </Col>
                                                            </Row>
                                                        </div>
                                                    </Form>
                                                );
                                            }}
                                        </Formik>
                                    </Col>

                                </Row>

                            </div>

                        </Modal.Body>
                    </Modal>
                    <AdditemsModal showAdditemsModal={this.state.showAdditemsModal}
                        hideAdditemsModal={this.hideAdditemsModal}
                        addnewitemhandleSubmit={this.addnewitemhandleSubmit}
                        additemConfirm={this.state.additemConfirm}
                        Additemsloder={this.state.Additemsloder}
                        closeadditemsConfirmModal={this.closeadditemsConfirmModal}
                        errorItemMessage={this.state.errorItemMessage}
                        errorItemMessageModal={this.state.errorItemMessageModal}
                        hideAdditemsWorningModal={this.hideAdditemsWorningModal}

                    />
                    <Modal
                        show={this.state.addtrmsConfirm}
                        onHide={this.closeaddtrmsConfirmModal}
                        className="payOptionPop"
                    >
                        <Modal.Body>
                            <Row>
                                <Col md={12} className="text-center">
                                    <Image src={SuccessIco} />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12} className="text-center">
                                    <h5> Term created successfully</h5>
                                </Col>
                            </Row>
                        </Modal.Body>
                        <Modal.Footer>

                            <Button
                                onClick={() => {
                                    this.closeaddtrmsConfirmModal();
                                }}
                                className="but-gray m-auto">
                                Return
                        </Button>
                        </Modal.Footer>
                    </Modal>


                    {/* Add Vendor Modal */}
                    <Modal
                        show={this.state.showAddVendor}
                        onHide={this.hideAddVendorModalHandler}
                        className="payOptionPop"
                    >
                        <Modal.Header closeButton>
                            <Modal.Title>Add Vendor</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="modalBody content-body noTabs">
                                <AddBusinessVendors
                                    handleAddConfirMmsg={this.handleAddConfirMmsg}
                                    businessId={this.props.selectedBusinessID}
                                    handelAddModalClose={this.hideAddVendorModalHandler}
                                />
                            </div>
                        </Modal.Body>

                    </Modal>
                    {/*======  Add confirmation popup  ===== */}
                    <Modal
                        show={this.state.addConfirMmsg}
                        onHide={this.handleAddConfirMmsgClose}
                        className="payOptionPop"
                    >
                        <Modal.Body>
                            <Row>
                                <Col md={12} className="text-center">
                                    <Image src={SuccessIco} />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12} className="text-center">
                                    <h5>Vendor has been successfully Added</h5>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12} className="text-center">
                                    <Button
                                        onClick={this.handleAddConfirMmsgClose}
                                        className="but-gray m-auto"
                                    >
                                        Return
                                </Button>
                                </Col>
                            </Row>
                        </Modal.Body>
                    </Modal>


                </div>

            </div >
        );
    }
}
AddInvoice.propTypes = {
    selectedID: PropTypes.number,
    selectedBusinessID: PropTypes.number,
    emailAttachID: PropTypes.number,
    selectedStatus: PropTypes.string,
    s3FileLocation: PropTypes.string,
    modalCloseReviewInvoice: PropTypes.func,
    getAllList: PropTypes.func,
    emailRequest: PropTypes.object,
    fileName: PropTypes.string,
};



export default AddInvoice;
