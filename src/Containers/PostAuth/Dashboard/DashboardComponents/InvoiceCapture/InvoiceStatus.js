export const INVOICE_STATUS_ALL = 'all';
export const INVOICE_STATUS_UNOPENED = 'unopened';
export const INVOICE_STATUS_PENDING_VARIFICATION = 'pending-verification';
export const INVOICE_STATUS_VERIFIED = 'verified';
export const INVOICE_STATUS_ERROR_REPORTED = 'error-reported';

