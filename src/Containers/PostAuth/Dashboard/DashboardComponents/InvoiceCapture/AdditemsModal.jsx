/* eslint-disable quotes */
/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
/* eslint-disable */
import { Field, Form, Formik } from "formik";
import React, { Component } from "react";
import { Button, Col,  FormControl, FormGroup, Image, Modal, Row } from "react-bootstrap";
import * as Yup from "yup";
import SuccessIco from "./../../../../../assets/success-ico.png";

const initialValues = {
    itemName: "",
    description: "",
    unitPrice: 0,
    
};

const additemsSchema = Yup.object().shape({
    itemName: Yup
        .string()
        .trim("Please remove whitespace")
        .strict()
        .required("Please enter item name"),
    description: Yup
        .string()
        .trim("Please remove whitespace")
        .strict()
        .required("Please enter description"),
    unitPrice: Yup.number().required("Please enter unit price").test(
        'unitPrice',
        'Unit price upto two decimal places',
        function (value) {
            if (value) {
                let isValid = false;
                if (/^\d*(\.\d{0,2})?$/.test(value)) {
                    isValid = true;
                }

                return isValid;
            }
            return true;
        }
    ),
    
});
class AdditemsModal extends Component {
    render() {
        return (
            <div>
               
                <Modal
                    show={this.props.showAdditemsModal}
                    onHide={this.props.hideAdditemsModal}
                    className="payOptionPop"
                >
                    <Modal.Header>
                        <Modal.Title>Add item</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="modalBody content-body noTabs">
                            <Row>
                                <Col md={12}>
                                    <Formik
                                        initialValues={initialValues}
                                        validationSchema={additemsSchema}
                                        onSubmit={this.props.addnewitemhandleSubmit}
                                        enableReinitialize={true}
                                    >
                                        {({
                                            values,
                                            errors,
                                            touched

                                        }) => {
                                            return (
                                                <Form novalidate>
                                                    <Row className="show-grid">
                                                        <Col xs={6} md={6}>
                                                            <FormGroup controlId="itemName">
                                                                <span>Item Name
                                                                        <span className="required">*</span>
                                                                </span>
                                                                <Field
                                                                    name="itemName"
                                                                    type="text"
                                                                    className={`form-control`}
                                                                    autoComplete="nope"
                                                                    placeholder="Enter"
                                                                    value={values.itemName}
                                                                    autoFocus

                                                                />
                                                                {errors.itemName && touched.itemName ? (
                                                                    <span className="errorMsg">{errors.itemName}</span>
                                                                ) : null}
                                                                <FormControl.Feedback />
                                                            </FormGroup>
                                                           

                                                           
                                                        </Col>
                                                        <Col xs={6} md={6}>
                                                            <FormGroup controlId="unitPrice">
                                                                <span>Unit Price
                                                                        <span className="required">*</span>
                                                                </span>
                                                                <Field
                                                                    name="unitPrice"
                                                                    type="number"
                                                                    className={`form-control`}
                                                                    autoComplete="nope"
                                                                    placeholder="Enter"
                                                                    value={values.unitPrice}


                                                                />
                                                                {errors.unitPrice && touched.unitPrice ? (
                                                                    <span className="errorMsg">{errors.unitPrice}</span>
                                                                ) : null}
                                                                <FormControl.Feedback />
                                                            </FormGroup>
                                                            </Col>
                                                        <Col xs={12} md={12}>
                                                        <FormGroup controlId="description">
                                                                <span>Description
                                                                        <span className="required">*</span>
                                                                </span>
                                                                <Field
                                                                    name="description"
                                                                    component="textarea"
                                                                    className={`form-control`}
                                                                    autoComplete="nope"
                                                                    placeholder="Enter"
                                                                    value={values.description}


                                                                />
                                                                {errors.description && touched.description ? (
                                                                    <span className="errorMsg">{errors.description}</span>
                                                                ) : null}
                                                                <FormControl.Feedback />
                                                            </FormGroup>
                                                           
                                                        </Col>

                                                    </Row>
                                                    <Row>
                                                        <Col md={12}>
                                                            <p style={{ paddingTop: '20px' }}><span className="required">*</span> These fields are required.</p>
                                                        </Col>
                                                    </Row>


                                                    <div className="formButtonWrap">
                                                        <Row>
                                                            <Col className="text-center">
                                                                <Button type="button" className="but-gray" onClick={this.props.hideAdditemsModal}>Cancel</Button>
                                                                <Button type="submit" className="blue-btn  m-l-5"  disabled={this.props.Additemsloder} >Save</Button>
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                </Form>
                                            );
                                        }}
                                    </Formik>
                                </Col>

                            </Row>

                        </div>

                    </Modal.Body>
                </Modal>
                <Modal
                        show={this.props.additemConfirm}
                        onHide={this.props.closeadditemsConfirmModal}
                        className="payOptionPop"
                    >
                        <Modal.Body>
                            <Row>
                                <Col md={12} className="text-center">
                                    <Image src={SuccessIco} />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12} className="text-center">
                                    <h5> Item added successfully</h5>
                                </Col>
                            </Row>
                        </Modal.Body>
                        <Modal.Footer>

                            <Button
                                onClick={this.props.closeadditemsConfirmModal}
                                className="but-gray">
                                Return
                        </Button>
                        </Modal.Footer>
                    </Modal>
                    <Modal
                        show={this.props.errorItemMessageModal}
                        onHide={this.props.hideAdditemsWorningModal}
                        className="payOptionPop"
                    >
                        <Modal.Body>
                            <Row>
                            <Col md={12} className="text-center">
                                <h5> {this.props.errorItemMessage}</h5>
                            </Col>
                            </Row>
                        </Modal.Body>
                        <Modal.Footer>

                            <Button
                                onClick={this.props.hideAdditemsWorningModal}
                                className="but-gray">
                                Return
                        </Button>
                        </Modal.Footer>
                    </Modal>
            </div>
        );
    }

}

export default AdditemsModal;
