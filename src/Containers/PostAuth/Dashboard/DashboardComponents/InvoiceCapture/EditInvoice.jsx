/* eslint-disable quotes */
/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
/* eslint-disable */
import { ErrorMessage, Field, FieldArray, Form, Formik } from 'formik';
import PropTypes from 'prop-types';
import React, { Component, Fragment } from 'react';
import { Button, Col, Collapse, FormControl, FormGroup, Image, Modal, Row, Tab, Table, Tabs } from 'react-bootstrap';
import DatePicker from 'react-date-picker';
import { IoMdDownload } from 'react-icons/io';
import { Link } from 'react-router-dom';
import * as Yup from 'yup';
import crossSmIcon from './../../../../../assets/crossSmIcon.png';
import pdfIcon from './../../../../../assets/pdficon.png';
import SuccessIco from './../../../../../assets/success-ico.png';
import LoadingSpinner from './../../../../../Components/LoadingSpinner/LoadingSpinner';
//import { pdfIcon as FaFilePdf } from 'react-icons/fa';
import { CountryService } from './../../../../../services/country.service';
import { UserBusiness } from './../../../../../services/userBusiness.service';
import axios from './../../../../../shared/eaxios';
import AddBusinessVendors from './../Business/BusinessVendors/AddBusinessVendors';
import AdditemsModal from './AdditemsModal';
import './EditInvoice.scss';
import './VerifyInvoice.scss';
import { formatDate } from './../../../../../shared/formatDate';

const initialValues = {
    invoiceNumber: '',
    invoiceDuedate: '',
    invoiceDate: '',
    poNumber: '',
    terms: '',
    businessName: '',
    businessAddress1: '',
    businessPhone: '',
    vendorName: '',
    vendorAddress: '',
    vendorPhoneNo: '',
    subTotal: '0.00',
    amountDue: '0.00',
    totalTax: '0.00',
    comment: '',
    invoiceItems: [],
    emailAttachment: {},
    errorObj: {},
    businessData: null,
    businessDataError: null,
    businessLoader: false,
    itemlist: [],
    showAdditemsModal: false,
    errorItemMessage: "",
    additemConfirm: false,
    Additemsloder: false,
    auditTrailNote: '',
    taxType: null, // TAX_LINE & TAX_INVOICE
    invoiceItems: [
        {
            name: '',
            description: '',
            quantity: 1,
            unitPrice: parseFloat(0).toFixed(2),
            taxPercent: parseFloat(0).toFixed(2),
            taxName: 'Tax',
            subtotal: 0.00,
            isTaxable: true,
            itemMaster: 0.00,
            showflag: true,
            expencedisable:
                false,
            unitDisFlag: false,
        }
    ],

};

const addInvoiceSchema = Yup.object().shape({
    invoiceNumber: Yup
        .string()
        .required('Please enter invoice number'),
    invoiceDuedate: Yup
        .string('Please enter invoice due date')
        .typeError('Please enter invoice due date')
        .required('Please enter invoice due date'),
    invoiceDate: Yup
        .string('Please enter invoice date')
        .typeError('Please enter invoice date')
        .required('Please enter invoice date'),
    poNumber: Yup
        .string(),
    //.required('Please enter PO number'),
    terms: Yup
        .string(),
    //.required('Please enter terms'),
    businessName: Yup
        .string(),
    businessAddress1: Yup
        .string(),
    businessPhone: Yup
        .string(),
    vendorName: Yup
        .string(),
    // .required('Please enter vendor name'),
    vendorAddress: Yup
        .string(),
    // .required('Please enter vendor address'),
    vendorPhoneNo: Yup
        .string(),
    comment: Yup
        .string(),
    auditTrailNote: Yup
        .string(),
    // .required('Please enter vendor phone no.'),
    invoiceItems: Yup.array().of(Yup.object().shape({
        name: Yup.string().required("Please enter name"),
        description: Yup.string(),
        //.required("Please enter description"),
        quantity: Yup.number()
            .max(99999, "Quantity cannot be more than 5 digits.")
            .required("Please enter quantity")
            .typeError('Quantity should be a number')
            .moreThan(0, 'Quantity should be greater than 0'),
        unitPrice: Yup.string()
            .required("Please enter unit price")
            .typeError('Unit price should be number')
            //.moreThan(0, 'Unit Price should be greater than 0.00')
            //.max(99999, "maximum unit price can't more than 5 digit")

            .test(//5 digit with decimal
                'unitPrice',
                'Maximum unit price cannot be more than 5 digits with two decimals.',
                function (value) {
                    if (value) {
                        let isValid = false;
                        if (/^[+]?[0-9]\d{0,4}(\.\d{1,2})?%?$/.test(value)) {
                            isValid = true;
                        }
                        return isValid;
                    }
                    return true;
                }
            )
            .test(//upto two decimal places
                'unitPrice',
                'Unit price should be decimal places',
                function (value) {
                    if (value) {
                        let isValid = false;
                        if (/^[0-9]*\.[0-9][0-9]$/.test(value)) {
                        //if (/^\d*(\.\d{0,2})?$/.test(value)) {
                            isValid = true;
                        }
                        return isValid;
                    } 
                    return true;
                }
        ).test(//can not start with 0 but 0.01 valid
            'unitPrice',
            'Unit price can not start with 0',
            function (value) {
                if (value) {
                    let isValid = false;
                    if  (/^(([1-9][0-9]*)|((0|[1-9]\d*)(\.\d+)))$/.test(value)) {
                        isValid = true;
                    }
                    return isValid;
                }
                return true;
            }
        ).test(//greater than 0.00
            'unitPrice',
            'Unit Price should be greater than 0.00',
            function (value) {
                if (value) {
                    let isValid = false;
                    if (/^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$/.test(value)) {
                        isValid = true;
                    }
                    return isValid;
                }
                return true;
            }
                
        ),

        // taxPercent: Yup.number()
        //     .max(100, "maximum tax percent can't more than 100")
        //     .typeError('Tax rate should be a number')
        //     //.moreThan(0, 'Tax rate should be greater than 0.00')
        //     .test(
        //         'taxPercent',
        //         'Tax rate upto two decimal places',
        //         (value) => {
        //             if (value) {
        //                 let isValid = false;
        //                 if (/^\d*(\.\d{0,2})?$/.test(value)) {
        //                     isValid = true;
        //                 }

        //                 return isValid;
        //             }
        //             return true;
        //         }
        //     ),
        taxPercent: Yup.string()
            .typeError('Tax rate should be a number')
            .test(
                'taxPercent',
                'Tax rate upto two decimal places or invalid input',
                (value) => {

                    if (value) {
                        let isValid = false;
                        //if (/^\d*(\.\d{0,2})?$/.test(value)) {
                        if (/^(?:100|\d{1,2})(?:\.\d{1,2})?$/.test(value)) {
                            isValid = true;
                        }

                        return isValid;
                    }
                    return true;
                }
            )
            .test(//can not start with 0 but 0.01 valid
                'taxPercent',
                'Tax rate can not start with 0 but you can put 0.00',
                function (value) {
                    if (value) {
                        let isValid = false;
                        if (/^(([1-9][0-9]*)|((0|[1-9]\d*)(\.\d+)))$/.test(value)) {
                            isValid = true;
                        }
                        return isValid;
                    }
                    return true;
                }
            )
            .test(//can not start with 0 but 0.01 valid
                'taxPercent',
                // 'Tax rate cannot be greater than 100 or invalid input',
                'Tax rate cannot be greater than 100',
                function (value) {

                    if (value) {
                        let isValid = false;
                        if (parseFloat(value) <= parseFloat(100.00)) {
                            isValid = true;
                        }
                        return isValid;
                    }
                    return true;
                }
            )
            ,



        taxName: Yup
            .string()
        .required("Please enter tax name"),

        subtotal: Yup.number().required('Please enter total amount')
    })),
    subTotal: Yup
        .number()
        .required('Please enter subtotal')
        .test(
            'subTotal',
            'Subtotal upto two decimal places',
            function (value) {
                if (value) {
                    let isValid = false;
                    if (/^\d*(\.\d{0,2})?$/.test(value)) {
                        isValid = true;
                    }

                    return isValid;
                }
                return true;
            }
        ),
    totalTax: Yup
        .string()
        // .required('Please enter total tax')
        .typeError('Total tax should be number')
        .test(//upto two decimal places
            'totalTax',
            'Total tax upto two decimal places',
            function (value) {
                if (value) {
                    let isValid = false;
                    if (/^\d*(\.\d{0,2})?$/.test(value)) {
                        isValid = true;
                    }
                    return isValid;
                }
                return true;
            }
        )
        .test(//5 digit with decimal
            'totalTax',
            'Maximum total tax can not more than 5 digit with two decimal',
            function (value) {
                if (value) {
                    let isValid = false;
                    if (/^[+]?[0-9]\d{0,4}(\.\d{1,2})?%?$/.test(value)) {
                        isValid = true;
                    }
                    return isValid;
                }
                return true;
            }
        ).test(//can not start with 0 but 0.01 valid
            'totalTax',
            'Total tax can not start with 0, you can put 0.00',
            function (value) {
                if (value) {
                    let isValid = false;
                    if (/^(([1-9][0-9]*)|((0|[1-9]\d*)(\.\d+)))$/.test(value)) {
                        isValid = true;
                    }
                    return isValid;
                }
                return true;
            }
        ),
    amountDue: Yup
        .number()
        .required('Please enter due amount')
        .test(
            'amountDue',
            'Due amount upto two decimal places',
            function (value) {
                if (value) {
                    let isValid = false;
                    if (/^\d*(\.\d{0,2})?$/.test(value)) {
                        isValid = true;
                    }

                    return isValid;
                }
                return true;
            }
        )
});

const initialValuestrems = {
    termName: '',
    netDueDays: '',
    discountPercent: '',
    discountApplicableDays: ''
};
const addtremsSchema = Yup.object().shape({
    termName: Yup
        .string()
        .required('Please enter  term name'),
    netDueDays: Yup
        .number(),
    //.required('Please enter net due days'),
    discountPercent: Yup
        .number()
        //.required('Please enter discount percent')
        .test(
            'discountPercent',
            'Discount percent upto two decimal places',
            function (value) {
                if (value) {
                    let isValid = false;
                    if (/^\d*(\.\d{0,2})?$/.test(value)) {
                        isValid = true;
                    }

                    return isValid;
                }
                return true;
            }
        ),
    discountApplicableDays: Yup
        .string()
    //.required('Please enter  discount applicable days')
});




class EditInvoice extends Component {
    state = {

        errorItemMessage: "",
        errorItemMessageModal: false,
        additemConfirm: false,
        showAddVendor: false,
        showAdditemsModal: false,
        Additemsloder: false,
        showCreateTrmsModal: false,
        errorMessage: '',
        showinvoiceWarningModal: false,
        succesSubmitModal: false,
        loader: false,
        termLoader: false,
        invoiceDetails: true,
        customerVendor: true,
        lineItems: true,
        invDetails: true,
        invAdditionalNote: true,
        invComment: true,
        businessDetails: {},
        vendorLists: [],
        termLists: [],
        emailAttachmentId: '',
        invoiceId: '',
        auditTrails: [],
        tremserrorMessage: '',
        trmsloader: false,
        addtrmsConfirm: false,
        itemlist: [],
        tempitemlist: [],
        key: 'expense',
        Accountslist: [],
        tempAccountslist: [],
        itemsdetails: {},
        showedititemsModal: false,
        edititemConfirm: false,
        edititemsloder: false,
        erroreditItemMessage: '',
        erroreditItemMessagemodal: false,
        itemid: '',
        newaddItemshow: true,
        seletedItemName: '',
        expeTitalId: '',
        selectedInvoiceDate: null,
        vendorLoader: false,
        isTaxableState: true
    }
    constructor(props) {
        super(props);
        this.getAccountsref = React.createRef();
        this.getitemref = React.createRef();
        this.getexpenceref = React.createRef()
    }
    cal = (nun1, num2) => {
        // console.log(nun1)
        // console.log(num2)
        if (num2 !== '') {
            const val = (parseFloat(nun1) + parseFloat(num2));
            // console.log('1')
            return parseFloat(val).toFixed(2);
        } else {
            // console.log('2')
            return parseFloat(nun1).toFixed(2);
        }

    }


    taxTypeCalculation = ({
        invoiceItems
    }, taxType, setFieldValue) => {
        setFieldValue('totalTax', (0.00).toFixed(2));
        if (taxType === 'TAX_LINE') {
            this.setState({
                isTaxableState: true
            })
            console.log(taxType, ' ### 1 invoiceItems### ', invoiceItems)
            const taxLineTotalItems = invoiceItems.map(invoice => this.getTotal(invoice.unitPrice, invoice.quantity, 0));
            console.log(taxType, ' ### 1 taxLineTotalItems ### ', taxLineTotalItems)
            const subTotal = taxLineTotalItems.reduce((tot, value) => (tot + value));
            setFieldValue('subTotal', subTotal.toFixed(2));

            const taxLineTotTaxlItems = invoiceItems.map(invoice => this.getTotalTax(invoice.unitPrice, invoice.quantity, invoice.taxPercent));
            const totalTax = taxLineTotTaxlItems.reduce((tot, value) => (tot + value));
            setFieldValue('totalTax', totalTax.toFixed(2));

            const amountDue = subTotal + totalTax;
            setFieldValue('amountDue', amountDue.toFixed(2));

        } else if (taxType === 'TAX_INVOICE') {
            this.setState({
                isTaxableState: false
            })
            console.log(taxType, ' ### 2 invoiceItems### ', invoiceItems);
            const taxInvoiceTotalItems = invoiceItems.map(invoice => this.getTotal(invoice.unitPrice, invoice.quantity, 0));
            console.log(taxType, ' ### 2 taxInvoiceTotalItems### ', taxInvoiceTotalItems);
            const subTotal = taxInvoiceTotalItems.reduce((tot, value) => (tot + value));
            setFieldValue('subTotal', subTotal.toFixed(2));
            // set amount due
            setFieldValue('amountDue', this.cal(subTotal.toFixed(2), 0.00));

        }
    }

    /* Added reuseable function */
    getTotal = (unitPrice, qty, taxPercent) => {
        unitPrice = isFinite(unitPrice) ? unitPrice : 0.00;
        qty = isFinite(qty) ? qty : 0;
        taxPercent = isFinite(taxPercent) ? taxPercent : 0;

        const total = parseFloat(unitPrice * qty);
        let taxAmount = 0.00;
        if (taxPercent > 0) {
            taxAmount = this.getTaxAmount(total, taxPercent)
        }

        return (total + taxAmount);
    }
    /* */
    getTaxAmount = (amount, percent) => {
        amount = isFinite(amount) ? amount : 0.00;
        percent = isFinite(percent) ? percent : 0;

        return (percent / 100) * amount;
    }

    /* */
    getTotalTax = (unitPrice, qty, taxPercent) => {
        let totaltax = 0.00;
        const tempTotal = parseFloat(unitPrice * qty);
        const percentCalc = (taxPercent / 100) * tempTotal;
        return totaltax = parseFloat(totaltax) + parseFloat(percentCalc);
    }

    setsubtotal = (arry, index, tag, value, setFieldValue) => {
        let grandsubtotal = 0;
        let totaltax = 0;
        if (tag === 'Quantity') {
            arry[index].quantity = isNaN(value) ? 0 : value
        } else if (tag === 'Unit price') {
            arry[index].unitPrice = isNaN(value) ? 0.00 : value
        } else if (tag === 'Tax Rate') {
            arry[index].taxPercent = isNaN(value) ? 0.00 : value
        }
        // else{
        //     arry[index].taxName = value ? '' : value
        // }
        arry.map((inv, index) => {
            const tempTotal = parseFloat(inv.unitPrice * inv.quantity);
            const percentCalc = (inv.taxPercent / 100) * tempTotal;

            //const subtotal = (inv.unitPrice * inv.quantity).toFixed(2);
            //const subtotal = (parseFloat(tempTotal) + parseFloat(percentCalc)).toFixed(2);
            const subtotal = (parseFloat(tempTotal)).toFixed(2);
            grandsubtotal = parseFloat(grandsubtotal) + parseFloat(subtotal);

            totaltax = parseFloat(totaltax) + parseFloat(percentCalc);
            const amountDue = parseFloat(grandsubtotal) + parseFloat(totaltax);
            setFieldValue('totalTax', totaltax.toFixed(2));
            console.log(':::::::::::Edit Invoice::::::::::::::', totaltax);
            setFieldValue('amountDue', amountDue.toFixed(2));
        });
        return grandsubtotal.toFixed(2);
    }

    settotalamount = (values, index, tag, value) => {
        const arry = values.invoiceItems
        console.log('totalTax', values.totalTax)
        const totalTax = values.totalTax === '' ? 0.00 : values.totalTax;
        let totalamout = 0
        if (tag === 'Quantity') {
            arry[index].quantity = isNaN(value) ? 0 : value
        } else if (tag === 'Unit price') {
            arry[index].unitPrice = isNaN(value) ? 0.00 : value
        } else if (tag === 'Tax Rate') {
            arry[index].taxPercent = isNaN(value) ? 0.00 : value
        }
        arry.map((inv, index) => {
            const tempTotal = parseFloat(inv.unitPrice * inv.quantity);
            const percentCalc = (inv.taxPercent / 100) * tempTotal;

            //const subtotal = (inv.unitPrice * inv.quantity).toFixed(2);

            const subtotal = (parseFloat(tempTotal) + parseFloat(percentCalc)).toFixed(2);

            totalamout = parseFloat(totalamout) + parseFloat(subtotal);
        });
        return (parseFloat(totalTax) + parseFloat(totalamout)).toFixed(2);
    }


    editItem = (index, value) => {
        console.log(index)
        console.log(value)

    }



    setdescription = (param) => {
        const item = this.state.itemlist.filter(item => Number(item.id) === Number(param));
        return item[0] && item[0].description
    }
    setItemMasterId = (param) => {
        const item = this.state.itemlist.filter(item => Number(item.id) === Number(param));
        const obj = { id: item[0].id }
        console.log('setItemMasterId', obj);
        return obj
    }
    setunitPrice = (param) => {
        const item = this.state.itemlist.filter(item => Number(item.id) === Number(param));
        // console.log(item)
        return item[0] && item[0].unitPrice.toFixed(2)
    }


    displayError = (err) => {
        let errorMessage = '';
        try {
            errorMessage = err.data.message ? err.data.message : err.data.error_description;
        } catch (err) {
            errorMessage = "No records found."
        }
        return errorMessage;
    }

    handelError = (err) => {
        let errorMessage = '';
        try {
            errorMessage = err.data.message ? err.data.message : err.data.error_description;
        } catch (err) {
            errorMessage = "No records found."
        }
        return errorMessage;
    }

    showAdditemsModal = () => {
        this.setState({ showAdditemsModal: true })
    }

    hideAdditemsModal = () => {
        this.setState({ showAdditemsModal: false })
    }
    showAdditemsModal = () => {
        this.setState({ showAdditemsModal: true })
    }

    hideAdditemsModal = () => {
        this.setState({ showAdditemsModal: false })
    }
    addnewitemhandleSubmit = (param) => {
        console.log(param)
        this.setState({
            Additemsloder: true
        }, () => {
            axios.post(`/autofy/saveItem/${this.props.selectedBusinessID}`, param)
                .then(res => {
                    this.getItemlist();
                    this.setState({
                        Additemsloder: false,
                        showAdditemsModal: false,
                        additemConfirm: true
                    })

                })
                .catch(err => {
                    let errorItemMessage = this.handelError(err);
                    this.setState({
                        errorItemMessage,
                        showAdditemsModal: false,
                        errorItemMessageModal: true,
                        Additemsloder: false

                    })
                }
                )

        })
        console.log(param)


    }

    closeadditemsConfirmModal = () => {
        this.setState({ additemConfirm: false })
    }
    hideAdditemsWorningModal = () => {
        this.setState({ errorItemMessageModal: false })
    }

    getItemlist = () => {
        this.setState({ loader: true }, () => {
            axios.get(`autofy/getItems/${this.props.selectedBusinessID}`)
                .then(res => {
                    const itemlist = res.data;
                    this.setState({
                        itemlist,
                        loader: false
                    });

                })
                .catch(err => {

                });
        });
    };

    getDetailsList = () => {

        this.setState({
            detailsLoader: true
        });

        axios
            .get(`invoice/${this.props.selectedID}/${this.props.selectedBusinessID}`)
            .then(({ data }) => {
                let errorObj = {};
                try {
                    errorObj = JSON.parse(data.error.errorJson);
                } catch (err) {
                    console.error('Unable to parse json object', err);
                }
                console.log('error object #', errorObj);


                console.log('+++++++++++++++++++++++++++ editing data +++++++++++++++++++++++++++++++++', data);
                {
                data.invoiceItems[0]['isTaxable'] == true
                    ? this.setState({
                        isTaxableState: true
                    })
                    : this.setState({
                        isTaxableState: false
                    })
                }

                initialValues['taxType'] = (data.invoiceItems[0]['isTaxable'] == true ? 'TAX_LINE' : 'TAX_INVOICE');
                initialValues['invoiceNumber'] = data.invoiceNumber;
                initialValues['invoiceDate'] = new Date(data.invoiceDate);
                initialValues['invoiceDuedate'] = new Date(data.invoiceDueDate);
                initialValues['poNumber'] = data.invoicePO;
                initialValues['terms'] = data.termRequest && data.termRequest.id ? data.termRequest.id : ''
                // EXP
                initialValues['subTotal'] = data.subTotal.toFixed(2);
                initialValues['vendorName'] = data.customerVendorId;
                initialValues['amountDue'] = (data.subTotal + data.totalTax).toFixed(2);
                initialValues['totalTax'] = data.totalTax.toFixed(2);

                console.log('||||||||||||||||', data.invoiceItems);

                data.invoiceItems.map((item) => {
                    if (item['taxPercent'] === 0 ){
                        item['taxPercent'] = item.taxPercent.toFixed(2);
                    }
                   
                    item['showflag'] = true;
                    item['expencedisable'] = false;
                })
                initialValues['invoiceItems'] = data.invoiceItems;//*
                initialValues['comment'] = data.comment;
                initialValues['businessName'] = data.businessName;
                initialValues['businessAddress1'] = data.businessAddress1;
                initialValues['businessPhone'] = data.businessPhone;
                const vendors = this.state.vendorLists;
                const vendor = vendors.filter(v => {
                    return Number(v.id) === Number(initialValues['vendorName']);
                });
                initialValues['vendorAddress'] = vendor[0] && vendor[0].address;
                initialValues['vendorPhoneNo'] = vendor[0] && vendor[0].phoneNumber;
                const auditTrails = data.auditTrails;
                const emailAttachment = data.emailAttachment;

                this.setState({
                    emailAttachment,
                    auditTrails,
                    emailAttachmentId: data.emailAttachmentId,
                    detailsLoader: false,
                    detailsList: data,
                    errorObj

                });

                //initialerReport['invoiceItems'] = res.data.invoiceItems;
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    detailsError: errorMsg,
                    detailsLoader: false

                });

                setTimeout(() => {
                    this.setState({ detailsError: null });
                }, 5000);

            });
    }


    deleteallrow = (value, setFieldValue) => {

        console.log(value)
        setFieldValue("invoiceItems", [])
        // setFieldValue("invoiceItems", [{ id: null, name: '', description: '', quantity: 0, unitPrice: 0, subtotal: 0, isTaxable: false, tax: null, itemMaster: '' }])
        // setFieldValue("invoiceItems", [{ id: null, name: '', description: '', quantity: 0, unitPrice: 0, subtotal: 0, isTaxable: false, tax: null, itemMaster: '', showflag: true, expencedisable : false }])
        setFieldValue("subTotal", '0.00')
        setFieldValue("amountDue", this.cal(0.00, value.totalTax))

    }
    deletSingleRow = (index, { invoiceItems, totalTax }, setFieldValue, callback) => {
        let totalAmount = 0;
        let grandsubtotal = 0;
        let totaltax = 0;
        let amountDue = 0;

        if (invoiceItems.length) {
            invoiceItems.map((data, i) => {
                if (i !== index) {

                    const tempTotal = (data.unitPrice * data.quantity);
                    const percentCalc = (data.taxPercent / 100) * tempTotal;

                    // const subTotal = (data.unitPrice * data.quantity).toFixed(2);
                    // totalAmount = parseFloat(totalAmount) + parseFloat(subTotal);

                    const subTotal = (parseFloat(tempTotal)).toFixed(2);
                    grandsubtotal = parseFloat(grandsubtotal) + parseFloat(subTotal);
                    totaltax = parseFloat(totaltax) + parseFloat(percentCalc);

                    totalAmount = parseFloat(totalAmount) + parseFloat(subTotal);
                    amountDue = parseFloat(grandsubtotal) + parseFloat(totaltax);
                }
            });
        }

        // setFieldValue("subTotal", totalAmount.toFixed(2));
        // setFieldValue("amountDue", this.cal(totalAmount, totalTax))

        setFieldValue('totalTax', totaltax.toFixed(2));
        setFieldValue("subTotal", totalAmount.toFixed(2));
        setFieldValue('amountDue', amountDue.toFixed(2));
        callback(index);
    }



    handleSubmit = (values) => {
        //values.invoiceItems = values.invoiceItems.map((item, i) => { item.isTaxable = this.state.isTaxableState; return item });
        let temp = values.invoiceItems.map((item, i) => { item.isTaxable = this.state.isTaxableState; return item });

        for (const element of temp) {
            element.id = null
        }
        //temp[0].id = null;/////////////////////////////////////////////////////////////
        const param = {
            // "emailAttachmentId" : this.state.emailAttachmentId,
            'emailAttachmentId': 37761,
            'customerVendorId': values.vendorName,
            'invoiceNumber': values.invoiceNumber,
            'invoicePO': values.poNumber,
            'invoiceTerm': values.terms,
            "totalTax": values.totalTax,
            "totalAmount": values.amountDue,
            "subTotal": values.subTotal,
            'invoiceDate': formatDate(values.invoiceDate),
            'invoiceDueDate': formatDate(values.invoiceDuedate),
            'invoiceState': 'UPLOADED',
            'termId': values.terms,
            'invoiceItems': temp,
            'comment': values.comment,
            'auditTrailNote': values.auditTrailNote
        };
        param.isLineItemTaxEnabled = this.state.isTaxableState;
        // param.isTaxable = values.taxType === 'TAX_LINE' ? true : false;
        console.log('-----------------------Invoive Edit (submit)-------------------------------', param);

        axios.put(`invoice/capture/unvoid/${this.state.detailsList.id}/${this.props.selectedBusinessID}`, param)
            .then(res => {
                this.showsuccesSubmitModal();
            })
            .catch(err => {
                let errorMessage = this.handelError(err);
                this.setState({
                    errorMessage,
                    succesSubmitModal: false,
                    showinvoiceWarningModal: true

                });

            });


    }

    getBusiness = () => {
        this.setState({
            loader: true
        }, (() => {
            const userBusiness = new UserBusiness();
            userBusiness.getBusinessDetails()
                .then(bDetails => {
                    const businessDetails = bDetails;
                    ///console.log(bDetails)
                    initialValues['customerPhNo'] = bDetails.businessPhone;
                    this.setState({
                        businessDetails
                    }, () => {

                        this.getSCCName(
                            bDetails.businessName,
                            bDetails.businessAddress1,
                            bDetails.businessCountryCode,
                            bDetails.businessStateCode,
                            bDetails.businessCityId,
                            bDetails.businessZipcode
                        );

                    });


                });
        }));

    }

    getSCCName = (businessName, Address1, countryReq, stateReq, cityReq, businessZipcode) => {

        const countryService = new CountryService();
        countryService.getData().then(({ countries, states, cities }) => { //countries, states or cities
            let countriesList = countries;
            let citiesList = cities;
            let statesList = states;
            let stateName, countryName, cityName;

            for (const country of countriesList) {
                if ((country.countryCode) === (countryReq)) countryName = country.countryName;
            }

            for (const st of statesList) {
                if ((st.stateCode) === (stateReq)) stateName = st.stateName;
            }

            for (const city of citiesList) {
                if ((city.id) === (cityReq)) cityName = city.cityName;
            }
            const { businessDetails } = this.state;
            const Country = (businessDetails.businessCountryCode === 'US' || businessDetails.businessCountryCode === 'United States') ? 'USA' :
                businessDetails.businessCountryCode === 'CANADA' ? 'CANADA' : null;
            initialValues['customerName'] = businessName;
            initialValues['customerAddress'] = Address1 + ' ' + cityName + ' ' + businessDetails.businessStateCode + ' ' +
                businessDetails.businessZipcode + Country;

            this.setState({
                loader: false
            });

        });
    }
    fetchVendorList = () => {
        this.setState({
            vendorLoader: true
        }, () => {
            axios
                .get(
                    `customerVendor/vendorlist/${this.props.selectedBusinessID}?since=0&limit=20000000&key=&direction=&prop=&businessType=Fintainium Customer`
                )
                .then(res => {
                    const vendorLists = res.data.entries;
                    //console.log('vendorLists = res.data.entries', vendorLists);
                    const vendor = vendorLists.filter(v => {
                        return Number(v.id) === Number(initialValues['vendorName']);
                    });
                    initialValues['vendorAddress'] = vendor[0] && vendor[0].address;
                    initialValues['vendorPhoneNo'] = vendor[0] && vendor[0].phoneNumber;
                    this.setState({
                        vendorLists,
                        vendorLoader: false
                    });

                })
                .catch();
        });

    };
    fetchterm = () => {
        this.setState({
            termLoader: true
        }, () => {
            axios
                .get(
                    `/term/list/${this.props.selectedBusinessID}`
                )
                .then(res => {
                    //console.log('termLists ', res);
                    const termLists = res.data;
                    this.setState({
                        termLists,
                        termLoader: false
                    });

                })
                .catch();
        });

    };
    handleChange = (e) => {
        //console.log(e)
    }

    getinvoiceDetails() {
        // console.log('getinvoiceDetails', this.state.businessDetails)
        this.setState({
            loader: true
        }, () => {
            axios
                .get(
                    `/invoice/${this.state.invoiceId}`
                )
                .then(res => {
                    // console.log('getinvoiceDetails ', res);
                    const getinvoiceDetails = res.data;
                    initialValues['invoiceNumber'] = getinvoiceDetails.invoiceNumber;
                    initialValues['invoiceDate'] = new Date(getinvoiceDetails.invoiceDate);
                    initialValues['invoiceDuedate'] = new Date(getinvoiceDetails.invoiceDueDate);
                    initialValues['poNumber'] = getinvoiceDetails.invoicePO;
                    initialValues['terms'] = getinvoiceDetails.termRequest && getinvoiceDetails.termRequest.id ? getinvoiceDetails.termRequest.id : ''
                    initialValues['subTotal'] = getinvoiceDetails.subTotal;
                    initialValues['vendorName'] = getinvoiceDetails.customerVendorId;
                    initialValues['amountDue'] = getinvoiceDetails.totalAmount;
                    initialValues['totalTax'] = getinvoiceDetails.totalTax;
                    initialValues['invoiceItems'] = getinvoiceDetails.invoiceItems;
                    initialValues['comment'] = getinvoiceDetails.comment;
                    const vendors = this.state.vendorLists;
                    const vendor = vendors.filter(v => {
                        return Number(v.id) === Number(initialValues['vendorName']);
                    });
                    initialValues['vendorAddress'] = vendor[0] && vendor[0].address;
                    initialValues['vendorPhoneNo'] = vendor[0] && vendor[0].phoneNumber;
                    const auditTrails = getinvoiceDetails.auditTrails;
                    const emailAttachment = getinvoiceDetails.emailAttachment;




                    this.setState({
                        emailAttachment,
                        auditTrails,
                        emailAttachmentId: getinvoiceDetails.emailAttachmentId,
                        loader: false,

                    });

                })
                .catch();
        });

    }
    handleChange = (e, field) => {
        this.setState({
            [field]: e.target.value
        });
    };
    populatevendorvendorAddress = e => {
        const vendorId = e.target.value;
        const vendors = this.state.vendorLists;
        const vendor = vendors.filter(v => {
            return Number(v.id) === Number(vendorId);
        });
        return vendor[0] && vendor[0].address

    };
    populatevendorvendorvendorName = e => {
        const vendorId = e.target.value;
        const vendors = this.state.vendorLists;
        const vendor = vendors.filter(v => {
            return Number(v.id) === Number(vendorId);
        });
        return vendor[0] && vendor[0].id

    };
    populatevendorvendorvendorPhoneNo = e => {
        const vendorId = e.target.value;
        const vendors = this.state.vendorLists;
        const vendor = vendors.filter(v => {
            return Number(v.id) === Number(vendorId);
        });
        return vendor[0] && vendor[0].phoneNumber

    };
    downloadurl(url) {
        setTimeout(() => {
            window.open(
                url,
                '_blank'
            );
        }, 100);
    }
    closesuccesSubmitModal = () => {
        this.setState({ succesSubmitModal: false });

    }

    handleSuccessReturn = () => {
        this.closesuccesSubmitModal();
        this.props.modalCloseReviewInvoice();
        this.props.getAllList();
    }
    showsuccesSubmitModal = () => {
        this.setState({ succesSubmitModal: true });

    }
    closeinvoiceWarningModal = () => {
        this.setState({ showinvoiceWarningModal: false });

    }
    // addnewTrmshandleSubmit = (value) => {
    //     console.log(value)
    //     const config = {
    //         headers: {
    //             "Content-Type": "application/json"
    //         }
    //     };
    //     const param = {
    //         termName: value.termName,
    //         netDueDays: value.netDueDays,
    //         discountPercent: value.discountPercent,
    //         discountApplicableDays: value.discountApplicableDays,
    //         archive: false,
    //         isActive: true
    //     }
    //     this.setState({
    //         trmsloader: true,
    //     }, () => {
    //         axios
    //             .post(`/term/create/${this.props.selectedBusinessID}`, param, config)
    //             .then(res => {
    //                 console.log(res)
    //                 this.setState({
    //                     showCreateTrmsModal: false,
    //                     addtrmsConfirm: true
    //                 });
    //             })
    //             .catch(err => {
    //                 let tremserrorMessage = this.handelError(err);
    //                 this.setState({
    //                     tremserrorMessage,
    //                     trmsloader: false,

    //                 })
    //             });
    //     })
    // }
    addnewTrmshandleSubmit = (value) => {
        console.log('--------------------addnewTrmshandleSubmit-----------------', value);

        const config = {
            headers: {
                "Content-Type": "application/json"
            }
        };
        const param = {
            termName: value.termName,
            netDueDays: (value.netDueDays === '' ? 0.00 : (value.netDueDays)),
            discountPercent: (value.discountPercent === '' ? 0 : (value.discountPercent)),
            discountApplicableDays: (value.discountApplicableDays === '' ? 0 : (value.discountApplicableDays)),
            archive: false,
            isActive: true
        }

        console.log('**************************************', param);
        this.setState({
            trmsloader: true,
        }
            , () => {
                axios
                    .post(`/term/create/${this.props.selectedBusinessID}`, param, config)
                    .then(res => {
                        console.log(res)
                        this.setState({
                            showCreateTrmsModal: false,
                            addtrmsConfirm: true
                        });
                    })
                    .catch(err => {
                        let tremserrorMessage = this.handelError(err);
                        this.setState({
                            tremserrorMessage,
                            trmsloader: false,

                        })
                    });
            }
        )
    }
    hideCreatetremsModal = () => {
        this.setState({ showCreateTrmsModal: false })
    }
    showCreatetremsModal = () => {
        this.setState({ showCreateTrmsModal: true })
    }
    closeaddtrmsConfirmModal = () => {
        this.setState({ addtrmsConfirm: false }, () => {
            this.fetchterm();
        })
    }

    handlegetBusiness = () => {

        this.setState({
            showBusinessModal: true,
            editBusinessLoading: true,
            editBusinessUI: true
        }, () => {

            axios
                .get(

                    `business/${this.props.selectedBusinessID}`
                )
                .then(res => {
                    console.log('editBusiness', res.data);
                    let getinvoiceDetails = res.data;
                    initialValues['businessName'] = getinvoiceDetails.businessName;
                    initialValues['businessAddress1'] = getinvoiceDetails.businessAddress1;
                    initialValues['businessPhone'] = getinvoiceDetails.businessPhone;
                    if (this._isMounted) {
                        this.setState({
                            businessData: res.data,
                            businessLoader: false
                        }, () => {
                            // initialValues = this.state.editBusiness;
                            // console.log('editBusiness', initialValues);
                        });
                    }


                })
                .catch(e => {
                    let errorMsg = this.displayError(e);
                    this.setState({
                        businessDataError: errorMsg,
                        businessLoader: false

                    });

                    setTimeout(() => {
                        this.setState({ businessDataError: null });
                    }, 5000);

                });

        });
    }

    componentWillMount() {

        // const invoiceKey = { ...this.props };
        // //console.log('componentDidMount EditInvoice', invoiceKey.invId)
        // this.setState({
        //     invoiceId: invoiceKey.invId
        // });
    }

    hideAddVendorModalHandler = () => {
        this.setState({ showAddVendor: false })
    }
    showAddVendorModalHandler = () => {
        this.setState({ showAddVendor: true })
    }
    subtotalcalculation = (param) => {
        // let  total = 0;
        // initialValues.invoiceItems.map((item) => {
        //     total = total + item.subtotal

        // });
        console.log(param)
        return param

    };
    setname = (param) => {
        const item = this.state.itemlist.filter(item => Number(item.id) === Number(param));
        return item[0] && item[0].name
    }
    setsubtotalitemwise = (arry, index, value) => {
        console.log('arry, index, value', arry, index, value);
        let grandsubtotal = 0
        arry[index].quantity = 1
        console.log('====================value===============> ', value)
        arry[index].unitPrice = isNaN(value) ? 0.00 : value
        arry.map((inv, index) => {

            const subtotal = (inv.unitPrice * inv.quantity).toFixed(2);
            console.log('==================inv=================> ', inv)
            grandsubtotal += parseFloat(subtotal);
        });
        return grandsubtotal.toFixed(2);
    }
    isvaliderroritems(param) {

        const item = this.state.errorObj && this.state.errorObj.invoiceItems && this.state.errorObj.invoiceItems.filter(item => Number(item.id) === Number(param));

        if (item && item.length > 0) {

            return item[0].isvalid
        }
        return false

    }
    settotalamountwise = (values, index, value) => {
        const arry = values.invoiceItems
        const totalTax = values.totalTax;
        let totalamout = 0
        // arry[index].quantity = 1
        arry[index].unitPrice = isNaN(value) ? 0.00 : value
        arry.map((inv, index) => {
            const subtotal = (inv.unitPrice * inv.quantity).toFixed(2);
            totalamout = parseFloat(totalamout) + parseFloat(subtotal);
        });
        return (parseFloat(totalTax) + parseFloat(totalamout)).toFixed(2);
    }
    settotalamont = (values) => {
        let totalamout = 0
        const totalTax = isNaN(values.totalTax) ? 0.00 : values.totalTax;
        values.map((inv, index) => {
            const subtotal = (inv.unitPrice * inv.quantity).toFixed(2);
            totalamout = parseFloat(totalamout) + parseFloat(subtotal);
        });
        return (parseFloat(totalTax) + parseFloat(totalamout)).toFixed(2);
    }
    expenseitemclick = (param, setFieldValue, index, values) => {
        const item = this.state.Accountslist.filter(item => Number(item.id) === Number(param));
        values.invoiceItems[index].expencedisable = true;
        setFieldValue(`invoiceItems.${index}.itemRefType`, 'EXPENSE');
        if (item && item.length > 0) {
            setFieldValue(`invoiceItems.${index}.itemMaster`, { id: item[0].id });
            setFieldValue(`invoiceItems.${index}.name`, item[0].name);
            setFieldValue(`invoiceItems.${index}.description`, item[0].description);
            setFieldValue(`invoiceItems.${index}.quantity`, 1);
            setFieldValue(`invoiceItems.${index}.unitPrice`, 0.00);
            setFieldValue(`invoiceItems.${index}.unitDisFlag`, false);///// unitDisFlag 30-01-2020
            setFieldValue("subTotal", this.setsubtotalitemwise(values.invoiceItems, index, 0))
            setFieldValue("amountDue", this.settotalamountwise(values, index, 0))
        } else {
            setFieldValue(`invoiceItems.${index}.itemMaster`, { id: '' });
            setFieldValue(`invoiceItems.${index}.name`, '');
            setFieldValue(`invoiceItems.${index}.description`, '');
            setFieldValue(`invoiceItems.${index}.quantity`, 0);
            setFieldValue(`invoiceItems.${index}.unitPrice`, 0.00);
        }

    }
    itemclick = (param, setFieldValue, index, values) => {
        const item = this.state.itemlist.filter(item => Number(item.id) === Number(param));
        values.invoiceItems[index].expencedisable = false
        setFieldValue(`invoiceItems.${index}.itemRefType`, 'ITEM');
        if (item && item.length > 0) {
            setFieldValue(`invoiceItems.${index}.itemMaster`, { id: item[0].id });
            setFieldValue(`invoiceItems.${index}.name`, item[0].itemName);
            setFieldValue(`invoiceItems.${index}.description`, item[0].description);
            setFieldValue(`invoiceItems.${index}.quantity`, 1);
            setFieldValue(`invoiceItems.${index}.unitPrice`, item[0].unitPrice.toFixed(2));
            setFieldValue(`invoiceItems.${index}.unitDisFlag`, true);///// unitDisFlag 30-01-2020
            setFieldValue("subTotal", this.setsubtotalitemwise(values.invoiceItems, index, item[0].unitPrice))
            setFieldValue("amountDue", this.settotalamountwise(values, index, item[0].unitPrice))

        } else {
            setFieldValue(`invoiceItems.${index}.itemMaster`, { id: '' });
            setFieldValue(`invoiceItems.${index}.name`, '');
            setFieldValue(`invoiceItems.${index}.description`, '');
            setFieldValue(`invoiceItems.${index}.quantity`, 0);
            setFieldValue(`invoiceItems.${index}.unitPrice`, 0.00);
        }

    }
    setitemName = (param) => {
        console.log("param...", param)
        const item = this.state.itemlist.filter(item => item.itemName.toLowerCase() === param.toLowerCase());
        console.log("item.....", item)

        if (item && item.length > 0) {
            return item[0].itemName
        } else {
            const itemAccount = this.state.Accountslist.filter(item => item.name.toLowerCase() === param.toLowerCase());
            console.log("itemAccount.....", itemAccount)
            if (itemAccount && itemAccount.length > 0) {
                return itemAccount[0].name
            }
            // EXP
            else {
                return param
            }
        }
        return 'Select'
    }
    // addnewitem = (index, values, setFieldValue, type) => {
    //     values.invoiceItems[index].showflag = false
    //     setFieldValue(`invoiceItems.${index}.itemMaster`, { id: '' });
    //     setFieldValue(`invoiceItems.${index}.itemRefType`, type);
    //     setFieldValue(`invoiceItems.${index}.name`, '');
    //     setFieldValue(`invoiceItems.${index}.description`, '');
    //     setFieldValue(`invoiceItems.${index}.quantity`, 1);
    //     setFieldValue(`invoiceItems.${index}.unitPrice`, 0.00);
    //     if (type !== "ITEM") {
    //         setFieldValue(`invoiceItems.${index}.expencedisable`, true);
    //     }
    //     setFieldValue("subTotal", this.setsubtotalitemwise(values.invoiceItems, index, 0))
    //     setFieldValue("amountDue", this.settotalamont(values.invoiceItems))

    // }
    showitem = (param) => {
        return param
    }
    getAccountsfilterClick = () => {
        //console.log(this.getAccountsref.current.value)
        const filterValue = (this.getAccountsref.current || {}).value.toLowerCase().trim();
        console.log(filterValue)
        if (filterValue === '') {
            const Accountslist = [...this.state.tempAccountslist]
            this.setState({
                Accountslist
            })
        } else {
            const Accountslist = [...this.state.tempAccountslist].filter(value => value.name.toLowerCase().includes(filterValue));
            this.setState({
                Accountslist
            })
        }


    }
    getExpencefilterClick = () => {
        // console.log(this.getexpenceref.current.value)
        const filterValue = (this.getexpenceref.current || {}).value.toLowerCase().trim();
        if (filterValue === '') {
            const Accountslist = [...this.state.tempAccountslist]
            this.setState({
                Accountslist
            })
        } else {
            const Accountslist = [...this.state.tempAccountslist].filter(value => value.name.toLowerCase().includes(filterValue));
            this.setState({
                Accountslist
            })
        }


    }

    getitemlistfilterClick = () => {
        // console.log(this.getitemref.current.value)
        const filterValue = (this.getitemref.current || {}).value.toLowerCase().trim();;
        if (filterValue === '') {
            const itemlist = [...this.state.tempitemlist]
            this.setState({
                itemlist
            })
        } else {
            const itemlist = [...this.state.tempitemlist].filter(value => value.itemName.toLowerCase().includes(filterValue));
            this.setState({
                itemlist
            })
        }


    }
    expenceassegingtoall = (e, values, setFieldValue) => {
        this.setState({ expeTitalId: e })
        const expenceitem = this.state.Accountslist.filter(item => item.name === e);
        const expenceitemarry = []
        for (let index in values.invoiceItems) {
            expenceitemarry.push({ name: expenceitem[0].name, description: expenceitem[0].description, quantity: 0, unitPrice: 0.00, subtotal: 0, tax: null, itemRefType: 'EXPENSE', showflag: true, expencedisable: true, unitDisFlag: false })
        }
        setFieldValue("invoiceItems", expenceitemarry)
        setFieldValue("subTotal", 0.00)
        setFieldValue("amountDue", values.invoiceItems.totalTax)


    }



    getItemlist = () => {
        this.setState({ loader: true }, () => {
            axios.get(`autofy/getItems/${this.props.selectedBusinessID}`)
                .then(res => {
                    const itemlist = res.data;
                    const tempitemlist = [...res.data]
                    this.setState({
                        itemlist,
                        tempitemlist,
                        loader: false
                    });

                })
                .catch(err => {

                });
        });
    };
    getAccountslist = () => {
        this.setState({ loader: true }, () => {
            axios.get(`autofy/getAccounts/${this.props.selectedBusinessID}`)
                .then(res => {
                    const Accountslist = res.data;
                    const tempAccountslist = [...res.data]
                    this.setState({
                        Accountslist,
                        tempAccountslist,
                        loader: false
                    });

                })
                .catch(err => {

                });
        });

    }

    handleAddConfirMmsg = () => {
        this.setState({
            addConfirMmsg: true,
        }, () => {
            this.fetchVendorList();
        });

    }
    handleAddConfirMmsgClose = () => {
        this.setState({
            addConfirMmsg: false,
        });

    }

    handleInvoiceDate = (date) => {
        this.setState({
            selectedInvoiceDate: new Date(date)
        });
    }

    componentDidMount() {
        console.log('componentDidMount', this.props.emailRequest);
        (this.props.selectedID == 0 ? this.handlegetBusiness() : null)
        this.getDetailsList()
        this.getItemlist();
        this.fetchVendorList();
        // this.getBusiness();
        this.fetchterm();
        this.getAccountslist();
        // this.getinvoiceDetails();

    }

    componentDidUpdate() {
        console.log('componentDidUpdate', this.props.emailRequest);
    }


    render() {
        // console.log('initialValues', initialValues)
        const {
            loader,
            errorObj,
            auditTrails,
            emailAttachment,
            invoiceDetails,
            termLists,
            vendorLists,
            Accountslist,
            customerVendor,
            lineItems,
            invDetails,
            invAdditionalNote,
            itemlist,
            invComment,
            selectedInvoiceDate

        } = this.state;
        const { emailRequest } = this.props;


        return (
            <div className="verifyInvoiceOuter invoicMasterWrap">
                <div className="invoiceMasterHeader">
                    <Row>
                        <Col md={3}>
                            <h1>Edit Invoice</h1>
                        </Col>
                    </Row>
                </div>

                <div className="invoiceMasterBody content-body">
                    {loader ?
                        (
                            <LoadingSpinner className="text-center" />

                        )
                        : (

                            <Formik
                                initialValues={initialValues}
                                validationSchema={addInvoiceSchema}
                                onSubmit={this.handleSubmit}
                                enableReinitialize
                            >
                                {({
                                    values,
                                    errors,
                                    setFieldValue,
                                    touched,
                                    handleChange,
                                    handleBlur
                                }) => {
                                    // console.log('****values*****', values);
                                    return (
                                        <Form noValidate>
                                            <Row>
                                                <Col md={7}>
                                                    <div className="content-block leftContentBlock">
                                                        <Row>
                                                            <Col md={12}>
                                                                {this.props && this.props.s3FileLocation ? (
                                                                    <iframe
                                                                        src={this.props.s3FileLocation + '#toolbar=0&navpanes=0&statusbar=0&view=Fit;readonly=true; disableprint=true;'}
                                                                        width="100%"
                                                                        height="500"
                                                                        frameBorder="0"
                                                                        title="Invoice"
                                                                        allowFullScreen
                                                                    ></iframe>
                                                                ) : (
                                                                        <div className="invNotFound">
                                                                            <h5>No invoice found.</h5>
                                                                        </div>
                                                                    )
                                                                }
                                                                <hr />
                                                            </Col>
                                                        </Row>

                                                        <Row>
                                                            <Col md={12}>
                                                                {
                                                                    !emailRequest.isAccountingToolEnable ? (
                                                                        <div className="row">
                                                                            <div className="col-sm-12">
                                                                                <FormGroup controlId="taxType">

                                                                                    <Field
                                                                                        name="taxType"
                                                                                        type="radio"
                                                                                        onChange={(e) => {
                                                                                            handleChange(e);
                                                                                            this.taxTypeCalculation(values, 'TAX_LINE', setFieldValue);
                                                                                        }}
                                                                                        checked={values.taxType === 'TAX_LINE'}
                                                                                        value={`TAX_LINE`}
                                                                                    /> Apply Tax to Line Items
                                                                                                    &nbsp; &nbsp;
                                                                                                <Field
                                                                                        name="taxType"
                                                                                        type="radio"
                                                                                        onChange={(e) => {
                                                                                            handleChange(e);
                                                                                            this.taxTypeCalculation(values, 'TAX_INVOICE', setFieldValue);
                                                                                        }}
                                                                                        checked={values.taxType === 'TAX_INVOICE'}
                                                                                        value={`TAX_INVOICE`}

                                                                                    /> Apply Tax to Invoice


                                                                                            </FormGroup>
                                                                            </div>
                                                                        </div>
                                                                    ) : null
                                                                }
                                                                <div className="invInfoTable">
                                                                    <div className="table-responsive">

                                                                        <FieldArray
                                                                            name="invoiceItems"
                                                                            render={helpers => (
                                                                                <Fragment>

                                                                                    <Table>
                                                                                        <thead className="theaderBg">
                                                                                            <tr>
                                                                                                <th width="190">Name&nbsp;/&nbsp;Description  <span className="required">*</span></th>
                                                                                                <th width="100">Quantity  <span className="required">*</span></th>
                                                                                                <th width="150">Unit Price  <span className="required">*</span></th>
                                                                                                {!emailRequest.isAccountingToolEnable && values.taxType === 'TAX_LINE' && <th width="">Tax Rate %</th>}
                                                                                                {!emailRequest.isAccountingToolEnable && values.taxType === 'TAX_LINE' && <th width="150">Tax Name</th>}
                                                                                                <th width="140">Subtotal  <span className="required">*</span></th>
                                                                                                <th width="50">&nbsp;&nbsp;</th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                            {/* {JSON.stringify(helpers)} */}
                                                                                            {values.invoiceItems && values.invoiceItems.length ? values.invoiceItems.map((inv, index) => {
                                                                                                { console.log(`values.invoiceItems[${index}].taxPercent`, inv.taxPercent)}
                                                                                                // inv.subtotal = inv.unitPrice * inv.quantity;

                                                                                                //inv.unitPrice = parseFloat(inv.unitPrice).toFixed(2);
                                                                                                //inv.subtotal = isNaN((inv.unitPrice * inv.quantity).toFixed(2)) ? 0.00 : (inv.unitPrice * inv.quantity).toFixed(2)

                                                                                                const tempTotal = (inv.unitPrice * inv.quantity);
                                                                                                let percentCalc;
                                                                                                values.taxType === 'TAX_LINE' ? percentCalc = (inv.taxPercent / 100) * tempTotal : percentCalc = 0;
                                                                                                const subtotal = tempTotal;
                                                                                                inv.subtotal = isNaN((subtotal).toFixed(2)) ? 0.00 : (subtotal).toFixed(2);


                                                                                                inv.tax = (inv.subtotal * values.taxPercent) / 100;
                                                                                                
                                                                                                // inv.id = null;
                                                                                                return (
                                                                                                    <tr key={index}>
                                                                                                        <td width="280">
                                                                                                            {/* EXP */}

                                                                                                            <FormGroup controlId="formBasicText" >

                                                                                                                <Field
                                                                                                                    name={`invoiceItems.${index}.name`}
                                                                                                                    type='text'
                                                                                                                    placeholder="Name"
                                                                                                                    className={this.isvaliderroritems(inv.id) ? 'err-border form-control' : 'form-control'}
                                                                                                                    autoComplete="nope"
                                                                                                                />


                                                                                                                <ErrorMessage name={`invoiceItems.${index}.name`} render={(msg) => <span className="errorMsg">{msg}</span>} />
                                                                                                                <FormControl.Feedback />
                                                                                                            </FormGroup>

                                                                                                            <FormGroup controlId="formBasicText" style={{ display: 'none' }}>
                                                                                                                <Field
                                                                                                                    name={`invoiceItems.${index}.itemRefType`}
                                                                                                                    component="textarea"

                                                                                                                    className={`form-control`}
                                                                                                                    autoComplete="nope"
                                                                                                                />

                                                                                                                <FormControl.Feedback />
                                                                                                            </FormGroup>
                                                                                                            {values.invoiceItems[index].showflag ? (
                                                                                                                <FormGroup controlId="formBasicText" style={{ display: 'none' }}>

                                                                                                                    <Field
                                                                                                                        name={`invoiceItems.${index}.name`}
                                                                                                                        component="select"
                                                                                                                        className={this.isvaliderroritems(inv.id) ? 'err-border form-control' : 'form-control'}
                                                                                                                        className={'form-control'}
                                                                                                                        autoComplete="nope"
                                                                                                                        placeholder="Name"
                                                                                                                        onChange={e => {
                                                                                                                            handleChange(e);
                                                                                                                            setFieldValue(`invoiceItems.${index}.itemMaster`, this.setItemMasterId(e.target.value));
                                                                                                                            setFieldValue(`invoiceItems.${index}.description`, this.setdescription(e.target.value));
                                                                                                                            setFieldValue(`invoiceItems.${index}.unitPrice`, this.setunitPrice(e.target.value));

                                                                                                                        }}
                                                                                                                    >
                                                                                                                        <option value="" >
                                                                                                                            Select
                                                                                                                    </option>

                                                                                                                        {itemlist.length ?
                                                                                                                            itemlist.map(item => (
                                                                                                                                <option
                                                                                                                                    value={item.id}
                                                                                                                                    key={item.id}

                                                                                                                                >
                                                                                                                                    {item.itemName}
                                                                                                                                </option>
                                                                                                                            )) : null}

                                                                                                                        {Accountslist.length ?
                                                                                                                            Accountslist.map(item => (
                                                                                                                                <option
                                                                                                                                    value={item.id}
                                                                                                                                    key={item.id}

                                                                                                                                >
                                                                                                                                    {item.name}
                                                                                                                                </option>
                                                                                                                            )) : null}


                                                                                                                    </Field>
                                                                                                                    {errors.invoiceItems && errors.invoiceItems[index] && errors.invoiceItems[index].name && (Array.isArray(touched.invoiceItems) && touched.invoiceItems[index] && touched.invoiceItems[index].name) ? (
                                                                                                                        <span className="errorMsg">
                                                                                                                            {errors.invoiceItems[index].name}
                                                                                                                        </span>
                                                                                                                    ) : null}
                                                                                                                    <FormControl.Feedback />

                                                                                                                </FormGroup>
                                                                                                            ) : null
                                                                                                            }


                                                                                                            <FormGroup controlId="formBasicText">
                                                                                                                <Field
                                                                                                                    name={`invoiceItems.${index}.description`}
                                                                                                                    component="textarea"
                                                                                                                    //type="text"
                                                                                                                    //className={`form-control`}
                                                                                                                    className={this.isvaliderroritems(inv.id) ? 'err-border form-control' : 'form-control'}
                                                                                                                    autoComplete="nope"
                                                                                                                    placeholder="Description"
                                                                                                                />
                                                                                                                {errors.invoiceItems && errors.invoiceItems[index] && errors.invoiceItems[index].description && (Array.isArray(touched.invoiceItems) && touched.invoiceItems[index] && touched.invoiceItems[index].description) ? (
                                                                                                                    <span className="errorMsg">
                                                                                                                        {errors.invoiceItems[index].description}
                                                                                                                    </span>
                                                                                                                ) : null}
                                                                                                                <FormControl.Feedback />
                                                                                                            </FormGroup>
                                                                                                            <div>
                                                                                                                {/* <Button type="button" bsStyle="primary blue-btn" onClick={() => this.editItem(values.invoiceItems[index].name)}>
                                                                                                                Edit item
                                                                                                                </Button> */}
                                                                                                            </div>
                                                                                                        </td>

                                                                                                        <td width="80">
                                                                                                            <FormGroup controlId="formBasicText">
                                                                                                                <Field
                                                                                                                    name={`invoiceItems.${index}.quantity`}
                                                                                                                    type="number"
                                                                                                                    className={this.isvaliderroritems(inv.id) ? 'err-border form-control' : 'form-control'}
                                                                                                                    autoComplete="nope"
                                                                                                                    placeholder="Quantity"
                                                                                                                    disabled={values.invoiceItems[index].expencedisable}
                                                                                                                    min="0"
                                                                                                                    onChange={e => {
                                                                                                                        handleChange(e);
                                                                                                                        setFieldValue(`invoiceItems.${index}.quantity`, e.target.value.replace(/^.*?([1-9]+[0-9]*).*$/, '$1'))
                                                                                                                        setFieldValue("subTotal", this.setsubtotal(values.invoiceItems, index, 'Quantity', e.target.value, setFieldValue))
                                                                                                                        //setFieldValue("amountDue", this.settotalamount(values, index, 'Quantity', e.target.value))
                                                                                                                    }}
                                                                                                                />
                                                                                                                {errors.invoiceItems && errors.invoiceItems[index] && errors.invoiceItems[index].quantity && (Array.isArray(touched.invoiceItems) && touched.invoiceItems[index] && touched.invoiceItems[index].quantity) ? (
                                                                                                                    <span className="errorMsg">
                                                                                                                        {errors.invoiceItems[index].quantity}
                                                                                                                    </span>
                                                                                                                ) : null}
                                                                                                                <FormControl.Feedback />
                                                                                                            </FormGroup>
                                                                                                        </td>
                                                                                                        <td width="100">
                                                                                                            <FormGroup controlId="formBasicText">
                                                                                                                <Field
                                                                                                                    name={`invoiceItems.${index}.unitPrice`}
                                                                                                                    type="text"
                                                                                                                    className={this.isvaliderroritems(inv.id) ? 'err-border form-control dollerInput unitPrice' : 'form-control dollerInput unitPrice'}
                                                                                                                    autoComplete="nope"
                                                                                                                    placeholder="Unit price"
                                                                                                                    min="0.00"
                                                                                                                    onBlur={e => {
                                                                                                                        if(e.target.value) {
                                                                                                                            const up = isNaN(parseFloat(e.target.value)) ? 0 : parseFloat(e.target.value);
                                                                                                                            setFieldValue(`invoiceItems.${index}.unitPrice`, up.toFixed(2))
                                                                                                                        } 
                                                                                                                    }}
                                                                                                                    disabled={values.invoiceItems[index].unitDisFlag}
                                                                                                                    onChange={e => {
                                                                                                                        // console.log('----------------------- unitPrice --------------------------', parseFloat(String(e.target.value)));
                                                                                                                        // let n = parseFloat(e.target.value)
                                                                                                                        handleChange(e);
                                                                                                                        setFieldValue("subTotal", this.setsubtotal(values.invoiceItems, index, 'Unit price', e.target.value, setFieldValue))
                                                                                                                        //setFieldValue("amountDue", this.settotalamount(values, index, 'Unit price', e.target.value))

                                                                                                                        
                                                                                                                    }}
                                                                                                                />
                                                                                                                {errors.invoiceItems && errors.invoiceItems[index] && errors.invoiceItems[index].unitPrice && (Array.isArray(touched.invoiceItems) && touched.invoiceItems[index] && touched.invoiceItems[index].unitPrice) ? (
                                                                                                                    <span className="errorMsg">
                                                                                                                        {errors.invoiceItems[index].unitPrice}
                                                                                                                    </span>
                                                                                                                ) : null}
                                                                                                                <FormControl.Feedback />
                                                                                                            </FormGroup>
                                                                                                        </td>
                                                                                                        {/* tax rate start*/}
                                                                                                        {!emailRequest.isAccountingToolEnable && values.taxType === 'TAX_LINE'
                                                                                                            ? (<td width="90">
                                                                                                                <FormGroup controlId="formBasicText">
                                                                                                                    <Field
                                                                                                                        name={`invoiceItems.${index}.taxPercent`}
                                                                                                                        type="text"
                                                                                                                        className={this.isvaliderroritems(inv.id) ? 'err-border form-control' : 'form-control'}
                                                                                                                        autoComplete="nope"
                                                                                                                        placeholder="%"
                                                                                                                        // min="0"
                                                                                                                        //value={`values.invoiceItems.${index}.taxPercent` > 0 ? 0.00 ||}
                                                                                                                        disabled={values.invoiceItems[index].unitDisFlag}
                                                                                                                        onBlur={e => {
                                                                                                                            if(e.target.value) {
                                                                                                                                const up = isNaN(parseFloat(e.target.value)) ? 0 : parseFloat(e.target.value);
                                                                                                                                setFieldValue(`invoiceItems.${index}.taxPercent`, up.toFixed(2))
                                                                                                                            } 
                                                                                                                        }}
                                                                                                                        onChange={e => {
                                                                                                                            handleChange(e);
                                                                                                                            setFieldValue("subTotal", this.setsubtotal(values.invoiceItems, index, 'Tax Rate', e.target.value, setFieldValue))
                                                                                                                            //setFieldValue("amountDue", this.settotalamount(values, index, 'Tax Rate', e.target.value))
                                                                                                                        }}
                                                                                                                    />
                                                                                                                    {errors.invoiceItems && errors.invoiceItems[index] && errors.invoiceItems[index].taxPercent && (Array.isArray(touched.invoiceItems) && touched.invoiceItems[index] && touched.invoiceItems[index].taxPercent) ? (
                                                                                                                        <span className="errorMsg">
                                                                                                                            {errors.invoiceItems[index].taxPercent}
                                                                                                                        </span>
                                                                                                                    ) : null}
                                                                                                                    <FormControl.Feedback />
                                                                                                                </FormGroup>
                                                                                                            </td>) : null}

                                                                                                        {/* tax rate end */}

                                                                                                        {/* tax name*/}
                                                                                                        {!emailRequest.isAccountingToolEnable && values.taxType === 'TAX_LINE' ? (<td width="90">
                                                                                                            <FormGroup controlId="formBasicText">
                                                                                                                <Field
                                                                                                                    name={`invoiceItems.${index}.taxName`}
                                                                                                                    type="text"
                                                                                                                    className={this.isvaliderroritems(inv.id) ? 'err-border form-control' : 'form-control'}
                                                                                                                    autoComplete="nope"
                                                                                                                    placeholder=""
                                                                                                                    min="0"
                                                                                                                    disabled={values.invoiceItems[index].unitDisFlag}
                                                                                                                    onChange={e => {
                                                                                                                        handleChange(e);
                                                                                                                        // setFieldValue("subTotal", this.setsubtotal(values.invoiceItems, index, 'Tax Name', e.target.value))
                                                                                                                        // setFieldValue("amountDue", this.settotalamount(values, index, 'Tax Name', e.target.value))
                                                                                                                    }}
                                                                                                                />
                                                                                                                {errors.invoiceItems && errors.invoiceItems[index] && errors.invoiceItems[index].taxName && (Array.isArray(touched.invoiceItems) && touched.invoiceItems[index] && touched.invoiceItems[index].taxName) ? (
                                                                                                                    <span className="errorMsg">
                                                                                                                        {errors.invoiceItems[index].taxName}
                                                                                                                    </span>
                                                                                                                ) : null}
                                                                                                                <FormControl.Feedback />
                                                                                                            </FormGroup>
                                                                                                        </td>) : null}

                                                                                                        {/* tax name */}

                                                                                                        <td width="120">
                                                                                                            <FormGroup controlId="formBasicText">
                                                                                                                <Field
                                                                                                                    name={`invoiceItems.${index}.subtotal`}
                                                                                                                    type="text"
                                                                                                                    className={this.isvaliderroritems(inv.id) ? 'err-border form-control dollerInput' : 'form-control dollerInput'}
                                                                                                                    autoComplete="nope"
                                                                                                                    placeholder="Sub Total"
                                                                                                                    readonly="true"
                                                                                                                />
                                                                                                                {errors.invoiceItems && errors.invoiceItems[index] && errors.invoiceItems[index].subtotal && (Array.isArray(touched.invoiceItems) && touched.invoiceItems[index] && touched.invoiceItems[index].subtotal) ? (
                                                                                                                    <span className="errorMsg">
                                                                                                                        {errors.invoiceItems[index].subtotal}
                                                                                                                    </span>
                                                                                                                ) : null}
                                                                                                                <FormControl.Feedback />
                                                                                                            </FormGroup>
                                                                                                        </td>
                                                                                                        <td width="80">
                                                                                                            <div className="showHideItem">
                                                                                                                <Link to="#" className="smIconBg"
                                                                                                                    onClick={() => this.deletSingleRow(index, values, setFieldValue, (i) => helpers.remove(i))}>
                                                                                                                    <Image src={crossSmIcon} />
                                                                                                                </Link>
                                                                                                            </div>

                                                                                                        </td>
                                                                                                    </tr>
                                                                                                );
                                                                                            }) : null}
                                                                                        </tbody>
                                                                                        {/* <tfoot>
                                                                                        <tr>
                                                                                            <td colSpan={4} className="text-center">
                                                                                           
                                                                                            </td>
                                                                                        </tr>
                                                                                     </tfoot> */}

                                                                                        <tfoot>
                                                                                            <tr>
                                                                                                <td colSpan={7} className="text-center">

                                                                                                    <Button type="button" className="mr-3" bsStyle="primary blue-btn" onClick={() => helpers.push({ name: '', description: '', quantity: 1, unitPrice: parseFloat(0).toFixed(2), taxPercent: parseFloat(0).toFixed(2), taxName: 'Tax', subtotal: 0, isTaxable: false, itemMaster: null, showflag: true, expencedisable: false, itemRefType: 'ITEM' })}>
                                                                                                        Add Line
                                                                                                    </Button>{values.invoiceItems.length > 0 ? (
                                                                                                        <Button type="button" className="primary but-gray ml-5" onClick={() => this.deleteallrow(values, setFieldValue)}>
                                                                                                            Delete All Rows
                                                                                                        </Button>
                                                                                                    ) : null
                                                                                                    }

                                                                                                </td>
                                                                                            </tr>
                                                                                        </tfoot>
                                                                                    </Table>


                                                                                </Fragment>
                                                                            )}
                                                                        />
                                                                    </div>
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col md={12}>
                                                                <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                </Col>
                                                <Col md={5}>
                                                    <div className="content-block rightContentBlock">
                                                        <Tabs defaultActiveKey="details" transition={false} id="noanim-tab-example">
                                                            <Tab eventKey="details" title="Details">
                                                                <div className="invDisplayRightBlock collapaseOuter">
                                                                    <h3 onClick={() => this.setState({ invoiceDetails: !invoiceDetails })}
                                                                        aria-controls="invoice-details"
                                                                        aria-expanded={invoiceDetails}
                                                                        className={invoiceDetails === false ? 'collapas-show' : 'collapas-hide'}
                                                                        style={{ marginBottom: '0' }}
                                                                    >
                                                                        Invoice Details
                                                                    </h3>

                                                                    <Collapse in={invoiceDetails}>
                                                                        <div id="audit-details">
                                                                            <Table>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.invoiceNumber ? ' text-danger ' : '')}>Invoice Number <span className="required">*</span></span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="invoiceNumber"
                                                                                                    type="text"
                                                                                                    className={'form-control'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="Enter"
                                                                                                    value={values.invoiceNumber || ''}
                                                                                                    autoFocus
                                                                                                />
                                                                                                {errors.invoiceNumber && touched.invoiceNumber ? (
                                                                                                    <span className="errorMsg">
                                                                                                        {errors.invoiceNumber}
                                                                                                    </span>
                                                                                                ) : null}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.invoiceDate ? ' text-danger ' : '')}>Invoice Date <span className="required">*</span></span>
                                                                                        </td>
                                                                                        <td className="text-right dateEditInv">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                {/* <Field
                                                                                            name="invoiceDate"
                                                                                            type="text"
                                                                                            className={'form-control'}
                                                                                            autoComplete="nope"
                                                                                            placeholder="Enter"
                                                                                            value={values.invoiceDate || ''}
                                                                                            autoFocus
                                                                                        /> */}
                                                                                                <DatePicker
                                                                                                    className={'form-control'}
                                                                                                    onChange={value => {
                                                                                                        setFieldValue('invoiceDate', value);
                                                                                                        this.handleInvoiceDate(value);
                                                                                                    }}
                                                                                                    value={values.invoiceDate}
                                                                                                    // maxDate={maxDate}
                                                                                                    name="invoiceDate"
                                                                                                    showLeadingZeros
                                                                                                />
                                                                                                <div className="clearfix"></div>
                                                                                                {errors.invoiceDate && touched.invoiceDate ? (
                                                                                                    <span className="errorMsg">
                                                                                                        {errors.invoiceDate}
                                                                                                    </span>
                                                                                                ) : null}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.invoiceDueDate ? ' text-danger ' : '')}>Invoice Due Date <span className="required">*</span></span>
                                                                                        </td>
                                                                                        <td className="text-right dateEditInv">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <DatePicker
                                                                                                    className={'form-control'}
                                                                                                    onChange={value => {
                                                                                                        setFieldValue('invoiceDuedate', value);

                                                                                                    }}
                                                                                                    value={values.invoiceDuedate}
                                                                                                    minDate={selectedInvoiceDate ? new Date(selectedInvoiceDate) : false}
                                                                                                    name="invoiceDuedate"
                                                                                                    showLeadingZeros
                                                                                                />
                                                                                                <div className="clearfix"></div>
                                                                                                {errors.invoiceDuedate && touched.invoiceDuedate ? (
                                                                                                    <span className="errorMsg">
                                                                                                        {errors.invoiceDuedate}
                                                                                                    </span>
                                                                                                ) : null}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.invoicePO ? ' text-danger ' : '')}>P.O. </span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="poNumber"
                                                                                                    type="text"
                                                                                                    className={'form-control'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="Enter"
                                                                                                    value={values.poNumber || ''}
                                                                                                    autoFocus
                                                                                                />
                                                                                                {errors.poNumber && touched.poNumber ? (
                                                                                                    <span className="errorMsg">
                                                                                                        {errors.poNumber}
                                                                                                    </span>
                                                                                                ) : null}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.termId ? ' text-danger ' : '')}>Terms </span>
                                                                                            <span className="text-primary">
                                                                                                &nbsp;(&nbsp;<a style={{ cursor: 'pointer' }} onClick={this.showCreatetremsModal}>Add Term</a>&nbsp;)
                                                                                            </span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="terms"
                                                                                                    component="select"
                                                                                                    className={'form-control'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="select"
                                                                                                    value={values.terms || ''}
                                                                                                >
                                                                                                    <option value="" >
                                                                                                        Select
                                                                                                    </option>
                                                                                                    {termLists.length ?
                                                                                                        termLists.map(term => (
                                                                                                            <option
                                                                                                                value={term.id}
                                                                                                                key={term.id}

                                                                                                            >
                                                                                                                {term.termName}
                                                                                                            </option>
                                                                                                        )) : null}
                                                                                                </Field>

                                                                                                {errors.terms && touched.terms ? (
                                                                                                    <span className="errorMsg">{errors.terms}</span>
                                                                                                ) : null}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </Table>
                                                                        </div>
                                                                    </Collapse>
                                                                </div>


                                                                <div className="invDisplayRightBlock collapaseOuter">
                                                                    <h3 onClick={() => this.setState({ customerVendor: !customerVendor })}
                                                                        aria-controls="customer-vendor"
                                                                        aria-expanded={customerVendor}
                                                                        className={customerVendor === false ? 'collapas-show' : 'collapas-hide'}
                                                                        style={{ marginBottom: '0' }}
                                                                    >
                                                                        Customer &amp; Vendor
                                                                    </h3>

                                                                    <Collapse in={customerVendor}>
                                                                        <div id="audit-details">
                                                                            <Table>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.businessName ? ' text-danger ' : '')}>Customer Name</span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="businessName"
                                                                                                    type="text"
                                                                                                    className={'form-control'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="Enter"
                                                                                                    value={values.businessName || ''}
                                                                                                    autoFocus
                                                                                                    disabled
                                                                                                />
                                                                                                {/* {errors.businessName && touched.businessName ? (
                                                                                            <span className="errorMsg">
                                                                                                {errors.businessName}
                                                                                            </span>
                                                                                        ) : null} */}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.businessAddress1 ? ' text-danger ' : '')}>Customer Address</span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="businessAddress1"
                                                                                                    type="text"
                                                                                                    className={'form-control'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="Enter"
                                                                                                    value={values.businessAddress1 || ''}
                                                                                                    autoFocus
                                                                                                    disabled
                                                                                                />
                                                                                                {/* {errors.businessAddress1 && touched.businessAddress1 ? (
                                                                                            <span className="errorMsg">
                                                                                                {errors.businessAddress1}
                                                                                            </span>
                                                                                        ) : null} */}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.businessPhone ? ' text-danger ' : '')}>Customer Phone No</span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="businessPhone"
                                                                                                    type="text"
                                                                                                    className={'form-control'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="Enter"
                                                                                                    value={values.businessPhone || ''}
                                                                                                    autoFocus
                                                                                                    disabled
                                                                                                />
                                                                                                {/* {errors.businessPhone && touched.businessPhone ? (
                                                                                            <span className="errorMsg">
                                                                                                {errors.businessPhone}
                                                                                            </span>
                                                                                        ) : null} */}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.companyName ? ' text-danger ' : '')}>Vendor Name <span className="required">*</span></span>
                                                                                            <span className="text-primary">
                                                                                                &nbsp;(&nbsp;<a style={{ cursor: 'pointer' }} onClick={this.showAddVendorModalHandler}>Add Vendor</a>&nbsp;)
                                                                                            </span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="vendorName"
                                                                                                    component="select"
                                                                                                    className={'form-control'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="select"
                                                                                                    value={values.vendorName || ''}
                                                                                                    onChange={e => {
                                                                                                        handleChange(e);
                                                                                                        setFieldValue("vendorName", this.populatevendorvendorvendorName(e));
                                                                                                        setFieldValue("vendorAddress", this.populatevendorvendorAddress(e));
                                                                                                        setFieldValue("vendorPhoneNo", this.populatevendorvendorvendorPhoneNo(e));
                                                                                                    }}
                                                                                                >
                                                                                                    {/* <option value="" selected="selected">
                                                                                                        Select
                                                                                                    </option> */}
                                                                                                    {vendorLists.length ?
                                                                                                        vendorLists.map(vendor => (
                                                                                                            <option
                                                                                                                value={vendor.id}
                                                                                                                key={vendor.id}

                                                                                                            >
                                                                                                                {vendor.companyName}
                                                                                                            </option>
                                                                                                        )) : null}
                                                                                                </Field>

                                                                                                {errors.vendorName && touched.vendorName ? (
                                                                                                    <span className="errorMsg">{errors.vendorName}</span>
                                                                                                ) : null}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.address ? ' text-danger ' : '')}>Vendor Address</span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="vendorAddress"
                                                                                                    type="text"
                                                                                                    className={'form-control'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="Enter"
                                                                                                    value={values.vendorAddress || ''}
                                                                                                    disabled
                                                                                                    autoFocus
                                                                                                />
                                                                                                {/* {errors.vendorAddress && touched.vendorAddress ? (
                                                                                            <span className="errorMsg">
                                                                                                {errors.vendorAddress}
                                                                                            </span>
                                                                                        ) : null} */}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.phoneNumber ? ' text-danger ' : '')}>Vendor Phone No</span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="vendorPhoneNo"
                                                                                                    type="text"
                                                                                                    className={'form-control'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="Enter"
                                                                                                    value={values.vendorPhoneNo || ''}
                                                                                                    disabled
                                                                                                    autoFocus
                                                                                                />
                                                                                                {/* {errors.vendorPhoneNo && touched.vendorPhoneNo ? (
                                                                                            <span className="errorMsg">
                                                                                                {errors.vendorPhoneNo}
                                                                                            </span>
                                                                                        ) : null} */}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </Table>
                                                                        </div>
                                                                    </Collapse>
                                                                </div>

                                                                {/* <div className="invDisplayRightBlock collapaseOuter">
                                                                    <h3 onClick={() => this.setState({ lineItems: !lineItems })}
                                                                        aria-controls="line-items"
                                                                        aria-expanded={lineItems}
                                                                        className={lineItems === false ? 'collapas-show' : 'collapas-hide'}
                                                                        style={{ marginBottom: '0' }}
                                                                    >
                                                                        Line Items
                                                                    </h3>

                                                                    <Collapse in={lineItems}>
                                                                        <div id="audit-details">
                                                                            <Table>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className="companyNameRblock">Line Item</span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            {initialValues['invoiceItems'].length}
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </Table>
                                                                        </div>
                                                                    </Collapse>
                                                                </div> */}


                                                                <div className="invDisplayRightBlock collapaseOuter">
                                                                    <h3 onClick={() => this.setState({ invDetails: !invDetails })}
                                                                        aria-controls="invoice-details"
                                                                        aria-expanded={invDetails}
                                                                        className={invDetails === false ? 'collapas-show' : 'collapas-hide'}
                                                                        style={{ marginBottom: '0' }}
                                                                    >
                                                                        Invoice Details
                                                                    </h3>

                                                                    <Collapse in={invDetails}>
                                                                        <div id="audit-details">
                                                                            <Table>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.totalAmount ? ' text-danger ' : '')}>Subtotal</span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="subTotal"
                                                                                                    type="number"
                                                                                                    className={'form-control dollerInput'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="Enter"
                                                                                                    value={values.subTotal || 0.00}
                                                                                                    autoFocus
                                                                                                    disabled
                                                                                                />
                                                                                                {errors.subTotal && touched.subTotal ? (
                                                                                                    <span className="errorMsg">
                                                                                                        {errors.subTotal}
                                                                                                    </span>
                                                                                                ) : null}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>
                                                                                    {/* {!emailRequest.isAccountingToolEnable && values.taxType === 'TAX_INVOICE' && */}
                                                                                    {!emailRequest.isAccountingToolEnable  &&
                                                                                        <tr>
                                                                                            <td>

                                                                                                <span className={"companyNameRblock" + (errorObj && errorObj.totalTax ? ' text-danger ' : '')}>Total Tax</span>
                                                                                            </td>
                                                                                            <td className="text-right">
                                                                                                <FormGroup controlId="formBasicText">
                                                                                                    <Field
                                                                                                        name="totalTax"
                                                                                                        type="text"
                                                                                                        className={'form-control dollerInput'}
                                                                                                        autoComplete="nope"
                                                                                                        placeholder="Enter"
                                                                                                        value={values.totalTax || 0.00}
                                                                                                        onBlur={e => {
                                                                                                            if(e.target.value) {
                                                                                                                const up = isNaN(parseFloat(e.target.value)) ? 0 : parseFloat(e.target.value);
                                                                                                                setFieldValue('totalTax', up.toFixed(2))
                                                                                                            } 
                                                                                                        }}
                                                                                                        onChange={e => {
                                                                                                            handleChange(e, setFieldValue("amountDue", this.cal(values.subTotal, e.target.value)));

                                                                                                        }}
                                                                                                       disabled={values.taxType === 'TAX_INVOICE' ? false : true}

                                                                                                    />

                                                                                                    {errors.totalTax && touched.totalTax ? (
                                                                                                        <span className="errorMsg">
                                                                                                            {errors.totalTax}
                                                                                                        </span>
                                                                                                    ) : null}
                                                                                                    <FormControl.Feedback />
                                                                                                </FormGroup>
                                                                                            </td>
                                                                                        </tr>
                                                                                    }
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span className={"companyNameRblock" + (errorObj && errorObj.balanceAmount ? ' text-danger ' : '')}>Amount Due</span>
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <FormGroup controlId="formBasicText">
                                                                                                <Field
                                                                                                    name="amountDue"
                                                                                                    type="number"
                                                                                                    className={'form-control dollerInput'}
                                                                                                    autoComplete="nope"
                                                                                                    placeholder="Enter"
                                                                                                    value={values.amountDue || 0.00}
                                                                                                    autoFocus
                                                                                                    disabled

                                                                                                />
                                                                                                {errors.amountDue && touched.amountDue ? (
                                                                                                    <span className="errorMsg">
                                                                                                        {errors.amountDue}
                                                                                                    </span>
                                                                                                ) : null}
                                                                                                <FormControl.Feedback />
                                                                                            </FormGroup>
                                                                                        </td>
                                                                                    </tr>

                                                                                </tbody>
                                                                            </Table>
                                                                        </div>
                                                                    </Collapse>
                                                                </div>

                                                                <div className="invDisplayRightBlock collapaseOuter">
                                                                    <h3 onClick={() => this.setState({ invAdditionalNote: !invAdditionalNote })}
                                                                        aria-controls="inv-note"
                                                                        aria-expanded={invAdditionalNote}
                                                                        className={invAdditionalNote === false ? 'collapas-show' : 'collapas-hide'}
                                                                        style={{ marginBottom: '0' }}
                                                                    >
                                                                        Invoice Additional Note
                                                                    </h3>
                                                                    <Collapse in={invAdditionalNote}>
                                                                        <div id="audit-details">
                                                                            <Table>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>

                                                                                            <Field
                                                                                                name="comment"
                                                                                                component="textarea"
                                                                                                className={'additionalNoteArea' + (errorObj && errorObj.comment ? ' border-danger ' : '')}
                                                                                                autoComplete="nope"
                                                                                                placeholder="Enter"
                                                                                                value={values.comment || ''}
                                                                                                autoFocus
                                                                                            />
                                                                                            {errors.comment && touched.comment ? (
                                                                                                <span className="errorMsg">
                                                                                                    {errors.comment}
                                                                                                </span>
                                                                                            ) : null}
                                                                                            <FormControl.Feedback />

                                                                                            {/* <textarea className="additionalNoteArea">
                                                                                            </textarea> */}
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </Table>
                                                                        </div>
                                                                    </Collapse>
                                                                </div>

                                                                <div className="invDisplayRightBlock collapaseOuter">
                                                                    <h3 onClick={() => this.setState({ invComment: !invComment })}
                                                                        aria-controls="inv-note"
                                                                        aria-expanded={invComment}
                                                                        className={invComment === false ? 'collapas-show' : 'collapas-hide'}
                                                                        style={{ marginBottom: '0' }}
                                                                    >
                                                                        Comment
                                                                    </h3>
                                                                    <Collapse in={invComment}>
                                                                        <div id="audit-details">
                                                                            <Table>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <Field
                                                                                                name="auditTrailNote"
                                                                                                component="textarea"
                                                                                                className={'additionalNoteArea'}
                                                                                                autoComplete="nope"
                                                                                                placeholder="Enter"
                                                                                                value={values.auditTrailNote || ''}
                                                                                                autoFocus
                                                                                            />
                                                                                            {errors.auditTrailNote && touched.auditTrailNote ? (
                                                                                                <span className="errorMsg">
                                                                                                    {errors.auditTrailNote}
                                                                                                </span>
                                                                                            ) : null}
                                                                                            <FormControl.Feedback />
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </Table>
                                                                        </div>
                                                                    </Collapse>
                                                                </div>
                                                                <div className="invButtonsWrap text-center">

                                                                    <Button type="submit" bsStyle="primary blue-btn ml-5" >
                                                                        Save
                                                                    </Button>

                                                                </div>
                                                            </Tab>

                                                            <Tab eventKey="assets" title="Assets">

                                                                <div className="invDisplayRightBlock">
                                                                    <h3>
                                                                        Original Email
                                                                    </h3>
                                                                    <div id="assets" className="invRightBlockCon">
                                                                        <h4>{emailAttachment && emailAttachment.emailRequest && emailAttachment.emailRequest.emailMsgFrom}</h4>
                                                                        <p>{emailAttachment && emailAttachment.emailRequest && emailAttachment.emailRequest.emailMsgSubject}</p>
                                                                    </div>
                                                                </div>

                                                                <div className="invDisplayRightBlock">
                                                                    <h3>
                                                                        Original Invoice
                                                                    </h3>
                                                                    <div id="invoicePdf" className="invRightBlockCon">
                                                                        <ul>
                                                                            <li>
                                                                                {emailAttachment && <div> <Image src={pdfIcon} width="15" /> {emailAttachment.fileName} &nbsp; &nbsp; <span className="link" onClick={() => this.downloadurl(emailAttachment.s3FileLocation)} ><IoMdDownload /> Download</span></div>}
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </Tab>
                                                            <Tab eventKey="comments" title="Comments">
                                                                <div className="invDisplayRightBlock">


                                                                    <ul>
                                                                        {(auditTrails.length > 0) && auditTrails.map(audit => audit.comment != '' && (<li>{audit.comment}</li>))}
                                                                    </ul>
                                                                </div>
                                                            </Tab>
                                                            <Tab eventKey="audittrails" title="Audit Trail">
                                                                <div className="invDisplayRightBlock">
                                                                    <h3>
                                                                        Audit Trail
                                                                    </h3>
                                                                    <div id="audit-details">
                                                                        <Table>
                                                                            <tbody>
                                                                                {auditTrails.length > 0 ? (auditTrails.map((audit, k) =>

                                                                                    < tr key={k}>
                                                                                        <td>
                                                                                            <span className="companyNameRblock">
                                                                                                {audit.userName}
                                                                                            </span>
                                                                                            <br />
                                                                                            {audit.activityDate}
                                                                                        </td>
                                                                                        <td className="text-right">
                                                                                            <span
                                                                                                className="statusRblock submit"
                                                                                            >
                                                                                                {audit.invoiceState.replace(/_/g, ' ')}
                                                                                            </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                )) : null
                                                                                }
                                                                            </tbody>
                                                                        </Table>
                                                                    </div>
                                                                </div>
                                                            </Tab>
                                                        </Tabs>


                                                    </div>
                                                </Col>
                                            </Row>
                                        </Form>
                                    );

                                }}
                            </Formik>

                        )
                    }
                    <Modal
                        show={this.state.succesSubmitModal}
                        onHide={this.closesuccesSubmitModal}
                        className="payOptionPop"
                    >
                        <Modal.Body>
                            <Row>
                                <Col md={12} className="text-center">
                                    <Image src={SuccessIco} />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12} className="text-center">
                                    {/* <h5>Are you sure you want to unlock your card ?</h5> */}
                                    <h5>Invoice edited successfully </h5>
                                </Col>
                            </Row>
                            <div className="text-center">
                                <button
                                    className="btn btn-dark but-gray my-3"
                                    type="button"
                                    disabled={loader}
                                    onClick={this.handleSuccessReturn}
                                >
                                    Done
                            </button>
                            </div>
                        </Modal.Body>
                    </Modal>

                    <AdditemsModal showAdditemsModal={this.state.showAdditemsModal}
                        hideAdditemsModal={this.hideAdditemsModal}
                        addnewitemhandleSubmit={this.addnewitemhandleSubmit}
                        additemConfirm={this.state.additemConfirm}
                        Additemsloder={this.state.Additemsloder}
                        closeadditemsConfirmModal={this.closeadditemsConfirmModal}
                        errorItemMessage={this.state.errorItemMessage}
                        errorItemMessageModal={this.state.errorItemMessageModal}
                        hideAdditemsWorningModal={this.hideAdditemsWorningModal}

                    />

                    <Modal
                        show={this.state.showinvoiceWarningModal}
                        onHide={this.closeinvoiceWarningModal}
                        className="payOptionPop"
                    >
                        <Modal.Body>
                            <Row>
                                <Col md={12} className="text-center">
                                    <p> {this.state.errorMessage}</p>
                                </Col>
                            </Row>
                            <div className="text-center">
                                <Button
                                    onClick={this.closeinvoiceWarningModal}
                                    className="but-gray m-auto">
                                    Return
                            </Button>
                            </div>

                        </Modal.Body>

                    </Modal>



                    <Modal
                        show={this.state.showCreateTrmsModal}
                        onHide={this.hideCreatetremsModal}
                        className="payOptionPop"
                    >
                        <Modal.Header>
                            <Modal.Title>Create Term</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="modalBody content-body noTabs">
                                <Row>
                                    <Col md={12}>
                                        <Formik
                                            initialValues={initialValuestrems}
                                            validationSchema={addtremsSchema}
                                            onSubmit={this.addnewTrmshandleSubmit}
                                            enableReinitialize={true}
                                        >
                                            {({
                                                values,
                                                errors,
                                                touched

                                            }) => {
                                                return (
                                                    <Form novalidate>
                                                        <Row className="show-grid">
                                                            <Col xs={6} md={12}>

                                                                <FormGroup controlId="termName">
                                                                    <label>Term Name
                                                                        <span className="required">*</span>
                                                                    </label>
                                                                    <Field
                                                                        name="termName"
                                                                        type="text"
                                                                        className={`form-control`}
                                                                        autoComplete="nope"
                                                                        placeholder="Enter"
                                                                        value={values.termName}
                                                                        autoFocus

                                                                    />
                                                                    {errors.termName && touched.termName ? (
                                                                        <span className="errorMsg">{errors.termName}</span>
                                                                    ) : null}
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col md={12}>
                                                                <p style={{ paddingTop: '20px' }}><span className="required">*</span> This field is required.</p>
                                                            </Col>
                                                        </Row>


                                                        <div className="formButtonWrap">
                                                            <Row>
                                                                <Col className="text-center">
                                                                    <Button type="button" className="but-gray mr-3" onClick={this.hideCreatetremsModal}>Cancel</Button>
                                                                    <Button type="submit" className="blue-btn  m-l-5"  >Save</Button>
                                                                </Col>
                                                            </Row>
                                                        </div>
                                                    </Form>
                                                );
                                            }}
                                        </Formik>
                                    </Col>

                                </Row>

                            </div>

                        </Modal.Body>
                    </Modal>

                    <Modal
                        show={this.state.addtrmsConfirm}
                        onHide={this.closeaddtrmsConfirmModal}
                        className="payOptionPop"
                    >
                        <Modal.Body>
                            <Row>
                                <Col md={12} className="text-center">
                                    <Image src={SuccessIco} />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12} className="text-center">
                                    <h5> Term created successfully</h5>
                                </Col>
                            </Row>
                        </Modal.Body>
                        <Modal.Footer>

                            <Button
                                onClick={() => {
                                    this.closeaddtrmsConfirmModal();
                                }}
                                className="but-gray m-auto">
                                Return
                        </Button>
                        </Modal.Footer>
                    </Modal>
                    {/* Add Vendor Modal */}
                    <Modal
                        show={this.state.showAddVendor}
                        onHide={this.hideAddVendorModalHandler}
                        className="payOptionPop"
                    >
                        <Modal.Header closeButton>
                            <Modal.Title>Add Vendor</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="modalBody content-body noTabs">
                                <AddBusinessVendors
                                    handleAddConfirMmsg={this.handleAddConfirMmsg}
                                    businessId={this.props.selectedBusinessID}
                                    handelAddModalClose={this.hideAddVendorModalHandler}
                                />
                            </div>
                        </Modal.Body>

                    </Modal>
                    {/*======  Add confirmation popup  ===== */}
                    <Modal
                        show={this.state.addConfirMmsg}
                        onHide={this.handleAddConfirMmsgClose}
                        className="payOptionPop"
                    >
                        <Modal.Body>
                            <Row>
                                <Col md={12} className="text-center">
                                    <Image src={SuccessIco} />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12} className="text-center my-3">
                                    <h5>Vendor has been successfully Added</h5>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12} className="text-center">
                                    <Button
                                        onClick={this.handleAddConfirMmsgClose}
                                        className="but-gray"
                                    >
                                        Return
                                </Button>
                                </Col>
                            </Row>
                        </Modal.Body>
                    </Modal>

                </div>

            </div >
        );
    }
}
EditInvoice.propTypes = {
    selectedID: PropTypes.number,
    selectedBusinessID: PropTypes.number,
    selectedStatus: PropTypes.string,
    s3FileLocation: PropTypes.string,
    modalCloseReviewInvoice: PropTypes.func,
    getAllList: PropTypes.func,
    emailRequest: PropTypes.object,

};



export default EditInvoice;
