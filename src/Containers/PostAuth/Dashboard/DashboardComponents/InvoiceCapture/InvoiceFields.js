export const INVOICE_NUMBER = 'invoiceNumber';
export const INVOICE_DATE = 'invoiceDate';
export const INVOICE_DUE_DATE = 'invoiceDueDate';
export const INVOICE_PO = 'invoicePO';

export const TERM_ID = 'termId';

export const CUSTOMER_NAME = 'businessName';
export const CUSTOMER_ADDRESS = 'businessAddress1';
export const CUSTOMER_PHONE_NO = 'businessPhone';

export const VENDOR_NAME = 'companyName';
export const VENDOR_ADDRESS = 'address';
export const VENDOR_PHONE_NO = 'phoneNumber';

export const SUBTOTAL = 'subTotal';
export const TOTAL_TAX = 'totalTax';
export const AMOUNT_DUE = 'totalAmount';

export const CURRENCY = 'currency';
export const COMMENT = 'comment'; 
export const INVOICE_ITEMS = 'invoiceItems';

