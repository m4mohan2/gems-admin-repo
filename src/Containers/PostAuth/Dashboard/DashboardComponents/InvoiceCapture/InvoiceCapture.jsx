/* eslint-disable no-unused-vars */
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Col, Image, Modal, Row } from 'react-bootstrap';
import { Link, NavLink, Redirect, Route, Switch } from 'react-router-dom';
import refreshIcon from './../../../../../assets/refreshIcon.png';
import './InvoiceCapture.scss';
import InvoiceCaptureMain from './InvoiceCaptureMain';


class InvoiceCapture extends Component {

    state = {
        businessId: 0,
        allListLoader: false,
        allList: [],
        showUpload: false,
        allListError: null,
    };


    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Unknown error!';
        }

        this.setState({
            errorMessge: errorMessge
        });

        setTimeout(() => {
            this.setState({ errorMessge: null });
        }, 5000);
    }

    handleUploadCloseManual = () => {
        this.setState({
            showUpload: false
        });
    };

    handleUploadShow = () => {
        this.setState({ showUpload: true });
    };

    createInvoiceHandler = () => {
        this.props.history.push('/dashboard/invoice-capture/create-invoice');
    }


    commponentDidMount = () => {
        console.log('MOUNT calling.........................');
    }

    render() {
        const { pathname } = this.props.history.location;

        console.log('page name', pathname);
        return (
            <div className="invoiceCaptureOuter">
                {pathname !== '/dashboard/invoice-capture/create-invoice' ?
                    <div className="mainTab">
                        <ul className="nav nav-tabs px-3">
                            <li>
                                <NavLink to="/dashboard/invoice-capture/all">
                                    All
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/dashboard/invoice-capture/unopened" >
                                    Unopened
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/dashboard/invoice-capture/pending-verification">
                                    Pending Verification
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/dashboard/invoice-capture/verified">
                                    Verified
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/dashboard/invoice-capture/error-reported">
                                    Error Reported
                                </NavLink>
                            </li>
                        </ul>
                    </div>
                    : null
                }

                <div className="invCaptureBody">

                    <div className="invCaptureTableArea">
                        <Switch>
                            <Route path="/dashboard/invoice-capture/:invoiceStatus" component={InvoiceCaptureMain} />
                            <Redirect from="/dashboard/invoice-capture" to="/dashboard/invoice-capture/all" />
                        </Switch>
                    </div>
                </div>


                <Modal
                    show={this.state.showUpload}
                    onHide={this.handleUploadCloseManual}
                    className="uploadPop"
                >
                    <Modal.Header closeButton>
                        <Modal.Title>Upload Invoice</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h5 className="downloadInvoiceCsv"><a href={this.state.csvDownlaodLink}><i className="glyphicon glyphicon-download-alt"></i> Download Sample CSV</a></h5>
                    </Modal.Body>
                </Modal>

            </div>
        );
    }
}

InvoiceCapture.propTypes = {
    history: PropTypes.object
};


export default InvoiceCapture;
