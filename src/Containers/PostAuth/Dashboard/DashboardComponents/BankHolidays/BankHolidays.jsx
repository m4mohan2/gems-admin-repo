import React, { Component } from 'react';
import axios from '../../../../../shared/eaxios';
import {
    Table,
    // Dropdown,
    Row,
    Modal,
    Col,
    Image,
    Button,
} from 'react-bootstrap';
// import { FaCaretDown } from 'react-icons/fa';
// import { FaCaretUp } from 'react-icons/fa';
import Pagination from 'react-js-pagination';
//import { Link } from 'react-router-dom';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
//import refreshIcon from './../../../../../assets/refreshIcon.png';
import moment from 'moment';
import AddBankHoliday from './AddBankHoliday';
import SuccessIco from './../../../../../assets/success-ico.png';
import { CountryService } from './../../../../../services/country.service';



class BankHolidays extends Component {
    state = {
        countries: [],
        holidaysLists: [],
        activePage: 1,
        totalCount: 0,
        itemPerPage: 20,
        loading: false,
        errorMessge: null,
        selectedCountry: 'United States',
        selectedYear: moment().format('YYYY'),
        yearArray: [],
        showActionModal: false,
        showConfirMmsg:false,
        modalName:'',
        modalData:{},
        alertMsg: false,
        alertState: null,
        deleteLoader:false


    }

    _isMounted = false;

    componentDidMount() {

        this._isMounted = true;

        /****year drop down calculation****/
        let year1 = moment().format('YYYY') - 2;
        let yearArray = [];
        for (let i = 0; i < 5; i++) {
            yearArray.push(year1);
            year1++;
        }
        this.setState({
            yearArray: yearArray
        });

        /**********country dropdown*************/
        const countryService = new CountryService;
        countryService.getData().then(({ countries }) => {
            this.setState({ countries });
        }); 


        this._isMounted = true;
        this.fetchHolidaysList();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    fetchHolidaysList = (

    ) => {

        this.setState({
            loading: true,

        }, () => {

            axios
                .get(
                    `holiday/list?year=${this.state.selectedYear}&countryName=${this.state.selectedCountry}`
                )
                .then(res => {
                    const holidaysLists = res.data;
                    if (this._isMounted && holidaysLists) {
                        this.setState({
                            holidaysLists: holidaysLists,
                            loading: false
                        }, () => {
                        });
                    }


                })
                .catch(e => {
                    let errorMsg = this.displayError(e);
                    this.setState({
                        errorMessge: errorMsg,
                        loading: false
                    });
                    setTimeout(() => {
                        this.setState({ errorMessge: null });
                    }, 5000);

                });

        });

    };

    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber });
        this.fetchHolidaysList(pageNumber > 0 ? pageNumber - 1 : 0, this.state.sort, this.state.field);
    };

    resetPagination = () => {
        this.setState({ activePage: 1 });
    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Unknown error!';
        }
        return errorMessge;
    }

    handelSearch = () => {
        this.fetchHolidaysList();
    }

    resetSearch = () => {
        this.setState({
        }, () => {
            this.fetchHolidaysList();
        });
    }

    handleChangeYear = (e) => {
        this.setState({ selectedYear: e.target.value },
            () => {
                console.log('this.state.selectedYear', this.state.selectedYear);
                this.fetchHolidaysList();
            });
    }

    handleChangeCountry = (e) => {
        this.setState({ selectedCountry: e.target.value },
            () => {
                console.log('this.state.selectedCountry', this.state.selectedCountry);
                this.fetchHolidaysList();
            });
    }

    handelAlertMsg = (param) => {
        this.setState({
            alertMsg: true,
            alertState: param
        });

    }

    closeAlertMsg = () => {
        this.setState({
            alertMsg: false,
            alertState: null
        });
    }

    handelModal = (param, data) => {
        data.name = param;
        this.setState({
            showActionModal: true,
            modalName: param,
            modalData:data
        });
    }

    handelModalClose = () => {
        this.setState({
            showActionModal: false,
            modalData:{},
            modalName: ''
        });
    }

    handleConfirmMsg = () =>{
        this.setState({
            showConfirMmsg: true,
            showActionModal:false
            
        });
    }

    handleConfirmMsgClose = () => {
        this.setState({
            showConfirMmsg: false,
        }, () => this.fetchHolidaysList());
    };
    

    handledelete = () =>{
        console.log('this.state.modalData.id', this.state.modalData.id);
        this.setState({
            modalName:'delete',
            deleteLoader: true,
            alertMsg:false,
            
        });
        axios
            .delete(
                `holiday/delete/${this.state.modalData.id}`
            )
            .then(res => {
                console.log('delete holiday res',res);
                if (this._isMounted ) {
                    this.setState({
                        deleteLoader: false,
                        showActionModal: false,
                        showConfirMmsg:true,
                    }, () => {
                        this.fetchHolidaysList();
                    });
                }
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    errorMessge: errorMsg,
                    deleteLoader: false
                });
                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);

            });
    }


    render() {
        return (
            <React.Fragment>
                <div className="bg-white py-3 border-bottom">
                    <h6 className="pl-3 ml-3 font-weight-400 pt-1">{`List of ${this.state.selectedCountry === 'United States' ? 'US' : this.state.selectedCountry} Bank Holidays ${this.state.selectedYear}`}</h6>
                </div>

                <div className="dashboardInner businessOuter pt-3">
                    {this.state.errorMessge
                        ?
                        <div className="alert alert-danger col-12" role="alert">
                            ERROR : {this.state.errorMessge}
                        </div>
                        :
                        null
                    }

                    <div className="row mb-3">
                        <div className="col-12">
                            <select
                                id={this.state.selectedCountry}
                                className="form-control truncate pr-35 float-left w-210 mr-3"
                                onChange={this.handleChangeCountry}
                                value={this.state.selectedCountry}>
                                {/* <option value="" defaultValue="select">
                                    {this.state.countries.length ? 'Select Country' : 'Loading...'}
                                </option> */}
                                {this.state.countries.map(country => (
                                    <option
                                        key={country.id}
                                        value={country.countryName}
                                    >
                                        {country.countryName}
                                    </option>
                                ))}
                            </select>
                        
                            <select
                                className="form-control truncate pr-35 float-left w-210 mr-3"
                                value={this.state.selectedYear}
                                onChange={this.handleChangeYear}

                            >
                                
                                {this.state.yearArray.length > 0
                                    &&
                                    (this.state.yearArray.map(year => (
                                        <React.Fragment key={year}>
                                            <option value={year}>{year}</option>
                                        </React.Fragment>
                                    )

                                    ))
                                }
                            </select>
                            <div className="pull-right text-right">
                                <button className="btn btn-primary" onClick={() => this.handelModal('add', {})}>Add Bank Holidays</button>
                            </div>
                        </div>

                        
                    </div>

                    <div className="boxBg">

                        <Table responsive hover>
                            <thead className="theaderBg">
                                <tr>
                                    <th>Date</th>
                                    <th>Day</th>
                                    <th>Holiday Description</th>
                                </tr>
                            </thead>
                            <tbody>

                                {this.state.loading ? (<tr>
                                    <td colSpan={12}>
                                        <LoadingSpinner />
                                    </td>
                                </tr>)
                                    :
                                    this.state.holidaysLists.length > 0 ? (
                                        this.state.holidaysLists.map(holiday => (
                                            <tr key={holiday.id} onClick={() => this.handelModal('edit', holiday)}>
                                                <td>{holiday.month}</td>
                                                <td>{holiday.day}</td>
                                                <td>{holiday.purpose}</td>
                                            </tr>
                                        )))
                                        :
                                        this.state.errorMessge ? <tr>
                                            <td colSpan={12}>
                                                <p className="text-center">{this.state.errorMessge}</p>
                                            </td>
                                        </tr>
                                            : (
                                                <tr>
                                                    <td colSpan={12}>
                                                        <p className="text-center">No records found</p>
                                                    </td>
                                                </tr>
                                            )
                                }
                            </tbody>
                        </Table>
                    </div>
                    {this.state.totalCount ? (
                        <Row>
                            <Col md={12}>
                                <div className="paginationOuter text-right">
                                    <Pagination
                                        activePage={this.state.activePage}
                                        itemsCountPerPage={this.state.itemPerPage}
                                        totalItemsCount={this.state.totalCount}
                                        onChange={this.handlePageChange}
                                    />
                                </div>
                            </Col>
                        </Row>
                    ) : null}

                    <Modal
                        show={this.state.showActionModal}
                        onHide={this.handelModalClose}
                        className="right half noPadding slideModal"
                    >
                        <Modal.Header closeButton></Modal.Header>
                        <Modal.Body>
                            <div className="modalHeader">
                                <div className="row">
                                    <div className="col-6">
                                        <h1>{this.state.modalName === 'add' ? 'Add Bank Holidays' : (this.state.modalName === 'edit' ? 'Edit Bank Holidays' : '')}</h1>
                                    </div>
                                    <div className="col-5 text-right">
                                        {this.state.modalName !== 'add' ? <button className="btn btn-dark" onClick={() => this.handelAlertMsg('delete')}>Delete</button> : ''}
                                        {/* {this.state.modalName !== 'add' ? <button className="btn btn-dark" onClick={this.handledelete}>Delete</button> : ''} */}
                                    </div>
                                </div>
                            </div>

                            <div className="modalBody content-body noTabs">
                                <AddBankHoliday
                                    isLoading={this.state.deleteLoader}
                                    onSuccess={this.handleConfirmMsg}
                                    data={this.state.modalData}
                                />

                            </div>

                        </Modal.Body>

                    </Modal>



                    {/*======  confirmation popup  ===== */}
                    <Modal
                        show={this.state.showConfirMmsg}
                        onHide={this.handleConfirmMsgClose}
                        className="payOptionPop"
                    >
                        <Modal.Body>
                            <Row>
                                <Col md={12} className="text-center">
                                    <Image src={SuccessIco} />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12} className="text-center my-3 py-3">
                                    <h5>
                                        {this.state.modalName === 'add' && 'Bank holiday has been successfully added'}
                                        {this.state.modalName === 'edit' && 'Bank holiday has been successfully edited'}
                                        {this.state.modalName === 'delete' && 'Bank holiday has been successfully deleted'}
                                    </h5>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12} className="text-center">
                                    <Button
                                        onClick={this.handleConfirmMsgClose}
                                        className="but-gray"
                                    >
                                        Return
                                    </Button>
                                </Col>
                            </Row>

                        </Modal.Body>
                    </Modal>

                    {/*======  before action confirmation massage popup  ===== */}
                    <Modal
                        show={this.state.alertMsg}
                        onHide={this.closeAlertMsg}
                        className="payOptionPop"
                    >
                        <Modal.Body>
                            <div className="m-auto text-center">
                                <h6 className="mb-3">

                                    {
                                        this.state.alertState === 'delete'
                                            ?
                                            <span>Do you want to delete this bank holiday?</span>
                                            :
                                            (
                                                this.state.alertState === 'add'
                                                    ?
                                                    <span>Do you want set this bank as a default bank?</span>
                                                    :
                                                    (
                                                        this.state.alertState === 'edit'
                                                            ?
                                                            <span>Do you want to Change this bank status?</span>
                                                            : null
                                                    )
                                            )
                                    }

                                </h6>
                            </div>
                            <div className="m-auto text-center">
                                <button className="btn btn-secondary mr-2  btn-darkBlue" onClick={() => this.closeAlertMsg()}>Return</button>

                                {/* diffrent action called from here */}

                                {this.state.alertState === 'delete'
                                    ?
                                    (
                                        this.state.errorMessge == null
                                            ?
                                            <button className="btn btn-danger" onClick={() => this.handledelete()}>Confirm</button>
                                            :
                                            null
                                    )
                                    : ''


                                }
                            </div>

                        </Modal.Body>
                    </Modal>


                </div>

            </React.Fragment>
            
        );
    }

}
export default BankHolidays;
