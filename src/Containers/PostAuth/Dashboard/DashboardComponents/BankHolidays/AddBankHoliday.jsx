import React, { Component } from 'react';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';
import { CountryService } from './../../../../../services/country.service';
import axios from '../../../../../shared/eaxios';
import DatePicker from 'react-date-picker';
import PropTypes from 'prop-types';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
//let moment = require('moment');


let initialValues = {
    date: '',
    purpose: '',
    countryName: '',
};

const holidaySchema = Yup.object().shape({   
    countryName: Yup.string()
        .required('Please select country'),    
    date: Yup.string()
        .trim('Please remove whitespace')
        .required('Please enter Date'),    
    purpose: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter holiday purpose')

});




class AddBankHoliday extends Component {

    state=({
        countries:[],
        loader:false,
        errorMessge:null,
        holidayId:null,
    })
    _isMounted = false;

    componentDidMount() {
        console.log('------------data----------', this.props.data);
    
        this._isMounted = true;

        const countryService = new CountryService;
        countryService.getData().then(({ countries }) => { 
            this.setState({ countries });
        });
        
        if (this.props.data.name === 'edit'){
            initialValues = {
                date: new Date(this.props.data.date),
                purpose: this.props.data.purpose,
                countryName: this.props.data.countryName,
            };
            this.setState({
                holidayId: this.props.data.id,
            });
        }

        
    }
    
    componentWillUnmount() {
        this._isMounted = false;
        initialValues = {
            date: '',
            purpose: '',
            countryName: '',
        };
        this.setState({
            getdate: null

        });
    }


    displayError = (e) => {
        console.log('displayError', e.data.message);
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }

        return errorMessge;
    }



    handleSubmit = (values) =>{
        this.setState({
            loader:true
        });
        
        //values.date = moment(new Date(values.date)).add(1, 'days')._d;
        
        values.date.setDate(values.date.getDate() + 1);
        axios
            .post('holiday/add', values)
            .then(res => {
                console.log(res);
                this.setState({
                    loader: false
                }, () => this.props.onSuccess());
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    errorMessge: errorMsg,
                    loader: false

                });

                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);

            });
    }

    handleEdit = (values) => {

        let temp1 = (new Date(this.props.data.date)).toString();
        let temp2 = (new Date(values.date)).toString();

        console.log(temp1);
        console.log(temp2);
        
        if (temp1 !== temp2){
            values.date.setDate(values.date.getDate() + 1);
        }
        
        this.setState({
            loader: true
        });
        

        axios
            .put(`holiday/update/${this.state.holidayId}`, values)
            .then(res => {
                console.log(res);
                this.setState({
                    loader: false
                }, () => this.props.onSuccess());
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    errorMessge: errorMsg,
                    loader: false

                });

                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);

            });
    }


    render() {
        return (
            <div>
                {this.state.errorMessge && <div className="alert alert-warning" role="alert">{this.state.errorMessge}</div>}
                <Formik
                    initialValues={initialValues}
                    validationSchema={holidaySchema}
                    onSubmit={this.props.data.name === 'edit' ? this.handleEdit : this.handleSubmit}
                    enableReinitialize={true}
                    //enableReinitialize
                >
                    {({
                        values,
                        errors,
                        touched,
                        handleChange,
                        //handleBlur,
                        setFieldValue
                    }) => {

                        return (
                            <Form>
                                
                                <div className="row">
                                    {this.state.loader && <div className="col-12"><LoadingSpinner /></div>}
                                    {this.props.isLoading && <div className="col-12"><LoadingSpinner /></div>}
                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group" controlid="formBasicText">
                                            <div>Country <span className="required">*</span></div>

                                            <Field
                                                name="countryName"
                                                component="select"
                                                className={`input-elem ${values.countryName &&
                                                    'input-elem-filled'} form-control`}
                                                autoComplete="nope"
                                                value={values.countryName || ''}
                                                onChange={e => {
                                                    handleChange(e);
                                                }}
                                            >
                                                <option value="" defaultValue="select">
                                                    {this.state.countries.length ? 'Select Country' : 'Loading...'}
                                                </option>
                                                {this.state.countries.map(country => (
                                                    <option
                                                        key={country.id}
                                                        value={country.countryName}
                                                    >
                                                        {country.countryName}
                                                    </option>
                                                ))}
                                            </Field>

                                            {errors.countryName && touched.countryName ? (
                                                <span className="errorMsg ml-3">
                                                    {errors.countryName}
                                                </span>
                                            ) : null}


                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group" controlid="formBasicText">
                                            <div>Date <span className="required">*</span></div>

                                            <DatePicker
                                                name="date"
                                                onChange={value =>
                                                {
                                                    console.log('value----', value),
                                                    setFieldValue(
                                                        'date',
                                                        value
                                                    );
                                                }
                                                   
                                                }
                                                value={values.date || ''}
                                                className={'form-control'}

                                            />

                                            {errors.date &&
                                                touched.date ? (
                                                    <span className="errorMsg ml-3">
                                                        {errors.date}
                                                    </span>
                                                ) : null}
                                        </div>
                                    </div> 

                                    <div className="col-xs-12 col-md-6">
                                        <div className="form-group" controlid="formBasicText">
                                            <div>Purpose <span className="required">*</span></div>
                                            <Field
                                                name="purpose"
                                                type="text"
                                                className={`input-elem ${values.purpose &&
                                                    'input-elem-filled'} form-control`}
                                                autoComplete="nope"
                                                value={values.purpose || ''}
                                            />
                                            {errors.purpose &&
                                                touched.purpose ? (
                                                    <span className="errorMsg ml-3">
                                                        {errors.purpose}
                                                    </span>
                                                ) : null}
                                        </div>
                                    </div>           
                                </div>
                                <div className="text-center">
                                    <button className="btn btn-primary" type="submit">
                                        Submit
                                    </button>                  
                                </div>
                                                 
                            </Form>
                        );   
                    }
                    }
                </Formik>                 

            </div>
        );
    }
}
AddBankHoliday.propTypes = {

    onSuccess: PropTypes.func,
    data: PropTypes.object,
    isLoading: PropTypes.bool
    
};
export default AddBankHoliday;
