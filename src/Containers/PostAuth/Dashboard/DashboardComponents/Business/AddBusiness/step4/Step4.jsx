import React, { Component } from 'react';
import { 
    Link, 
    //Redirect 
} from 'react-router-dom';
import { Row, Col, Button, Modal, FormGroup } from 'react-bootstrap';
import { Formik, Field, Form } from 'formik';
import { connect } from 'react-redux';
import Terms from './../../../../../../../Components/Dashboard/Terms/Terms';
import PropTypes from 'prop-types';



import * as Yup from 'yup';


import axios from './../../../../../../../shared/eaxios';

import { CountryService } from './../../../../../../../services/country.service';

const initialValues = {
    accountName: '',
    bankListId: '',
    accountNumber: '',
    address: '',
    swift: 'Don\'t know',
    //city: "",
    province: 'NET BANKING',
    zip: '',
    routingNumber: '',
    country: '',
    businessId: 1,
    state: '',
    countryId: '',
    cityId: '',
    stateId: ''
};

const step1Schema = Yup.object().shape({
    accountName: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .max(100)
        .required('Please enter name'),
    bankListId: Yup.string()
        .trim('Please remove whitespace')
        .required('Please select bank'),
    routingNumber: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .test(
            'validateRouting',
            'Please enter number',
            function (value) {
                if (value) {
                    let isValid = false;
                    if (/^[0-9]*$/.test(value)) {
                        isValid = true;
                    }
                    return isValid;
                }
                return true;
            }
        ).required('Please enter routing number'),
    accountNumber: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .test(
            'validateRouting',
            'Please enter number',
            function (value) {
                if (value) {
                    if (/^[0-9]*$/.test(value)) return true;
                    return false;
                }
                return true;
            }
        ).max(17, 'Account Number should be 17 digits maximum.').required('Please enter account number'),
    address: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .max(100)
        .required('Please enter bank address'),

    countryId: Yup.string()
        .trim('Please remove whitespace')
        .required('Please select country'),
    stateId: Yup.string()
        .trim('Please remove whitespace')
        .required('Please enter state'),

    cityId: Yup.string()
        .trim('Please remove whitespace')
        .required('Please enter city'),
    zip: Yup.string().test(
        'validateZipCode',
        'Please enter a valid zip code',
        function (value) {
            if (value) {
                let isValid = false;
                // eslint-disable-next-line no-useless-escape
                if (/^\d{5}[\-|\s]?\d{0,4}$/.test(value)) {
                    isValid = true;
                } else if (/^[a-zA-Z0-9]{3}\s[a-zA-Z0-9]{3}$/.test(value)) {
                    let expArr = value.split(' ');
                    if (/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{3}$/.test(expArr[0]) && /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{3}$/.test(expArr[1])) {
                        isValid = true;
                    }
                }
                return isValid;
            }
            return true;
        }
    ).required('Please enter zip code'),
    state: Yup.string().required('Please select state'),
    country: Yup.string().required('Please select country'),
});

class Step4 extends Component {
    state = {
        errorMessge: null,
        banklists: [],
        countries: [],
        states: [],
        cities: [],
        selectedStates: [],
        selectedCities: [],
        showTerms: false
    };

    backToBusinessDetails=()=> {

        console.log('yess shot the moon ');
        this.props.changeActiveTab('businessdetails');

        this.props.changeActiveSubTab('businessinfo');

    }

    termsHandleClose = () => {
        this.setState({ showTerms: false });
    };

    termsHandleShow = () => {
        this.setState({ showTerms: true });
    };

    handleSubmit = values => {
        console.log('Values ', values);
        values.zipCode = values.zip;

        for (const st of this.state.states) {
            if (parseInt(st.id) == parseInt(values.stateId)) values.stateCode = st.stateCode;
        }

        for (const country of this.state.countries) {
            if (parseInt(country.id) == parseInt(values.countryId)) values.countryCode = country.countryCode;
        }

        let valuesArray = {
            'bankListId': values.bankListId,
            'accountName': values.accountName,
            'routingNumber': values.routingNumber,
            'accountNumber': values.accountNumber,
            'address': values.address,
            'zipCode': values.zipCode,
            'countryCode': values.countryCode,
            'stateCode': values.stateCode,
            'cityId': values.cityId,
            'status': true,
            'activeStatus': true,
            'archieve': false
        };

        axios
            .post(`transaction/bank/create/${localStorage.getItem('businessId')}`, valuesArray)
            .then(res => {
                console.log('Response ', res);
                this.props.onSuccess();
            })
            .catch(this.displayError);
    };

    handleChange = (e, field) => {
        this.setState({
            [field]: e.target.value
        });
    };

    displayError = (e) => {
        console.log('Display error ', e);
        const errorMessge = e.data ? e.data.message ? e.data.message : e.data.error : 'Unknown error!';
        this.setState({
            errorMessge: errorMessge
        });
        setTimeout(() => {
            this.setState({ errorMessge: null });
        }, 3500);
    }

    componentWillMount() {
        //axios.get('/banklist')
        let direction = true;
        axios.get(`bank/list?since=0&limit=2000&&direction=${direction}`)
            .then(res => {
                const banklists = res.data.entries;
                this.setState({ banklists: banklists });
                console.log('master bank', this.state.banklists);
            })
            .catch(this.displayError);
    }

    componentDidMount() {
        const countryService = new CountryService;
        countryService.getData().then(({ countries, states, cities }) => {  //countries, states or cities
            this.setState({ countries, states, cities });
        });


    }

    populateState = e => {
        const countryId = e.target.value;
        const states = this.state.states;
        const newStates = states.filter(state => {
            return Number(state.countryId) === Number(countryId);
        });
        this.setState({ selectedStates: newStates, selectedCities: [] });
    };

    populateCity = e => {
        const stateId = e.target.value;
        const cities = this.state.cities;
        const newCities = cities.filter(city => {
            return Number(city.stateId) === Number(stateId);
        });
        this.setState({ selectedCities: newCities });
    };

    render() {
        console.log('Step 4 props~~~~~~~~', this.props);
        const { step3 } = this.props;
        if (!step3) {
            //return <Redirect to="/registration/step-1" />
        }    

        const { 
            selectedCities, 
            selectedStates, 
            //selectedCountryId 
        } = this.state;
        return (
            <div className="regSteps step4Outer">
                <div className="regWhiteOuter">

                    
                    <div className="regConInside">
                        <Formik
                            initialValues={initialValues}
                            validationSchema={step1Schema}
                            onSubmit={this.handleSubmit}
                        >
                            {({
                                values,
                                errors,
                                touched,
                                //isSubmitting,
                                handleChange,
                                //handleBlur,
                                setFieldValue
                            }) => {

                                return (
                                    <Form>

                                        <Row>
                                            <Col sm={12}>
                                                {this.state.errorMessge ? (
                                                    <div className="alert alert-danger text-center" role="alert">
                                                        <h6>{this.state.errorMessge}</h6>
                                                    </div>
                                                ) : null}
                                            </Col>
                                        </Row>

                                        <Row className="show-grid">

                                            <Col xs={12} md={6}>
                                                <FormGroup controlId="formBasicText">
                                                    <div>Name on the Account <span className="required">*</span></div>
                                                    <Field
                                                        name="accountName"
                                                        type="text"
                                                        className={`input-elem ${values.accountName &&
                                                            'input-elem-filled'} form-control`}
                                                        autoComplete="nope"
                                                        autoFocus
                                                        value={values.accountName || ''}
                                                    />
                                                    {errors.accountName && touched.accountName ? (
                                                        <span className="errorMsg ml-3">
                                                            {errors.accountName}
                                                        </span>
                                                    ) : null}


                                                </FormGroup>
                                            </Col>        


                                            <Col xs={12} md={6}>
                                                <FormGroup controlId="formBasicText">
                                                    <div>Bank Name <span className="required">*</span></div>
                                                    <Field
                                                        name="bankListId"
                                                        component="select"
                                                        className={`input-elem ${values.bankListId &&
                                                            'input-elem-filled'} form-control`}
                                                        autoComplete="nope"
                                                        placeholder="Enter"
                                                        value={values.bankListId || ''}
                                                    >
                                                        <option value="0" selected="selected">
                                                            Select Bank
                                                        </option>

                                                        {this.state.banklists.map(masterBanks => (
                                                            <option key={masterBanks.id} value={masterBanks.id}>
                                                                {masterBanks.bankname}
                                                            </option>
                                                        ))}
                                                    </Field>
                                                    {errors.bankListId && touched.bankListId ? (
                                                        <span className="errorMsg ml-3">
                                                            {errors.bankListId}
                                                        </span>
                                                    ) : null}


                                                </FormGroup>
                                            </Col>      
                                        </Row>
                                        <Row className="show-grid">
                                            <Col xs={12} md={6}>
                                                <FormGroup controlId="formBasicText">
                                                    <div>ABA/Routing Number <span className="required">*</span></div>
                                                    <Field
                                                        name="routingNumber"
                                                        type="text"
                                                        className={`input-elem ${values.routingNumber &&
                                                            'input-elem-filled'} form-control`}
                                                        autoComplete="nope"
                                                    />
                                                    {errors.routingNumber &&
                                                        touched.routingNumber ? (
                                                            <span className="errorMsg ml-3">
                                                                {errors.routingNumber}
                                                            </span>
                                                        ) : null}


                                                </FormGroup>
                                            </Col>           

                                            <Col xs={12} md={6}>
                                                <FormGroup controlId="formBasicText">
                                                    <div>Account Number <span className="required">*</span></div>
                                                    <Field
                                                        name="accountNumber"
                                                        type="text"
                                                        className={`input-elem ${values.accountNumber &&
                                                            'input-elem-filled'} form-control`}
                                                        autoComplete="nope"
                                                        value={values.accountNumber || ''}
                                                    />
                                                    {errors.accountNumber && touched.accountNumber ? (
                                                        <span className="errorMsg ml-3">
                                                            {errors.accountNumber}
                                                        </span>
                                                    ) : null}


                                                </FormGroup>
                                            </Col>                  
                                            
                                            
                                        </Row>
                                        <Row className="show-grid">
                                            
                                            <Col xs={12} md={6}>
                                                <FormGroup controlId="formBasicText">
                                                    <div>Bank Address <span className="required">*</span></div>
                                                    <Field
                                                        name="address"
                                                        type="text"
                                                        className={`input-elem ${values.address &&
                                                            'input-elem-filled'} form-control`}
                                                        autoComplete="nope"
                                                        value={values.address || ''}
                                                    />
                                                    {errors.address && touched.address ? (
                                                        <span className="errorMsg ml-3">{errors.address}</span>
                                                    ) : null}


                                                </FormGroup>
                                            </Col> 



                                            <Col xs={12} md={6}>
                                                <FormGroup controlId="formBasicText">
                                                    <div>Zip Code <span className="required">*</span></div>
                                                    <Field
                                                        name="zip"
                                                        type="text"
                                                        className={`input-elem ${values.zip &&
                                                            'input-elem-filled'} form-control`}
                                                        autoComplete="nope"
                                                        value={values.zip || ''}
                                                    />
                                                    {errors.zip && touched.zip ? (
                                                        <span className="errorMsg ml-3">{errors.zip}</span>
                                                    ) : null}


                                                </FormGroup>
                                            </Col>            

                                            
                                            
                                        </Row>

                                        <Row className="show-grid">


                                            <Col xs={12} md={6}>
                                                <FormGroup controlId="formBasicText">
                                                    <div>Country <span className="required">*</span></div>
                                                    <Field
                                                        name="country"
                                                        component="select"
                                                        className={`input-elem ${values.country &&
                                                            'input-elem-filled'} form-control `}
                                                        autoComplete="nope"
                                                        value={values.country || ''}
                                                        onChange={e => {
                                                            console.log('E ', e);
                                                            handleChange(e);
                                                            setFieldValue('countryId', e.target.value);
                                                            this.populateState(e);
                                                        }}
                                                    >
                                                        <option value="" selected="selected">
                                                            {this.state.countries.length ? 'Select Country' : 'Loading...'}
                                                        </option>
                                                        {this.state.countries.map(country => (
                                                            <option key={country.id} value={country.id}>
                                                                {country.countryName}
                                                            </option>
                                                        ))}
                                                    </Field>
                                                    {errors.country && touched.country ? (
                                                        <span className="errorMsg ml-3">{errors.country}</span>
                                                    ) : null}


                                                </FormGroup>
                                            </Col> 
                                            
                                            <Col xs={12} md={6}>
                                                <FormGroup controlId="formBasicText">
                                                    <div>State <span className="required">*</span></div>
                                                    <Field
                                                        name="state"
                                                        component="select"
                                                        className={`input-elem topShift ${values.state &&
                                                            'input-elem-filled'} form-control`}
                                                        autoComplete="nope"
                                                        value={values.state || ''}
                                                        onChange={e => {
                                                            handleChange(e);
                                                            setFieldValue('stateId', e.target.value);
                                                            this.populateCity(e);
                                                        }}
                                                        disabled={selectedStates.length ? false : true}
                                                    >
                                                        <option value="" selected="selected">
                                                            Select state
                                                        </option>
                                                        {selectedStates.map(stName => (
                                                            <option value={stName.id} key={stName.id}>
                                                                {stName.stateName}
                                                            </option>
                                                        ))}
                                                    </Field>
                                                    {errors.state && touched.state ? (
                                                        <span className="errorMsg ml-3">{errors.state}</span>
                                                    ) : null}


                                                </FormGroup>
                                            </Col>



                                            <Col xs={12} md={6}>
                                                <FormGroup controlId="formBasicText">
                                                    <div>City <span className="required">*</span></div>
                                                    <Field
                                                        component="select"
                                                        name="cityId"
                                                        placeholder="select"
                                                        className={`input-elem topShift ${values.cityId &&
                                                            'input-elem-filled'} form-control`}
                                                        value={values.cityId || ''}
                                                        onChange={e => {
                                                            handleChange(e);
                                                        }}
                                                        disabled={selectedCities.length ? false : true}
                                                    >
                                                        <option value="">Select City</option>

                                                        {selectedCities.map(ct => (
                                                            <option value={ct.id} key={ct.id}>
                                                                {ct.cityName}
                                                            </option>
                                                        ))}
                                                    </Field>
                                                    {errors.cityId && touched.cityId ? (
                                                        <span className="errorMsg ml-3">{errors.cityId}</span>
                                                    ) : null}


                                                </FormGroup>
                                            </Col>


                                        </Row>

                                        <Row>
                                            <Col md={12}>
                                                <p className="textReqiredRegPayment"><span className="required">*</span> These fields are required.</p>
                                            </Col>
                                        </Row>

                                        <Row className="show-grid text-center">
                                            <Col md={12}>

                                                {/* <Button className="btnStylePrimary backBtn text-left" onClick={this.backToBusinessDetails}>
                                                            Go Back
                                                </Button> */}
                                                <Button className="btnStylePrimary" type="submit">
                                                        Next
                                                </Button>
                                            </Col>
                                        </Row>
                                    </Form>
                                );
                            }}
                        </Formik>
                    </div>
                    {/*--End: regConInside--*/}

                    <Row className="show-grid">
                        <Col md={12} className="text-center">
                                By creating an account, you are agreeing to our{' '}
                            <Link to="#" onClick={this.termsHandleShow}>terms</Link>
                        </Col>
                    </Row>
                </div>
                {/*--End: regWhiteOuter--*/}

                


                <Modal show={this.state.showTerms} onHide={this.termsHandleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Terms</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Terms />
                        <Row className="show-grid text-center">
                            <Col xs={12} md={12}>
                                <Button
                                    className="but-gray  m-l-5"
                                    onClick={this.termsHandleClose}
                                >
                                    Close
                                </Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}


//export default Step4;


Step4.propTypes = {

    step3: PropTypes.object,
    changeActiveSubTab: PropTypes.func,
    changeActiveTab:PropTypes.func,
    onSuccess:PropTypes.func,
    // step1data: PropTypes.object,
    // step3data: PropTypes.object,
    // onSubmitAction: PropTypes.func,

};

const mapStateToProps = state => {
    return {
        step3: state.businessRegistration.step3
    };
};

export default connect(
    mapStateToProps
)(Step4);
