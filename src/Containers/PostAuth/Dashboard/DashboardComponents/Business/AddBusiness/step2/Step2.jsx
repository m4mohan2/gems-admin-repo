import React, { Component } from 'react';
import { Row, Col, Button, Modal, Image, FormGroup } from 'react-bootstrap';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';
import { connect } from 'react-redux';
import SuccessIco from './../../../../../../../assets/success-ico.png';
import InputMask from 'react-input-mask';
import { businessRegistrationProcessStep2 } from './../../../../../../../redux/actions/businessRegistration';
import PropTypes from 'prop-types';

import axios from './../../../../../../../shared/eaxios';


const initialValues = {
    otp: '',
    phone: '',
    emailOtp: ''
};




const step2Schema = Yup.object().shape({
    emailOtp: Yup.string('Please enter verification code sent in email')
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter verification code sent in email')
        .max(6)
        .min(6),
    otp: Yup.string('Please enter verification code sent in sms')
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter verification code sent in sms')
        .max(6)
        .min(6),
    phone: Yup.string()
        .min(14, 'Phone number must be 10 digit')
        .required('Please enter phone number')
});

const verifyOtpSchema = Yup.object().shape({
    emailOtp: Yup.string('Please enter verification code sent in email')
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter verification code sent in email')
        .max(6)
        .min(6)
});

class Step2 extends Component {
  state = {
      errorMessge: null,
      shown: false,
      showTerms: false,
      showSuccessModal: false
  };

  handleSuccessClose = () => {
      this.setState({ showSuccessModal: false });
  };

    backToUserDetails = () => {
        this.props.changeActiveTab('user');
    }

  displayError = e => {
      const errorMessge = e.data
          ? e.data.message
              ? e.data.message
              : e.data.error
          : 'Unknown error!';
      this.setState({
          errorMessge: errorMessge
      });
      setTimeout(() => {
          this.setState({ errorMessge: null });
      }, 3500);
  };

  sendVerificationMail = (displaySuccess = 1) => {
      axios
          .get('/user/sendVerificationEmail')
          .then(response => {
              console.log('Send mail response ', response);
              (response.status === 200) && (displaySuccess !== 2) && this.setState({ showSuccessModal: true });
          })
          .catch(this.displayError);
  };

  

  componentDidMount() {
      const step1Data = { ...this.props.step1 };
      step1Data && this.sendVerificationMail(2);
  }

  termsHandleClose = () => {
      this.setState({ showTerms: false });
  };

  termsHandleShow = () => {
      this.setState({ showTerms: true });
  };

  showPhoneHandler = () => {
      this.setState({
          shown: !this.state.shown
      });
  };

  handleSubmit = values => {
      console.log('Handle submit ', values);
      //this.props.onSubmitAction(values);
      //this.props.history.push("/registration/step-3");
  };

  verifyEmail = values => {
      const code = values.emailOtp;

      console.log('verify email values ', code);
      if (code) {
          const values = {
              verificationCode: code
          };

          axios
              .put('/user/verifyEmail', values)
              .then(response => {
                  console.log('Send mail response ', response);
                  if (response && response.status == 200){
                      this.props.changeActiveTab('businessdetails');
                  }
                      
              })
              .catch(this.displayError);
      }
  };

  render() {

      //   const { step1 } = this.props;
      //   if (!step1) return <Redirect to="/registration/step-1" />;

      let shown = {
          display: this.state.shown ? 'block' : 'none'
      };

      return (
          <div className="regSteps step2Outer">
            
              {/*--End:regToppanel--- */} 
              <div className="regWhiteOuter text-center">
                  <Row>
                      <Col md={12}>
                          <p className="textWidth stepsIntro p-4 text-center">
                  For your security, we want to make sure itss really you. We
                  have sent an email with a 6-digit verification code to the
                  email address you provided. Please enter the code you received
                  below.
                          </p>
                      </Col>
                  </Row>

                  <div className="regConInside">
                      <Formik
                          initialValues={initialValues}
                          validationSchema={verifyOtpSchema}
                          onSubmit={this.verifyEmail}
                      >
                          {({ 
                              values, 
                              touched, 
                              //handleChange, 
                              //handleBlur, 
                              errors 
                          }) => {
                              return (
                                  <Form>
                                      <Row>
                                          <Col sm={12} className="text-center">
                                              {this.state.errorMessge ? (
                                                  <div className="alert alert-danger" role="alert">
                                                      <h6>{this.state.errorMessge}</h6>
                                                  </div>
                                              ) : null}
                                          </Col>
                                      </Row>

                                      

                                      <Row className="show-grid px-4">
                                          <Col xs={12} md={6}>
                                              <FormGroup controlId="formBasicText">
                                                  <div className="text-left">Verification Code Sent In Email <span className="required">*</span></div>
                                                  <Field
                                                      name="emailOtp"
                                                      type="password"
                                                      value={values.emailOtp}
                                                      autoFocus
                                                      className={`input-elem ${values.firstName &&
                                                          'input-elem-filled'} form-control w-100`}
                                                  />
                                                  {errors.emailOtp && touched.emailOtp ? (
                                                      <span className="errorMsg ml-3">
                                                          {errors.emailOtp}
                                                      </span>
                                                  ) : null}


                                              </FormGroup>
                                          </Col>
                                          <Col xs={12} md={6} className="pt-4 text-left">
                                              <Button className="btnOutLine regDefaultBtn mr-3" type="submit">
                                                Verify Email
                                              </Button>
                                              <Button className="btnOutLine regDefaultBtn resendCode" onClick={this.sendVerificationMail}>
                                                Resend Verification Code
                                              </Button>
                                          </Col>
                                      </Row>
                                  </Form>
                              );
                          }}
                      </Formik>
                      {/* check */}
                      <div style={{ display: 'none' }}>
                          <Formik
                              initialValues={initialValues}
                              validationSchema={step2Schema}
                              onSubmit={this.handleSubmit}
                          >
                              {({ values, touched, handleChange, handleBlur, errors }) => (
                                  <Form>
                                      <Row className="show-grid">
                                          <Col md={10} />
                                      </Row>

                                      <Row className="show-grid">
                                          <Col md={12}>
                                              <p style={{ marginTop: '20px', fontSize: '19px' }}>
                            OR{' '}
                                              </p>
                                              <p className="paraStyle02">
                            Enter your mobile phone number to receive an SMS
                            message with a verification code.
                                                  <span
                                                      onClick={this.showPhoneHandler}
                                                      className={
                                                          this.state.shown
                                                              ? 'phValidationBtn closeIco'
                                                              : 'phValidationBtn openIco'
                                                      }
                                                  >
                              open
                                                  </span>
                                              </p>
                                          </Col>
                                      </Row>

                                      <div className="phoneValidation" style={shown}>
                                          <Row className="show-grid">
                                              <Col md={12}>
                                                  <div className="question otpNo tel">
                                                      <InputMask
                                                          mask="1 999-999-9999"
                                                          maskChar={null}
                                                          type="tel"
                                                          name="phone"
                                                          className={`input-elem ${values.phone &&
                                  'input-elem-filled'}`}
                                                          value={values.phone}
                                                          onChange={handleChange}
                                                          onBlur={handleBlur}
                                                      />
                                                      <label>Phone Number</label>
                                                      {errors.phone && touched.phone ? (
                                                          <span className="errorMsg">{errors.phone}</span>
                                                      ) : null}
                                                  </div>
                                                  <Button className="sendOtp">
                              Send Verification Code
                                                  </Button>
                                              </Col>
                                          </Row>

                                          <Row className="show-grid">
                                              <Col md={10}>
                                                  <div className="question otpNo">
                                                      <Field
                                                          name="otp"
                                                          type="password"
                                                          value={values.otp}
                                                      />
                                                      <label>Verification Code Sent In SMS</label>
                                                      {errors.otp && touched.otp ? (
                                                          <span className="errorMsg">{errors.otp}</span>
                                                      ) : null}
                                                  </div>
                                                  <Button className="sendOtp">
                              Verify Phone Number
                                                  </Button>
                                              </Col>
                                          </Row>
                                      </div>


                                  </Form>
                              )}
                          </Formik>
                          
                      </div>

                      <Row>
                          <Col md={12} className="px-4">
                              <p className="textReqiredRegPayment text-left ml-3"><span className="required">*</span> This field is required.</p>
                          </Col>
                      </Row>

                      <Row className="show-grid">
                          <Col className="text-center">
                              <Button onClick={ this.backToUserDetails } className="btnStylePrimary backBtn text-left">
                                    Go Back
                              </Button>
                            

                              {/* <Button className="btnStylePrimary" type="submit">
                          Next
                        </Button> */}
                          </Col>
                      </Row>

                  </div>
              </div>

              <Modal show={this.state.showTerms} onHide={this.termsHandleClose}>
                  <Modal.Header closeButton>
                      <Modal.Title>TERMS OF USE of <br /> FintainiumLLC. offer</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                      <Row className="show-grid text-center">
                          <Col xs={12} md={12}>
                              <Button
                                  className="but-gray  m-l-5 mt-3"
                                  onClick={this.termsHandleClose}
                              >
                  Close
                              </Button>
                          </Col>
                      </Row>
                  </Modal.Body>
              </Modal>

              <Modal
                  show={this.state.showSuccessModal}
                  onHide={this.handleSuccessClose}
                  className="payOptionPop"
              >
                  <Modal.Body>
                      <Row>
                          <Col md={12} className="text-center">
                              <Image src={SuccessIco} />
                          </Col>
                      </Row>
                      <Row>
                          <Col md={12} className="text-center">
                              <h5>
                  Verification code has been sent successfully
                              </h5>
                          </Col>
                      </Row>
                  </Modal.Body>
                  <Modal.Footer>
                      <Button
                          onClick={this.handleSuccessClose}
                          className="but-gray"
                      >
              Return
                      </Button>
                  </Modal.Footer>
              </Modal>

          </div>
      );
  }
}

Step2.propTypes = {
    
    step1: PropTypes.object,
    changeActiveTab: PropTypes.func,
    
};

const mapStateToProps = state => {
    return {
        step1: state.businessRegistration.step1
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSubmitAction: data => dispatch(businessRegistrationProcessStep2(data))
    };
};
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Step2);


