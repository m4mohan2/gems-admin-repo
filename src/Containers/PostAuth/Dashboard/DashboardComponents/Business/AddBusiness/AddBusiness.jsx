import React,{Component} from 'react';
import { Tab, Tabs } from 'react-bootstrap';
import Step1 from './step1/Step1';
//import Step2 from './step2/Step2';
import Step3 from './step3/Step3';
//import Step3signerinfo from './step3/Step3-substep2';
//import Step4 from './step4/Step4';
import PropTypes from 'prop-types';
import './../Business.scss';

class AddBusiness extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            key: 'user',
            keysub:'businessinfo'
        };
    }

    tabUpdate=(pram)=> {
        this.setState({
            key: pram,
        });

        
    }

    subTabUpdate = (pram) => {
        this.setState({
            keysub: pram,
        });

    }



    render() {
        return (
            <div>

                <Tabs
                    id="add-business"
                    activeKey={this.state.key}
                    onSelect={key => this.setState({ key })}
                    className="addFromTab"
                >
                    <Tab
                        eventKey="user"
                        title="YOUR DETAILS"
                        disabled={this.state.key && this.state.key === 'user' ? false : true}
                    >
                        {this.state.key === 'user' ? <Step1 changeActiveTab={this.tabUpdate} /> : null}
                    </Tab>
                    {/* <Tab 
                    eventKey="verify" 
                    title="VERIFY" 
                    disabled={this.state.key && this.state.key === 'verify' ? false : true}
                >
                    {this.state.key === 'verify' ? <Step2 changeActiveTab={this.tabUpdate}/>: null}
                </Tab> */}
                    <Tab
                        eventKey="businessdetails"
                        title="BUSINESS DETAILS"
                        disabled={this.state.key && this.state.key === 'businessdetails' ? false : true}
                    >
                        <Step3 onSuccess={this.props.onSuccess} changeActiveTab={this.tabUpdate} changeActiveSubTab={this.subTabUpdate} />

                        {/* {this.state.key === 'businessdetails' ? <Tabs
                            id="business-details"
                            activeKey={this.state.keysub}
                            onSelect={keysub => this.setState({ keysub })}
                            className="addFromSubTab"
                        >
                            <Tab 
                                eventKey="businessinfo" 
                                title="Business Information"
                            >
                                <Step3 changeActiveTab={this.tabUpdate} changeActiveSubTab={this.subTabUpdate} />
                            </Tab>
                            <Tab 
                                eventKey="signerinfo" 
                                title="Authorized Signer Information"
                                disabled={this.state.key && this.state.key === 'signerinfo' ? false : true}
                            >
                                <Step3signerinfo changeActiveTab={this.tabUpdate} changeActiveSubTab={this.subTabUpdate} />
                            </Tab> 
                        </Tabs> : null} */}

                    </Tab>
                    {/* <Tab
                        eventKey="bank"
                        title="CONNECT YOUR BANK"
                        disabled={this.state.key && this.state.key === 'bank' ? false : true}
                    >
                        {
                            this.state.key === 'bank'
                                ? <Step4 onSuccess={this.props.onSuccess} changeActiveSubTab={this.subTabUpdate} changeActiveTab={this.tabUpdate} />
                                : null
                        }
                        
                    </Tab> */}
                </Tabs>


            </div>
        );
    }
}
AddBusiness.propTypes = {

    onSuccess: PropTypes.func,

};
export default AddBusiness;
