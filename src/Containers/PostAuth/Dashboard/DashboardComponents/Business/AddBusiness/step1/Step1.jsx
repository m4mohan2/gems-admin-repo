import React, { 
    Component,
    //Fragment 
} from 'react';
import {
    Row,
    Col,
    FormGroup,
} from 'react-bootstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';

import axios from './../../../../../../../shared/eaxios';

import { businessRegistrationProcessStep1 } from './../../../../../../../redux/actions/businessRegistration';

let initialValues = {
    firstName: '',
    lastName: '',
    userName: '',
    password: '',
    confirmpassword: '',
    businessEmail:'',
    //businessType: '',
    businessEmailVerified: false,
    emailAddress: '',
    businessName: '',
    businessNumber: 12,

    changeRequired: true,
    neverExpire: true,
    inactivityPeriod: 100,
    credentialsExpired: false,
    enable: true,
    roles: ['Payor'],
};

let initialValuesRef = initialValues;

const step1Schema = Yup.object().shape({
    // firstName: Yup.string()
    //     .trim('Please remove whitespace')
    //     .strict()
    //     .required('Please enter first name'),
    // lastName: Yup.string()
    //     .trim('Please remove whitespace')
    //     .strict()
    //     .required('Please enter last name'),

    businessEmail: Yup.string()
        .trim('Please remove whitespace')
        .email('Email ID must be valid')
        .strict()
        .required('Please enter valid business email id'),

    // emailAddress: Yup.string()
    //     .trim('Please remove whitespace')
    //     .email('Email ID must be valid')
    //     .strict()
    //     .required('Please enter valid email id'),
    businessName: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter company name'),

    // businessType: Yup.string()
    //     .strict()
    //     .required('Please select a business type'),
    // password: Yup.string()
    //     .trim('Please remove whitespace')
    //     .strict()
    //     .min(8)
    //     .required('Please enter password'),
    // confirmpassword: Yup.string()
    //     .trim('Please remove whitespace')
    //     .strict()
    //     .oneOf([Yup.ref('password')], 'Password must match')
    //     .required('Please enter password again')
});


// const businessTypeOpt = () => {
//     return (
//         <Fragment>
//             <option value="">Select a Business Type</option>
//             <option value="CORPORATION">Corporation</option>
//             <option value="LIMITED_LIABILITY_COMPANY">Limited Liability Company (LLC)</option>
//             <option value="SOLE_PROPRIETORSHIP"> Sole Proprietorship </option>
//             <option value="MUNICIPAL"> Municipal </option>
//             <option value="NON_PROFIT_CORPORATION"> Non-Profit Corporation </option>
//             <option value="PARTNERSHIP"> Partnership </option>
//             <option value="PUBLICLY_TRADED_COMPANY"> Publicly Traded Company </option> 
//         </Fragment>
//     );
// };

class Step1 extends Component {

    state = {
        errorMessge: null,
    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Unknown error!';
        }

        this.setState({
            errorMessge: errorMessge
        });

        setTimeout(() => {
            this.setState({ errorMessge: null });
        }, 5000);
    }

    componentDidMount() {
        initialValues = {
            firstName: '',
            lastName: '',
            userName: '',
            password: '',
            confirmpassword: '',
            businessEmail:'',
            businessEmailVerified: false,
            emailAddress: '',
            businessName: '',
            businessNumber: 12,
            //businessType: '',
            changeRequired: true,
            neverExpire: true,
            inactivityPeriod: 100,
            credentialsExpired: false,
            enable: true,
            roles: ['Payor'],
        };


    }
    componentDidUpdate() {
        initialValues = {
            firstName: '',
            lastName: '',
            userName: '',
            password: '',
            confirmpassword: '',
            businessEmail:'',
            businessEmailVerified: false,
            emailAddress: '',
            businessName: '',
            businessNumber: 12,
            // businessType: '',
            changeRequired: true,
            neverExpire: true,
            inactivityPeriod: 100,
            credentialsExpired: false,
            enable: true,
            roles: ['Payor'],
        };
    }
    componentUnmount() {

    }

    handleSubmit = values => {
        console.log('calling submit....');
        const temp = new Date();
        let tempUsername = 'user_' + temp.getTime();
        // eslint-disable-next-line no-unused-vars
        let tempEmail = temp.getTime();
        tempEmail = 'noemail_' + tempEmail + '@fintainium.com';

        const registrationArr = {
            businessName: values.businessName,///
            businessNumber: '',
            businessAddress1: '',
            businessCountryCode: 'US',
            businessStateCode: 'NY',
            businessCityId: 1,
            businessZipcode: '',
            businessPhone: '',
            businessPhoneVerified: false,
            businessWebsite: '',
            businessEmail: values.businessEmail,
            businessEmailVerified: false,
            //businessType: values.businessType,
            incorporationDate: null,
            incorporationStateCode: 'NY',
            incorporationCityId: 1,
            authorizerFristname: '',
            authorizerLastname: '',
            authorizerPIN: '',
            authorizerAddress: '',
            authorizerZipcode: '',
            authorizerCountryCode: 'US',
            authorizerStateCode: 'NY',
            authorizerCityId: 1,
            authorizerDateOfBirth: '',
            
            status: true,
            archive: false,
            isCustomer: true, 
        };

        //console.log('***************************', registrationArr);
        axios
            .post('/business', registrationArr)
            .then(response => {
                //console.log('Add business response ----------------------------------------------', response);
                localStorage.setItem('businessId', `${response.data.id}`);
                localStorage.setItem('isCustomer', `${response.data.isCustomer}`);
                localStorage.setItem('status', `${response.data.status}`);
                localStorage.setItem('archive', `${response.data.archive}`);
                // if (response.status == 200) {
                //     this.props.changeActiveTab('businessdetails');

                // }
                if (response.data && response.data.id > 0) {
                    let userArray = {
                        'firstName': values.firstName,
                        'lastName': values.lastName,
                        'userName': tempUsername,
                        //'password': values.password,
                        'password': null,
                        'emailAddress': values.emailAddress,
                        'phoneNumber': '',
                        'department': '',
                        'jobTitle': '',
                        'roles': ['Payor']
                    };

                    this.props.onSubmitAction(Object.assign(userArray, registrationArr));
                    this.props.changeActiveTab('businessdetails');

                    // axios
                    //     .post(`/user/create/${response.data.id}`, userArray).then(response => {
                    //         this.props.onSubmitAction(Object.assign(userArray, registrationArr));

                    //         if (response.status == 200) {
                    //             this.props.changeActiveTab('businessdetails');

                    //         }
                    //     }).catch(this.displayError);
                }
            })
            .catch(this.displayError);
    }

    render() {
        const { step1 } = this.props;
        step1 && Object.assign(initialValues, step1, { password: '', confirmpassword: '' });

        return (
            <div className="regSteps step1Outer">
                <div className="regWhiteOuter">
                    <Row className="show-grid text-center">
                        <Col md={12}>
                            <h3 className='text-primary pb-3'>Get Started with Fintainium</h3>
                        </Col>
                    </Row>

                    <div className="regConInside">
                        <Formik
                            initialValues={initialValuesRef}
                            validationSchema={step1Schema}
                            onSubmit={this.handleSubmit}
                        >
                            {({
                                values,
                                errors,
                                touched,
                                //isSubmitting 

                            }) => {
                                return (
                                    <Form className="px-4">
                                        <Row>
                                            <Col sm={12}>
                                                {this.state.errorMessge ? (
                                                    <div className="alert alert-danger text-center" role="alert">
                                                        <h6>{this.state.errorMessge}</h6>
                                                    </div>
                                                ) : null}
                                            </Col>
                                        </Row>

                                        <Row className="show-grid">


                                            {/* <Col xs={12} md={6}>
                                                <FormGroup controlId="formBasicText">
                                                    <div>First Name <span className="required">*</span></div>

                                                    <Field
                                                        name="firstName"
                                                        type="text"
                                                        className={`input-elem ${values.firstName &&
                                                            'input-elem-filled'} form-control`}
                                                    //autoComplete="nope"
                                                    //autoFocus
                                                    />
                                                    {errors.firstName && touched.firstName ? (
                                                        <span className="errorMsg pl-3">{errors.firstName}</span>
                                                    ) : null}


                                                </FormGroup>
                                            </Col>


                                            <Col xs={12} md={6}>
                                                <FormGroup controlId="formBasicText">
                                                    <div>Last Name <span className="required">*</span></div>

                                                    <Field
                                                        name="lastName"
                                                        type="text"
                                                        className={`input-elem ${values.lastName &&
                                                            'input-elem-filled'} form-control`}
                                                        autoComplete="nope"
                                                    />
                                                    {errors.lastName && touched.lastName ? (
                                                        <span className="errorMsg pl-3">
                                                            {errors.lastName}
                                                        </span>
                                                    ) : null}


                                                </FormGroup>
                                            </Col> */}

                                        </Row>
                                        <Row className="show-grid">


                                            {/* <Col xs={12} md={6}>
                                                <FormGroup controlId="formBasicText">
                                                    <div>User Email (Username) <span className="required">*</span></div>

                                                    <Field
                                                        name="emailAddress"
                                                        type="text"
                                                        className={`input-elem ${values.emailAddress &&
                                                            'input-elem-filled'} form-control`}
                                                        autoComplete="nope"
                                                    />
                                                    {errors.emailAddress && touched.emailAddress ? (
                                                        <span className="errorMsg pl-3">{errors.emailAddress}</span>
                                                    ) : null}


                                                </FormGroup>
                                            </Col> */}


                                            <Col xs={12} md={6}>
                                                <FormGroup controlId="formBasicText">
                                                    <div>Company Name <span className="required">*</span></div>

                                                    <Field
                                                        name="businessName"
                                                        type="text"
                                                        className={`input-elem ${values.businessName &&
                                                            'input-elem-filled'} form-control`}
                                                        autoComplete="nope"
                                                    />
                                                    {errors.businessName && touched.businessName ? (
                                                        <span className="errorMsg pl-3">
                                                            {errors.businessName}
                                                        </span>
                                                    ) : null}


                                                </FormGroup>
                                            </Col>
                                        
                                            {/* 
                                        <Row className="show-grid">

                                            <Col xs={12} md={6}>
                                                <FormGroup controlId="formBasicText">
                                                    <div>Password <span className="required">*</span></div>

                                                    <Field
                                                        name="password"
                                                        type="password"
                                                        className={`input-elem ${values.password &&
                                                            'input-elem-filled'} form-control`}
                                                        autoComplete="nope"
                                                    />
                                                    {errors.password && touched.password ? (
                                                        <span className="errorMsg pl-3">
                                                            {errors.password}
                                                        </span>
                                                    ) : null}


                                                </FormGroup>
                                            </Col>                    


                                            <Col xs={12} md={6}>
                                                <FormGroup controlId="formBasicText">
                                                    <div>Confirm Password <span className="required">*</span></div>

                                                    <Field
                                                        name="confirmpassword"
                                                        type="password"
                                                        className={`input-elem ${values.confirmpassword &&
                                                            'input-elem-filled'} form-control`}
                                                        autoComplete="nope"
                                                    />
                                                    {errors.confirmpassword &&
                                                        touched.confirmpassword ? (
                                                            <span className="errorMsg pl-3">
                                                                {errors.confirmpassword}
                                                            </span>
                                                        ) : null}


                                                </FormGroup>
                                            </Col>   
                                        </Row> */}
                                        


                                            <Col xs={12} md={6}>
                                                <FormGroup controlId="formBasicText">
                                                    <div>Business Email <span className="required">*</span></div>

                                                    <Field
                                                        name="businessEmail"
                                                        type="text"
                                                        className={`input-elem ${values.businessEmail &&
                                                        'input-elem-filled'} form-control`}
                                                        autoComplete="nope"
                                                    />
                                                    {errors.businessEmail && touched.businessEmail ? (
                                                        <span className="errorMsg pl-3">{errors.businessEmail}</span>
                                                    ) : null}


                                                </FormGroup>
                                            </Col>

                                            {/* <Col xs={12} md={6}>
                                                <FormGroup controlId="formBasicText">
                                                    <div>Legal Structure (Business type)<span className="required">*</span></div>
                                                    <Field 
                                                        component="select" 
                                                        name="businessType" 
                                                        className="form-control"
                                                    >
                                                        {businessTypeOpt()}
                                                    </Field>
                                                    {errors.businessType && touched.businessType ? (
                                                        <span className="errorMsg pl-3">{errors.businessType}</span>
                                                    ) : null}
                                                </FormGroup>
                                            </Col> */}
                                        </Row>
                                        <Row className="mt-3">
                                            <Col md={12}>
                                                <p className="textReqiredRegPayment"><span className="required">*</span> These fields are required.</p>
                                            </Col>
                                        </Row>
                                        <Row className="show-grid">
                                            <Col md={12} className="text-center">
                                                <button className="btnStylePrimary" type="submit">
                                                    Next
                                                </button>
                                            </Col>
                                        </Row>
                                    </Form>
                                );
                            }}
                        </Formik>
                    </div>
                    {/*---END: regConInside----*/}
                </div>
                {/*--End: regWhiteOuter--*/}
            </div>
        );
    }
}





Step1.propTypes = {
    onSubmitAction: PropTypes.func,
    step1: PropTypes.object,
    history: PropTypes.object,
    changeActiveTab: PropTypes.func,
};


const mapStateToProps = state => {
    return {
        step1: state.businessRegistration.step1
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSubmitAction: data => dispatch(businessRegistrationProcessStep1(data))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Step1);






