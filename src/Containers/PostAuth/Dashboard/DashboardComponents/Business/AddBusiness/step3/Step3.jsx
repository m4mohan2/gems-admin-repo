import React, { 
    Component,
    Fragment 
} from 'react';
import { Link, } from 'react-router-dom';
import {
    Row,
    Col,
    Button,
    Modal,
    FormGroup
} from 'react-bootstrap';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import DatePicker from 'react-date-picker';

import InputMask from 'react-input-mask';


import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';
import axios from './../../../../../../../shared/eaxios';

import Terms from './../../../../../../../Components/Dashboard/Terms/Terms';

import { businessRegistrationProcessStep3 } from './../../../../../../../redux/actions/businessRegistration';
import { CountryService } from './../../../../../../../services/country.service';



let initialValues = {
    businessName: '',
    businessNumber: '',
    businessAddress1: '',
    businessAddress2: '',
    businessType:'',
    businessCountryCode: '',
    businessStateCode: '',
    businessCityId: '',

    businessZipcode: '',
    businessPhone: '',
    businessPhoneExt: '',
    businessPhoneVerified: false,
    businessEmailVerified: false,
    businessWebsite: '',

    incorporationDate: '',
    //incorporationCountryCode: '',
    incorporationCountry: '',
    incorporationState: '',
    incorporationCity: '',
    registrationDate: '',
};


const step3Schema = Yup.object().shape({
    businessName: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter company name'),
    businessType: Yup.string()
        .strict()
        .required('Please select a business type'),
    businessNumber: Yup.string().min(10, 'Business number should be 9 digits minimum.').max(11, 'Business number should be 10 digits maximum.')
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter business tax ID/SSN'),
    businessAddress1: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter street address'),
    businessZipcode: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .test(
            'businessZipcode',
            'Please enter a valid zip code',
            function (value) {
                if (value) {
                    let isValid = false;
                    // eslint-disable-next-line no-useless-escape
                    if (/^\d{5}([\-|\s]\d{1,4})?$/.test(value)) {
                        isValid = true;
                    } else if (/^[a-zA-Z0-9]{3}\s[a-zA-Z0-9]{3}$/.test(value)) {
                        let expArr = value.split(' ');

                        if (/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{3}$/.test(expArr[0]) && /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{3}$/.test(expArr[1])) {
                            isValid = true;
                        }
                    }
                    return isValid;
                }

            }
        )
        .required('Please enter a valid zip code'),


    incorporationDate: Yup.string()
        .trim('Please remove whitespace')
        .required('Please enter Date'),
    businessPhone: Yup.string()
        .min(14, 'Phone number must be 10 digit')
        .required('Please enter phone number'),
    businessWebsite: Yup.string()
        .matches(
            // eslint-disable-next-line no-useless-escape
            /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/,
            'Website must be a valid url'
        )
        .required('Please enter website URL'),
    incorporationCountry: Yup.string()
        .trim('Please remove whitespace')    
        .strict()    
        .required('Please select country'),

    incorporationState: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please select state of incorporation'),
    incorporationCity: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please select city of incorporation '),

    businessCountryCode: Yup.string()
        .required('Please select country'),


    businessStateCode: Yup.string()
        .trim('Please remove whitespace')
        .required('Please select state'),


    businessCityId: Yup.string()
        .trim('Please remove whitespace')
        .required('Please select city'),


});

const businessTypeOpt = () => {
    return (
        <Fragment>
            <option value="">Select a Business Type</option>
            <option value="CORPORATION">Corporation</option>
            <option value="LIMITED_LIABILITY_COMPANY">Limited Liability Company (LLC)</option>
            <option value="SOLE_PROPRIETORSHIP"> Sole Proprietorship </option>
            <option value="MUNICIPAL"> Municipal </option>
            <option value="NON_PROFIT_CORPORATION"> Non-Profit Corporation </option>
            <option value="PARTNERSHIP"> Partnership </option>
            <option value="PUBLICLY_TRADED_COMPANY"> Publicly Traded Company </option> 
        </Fragment>
    );
};


class Step3 extends Component {

    step1Data = {};

    state = {

        businessName: null,
        errorMessge: null,
        dateIncorporation: new Date(),
        countries: [],
        states: [],
        cities: [],
        selectedStates: [],
        selectedCities: [],
        showTerms: false,
        selectedBusinessStates: [],
        selectedIncorpStates: [],
        selectedBusinessCities: [],
        selectedIncorpCities: [],

    };

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Unknown error!';
        }

        this.setState({
            errorMessge: errorMessge
        });

        setTimeout(() => {
            this.setState({ errorMessge: null });
        }, 5000);
    }

    handleSubmit = (values, { resetForm }) => {
        console.log(' Reset Form ', resetForm);
        values.businessEmail = this.props.step1data.businessEmail;
        values.isCustomer= true;
        values.status= true;
        values.archive= false;  
        
        axios
            //.put('/business', valuesArray)
            .put(`/business/${localStorage.getItem('businessId')}`, values)
            .then(() => {
                this.props.onSuccess();
                //this.props.onSubmitAction(null);
            })
            .catch(this.displayError);

        //this.props.onSubmitAction(values);
        
        //resetForm({});
        Object.assign(initialValues, {
            businessName: '',
            businessType:'',
            businessNumber: '',
            businessAddress1: '',
            businessAddress2: '',
            businessCountryCode: '',
            businessStateCode: '',
            businessCityId: '',
            businessZipcode: '',
            businessPhone: '',
            businessPhoneExt: '',
            businessPhoneVerified: false,
            businessEmailVerified: false,
            businessWebsite: '',
            incorporationDate: '',
            incorporationCountryCode: '',
            incorporationState: '',
            incorporationCity: '',
            registrationDate: '',
        });

        this.props.changeActiveSubTab('signerinfo');
    };

    onChange = date => this.setState({ dateIncorporation: date });

    termsHandleClose = () => {
        this.setState({ showTerms: false });
    };

    termsHandleShow = () => {
        this.setState({ showTerms: true });
    };


    handleChange = (e, field) => {
        this.setState({
            [field]: e.target.value
        });
    };

    populateState = (e, type) => {
        // eslint-disable-next-line no-undef
        console.log('calling...', e.target.value);
        const countryCode = e.target.value;
        const states = this.state.states;
        let countryId = 0;
        for (let country of this.state.countries) if (country.countryCode === countryCode) countryId = country.id;
        const newStates = states.filter(state => Number(state.countryId) === Number(countryId));

        if (type === 'business') {
            this.setState({ selectedBusinessStates: newStates, selectedBusinessCities: [] });
        }
        else if (type === 'incorp') {
            this.setState({ selectedIncorpStates: newStates, selectedIncorpCities: [] });
        }
    };

    populateCity = (e, type) => {
        console.log('populate city ', e);
        const stateCode = e.target.value;
        const cities = this.state.cities;
        let stateId = 0;
        for (let st of this.state.states) if (st.stateCode === stateCode) stateId = st.id;
        const newCities = cities.filter(ct => Number(ct.stateId) === Number(stateId));

        if (type === 'business') { this.setState({ selectedBusinessCities: newCities }); }
        else if (type === 'incorp') { this.setState({ selectedIncorpCities: newCities }); }

    };

    componentDidMount() {

        console.log('check props', this.props.step3data);
        this.props.step3data && this.setState({
            
        });

        const countryService = new CountryService;

        countryService.getData().then(({ countries, states, cities }) => {  //countries, states or cities
            this.setState({ countries, states, cities }, () => {
                if (this.props.step3data) {
                    this.populateState({ target: { value: this.props.step3data.businessCountryCode } }, 'business');
                    this.populateState({ target: { value: this.props.step3data.incorporationCountryCode } }, 'incorp');
                }
            });
        });
    }


    render() {

        let maxDate = new Date();
        maxDate.setDate(maxDate.getDate() - 1);

        const { step1data, step3data } = this.props;
        console.log('props from step3', this.props);

        step1data && Object.assign(initialValues, {
            businessName: step1data.businessName || ''
        });

        step3data && Object.assign(initialValues, step3data);
        console.log('propsssss', this.props);
        const {
            selectedBusinessStates,
            selectedIncorpStates,
            selectedBusinessCities,
            selectedIncorpCities
        } = this.state;

        return (
            <div className="regSteps step3Outer">
                <div className="regWhiteOuter">
                    <div className="regConInside">
                        <Formik
                            enableReinitialize
                            initialValues={initialValues}
                            validationSchema={step3Schema}
                            onSubmit={this.handleSubmit}
                        >
                            {({
                                values,
                                errors,
                                touched,
                                handleChange,
                                handleBlur,
                                setFieldValue
                            }) => {

                                return (
                                    <Form>

                                        <Row>
                                            <Col sm={12}>
                                                {this.state.errorMessge ? (
                                                    <div className="alert alert-danger text-center" role="alert">
                                                        <h6>{this.state.errorMessge}</h6>
                                                    </div>
                                                ) : null}
                                            </Col>
                                        </Row>


                                        <Row className="show-grid">
                                            <Col md={12}>
                                                <div className="businessDetailsTab">


                                                    <Row className="show-grid">

                                                    </Row>

                                                    <Row className="show-grid">

                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>Company Name <span className="required">*</span></div>
                                                                <Field
                                                                    name="businessName"
                                                                    type="text"
                                                                    className={`input-elem ${values.businessName &&
                                                                        'input-elem-filled'} form-control`}
                                                                    autoComplete="nope"
                                                                    value={values.businessName || ''}
                                                                    //autoFocus
                                                                    disabled
                                                                />
                                                                {errors.businessName &&
                                                                    touched.businessName ? (
                                                                        <span className="errorMsg ml-3">
                                                                            {errors.businessName}
                                                                        </span>
                                                                    ) : null}


                                                            </FormGroup>
                                                        </Col>
                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>Legal Structure (Business type)<span className="required">*</span></div>
                                                                <Field 
                                                                    component="select" 
                                                                    name="businessType" 
                                                                    className="form-control"
                                                                >
                                                                    {businessTypeOpt()}
                                                                </Field>
                                                                {errors.businessType && touched.businessType ? (
                                                                    <span className="errorMsg pl-3">{errors.businessType}</span>
                                                                ) : null}
                                                            </FormGroup>
                                                        </Col>
                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>Business Tax ID/SSN <span className="required">*</span></div>

                                                                <InputMask
                                                                    mask="99-99999999"
                                                                    maskChar={null}
                                                                    type="text"
                                                                    name="businessNumber"
                                                                    className={`input-elem ${values.businessNumber &&
                                                                        'input-elem-filled'} form-control`}
                                                                    value={values.businessNumber || ''}
                                                                    onChange={handleChange}
                                                                    onBlur={handleBlur}
                                                                />
                                                                {errors.businessNumber &&
                                                                    touched.businessNumber ? (
                                                                        <span className="errorMsg ml-3">
                                                                            {errors.businessNumber}
                                                                        </span>
                                                                    ) : null}


                                                            </FormGroup>
                                                        </Col>
                                                    </Row>

                                                    <Row className="show-grid">

                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>Street Address <span className="required">*</span></div>
                                                                <Field
                                                                    name="businessAddress1"
                                                                    type="text"
                                                                    className={`input-elem ${values.businessAddress1 &&
                                                                        'input-elem-filled'} form-control`}
                                                                    autoComplete="nope"
                                                                    value={values.businessAddress1 || ''}
                                                                />
                                                                {errors.businessAddress1 &&
                                                                    touched.businessAddress1 ? (
                                                                        <span className="errorMsg ml-3">
                                                                            {errors.businessAddress1}
                                                                        </span>
                                                                    ) : null}


                                                            </FormGroup>
                                                        </Col>

                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>Country <span className="required">*</span></div>

                                                                <Field
                                                                    name="businessCountryCode"
                                                                    component="select"
                                                                    className={`input-elem ${values.businessCountryCode &&
                                                                        'input-elem-filled'} form-control`}
                                                                    autoComplete="nope"
                                                                    value={values.businessCountryCode || ''}
                                                                    onChange={e => {
                                                                        handleChange(e);
                                                                        this.populateState(e, 'business');
                                                                    }}
                                                                >
                                                                    <option value="" defaultValue="select">
                                                                        {this.state.countries.length ? 'Select Country' : 'Loading...'}
                                                                    </option>
                                                                    {this.state.countries.map(country => (
                                                                        <option
                                                                            key={country.id}
                                                                            value={country.countryCode}
                                                                        >
                                                                            {country.countryName}
                                                                        </option>
                                                                    ))}
                                                                </Field>

                                                                {errors.businessCountryCode && touched.businessCountryCode ? (
                                                                    <span className="errorMsg ml-3">
                                                                        {errors.businessCountryCode}
                                                                    </span>
                                                                ) : null}


                                                            </FormGroup>
                                                        </Col>

                                                    </Row>

                                                    <Row className="show-grid">


                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>State <span className="required">*</span></div>
                                                                <Field
                                                                    name="businessStateCode"
                                                                    component="select"
                                                                    className={`input-elem topShift ${values.businessStateCode &&
                                                                        'input-elem-filled'} form-control`}
                                                                    autoComplete="nope"
                                                                    value={values.businessStateCode || ''}
                                                                    onChange={e => {
                                                                        handleChange(e);
                                                                        this.populateCity(e, 'business');
                                                                    }}
                                                                    disabled={
                                                                        this.state.selectedBusinessStates.length
                                                                            ? false
                                                                            : true
                                                                    }
                                                                >
                                                                    <option value="" defaultValue="select">
                                                                        Select state
                                                                    </option>
                                                                    {selectedBusinessStates.map(stName => (
                                                                        <option value={stName.stateCode} key={stName.id}>
                                                                            {stName.stateName}
                                                                        </option>
                                                                    ))}
                                                                </Field>
                                                                {errors.businessStateCode && touched.businessStateCode ? (
                                                                    <span className="errorMsg ml-3">
                                                                        {errors.businessStateCode}
                                                                    </span>
                                                                ) : null}
                                                            </FormGroup>
                                                        </Col>

                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>City <span className="required">*</span></div>
                                                                <Field
                                                                    component="select"
                                                                    name="businessCityId"
                                                                    placeholder="select"
                                                                    className={`input-elem topShift ${values.stateId &&
                                                                        'input-elem-filled'} form-control`}
                                                                    value={values.businessCityId || ''}
                                                                    onChange={e => {
                                                                        handleChange(e);
                                                                    }}
                                                                    disabled={selectedBusinessCities.length ? false : true}
                                                                >
                                                                    <option value="">Select City</option>

                                                                    {selectedBusinessCities.map(ct => (
                                                                        <option value={ct.id} key={ct.id}>
                                                                            {ct.cityName}
                                                                        </option>
                                                                    ))}
                                                                </Field>
                                                                {errors.businessCityId && touched.businessCityId ? (
                                                                    <span className="errorMsg ml-3">
                                                                        {errors.businessCityId}
                                                                    </span>
                                                                ) : null}
                                                            </FormGroup>
                                                        </Col>

                                                    </Row>

                                                    <Row className="show-grid">

                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>Zip Code <span className="required">*</span></div>
                                                                <Field
                                                                    name="businessZipcode"
                                                                    type="text"
                                                                    className={`input-elem ${values.businessZipcode &&
                                                                        'input-elem-filled'} form-control`}
                                                                    autoComplete="nope"
                                                                    value={values.businessZipcode || ''}
                                                                />
                                                                {errors.businessZipcode &&
                                                                    touched.businessZipcode ? (
                                                                        <span className="errorMsg ml-3">
                                                                            {errors.businessZipcode}
                                                                        </span>
                                                                    ) : null}
                                                            </FormGroup>
                                                        </Col>

                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>Company Phone <span className="required">*</span></div>
                                                                <InputMask
                                                                    mask="1 999-999-9999"
                                                                    maskChar={null}
                                                                    type="tel"
                                                                    name="businessPhone"
                                                                    className={`input-elem ${values.businessPhone &&
                                                                        'input-elem-filled'} form-control`}
                                                                    value={values.businessPhone || ''}
                                                                    onChange={handleChange}
                                                                    onBlur={handleBlur}
                                                                />
                                                                {errors.businessPhone &&
                                                                    touched.businessPhone ? (
                                                                        <span className="errorMsg ml-3">
                                                                            {errors.businessPhone}
                                                                        </span>
                                                                    ) : null}
                                                            </FormGroup>
                                                        </Col>

                                                    </Row>

                                                    <div className="stepsRowSpace">
                                                        <Row className="show-grid">

                                                            <Col xs={12} md={6}>
                                                                <FormGroup controlId="formBasicText">
                                                                    <div>Website <span className="required">*</span></div>
                                                                    <Field
                                                                        name="businessWebsite"
                                                                        type="text"
                                                                        className={`input-elem ${values.businessWebsite &&
                                                                            'input-elem-filled'} form-control`}
                                                                        autoComplete="nope"
                                                                        value={values.businessWebsite || ''}
                                                                    />
                                                                    {errors.businessWebsite &&
                                                                        touched.businessWebsite ? (
                                                                            <span className="errorMsg ml-3">
                                                                                {errors.businessWebsite}
                                                                            </span>
                                                                        ) : null}
                                                                </FormGroup>
                                                            </Col>

                                                            <Col xs={12} md={6}>
                                                                <FormGroup controlId="formBasicText">
                                                                    <div>Date of Incorporation<span className="required">*</span></div>

                                                                    <DatePicker
                                                                        name="incorporationDate"
                                                                        onChange={value =>
                                                                            setFieldValue(
                                                                                'incorporationDate',
                                                                                value
                                                                            )
                                                                        }
                                                                        maxDate={maxDate}
                                                                        value={values.incorporationDate || ''}
                                                                        className={'form-control'}

                                                                    />

                                                                    {errors.incorporationDate &&
                                                                        touched.incorporationDate ? (
                                                                            <span className="errorMsg ml-3">
                                                                                {errors.incorporationDate}
                                                                            </span>
                                                                        ) : null}
                                                                </FormGroup>
                                                            </Col>



                                                        </Row>
                                                    </div>
                                                    <div className="stepsRowSpace">
                                                        <Row className="show-grid">
                                                            <Col xs={12} md={6}>
                                                                <FormGroup controlId="formBasicText">
                                                                    <div>Country of  Incorporation <span className="required">*</span></div>

                                                                    <Field
                                                                        name="incorporationCountry"
                                                                        component="select"
                                                                        className={`input-elem ${values.incorporationCountry &&
                                                                            'input-elem-filled'} form-control`}
                                                                        autoComplete="nope"
                                                                        // value={values.incorporationCountry || ""}
                                                                        onChange={e => {
                                                                            handleChange(e);
                                                                            this.populateState(e, 'incorp');
                                                                        }}
                                                                        value={values.incorporationCountry || ''}
                                                                    >
                                                                        <option value="" defaultValue="select">
                                                                            {this.state.countries.length ? 'Select Country' : 'Loading...'}
                                                                        </option>
                                                                        {this.state.countries.map(country => (
                                                                            <option
                                                                                key={country.id}
                                                                                value={country.countryCode}
                                                                            >
                                                                                {country.countryName}
                                                                            </option>
                                                                        ))}
                                                                    </Field>

                                                                    {errors.incorporationCountry && touched.incorporationCountry ? (
                                                                        <span className="errorMsg ml-3">
                                                                            {errors.incorporationCountry}
                                                                        </span>
                                                                    ) : null}


                                                                </FormGroup>
                                                            </Col>

                                                            <Col xs={12} md={6}>
                                                                <FormGroup controlId="formBasicText">
                                                                    <div>State of Incorporation <span className="required">*</span></div>

                                                                    <Field
                                                                        name="incorporationState"
                                                                        component="select"
                                                                        className={`input-elem topShift ${values.incorporationState &&
                                                                            'input-elem-filled'} form-control`}
                                                                        autoComplete="nope"
                                                                        disabled={
                                                                            this.state.selectedIncorpStates.length
                                                                                ? false
                                                                                : true
                                                                        }
                                                                        onChange={e => {
                                                                            handleChange(e);
                                                                            this.populateCity(e, 'incorp');
                                                                        }}
                                                                        value={values.incorporationState || ''}
                                                                    >
                                                                        <option value="" defaultValue="select">
                                                                            Select state
                                                                        </option>
                                                                        {selectedIncorpStates.map(stName => (
                                                                            <option value={stName.stateCode} key={stName.id}>
                                                                                {stName.stateName}
                                                                            </option>
                                                                        ))}
                                                                    </Field>

                                                                    {errors.incorporationState &&
                                                                        touched.incorporationState ? (
                                                                            <span className="errorMsg ml-3">
                                                                                {errors.incorporationState}
                                                                            </span>
                                                                        ) : null}
                                                                </FormGroup>
                                                            </Col>



                                                            <Col xs={12} md={6}>
                                                                <FormGroup controlId="formBasicText">
                                                                    <div>City of Incorporation <span className="required">*</span></div>

                                                                    <Field
                                                                        component="select"
                                                                        name="incorporationCity"
                                                                        placeholder="select"
                                                                        className={`input-elem topShift ${values.incorporationCity &&
                                                                            'input-elem-filled'} form-control`}
                                                                        value={values.incorporationCity || ''}
                                                                        onChange={e => {
                                                                            handleChange(e);
                                                                        }}
                                                                        disabled={selectedIncorpCities.length ? false : true}
                                                                    >
                                                                        <option value="">Select City</option>

                                                                        {selectedIncorpCities.map(ct => (
                                                                            <option value={ct.id} key={ct.id}>
                                                                                {ct.cityName}
                                                                            </option>
                                                                        ))}
                                                                    </Field>

                                                                    {errors.incorporationCity &&
                                                                        touched.incorporationCity ? (
                                                                            <span className="errorMsg ml-3">
                                                                                {errors.incorporationCity}
                                                                            </span>
                                                                        ) : null}
                                                                </FormGroup>
                                                            </Col>


                                                        </Row>
                                                    </div>
                                                    {/*stepsRowSpace*/}
                                                </div>
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col md={12}>
                                                <p className="textReqiredRegPayment"><span className="required">*</span> These fields are required.</p>
                                            </Col>
                                        </Row>

                                        <Row className="show-grid">
                                            <Col md={12} className="text-center">
                                                {/* <Button onClick={this.handleBack} className="btnStylePrimary backBtn text-left">
                                                            Go Back
                                                </Button> */}
                                                <Button className="btnStylePrimary" type="submit">
                                                    Submit
                                                </Button>
                                            </Col>
                                        </Row>
                                    </Form>
                                );
                            }}
                        </Formik>
                    </div>
                    {/*--End: regConInside--*/}

                    <Row className="show-grid">
                        <Col md={12} className="text-center mt-3">
                            <p>
                                By creating an account, you are agreeing to our{' '}
                                <Link to="#" onClick={this.termsHandleShow}>terms</Link>
                            </p>
                        </Col>
                    </Row>
                </div>
                {/*--End: regWhiteOuter--*/}


                <Modal show={this.state.showTerms} onHide={this.termsHandleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>TERMS OF USE of <br /> FintainiumLLC. offer</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Terms />
                        <Row className="show-grid text-center">
                            <Col xs={12} md={12}>
                                <Button
                                    className="but-gray  m-l-5 mt-3"
                                    onClick={this.termsHandleClose}
                                >
                                    Close
                                </Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

            </div>
        );
    }
}

Step3.propTypes = {

    step1: PropTypes.object,
    changeActiveSubTab: PropTypes.func,
    step1data: PropTypes.object,
    step3data: PropTypes.object,
    onSubmitAction: PropTypes.func,
    changeActiveTab: PropTypes.func,
    onSuccess: PropTypes.func,

};

const mapStateToProps = state => {
    return {
        step1data: state.businessRegistration.step1,
        step3data: state.businessRegistration.step3
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSubmitAction: data => dispatch(businessRegistrationProcessStep3(data))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Step3);
