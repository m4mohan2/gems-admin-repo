
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
    //Grid, 
    Row,
    Col,
    Button,
    Modal,
    FormGroup
} from 'react-bootstrap';
import { connect } from 'react-redux';

import DatePicker from 'react-date-picker';


import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';

// import Autocomplete from 'react-autocomplete';
import Terms from './../../../../../../../Components/Dashboard/Terms/Terms';

import axios from './../../../../../../../shared/eaxios';


import { CountryService } from './../../../../../../../services/country.service';

import { businessRegistrationProcessStep3Sub } from '../../../../../../../redux/actions/businessRegistration';

import PropTypes from 'prop-types';


const initialValues = {
    authorizerFristname: '',
    authorizerLastname: '',
    //authorizerPIN: '',
    authorizerAddress: '',
    authorizerStateCode: '',
    authorizerCountryCode: '',
    authorizerCity: '',
    CN: '',
    authorizerZipcode: '',
    authorizerDOB: '',
    authorizerAge: ''
};

const step3Schema = Yup.object().shape({
    authorizerFristname: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter first name')
        .test('len', 'First name can not be more than 50 characters long', val => {
            if (val) return val.length <= 50;
            else return true;
        }),
    authorizerLastname: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter last name')
        .test('len', 'Last name can not be more than 50 characters long', val => {
            if (val) return val.length <= 50;
            else return true;
        }),
    // authorizerPIN: Yup.number().required("Please enter SSN/SIN"),
    authorizerCountryCode: Yup.string().required('Please select country'),
    authorizerAddress: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter street address'),
    authorizerStateCode: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter state'),
    authorizerCity: Yup.string()
        .strict()
        .required('Please enter city'),
    authorizerZipcode: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .test(
            'authorizerZipcode',
            'Please enter a valid zip code',
            function (value) {
                if (value) {
                    let isValid = false;
                    // eslint-disable-next-line no-useless-escape
                    if (/^\d{5}([\-|\s]\d{1,4})?$/.test(value)) {
                        isValid = true;
                    } else if (/^[a-zA-Z0-9]{3}\s[a-zA-Z0-9]{3}$/.test(value)) {
                        let expArr = value.split(' ');

                        if (/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{3}$/.test(expArr[0]) && /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{3}$/.test(expArr[1])) {
                            isValid = true;
                        }
                    }
                    return isValid;
                }

            }
        ).required('Please enter a valid zip code'),

    authorizerDOB: Yup.string()
        .required()
        .required('Please enter age'),
});

class Step3signerinfo extends Component {
    state = {
        errorMessge: null,
        countries: [],
        states: [],
        cities: [],
        selectedStates: [],
        selectedCities: [],
        local: {},
        showTerms: false

    };

    backToBusinessinfo = () => {
        this.props.changeActiveSubTab('businessinfo');

    }

    termsHandleClose = () => {
        this.setState({ showTerms: false });
    };

    termsHandleShow = () => {
        this.setState({ showTerms: true });
    };

    displayError = (e) => {
        const errorMessge = e.data ? (e.data.message ? e.data.message : e.data.error) : 'Unknown error!';
        this.setState({
            errorMessge: errorMessge
        });
        setTimeout(() => {
            this.setState({ errorMessge: null });
        }, 3500);
    }

    handleSubmit = values => {


        const step3Data = { ...values, ...this.props.step3 };

        console.log('Step 3 Data ', step3Data);
        //const authorizerCountryCode - countryId, authorizerStateCode - authorizerState

        let authorizerDOB = new Date(step3Data.authorizerDOB);
        let month = authorizerDOB.getUTCMonth() + 1; //months from 1-12
        let day = authorizerDOB.getUTCDate();
        let year = authorizerDOB.getUTCFullYear();
        day = day < 10 ? '0' + day : day;
        month = month < 10 ? '0' + month : month;
        authorizerDOB = year + '-' + month + '-' + day;

        let incorporationDate = new Date(step3Data.incorporationDate);
        let incorpmonth = incorporationDate.getUTCMonth() + 1; //months from 1-12
        let incorpday = incorporationDate.getUTCDate();
        let incorpyear = incorporationDate.getUTCFullYear();

        incorpday = incorpday < 10 ? '0' + incorpday : incorpday;
        incorpmonth = incorpmonth < 10 ? '0' + incorpmonth : incorpmonth;
        incorporationDate = incorpyear + '-' + incorpmonth + '-' + incorpday;

        function _calculateAge(
            //birthday
        ) {
            //let bd = new Date(birthday);
            let ageDifMs = Date.now() - step3Data.authorizerDOB.getTime();
            let ageDate = new Date(ageDifMs);
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        }



        const valuesArray = {
            'businessName': step3Data.businessName,
            'businessNumber': step3Data.businessNumber ? step3Data.businessNumber.split('-').join('') : null,
            'businessAddress1': step3Data.businessAddress1,
            'businessCountryCode': step3Data.businessCountryCode,
            'businessStateCode': step3Data.businessStateCode,
            'businessCityId': step3Data.businessCityId,
            'businessZipcode': step3Data.businessZipcode,
            'businessPhone': step3Data.businessPhone,
            'businessPhoneVerified': step3Data.businessPhoneVerified,
            'businessWebsite': step3Data.businessWebsite,
            'businessEmail': this.props.step1.businessEmail,
            //'businessEmailVerified': step3Data.businessEmailVerified,
            'businessEmailVerified': this.props.step1.businessEmailVerified,
            'incorporationDate': incorporationDate,
            'incorporationCountryCode': step3Data.incorporationCountryCode,
            'incorporationStateCode': step3Data.incorporationState,
            'incorporationCityId': step3Data.incorporationCity,
            'authorizerFristname': step3Data.authorizerFristname,
            'authorizerLastname': step3Data.authorizerLastname,
            //'authorizerPIN': step3Data.authorizerPIN,
            'authorizerAddress': step3Data.authorizerAddress,
            'authorizerZipcode': step3Data.authorizerZipcode,
            'authorizerCountryCode': step3Data.authorizerCountryCode,
            'authorizerStateCode': step3Data.authorizerStateCode,
            'authorizerCityId': step3Data.authorizerCity,
            'authorizerDOB': authorizerDOB,
            'authorizerAge': _calculateAge(step3Data.authorizerDOB),

            'status': this.props.step1.status,
            'archive': this.props.step1.archive,
            'isCustomer': this.props.step1.isCustomer,

        };


        console.log('----Values Array step3 2---------', valuesArray);

        axios
            //.put('/business', valuesArray)
            .put(`/business/${localStorage.getItem('businessId')}`, valuesArray)
            .then(() => {
                console.log('Save in redux');
                this.props.onSubmitAction(null);
                this.props.changeActiveTab('bank');
            })
            .catch(this.displayError);
    };

    calculateDate = (date, cb) => {
        const today = new Date();
        const dob = new Date(date);
        let timeDiff = Math.abs(today.getTime() - dob.getTime());
        timeDiff = Math.ceil(timeDiff / (1000 * 3600 * 24 * 365));

        // initialValues.age = timeDiff;
        if (typeof cb === 'function') cb(timeDiff);
    };

    handleChange = (e, field) => {
        this.setState({
            [field]: e.target.value
        });
    };

    componentDidMount() {

        console.log('Step3 data ', this.props.step3Sub);
        const countryService = new CountryService;
        countryService.getData().then(({ countries, states, cities }) => {  //countries, states or cities
            this.setState({ countries, states, cities }, () => {
                this.props.step3Sub && this.populateState({ target: { value: this.props.step3Sub.authorizerCountryCode } }, this.props.step3Sub.authorizerStateCode);
            });
        });
    }

    populateState = (e, stateCode = '') => {
        console.log('Populate state ', e.target.value, stateCode);
        const countryCode = e.target.value;
        const states = this.state.states;
        let countryId = 0;
        for (let country of this.state.countries) if (country.countryCode === countryCode) countryId = country.id;
        const newStates = states.filter(state => Number(state.countryId) === Number(countryId));
        this.setState({ selectedStates: newStates, selectedCities: [] }, () => {
            stateCode && this.populateCity({ target: { value: stateCode } });
        });
    }

    populateCity = e => {
        console.log('Populate city ', e.target.value);
        const stateCode = e.target.value;
        const cities = this.state.cities;
        let stateId = 0;
        for (let st of this.state.states) if (st.stateCode === stateCode) stateId = st.id;
        const newCities = cities.filter(ct => Number(ct.stateId) === Number(stateId));
        this.setState({ selectedCities: newCities });
    }



    navigateTab = () => {
        //---------------this.props.history.push('/registration/step-3');
    }

    render() {
        console.log('this.props', this.props);

        let maxDate = new Date();
        maxDate.setDate(maxDate.getDate() - 1);

        const { step1, step3Sub } = this.props;
        console.log('Step3 V2 data', step3Sub);
        if (!step1) {
            //---------------return <Redirect to="/registration/step-1" />
        }

        step1 && Object.assign(initialValues, {
            businessName: step1.businessName || ''
        });

        step3Sub && Object.assign(initialValues, step3Sub);

        const {
            selectedCities,
            selectedStates,
            //selectedCountryId 
        } = this.state;
        return (
            <div className="regSteps step3Outer">

                {/*--End:regToppanel--- */}
                <div className="regWhiteOuter">

                    <div className="regConInside">
                        <Formik
                            initialValues={initialValues}
                            validationSchema={step3Schema}
                            onSubmit={this.handleSubmit}
                        >
                            {({ values, errors, touched, setFieldValue, handleChange }) => {
                                return (
                                    <Form>

                                        <Row>
                                            <Col sm={12}>
                                                {this.state.errorMessge ? (
                                                    <div className="alert alert-danger text-center" role="alert">
                                                        <h6>{this.state.errorMessge}</h6>
                                                    </div>
                                                ) : null}
                                            </Col>
                                        </Row>

                                        <Row className="show-grid">
                                            <Col md={12}>
                                                <div className="businessDetailsTab">
                                                    <Row className="show-grid">

                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>First Name <span className="required">*</span></div>
                                                                <Field
                                                                    name="authorizerFristname"
                                                                    type="text"
                                                                    className={`input-elem ${values.authorizerFristname &&
                                                                        'input-elem-filled'} form-control`}
                                                                    autoComplete="nope"
                                                                    autoFocus
                                                                />
                                                                {errors.authorizerFristname &&
                                                                    touched.authorizerFristname ? (
                                                                        <span className="errorMsg ml-3">
                                                                            {errors.authorizerFristname}
                                                                        </span>
                                                                    ) : null}


                                                            </FormGroup>
                                                        </Col>




                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>Last Name <span className="required">*</span></div>
                                                                <Field
                                                                    name="authorizerLastname"
                                                                    type="text"
                                                                    className={`input-elem ${values.authorizerLastname &&
                                                                        'input-elem-filled'} form-control`}
                                                                    autoComplete="nope"
                                                                />
                                                                {errors.authorizerLastname &&
                                                                    touched.authorizerLastname ? (
                                                                        <span className="errorMsg ml-3">
                                                                            {errors.authorizerLastname}
                                                                        </span>
                                                                    ) : null}


                                                            </FormGroup>
                                                        </Col>

                                                    </Row>

                                                    <Row className="show-grid">

                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>Date of Birth <span className="required">*</span></div>
                                                                <DatePicker
                                                                    onChange={value => {
                                                                        setFieldValue('authorizerDOB', value);
                                                                        this.calculateDate(value, age =>
                                                                            setFieldValue('authorizerAge', age)
                                                                        );
                                                                    }}
                                                                    value={values.authorizerDOB}
                                                                    maxDate={maxDate}
                                                                    name="authorizerDOB"
                                                                    className="form-control"
                                                                />
                                                                {errors.authorizerDOB &&
                                                                    touched.authorizerDOB ? (
                                                                        <span className="errorMsg ml-3">
                                                                            {errors.authorizerDOB}
                                                                        </span>
                                                                    ) : null}


                                                            </FormGroup>
                                                        </Col>                 

                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>Street Address <span className="required">*</span></div>
                                                                <Field
                                                                    name="authorizerAddress"
                                                                    type="text"
                                                                    className={`input-elem ${values.authorizerAddress &&
                                                                        'input-elem-filled'} form-control`}
                                                                    autoComplete="nope"
                                                                />
                                                                {errors.authorizerAddress &&
                                                                    touched.authorizerAddress ? (
                                                                        <span className="errorMsg ml-3">
                                                                            {errors.authorizerAddress}
                                                                        </span>
                                                                    ) : null}


                                                            </FormGroup>
                                                        </Col>


                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>Country <span className="required">*</span></div>
                                                                <Field
                                                                    name="authorizerCountryCode"
                                                                    component="select"
                                                                    className={`input-elem ${values.authorizerCountryCode &&
                                                                        'input-elem-filled'} form-control`}
                                                                    autoComplete="nope"
                                                                    value={values.authorizerCountryCode || ''}
                                                                    onChange={e => {
                                                                        handleChange(e);
                                                                        this.populateState(e);
                                                                    }}
                                                                >
                                                                    <option value="" selected="selected">
                                                                        {this.state.countries.length ? 'Select Country' : 'Loading...'}
                                                                    </option>
                                                                    {this.state.countries.map(country => (
                                                                        <option
                                                                            key={country.id}
                                                                            value={country.countryCode}
                                                                        >
                                                                            {country.countryName}
                                                                        </option>
                                                                    ))}
                                                                </Field>
                                                                {errors.authorizerCountryCode && touched.authorizerCountryCode ? (
                                                                    <span className="errorMsg ml-3">
                                                                        {errors.authorizerCountryCode}
                                                                    </span>
                                                                ) : null}


                                                            </FormGroup>
                                                        </Col>
                                             

                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>State <span className="required">*</span></div>
                                                                <Field
                                                                    name="authorizerStateCode"
                                                                    component="select"
                                                                    className={`input-elem topShift ${values.authorizerStateCode &&
                                                                        'input-elem-filled'} form-control`}
                                                                    autoComplete="nope"
                                                                    value={values.authorizerStateCode || ''}
                                                                    onChange={e => {
                                                                        handleChange(e);
                                                                        this.populateCity(e);
                                                                    }}
                                                                    disabled={
                                                                        selectedStates.length ? false : true
                                                                    }
                                                                >
                                                                    <option value="" selected="selected">
                                                                        Select state
                                                                    </option>
                                                                    {selectedStates.map(stName => (
                                                                        <option value={stName.stateCode} key={stName.id}>
                                                                            {stName.stateName}
                                                                        </option>
                                                                    ))}
                                                                </Field>
                                                                {errors.authorizerStateCode &&
                                                                    touched.authorizerStateCode ? (
                                                                        <span className="errorMsg ml-3">
                                                                            {errors.authorizerStateCode}
                                                                        </span>
                                                                    ) : null}


                                                            </FormGroup>
                                                        </Col>





                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>City <span className="required">*</span></div>
                                                                <Field
                                                                    component="select"
                                                                    name="authorizerCity"
                                                                    placeholder="select"
                                                                    className={`input-elem topShift ${values.authorizerCity &&
                                                                        'input-elem-filled'} form-control`}
                                                                    value={values.authorizerCity || ''}
                                                                    onChange={e => {
                                                                        handleChange(e);
                                                                    }}
                                                                    disabled={selectedCities.length ? false : true}
                                                                >
                                                                    <option value="">Select City</option>

                                                                    {selectedCities.map(ct => (
                                                                        <option value={ct.id} key={ct.id}>
                                                                            {ct.cityName}
                                                                        </option>
                                                                    ))}
                                                                </Field>
                                                                {errors.authorizerCity && touched.authorizerCity ? (
                                                                    <span className="errorMsg ml-3">
                                                                        {errors.authorizerCity}
                                                                    </span>
                                                                ) : null}


                                                            </FormGroup>
                                                        </Col>




                                                   

                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>Zip Code <span className="required">*</span></div>
                                                                <Field
                                                                    name="authorizerZipcode"
                                                                    type="text"
                                                                    className={`input-elem ${values.authorizerZipcode &&
                                                                            'input-elem-filled'} form-control`}
                                                                    autoComplete="nope"
                                                                />
                                                                {errors.authorizerZipcode &&
                                                                        touched.authorizerZipcode ? (
                                                                        <span className="errorMsg ml-3">
                                                                            {errors.authorizerZipcode}
                                                                        </span>
                                                                    ) : null}


                                                            </FormGroup>
                                                        </Col>

                                                            


                                                    </Row>
                                                  
                                                </div>
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col md={12}>
                                                <p className="textReqiredRegPayment"><span className="required">*</span> These fields are required.</p>
                                            </Col>
                                        </Row>

                                        <Row className="show-grid">
                                            <Col md={12} className="text-center">
                                                <Button onClick={this.backToBusinessinfo} className="btnStylePrimary backBtn text-left">
                                                    Go Back
                                                </Button>
                                                <Button className="btnStylePrimary" type="submit">
                                                    Next
                                                </Button>
                                            </Col>
                                        </Row>
                                    </Form>
                                );
                            }}
                        </Formik>
                    </div>
                    {/*--End: regConInside--*/}

                    <Row className="show-grid">
                        <Col md={12} className="text-center mt-3">
                            <p>
                                By creating an account, you are agreeing to our{' '}
                                <Link to="#" onClick={this.termsHandleShow}>terms</Link>
                            </p>
                        </Col>
                    </Row>
                </div>
                {/*--End: regWhiteOuter--*/}


                <Modal show={this.state.showTerms} onHide={this.termsHandleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>TERMS OF USE of <br /> FintainiumLLC. offer</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Terms />
                        <Row className="show-grid text-center">
                            <Col xs={12} md={12}>
                                <Button
                                    className="but-gray  m-l-5 mt-3"
                                    onClick={this.termsHandleClose}
                                >
                                    Close
                                </Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

            </div>
        );
    }
}

Step3signerinfo.propTypes = {
    step3Sub: PropTypes.object,
    step3: PropTypes.object,
    step1: PropTypes.object,
    onSubmitAction: PropTypes.func,
    changeActiveTab: PropTypes.func,
    changeActiveSubTab: PropTypes.func,
};

const mapStateToProps = state => {
    return {
        step3: state.businessRegistration.step3,
        step1: state.businessRegistration.step1,
        step3Sub: state.businessRegistration.step3Sub,
    };
};

const mapDispatchToProps = dispatch => {

    return {
        onSubmitAction: data => dispatch(businessRegistrationProcessStep3Sub(data))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Step3signerinfo);
