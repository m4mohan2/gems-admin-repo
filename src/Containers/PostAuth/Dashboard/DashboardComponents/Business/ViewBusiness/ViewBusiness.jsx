import React,{Component} from 'react';
import {
    Row,
    Col,
    // FormGroup,
    // FormControl,
    Button,
    Image,
    Tabs,
    Tab,
    Modal
} from 'react-bootstrap';
import axios from './../../../../../../shared/eaxios';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import LoadingSpinner from './../../../../../../Components/LoadingSpinner/LoadingSpinner';
import BussinessEdit from './../BusinessEdit';
//import BusinessVendors from './../BusinessVendors/BusinessVendors';
import BusinessVendorsMaster from './../BusinessVendors/BusinessVendorsMaster';
import BusinessCustomers from './../BusinessCustomers/BusinessCustomers';
import BusinessBanks from './../BusinessBanks/BusinessBanks';
import BusinessSettings from './../BusinessSettings/BusinessSettings';
import BusinessInvoice from './../../Invoice/BusinessInvoice';
//import BusinessUsers from '../BusinessUsers/BusinessUsers';
import Pricing from '../Pricing/Pricing';
import SuccessIco from './../../../../../../assets/success-ico.png';
import { handelBusinessIDName } from './../../../../../../redux/actions/business';
import {
    businessEditProcessStep1,
    businessEditProcessStep2,
    businessEditProcessRest
} from './../../../../../../redux/actions/businessEdit';


class ViewBusiness extends Component {

    state = {
        key:'Business',
        editErrorMessge: null,
        selectedBusinessId:null,
        editBusiness: {},
        BusinessDataLoad:false,
        showConfirMmsg:false,
        businessEditLoader: false
    }
    _isMounted = false;

    isEmpty=(obj)=> {
        for (let key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }

    successMessgae = () => {
        this.setState({
            showConfirMmsg:true,
        });
        
    };

    handleConfirmReviewClose = () => {
        this.setState({
            showConfirMmsg: false,
            key: 'Business'
        },()=>{
            //this.props.history.push('/dashboard/business');
        });        
    }

    handelModPhone = (businessPhone) => {
        if (businessPhone !== '') {

            let formatedPhone = '';
            let formatedPhoneArray = [];

            for (let letter of businessPhone) {
                if (isNaN(letter) === false) {
                    formatedPhoneArray.push(letter);
                }
            }

            formatedPhoneArray = formatedPhoneArray.filter(entry => entry.trim() !== '');
            if (formatedPhoneArray.length === 11) {
                formatedPhoneArray.splice(0, 1);
            }
            formatedPhoneArray.splice(0, 0, '1 ');
            formatedPhoneArray.splice(5, 0, '-');
            formatedPhoneArray.splice(9, 0, '-');

            for (let n of formatedPhoneArray) {
                formatedPhone += n;
            }
            businessPhone = formatedPhone;
        }
        return businessPhone;
    }
    
    BusinessDatafetch = () => {
        this.setState({
            businessEditLoader:true
        });
        axios
            .get(

                `business/${this.state.selectedBusinessId}`
            )
            .then(res => {

                let step1Obj={};

                step1Obj.businessName = res.data.businessName;
                step1Obj.businessNumber = res.data.businessNumber;
                step1Obj.businessAddress1 = res.data.businessAddress1;
                step1Obj.businessCountryCode = res.data.businessCountryCode;
                step1Obj.businessStateCode = res.data.businessStateCode;
                step1Obj.businessCityId = res.data.businessCityId;
                step1Obj.businessZipcode = res.data.businessZipcode;
                step1Obj.businessPhone = this.handelModPhone(res.data.businessPhone);
                step1Obj.businessEmail = res.data.businessEmail;
                step1Obj.businessWebsite = res.data.businessWebsite;
                step1Obj.isRegistered = res.data.isRegistered;
                step1Obj.businessType = res.data.businessType;


                let step2Obj = {}; 
                step2Obj.authorizerFristname = res.data.authorizerFristname;
                step2Obj.authorizerLastname = res.data.authorizerLastname;
                step2Obj.authorizerAddress = res.data.authorizerAddress;
                step2Obj.authorizerCountryCode = res.data.authorizerCountryCode;
                step2Obj.authorizerStateCode = res.data.authorizerStateCode;
                step2Obj.authorizerCityId = res.data.authorizerCityId;
                step2Obj.authorizerZipcode = res.data.authorizerZipcode;
                step2Obj.authorizerDOB = res.data.authorizerDOB;

                step2Obj.incorporationDate = res.data.incorporationDate;
                step2Obj.incorporationCountryCode = res.data.incorporationCountryCode;
                step2Obj.incorporationStateCode = res.data.incorporationStateCode;
                step2Obj.incorporationCityId = res.data.incorporationCityId;

                let restObj = {};

                restObj.achAmtDailyMaxLimit = res.data.achAmtDailyMaxLimit;
                restObj.achPreProcessDays= res.data.achPreProcessDays;
                restObj.archive= res.data.archive;
                restObj.authorizerAge= res.data.authorizerAge;
                restObj.authorizerPIN= res.data.authorizerPIN;
                restObj.autofyAgentToken= res.data.autofyAgentToken;
                restObj.autofyEndpointId= res.data.autofyEndpointId;
                restObj.autofyUserId= res.data.autofyUserId;
                restObj.businessAddress2= res.data.businessAddress2;
                restObj.businessEmailVerified= res.data.businessEmailVerified;
                restObj.businessPhoneExt= res.data.businessPhoneExt;
                restObj.businessPhoneVerified= res.data.businessPhoneVerified;
                restObj.id= res.data.id;
                restObj.imageURL= res.data.imageURL;
                restObj.invoiceMaxAmtLimit= res.data.invoiceMaxAmtLimit;
                restObj.invoiceValueLimit= res.data.invoiceValueLimit;
                restObj.isCustomer= res.data.isCustomer;
                restObj.registrationDate= res.data.registrationDate;
                restObj.status= res.data.status;
                restObj.type= res.data.type;
                restObj.unitedMailLogo= res.data.unitedMailLogo;
                restObj.unitedMailPromoImage= res.data.unitedMailPromoImage;

                this.props.storeBusinessEditData(step1Obj, step2Obj, restObj);

                if (this._isMounted) {
                    this.setState({
                        editBusiness: res.data,
                        businessEditLoader: false
                    }, () => {
                        console.log('this.state.editBusiness', this.state.editBusiness.hasOwnProperty('id'));
                        console.log('Selected business data type', this.state.editBusiness.type);
                        // if (this.isEmpty(this.state.editBusiness)) {
                        //     console.log('1');
                        // }
                        // else {
                        //     this.setState({
                        //         BusinessDataLoad: true
                        //     });

                        // }
                    }

                    );
                }


            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    editErrorMessge: errorMsg,
                    businessEditLoader:false

                });

                setTimeout(() => {
                    this.setState({ editErrorMessge: null });
                }, 5000);

            });
    }

    componentDidMount(){
        //console.log('this.props.globalState.business.businessData.id', this.props.globalState.business.businessData);
        this._isMounted = true; 
        if (this._isMounted) {
            if (this.props.globalState.business.businessData !== null){
                this.setState({
                    selectedBusinessId: this.props.globalState.business.businessData.id
                    //selectedBusinessId: '------',
                }, () => this.BusinessDatafetch());
            }
            else{
                // this.setState({
                //     editErrorMessge : 'Go Back to the Business Menu and select again'
                // });

                // setTimeout(() => {
                //     this.setState({ editErrorMessge: null });
                //     this.props.history.push('/dashboard/business');
                // }, 5000);
                this.props.history.push('/dashboard/business');
            }
            
        }


    }

    componentWillUnmount() {
        this._isMounted = false;
        this.props.onClickAction(null);
    }

    displayError = (e) => {
        console.log('displayError', e.data.message);
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        // this.setState({
        //     errorMessge: errorMessge,
        //     errMsg: errorMessge
        // });

        // setTimeout(() => {
        //     this.setState({ errorMessge: null });
        // }, 5000);
        return errorMessge;
    }

    render(){
        
        return(
            <div>
                {this.state.businessEditLoader === true ? <LoadingSpinner /> :
                    <Tabs
                        id="controlled-tab-example"
                        activeKey={this.state.key}
                        onSelect={key => this.setState({ key })}
                        className="cusTabControl px-3 mainMenuTab "
                    >
                        <Tab eventKey="Business" title="Business">

                            {this.state.key === 'Business'
                                ?
                                (this.state.businessEditLoader ? (<LoadingSpinner />)
                                    : this.state.editBusiness.hasOwnProperty('id') === true
                                        ? <BussinessEdit
                                            {...this.props}
                                            {...this.state.editBusiness}
                                            onSuccess={this.successMessgae}
                                            BusinessDatafetch={this.BusinessDatafetch}
                                        /> : this.state.editErrorMessge ? <div className="alert alert-danger">{this.state.editErrorMessge}</div> : null) : null
                            }

                        </Tab>
                        
                        {
                            this.state.editBusiness.type == 'Fintainium Customer'
                                ?
                                <Tab eventKey="Settings" title="Settings">
                                    {this.state.key === 'Settings' ? <BusinessSettings /> : null}
                                </Tab>
                                :
                                null
                        }

                        {
                            this.state.editBusiness.type == 'Customer' || this.state.editBusiness.type == 'Fintainium Customer'
                                ?
                                <Tab eventKey="vendors" title="Vendors">

                                    {this.state.key === 'vendors' ? <BusinessVendorsMaster /> : null}
                                </Tab>
                                :
                                null
                        }

                        {
                            this.state.editBusiness.type == 'Vendor' || this.state.editBusiness.type == 'Fintainium Customer'
                                ?
                                <Tab eventKey="customers" title="Customers">
                                    {this.state.key === 'customers' ? <BusinessCustomers /> : null}
                                </Tab>
                                :
                                null

                        }

                        <Tab eventKey="banks" title="Banks">
                            {this.state.key === 'banks' ? <BusinessBanks /> : null}
                        </Tab>

                        {
                            this.state.editBusiness.type == 'Fintainium Customer'
                                ?
                                <Tab eventKey="invoice" title="Invoice">
                                    {this.state.key === 'invoice' ? <BusinessInvoice/>: null}
                                </Tab>
                                :
                                null

                        }


                        {/* {
                            this.state.editBusiness.type !== 'Vendor/Customer'
                                ?
                                <Tab eventKey="users" title="Users">
                                    {this.state.key === 'users' ? <BusinessUsers/>: null}
                                </Tab>
                                :
                                null
                        } */}

                        {
                            <Tab eventKey="pricing" title="Pricing">
                                {this.state.key === 'pricing' ? <Pricing/> : null}
                            </Tab>
                        }

                    </Tabs>
                }
                
                {/*======  confirmation popup  ===== */}
                <Modal
                    show={this.state.showConfirMmsg}
                    onHide={this.handleConfirmReviewClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center my-3">
                                <h5>Business has been successfully updated</h5>
                            </Col>
                        </Row>

                        <Row>
                            <Col md={12} className="text-center">
                                <Button
                                    onClick={this.handleConfirmReviewClose}
                                    className="but-gray"
                                >
                                    Return
                                </Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                   
                </Modal>

            </div>
            
        );
    }
}

ViewBusiness.propTypes = {
    globalState: PropTypes.object,
    onClickAction: PropTypes.func,
    storeBusinessEditData: PropTypes.func,
    history: PropTypes.object,
};

const mapStateToProps = state => {
    return {
        globalState: state
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onClickAction: data => dispatch(handelBusinessIDName(data)),

        storeBusinessEditData : (step1,step2,step3) => {   
            dispatch(businessEditProcessStep1(step1));
            dispatch(businessEditProcessStep2(step2));
            dispatch(businessEditProcessRest(step3));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewBusiness);


