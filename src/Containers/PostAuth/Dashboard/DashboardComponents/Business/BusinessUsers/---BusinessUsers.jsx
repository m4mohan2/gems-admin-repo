import React, { Component, Fragment } from 'react';
import { 
    Button, 
    Col, 
    Collapse, 
    Image, 
    Modal, 
    Row, 
    Tab, 
    Table, 
    Tabs, 
    //OverlayTrigger, 
    Tooltip 
} from 'react-bootstrap';
import Pagination from 'react-js-pagination';
import { Link } from 'react-router-dom';
import { UserBusiness } from '../../../services/userBusiness.service';
import axios from '../../../shared/eaxios';
import refreshIcon from './../../../assets/refreshIcon.png';
import SuccessIco from './../../../assets/success-ico.png';
import LoadingSpinner from './../../UI/LoadingSpinner/LoadingSpinner';
import SortingOrder from './../../../../../../Components/Utilities/SortingOrder';
import './Administrator.scss';
import EditUser from './EditUser';

class BusinessUsers extends Component {

    state = {
        userLists: [],
        editUser: [],
        activePage: 1,
        totalCount: 0,
        itemPerPage: 20,
        successMessage: null,
        showUser: false,
        showConfirMmsg: false,
        errorMessge: null,
        errMsg: null,
        showDeleteMmsg: false,
        showDeleteConfirm: false,
        searchInput: '',
        userRole: [],
        searchByName: false,
        showModal: false,
        statusMessage: '',
        loader: true,
        errorMessageLoading: '',
        searchCount: '',
        direction: '',
        propName: '',
        propOrder: true,
        showPermission: false,
        permissionLists: [],
        expandController: [],
        permissionSet: [],
        isChanged: false,
        userID: '',
        modalType: '',
        permissionError: '',
        permissionLoader: false,
        showModalError: false,
        successType: '',
        showActionModal: false,
        key: 'userDetails',
        userDetailsState: '',
        isReload: false,
        editUserEnable: false,
        disabled: false,
        userDeactiveWarning: false
    };

    handleEditUserEnable = () => {
        this.setState({
            editUserEnable: true,
            disabled: true,
            key: 'userDetails'       
        });
    }

    handleEditUserDisable = () => {
        this.setState({
            editUserEnable: false,
            disabled: false
        });
        this.onCancel(this.state.editUser);
    }

    permissionApiTypes = {};

    hideModalError = () => {
        this.setState({
            showModalError: false
        });
    }


    handleCloseUser = () => {
        this.setState({
            showUser: false
        });
    };

    hideModal = () => {
        this.setState({
            showModal: false
        });
    }

    showActionModal = (id, userDetails) => {
        console.log('--', userDetails);
        const userDetailsState = userDetails;
        let permissionSet = [];
        userDetailsState.permissionEntities.map(per => (
            permissionSet.push(per.authority)
        ));
        this.setState({
            userDetailsState,
            showActionModal: true,
            permissionSet,
            userID: id,
            editUser: userDetails
        }, () => { console.log('permissionSet', permissionSet);});
    }

    hideActionModal = (type) => {
        if (this.state.isChanged === true) {
            this.handleDeleteSuccessOpen('warning');
        }
        else {
            this.setState({
                showActionModal: false,
                key: 'userDetails',
                expandController: []
            });
            if (type === 'auto') { this.fetchExistingUserList();}
        }
        this.handleEditUserDisable();
    }


    handlerWarningDelete = (userID) => {
        console.log('user', userID);
        this.setState({
            showDeleteMmsg: true,
            deleteConfirm: userID
        });
    };

    handlerWarningDeleteClose = () => {
        this.setState({
            showDeleteMmsg: false
        });
    };

    handleDeleteSuccessOpen = (type) => {
        this.setState({
            showDeleteConfirm: true,
            modalType: type
        });
    }
    handleDeleteSuccessClose = () => {
        this.setState({
            showDeleteConfirm: false,
            isChanged: false,
            showActionModal: false
        });
    }

    successMessgae = (userDetails) => {
        const userLists = this.state.userLists;
        const updatedUserList = userLists.map(user => {
            return userDetails.id === user.id ? userDetails : user;
        });

        this.setState({
            showConfirMmsg: true,
            showCustomer: false,
            userList: updatedUserList
        });
    };

    handleConfirmReviewClose = () => {
        this.setState({
            showConfirMmsg: false
        });
    };


    handleConfirmReviewShow = (successType) => {
        this.setState({ showConfirMmsg: true, successType });
    };

    handleOpenUser = () => {
        this.setState({ showUser: true });
    };

    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber });
        this.fetchExistingUserList(pageNumber > 0 ? pageNumber - 1 : 0);
    };

    searchTextHandel = (e) => {
        this.setState({ searchInput: e.target.value });
    }
    resetSearch = () => {
        this.setState({ searchInput: '' }, () => {
            this.fetchExistingUserList(since, key);
        });
        let since = 0, key = '';

    }

    handelSearch = () => {
        let since = 0;
        const totalCount = 0;
        this.fetchExistingUserList(since, this.state.searchInput);
        this.setState({
            searchByName: true,
            searchCount: totalCount
        });
    }

    handlerSearchByEnter = (e) => {
        // ACTIVE ON PRESS ENTER KEY      
        //console.log('enterdff', e);
        if (e.key === 'Enter') {
            this.handelSearch();
        }
        else {
            return false;
        }
    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        this.setState({
            errorMessge: errorMessge,
            errMsg: e.data.error === 'LOGIN_TIMEOUT' ? errorMessge : '',
            showDeleteMmsg: false,
            errorStay: errorMessge,
            loader: false,
            errorMessageLoading: errorMessge,
            showModalError: true
        });

        setTimeout(() => {
            this.setState({
                errorMessge: null,
                showModalError: false
            });
        }, 5000);
    }

    // USER LIST
    fetchExistingUserList = (
        since = 0, 
        //searchInput = '', 
        //propOrder = true, 
        //propName = 'name'
    ) => {
        this.setState({ loader: true }, () => {
            axios
                .get(`/user/list?since=${since}&limit=${this.state.itemPerPage}&key=${this.state.searchInput}&direction=${this.state.propOrder}&prop=${this.state.propName}`)
                .then(res => {
                    console.log(res);
                    console.log('User List', res.data);
                    if (res.data) {
                        const totalCount = res.data.total;
                        const userLists = res.data.entries;
                        this.setState({
                            userLists,
                            totalCount,
                            loader: false
                        });
                    }
                })
                .catch(this.displayError);
        });
    };

    // RETRIEVE PERMISION ROLE
    userPermission = () => {
        const user = new UserBusiness();
        user.getPermissions().then(arr => {
            this.setState({ userRole: arr });
            console.log('Permission array ', this.state.userRole);
        });
    }

    // PERMISSION LIST
    permissionList = () => {
        axios
            .get('/permission/lists')
            .then(res => {
                console.log('permission list', res);
                const permissionLists = res.data;

                permissionLists.map(permissionList => {
                    this.permissionApiTypes[permissionList.apiName] = [];
                });

                this.setState({
                    permissionLists
                });
            })
            .catch(this.displayError);
    }

    handelExpand = (key) => {
        let temp = [];
        let idIndex = this.state.expandController.indexOf(key);
        if (idIndex > -1) {
            temp.splice(idIndex, 1);
        } else {
            temp.push(key);
        }
        this.setState({
            expandController: temp
        });
    }

    handelPermissionSwitch = (permission) => {
        let temp = this.state.permissionSet;
        let idIndex = temp.indexOf(permission);

        if (idIndex > -1) {
            temp.splice(idIndex, 1);
        } else {
            temp.push(permission);
        }
        this.setState({ isChanged: true, permissionSet: temp });
    }

    handelPermissionSubmit = () => {
        this.handleDeleteSuccessClose('wait');
        this.setState({ modalType: '' });
        const permissionSet = this.state.permissionSet;

        // Reset Existing API Types Values
        for (const key in this.permissionApiTypes) {
            if (this.permissionApiTypes.hasOwnProperty(key)) {
                this.permissionApiTypes[key] = [];
            }
        }

        for (const key in permissionSet) {
            if (permissionSet.hasOwnProperty(key)) {
                const element = permissionSet[key].split('_');
                if (this.permissionApiTypes.hasOwnProperty(element[0])) {
                    const pValue = element.length === 3 ? element[1] + '_' + element[2] : element[1];
                    if (this.permissionApiTypes[element[0]].indexOf(pValue) === -1)
                        this.permissionApiTypes[element[0]].push(pValue);
                }
            }
        }

        const payload = {
            permissions: this.permissionApiTypes,
            userId: this.state.userID
        };

        this.setState({
            permissionLoader: true
        }, () => {
            axios
                .put('/role/setPermission', payload)
                .then(res => {
                    console.log(res);
                    this.setState({
                        permissionLoader: false,
                    });
                    this.handleConfirmReviewShow('per');
                    this.hideActionModal('auto');
                })
                .catch(err => {
                    let errorMessge = '';
                    try {
                        errorMessge = err.data.message ? err.data.message : err.data.error;
                    } catch (err) {
                        errorMessge = 'Unknown Error';
                    }                    
                    this.setState({
                        permissionError: errorMessge,
                        permissionLoader: false
                    });
                    setTimeout(() => {
                        this.setState({
                            permissionError: ''
                        });
                    }, 2500);
                });
        });
    }


    // DELETE USER
    deleteUser = userID => {
        console.log(userID);
        axios
            .delete(`/user/${userID}`)
            .then(response => {
                console.log('response', response);
                this.fetchExistingUserList();
                this.setState({
                    showStatus: true,
                    successMessage: 'User has been deleted successfully',
                    showDeleteMmsg: false,
                    showDeleteConfirm: true,
                });
                setTimeout(() => {
                    this.setState({ successMessage: null });
                }, 2500);
            })
            .catch(this.displayError);
    };

    // EDIT USER
    editUser = userID => {
        console.log('edit user===========', userID);
        axios
            .get(`/user/individual/${userID}`)
            .then(user => {
                console.log('existing user', user.data);
                const details = user.data;
                this.setState({
                    showUser: true,
                    editUser: details
                });
            })
            .catch(this.displayError);
    };

    // DEACTIVE WARNING MODAL
    dectiveWarningHandler = () => {
        this.setState({
            showModalError: true,
            userDeactiveWarning: true
        });
    }

    // ACTIVE INACTIVE ACCOUNT
    accountBlockHandler = (userID, newStatus) => {
        console.log('status now ---', newStatus);
        let newValue = {
            'accountLocked': !newStatus
        };
        axios
            .put(`/user/activeInactiveAccount/${userID}`, newValue)
            .then(res => {
                console.log('Active status change to --', newValue, res);
                this.fetchExistingUserList();
                this.hideActionModal('auto');
                this.setState({
                    showModal: true,
                    statusMessage: newStatus,
                    showModalError: false,
                    userDeactiveWarning: false
                });
            })
            .catch(this.displayError);
    }

    // SORTING TRUE FOR ASCENDING AND FALSE FOR DECENDING
    sortingHandler = (prop, bool) => {
        let since = 0;
        //console.log('sort==',prop,bool);
        this.setState({
            propName: prop,
            propOrder: bool
        }, () => {
            this.fetchExistingUserList(since, this.state.searchInput, prop, bool);
        });
    }

    handelError = (err) => {
        let errorMessage = '';
        try {
            errorMessage = err.data.message ? err.data.message : err.data.error_description;
        } catch (err) {
            errorMessage = 'No records found.';
        }
        return errorMessage;
    }

    // CANCEL AND RELOAD
    onCancel = (userDetails) => {
        this.setState({
            isReload: true,
        }, () => this.setState({ editUser: userDetails, isReload: false }));
    }



    componentDidMount() {
        this.fetchExistingUserList();
        this.userPermission();
        this.permissionList();
    }


    render() {
        const userProps = {
            userData: this.state.editUser,
            role: this.state.userRole,
            isEditable: this.state.editUserEnable,
            disabled: this.state.disabled
        };

        const {
            propName,
            propOrder,
            //invoiceShow,
            //bankShow,
            permissionLists,
            expandController,
            permissionSet,
            userRole
        } = this.state;

        return (
            <div className="adminAddUser">
                <Row className="show-grid m-b-20">
                    <Col xs={12} md={12}>
                        <Row className="show-grid">
                            <Col xs={12} md={6}>
                                <div className="topSearchInvoice">
                                    <div className="form-inline">
                                        {/* <h4>Users</h4> */}
                                        <input
                                            type="text"
                                            className="form-control searchMain"
                                            placeholder="Search.."
                                            onChange={this.searchTextHandel}
                                            value={this.state.searchInput || ''}
                                            onKeyPress={this.handlerSearchByEnter}
                                        />

                                        <div className="form-group m-l-20">
                                            <button
                                                type="button"
                                                className="btn search-btn"
                                                onClick={() => this.handelSearch()}
                                            >
                                                Search
                                            </button>
                                        </div>
                                        <div className="form-group m-l-10">
                                            <Link to="#">
                                                <Image src={refreshIcon} onClick={() => this.resetSearch()} />
                                            </Link>
                                        </div>
                                        {/* <div className="form-group m-l-20">
                                            <button
                                                type="button"
                                                className="btn blue-btn advSearchBtn"
                                            >
                                                Advanced Search
                                            </button>
                                        </div> */}
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                </Row>


                {/* {this.state.successMessage ? (
                    <Row>
                        <Col xs={12}>
                            <div className="alert alert-success" role="alert">
                                {this.state.successMessage}
                            </div>
                        </Col>
                    </Row>
                ) : this.state.errorMessge ? (
                    <div className="alert alert-danger" role="alert">
                        {this.state.errorMessge}
                    </div>
                ) : null} */}
                {this.state.permissionError !== '' ?
                    <Row>
                        <Col md={12} className="alert alert-danger">
                            {this.state.permissionError}
                        </Col>
                    </Row>
                    : null
                }
                <div className="boxBg p-b-15 hideCheckBox userListTable">
                    <Table responsive hover>
                        <thead className="theaderBg">
                            <tr>
                                <th className="hide">
                                    <label className="customCkBox">
                                        <input type="checkbox" />
                                        <span className="checkmark" />
                                    </label>
                                </th>
                                <th>Name
                                    <div className="sort">
                                        <SortingOrder
                                            orderBy={true}
                                            propertyName={'name'}
                                            propName={propName}
                                            propOrder={propOrder}
                                            directionIcon={'top'}
                                            onClickSort={this.sortingHandler}
                                        />
                                        <SortingOrder
                                            orderBy={false}
                                            propertyName={'name'}
                                            propName={propName}
                                            propOrder={propOrder}
                                            directionIcon={'bottom'}
                                            onClickSort={this.sortingHandler}
                                        />
                                    </div>
                                </th>
                                <th>Email Id
                                    <div className="sort">
                                        <SortingOrder
                                            orderBy={true}
                                            propertyName={'email'}
                                            propName={propName}
                                            propOrder={propOrder}
                                            directionIcon={'top'}
                                            onClickSort={this.sortingHandler}
                                        />
                                        <SortingOrder
                                            orderBy={false}
                                            propertyName={'email'}
                                            propName={propName}
                                            propOrder={propOrder}
                                            directionIcon={'bottom'}
                                            onClickSort={this.sortingHandler}
                                        />
                                    </div>
                                </th>
                                <th>UserType
                                    <div className="sort">
                                        <SortingOrder
                                            orderBy={true}
                                            propertyName={'userType'}
                                            propName={propName}
                                            propOrder={propOrder}
                                            directionIcon={'top'}
                                            onClickSort={this.sortingHandler}
                                        />
                                        <SortingOrder
                                            orderBy={false}
                                            propertyName={'userType'}
                                            propName={propName}
                                            propOrder={propOrder}
                                            directionIcon={'bottom'}
                                            onClickSort={this.sortingHandler}
                                        />
                                    </div>
                                </th>
                                <th className="text-center">Date Added
                                    <div className="sort">
                                        <SortingOrder
                                            orderBy={true}
                                            propertyName={'dateAdded'}
                                            propName={propName}
                                            propOrder={propOrder}
                                            directionIcon={'top'}
                                            onClickSort={this.sortingHandler}
                                        />
                                        <SortingOrder
                                            orderBy={false}
                                            propertyName={'dateAdded'}
                                            propName={propName}
                                            propOrder={propOrder}
                                            directionIcon={'bottom'}
                                            onClickSort={this.sortingHandler}
                                        />
                                    </div>
                                </th>
                                <th width="3%">&nbsp;</th>
                                <th>
                                    Status
                                    <div className="sort">
                                        <SortingOrder
                                            orderBy={true}
                                            propertyName={'status'}
                                            propName={propName}
                                            propOrder={propOrder}
                                            directionIcon={'top'}
                                            onClickSort={this.sortingHandler}
                                        />
                                        <SortingOrder
                                            orderBy={false}
                                            propertyName={'status'}
                                            propName={propName}
                                            propOrder={propOrder}
                                            directionIcon={'bottom'}
                                            onClickSort={this.sortingHandler}
                                        />
                                    </div>
                                </th>

                            </tr>
                        </thead>

                        <tbody>
                            {
                                this.state.errorMessge ? (<tr>
                                    <td colSpan={6} className="text-center">
                                        {this.state.errorMessge}
                                    </td>
                                </tr>)
                                    :
                                    this.state.loader ? (<tr>
                                        <td colSpan={6}>
                                            <LoadingSpinner />
                                        </td>
                                    </tr>) :
                                        this.state.userLists.length > 0 ? (
                                            this.state.userLists.map(userList => (

                                                <tr
                                                    key={userList.id}
                                                    className={userList.accountLocked === false ? null : 'inactive'}
                                                    onClick={() => this.showActionModal(userList.id, userList)}
                                                >
                                                    <td className="hide">
                                                        <label className="customCkBox">
                                                            <input type="checkbox" />
                                                            <span className="checkmark" />
                                                        </label>
                                                    </td>
                                                    <td>{userList.firstName} {userList.lastName} {sessionStorage.getItem('userName') !== userList.emailAddress ? null : (<strong>(Self)</strong>)} </td>
                                                    <td>{userList.emailAddress}</td>
                                                    <td>{userList.roleEntities.length > 0 ? userList.roleEntities[0].roleName : null}</td>
                                                    <td className="text-center">{userList.createdDate}</td>
                                                    <td>&nbsp;</td>
                                                    <td>{userList.accountLocked !== true ? 'Active' : 'Inactive'}</td>
                                                </tr>
                                            ))
                                        ) :

                                            (
                                                <tr><td colSpan={6} style={{ textAlign: 'center' }}>
                                                    {this.state.searchCount === 0 ? (<p>Please search by valid email id</p>) : 'No records found.'}
                                                </td></tr>
                                            )

                            }
                        </tbody>
                    </Table>
                </div>

                {this.state.totalCount ? (
                    <Row>
                        <Col md={12}>
                            <div className="paginationOuter text-right">
                                <Pagination
                                    activePage={this.state.activePage}
                                    itemsCountPerPage={this.state.itemPerPage}
                                    totalItemsCount={this.state.totalCount}
                                    onChange={this.handlePageChange}
                                />
                            </div>
                        </Col>
                    </Row>
                ) : null}

                {/*==================EDIT USER DETAILS======================*/}
                <Modal
                    show={this.state.showUser}
                    onHide={this.handleCloseUser}
                    className="enterPayInfoPop"
                >
                    <Modal.Header>
                        <Modal.Title>Edit User</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <EditUser
                            {...this.state.editUser}
                            onSuccess={this.successMessgae}
                            closeUser={this.handleCloseUser}
                        />
                    </Modal.Body>
                </Modal>

                {/*====payment confirm review popup===== */}
                <Modal
                    show={this.state.showConfirMmsg}
                    onHide={this.handleConfirmReviewClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                {
                                    this.state.successType === 'per' ?
                                        <h5>Permissions have been successfully updated</h5>
                                        :
                                        <h5>User has been successfully updated</h5>
                                }
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handleConfirmReviewClose}
                            className="but-gray"
                        >
                            Done
                        </Button>
                    </Modal.Footer>
                </Modal>

                {/*=======DELETE APPROVE POPUP======== */}
                <Modal
                    show={this.state.showDeleteMmsg}
                    onHide={this.handlerWarningDeleteClose}
                    className="payOptionPop"
                >
                    <Modal.Body>

                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Warning!! User will be permanently deleted and cannot be recovered. Are you sure ? </h5>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handlerWarningDeleteClose}
                            className="but-gray"
                        >
                            Return
                        </Button>
                        <Button
                            onClick={() => this.deleteUser(this.state.deleteConfirm)}
                            className="btn-danger"
                        >
                            Proceed
                        </Button>
                    </Modal.Footer>
                </Modal>

                {/*====DELETE SUCCESS POPUP===== */}
                <Modal
                    show={this.state.showDeleteConfirm}
                    onHide={this.handleDeleteSuccessClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        {this.state.modalType === 'warning' ?
                            <Fragment>
                                <Row>
                                    <Col md={12} className="text-center">
                                        <h5>You have made some changes, do you want to save?</h5>
                                    </Col>
                                </Row>
                            </Fragment>
                            :
                            <Fragment>
                                <Row>
                                    <Col md={12} className="text-center">
                                        <Image src={SuccessIco} />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={12} className="text-center">
                                        <h5>User has been successfully deleted</h5>
                                    </Col>
                                </Row>
                            </Fragment>
                        }
                    </Modal.Body>
                    <Modal.Footer>
                        {this.state.modalType === 'warning' ?
                            <Fragment>
                                <Button
                                    onClick={this.handleDeleteSuccessClose}
                                    className="but-gray"
                                >
                                    Close
                                </Button>
                                <Button
                                    onClick={this.handelPermissionSubmit}
                                    className="blue-btn"
                                >
                                    Save
                                </Button>
                            </Fragment>
                            :
                            <Button
                                onClick={this.handleDeleteSuccessClose}
                                className="but-gray"
                            >
                                Done
                            </Button>
                        }
                    </Modal.Footer>
                </Modal>


                <Modal
                    show={this.state.showModal}
                    onHide={this.hideModal}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>User has been successfully {this.state.statusMessage !== true ? 'deactivated' : 'activated'}. </h5>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.hideModal}
                            className="but-gray"
                        >
                            Done
                        </Button>
                    </Modal.Footer>
                </Modal>

                {/* SET PERMISSION */}
                <Modal
                    show={this.state.showPermission}
                    onHide={() => this.hidePermissionHandler('manual')}
                    className="right half noPadding"
                >
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body>
                        <div className="invoicMasterWrap">
                            <div className="invoiceMasterHeader">
                                <h1>User Permissions</h1>
                            </div>
                            {this.state.permissionLoader ?
                                <LoadingSpinner /> : null
                            }

                            <div className="invoiceMasterBody content-body">
                                {
                                    permissionLists.map(permissionList =>
                                        (<div className="permissionBlock" key={permissionList.apiName}>
                                            <Row>
                                                <Col md={12}>
                                                    <h4
                                                        className={expandController.indexOf(permissionList.displayName) > -1 ? 'hideDetails' : null}
                                                        onClick={() => this.handelExpand(permissionList.displayName)}
                                                        aria-controls="bankingPermission"
                                                        aria-expanded={expandController.indexOf(permissionList.displayName) > -1 ? true : false}>{permissionList.displayName}
                                                    </h4>
                                                    <Collapse in={expandController.indexOf(permissionList.displayName) > -1 ? true : false}>
                                                        <div id="bankingPermission" className="permissionControl">
                                                            <ul>
                                                                {permissionList.permissionTypes.map(permissionType => (
                                                                    <li key={permissionType.permission}>
                                                                        <label className="switch">
                                                                            <input
                                                                                type="checkbox"
                                                                                name={permissionType.displayName}
                                                                                checked={permissionSet.indexOf(permissionType.permission) > -1 ? true : false}
                                                                                onClick={() => this.handelPermissionSwitch(permissionType.permission)}
                                                                            />
                                                                            <span className="slider round"></span>
                                                                        </label>
                                                                        <span className="label">{permissionType.displayName}</span>
                                                                    </li>
                                                                ))}
                                                            </ul>
                                                        </div>
                                                    </Collapse>
                                                </Col>
                                            </Row>
                                        </div>
                                        )
                                    )
                                }
                            </div>
                        </div>

                    </Modal.Body>

                    <Modal.Footer>
                        <Button
                            onClick={() => this.hidePermissionHandler('manual')}
                            className="but-gray"
                        >
                            Return
                        </Button>
                        {this.state.isChanged ?
                            <Button
                                onClick={this.handelPermissionSubmit}
                                className="blue-btn"
                            >
                                Save
                            </Button>
                            : null}
                    </Modal.Footer>
                </Modal>

                {/*==== SLIDE MODAL FOR ALL FUNCTIONAL ===== */}
                <Modal
                    show={this.state.showActionModal}
                    onHide={this.hideActionModal}
                    className="right half noPadding slideModal userModal"
                >
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body>
                        <div className="modalHeader">
                            <Row>
                                <Col xs={7} md={6}>
                                    <h1>{this.state.userDetailsState.firstName} {this.state.userDetailsState.lastName}</h1>
                                    {this.state.userDetailsState.jobTitle && <p> User Role: {this.state.userDetailsState.jobTitle}</p>}
                                    {this.state.userDetailsState.emailAddress && <p> Email: {this.state.userDetailsState.emailAddress}</p>}
                                </Col>
                                <Col xs={5} md={6} className="text-right">

                                    {sessionStorage.getItem('userName') !== this.state.userDetailsState.emailAddress ? (<Fragment>
                                        {this.state.userRole.indexOf('USER_UPDATE') !== -1 ? (
                                            <Button className="blue-btn" onClick={this.handleEditUserEnable} >
                                                Edit
                                            </Button>
                                        ) : null}

                                        {this.state.userRole.indexOf('USER_DELETE') !== -1 ? (
                                            <Button className="but-gray"
                                                onClick={() => this.handlerWarningDelete(this.state.userDetailsState.id)}
                                            >
                                                Delete
                                            </Button>) : null}

                                        {this.state.userRole.indexOf('USER_UPDATE') !== -1 ? (
                                            <Fragment>
                                                {this.state.userDetailsState.accountLocked === false ? (
                                                    <Button
                                                        className="blue-btn"
                                                        onClick={this.dectiveWarningHandler}>
                                                        Deactivate
                                                    </Button>
                                                ) : (
                                                    <Button className="blue-btn"
                                                        onClick={() => this.accountBlockHandler(this.state.userDetailsState.id, this.state.userDetailsState.accountLocked)}
                                                    >
                                                        {this.state.userDetailsState.accountLocked !== true ? 'Deactivate ' : 'Activate'}
                                                    </Button>
                                                )}
                                            </Fragment>
                                        ) : null}

                                    </Fragment>) : null}

                                </Col>
                            </Row>

                        </div>
                        <div className="modalBody modalTabStyle">

                            <Tabs
                                id="userTab"
                                activeKey={this.state.key}
                                onSelect={key => this.setState({ key })}
                            >
                                <Tab eventKey="userDetails" title="User Details">
                                    <div className="tabContent">
                                        {this.state.isReload === true ? (<LoadingSpinner />) : (
                                            <Fragment>
                                                <EditUser
                                                    {...userProps}
                                                    onSuccess={this.successMessgae}
                                                    closeUser={this.handleCloseUser}
                                                    onDisable={this.handleEditUserDisable}
                                                />
                                            </Fragment>)
                                        }
                                    </div>
                                </Tab>

                                <Tab eventKey="userPermissions"
                                    title={
                                        <Fragment>
                                            {sessionStorage.getItem('userName') !== this.state.userDetailsState.emailAddress && userRole.indexOf('USER_AUTH_PERMISSIONS') > -1 ? null : (
                                                <div style={{ position: 'relative' }}>
                                                    <Tooltip placement="right" className="in" id="tooltip-right">
                                                        You must have permission to update user permissions
                                                    </Tooltip>
                                                </div>
                                            )}

                                            User Permissions
                                        </Fragment>
                                    }
                                    disabled={sessionStorage.getItem('userName') !== this.state.userDetailsState.emailAddress && userRole.indexOf('USER_AUTH_PERMISSIONS') > -1 ? false : true}
                                >
                                    <div className="tabContent">
                                        <div className="invoicMasterWrap">
                                            {this.state.permissionLoader ?
                                                <LoadingSpinner /> : null
                                            }
                                            <Fragment>
                                                {
                                                    permissionLists.map(permissionList =>
                                                        (<div className="permissionBlock" key={permissionList.apiName}>
                                                            <Row>
                                                                <Col md={12}>
                                                                    <h4
                                                                        className={expandController.indexOf(permissionList.displayName) > -1 ? 'hideDetails' : null}
                                                                        onClick={() => this.handelExpand(permissionList.displayName)}
                                                                        aria-controls="bankingPermission"
                                                                        aria-expanded={expandController.indexOf(permissionList.displayName) > -1 ? true : false}>{permissionList.displayName}
                                                                    </h4>
                                                                    <Collapse in={expandController.indexOf(permissionList.displayName) > -1 ? true : false}>
                                                                        <div id="bankingPermission" className="permissionControl">
                                                                            <ul>
                                                                                {permissionList.permissionTypes.map(permissionType => (
                                                                                    <li key={permissionType.permission}>
                                                                                        <label className="switch">
                                                                                            <input
                                                                                                type="checkbox"
                                                                                                name={permissionType.displayName}
                                                                                                checked={permissionSet.indexOf(permissionType.permission) > -1 ? true : false}
                                                                                                onClick={() => this.handelPermissionSwitch(permissionType.permission)}
                                                                                            />
                                                                                            <span className="slider round"></span>
                                                                                        </label>
                                                                                        <span className="label">{permissionType.displayName}</span>
                                                                                    </li>
                                                                                ))}
                                                                            </ul>
                                                                        </div>
                                                                    </Collapse>
                                                                </Col>
                                                            </Row>
                                                        </div>
                                                        )
                                                    )
                                                }
                                            </Fragment>

                                        </div>
                                    </div>
                                    <div className="text-center">
                                        {this.state.isChanged ?
                                            <Button
                                                onClick={this.handelPermissionSubmit}
                                                className="blue-btn"
                                            >
                                                Save
                                            </Button>
                                            : null}
                                    </div>
                                </Tab>
                            </Tabs>

                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        {/* <Button
                            onClick={() => this.hideActionModal("manual")}
                            className="but-gray"
                        >
                            Return
                        </Button> 
                        {this.state.isChanged ?
                            <Button
                                onClick={this.handelPermissionSubmit}
                                className="blue-btn"
                            >
                                Save
                            </Button>
                            : null}*/}
                    </Modal.Footer>
                </Modal>


                {/*==== DISPLAY ERROR MESSAGE ===== */}
                <Modal
                    show={this.state.showModalError}
                    onHide={this.hideModalError}
                    className="payOptionPop"
                >
                    <Modal.Body>

                        <Row>
                            <Col md={12} className="text-center">
                                <h5>
                                    {this.state.errorMessge ? (
                                        <h5> {this.state.errorMessge}</h5>
                                    ) : null}

                                    {
                                        this.state.userDeactiveWarning === true ?
                                            'De-activated users should be kept for 1 year. After 1 year of inactivity, they can be deleted from a customer\'s platform'
                                            :
                                            null
                                    }

                                </h5>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.hideModalError}
                            className="but-gray"
                        >
                            Return
                        </Button>

                        {
                            this.state.userDeactiveWarning === true ?
                                (<Button className="blue-btn"
                                    onClick={() => this.accountBlockHandler(this.state.userDetailsState.id, this.state.userDetailsState.accountLocked)}
                                >
                                    {this.state.userDetailsState.accountLocked !== true ? 'Deactivate ' : 'Activate'}
                                </Button>)
                                :
                                null
                        }

                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default BusinessUsers;
