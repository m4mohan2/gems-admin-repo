import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from '../../../../../../shared/eaxios';
import PropTypes from 'prop-types';
import LoadingSpinner from '../../../../../../Components/LoadingSpinner/LoadingSpinner';
import {
    Table,
    //Image,
    Row,
    Col,
} from 'react-bootstrap';
import Pagination from 'react-js-pagination';
import { FaCaretDown } from 'react-icons/fa';
import { FaCaretUp } from 'react-icons/fa';

class BusinessUsers extends Component {

    state = {
        userLists: [],
        activePage: 1,
        itemPerPage: 20,
        totalCount: 0,
        searchInput:'',
        direction: '',
        field: '',
        propOrder: true,
        loader:false,
        errorMessge:null,
        sortingActiveID:1
    }
    
    _isMounted = false;

    displayError = (e) => {
        console.log('displayError', e.data.message);
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    sortingActive = (id) => {
        this.setState({
            sortingActiveID: id
        }, () => { console.log('this.state.sortingActiveID', this.state.sortingActiveID); });
    }

    handleGetUserList = (
        since = 0,
        sort = '', // sort with a = true, d =flase --4
        field = '', // sort with field name --5

    ) => {
        this.setState({ 
            loader: true,
            propOrder: sort,
            field: field
        }, () => {
            axios
                .get(`/user/list/${this.props.globalState.business.businessData.id}?since=${since}&limit=${this.state.itemPerPage}&key=${this.state.searchInput}&direction=${this.state.propOrder}&prop=${this.state.field}`)
                .then(res => {
                    console.log(res);
                    console.log('User List', res.data);
                    if (res.data) {
                        const totalCount = res.data.total;
                        const userLists = res.data.entries;
                        if (this._isMounted){
                            this.setState({
                                userLists,
                                totalCount,
                                loader: false
                            }, () => { console.log('userLists', this.state.userLists);});
                        }
                        
                    }
                })
                .catch(e => {
                    let errorMsg = this.displayError(e);
                    this.setState({
                        errorMessge: errorMsg,
                        loader: false
                    });

                });
                
        });
    };

    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber });
        this.handleGetUserList(pageNumber > 0 ? pageNumber - 1 : 0);
    };

    handelResetPagination = () => {
        this.setState({ activePage: 1 });
    }

    componentDidMount(){
        this._isMounted = true;
        this.handleGetUserList();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    render(){
        return(
            <div className='m-3'>

                {/* <div className="row my-3">
                    <div className="col-sm-12">
                        <input
                            type="text"
                            className="w-210 form-control float-left"
                            placeholder="Type to search"
                            onChange={this.handelOnchange}
                            onKeyPress={this.handlekeyPress}
                            value={this.state.businessSearchKey}
                        />
                        <button
                            type="button"
                            className="btn ml-3 search-btn text-black float-left cus-mt-3"
                            onClick={() => this.handelSearch()}
                        >
                            Search
                        </button>
                    </div>
                </div> */}

                <div className="boxBg">
                    <Table responsive hover>
                        <thead className="theaderBg">
                            <tr>
                                <th>
                                    <span className="float-left pr-2">Name </span>
                                    <span className="d-flex flex-column-reverse sortingFontSize">
                                        <button className="custom-btn-focus">
                                            <FaCaretDown
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 2 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.handleGetUserList(0, false, 'firstName');
                                                    this.setState({ activePage: 1 });
                                                    this.sortingActive(2);
                                                }} />
                                        </button>
                                        <button className="custom-btn-focus">
                                            <FaCaretUp
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 1 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.handleGetUserList(0, true, 'firstName');
                                                    this.setState({ activePage: 1 });
                                                    this.sortingActive(1);
                                                }}
                                            
                                            />
                                        </button>
                                    </span>
                                </th>
                                <th>
                                    <span className="float-left pr-2">Email ID </span>
                                    <span className="d-flex flex-column-reverse sortingFontSize">
                                        <button className="custom-btn-focus">
                                            <FaCaretDown
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 4 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.handleGetUserList(0, false, 'userName');
                                                    this.setState({ activePage: 1 });
                                                    this.sortingActive(4);
                                                }} />
                                        </button>
                                        <button className="custom-btn-focus">
                                            <FaCaretUp
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 3 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.handleGetUserList(0, true, 'userName');
                                                    this.setState({ activePage: 1 });
                                                    this.sortingActive(3);
                                                }}

                                            />
                                        </button>
                                    </span>
                                </th>
                                <th>
                                    <span className="float-left pr-2">User Type </span>
                                    <span className="d-flex flex-column-reverse sortingFontSize">
                                        <button className="custom-btn-focus">
                                            <FaCaretDown
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 6 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.handleGetUserList(0, false, 'userType');
                                                    this.setState({ activePage: 1 });
                                                    this.sortingActive(6);
                                                }} />
                                        </button>
                                        <button className="custom-btn-focus">
                                            <FaCaretUp
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 5 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.handleGetUserList(0, true, 'userType');
                                                    this.setState({ activePage: 1 });
                                                    this.sortingActive(5);
                                                }}

                                            />
                                        </button>
                                    </span>
                                </th>
                                <th>
                                    <span className="float-left pr-2">Date Added </span>
                                    <span className="d-flex flex-column-reverse sortingFontSize">
                                        <button className="custom-btn-focus">
                                            <FaCaretDown
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 8 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.handleGetUserList(0, false, 'createdDate');
                                                    this.setState({ activePage: 1 });
                                                    this.sortingActive(8);
                                                }} />
                                        </button>
                                        <button className="custom-btn-focus">
                                            <FaCaretUp
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 7 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.handleGetUserList(0, true, 'createdDate');
                                                    this.setState({ activePage: 1 });
                                                    this.sortingActive(7);
                                                }}

                                            />
                                        </button>
                                    </span>
                                </th>
                                <th>
                                    <span className="float-left pr-2">Status </span>
                                    <span className="d-flex flex-column-reverse sortingFontSize">
                                        <button className="custom-btn-focus">
                                            <FaCaretDown
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 10 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.handleGetUserList(0, false, 'accountLocked');
                                                    this.setState({ activePage: 1 });
                                                    this.sortingActive(10);
                                                }} />
                                        </button>
                                        <button className="custom-btn-focus">
                                            <FaCaretUp
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 9 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.handleGetUserList(0, true, 'accountLocked');
                                                    this.setState({ activePage: 1 });
                                                    this.sortingActive(9);
                                                }}

                                            />
                                        </button>
                                    </span>
                                </th>
                                
                            </tr>
                        </thead>
                        <tbody>

                            {this.state.loader ? (<tr>
                                <td colSpan={12}>
                                    <LoadingSpinner />
                                </td>
                            </tr>) :
                                this.state.userLists.length > 0 ? (
                                    this.state.userLists.map(userList => (
                                        <tr key={userList.id} >
                                            <td>{userList.firstName} {userList.lastName}</td>
                                            <td>{userList.userName}</td>
                                            <td>{userList.roleEntities[0].roleName}</td>
                                            <td>{userList.createdDate}</td>
                                            <td>{userList.accountLocked ? 'Inactive': 'Active'}</td>
                                        </tr>
                                    ))
                                )
                                    :
                                    this.state.errorMessge ? <tr>
                                        <td colSpan={6}>
                                            <p className="text-center">{this.state.errorMessge}</p>
                                        </td>
                                    </tr> : (
                                        <tr>
                                            <td colSpan={6}>
                                                <p className="text-center">No records found</p>
                                            </td>
                                        </tr>
                                    )
                            }

                        </tbody>
                    </Table>

                </div>
                {this.state.totalCount ? (
                    <Row>
                        <Col md={12}>
                            <div className="paginationOuter text-right">
                                <Pagination
                                    activePage={this.state.activePage}
                                    itemsCountPerPage={this.state.itemPerPage}
                                    totalItemsCount={this.state.totalCount}
                                    onChange={this.handlePageChange}
                                />
                            </div>
                        </Col>
                    </Row>
                ) : null}

            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        globalState: state
    };
};

BusinessUsers.propTypes = {
    globalState: PropTypes.object,
};

export default connect(mapStateToProps, null)(BusinessUsers);
