/* eslint-disable no-useless-escape */
import React, {
    PureComponent,
    Fragment
} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    Row,
    Col,
    FormGroup,
    FormControl,
    Tabs,
    Tab,
    Image,
    Button,
    Modal,
   
} from 'react-bootstrap';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';
import InputMask from 'react-input-mask';
import axios from './../../../../../shared/eaxios';
import { CountryService } from '../../../../../services/country.service';
// import DatePicker from 'react-date-picker';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import Dropzone from 'react-dropzone';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import UploadIcon from './../../../../../assets/uploading.png';
import { Link } from 'react-router-dom';
import SuccessIco from './../../../../../assets/success-ico.png';
//import { GoCalendar } from 'react-icons/go';
import { businessEditProcessStep1, businessEditProcessStep2 } from '../../../../../redux/actions/businessEdit';

const editBusinessSchema1 = Yup.object().shape({


    businessName: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter Business Name'),
    businessNumber: Yup
        .string()
        //.min(10, 'Business number minimum be 9 digit').max(11, 'Business number maximum be 10 digit')
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter business tax ID/SSN'),
    businessEmail: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter business email ID')
        .email('Email must be valid'),
    businessWebsite: Yup.string()
        .required('Please enter business website')
        .trim('Please remove whitespace'),
    businessPhone: Yup.string().min(14, 'Phone number must be 10 digit').required('Please enter business phone'),
    businessAddress1: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please add an address'),
    businessZipcode: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .test(
            'businessZipcode',
            'Please enter a valid zip code',
            function (value) {
                if (value) {
                    let isValid = false;
                    if (/^\d{5}([\-|\s]\d{1,4})?$/.test(value)) {
                        isValid = true;
                    } else if (/^[a-zA-Z0-9]{3}\s[a-zA-Z0-9]{3}$/.test(value)) {
                        let expArr = value.split(' ');

                        if (/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{3}$/.test(expArr[0]) && /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{3}$/.test(expArr[1])) {
                            isValid = true;
                        }
                    }
                    return isValid;
                }

            }
        )
        .required('Please enter zip code'),
    businessCountryCode: Yup.string().required('Please select a country'),
    businessStateCode: Yup.string()
        .trim('Please remove whitespace').required('Please select a state'),
    city: Yup.string()
        .trim('Please remove whitespace'),
    businessCityId: Yup
        .number().moreThan(1, 'Please select a city')
        .required('Please select a city')
    //.trim('Please remove whitespace'),


});
const editBusinessSchema = Yup.object().shape({
    authorizerFristname: Yup
        .string()
        .trim('Please remove whitespace')
        .required('Please enter authorizer frist name')
        .strict(),
    authorizerLastname: Yup
        .string()
        .trim('Please remove whitespace')
        .required('Please enter authorizer last name')
        .strict(),
    authorizerAddress: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please add an address'),
    authorizerCountryCode: Yup.string()
        .required('Please select a country'),
    authorizerStateCode: Yup.string()
        .trim('Please remove whitespace')
        .required('Please select a state'),
    authorizerCityId: Yup.number().moreThan(1, 'Please select a city')
        .required('Please select a city'),
    authorizerZipcode: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .test(
            'validateauthorizerZipcode',
            'Please enter a valid zip code',
            function (value) {
                if (value) {
                    let isValid = false;
                    // eslint-disable-next-line no-useless-escape
                    if (/^\d{5}([\-|\s]\d{1,4})?$/.test(value)) {
                        isValid = true;
                    } else if (/^[a-zA-Z0-9]{3}\s[a-zA-Z0-9]{3}$/.test(value)) {
                        let expArr = value.split(' ');

                        if (/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{3}$/.test(expArr[0]) && /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{3}$/.test(expArr[1])) {
                            isValid = true;
                        }
                    }
                    return isValid;
                }

            }
        )
        .required('Please enter zip code'),
    authorizerDOB: Yup
        .string().nullable()
        .required('Please enter authorizer date of birth'),
    authorizerAge: Yup.number(),
    incorporationDate: Yup
        .string().nullable()
        .trim('Please remove whitespace')
        .required('Please enter incorporation date'),
    incorporationCountryCode: Yup.string()
        .required('Please select a country'),
    incorporationStateCode: Yup.string()
        .trim('Please remove whitespace')
        .required('Please select a state'),
    incorporationCityId: Yup.number().moreThan(1, 'Please select a city')
        .required('Please select a city'),
});

const businessTypeOpt = () => {
    return (
        <Fragment>
            <option value="">Select a Business Type</option>
            <option value="CORPORATION">Corporation</option>
            <option value="LIMITED_LIABILITY_COMPANY">Limited Liability Company (LLC)</option>
            <option value="SOLE_PROPRIETORSHIP"> Sole Proprietorship </option>
            <option value="MUNICIPAL"> Municipal </option>
            <option value="NON_PROFIT_CORPORATION"> Non-Profit Corporation </option>
            <option value="PARTNERSHIP"> Partnership </option>
            <option value="PUBLICLY_TRADED_COMPANY"> Publicly Traded Company </option> 
        </Fragment>
    );
};

let initialValues2 = {
    authorizerAddress: '',
    authorizerCityId: null,
    authorizerCountryCode: '',
    authorizerDOB: '',
    authorizerFristname: '',
    authorizerLastname: '',
    authorizerStateCode: '',
    authorizerZipcode: '',
    incorporationCityId: null,
    incorporationCountryCode: '',
    incorporationDate: null,
    incorporationStateCode: ''
};

class BussinessEdit extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            countries: [],
            states: [],
            cities: [],
            selectedBusinessStates: [],
            selectedAutorizeStates: [],
            selectedIncorpStates: [],
            selectedBusinessCities: [],
            selectedAutorizeCities: [],
            selectedIncorpCities: [],

            errorMessge: null,
            uploadLoader: false,
            imageURL: 'null',
            key: 'bi',
            ssnError: null,

            startDate: new Date(), //date picker,
            succSendmailModal:false,
            sendmailWarningModal: false,
            sendResDisable: false

        };
    }

    //date picker
    handleChange = date => {
        this.setState({
            startDate: date
        });
    };

    _isMounted = false;


    handleCloseVendor = () => {
        this.props.onClick();
    };

    sendEmailHandeler =()=>{
        axios.get(`/business/sendRegistrationMail/${this.props.globalState.business.businessData.id}`)
            .then(res => {
                
                console.log('res', res);
                this.setState({ succSendmailModal: true, sendResDisable:true });
            })
            .catch(err => { 
                this.displayError(err);
                // this.setState({ sendmailWarningModal: true });
            });
    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        this.setState({
            errorMessge: errorMessge
        });

        setTimeout(() => {
            this.setState({ errorMessge: null });
        }, 5000);
    }

    handleChangeTab = (param) => {

        console.log('param', param);
        this.setState({
            key: param
        });

    }

    ssnFormatter = (param) => {

        let temp;
        if (param.indexOf('-') == -1 && param.length > 2) {
            temp = param.slice(0, 2) + '-' + param.slice(2, param.length);
        } else {
            temp = param;
        }

        if (temp.length > 11) {
            this.setState({
                ssnError: 'Tax ID should not be greater than 10 digit'
            });
        } else if (temp.length < 10) {
            this.setState({
                ssnError: 'Tax ID should not be less than 9 digit'
            });
        } else {
            this.setState({
                ssnError: null
            });
        }
        return temp;
    }

    closeSuccSendmailModal = () => {
        this.setState(
            { succSendmailModal: false }, () => {
            });

    }
    closeinvoiceWarningModal = () => {
        this.setState({ sendmailWarningModal: false });

    }

    showsuccSendmailModal = () => {
        this.setState({ succSendmailModal: true });

    }


    componentDidMount() {
        console.log('--------------------this.props.globalState.business.businessData.id -------------------', this.props.globalState.business.businessData.id);

        initialValues2 = this.props.step2Redux;
        if (this.props.step2Redux.incorporationDate != '') {
            initialValues2.incorporationDate = new Date(this.props.step2Redux.incorporationDate);
        }
        if (this.props.step2Redux.authorizerDOB != '') {
            initialValues2.authorizerDOB = (new Date(this.props.step2Redux.authorizerDOB));
        }


        this._isMounted = true;
        const countryService = new CountryService;
        countryService.getData().then(({ countries, states, cities }) => { //countries, states or cities
            if (this._isMounted) {
                this.setState({ countries, states, cities }, () => {
                    this.populateState({ target: { value: this.props.businessCountryCode } }, 'business');
                    this.populateState({ target: { value: this.props.authorizerCountryCode } }, 'authorize');
                    this.populateState({ target: { value: this.props.incorporationCountryCode } }, 'incorp');
                });
            }



        }).catch(e => {
            console.log(e);
        });

        if (this._isMounted) {
            this.setState({
                imageURL: this.props.imageURL
            });
        }

    }

    componentDidUpdate() {
        console.log('did update calling here');
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    populateState = (e, type) => {
        const countryCode = e.target.value;
        const states = this.state.states;
        let countryId = 0;
        for (let country of this.state.countries) if (country.countryCode === countryCode) countryId = country.id;
        const newStates = states.filter(state => Number(state.countryId) === Number(countryId));

        if (type === 'business') {
            this.setState({ selectedBusinessStates: newStates, selectedBusinessCities: [] }, () => {
                this.populateCity({ target: { value: this.props.businessStateCode } }, type);
            });
        }
        else if (type === 'authorize') {
            this.setState({ selectedAutorizeStates: newStates, selectedAutorizeCities: [] }, () => {
                this.populateCity({ target: { value: this.props.authorizerStateCode } }, type);
            });
        }
        else if (type === 'incorp') {
            this.setState({ selectedIncorpStates: newStates, selectedIncorpCities: [] }, () => {
                this.populateCity({ target: { value: this.props.incorporationStateCode } }, type);
            });
        }
    };

    populateCity = (e, type) => {

        const stateCode = e.target.value;
        const cities = this.state.cities;
        let stateId = 0;

        for (let st of this.state.states) if (st.stateCode === stateCode) stateId = st.id;
        const newCities = cities.filter(ct => Number(ct.stateId) === Number(stateId));

        if (type === 'business') { this.setState({ selectedBusinessCities: newCities }); }
        else if (type === 'authorize') { this.setState({ selectedAutorizeCities: newCities }); }
        else if (type === 'incorp') {
            this.setState({ selectedIncorpCities: newCities });
        }

    };

    onDrop = files => {

        this.setState({
            files,
            dropzoneActive: false,
            isDrop: true
        });

        if (files.length > 0) {
            // console.log('image details', files[0]);
            const config = {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    //sessionKey: localStorage.getItem("sessionKey")
                }
            };
            let formData = new window.FormData();

            formData.append('file', files[0]);

            // FILE SIZE RESTRICTION
            if (files[0]['size'] > 600000) {
                this.setState({
                    errorMessage: 'File size should be less than 600KB',
                    isDrop: false,
                });
                setTimeout(() => {
                    this.setState({ errorMessage: null });
                }, 5000);
            }

            else {

                this.setState({ uploadLoader: true }, () => {
                    axios
                        .put(`/business/updateLogo/${this.props.id}`, formData, config)

                        .then(res => {
                            // console.log('###logo image###', res.data);
                            if (this._isMounted) {
                                this.setState({
                                    uploadLoader: false,
                                    successMessage: 'Logo has been successfully uploaded',
                                    imageURL: res.data.imageURL
                                }, () => this.props.BusinessDatafetch());
                            }

                        })
                        .catch(err => {
                            console.log(err);
                            this.setState({
                                uploadLoader: false,
                                errorMessage: err.data.message
                            });
                        });
                });
            }

        }


        else if ((files[0]['type'] !== 'image/png') && (files[0]['type'] !== 'image/jpeg')) {
            this.setState({
                errorMessage: 'Please upload jpeg/png file',
                isDrop: false,
            });
            setTimeout(() => {
                this.setState({ errorMessage: null });
            }, 5000);
        }

    };


    submitStep1 = (values) => {
        values.businessNumber = values.businessNumber ? values.businessNumber.split('-').join('') : null;
        this.props.onSubmitStep1Action(values);
        this.handleChangeTab('profile');
    }

    submitStep2 = (values) => {



        this.props.onSubmitStep2Action(values);


        let EditBusinessObject = {
            ...this.props.step1Redux,
            ...this.props.step2Redux,
            ...this.props.restRedux
        };


        let req = {
            url: `/business/${EditBusinessObject.id}`,
            method: 'PUT',
            data: EditBusinessObject
        };

        axios(req)
            .then(() => {

                this.props.onSuccess();

            })
            .catch(this.displayError);


    }



    render() {
        const {
            selectedBusinessStates,
            selectedAutorizeStates,
            selectedIncorpStates,
            selectedBusinessCities,
            selectedAutorizeCities,
            selectedIncorpCities
        } = this.state;

        const{
            step1Redux
        } =this.props;

        return (
            <div>
                <Tabs
                    id="controlled-tab-BI-Edit"
                    activeKey={this.state.key}
                    //onSelect={key => this.setState({ key })}
                    onSelect={() => ((prevState) => ({
                        key: prevState,
                    }))}
                    className="mb-4 asiCalenderFix"
                >
                    <Tab eventKey="bi" title="Business Information">
                        <div className="p-3">
                            <Row>
                                <Col md={12}>
                                    {this.state.errorMessge ? (
                                        <div className="alert alert-danger" role="alert">
                                            {this.state.errorMessge}
                                        </div>
                                    ) : null}
                                </Col>
                            </Row>
                                        
                            <Row>

                                {step1Redux.isRegistered === false && this.state.sendResDisable === false 
                                    ? 
                                    <div className='col-sm-12 text-right mb-3'>
                                        <button className="btn btn-primary" onClick={this.sendEmailHandeler}>Send Registration Email</button>
                                    </div> 
                                    : 
                                    <div className='col-sm-12 text-right mb-3'>
                                        <button disabled className="btn btn-secondary">Send Registration Email</button>
                                    </div>
                                }

                                {/* <div className='col-sm-12 text-right mb-3'>
                                    <button className="btn btn-primary" onClick={this.sendEmailHandeler}>Send Registration Email</button>
                                </div> */}
                                <Col xs={12} md={3}>

                                    <div className="companyLogo border bg-light mb-4">
                                        <Dropzone
                                            //onDrop={acceptedFiles => console.log(acceptedFiles)}
                                            onDrop={this.onDrop}
                                        >
                                            {({ getRootProps, getInputProps }) => (
                                                <section>
                                                    <div {...getRootProps()}>
                                                        <input {...getInputProps()} />
                                                        <Fragment>
                                                            <div className="uploadWrap">
                                                                {this.state.uploadLoader ? (
                                                                    <LoadingSpinner />
                                                                ) : ((this.state.imageURL == 'null' || this.state.imageURL == '') ?
                                                                    (<Image
                                                                        src={UploadIcon}
                                                                        alt="upload"
                                                                    />)
                                                                    :
                                                                    (<Image src={this.state.imageURL} />)
                                                                )}


                                                                <p className="sm-txt-blue text-center">
                                                                    Please upload only jpg/png file
                                                                </p>
                                                            </div>
                                                        </Fragment>
                                                        <button type="button" className="btn btn-primary btn-position">
                                                            Upload logo
                                                        </button>
                                                    </div>
                                                </section>
                                            )}
                                        </Dropzone>
                                    </div>
                                    {/* <div className="mt-4 text-primary p-4">File size should be less than 600KB</div> */}

                                    {this.state.errorMessage ? <div className='mt-4 text-danger p-4'>{this.state.errorMessage}</div> : null}
                                </Col>
                                <Col xs={12} md={9}>
                                    {console.log('step1Redux',step1Redux)}
                                    <Formik
                                        enableReinitialize
                                        initialValues={this.props.step1Redux && this.props.step1Redux}
                                        validationSchema={editBusinessSchema1}
                                        //onSubmit={this.handleSubmit}
                                        onSubmit={this.submitStep1}
                                    >
                                        {({
                                            values,
                                            errors,
                                            touched,
                                            handleChange,
                                            handleBlur,
                                            //setFieldValue
                                        }) => {
                                            return (

                                                <Form className='editFromTab'>
                                                    <Row className="show-grid">
                                                        <Col xs={6} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>Business Name <span className="required">*</span></div>
                                                                <Field
                                                                    name="businessName"
                                                                    type="text"
                                                                    className={'form-control'}
                                                                    autoComplete="nope"
                                                                    placeholder="Enter"
                                                                    value={values.businessName}
                                                                />
                                                                {errors.businessName && touched.businessName ? (
                                                                    <span className="errorMsg pl-3">{errors.businessName}</span>
                                                                ) : null}
                                                                <FormControl.Feedback />
                                                            </FormGroup>
                                                        </Col>

                                                        <Col xs={6} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>Legal Structure (Business type) <span className="required">*</span></div>
                                                                <Field 
                                                                    component="select" 
                                                                    name="businessType" 
                                                                    className="form-control"
                                                                    value={values.businessType}
                                                                >
                                                                    {businessTypeOpt()}
                                                                </Field>
                                                                {/* {errors.businessType && touched.businessType ? (
                                                                    <span className="errorMsg pl-3">{errors.businessType}</span>
                                                                ) : null}} */}
                                                                <FormControl.Feedback />
                                                            </FormGroup>
                                                        </Col>

                                                        <Col xs={6} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>Business Tax ID/SSN <span className="required">*</span></div>
                                                                <Field
                                                                    type="text"
                                                                    name="businessNumber"
                                                                    className={'form-control'}
                                                                    value={this.ssnFormatter(values.businessNumber)}
                                                                    onChange={e => {
                                                                        handleChange(e);
                                                                    }}
                                                                    onBlur={handleBlur}
                                                                />
                                                                {errors.businessNumber && touched.businessNumber ? (
                                                                    <span className="errorMsg pl-3">{errors.businessNumber}</span>
                                                                ) : null}
                                                                {!errors.businessNumber && touched.businessNumber ? (
                                                                    <span className="errorMsg pl-3">{this.state.ssnError}</span>
                                                                ) : null}

                                                                <FormControl.Feedback />
                                                                {/* <HelpBlock>Validation Text</HelpBlock> */}
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>

                                                    <Row className="show-grid">
                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formControlsTextarea">
                                                                <div>Business Street Address <span className="required">*</span></div>
                                                                <Field
                                                                    name="businessAddress1"
                                                                    type="text"
                                                                    className={'form-control'}
                                                                    autoComplete="nope"
                                                                    placeholder="Enter"
                                                                    value={values.businessAddress1}
                                                                />
                                                                {errors.businessAddress1 && touched.businessAddress1 ? (
                                                                    <span className="errorMsg pl-3">{errors.businessAddress1}</span>
                                                                ) : null}
                                                                <FormControl.Feedback />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>Country <span className="required">*</span></div>
                                                                <Field
                                                                    component="select"
                                                                    name="businessCountryCode"
                                                                    placeholder="select"
                                                                    className="form-control"
                                                                    value={values.businessCountryCode || ''}
                                                                    onChange={e => {
                                                                        handleChange(e);
                                                                        this.populateState(e, 'business');
                                                                    }}
                                                                >
                                                                    <option value="">
                                                                        {this.state.countries.length ? 'Select Country' : 'Loading...'}
                                                                    </option>


                                                                    {this.state.countries.map(country => (
                                                                        <option key={country.id} value={country.countryCode}>
                                                                            {country.countryName}
                                                                        </option>
                                                                    ))}
                                                                </Field>
                                                                {errors.businessCountryCode && touched.businessCountryCode ? (
                                                                    <span className="errorMsg pl-3">{errors.businessCountryCode}</span>
                                                                ) : null}
                                                                <FormControl.Feedback />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>

                                                    <Row className="show-grid">
                                                        <Col xs={12} md={6} className="cityField">
                                                            <FormGroup controlId="formBasicText">
                                                                <div>State <span className="required">*</span></div>
                                                                <Field
                                                                    component="select"
                                                                    name="businessStateCode"
                                                                    placeholder="select"
                                                                    className="form-control"
                                                                    value={values.businessStateCode || ''}
                                                                    onChange={e => {
                                                                        handleChange(e);
                                                                        this.populateCity(e, 'business');
                                                                    }}
                                                                    disabled={selectedBusinessStates.length ? false : true}
                                                                >
                                                                    <option value="">Select State</option>

                                                                    {selectedBusinessStates.map(stName => (
                                                                        <option value={stName.stateCode} key={stName.id}>
                                                                            {stName.stateName}
                                                                        </option>
                                                                    ))}
                                                                </Field>
                                                                {errors.businessStateCode && touched.businessStateCode ? (
                                                                    <span className="errorMsg pl-3">{errors.businessStateCode}</span>
                                                                ) : null}
                                                                <FormControl.Feedback />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col xs={12} md={6} className="cityField">
                                                            <FormGroup controlId="formBasicText">
                                                                <div>City  <span className="required">*</span></div>

                                                                <Field
                                                                    component="select"
                                                                    name="businessCityId"
                                                                    placeholder="select"
                                                                    className="form-control"
                                                                    value={values.businessCityId || ''}
                                                                    onChange={e => {
                                                                        handleChange(e);
                                                                    }}
                                                                    disabled={selectedBusinessCities.length ? false : true}
                                                                >
                                                                    <option value="">Select City</option>

                                                                    {selectedBusinessCities.map(ct => (
                                                                        <option value={ct.id} key={ct.id}>
                                                                            {ct.cityName}
                                                                        </option>
                                                                    ))}
                                                                </Field>
                                                                {errors.businessCityId && touched.businessCityId ? (
                                                                    <span className="errorMsg pl-3">{errors.businessCityId}</span>
                                                                ) : null}


                                                                <FormControl.Feedback />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>


                                                    <Row className="show-grid">
                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>Zip Code <span className="required">*</span></div>
                                                                <Field
                                                                    name="businessZipcode"
                                                                    type="text"
                                                                    className={'form-control'}
                                                                    autoComplete="nope"
                                                                    placeholder="Enter"
                                                                />
                                                                {errors.businessZipcode && touched.businessZipcode ? (
                                                                    <span className="errorMsg pl-3">{errors.businessZipcode}</span>
                                                                ) : null}
                                                                <FormControl.Feedback />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>Business Phone <span className="required">*</span></div>

                                                                <InputMask
                                                                    mask="1 999-999-9999"
                                                                    maskChar={null}
                                                                    type="tel"
                                                                    name="businessPhone"
                                                                    className={`form-control ${values.businessPhone &&
                                                                        'input-elem-filled'}`}
                                                                    value={values.businessPhone}
                                                                    onChange={handleChange}
                                                                    onBlur={handleBlur}
                                                                />
                                                                {errors.businessPhone && touched.businessPhone ? (
                                                                    <span className="errorMsg pl-3">{errors.businessPhone}</span>
                                                                ) : null}
                                                                <FormControl.Feedback />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>

                                                    <Row className="show-grid">
                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>Business Email <span className="required">*</span></div>
                                                                <Field
                                                                    name="businessEmail"
                                                                    type="text"
                                                                    className={'form-control'}
                                                                    autoComplete="nope"
                                                                    placeholder="Enter"
                                                                    value={values.businessEmail}
                                                                />
                                                                {errors.businessEmail && touched.businessEmail ? (
                                                                    <span className="errorMsg pl-3">{errors.businessEmail}</span>
                                                                ) : null}
                                                                <FormControl.Feedback />
                                                            </FormGroup>
                                                        </Col>


                                                        <Col xs={12} md={6}>
                                                            <FormGroup controlId="formBasicText">
                                                                <div>Business Website<span className="required">*</span></div>
                                                                <Field
                                                                    name="businessWebsite"
                                                                    type="text"
                                                                    className={'form-control'}
                                                                    autoComplete="nope"
                                                                    placeholder="Enter"
                                                                    value={values.businessWebsite}
                                                                />
                                                                {errors.businessWebsite && touched.businessWebsite ? (
                                                                    <span className="errorMsg pl-3">{errors.businessWebsite}</span>
                                                                ) : null}
                                                                <FormControl.Feedback />
                                                            </FormGroup>
                                                        </Col>

                                                        <Col md={12}>
                                                            <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                                        </Col>

                                                    </Row>

                                                    <div>
                                                        <Link to="/dashboard/business" className="bg-secondary text-white ml-3 px-3 py-2 border-0" >
                                                            Back to Business
                                                        </Link>
                                                        <button
                                                            type="submit"
                                                            className="action-btn ml-3 px-3 py-1"
                                                            //onClick={() => this.handleChangeTab('profile')}
                                                            disabled={!errors.businessNumber && touched.businessNumber && this.state.ssnError ? true : false}
                                                        >
                                                            Next
                                                        </button>
                                                    </div>
                                                </Form>
                                            );
                                        }}
                                    </Formik>

                                </Col>
                            </Row>
                            <Row>&nbsp;</Row>

                        </div>
                    </Tab>
                    <Tab eventKey="profile" title="Autorized Signer Information" disabled={this.state.key === 'profile' ? true : false}>
                        <Row>
                            <Col md={12}>
                                {this.state.errorMessge ? (
                                    <div className="alert alert-danger mx-3" role="alert">
                                        {this.state.errorMessge}
                                    </div>
                                ) : null}
                            </Col>
                        </Row>
                        <div className="p-3">
                            <Formik
                                enableReinitialize
                                //initialValues={this.props.step2Redux && this.props.step2Redux} initialValues2
                                initialValues={initialValues2}
                                validationSchema={editBusinessSchema}
                                onSubmit={this.submitStep2}
                            >
                                {({
                                    values,
                                    errors,
                                    touched,
                                    handleChange,
                                    setFieldValue
                                }) => {

                                    return (

                                        <Form className='editFromTab'>
                                            <Row className="show-grid">
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <div>First Name <span className="required">*</span></div>

                                                        <Field
                                                            name="authorizerFristname"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.authorizerFristname}
                                                        />
                                                        {errors.authorizerFristname && touched.authorizerFristname ? (
                                                            <span className="errorMsg pl-3">{errors.authorizerFristname}</span>
                                                        ) : null}

                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <div>Last Name <span className="required">*</span></div>
                                                        <Field
                                                            name="authorizerLastname"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.authorizerLastname}
                                                        />
                                                        {errors.authorizerLastname && touched.authorizerLastname ? (
                                                            <span className="errorMsg pl-3">{errors.authorizerLastname}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <div>Date of Birth <span className="required">*</span></div>
                                                        <div className="cus-date">
                                                            {/* <DatePicker
                                                                name="authorizerDOB"
                                                                onChange={value => {
                                                                    console.log('date authorizerDOB:   ', new Date(value), value);
                                                                    setFieldValue(
                                                                        'authorizerDOB',
                                                                        value
                                                                    );
                                                                }

                                                                }
                                                                value={values.authorizerDOB || ''}
                                                                className={'form-control'}

                                                            /> */}
                                                            <DatePicker
                                                                name="authorizerDOB"
                                                                //dateFormat="dd/MM/yyyy"
                                                                autoComplete="off"
                                                                maxDate={new Date()}
                                                                selected={values.authorizerDOB || ''}
                                                                onChange={value => {
                                                                    console.log('date authorizerDOB:   ', new Date(value), value);
                                                                    setFieldValue(
                                                                        'authorizerDOB',
                                                                        value
                                                                    );
                                                                }}
                                                                className={'form-control w-100'}
                                                            />
                                                            {/* <GoCalendar/> */}
                                                        </div>


                                                        {/* <Field
                                                            name="authorizerDOB"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                        /> */}
                                                        {errors.authorizerDOB && touched.authorizerDOB ? (
                                                            <span className="errorMsg pl-3">{errors.authorizerDOB}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <div>
                                                            Authorizer Street Address
                                                            <span className="required">*</span>
                                                        </div>
                                                        <Field
                                                            name="authorizerAddress"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.authorizerAddress}
                                                        />
                                                        {errors.authorizerAddress && touched.authorizerAddress ? (
                                                            <span className="errorMsg pl-3">{errors.authorizerAddress}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <div>
                                                            Country
                                                            <span className="required">*</span>
                                                        </div>
                                                        <Field
                                                            component="select"
                                                            name="authorizerCountryCode"
                                                            placeholder="select"
                                                            className="form-control"
                                                            value={values.authorizerCountryCode || ''}
                                                            onChange={e => {
                                                                handleChange(e);
                                                                this.populateState(e, 'authorize');
                                                            }}
                                                        >
                                                            <option value="">
                                                                {this.state.countries.length ? 'Select Country' : 'Loading...'}
                                                            </option>


                                                            {this.state.countries.map(country => (
                                                                <option key={country.id} value={country.countryCode}>
                                                                    {country.countryName}
                                                                </option>
                                                            ))}
                                                        </Field>
                                                        {errors.authorizerCountryCode && touched.authorizerCountryCode ? (
                                                            <span className="errorMsg pl-3">{errors.authorizerCountryCode}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>


                                                <Col xs={12} md={6} className="cityField">
                                                    <FormGroup controlId="formBasicText">
                                                        <div>
                                                            State
                                                            <span className="required">*</span>
                                                        </div>
                                                        <Field
                                                            component="select"
                                                            name="authorizerStateCode"
                                                            placeholder="select"
                                                            className="form-control"
                                                            value={values.authorizerStateCode || ''}
                                                            onChange={e => {
                                                                handleChange(e);
                                                                this.populateCity(e, 'authorize');
                                                            }}
                                                            disabled={selectedAutorizeStates.length ? false : true}
                                                        >
                                                            <option value="">Select State</option>

                                                            {selectedAutorizeStates.map(stName => (
                                                                <option value={stName.stateCode} key={stName.id}>
                                                                    {stName.stateName}
                                                                </option>
                                                            ))}
                                                        </Field>
                                                        {errors.authorizerStateCode && touched.authorizerStateCode ? (
                                                            <span className="errorMsg pl-3">{errors.authorizerStateCode}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={6} className="cityField">
                                                    <FormGroup controlId="formBasicText">
                                                        <div>
                                                            City
                                                            <span className="required">*</span>
                                                        </div>

                                                        <Field
                                                            component="select"
                                                            name="authorizerCityId"
                                                            placeholder="select"
                                                            className="form-control"
                                                            value={values.authorizerCityId || ''}
                                                            onChange={e => {
                                                                handleChange(e);
                                                            }}
                                                            disabled={selectedAutorizeCities.length ? false : true}
                                                        >
                                                            <option value="">Select City</option>

                                                            {selectedAutorizeCities.map(ct => (
                                                                <option value={ct.id} key={ct.id}>
                                                                    {ct.cityName}
                                                                </option>
                                                            ))}
                                                        </Field>
                                                        {errors.authorizerCityId && touched.authorizerCityId ? (
                                                            <span className="errorMsg pl-3">{errors.authorizerCityId}</span>
                                                        ) : null}


                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <div>
                                                            Zip Code
                                                            <span className="required">*</span>
                                                        </div>
                                                        <Field
                                                            name="authorizerZipcode"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                        />
                                                        {errors.authorizerZipcode && touched.authorizerZipcode ? (
                                                            <span className="errorMsg pl-3">{errors.authorizerZipcode}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>


                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formBasicText">


                                                        <div>Incorporation Date<span className="required">*</span></div>
                                                        <div className="cus-date">
                                                            {/* <DatePicker
                                                                name="incorporationDate"
                                                                onChange={value => {
                                                                    console.log('date incorporationDate', value);
                                                                    setFieldValue(
                                                                        'incorporationDate',
                                                                        value
                                                                    );
                                                                }

                                                                }
                                                                value={values.incorporationDate || ''}
                                                                className={'form-control'}

                                                            /> */}
                                                            <DatePicker
                                                                name="incorporationDate"
                                                                autoComplete="off"
                                                                //dateFormat="dd/MM/yyyy"
                                                                maxDate={new Date()}
                                                                selected={values.incorporationDate || ''}
                                                                onChange={value => {
                                                                    console.log('date incorporationDate:   ', new Date(value), value);
                                                                    setFieldValue(
                                                                        'incorporationDate',
                                                                        value
                                                                    );
                                                                }}
                                                                className={'form-control w-100'}
                                                            />
                                                        </div>

                                                        {/* <Field
                                                            name="incorporationDate"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                        /> */}
                                                        {errors.incorporationDate && touched.incorporationDate ? (
                                                            <span className="errorMsg pl-3">{errors.incorporationDate}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <div>
                                                            Country
                                                            <span className="required">*</span>
                                                        </div>
                                                        <Field
                                                            component="select"
                                                            name="incorporationCountryCode"
                                                            placeholder="select"
                                                            className="form-control"
                                                            value={values.incorporationCountryCode || ''}
                                                            onChange={e => {
                                                                handleChange(e);
                                                                this.populateState(e, 'incorp');
                                                            }}
                                                        >
                                                            <option value="">
                                                                {this.state.countries.length ? 'Select Country' : 'Loading...'}
                                                            </option>


                                                            {this.state.countries.map(country => (
                                                                <option key={country.id} value={country.countryCode}>
                                                                    {country.countryName}
                                                                </option>
                                                            ))}
                                                        </Field>
                                                        {errors.incorporationCountryCode && touched.incorporationCountryCode ? (
                                                            <span className="errorMsg pl-3">{errors.incorporationCountryCode}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>

                                                <Col xs={12} md={6} className="cityField">
                                                    <FormGroup controlId="formBasicText">
                                                        <div>
                                                            State
                                                            <span className="required">*</span>
                                                        </div>
                                                        <Field
                                                            component="select"
                                                            name="incorporationStateCode"
                                                            placeholder="select"
                                                            className="form-control"
                                                            value={values.incorporationStateCode || ''}
                                                            onChange={e => {
                                                                handleChange(e);
                                                                this.populateCity(e, 'incorp');
                                                            }}

                                                            disabled={selectedIncorpStates.length ? false : true}
                                                        >
                                                            <option value="">Select State</option>

                                                            {selectedIncorpStates.map(stName => (
                                                                <option value={stName.stateCode} key={stName.id}>
                                                                    {stName.stateName}
                                                                </option>
                                                            ))}
                                                        </Field>
                                                        {errors.incorporationStateCode && touched.incorporationStateCode ? (
                                                            <span className="errorMsg pl-3">{errors.incorporationStateCode}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={6} className="cityField">
                                                    <FormGroup controlId="formBasicText">
                                                        <div>
                                                            City
                                                            <span className="required">*</span>
                                                        </div>

                                                        <Field
                                                            component="select"
                                                            name="incorporationCityId"
                                                            placeholder="select"
                                                            className="form-control"
                                                            value={values.incorporationCityId || ''}
                                                            onChange={e => {
                                                                handleChange(e);
                                                            }}
                                                            disabled={selectedIncorpCities.length ? false : true}
                                                        >
                                                            <option value="">Select City</option>

                                                            {selectedIncorpCities.map(ct => (
                                                                <option value={ct.id} key={ct.id}>
                                                                    {ct.cityName}
                                                                </option>
                                                            ))}
                                                        </Field>
                                                        {errors.incorporationCityId && touched.incorporationCityId ? (
                                                            <span className="errorMsg pl-3">{errors.incorporationCityId}</span>
                                                        ) : null}


                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                            </Row>

                                            <div className="row text-center">

                                                <div className="col-sm-12">
                                                    <button type="button" className="action-btn ml-3 px-3 py-1 bg-secondary text-white" onClick={() => this.handleChangeTab('bi')}>Back</button>
                                                    <button type="submit" className="action-btn ml-3 px-3 py-1">Confirm</button>
                                                </div>

                                            </div>
                                        </Form>
                                    );
                                }}
                            </Formik>

                        </div>

                    </Tab>
                </Tabs>

                {/* <Row className="show-grid text-center">
                    <Col xs={12} md={12} className="my-3">
                        <Link to="/dashboard/business" className="bg-secondary text-white ml-3 px-3 py-2 border-0" >
                            Back to Business
                        </Link>
                        <button type="submit" className="action-btn ml-3 px-3 py-1">
                            Confirm
                        </button>
                    </Col>
                </Row> */}

                <Modal
                    show={this.state.succSendmailModal}
                    onHide={this.closeSuccSendmailModal}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                {/* <h5>Are you sure you want to unlock your card ?</h5> */}
                                <h5>Registration email successfully sent</h5>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            className="but-gray m-auto"
                            type="button"
                            onClick={
                                () => { this.closeSuccSendmailModal(); }
                            }
                        >Return</Button>
                    </Modal.Footer>
                </Modal>

                <Modal
                    show={this.state.sendmailWarningModal}
                    onHide={this.closeinvoiceWarningModal}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <p> {this.state.errorMessage}</p>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>

                        <Button
                            onClick={this.closeinvoiceWarningModal}
                            className="but-gray">
                            Return
                        </Button>
                    </Modal.Footer>
                </Modal>

            </div>
        );
    }
}

BussinessEdit.propTypes = {
    onClick: PropTypes.func,
    onSuccess: PropTypes.func,
    countryCode: PropTypes.string,
    tooltip: PropTypes.object,
    stateCode: PropTypes.string,
    cityId: PropTypes.string,
    authorizerCountryCode: PropTypes.string,
    businessCountryCode: PropTypes.string,
    incorporationCountryCode: PropTypes.string,
    businessStateCode: PropTypes.string,
    authorizerStateCode: PropTypes.string,
    incorporationStateCode: PropTypes.string,
    id: PropTypes.number,
    imageURL: PropTypes.string,
    step1Redux: PropTypes.object,
    step2Redux: PropTypes.object,
    restRedux: PropTypes.object,
    onSubmitStep1Action: PropTypes.func,
    onSubmitStep2Action: PropTypes.func,
    history: PropTypes.object,
    BusinessDatafetch: PropTypes.func,
    globalState: PropTypes.object,


};


const mapStateToProps = state => {
    return {
        step1Redux: state.businessEdit.step1,
        step2Redux: state.businessEdit.step2,
        restRedux: state.businessEdit.rest,
        globalState: state
    };
};
const mapDispatchToProps = dispatch => {
    return {
        onSubmitStep1Action: data => dispatch(businessEditProcessStep1(data)),
        onSubmitStep2Action: data => dispatch(businessEditProcessStep2(data)),

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(BussinessEdit);
