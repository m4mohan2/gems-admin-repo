import React, {
    Component,
    //Fragment 
} from 'react';
import { connect } from 'react-redux';
import { handelBusinessIDName } from '../../../../../redux/actions/business';
import axios from '../../../../../shared/eaxios';
import PropTypes from 'prop-types';
import LoadingSpinner from '../../../../../Components/LoadingSpinner/LoadingSpinner';
import { CountryService } from '../../../../../services/country.service';
import Pagination from 'react-js-pagination';
import { Link } from 'react-router-dom';
// import BussinessEdit from './BusinessEdit';
import AddBusiness from './AddBusiness/AddBusiness';
import {

    businessRegistrationProcessCleanup
} from '../../../../../redux/actions/businessRegistration';


import './Business.scss';
import refreshIcon from './../../../../../assets/refreshIcon.png';

import {
    Table,
    //Dropdown,
    Row,
    Modal,
    Col,
    Image,
    Button
} from 'react-bootstrap';
import { FaCaretDown } from 'react-icons/fa';
import { FaCaretUp } from 'react-icons/fa';
import SuccessIco from './../../../../../assets/success-ico.png';
import { Base64 } from 'js-base64';
let initialValues = {};

class Business extends Component {

    state = {
        businessLists: [],
        editVendor: [],
        showBusinessModal: false,
        successMessage: null,
        activePage: 1,
        totalCount: 0,
        itemPerPage: 250,
        showConfirMmsg: false,
        showAddBusinessConfirMmsg: false,
        DeleteBusinessModal: false,
        businessArchiveConfirMmsg: false,
        errorMessge: null,
        errMsg: null,
        showDeleteMmsg: false,
        showDeleteConfirm: false,
        loading: true,
        editBusinessLoading: false,
        editBusinessUI: false,
        selectedBusinessName: '',
        businessFilter: '',
        sortingActiveID: 1,
        key: 'bi',
        editBusiness: {},
        dropzoneActive: false,
        businessDeleteLoader: false,
        archiveConfirm: true,
        selectedBusiness: 'all',
        sort: null,
        field: null,
        businessSearchKey: '',
        bankModal: false

    };


    _isMounted = false;

    taxIDFormator = (param) =>{
        return param.slice(0, 2) + '-' + param.slice(2, param.length);    
    }

    handleChange = (e) => {
        this.setState({ selectedBusiness: e.target.value },
            () => {
                //console.log('this.state.value', this.state.selectedBusiness);
                this.fetchBusinessList(0, '', '', '', '', this.state.selectedBusiness);
                this.resetPagination();
            });
    }

    

    successMessgae = () => {
        this.setState({
            showConfirMmsg: true,
            showBusinessModal: false,
            editBusinessUI: false,
            activePage: 1
        });
        this.fetchBusinessList(0, '', '', '', '', this.state.selectedBusiness);
    };

    addbusinesssuccessMessgae = () => {
        this.setState({
            showAddBusinessConfirMmsg: true,
            showBusinessModal: false,
        });
        this.fetchBusinessList();
    };

    handleConfirmReviewClose = () => {
        this.setState({
            showConfirMmsg: false,
            showAddBusinessConfirMmsg: false,
            DeleteBusinessModal: false,
            businessArchiveConfirMmsg: false,
            archiveConfirm: true,

        });
    };

    handelState = (id, name, type) => {
        let businessObj = {};
        businessObj.id = id;
        businessObj.name = name;
        businessObj.type = type;

        console.log('businessObj', businessObj);
        this.props.onClickAction(businessObj);
        this.props.history.push('/dashboard/business/view');
    };

    handleCloseBusiness = () => {
        this.setState({
            showBusinessModal: false,
            editBusinessUI: false
        }, () => { this.props.cleanStore(null); });
    };

    displayError = (e) => {
        console.log('displayError', e.data.message);
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        
        return errorMessge;
    }

    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber });
        
        this.fetchBusinessList(
            pageNumber > 0 ? pageNumber - 1 : 0, 
            '', 
            Base64.encode(this.state.businessSearchKey), 
            this.state.sort, 
            this.state.field, 
            this.state.selectedBusiness
        );
    };

    handleChangeItemPerPage = (e) => {
        this.setState({ itemPerPage: e.target.value },
            () => {
                this.fetchBusinessList(
                    this.state.activePage > 0 ? this.state.activePage - 1 : 0,
                    '',
                    Base64.encode(this.state.businessSearchKey),
                    this.state.sort,
                    this.state.field,
                    this.state.selectedBusiness
                );
            });
    }

    resetPagination = () => {
        this.setState({ activePage: 1 });
    }

    

    businessAdd = () => {
        this.setState({
            showBusinessModal: true,
        }, () => { });
    }


    handelArchive = () => {
        this.setState({
            archiveConfirm: false,
            businessDeleteLoader: true
        });
        axios
            .delete(
                `business/${this.state.archiveBusinessID}`
            ).then(res => {
                console.log('business deleted successfully', res.data);
                this.fetchBusinessList();
                this.setState({
                    businessArchiveConfirMmsg: true,
                    businessDeleteLoader: false,
                    activePage: 1
                });

            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    errorMessge: errorMsg,
                    businessDeleteLoader: false,
                    DeleteBusinessModal: false,
                    archiveConfirm: true

                });

                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);

            });
    }

    businessArchive = (id) => {

        this.setState({
            archiveBusinessID: id,
            DeleteBusinessModal: true,
        });



    }

    businessEdit = (id, businessrow) => {
        console.log('id, businessrow', id, businessrow);

        this.setState({
            showBusinessModal: true,
            editBusinessLoading: true,
            editBusinessUI: true
        }, () => {

            axios
                .get(

                    `business/${id}`
                )
                .then(res => {
                    console.log('editBusiness', res.data);

                    if (this._isMounted) {
                        this.setState({
                            editBusiness: res.data,
                            editBusinessLoading: false
                        }, () => {
                            initialValues = this.state.editBusiness;
                            console.log('editBusiness', initialValues);
                        });
                    }


                })
                .catch(this.displayError);

        });

    }

    componentDidMount() {
        
        console.log('this.props.history', this.props.history);

        this._isMounted = true;
        this.fetchBusinessList(0, '', '', '', '', this.state.selectedBusiness);
        const countryService = new CountryService;
        countryService.getData().then(({ countries, states, cities }) => {
            //countries, states or cities
            this.setState({ countries, states, cities }, () => {
                this.populateState({ target: { value: this.props.countryCode } });
            });
        }).catch(e => {
            console.log(e);
        });

    }

    componentWillUnmount() {
        //this.props.onClickAction(null);
        this.setState({ errorMessge: null});
        this._isMounted = false;

    }

    fetchBusinessList = (
        since = 0, // Pagination strating from --1
        searchKey = '', // search by  ID --2 this.state.businessSearchKey
        filter = this.state.businessSearchKey, //  serch by keyword like 
        sort = '', // sort with a = true, d =flase --4
        field = '', // sort with field name --5
        custFilter = this.state.selectedBusiness //filter for dropdown --6
    ) => {

        this.setState({
            loading: true,

            sort: sort,
            field: field
        }, () => {
            console.log('state', this.state.sort, this.state.field);

            axios
                .get(

                    `business/list?since=${since}&limit=${this.state.itemPerPage}&id=${searchKey}&key=${filter}&direction=${this.state.sort}&prop=${this.state.field}&custFilter=${custFilter}`
                )
                .then(res => {
                    //console.log('res.data-----------------', res.data);
                    const businessLists = res.data.entries;
                    const totalCount = res.data.total;
                    if (this._isMounted) {
                        this.setState({
                            businessLists: businessLists,
                            totalCount: totalCount,
                            loading: false
                        });
                    }

                    console.log('business list-----------------------', this.state.businessLists);
                })
                .catch(e => {
                    let errorMsg = this.displayError(e);
                    this.setState({
                        errorMessge: errorMsg,
                        loading: false
                    });
                    setTimeout(() => {
                        this.setState({ errorMessge: null });
                    }, 5000);

                });

        });

    };

    sortingActive = (id) => {
        this.setState({
            sortingActiveID: id
        }, () => { console.log('this.state.sortingActiveID', this.state.sortingActiveID); });
    }

    populateState = e => {
        const countryCode = e.target.value;
        const states = this.state.states;
        let countryId = 0;
        for (let country of this.state.countries) if (country.countryCode === countryCode) countryId = country.id;
        const newStates = states.filter(state => Number(state.countryId) === Number(countryId));
        this.setState({ selectedStates: newStates, selectedCities: [] }, () => {
            this.populateCity({ target: { value: this.props.stateCode } });
        });
    };

    populateCity = e => {
        console.log('populate city ', e);
        const stateCode = e.target.value;
        const cities = this.state.cities;
        let stateId = 0;
        for (let st of this.state.states) if (st.stateCode === stateCode) stateId = st.id;
        const newCities = cities.filter(ct => Number(ct.stateId) === Number(stateId));
        this.setState({ selectedCities: newCities });
    };

    handelOnchange = (e) => {

        this.setState(
            {
                businessSearchKey: e.target.value,
            },
            () => {
                console.log('this.state.businessSearchKey', this.state.businessSearchKey);
            }
        );

    }

    handlekeyPress = (e) =>{
        if (e.key ==='Enter'){
            let businessSearchKey = this.state.businessSearchKey;
            businessSearchKey = businessSearchKey.trim();
            this.setState({
                businessSearchKey
            }, ()=>{
                let businessSearchKeyB64 = Base64.encode(this.state.businessSearchKey);
                this.fetchBusinessList(0, '', businessSearchKeyB64);
            });
            
        }
        
    }

    handelSearch = () => {

        if (this.state.businessSearchKey !=''){
            let businessSearchKey = this.state.businessSearchKey;
            businessSearchKey = businessSearchKey.trim();
            this.setState({
                businessSearchKey
            },()=>{
                let businessSearchKeyB64 = Base64.encode(this.state.businessSearchKey);
                this.fetchBusinessList(0, '', businessSearchKeyB64);
            });

            
        }
        
    }
    resetSearch = () => {

        this.setState({
            selectedBusiness: '',
            // selectedBankName: '',
            businessSearchKey: '',
            errorMessge:null
        }, () => this.fetchBusinessList());
        
    }

    handelBank = () => {

        console.log('getting...');

    }


    render() {

        //this.state.editBusiness && Object.assign(initialValues, { ...this.state.editBusiness });

        // console.log('props---', this.props);
        return (
            <div className="dashboardInner businessOuter">
                {/* var decodedData = window.atob(encodedData); */}
                
                {this.state.errorMessge
                    ?
                    <div className="alert alert-danger col-12" role="alert">
                        ERROR : {this.state.errorMessge}
                    </div>
                    :
                    null
                }
                <div className="row">
                    <div className="col-10">
                        <select
                            id={this.state.selectedBusiness}
                            className="form-control truncate pr-35 float-left w-210 mr-3"
                            onChange={this.handleChange}
                            value={this.state.selectedBusiness}>
                            <option value='all'>All Businesses</option>
                            <option value='registerBusiness'> Fintainium Customers </option>
                            <option value='vendorsBusiness'> Vendors</option>
                            <option value='customerBusiness'> Customers</option>

                        </select>

                        {/* <div className="form-group float-left">
                            <button
                                type="button"
                                className="btn search-btn text-black "
                                onClick={() => this.handelSearch()}
                            >
                                Search
                            </button>
                        </div> */}

                        <input 
                            type="text" 
                            className="w-210 form-control float-left" 
                            placeholder="Type to search" 
                            onChange={this.handelOnchange}
                            onKeyPress={this.handlekeyPress} 
                            value={this.state.businessSearchKey} 
                        />
                        

                        <button
                            type="button"
                            className="btn ml-3 search-btn text-black float-left cus-mt-3"
                            onClick={() => this.handelSearch()}
                        >
                            Search
                        </button>

                        <Link to="#" className='ml-2'>
                            <Image className="mt-2" src={refreshIcon} onClick={() => this.resetSearch()} />
                        </Link>

                    </div>
                    <div className="col-2">
                        
                        <Button variant="primary float-right mb-3 font-14" onClick={() => { this.businessAdd(); }}>Add Business</Button>
                    </div>
                </div>
                <div className="boxBg">

                    <Table responsive hover>
                        <thead className="theaderBg">
                            <tr>
                                {/* <th></th> */}
                                <th>
                                    <span className="float-left pr-2">Business Name </span>
                                    <span className="d-flex flex-column-reverse sortingFontSize">
                                        <button className="custom-btn-focus">
                                            <FaCaretDown
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 2 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.fetchBusinessList(0, '', '', false, 'business', this.state.selectedBusiness);
                                                    this.setState({ activePage: 1 });
                                                    this.sortingActive(2);
                                                }} />
                                        </button>
                                        <button className="custom-btn-focus">
                                            <FaCaretUp
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 1 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.fetchBusinessList(0, '', '', true, 'business', this.state.selectedBusiness);
                                                    this.setState({ activePage: 1 });
                                                    this.sortingActive(1);
                                                }} />
                                        </button>
                                    </span>
                                </th>
                                <th>
                                    <span className="float-left pr-2">Business Tax ID</span>
                                    <span className="d-flex flex-column-reverse sortingFontSize">
                                        <button className="custom-btn-focus">
                                            <FaCaretDown
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 4 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.fetchBusinessList(0, '', '', false, 'businessNumber', this.state.selectedBusiness);
                                                    this.setState({ activePage: 1 });
                                                    this.sortingActive(4);
                                                }} />
                                        </button>
                                        <button className="custom-btn-focus">
                                            <FaCaretUp
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 3 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.fetchBusinessList(0, '', '', true, 'businessNumber', this.state.selectedBusiness);
                                                    this.setState({ activePage: 1 });
                                                    this.sortingActive(3);
                                                }} />
                                        </button>
                                    </span>
                                </th>
                                <th>

                                    <span className="float-left pr-2">Phone No</span>
                                    <span className="d-flex flex-column-reverse sortingFontSize">
                                        <button className="custom-btn-focus">
                                            <FaCaretDown
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 6 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.fetchBusinessList(0, '', '', false, 'businessPhone', this.state.selectedBusiness);
                                                    this.setState({ activePage: 1 });
                                                    this.sortingActive(6);
                                                }} />
                                        </button>
                                        <button className="custom-btn-focus">
                                            <FaCaretUp
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 5 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.fetchBusinessList(0, '', '', true, 'businessPhone', this.state.selectedBusiness);
                                                    this.setState({ activePage: 1 });
                                                    this.sortingActive(5);
                                                }} />
                                        </button>
                                    </span>
                                </th>
                                <th>
                                    <span className="float-left pr-2">Email ID</span>
                                    <span className="d-flex flex-column-reverse sortingFontSize">
                                        <button className="custom-btn-focus">
                                            <FaCaretDown
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 8 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.fetchBusinessList(0, '', '', false, 'businessEmail', this.state.selectedBusiness);
                                                    this.setState({ activePage: 1 });
                                                    this.sortingActive(8);
                                                }} />
                                        </button>
                                        <button className="custom-btn-focus">
                                            <FaCaretUp
                                                className={'cursorPointer ' + (this.state.sortingActiveID == 7 ? 'activeColor' : '')}
                                                onClick={() => {
                                                    this.fetchBusinessList(0, '', '', true, 'businessEmail', this.state.selectedBusiness);
                                                    this.setState({ activePage: 1 });
                                                    this.sortingActive(7);
                                                }} />
                                        </button>
                                    </span>
                                </th>
                                {/* <th>
                                    Vendor Count

                                </th>
                                <th>Invoice Count</th> */}
                                <th>Business Type</th>
                                {/* <th></th> */}
                            </tr>
                        </thead>
                        <tbody>

                            {this.state.loading ? (<tr>
                                <td colSpan={12}>
                                    <LoadingSpinner />
                                </td>
                            </tr>) :
                                this.state.businessLists.length > 0 ? (
                                    this.state.businessLists.map(businessList => (
                                        <tr
                                            key={businessList.id}
                                            onClick={() => this.handelState(businessList.id, businessList.businessName, businessList.type)}
                                            className="cursorPointer"
                                        >
                                            {/* <td>
                                                {businessList.isCustomer === true ? <Dropdown>

                                                    <Dropdown.Toggle className="customActionButton">
                                                        <FiMenu />
                                                    </Dropdown.Toggle>

                                                    <Dropdown.Menu>
                                                        <Dropdown.Item onClick={() => { this.businessEdit(businessList.id, businessList); }}>
                                                            
                                                            Edit
                                                        </Dropdown.Item>
                                                        <Dropdown.Item onClick={() => { this.businessArchive(businessList.id); }}>
                                                            Archive
                                                        </Dropdown.Item>
                                                    </Dropdown.Menu>

                                                </Dropdown> : null}

                                            </td> */}
                                            <td>{businessList.businessName}</td>
                                            <td>{businessList.businessNumber !== '' ? this.taxIDFormator(businessList.businessNumber) : null}</td>
                                            <td>{businessList.businessPhone}</td>
                                            <td>{businessList.businessEmail}</td>
                                            {/* <td align="center" onClick={() => this.handelState(businessList.id, businessList.businessName)}><Link className="btn btn-primary text-white" to="vendor">{businessList.vendorCount}</Link></td>
                                            <td align="center"><Link className="btn btn-secondary text-white" to="#">{businessList.invoiceCount}</Link></td> */}
                                            {/* <td align="center">{businessList.isCustomer === true ? <span>Yes</span> : <span>No</span>}</td> */}
                                            <td>{businessList.type}</td>
                                            {/* <td><MdLaunch className="cursor-pointer" onClick={()=>this.handelBank()}/></td> */}
                                        </tr>
                                    ))
                                )
                                    :
                                    this.state.errorMessge ? <tr>
                                        <td colSpan={6}>
                                            <p className="text-center">{this.state.errorMessge}</p>
                                        </td>
                                    </tr> : (
                                        <tr>
                                            <td colSpan={6}>
                                                <p className="text-center">No records found</p>
                                            </td>
                                        </tr>
                                    )
                            }

                        </tbody>
                    </Table>

                </div>
                {this.state.totalCount ? (
                    <Row>
                        <Col md={4} className="d-flex flex-row mt-20">
                            <span className="mr-2 mt-2 font-weight-500">Items per page</span>
                            <select
                                id={this.state.itemPerPage}
                                className="form-control truncatefloat-left w-90"
                                onChange={this.handleChangeItemPerPage}
                                value={this.state.itemPerPage}>
                                <option value='50'>50</option>
                                <option value='100'>100</option>
                                <option value='150'>150</option>
                                <option value='200'>200</option>
                                <option value='250'>250</option>

                            </select> 
                        </Col>
                        <Col md={8}>
                            <div className="paginationOuter text-right">
                                <Pagination
                                    activePage={this.state.activePage}
                                    itemsCountPerPage={this.state.itemPerPage}
                                    totalItemsCount={this.state.totalCount}
                                    onChange={this.handlePageChange}
                                />
                            </div>
                        </Col>
                    </Row>
                ) : null}



                {/*==================EDIT/ADD BUSINESS DETAILS======================*/}
                <Modal
                    show={this.state.showBusinessModal}
                    onHide={this.handleCloseBusiness}
                    className="full-width-modal"
                    size="lg"
                >
                    <Modal.Header closeButton className="cusClose">
                        {this.state.editBusinessUI == true ? <Modal.Title>Edit Business</Modal.Title> : <Modal.Title>Add Business</Modal.Title>}
                    </Modal.Header>
                    <Modal.Body>
                        <AddBusiness onSuccess={this.addbusinesssuccessMessgae} />
                    </Modal.Body>
                </Modal>


                {/*======  confirmation popup  ===== */}
                <Modal
                    show={this.state.showConfirMmsg}
                    onHide={this.handleConfirmReviewClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Business has been successfully updated</h5>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handleConfirmReviewClose}
                            className="but-gray"
                        >
                            Return
                        </Button>
                    </Modal.Footer>
                </Modal>

                {/*====== Add business confirmation popup  ===== */}
                <Modal
                    show={this.state.showAddBusinessConfirMmsg}
                    onHide={this.handleConfirmReviewClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Business has been successfully added</h5>
                            </Col>
                        </Row>
                    </Modal.Body>
                    
                    <Row className="py-3">
                        <Col md={12} className="text-center">
                            <Button
                                onClick={this.handleConfirmReviewClose}
                                className="but-gray"
                            >
                                    Return
                            </Button>
                        </Col>
                    </Row>
                    
                </Modal>


                {/*====== business deleted  confirmation popup  ===== */}
                <Modal
                    show={this.state.DeleteBusinessModal}
                    onHide={this.handleConfirmReviewClose}
                    className="payOptionPop"
                >

                    <Modal.Body>
                        {this.state.businessArchiveConfirMmsg === true
                            ?
                            <React.Fragment>

                                <Row>
                                    <Col md={12} className="text-center">
                                        <Image src={SuccessIco} />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={12} className="text-center">
                                        <h5>Business has been successfully Deleted</h5>
                                    </Col>
                                </Row>

                            </React.Fragment> : (
                                this.state.businessDeleteLoader
                                    ? <LoadingSpinner />
                                    :
                                    <div className="text-center p-2"><h6>Warning!! User will be permanently deleted and can not be recovered.Are you sure ?</h6></div>)}

                        {this.state.businessDeleteLoader === false ? <div className='text-center'>
                            {this.state.archiveConfirm === true ? <Button
                                className="btn btn-danger mx-1"
                                onClick={this.handelArchive}
                            >
                                Proceed
                            </Button> : null}



                            {/* {this.state.businessDeleteLoader === false ? */}
                            <Button
                                onClick={this.handleConfirmReviewClose}
                                className="but-gray mx-1"
                            >
                                Return
                            </Button>
                            {/* : null} */}


                        </div> : null}

                    </Modal.Body>


                </Modal>



            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        globalState: state
    };
};
const mapDispatchToProps = dispatch => {
    return {
        onClickAction: data => dispatch(handelBusinessIDName(data)),
        cleanStore: data => dispatch(businessRegistrationProcessCleanup(data))
    };
};


Business.propTypes = {
    countryCode: PropTypes.string,
    stateCode: PropTypes.string,
    cityId: PropTypes.string,
    onClickAction: PropTypes.func,
    cleanStore: PropTypes.func,
    history: PropTypes.object,
};
export default connect(mapStateToProps, mapDispatchToProps)(Business);
