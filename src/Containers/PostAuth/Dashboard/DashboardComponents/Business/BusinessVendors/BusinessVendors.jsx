import React, {
    Component,
    //Fragment 
} from 'react';
import { connect } from 'react-redux';
import axios from './../../../../../../shared/eaxios';
import PropTypes from 'prop-types';
import LoadingSpinner from './../../../../../../Components/LoadingSpinner/LoadingSpinner';

import {
    Table,
    //Dropdown,
    Row,
    Modal,
    Col,
    Image,
    Button
} from 'react-bootstrap';
import { FaCaretDown } from 'react-icons/fa';
import { FaCaretUp } from 'react-icons/fa';
import SuccessIco from './../../../../../../assets/success-ico.png';
//import { FiMenu } from 'react-icons/fi';
import Pagination from 'react-js-pagination';
import AddBusinessVendors from './AddBusinessVendors';
import EditBusinessVendors from './EditBusinessVendors';



class BusinessVendors extends Component {

    state = {
        activePage: 1,
        totalCount: 0,
        itemPerPage: 250,
        sort: null,
        field: null,
        fetchErrorMsg: null,
        vendorLists: [],
        loading: true,
        sortingActiveID: 1,
        addModal: false,
        viewEditModal: false,
        vendorData: {},
        editConfirMmsg: false,
        addConfirMmsg: false

    }

    _isMounted = false;

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }

        return errorMessge;
    }
    sortingActive = (id) => {
        this.setState({
            sortingActiveID: id
        }, () => { console.log('this.state.sortingActiveID', this.state.sortingActiveID); });
    }
    fetchBusinessVendors = (
        since = 0, // Pagination strating from --1
        filter = '', //  serch by keyword like : name, ph --3
        sort = '', // sort with a = true, d =flase --4
        field = '', // sort with field name --5
    ) => {
        this.setState({
            loading: true,
            sort: sort,
            field: field
        }, () => {
            axios
                .get(

                    `customerVendor/vendorlist/${this.props.globalState.business.businessData.id}?since=${since}&limit=${this.state.itemPerPage}&key=${filter}&direction=${this.state.sort}&prop=${this.state.field}&businessType=${this.props.globalState.business.businessData.type}`

                    //http://localhost:7090/admin/customerVendor/customer/business/15847?since&limit&prop=&key=&direction
                    //`customerVendor/vendorlist/9740?since=${since}&limit=${this.state.itemPerPage}&key=${filter}&direction=${this.state.sort}&prop=${this.state.field}`
                )
                .then(res => {
                    const vendorLists = res.data.entries;
                    const totalCount = res.data.total;

                    if (this._isMounted) {
                        this.setState({
                            vendorLists,
                            totalCount: totalCount,
                            loading: false
                        });
                    }

                })
                .catch(e => {
                    let errorMsg = this.displayError(e);
                    this.setState({
                        fetchErrorMsg: errorMsg,
                        loading: false
                    }, () => {
                        console.log('this.state.fetchErrorMsg', this.state.fetchErrorMsg);
                    });

                    setTimeout(() => {
                        this.setState({ fetchErrorMsg: null });
                    }, 5000);
                });
        });
    }


    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber });
        this.fetchBusinessVendors(pageNumber > 0 ? pageNumber - 1 : 0, '', this.state.sort, this.state.field);
    };

    resetPagination = () => {
        this.setState({ activePage: 1 });
    }


    handleChangeItemPerPage = (e) => {
        this.setState({ itemPerPage: e.target.value },
            () => {
                this.fetchBusinessVendors(this.state.activePage > 0 ? this.state.activePage - 1 : 0, '', this.state.sort, this.state.field);
            });
    }



    handelAddModal = () => {
        this.setState({
            addModal: true,
        });
    }

    handelAddModalClose = () => {
        this.setState({
            addModal: false,
        });
    }


    handelviewEditModal = (data) => {
        console.log('this.state.viewEditData', data);
        this.setState({
            viewEditModal: true,
            vendorData: data
        }, () => {
            console.log('this.state.viewEditData@@', this.state.vendorData);
        });
    }

    handelviewEditModalClose = () => {
        this.setState({
            viewEditModal: false,
        });
    }

    handleEditConfirMmsg = () => {
        this.setState({
            viewEditModal: false,
            editConfirMmsg: true
        }, () => {
            this.fetchBusinessVendors();
        });
    }

    handleEditConfirMmsgClose = () => {

        this.setState({
            editConfirMmsg: false
        });
    }

    handleAddConfirMmsg = () => {
        this.setState({
            addConfirMmsg: true,
        }, () => {
            this.fetchBusinessVendors();
        });

    }

    handleAddConfirMmsgClose = () => {
        this.setState({
            addConfirMmsg: false,
        });

    }

    componentDidMount() {
        this._isMounted = true;

        console.log('this.props.globalState', this.props.globalState.business.businessData.id);
        this.fetchBusinessVendors();
    }

    componentWillMount() {
        this._isMounted = false;
    }

    render() {
        return (
            <div className="boxBg border-0 p-t-15 p-b-15">

                <Row className="px-3 pt-3">
                    <Col sm={6} md={6}>

                    </Col>
                    <Col sm={6} md={6}>
                        {this.props.globalState.business.businessData.type === 'Fintainium Customer' ? <button className="btn btn-primary float-right" onClick={() => this.handelAddModal()}>Add Vendor</button> : null}
                    </Col>
                </Row>
                <Row className="show-grid p-3">
                    <Col sm={12} md={12}>
                            
                        <div className="boxBg">
                            <Table responsive hover>
                                <thead className="theaderBg">
                                    <tr>
                                        {/* <th></th> */}
                                        <th>
                                            <span className="float-left pr-2">Business Name </span>
                                            <span className="d-flex flex-column-reverse sortingFontSize">
                                                <button className="custom-btn-focus sortingArrow">
                                                    <FaCaretDown
                                                        className={'cursorPointer ' + (this.state.sortingActiveID == 2 ? 'activeColor' : '')}
                                                        onClick={() => {
                                                            this.fetchBusinessVendors(0, '', false, 'business');
                                                            this.sortingActive(2);
                                                            this.setState({ activePage: 1 });
                                                        }} />
                                                </button>
                                                <button className="custom-btn-focus sortingArrow">
                                                    <FaCaretUp
                                                        className={'cursorPointer ' + (this.state.sortingActiveID == 1 ? 'activeColor' : '')}
                                                        onClick={() => {
                                                            this.fetchBusinessVendors(0, '', true, 'business');
                                                            this.sortingActive(1);
                                                            this.setState({ activePage: 1 });
                                                        }} />
                                                </button>
                                            </span>
                                        </th>
                                        <th>
                                            <span className="float-left pr-2">
                                                Vendor Name
                                            </span>
                                            <span className="d-flex flex-column-reverse sortingFontSize">
                                                <button className="custom-btn-focus sortingArrow">
                                                    <FaCaretDown
                                                        className={'cursorPointer ' + (this.state.sortingActiveID == 4 ? 'activeColor' : '')}
                                                        onClick={() => {
                                                            this.fetchBusinessVendors(0, '', false, 'vendor');
                                                            this.sortingActive(4);
                                                            this.setState({ activePage: 1 });
                                                        }} />
                                                </button>
                                                <button className="custom-btn-focus sortingArrow">
                                                    <FaCaretUp
                                                        className={'cursorPointer ' + (this.state.sortingActiveID == 3 ? 'activeColor' : '')}
                                                        onClick={() => {
                                                            this.fetchBusinessVendors(0, '', true, 'vendor');
                                                            this.sortingActive(3);
                                                            this.setState({ activePage: 1 });

                                                        }} />
                                                </button>
                                            </span>
                                        </th>
                                        <th>
                                            <span className="float-left pr-2">
                                                Email Address
                                            </span>
                                            <span className="d-flex flex-column-reverse sortingFontSize">
                                                <button className="custom-btn-focus sortingArrow">
                                                    <FaCaretDown
                                                        className={'cursorPointer ' + (this.state.sortingActiveID == 6 ? 'activeColor' : '')}
                                                        onClick={() => {
                                                            this.fetchBusinessVendors(0, '', false, 'email');
                                                            this.sortingActive(6);
                                                            this.setState({ activePage: 1 });
                                                        }} />
                                                </button>
                                                <button className="custom-btn-focus sortingArrow">
                                                    <FaCaretUp
                                                        className={'cursorPointer ' + (this.state.sortingActiveID == 5 ? 'activeColor' : '')}
                                                        onClick={() => {
                                                            this.fetchBusinessVendors(0, '', true, 'email');
                                                            this.sortingActive(5);
                                                            this.setState({ activePage: 1 });
                                                        }} />
                                                </button>
                                            </span>
                                        </th>
                                        <th>
                                            <span className="float-left pr-2">
                                                Phone Number
                                            </span>
                                            {/* <span className="d-flex flex-column-reverse sortingFontSize">
                                            <button className="custom-btn-focus sortingArrow">
                                                <FaCaretDown
                                                    className={'cursorPointer ' + (this.state.sortingActiveID == 6 ? 'activeColor' : '')}
                                                    onClick={() => {
                                                        this.fetchBusinessVendors(0, '', false, 'email');
                                                        this.sortingActive(6);
                                                        this.setState({ activePage: 1 });
                                                    }} />
                                            </button>
                                            <button className="custom-btn-focus sortingArrow">
                                                <FaCaretUp
                                                    className={'cursorPointer ' + (this.state.sortingActiveID == 5 ? 'activeColor' : '')}
                                                    onClick={() => {
                                                        this.fetchBusinessVendors(0, '', true, 'email');
                                                        this.sortingActive(5);
                                                        this.setState({ activePage: 1 });
                                                    }} />
                                            </button>
                                        </span> */}
                                        </th>
                                        <th>
                                            <span className="float-left pr-2">
                                                Payment Method
                                            </span>
                                            <span className="d-flex flex-column-reverse sortingFontSize">
                                                <button className="custom-btn-focus sortingArrow">
                                                    <FaCaretDown
                                                        className={'cursorPointer ' + (this.state.sortingActiveID == 8 ? 'activeColor' : '')}
                                                        onClick={() => {
                                                            this.fetchBusinessVendors(0, '', false, 'payment');
                                                            this.sortingActive(8);
                                                            this.setState({ activePage: 1 });
                                                        }} />
                                                </button>
                                                <button className="custom-btn-focus sortingArrow">
                                                    <FaCaretUp
                                                        className={'cursorPointer ' + (this.state.sortingActiveID == 7 ? 'activeColor' : '')}
                                                        onClick={() => {
                                                            this.fetchBusinessVendors(0, '', true, 'payment');
                                                            this.sortingActive(7);
                                                            this.setState({ activePage: 1 });
                                                        }} />
                                                </button>
                                            </span>
                                        </th>
                                        <th>
                                            <span className="float-left pr-2">
                                                Date Added
                                            </span>
                                            <span className="d-flex flex-column-reverse sortingFontSize">
                                                <button className="custom-btn-focus sortingArrow">
                                                    <FaCaretDown
                                                        className={'cursorPointer ' + (this.state.sortingActiveID == 10 ? 'activeColor' : '')}
                                                        onClick={() => {
                                                            this.fetchBusinessVendors(0, '', false, 'createdDate');
                                                            this.sortingActive(10);
                                                            this.setState({ activePage: 1 });
                                                        }} />
                                                </button>
                                                <button className="custom-btn-focus sortingArrow">
                                                    <FaCaretUp
                                                        className={'cursorPointer ' + (this.state.sortingActiveID == 9 ? 'activeColor' : '')}
                                                        onClick={() => {
                                                            this.fetchBusinessVendors(0, '', true, 'createdDate');
                                                            this.sortingActive(9);
                                                            this.setState({ activePage: 1 });
                                                        }} />
                                                </button>
                                            </span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.loading ? (<tr>
                                        <td colSpan={12}>
                                            <LoadingSpinner />
                                        </td>
                                    </tr>) :
                                        this.state.vendorLists.length > 0 ? (
                                            this.state.vendorLists.map(vendorList => (

                                                (vendorList.archive === false ?
                                                    (<tr key={vendorList.id} onClick={() => this.handelviewEditModal(vendorList)}>
                                                        {/* <td>
                                                        <Dropdown>

                                                            <Dropdown.Toggle className="customActionButton">
                                                                <FiMenu />
                                                            </Dropdown.Toggle>

                                                            <Dropdown.Menu>
                                                                <Dropdown.Item onClick={() => this.editVendor(vendorList.id)}>
                                                                    Edit
                                                                </Dropdown.Item>
                                                                <Dropdown.Item onClick={() => this.handlerWarningDelete(vendorList.id)}>
                                                                    Delete
                                                                </Dropdown.Item>
                                                            </Dropdown.Menu>

                                                        </Dropdown>
                                                    </td> */}
                                                        <td>{this.props.globalState.business.businessData.type === 'Fintainium Customer' ? vendorList.businessName : vendorList.companyName}</td>
                                                        <td>{this.props.globalState.business.businessData.type === 'Fintainium Customer' ? vendorList.companyName : vendorList.businessName}</td>
                                                        <td>{vendorList.emailAddress}</td>
                                                        <td>{vendorList.phoneNumber}</td>
                                                        <td>{vendorList.paymentMode}</td>
                                                        <td>{vendorList.createdDate}</td>
                                                        {/* <td><MdLaunch className="cursor-pointer" /></td> */}
                                                        {/* <td>
                                                        <div className="showHideItem">
                                                            <div className="d-flex justify-content-end">
                                                                <LinkWithTooltip
                                                                    tooltip="edit vendor"
                                                                    id="vendor-edit"
                                                                >
                                                                    <div className="smIconBg"
                                                                        onClick={() => this.editVendor(vendorList.id)}>
                                                                        <Image src={editSmIcon} />
                                                                    </div>
                                                                </LinkWithTooltip>
                                                                <LinkWithTooltip
                                                                    tooltip="delete vendor"
                                                                    id="vendor-del"
                                                                >
                                                                    <div className="smIconBg ml-1"
                                                                        onClick={() => this.handlerWarningDelete(vendorList.id)}>
                                                                        <Image src={crossSmIcon} />
                                                                    </div>

                                                                </LinkWithTooltip>

                                                            </div>
                                                        </div>
                                                    </td> */}
                                                    </tr>) : null)


                                            ))
                                        )
                                            :
                                            this.state.fetchErrorMsg ? null : (
                                                <tr>
                                                    <td colSpan={12}>
                                                        <p className="text-center">No records found</p>
                                                    </td>
                                                </tr>
                                            )
                                    }
                                </tbody>
                            </Table>

                        </div>
                        
                    </Col>
                </Row>

                {this.state.totalCount ? (
                    <Row className="px-3">
                        <Col md={4} className="d-flex flex-row mt-20">
                            <span className="mr-2 mt-2 font-weight-500">Items per page</span>
                            <select
                                id={this.state.itemPerPage}
                                className="form-control truncatefloat-left w-90"
                                onChange={this.handleChangeItemPerPage}
                                value={this.state.itemPerPage}>
                                <option value='50'>50</option>
                                <option value='100'>100</option>
                                <option value='150'>150</option>
                                <option value='200'>200</option>
                                <option value='250'>250</option>

                            </select>
                        </Col>
                        <Col md={8}>
                            <div className="paginationOuter text-right">
                                <Pagination
                                    activePage={this.state.activePage}
                                    itemsCountPerPage={this.state.itemPerPage}
                                    totalItemsCount={this.state.totalCount}
                                    onChange={this.handlePageChange}
                                />
                            </div>
                        </Col>
                    </Row>
                ) : null}

                {/* Add Vendor Modal */}
                <Modal
                    show={this.state.addModal}
                    onHide={this.handelAddModalClose}
                    className="right half noPadding slideModal"
                >
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body className="">
                        <div className="modalHeader">
                            <Row>
                                <Col md={9}>
                                    <h1>Add Vendor</h1>
                                </Col>
                            </Row>
                        </div>
                        <div className="modalBody content-body noTabs">
                            <AddBusinessVendors
                                handleAddConfirMmsg={this.handleAddConfirMmsg}
                                businessId={this.props.globalState.business.businessData.id}
                                handelAddModalClose={this.handelAddModalClose}
                            />
                        </div>
                    </Modal.Body>

                </Modal>

                {/* Edit Vendor Modal */}
                <Modal
                    show={this.state.viewEditModal}
                    onHide={this.handelviewEditModalClose}
                    className="right full noPadding slideModal"
                >
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body className="">
                        <div className="modalHeader">
                            <Row>
                                <Col md={9}>
                                    <h1>Edit Vendor</h1>
                                </Col>
                            </Row>
                        </div>
                        <div className="modalBody content-body noTabs">
                            <EditBusinessVendors
                                {...this.state.vendorData}
                                handelviewEditModalClose={this.handelviewEditModalClose}
                                handleEditConfirMmsg={this.handleEditConfirMmsg} />
                        </div>
                    </Modal.Body>

                </Modal>

                {/*====== Edit confirmation popup  ===== */}
                <Modal
                    show={this.state.editConfirMmsg}
                    onHide={this.handleEditConfirMmsgClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Vendor has been successfully edited</h5>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <Button
                                    onClick={this.handleEditConfirMmsgClose}
                                    className="but-gray"
                                >
                                    Return
                                </Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                {/*======  Add confirmation popup  ===== */}
                <Modal
                    show={this.state.addConfirMmsg}
                    onHide={this.handleAddConfirMmsgClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Vendor has been successfully Added</h5>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <Button
                                    onClick={this.handleAddConfirMmsgClose}
                                    className="but-gray"
                                >
                                    Return
                                </Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

            </div>
        );
    }

}



const mapStateToProps = state => {
    return {
        globalState: state
    };
};

BusinessVendors.propTypes = {
    globalState: PropTypes.object,
};

export default connect(mapStateToProps, null)(BusinessVendors);
