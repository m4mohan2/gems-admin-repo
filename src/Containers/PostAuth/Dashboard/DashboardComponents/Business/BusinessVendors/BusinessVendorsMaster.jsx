/* eslint-disable no-unused-vars */
import React, {
    Component,
    //Fragment 
} from 'react';
import { connect } from 'react-redux';
//import axios from './../../../../../../shared/eaxios';
import PropTypes from 'prop-types';
// import LoadingSpinner from './../../../../../../Components/LoadingSpinner/LoadingSpinner';

import {
    // Table,
    // //Dropdown,
    // Row,
    // Modal,
    // Col,
    // Image,
    // Button,
    Tabs,
    Tab,
} from 'react-bootstrap';
// import { FaCaretDown } from 'react-icons/fa';
// import { FaCaretUp } from 'react-icons/fa';
// import SuccessIco from './../../../../../../assets/success-ico.png';
// //import { FiMenu } from 'react-icons/fi';
// import Pagination from 'react-js-pagination';
// import AddBusinessVendors from './AddBusinessVendors';
// import EditBusinessVendors from './EditBusinessVendors';
import BusinessVendors from './BusinessVendors';
import EmailManagement from './EmailManagement';

class BusinessVendorsMaster extends Component {

    state = {
        // activePage: 1,
        // totalCount: 0,
        // itemPerPage: 250,
        // sort: null,
        // field: null,
        // fetchErrorMsg: null,
        // vendorLists: [],
        // loading: true,
        // sortingActiveID: 1,
        // addModal: false,
        // viewEditModal: false,
        // vendorData: {},
        // editConfirMmsg: false,
        // addConfirMmsg: false,

        key: 'vendorlist',

    }

    _isMounted = false;

    // displayError = (e) => {
    //     let errorMessge = '';
    //     try {
    //         errorMessge = e.data.message ? e.data.message : e.data.error_description;
    //     } catch (e) {
    //         errorMessge = 'Access is denied!';
    //     }

    //     return errorMessge;
    // }
    // sortingActive = (id) => {
    //     this.setState({
    //         sortingActiveID: id
    //     }, () => { console.log('this.state.sortingActiveID', this.state.sortingActiveID); });
    // }
    // fetchBusinessVendors = (
    //     since = 0, // Pagination strating from --1
    //     filter = '', //  serch by keyword like : name, ph --3
    //     sort = '', // sort with a = true, d =flase --4
    //     field = '', // sort with field name --5
    // ) => {
    //     this.setState({
    //         loading: true,
    //         sort: sort,
    //         field: field
    //     }, () => {
    //         axios
    //             .get(

    //                 `customerVendor/vendorlist/${this.props.globalState.business.businessData.id}?since=${since}&limit=${this.state.itemPerPage}&key=${filter}&direction=${this.state.sort}&prop=${this.state.field}&businessType=${this.props.globalState.business.businessData.type}`

    //                 //http://localhost:7090/admin/customerVendor/customer/business/15847?since&limit&prop=&key=&direction
    //                 //`customerVendor/vendorlist/9740?since=${since}&limit=${this.state.itemPerPage}&key=${filter}&direction=${this.state.sort}&prop=${this.state.field}`
    //             )
    //             .then(res => {
    //                 const vendorLists = res.data.entries;
    //                 const totalCount = res.data.total;

    //                 if (this._isMounted) {
    //                     this.setState({
    //                         vendorLists,
    //                         totalCount: totalCount,
    //                         loading: false
    //                     });
    //                 }

    //             })
    //             .catch(e => {
    //                 let errorMsg = this.displayError(e);
    //                 this.setState({
    //                     fetchErrorMsg: errorMsg,
    //                     loading: false
    //                 }, () => {
    //                     console.log('this.state.fetchErrorMsg', this.state.fetchErrorMsg);
    //                 });

    //                 setTimeout(() => {
    //                     this.setState({ fetchErrorMsg: null });
    //                 }, 5000);
    //             });
    //     });
    // }


    // handlePageChange = pageNumber => {
    //     this.setState({ activePage: pageNumber });
    //     this.fetchBusinessVendors(pageNumber > 0 ? pageNumber - 1 : 0, '', this.state.sort, this.state.field);
    // };

    // resetPagination = () => {
    //     this.setState({ activePage: 1 });
    // }


    // handleChangeItemPerPage = (e) => {
    //     this.setState({ itemPerPage: e.target.value },
    //         () => {
    //             this.fetchBusinessVendors(this.state.activePage > 0 ? this.state.activePage - 1 : 0, '', this.state.sort, this.state.field);
    //         });
    // }



    // handelAddModal = () => {
    //     this.setState({
    //         addModal: true,
    //     });
    // }

    // handelAddModalClose = () => {
    //     this.setState({
    //         addModal: false,
    //     });
    // }


    // handelviewEditModal = (data) => {
    //     //console.log('this.state.viewEditData.businessId', this.state.vendorData.businessId);
    //     this.setState({
    //         viewEditModal: true,
    //         vendorData: data
    //     }, () => {
    //         console.log('this.state.viewEditData@@', this.state.vendorData);
    //     });
    // }

    // handelviewEditModalClose = () => {
    //     this.setState({
    //         viewEditModal: false,
    //     });
    // }

    // handleEditConfirMmsg = () => {
    //     this.setState({
    //         viewEditModal: false,
    //         editConfirMmsg: true
    //     }, () => {
    //         this.fetchBusinessVendors();
    //     });
    // }

    // handleEditConfirMmsgClose = () => {

    //     this.setState({
    //         editConfirMmsg: false
    //     });
    // }

    // handleAddConfirMmsg = () => {
    //     this.setState({
    //         addConfirMmsg: true,
    //     }, () => {
    //         this.fetchBusinessVendors();
    //     });

    // }

    // handleAddConfirMmsgClose = () => {
    //     this.setState({
    //         addConfirMmsg: false,
    //     });

    // }

    componentDidMount() {
        this._isMounted = true;

        console.log('this.props.globalState businessVendorMaster', this.props.globalState.business.businessData.id);
        // this.fetchBusinessVendors();
    }

    componentWillMount() {
        this._isMounted = false;
    }

    render() {
        return (
            <div>
                <Tabs
                    id="controlled-tab-example"
                    activeKey={this.state.key}
                    onSelect={key => this.setState({ key })}
                    className="subTab px-4 subTabBg"
                >
                    <Tab eventKey="vendorlist" title="Vendor list">
                        <BusinessVendors/>
                    </Tab>
                    <Tab eventKey="emailmanagement" title="Email Management">
                        <EmailManagement businessId={this.props.globalState.business.businessData.id}/>
                    </Tab>
                </Tabs>    
                
            </div>
        );
    }

}



const mapStateToProps = state => {
    return {
        globalState: state
    };
};

BusinessVendorsMaster.propTypes = {
    globalState: PropTypes.object,
};

export default connect(mapStateToProps, null)(BusinessVendorsMaster);
