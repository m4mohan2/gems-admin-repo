import React, { Component, Fragment } from 'react';
import LoadingSpinner from '../../../../../../Components/LoadingSpinner/LoadingSpinner';
import { Col, Row, Button, Modal, Image, Table} from 'react-bootstrap';
import SuccessIco from './../../../../../../assets/success-ico.png';
import Pagination from 'react-js-pagination';
import axios from '../../../../../../shared/eaxios';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import PropTypes from 'prop-types';
// import {
//     ITEMPERPAGE
// } from '../../../../constants';
//import ItemPerPage from '../../../../shared/ItemPerPage';

const initialValues = {
    emailDNS:'',
};

const emailDNSSchema = Yup.object().shape({
    emailDNS: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter  domain/email'),

});

class EmailManagement extends Component {
    state = {
        EmailManagementHistory: [],
        loadingEmailManagement: false,
        errorEmailManagement: '',
        statusChange: false,
        statusChangeTxt:null,
        statusChangeFlag: null,
        statusConfirMmsg: false,
        statuserrorMsg:null,
        successMessage:null,
        activePage: 1,
        totalCount: 0,
        itemPerPage: 250,
        deleteObj:{},
        addObj:{},
        
    }

  
  handleStatusChangedClose = () => {
      this.setState({
          statusConfirMmsg: false,
          successMessage: null
      });
  }  
    
  handleAddBlacklist = (values) => { 

      let emailDNSObj = {
          'emailDNS': values.emailDNS,
          'isArchive': false
      };

      axios
          .post(`/business/saveBlacklistedEmail/${this.props.businessId}`, emailDNSObj)
          .then(res => {
              console.log('saveBlacklistedEmail re', res);
              values.emailDNS = '';
              this.setState({
                  statusChange: false,
                  successMessage: 'Record has been successfully added',
                  statusConfirMmsg: true
              });
              this.fetchEmailManagement();

          })
          .catch(err => {
              values.emailDNS = '';
              let statuserrorMsg = '';
              try {
                  statuserrorMsg = err.data.message ? err.data.message : err.data.errorInfo;
                  statuserrorMsg = err.data.error_description ? err.data.error_description : err.data.errorInfo;
              } catch (err) {
                  statuserrorMsg = 'No records found.';
              }
              this.setState({
                  loadingEmailManagement: false,
                  statuserrorMsg
              });
              setTimeout(() => {
                  this.setState({
                      statuserrorMsg: null,
                      statusChange: false,
                      statusChangeTxt: null,
                      statusChangeFlag: null,
                  });
              }, 2500);
          });


  }  
  handleSubmit = values => {

      this.setState({
          statusChangeFlag: 'ADD',
          statusChange: true,
          addObj: values,
          statusChangeTxt:'Do you want to add this list ?'
      });
    

  }  

  handleRemoveHide =()=>{
      this.setState({
          statusChange: false,
          statusChangeTxt:null
      });
  }

  handleRemoveFromBlackList = values => {
      axios
          .delete(`/business/blacklistedEmail/${values.id}/${this.props.businessId}`)
          .then(res => {
              console.log('del BlacklistedEmail re', res);
        
              this.setState({
                  statusChange: false,
                  successMessage:'Record has been successfully deleted',
                  statusConfirMmsg:true
              });
              this.fetchEmailManagement();
          })
          .catch(err => {
              let statuserrorMsg = '';
              try {
                  statuserrorMsg = err.data.message ? err.data.message : err.data.errorInfo;
                  statuserrorMsg = err.data.error_description ? err.data.error_description : err.data.errorInfo;
              } catch (err) {
                  statuserrorMsg = 'No records found.';
              }
              this.setState({
                  loadingEmailManagement: false,
                  statuserrorMsg
              });
              setTimeout(() => {
                  this.setState({ 
                      statuserrorMsg: null, 
                      statusChange: false,
                      statusChangeTxt: null,
                      statusChangeFlag: null, 
                  });
              }, 2500);
          });

  }

  handleRemove = values =>{
      this.setState({
          statusChangeFlag:'REMOVE',
          statusChange: true,
          deleteObj:values,
          statusChangeTxt: 'Do you want to remove this list ?'
      });

  }  

    // PAGE NAVIGATE
    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber },()=>{
            this.fetchEmailManagement(this.state.activePage > 0 ? this.state.activePage - 1 : 0,);
        });
        
    };

  handleChangeItemPerPage = (e) => {
      this.setState({ itemPerPage: e.target.value },
          () => {
              this.fetchEmailManagement(this.state.activePage > 0 ? this.state.activePage - 1 : 0,);
          });
  }

    // BILLING HISTORY
    fetchEmailManagement = (since = 0) => {
        this.setState({
            loadingEmailManagement: true
        }, () => {
            axios
                .get(`/business/blacklistedEmail/${this.props.businessId}?since=${since}&limit=${this.state.itemPerPage}&key=`)
                .then(res => {
                    const EmailManagementHistory = res.data.entries;
                    const totalCount = res.data.total;
                    this.setState({
                        EmailManagementHistory,
                        loadingEmailManagement: false,
                        totalCount
                    });
                })
                .catch(err => {
                    let errorEmailManagement = '';
                    try {
                        errorEmailManagement = err.data.message ? err.data.message : err.data.errorInfo;
                        errorEmailManagement = err.data.error_description ? err.data.error_description : err.data.errorInfo;

                    } catch (err) {
                        errorEmailManagement = 'No records found.';
                    }
                    this.setState({
                        loadingEmailManagement: false,
                        errorEmailManagement
                    });
                    setTimeout(() => {
                        this.setState({ errorEmailManagement: null });
                    }, 2500);
                });
        });

    }

 
    componentDidMount() {
        this.fetchEmailManagement();
    }

    render() {

        const {
            errorEmailManagement,
            loadingEmailManagement,
            EmailManagementHistory,
            // activePage,
            // itemPerPage,
            // totalCount
        } = this.state;

        return (
            <div className="billingTab EmailManagementHistoryModuleOuter p-3">
                <p>Store Domain and email addresses you want to be ignored</p>
                <div className="equal">

            
                    <Formik
                        initialValues={initialValues}
                        validationSchema={emailDNSSchema}
                        onSubmit={this.handleSubmit}
                        enableReinitialize={true}
                    >
                        {({
                            values,
                            // setFieldValue,
                            // handleChange,
                            // handleBlur,
                            errors,
                            touched
                        }) => {
                            const { emailDNS } = values;
                            return (
                                <div className='topSearchInvoice'>

                                    <Form className="form-inline pb-3">
                                        <div className="d-flex flex-row bd-highlight mb-3">
                                            <div className='form-group'>Enter Domain/Email here <span className="required">*</span></div>
                                            <div className='form-group m-l-10'>
                                                <Field
                                                    name="emailDNS"
                                                    type="text"
                                                    className={'form-control'}
                                                    autoComplete="nope"
                                                    placeholder="Enter"
                                                    value={emailDNS}
                                                />
                                                {errors.emailDNS && touched.emailDNS ? (
                                                    <div className='w-100 position-relative'><span className="errorMsg pl-3">{errors.emailDNS}</span></div>
                                                ) : null}
                                            </div>
                                            <div className='form-group m-l-20'>
                                                <button className="btn btn-primary" type="submit" >
                                                    Save
                                                </button>

                                            </div>
                                        </div>
                                        
                                    </Form>

                                </div>
                  
                            );
                        }}
                    </Formik>
                </div>
                
                <div className="boxBg">
                    <Table responsive hover>

                        <thead className="theaderBg">
                            <tr>
                                <th><span className="">Blacklisted DNS/Emails </span></th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            {errorEmailManagement ? (
                                <tr>
                                    <td colSpan={5}>
                                        <div className="alert alert-danger" role="alert">
                                            <p>{errorEmailManagement}</p>
                                        </div>
                                    </td>
                                </tr>
                            ) :
                                loadingEmailManagement ? (
                                    <Fragment>
                                        <tr>
                                            <td colSpan={5}>
                                                <LoadingSpinner />
                                            </td>
                                        </tr>
                                    </Fragment>
                                ) :
                                    EmailManagementHistory && EmailManagementHistory.length > 0 ? EmailManagementHistory.map(EmailManagement =>
                                        <tr key={EmailManagement.id}>
                                            <td><span className="">{EmailManagement.emailDNS}</span></td>
                                        
                                            <td className="text-right downloadLink">
                                                <Button className="btn btn-primary" onClick={() => this.handleRemove(EmailManagement)}>
                                              Remove
                                                </Button>
                                            </td>
                                        </tr>
                                    ) : errorEmailManagement ? null :
                                        <tr>
                                            <td colSpan={5} >
                                                <p className="text-center">No records found</p>
                                            </td>
                                        </tr>
                            }

                        </tbody>
                        
                    </Table>
                </div>    


                {this.state.totalCount ? (
                    <Row>
                        <Col md={4} className="d-flex flex-row mt-20">
                            <span className="mr-2 mt-2 font-weight-500">Items per page</span>
                            <select
                                id={this.state.itemPerPage}
                                className="form-control truncatefloat-left w-90"
                                onChange={this.handleChangeItemPerPage}
                                value={this.state.itemPerPage}>
                                <option value='50'>50</option>
                                <option value='100'>100</option>
                                <option value='150'>150</option>
                                <option value='200'>200</option>
                                <option value='250'>250</option>

                            </select>
                        </Col>
                        <Col md={8}>
                            <div className="paginationOuter text-right">
                                <Pagination
                                    activePage={this.state.activePage}
                                    itemsCountPerPage={this.state.itemPerPage}
                                    totalItemsCount={this.state.totalCount}
                                    onChange={this.handlePageChange}
                                />
                            </div>
                        </Col>
                    </Row>
                ) : null}

                <Modal
                    show={this.state.statusChange}
                    onHide={this.handleHide}
                    dialogClassName="modal-90w"
                    aria-labelledby="example-custom-modal-styling-title"
                >
                    <Modal.Body>
                        <div className="m-auto text-center">
                            <h6 className="py-2">{this.state.statusChangeTxt}</h6>
                        </div>
                        {this.state.statuserrorMsg ? <div className="alert alert-danger my-3 text-center col-12" role="alert">{this.state.statuserrorMsg}</div> : null}
                        <div className="m-auto text-center">
                            <button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.handleRemoveHide()}>Return</button>
                            {this.state.statuserrorMsg == null && this.state.statusChangeFlag==='REMOVE' ? <button className="btn btn-danger m-l-10" onClick={() => this.handleRemoveFromBlackList(this.state.deleteObj)}>Confirm</button> : null}
                            {this.state.statuserrorMsg == null && this.state.statusChangeFlag === 'ADD' ? <button className="btn btn-primary m-l-10" onClick={() => this.handleAddBlacklist(this.state.addObj)}>Confirm</button> : null}
                        </div>

                    </Modal.Body>
                </Modal>

                {/*====== Status change confirmation popup  ===== */}
                <Modal
                    show={this.state.statusConfirMmsg}
                    onHide={this.handleStatusChangedClose}
                    className="payOptionPop"
                >
                    <Modal.Body className="text-center">
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>{this.state.successMessage}</h5>
                            </Col>
                        </Row>
                        <Button
                            onClick={this.handleStatusChangedClose}
                            className="but-gray mt-3"
                        >
                            Return
                        </Button>
                    </Modal.Body>

                </Modal>
            </div>
        );
    }
}
EmailManagement.propTypes = {
    businessId: PropTypes.string,
};    

export default EmailManagement;
