import { Field, Form, Formik } from 'formik';
import React, { Component } from 'react';
import { 
    Button, 
    Col, 
    FormControl, 
    FormGroup, 
    Image, 
    Modal, 
    Row 
} from 'react-bootstrap';
import InputMask from 'react-input-mask';
import * as Yup from 'yup';
import { CountryService } from './../../../../../../services/country.service';
import axios from './../../../../../../shared/eaxios';
import SuccessIco from './../../../../../../assets/success-ico.png';
//import './../../customer/add-customer/AddCustomer.scss';
import LoadingSpinner from './../../../../../../Components/LoadingSpinner/LoadingSpinner';

import PropTypes from 'prop-types';


const initialValues = {
    companyName: '',
    taxNo: '',
    firstName: '',
    lastName: '',
    paymentMode: 'CHECK',
    emailAddress: '',
    phoneNumber: '',
    //address: '',
    addressLine1: '',
    addressLine2: '',
    addressLine3: '',
    addressLine4: '',
    addressLine5: '',
    printName: '',
    state: '',
    countryCode: '',
    stateCode: '',
    zipCode: '',
    cityId: '',
    city: '',
};

const addCustomerSchema = Yup.object().shape({
    companyName: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter company name'),
    taxNo: Yup
        .string()
        //.min(10, 'taxNo must be at least 9 characters'),
        .test(
            'taxtValidNo',
            'Please enter minimum 9 digit ',
            function (val) {
                if (val) {
                    // eslint-disable-next-line no-useless-escape
                    const rValue = val.replace(/\-/, '');
                    if ((rValue.length) < 9) {
                        return false;
                    }
                }
                return true;
            }
        ),
        
    //.required('Please enter business tax ID/SSN'),
    firstName: Yup
        .string()
        .trim('Please remove whitespace')
        .required('Please enter first name')
        .strict(),
    printName: Yup
        .string()
        .trim('Please remove whitespace')
        //.required('Please enter the details')
        .strict(),
    lastName: Yup
        .string()
        .trim('Please remove whitespace')
        .required('Please enter last name')
        .strict(),
    paymentMode: Yup.string()
        .notOneOf(['']),
    emailAddress: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .email('Email must be valid'),
    phoneNumber: Yup.string().min(14, 'Phone number must be 10 digit'),
    // address: Yup
    //     .string()
    //     .trim('Please remove whitespace')
    //     .strict()
    //     .required('Please enter address'),
    
    addressLine1: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter address line 1')
        .max(40, 'maximum characters length 40'),
    addressLine2: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .max(40, 'maximum characters length 40'),
    addressLine3: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .max(40, 'maximum characters length 40'),
    addressLine4: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .max(40, 'maximum characters length 40'),
    addressLine5: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .max(40, 'maximum characters length 40'),

    countryCode: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please select country'),
    zipCode: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .test(
            'zipCode',
            'Please enter a valid zip code',

            /*====================
            ZIP CODE VALIDATION COMBINATION
            1) minimum 5 digit
            2) max 9 digit
            3) space after 5 digit
            4) hiphen (-) after 5 digit
            5) after 5 digit then need to give space or hiphen 
            6) alpha numeric condition like-   A23 W45
            =========================
            */

            function (value) {
                if (value) {
                    let isValid = false;
                    // eslint-disable-next-line no-useless-escape
                    if (/^\d{5}([\-|\s]\d{1,4})?$/.test(value)) {
                        isValid = true;
                    } else if (/^[a-zA-Z0-9]{3}\s[a-zA-Z0-9]{3}$/.test(value)) {
                        let expArr = value.split(' ');

                        if (/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{3}$/.test(expArr[0]) && /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{3}$/.test(expArr[1])) {
                            isValid = true;
                        }
                    }
                    return isValid;
                }

            }
        )
        .required('Please enter zip code'),
    stateCode: Yup
        .string()
        .required('Please select state'),
    cityId: Yup
        .number()
        .required('Please select city')
});

class AddBusinessVendors extends Component {
    state = {
        files: [],
        dropzoneActive: false,
        isDrop: false,
        countries: [],
        states: [],
        cities: [],
        selectedStates: [],
        selectedCities: [],
        showConfirMmsg: false,
        errorMessge: null,
        userRole: [],
        csvDownlaodLink: '',
        addVendorLoader: false,
        addErrorMessge: null,
    };

    handleChange = (e, field) => {
        this.setState({
            [field]: e.target.value
        });
    };

    handleConfirmReviewClose = () => {
        this.setState({ showConfirMmsg: false });
    };
    handleConfirmReviewShow = () => {
        this.setState({ showConfirMmsg: true });
    };

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {

            errorMessge = 'Access is denied!';
        }

        return errorMessge;
    }

    handleSubmit = (values, { resetForm }) => {
        this.setState({
            addVendorLoader: true,
        });    
        console.log('values 1 ', values, this.state.states);

        const formData = new window.FormData();
        for (const key in values) {
            if (values.hasOwnProperty(key)) {
                formData.append(key, values[key]);
            }
        }

        const prevStateCode = values.stateCode;
        for (const st of this.state.states) {
            if (parseInt(st.id) === parseInt(values.stateCode)) values.stateCode = st.stateCode;
        }

        const prevCountryCode = values.countryCode;
        for (const country of this.state.countries) {
            if (parseInt(country.id) === parseInt(values.countryCode)) values.countryCode = country.countryCode;
        }

        let newValue = {
            companyName: values.companyName,
            //taxNo: values.taxNo,
            taxNo: values.taxNo ? values.taxNo.split('-').join('') : null,
            firstName: values.firstName,
            lastName: values.lastName,
            emailAddress: values.emailAddress,
            phoneNumber: values.phoneNumber,
            paymentMode: values.paymentMode,
            //address: values.address,
            printName: values.printName,
            addressLine1: values.addressLine1,
            addressLine2: values.addressLine2,
            addressLine3: values.addressLine3,
            addressLine4: values.addressLine4,
            addressLine5: values.addressLine5,
            zipCode: values.zipCode,
            countryCode: values.countryCode,
            stateCode: values.stateCode,
            cityId: values.cityId,

            status: true,
            isCustomer: false,
            archive: false

        };
        
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        };

        axios
            .post(`/customerVendor/create/${this.props.businessId}`, newValue, config)
            .then(res => {
                console.log(res);
                console.log('Vendor data', res.data);
                resetForm({});
                //this.setState({ showConfirMmsg: true });
                this.props.handelAddModalClose();
                this.props.handleAddConfirMmsg();
            }).catch(e => {
                values.stateCode= prevStateCode;
                values.countryCode = prevCountryCode;

                let errorMsg = this.displayError(e);
                this.setState({
                    addVendorLoader: false,
                    addErrorMessge: errorMsg,
                });

                setTimeout(() => {
                    this.setState({ deleteErrorMessge: null });
                }, 5000);

            });
            
    };

    onDrop = files => {
        console.log(files[0]);
        this.setState({
            files,
            dropzoneActive: false,
            isDrop: true
        });
        const config = {
            headers: {
                //sessionKey: localStorage.getItem("sessionKey"),
                'Content-Type': 'multipart/form-data'
            }
        };
        const formData = new window.FormData();
        formData.append('file', files[0]);
        axios
            .post('/customerVendor/parseCSV/customer', formData, config)
            .then(res => {
                console.log(res);
                this.setState({ showConfirMmsg: true });
            })
            .catch(this.displayError);
    };

    onDragEnter = () => {
        this.setState({
            dropzoneActive: true
        });
    };

    onDragLeave = () => {
        this.setState({
            dropzoneActive: false
        });
    };

    // // PERMISION
    // userPermission = () => {
    //     const user = new UserBusiness();
    //     user.getPermissions().then(arr => {
    //         this.setState({ userRole: arr });
    //         //console.log('Permission array ', this.state.userRole);
    //     });
    // }

    // SAMPLE CSV DOWNLOAD LINK
    // csvDownload = () => {
    //     const hostName = process.env.REACT_APP_HOST_URL;
    //     const csvDownlaodLink = hostName + '/vendor.csv';
    //     console.log('csv dwonload link', csvDownlaodLink);
    //     this.setState({
    //         csvDownlaodLink
    //     });
    // }


    populateState = e => {
        const countryId = e.target.value;
        const states = this.state.states;
        const newStates = states.filter(state => {
            return Number(state.countryId) === Number(countryId);
        });

        this.setState({ selectedStates: newStates, selectedCities: [] });
        console.log(this.state);
    };

    populateCity = e => {
        const stateId = e.target.value;
        const cities = this.state.cities;
        const newCities = cities.filter(city => {
            return Number(city.stateId) === Number(stateId);
        });
        this.setState({ selectedCities: newCities });
        console.log(this.state.selectedCities);
    };

    componentDidMount() {
        const countryService = new CountryService();
        countryService.getData().then(({ countries, states, cities }) => { //countries, states or cities
            this.setState({ countries, states, cities });
        });
        //this.userPermission();
        //this.csvDownload();
    }

    render() {


        const {
            selectedCities,
            selectedStates,
        } = this.state;
        // const overlayStyle = {
        //     position: 'absolute',
        //     top: 0,
        //     right: 0,
        //     bottom: 0,
        //     left: 0,
        //     padding: '2.5em 0',
        //     background: 'rgba(0,0,0,0.2)',
        //     border: '2px dotted rgba(0,0,0,0.5)',
        //     textAlign: 'center',
        //     color: '#fff'
        // };
        return (
            <div className="addcustomerSec">

                <div className="addVendorOuter">
                    {this.state.addErrorMessge ? (
                        <div className="alert alert-danger" role="alert">
                            {this.state.addErrorMessge}
                        </div>
                    ) : null}

                    <Row>
                        <Col sm={12}>
                            {this.state.addVendorLoader?<LoadingSpinner/>:null}
                        </Col>
                    </Row>    
                    <Row className="show-grid">

                        <Col xs={12} className="brd-right">
                            <Formik
                                initialValues={initialValues}
                                validationSchema={addCustomerSchema}
                                onSubmit={this.handleSubmit}
                            >
                                {({
                                    values,
                                    errors,
                                    touched,
                                    //isSubmitting,
                                    handleChange,
                                    handleBlur,
                                    //setFieldValue
                                }) => {
                                    return (
                                        <Form>
                                            <Row className="show-grid">
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <label>Company Name <span className="required">*</span></label>
                                                        <Field
                                                            name="companyName"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.companyName || ''}
                                                            autoFocus
                                                        />
                                                        {errors.companyName && touched.companyName ? (
                                                            <span className="errorMsg ml-3">
                                                                {errors.companyName}
                                                            </span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                            
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <label>Business Tax ID/SSN </label>
                                                        <InputMask
                                                            mask="99-99999999"
                                                            maskChar={null}
                                                            type="text"
                                                            name="taxNo"
                                                            className={'form-control'}
                                                            value={values.taxNo || ''}
                                                            onChange={handleChange}
                                                            onBlur={handleBlur}
                                                        />
                                                        {errors.taxNo && touched.taxNo ? (
                                                            <span className="errorMsg ml-3">{errors.taxNo}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                        {/* <HelpBlock>Validation Text</HelpBlock> */}
                                                    </FormGroup>
                                                </Col>
                                          
                                                {/* <Col xs={12} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Company Street Address <span className="required">*</span></label>
                                                        <Field
                                                            name="address"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.address || ''}
                                                        />
                                                        {errors.address && touched.address ? (
                                                            <span className="errorMsg ml-3">{errors.address}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col> */}
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Address Line 1 <span className="required">*</span></label>
                                                        <Field
                                                            name="addressLine1"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.addressLine1 || ''}
                                                        />
                                                        {errors.addressLine1 && touched.addressLine1 ? (
                                                            <span className="errorMsg ml-3">{errors.addressLine1}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Address Line 2 
                                                        </label>
                                                        <Field
                                                            name="addressLine2"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.addressLine2 || ''}
                                                        />
                                                        {errors.addressLine2 && touched.addressLine2 ? (
                                                            <span className="errorMsg ml-3">{errors.addressLine2}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Address Line 3
                                                        </label>
                                                        <Field
                                                            name="addressLine3"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.addressLine3 || ''}
                                                        />
                                                        {errors.addressLine3 && touched.addressLine3 ? (
                                                            <span className="errorMsg ml-3">{errors.addressLine3}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Address Line 4
                                                        </label>
                                                        <Field
                                                            name="addressLine4"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.addressLine4 || ''}
                                                        />
                                                        {errors.addressLine4 && touched.addressLine4 ? (
                                                            <span className="errorMsg ml-3">{errors.addressLine4}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formControlsTextarea">
                                                        <label>Address Line 5</label>
                                                        <Field
                                                            name="addressLine5"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.addressLine5 || ''}
                                                        />
                                                        {errors.addressLine5 && touched.addressLine5 ? (
                                                            <span className="errorMsg ml-3">{errors.addressLine5}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <label>Country <span className="required">*</span></label>
                                                        <Field
                                                            name="countryCode"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            value={values.countryCode || ''}
                                                            onChange={e => {
                                                                handleChange(e);
                                                                this.populateState(e);
                                                            }}
                                                        >
                                                            <option value="" selected="selected">
                                                                {this.state.countries.length ? 'Select Country' : 'Loading...'}
                                                            </option>

                                                            {this.state.countries.map(country => (
                                                                <option key={country.id} value={country.id}>
                                                                    {country.countryName}
                                                                </option>
                                                            ))}
                                                        </Field>

                                                        {errors.countryCode && touched.countryCode ? (
                                                            <span className="errorMsg ml-3">{errors.countryCode}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                           
                                                <Col xs={12} md={6} className="cityField">
                                                    <FormGroup controlId="formBasicText">
                                                        <label>State <span className="required">*</span></label>
                                                        <Field
                                                            name="stateCode"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            value={values.stateCode || ''}
                                                            onChange={e => {
                                                                handleChange(e);
                                                                this.populateCity(e);
                                                            }}
                                                            disabled={
                                                                this.state.selectedStates.length
                                                                    ? false
                                                                    : true
                                                            }
                                                        >
                                                            <option value="" selected="selected">
                                                                Select State
                                                            </option>
                                                            {
                                                                selectedStates.map(stName => (
                                                                    <option value={stName.id} key={stName.id}>
                                                                        {stName.stateName}
                                                                    </option>
                                                                ))}
                                                        </Field>
                                                        {errors.stateCode && touched.stateCode ? (
                                                            <span className="errorMsg ml-3">{errors.stateCode}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={6} className="cityField">
                                                    <FormGroup controlId="formBasicText">
                                                        <label>City <span className="required">*</span></label>

                                                        <Field
                                                            name="cityId"
                                                            component="select"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="select"
                                                            value={values.cityId || ''}
                                                            disabled={
                                                                this.state.selectedCities.length
                                                                    ? false
                                                                    : true
                                                            }
                                                        >
                                                            <option value="" selected="selected">
                                                                Select City
                                                            </option>
                                                            {
                                                                selectedCities.map(cityName => (
                                                                    <option value={cityName.id} key={cityName.id}>
                                                                        {cityName.cityName}
                                                                    </option>
                                                                ))}
                                                        </Field>
                                                        {errors.cityId && touched.cityId ? (
                                                            <span className="errorMsg ml-3">{errors.cityId}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                            
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <label>Zip Code <span className="required">*</span></label>
                                                        <Field
                                                            name="zipCode"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.zipCode || ''}
                                                        />
                                                        {errors.zipCode && touched.zipCode ? (
                                                            <span className="errorMsg ml-3">{errors.zipCode}</span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                            
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <label>First Name <span className="required">*</span></label>
                                                        <Field
                                                            name="firstName"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.firstName || ''}
                                                        />
                                                        {errors.firstName && touched.firstName ? (
                                                            <span className="errorMsg ml-3">
                                                                {errors.firstName}
                                                            </span>
                                                        ) : null}

                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <label>Last Name <span className="required">*</span></label>
                                                        <Field
                                                            name="lastName"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.lastName || ''}
                                                        />
                                                        {errors.lastName && touched.lastName ? (
                                                            <span className="errorMsg ml-3">
                                                                {errors.lastName}
                                                            </span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                            
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <label>Work Email</label>
                                                        <Field
                                                            name="emailAddress"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.emailAddress || ''}
                                                        />
                                                        {errors.emailAddress && touched.emailAddress ? (
                                                            <span className="errorMsg ml-3">
                                                                {errors.emailAddress}
                                                            </span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <label>Phone</label>
                                                        <InputMask
                                                            mask="1 999-999-9999"
                                                            maskChar={null}
                                                            type="tel"
                                                            name="phoneNumber"
                                                            placeholder="Enter"
                                                            className={`form-control ${values.phoneNumber &&
                                                                'input-elem-filled'}`}
                                                            onChange={handleChange}
                                                            onBlur={handleBlur}
                                                            value={values.phoneNumber || ''}
                                                        />
                                                        {errors.phoneNumber && touched.phoneNumber ? (
                                                            <span className="errorMsg ml-3">
                                                                {errors.phoneNumber}
                                                            </span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                            
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formControlsSelect">
                                                        <label>Preferred Payment Type</label>

                                                        <Field
                                                            component="select"
                                                            name="paymentMode"
                                                            placeholder="select"
                                                            className="form-control"
                                                            value={values.paymentMode}
                                                            //value="CHECK"
                                                            disabled
                                                        >
                                                            <option value="">Select</option>
                                                            <option value="ACH">ACH</option>
                                                            <option value="WIRE">Wire Transfer</option>
                                                            <option value="CHECK">CHECK</option>
                                                        </Field>
                                                        {errors.paymentMode && touched.paymentMode ? (
                                                            <span className="errorMsg ml-3">
                                                                {errors.paymentMode}
                                                            </span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                                <Col xs={12} md={6}>
                                                    <FormGroup controlId="formBasicText">
                                                        <label>Print On Check 
                                                            {/* <span className="required">*</span> */}
                                                        </label>
                                                        <Field
                                                            name="printName"
                                                            type="text"
                                                            className={'form-control'}
                                                            autoComplete="nope"
                                                            placeholder="Enter"
                                                            value={values.printName || ''}
                                                        />
                                                        {errors.printName && touched.printName ? (
                                                            <span className="errorMsg ml-3">
                                                                {errors.printName}
                                                            </span>
                                                        ) : null}
                                                        <FormControl.Feedback />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12}>
                                                    &nbsp;
                                                </Col>
                                            </Row>
                                            <Row className="show-grid">
                                                <Col xs={12} md={12} className="text-center">
                                                    <Button className="blue-btn" type="submit">
                                                        Save
                                                    </Button>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={12}>
                                                    <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                                </Col>
                                            </Row>
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Col>


                    </Row>



                </div>

                {/*======  confirmation popup  ===== */}
                <Modal
                    show={this.state.showConfirMmsg}
                    onHide={this.handleConfirmReviewClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Vendor has been successfully added</h5>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handleConfirmReviewClose}
                            className="but-gray"
                        >
                            Done
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

AddBusinessVendors.propTypes = {
    
    handleAddConfirMmsg: PropTypes.func,
    businessId: PropTypes.number,
    handelAddModalClose:PropTypes.func,

};

export default AddBusinessVendors;
