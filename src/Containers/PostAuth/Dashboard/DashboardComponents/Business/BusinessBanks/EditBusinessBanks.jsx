import React, {Component,
    //Fragment 
} from 'react';
import {
    //Table,
    //Dropdown,
    Row,
    Modal,
    Col,
    //Image,
    //Button
} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { CountryService } from './../../../../../../services/country.service';
import LoadingSpinner from './../../../../../../Components/LoadingSpinner/LoadingSpinner';
import axios from './../../../../../../shared/eaxios';



class EditBusinessBanks extends Component {

    state={
        countries:[],
        states:[], 
        cities:[],
        deleteConfirmMsg:false,
        alertMsg:false,
        alertState:null,
        errorMessge:null,
        statusError:null,
        setDefaultError:null,
        loader:false,
        changedStatus:null
    }

    _isMounted = false;

    displayError = (e) => {
        console.log('displayError', e.data.message);
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handelAlertMsg = (param) => {
        this.setState({
            alertMsg:true,
            alertState:param
        });
        
    }

    closeAlertMsg = () => {
        this.setState({
            alertMsg: false,
            alertState:null
        });
    }

    hendelSetDefault = () =>{
        this.setState({
            alertMsg: false,
            loader: true
        });
        axios
            .put(
                `transaction/bank/default/${this.props.editBankData.id}/${this.props.editBankData.businessId}`
            ).then(res => {
                console.log('set bank as default successfully', res.data);

                this.setState({
                    loader: false,
                }, () => {
                    this.props.success('setDefault');
                });


            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    loader: false,
                    errorMessge: errorMsg,
                });

                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);

            });
        
    }

    hendelStatus = (param) => {
        console.log('param', param);
        let changedStatus;
        if(param === true){
            changedStatus = 'inactive';
        }else{
            changedStatus = 'active';
        }
        this.setState({
            alertMsg: false,
            loader: true,
            changedStatus
        }, ()=>{
            axios
                .put(
                    `transaction/bank/${this.state.changedStatus}/${this.props.editBankData.id}/${this.props.editBankData.businessId}`

                ).then(res => {
                    console.log('bank status has been change successfully', res.data);

                    this.setState({
                        loader: false,
                    }, () => {
                        this.props.success('changeStatus');
                    });


                })
                .catch(e => {
                    let errorMsg = this.displayError(e);
                    this.setState({
                        loader: false,
                        errorMessge: errorMsg,
                    });

                    setTimeout(() => {
                        this.setState({ errorMessge: null });
                    }, 5000);

                });
            
        });
        

    }


    handelDeleteBank = () =>{
        this.setState({
            alertMsg: false,
            loader: true
        });
        axios
            .delete(
                `transaction/bank/${this.props.editBankData.id}/${this.props.editBankData.businessId}`
            ).then(res => {
                console.log('bank deleted successfully', res.data);

                this.setState({
                    loader: false,
                },()=>{
                    this.props.success('delete');
                });
                

            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    loader:false,
                    errorMessge: errorMsg,
                });

                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);

            });
    }



    componentDidMount(){
        console.log('editBankData------', this.props.editBankData);
        this._isMounted = true;

        const countryService = new CountryService;

        countryService.getData().then(({ countries, states, cities }) => {
            if (this._isMounted){
                this.setState({ countries, states, cities });
            }
            
        });
        
    }

    componentWillMount(){
        this._isMounted = false;
    }

    
    render(){
        const bankData = this.props.editBankData;
        return(
            <div>
                <div className="modalHeader">
                    <Row>
                        <Col md={4}>
                            <h1>Edit Bank</h1>
                        </Col>
                        <Col md={7} className="pr-1">
                            <button className="btn btn-primary ml-2 float-right" onClick={() => this.handelAlertMsg('changeStatus')}>Change Status</button>
                            {bankData.activeStatus === false ?<button className="btn btn-info ml-2 float-right" onClick={() => this.handelAlertMsg('setDefault')}>Set Default</button>:null}
                            <button className="btn btn-dark float-right ml-0" onClick={() => this.handelAlertMsg('delete')}>Delete</button>
                        </Col>
                    </Row>
                </div>
                <div className="modalBody content-body noTabs">
                    <Row>
                        <Col md={12}>
                            {this.state.loader ? <LoadingSpinner /> : null}
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>
                            {this.state.errorMessge ? <div className="alert alert-danger my-3 text-center col-12" role="alert">{this.state.errorMessge}</div> : null}
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6} sm={12} className="mb-2">
                            <div className="h6 font-weight-bold">Name on the Account</div>
                            <div className="h6">{bankData.accountName}</div>
                        </Col>
                        <Col md={6} sm={12} className="mb-2">
                            <div className="h6 font-weight-bold">Bank Name</div>
                            <div className="h6">{bankData.bankName}</div>
                        </Col>
                        <Col md={6} sm={12} className="mb-2">
                            <div className="h6 font-weight-bold">ABA/Routing Number</div>
                            <div className="h6">{bankData.routingNumber}</div>
                        </Col>
                        <Col md={6} sm={12} className="mb-2">
                            <div className="h6 font-weight-bold">Account Number</div>
                            <div className="h6">{bankData.accountNumber}</div>
                        </Col>
                        <Col md={6} sm={12} className="mb-2">
                            <div className="h6 font-weight-bold">Bank Address</div>
                            <div className="h6">{bankData.address}</div>
                        </Col>
                        <Col md={6} sm={12} className="mb-2">
                            <div className="h6 font-weight-bold">Country</div>
                            {
                                this.state.countries.length > 0
                                    ?
                                    this.state.countries.map(country => (
                                        country.countryCode === bankData.countryCode ? <div className="h6">{country.countryName}</div> : null
                                    ))
                                    :
                                    null

                            } 
                        </Col>
                        <Col md={6} sm={12} className="mb-2">
                            <div className="h6 font-weight-bold">State</div>
                            
                            {
                                this.state.states.length > 0 
                                    ? 
                                    this.state.states.map(state => (
                                        state.stateCode === bankData.stateCode ? <div className="h6">{state.stateName}</div>:null
                                    ))
                                    :
                                    null

                            } 

                        </Col>
                        <Col md={6} sm={12} className="mb-2">
                            <div className="h6 font-weight-bold">City</div>
                            {
                                this.state.cities.length > 0
                                    ?
                                    this.state.cities.map(city => (
                                        city.id === bankData.cityId ? <div className="h6">{city.cityName}</div> : null
                                    ))
                                    :
                                    null

                            } 
                        </Col>
                        <Col md={6} sm={12} className="mb-2">
                            <div className="h6 font-weight-bold">Zip Code</div>
                            <div className="h6">{bankData.zipCode}</div>
                        </Col>
                        <Col md={6} sm={12} className="mb-2">
                            <div className="h6 font-weight-bold">Bank Status</div>
                            <div className="h6">{bankData.status === true ? <span>Active</span> : <span>Inactive</span>}</div>
                        </Col>
                        <Col md={6} sm={12} className="mb-2">
                            <div className="h6 font-weight-bold">Default Bank</div>
                            <div className="h6">{bankData.activeStatus === true ? <span>Yes</span> : <span>No</span>}</div>
                        </Col>
                    </Row>
                    
                </div>

            
    

                {/*======  before action confirmation massage popup  ===== */}
                <Modal
                    show={this.state.alertMsg}
                    onHide={this.closeAlertMsg}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <div className="m-auto text-center">
                            <h6 className="mb-3">
                                {
                                    this.state.alertState === 'delete' 
                                        ?
                                        <span>Do You Want To Delete This Bank ?</span>
                                        :
                                        (
                                            this.state.alertState === 'setDefault'
                                                ?
                                                <span>Do you want set this bank as a default bank?</span>
                                                :
                                                (
                                                    this.state.alertState === 'changeStatus'
                                                        ?
                                                        <span>Do you want to Change this bank status?</span>
                                                        :null
                                                )
                                        )
                                }
                        
                            </h6>
                        </div>
                        <div className="m-auto text-center">
                            <button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.closeAlertMsg()}>Return</button>

                            {/* diffrent action called from here */}

                            {this.state.alertState==='delete'
                                ?
                                (
                                    this.state.errorMessge == null
                                        ?
                                        <button className="btn btn-danger" onClick={() => this.handelDeleteBank()}>Confirm</button>
                                        :
                                        null
                                )
                                :
                                (this.state.alertState === 'setDefault' 
                                    ? 
                                    (
                                        this.state.errorMessge == null
                                            ?
                                            <button className="btn btn-danger" onClick={() => this.hendelSetDefault()}>Confirm</button>
                                            :
                                            null
                                    )
                                    :
                                    (
                                        this.state.errorMessge == null
                                            ?
                                            <button className="btn btn-danger" onClick={() => this.hendelStatus(this.props.editBankData.status)}>Confirm</button>
                                            :
                                            null
                                    )
                                )
                                
                            }
                        </div>

                    </Modal.Body>
                </Modal>
            
            </div>


        );
    }

}

EditBusinessBanks.propTypes = {
    editBankData: PropTypes.object,
    success:PropTypes.func,
};

export default EditBusinessBanks;
