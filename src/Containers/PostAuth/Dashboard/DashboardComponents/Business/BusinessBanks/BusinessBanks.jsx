import React, {
    Component,
    //Fragment 
} from 'react';
import { connect } from 'react-redux';
import axios from '../../../../../../shared/eaxios';
import PropTypes from 'prop-types';
import LoadingSpinner from '../../../../../../Components/LoadingSpinner/LoadingSpinner';

import {
    Table,
    //Dropdown,
    Row,
    Modal,
    Col,
    Image,
    Button
} from 'react-bootstrap';
// import { FaCaretDown } from 'react-icons/fa';
// import { FaCaretUp } from 'react-icons/fa';
import SuccessIco from './../../../../../../assets/success-ico.png';
//import { FiMenu } from 'react-icons/fi';
import Pagination from 'react-js-pagination';
import AddBusinessBanks from './AddBusinessBanks';
import EditBusinessBanks from './EditBusinessBanks';


class BusinessBanks extends Component{
    
    state = {
        activePage: 1,
        totalCount: 0,
        itemPerPage: 250,
        fetchErrorMsg:null,
        BankLists:[],
        loading: true,
        sortingActiveID: 1,
        addModal:false,
        addConfirMmsg: false,
        showActionModal: false,
        editBankData:{},
        confirmMsg:false,
        confirmMsgStatus: null,

    }
    
    _isMounted = false;

    showActionModal = (data) => {
        this.setState({
            showActionModal: true,
            editBankData:data
        });
    }

    hideActionModal = () => {
        this.setState({
            showActionModal: false
        });
    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
       
        return errorMessge;
    }
    sortingActive = (id) => {
        this.setState({
            sortingActiveID: id
        }, () => { console.log('this.state.sortingActiveID', this.state.sortingActiveID); });
    }
    fetchBusinessBanks = (
        since = 0, // Pagination strating from --1
        
    ) =>{
        this.setState({
            loading: true,
        },()=>{
            axios
                .get(
                    `transaction/bank/${this.props.globalState.business.businessData.id}?since=${since}&limit=${this.state.itemPerPage}`
                )
                .then(res => {
                    console.log('res.data for banks', res.data);
                    const BankLists = res.data.entries;
                    const totalCount = res.data.total;
                    
                    if (this._isMounted) {
                        this.setState({
                            BankLists,
                            totalCount: totalCount,
                            loading: false
                        });
                    }

                })
                .catch(e => {
                    let errorMsg = this.displayError(e);
                    this.setState({
                        fetchErrorMsg: errorMsg,
                        loading: false
                    },()=>{
                        console.log('this.state.fetchErrorMsg', this.state.fetchErrorMsg);
                    });

                    setTimeout(() => {
                        this.setState({ fetchErrorMsg: null });
                    }, 5000);
                });
        });
    }

    handelAddModal = () => {
        this.setState({
            addModal: true,
        });
    }

    handelAddModalClose = () => {
        this.setState({
            addModal: false,
        });
    }

    handleaddConfirMmsg = () => {
        this.handelAddModalClose();
        this.setState({
            addConfirMmsg: true
        },()=>{
            this.fetchBusinessBanks();
        });
    }

    handleaddConfirMmsgClose = () => {

        this.setState({
            addConfirMmsg:false
        });
    }
    handleConfirMmsg = (param) => {
        this.setState({
            confirmMsg: true,
            confirmMsgStatus: param
        });
    }

    handleConfirMmsgClose = () => {
        this.fetchBusinessBanks();
        this.setState({
            confirmMsg: false,
            showActionModal:false
        });
    }
    


    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber });
        this.fetchBusinessBanks(pageNumber > 0 ? pageNumber - 1 : 0);
    };

    handleChangeItemPerPage = (e) => {
        this.setState({ itemPerPage: e.target.value },
            () => {
                this.fetchBusinessBanks(this.state.activePage > 0 ? this.state.activePage - 1 : 0);
            });
    }

    resetPagination = () => {
        this.setState({ activePage: 1 });
    }

  

    componentDidMount(){
        this._isMounted = true;

        console.log('this.props.globalState', this.props.globalState.business.businessData.id);
        this.fetchBusinessBanks();
    }

    componentWillMount(){
        this._isMounted = false;
    }

    render(){
        return(

            <div className="boxBg border p-t-15 p-b-15 px-3">
                <div className="row py-3">
                    <div className="col-6"></div>
                    <div className="col-6">
                        <button className="btn btn-primary float-right" onClick={() => this.handelAddModal()}>Add Bank</button>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <Table responsive hover>
                            <thead className="theaderBg">
                                <tr>
                                    <th>Bank Name</th>
                                    <th>A/C No</th>
                                    <th>Staus</th>
                                    <th>Set Default</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.loading ? (<tr>
                                    <td colSpan={12}>
                                        <LoadingSpinner />
                                    </td>
                                </tr>) :
                                    this.state.BankLists.length > 0 ? (
                                        this.state.BankLists.map(BankList => (

                                            (BankList.archieve === false ?
                                                (<tr key={BankList.id} onClick={() => this.showActionModal(BankList)}>
                                                    <td>{BankList.bankName}</td>
                                                    <td>{BankList.accountNumber}</td>
                                                    <td>{BankList.status === true ? <span>Active</span> : <span>Inactive</span>}</td>
                                                    <td>{BankList.activeStatus === true ? <span>Yes</span> : <span>No</span>}</td>

                                                </tr>) : null)


                                        ))
                                    )
                                        :
                                        this.state.fetchErrorMsg ? null : (
                                            <tr>
                                                <td colSpan={12}>
                                                    <p className="text-center">No records found</p>
                                                </td>
                                            </tr>
                                        )
                                }
                            </tbody>
                        </Table>
                        {this.state.totalCount ? (
                            <Row className="px-3">
                                <Col md={4} className="d-flex flex-row mt-20">
                                    <span className="mr-2 mt-2 font-weight-500">Items per page</span>
                                    <select
                                        id={this.state.itemPerPage}
                                        className="form-control truncatefloat-left w-90"
                                        onChange={this.handleChangeItemPerPage}
                                        value={this.state.itemPerPage}>
                                        <option value='50'>50</option>
                                        <option value='100'>100</option>
                                        <option value='150'>150</option>
                                        <option value='200'>200</option>
                                        <option value='250'>250</option>

                                    </select>
                                </Col>
                                <Col md={8}>
                                    <div className="paginationOuter text-right">
                                        <Pagination
                                            activePage={this.state.activePage}
                                            itemsCountPerPage={this.state.itemPerPage}
                                            totalItemsCount={this.state.totalCount}
                                            onChange={this.handlePageChange}
                                        />
                                    </div>
                                </Col>
                            </Row>
                        ) : null}

                    </div>
                </div>
                {/* Add  Modal */}
                <Modal
                    show={this.state.addModal}
                    onHide={this.handelAddModalClose}
                    className="right full noPadding slideModal"
                >
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body className="">
                        <div className="modalHeader">
                            <Row>
                                <Col md={9}>
                                    <h1>Add Bank</h1>
                                </Col>
                            </Row>
                        </div>

                        <div className="modalBody content-body noTabs">
                            <AddBusinessBanks 
                                businessId={this.props.globalState.business.businessData.id} 
                                onSuccess={this.handleaddConfirMmsg}
                                handelAddModalClose={this.handelAddModalClose}
                            />
                        </div>
                    </Modal.Body>

                </Modal>

                {/*======  confirmation popup  ===== */}
                <Modal
                    show={this.state.addConfirMmsg}
                    onHide={this.handleaddConfirMmsgClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <h5>Bank has been successfully Added</h5>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <Button
                                    onClick={this.handleaddConfirMmsgClose}
                                    className="but-gray"
                                >
                                    Return
                                </Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>


                {/*======Delete confirmation popup  ===== */}
                <Modal
                    show={this.state.confirmMsg}
                    onHide={this.handleDeleteConfirMmsgClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                {this.state.confirmMsgStatus === 'delete' ? <h5>Bank has been successfully Deleted</h5> : (this.state.confirmMsgStatus === 'setDefault' ? <h5>This Bank has been set as default bank</h5> : <h5>This Bank status has been changed</h5>)}  
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center">
                                <Button
                                    onClick={this.handleConfirMmsgClose}
                                    className="but-gray"
                                >
                                    Return
                                </Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                            


                <Modal
                    show={this.state.showActionModal}
                    onHide={this.hideActionModal}
                    className="right half noPadding slideModal"
                >
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body>
                        <EditBusinessBanks editBankData={this.state.editBankData} success={this.handleConfirMmsg}/>
                    </Modal.Body>

                </Modal>

            </div>
        
        );
    }

}



const mapStateToProps = state=> {
    return {
        globalState: state
    };
};

BusinessBanks.propTypes = {
    globalState: PropTypes.object,
};

export default connect(mapStateToProps, null)(BusinessBanks);
