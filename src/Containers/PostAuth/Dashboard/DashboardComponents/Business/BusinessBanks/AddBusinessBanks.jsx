import { Field, Form, Formik } from 'formik';
import React, { Component } from 'react';
import { Button, Col, FormControl, FormGroup, Row } from 'react-bootstrap';
import * as Yup from 'yup';
import { CountryService } from './../../../../../../services/country.service';
import axios from './../../../../../../shared/eaxios';
import PropTypes from 'prop-types';
import LoadingSpinner from './../../../../../../Components/LoadingSpinner/LoadingSpinner';

const initialValues = {
    accountName: '',
    bankListId: '',
    accountNumber: '',
    address: '',
    routingNumber: '',
    countryCode: '',
    zipCode: '',
    stateCode: '',
    cityId: '',
    activeStatus: false,
    archieve: false,
    status: true,
};
const yourDetailsSchema = Yup.object().shape({
    accountName: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .max(100)
        .required('Please enter name'),
    bankListId: Yup
        .string()
        .max(300)
        .required('Please enter bank name'),
    accountNumber: Yup
        .string()
        .max(17, 'Account Number should be 17 digits maximum.')
        .trim('Please remove whitespace')
        .strict()
        .test(
            'validateNumber',
            'Please enter number',
            function (value) {
                if (value) {
                    let isValid = false;
                    if (/^[0-9]*$/.test(value)) {
                        isValid = true;
                    }

                    return isValid;
                }
                return true;
            }
        )
        .required('Please enter account number'),
    address: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .max(100)
        .required('Please enter bank address'),
    routingNumber: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .test(
            'validateRouting',
            'Please enter number',
            function (value) {
                if (value) {
                    let isValid = false;
                    if (/^[0-9]*$/.test(value)) {
                        isValid = true;
                    }

                    return isValid;
                }
                return true;
            }
        )
        .required('Please enter ABA/Routing number'),
    countryCode: Yup.string().required('Please select country'),
    zipCode: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter zip code').test(
            'zipCode',
            'Please enter a valid zip code',
            function (value) {
                if (value) {
                    let isValid = false;
                    // eslint-disable-next-line no-useless-escape
                    if (/^\d{5}([\-|\s]\d{1,4})?$/.test(value)) {
                        isValid = true;
                    } else if (/^[a-zA-Z0-9]{3}\s[a-zA-Z0-9]{3}$/.test(value)) {
                        let expArr = value.split(' ');

                        if (/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{3}$/.test(expArr[0]) && /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{3}$/.test(expArr[1])) {
                            isValid = true;
                        }
                    }
                    return isValid;
                }

            }
        ),
    stateCode: Yup.string().required('Please select state'),
    cityId: Yup.number().required('Please enter bank city')
});

class AddBusinessBanks extends Component {
    state = {
        show: false,
        countries: [],
        states: [],
        cities: [],
        selectedStates: [],
        selectedCities: [],
        banklists: [],
        date: new Date(),
        startDate: new Date(),
        endDate: new Date(),
        banks: [],
        errorMessge: null,
        loading: false,
    };

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        this.setState({
            errorMessge: errorMessge,
            errorStay: errorMessge
        });

        setTimeout(() => {
            this.setState({ errorMessge: null });
        }, 5000);
    }
   
    handleSubmit = (
        values, 
        { resetForm }
    ) => {

        console.log(values);

        this.setState({
            loading:true
        });
        const formData = new window.FormData();
        for (const key in values) {
            if (values.hasOwnProperty(key)) {
                formData.append(key, values[key]);
            }
        }
        for (const st of this.state.states) {
            if (parseInt(st.id) === parseInt(values.stateCode))
                values.stateCode = st.stateCode;
            console.log(values.stateCode);
        }

        for (const country of this.state.countries) {
            if (parseInt(country.id) === parseInt(values.countryCode))
                values.countryCode = country.countryCode;
            console.log(values.countryCode);
        }

        const val = {
            accountName: values.accountName,
            bankListId: values.bankListId,
            accountNumber: values.accountNumber,
            address: values.address,
            routingNumber: values.routingNumber,
            countryCode: values.countryCode,
            zipCode: values.zipCode,
            stateCode: values.stateCode,
            cityId: values.cityId,
            //activeStatus: false,
            archieve: false,
            status: true
        };
        console.log('Add Bank Values', val);
        axios
            .post(`/transaction/bank/create/${this.props.businessId}`, val)
            .then(res => {
                console.log(res);
                resetForm({});
                this.props.onSuccess();

            })
            // .catch(this.displayError);
            .catch(err => {
                let errorMessage = '';
                try {
                    errorMessage = err.data.message ? err.data.message : err.data.error_description;
                } catch (err) {
                    errorMessage = 'No records found.';
                }
                this.setState({
                    loading: false,
                    errorMessage: errorMessage,

                });
                setTimeout(() => {
                    this.setState({ errorMessage: null });
                }, 2500);
            });
    };

    
    getCountry() {
        const countryService = new CountryService();
        countryService.getData().then(({ countries, states, cities }) => { //countries, states or cities
            this.setState({ countries, states, cities });
        });
    }

    populateState = e => {
        const countryId = e.target.value;
        const states = this.state.states;
        const newStates = states.filter(state => {
            return Number(state.countryId) === Number(countryId);
        });

        this.setState({ selectedStates: newStates, selectedCities: [] });
        console.log(this.state);
    };

    populateCity = e => {
        const stateId = e.target.value;
        const cities = this.state.cities;
        const newCities = cities.filter(city => {
            return Number(city.stateId) === Number(stateId);
        });
        this.setState({ selectedCities: newCities });
        console.log(this.state.selectedCities);
    };

    getMasterBank = () => {
  
        axios
            .get('/bank/list')
            .then(res => {
                const banklists = res.data.entries;
                this.setState({ banklists: banklists });
                console.log('master bank', this.state.banklists);
            })
            .catch(err => {
                this.setState({
                    errorMessageMasterBank:
                        'Error:: ' +
                        err.data.status +
                        ',' +
                        err.data.message +
                        ',' +
                        err.data.error
                });
                setTimeout(() => {
                    this.setState({ errorMessage: null });
                }, 2500);
            });
    };

    

    handleBankChange = () => {
        console.log('bank update');
        this.setState({
            showBankOption: true,
        });
    }

    handleBankChangeClose = () => {
        this.setState({
            showBankOption: false,
        });
    }

    componentDidMount() {
        this.getCountry();
        this.getMasterBank();
    }

    render() {
        const { 
            selectedCities, 
            selectedStates, 
            //selectedCountryId 
        } = this.state;
        return (

            <div>
                {this.state.loading ? <LoadingSpinner/>:null}
                <Formik
                    initialValues={initialValues}
                    validationSchema={yourDetailsSchema}
                    onSubmit={this.handleSubmit}
                >
                    {({
                        values,
                        errors,
                        touched,
                        //isSubmitting,
                        handleChange,
                        //handleBlur,
                        //setFieldValue
                    }) => {
                        return (
                            <Form>
                                {this.state.errorMessage ? (
                                    <Row>
                                        <Col sm={12} md={12}>
                                            <div className="alert alert-danger" role="alert">
                                                <h4>{this.state.errorMessage}</h4>
                                            </div>
                                        </Col>
                                    </Row>
                                ) : null}
                                <Row>
                                    <Col md={6}>
                                        <FormGroup controlId="formBasicText">
                                            <span>Name on the Account <span className="required">*</span></span>
                                            <Field
                                                name="accountName"
                                                type="text"
                                                className={'form-control'}
                                                autoComplete="nope"
                                                placeholder="Enter"
                                                value={values.accountName || ''}
                                                autoFocus
                                            />
                                            {errors.accountName && touched.accountName ? (
                                                <span className="errorMsg pl-3">
                                                    {errors.accountName}
                                                </span>
                                            ) : null}
                                            <FormControl.Feedback />
                                        </FormGroup>
                                    </Col>
                                    <Col md={6}>
                                        <FormGroup controlId="formBasicText">
                                            <span>Bank Name <span className="required">*</span></span>
                                            <Field
                                                name="bankListId"
                                                component="select"
                                                className={'form-control'}
                                                autoComplete="nope"
                                                placeholder="Enter"
                                                value={values.bankListId || ''}
                                                disabled={this.state.errorMessageMasterBank ? true : false}
                                            >
                                                <option value="" selected="selected">
                                                    Select Bank
                                                </option>

                                                {this.state.banklists.map(masterBanks => (
                                                    <option key={masterBanks.id} value={masterBanks.id}>
                                                        {masterBanks.bankname}
                                                    </option>
                                                ))}
                                            </Field>
                                            {errors.bankListId && touched.bankListId ? (
                                                <span className="errorMsg pl-3">
                                                    {errors.bankListId}
                                                </span>
                                            ) : null}
                                            <FormControl.Feedback />
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={6}>
                                        <FormGroup controlId="formBasicText">
                                            <span>ABA/Routing Number <span className="required">*</span></span>
                                            <Field
                                                name="routingNumber"
                                                type="text"
                                                className={'form-control'}
                                                autoComplete="nope"
                                                placeholder="Enter"
                                                value={values.routingNumber || ''}
                                            />
                                            {errors.routingNumber &&
                                                touched.routingNumber ? (
                                                    <span className="errorMsg pl-3">
                                                        {errors.routingNumber}
                                                    </span>
                                                ) : null}
                                            <FormControl.Feedback />
                                        </FormGroup>
                                    </Col>
                                    <Col md={6}>
                                        <FormGroup controlId="formBasicText">
                                            <span>Account Number <span className="required">*</span></span>
                                            <Field
                                                name="accountNumber"
                                                type="text"
                                                className={'form-control'}
                                                autoComplete="nope"
                                                placeholder="Enter"
                                                value={values.accountNumber || ''}
                                            />
                                            {errors.accountNumber && touched.accountNumber ? (
                                                <span className="errorMsg pl-3">
                                                    {errors.accountNumber}
                                                </span>
                                            ) : null}
                                            <FormControl.Feedback />
                                        </FormGroup>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={6}>
                                        <FormGroup controlId="formBasicText">
                                            <span>Bank Address <span className="required">*</span></span>
                                            <Field
                                                name="address"
                                                type="text"
                                                className={'form-control'}
                                                autoComplete="nope"
                                                placeholder="Enter"
                                                value={values.address || ''}
                                            />
                                            {errors.address && touched.address ? (
                                                <span className="errorMsg pl-3">{errors.address}</span>
                                            ) : null}
                                            <FormControl.Feedback />
                                        </FormGroup>
                                    </Col>
                                    <Col md={6}>
                                        <FormGroup controlId="formBasicText">
                                            <span>Country <span className="required">*</span></span>
                                            <Field
                                                name="countryCode"
                                                component="select"
                                                className={'form-control'}
                                                autoComplete="nope"
                                                placeholder="select"
                                                value={values.countryCode || ''}
                                                onChange={e => {
                                                    handleChange(e);
                                                    this.populateState(e);
                                                }}
                                            >
                                                <option value="" selected="selected">
                                                    {this.state.countries.length ? 'Select Country' : 'Loading...'}
                                                </option>

                                                {this.state.countries.map(country => (
                                                    <option key={country.id} value={country.id}>
                                                        {country.countryName}
                                                    </option>
                                                ))}
                                            </Field>

                                            {errors.countryCode && touched.countryCode ? (
                                                <span className="errorMsg pl-3">{errors.countryCode}</span>
                                            ) : null}
                                            <FormControl.Feedback />
                                        </FormGroup>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={6}>
                                        <FormGroup controlId="formBasicText">
                                            <span>State <span className="required">*</span></span>
                                            <Field
                                                name="stateCode"
                                                component="select"
                                                className={'form-control'}
                                                autoComplete="nope"
                                                placeholder="select"
                                                value={values.stateCode || ''}
                                                onChange={e => {
                                                    handleChange(e);
                                                    this.populateCity(e);
                                                }}
                                                disabled={
                                                    this.state.selectedStates.length
                                                        ? false
                                                        : true
                                                }
                                            >
                                                <option value="" selected="selected">
                                                    Select State
                                                </option>
                                                {
                                                    selectedStates.map(stName => (
                                                        <option value={stName.id} key={stName.id}>
                                                            {stName.stateName}
                                                        </option>
                                                    ))}
                                            </Field>
                                            {errors.stateCode && touched.stateCode ? (
                                                <span className="errorMsg pl-3">{errors.stateCode}</span>
                                            ) : null}

                                            <FormControl.Feedback />
                                        </FormGroup>
                                    </Col>
                                    <Col md={6} className="cityField">
                                        <FormGroup controlId="formBasicText">
                                            <span>City <span className="required">*</span></span>
                                            <Field
                                                name="cityId"
                                                component="select"
                                                className={'form-control'}
                                                autoComplete="nope"
                                                placeholder="select"
                                                value={values.cityId || ''}
                                                disabled={
                                                    this.state.selectedCities.length
                                                        ? false
                                                        : true
                                                }
                                            >
                                                <option value="" selected="selected">
                                                    Select City
                                                </option>
                                                {
                                                    selectedCities.map(cityName => (
                                                        <option value={cityName.id} key={cityName.id}>
                                                            {cityName.cityName}
                                                        </option>
                                                    ))}
                                            </Field>
                                            {errors.cityId && touched.cityId ? (
                                                <span className="errorMsg pl-3">{errors.cityId}</span>
                                            ) : null}
                                            <FormControl.Feedback />
                                        </FormGroup>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={6}>

                                        <FormGroup controlId="formBasicText">
                                            <span>Zip Code <span className="required">*</span></span>
                                            <Field
                                                name="zipCode"
                                                type="text"
                                                className={'form-control'}
                                                autoComplete="nope"
                                                placeholder="Enter"
                                                value={values.zipCode || ''}
                                            />
                                            {errors.zipCode && touched.zipCode ? (
                                                <span className="errorMsg pl-3">{errors.zipCode}</span>
                                            ) : null}
                                            <FormControl.Feedback />
                                        </FormGroup>
                                    </Col>
                                </Row>

                                <Row className="show-grid" style={{ display: 'none' }}>
                                    <Col xs={12} md={12}>
                                        <input
                                            type="text"
                                            name="businessId"
                                            value={values.businessId}
                                        />
                                    </Col>
                                </Row>

                                <Row>&nbsp;</Row>

                                <Row className="show-grid text-center">
                                    <Col xs={12} md={12}>
                                        <Button
                                            className="but-gray mr-2"
                                            onClick={()=>this.props.handelAddModalClose()}
                                        >
                                            Cancel
                                        </Button>
                                        <Button type="submit" className="blue-btn ml-2">
                                            Confirm
                                        </Button>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={12}>
                                        <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                    </Col>
                                </Row>
                            </Form>
                        );
                    }}
                </Formik>

            </div>
        
        );
    }
}

AddBusinessBanks.propTypes = {
    //businessId: PropTypes.string,
    businessId: PropTypes.number,
    onSuccess:PropTypes.func,
    handelAddModalClose:PropTypes.func,
    
};

export default AddBusinessBanks;
