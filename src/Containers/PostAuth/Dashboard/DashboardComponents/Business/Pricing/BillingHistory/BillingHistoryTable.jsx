import React, { Component } from 'react';
import styles from './BillingHistoryTable.module.scss';
import PropTypes from 'prop-types';
import LoadingSpinner from './../../../../../../../Components/LoadingSpinner/LoadingSpinner';
import NumberFormat from 'react-number-format';


class BillingHistoryTable extends Component {

    componentDidMount() {
        console.log('componentDidMount from BillingHistoryTable', this.props);
    }
    componentDidUpdate() {
        console.log('componentDidUpdate from BillingHistoryTable', this.props);
    }

    render() {
        return (
            <div>

                <div className="p-3 clearfix">
                    <div className="p-3">
                        <table className="table">
                            <thead>
                                <tr className={styles.cusTh}>
                                    <th scope="col">Date</th>
                                    <th scope="col">Type</th>
                                    <th scope="col">Reference Id</th>
                                    <th scope="col">Amount</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>

                                {this.props.billingHistryLstLoader
                                    ?
                                    <tr>
                                        <td colSpan='6'>
                                            <LoadingSpinner />
                                        </td>
                                    </tr>
                                    :

                                    this.props.billingHistryList.length > 0 ?
                                    
                                        (this.props.billingHistryList.map(
                                            billingHistry =>
                                                <tr key={billingHistry.id}>
                                                    <td scope="col">{billingHistry.endDate}</td>
                                                    <td scope="col">{billingHistry.description}</td>
                                                    <td scope="col">{billingHistry.referenceId}</td>
                                                    <td scope="col">
                                                        <NumberFormat
                                                            value={billingHistry.amount}
                                                            displayType={'text'}
                                                            thousandSeparator={true}
                                                            fractionSize={2}
                                                            prefix={'$'}
                                                            decimalScale={2}
                                                            fixedDecimalScale={true}
                                                            thousandsGroupStyle={'thousand'}
                                                            renderText={value => <span>{value}</span>}/>
                                                    </td>
                                                    <td className="text-warning"><a href={billingHistry.pdfUrl} target={'_blank'}><u>PDF</u></a></td>
                                                </tr>
                                        )) :  this.props.billingHistryLstError ? <tr>
                                            <td colSpan={6}>
                                                <p className="text-center">{this.props.billingHistryLstError}</p>
                                            </td>
                                        </tr> : (
                                            <tr>
                                                <td colSpan={6}>
                                                    <p className="text-center">No records found</p>
                                                </td>
                                            </tr>
                                        )



                
                                }

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        );
    }
}

BillingHistoryTable.propTypes = {
    billingHistryList: PropTypes.array,
    billingHistryLstLoader: PropTypes.bool,
    billingHistryLstError: PropTypes.string,
};

export default BillingHistoryTable;
