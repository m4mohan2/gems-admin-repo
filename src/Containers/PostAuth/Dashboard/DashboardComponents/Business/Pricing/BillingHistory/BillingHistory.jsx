import React, { Component } from 'react';
import BillingHistoryTable from './BillingHistoryTable';
import PropTypes from 'prop-types';
import Pagination from 'react-js-pagination';
import {

    Row,
    Col,

} from 'react-bootstrap';
class BillingHistory extends Component {

    state={
        activePage: 1,
        totalCount: 0,
        itemPerPage: 250,
    }


    componentDidMount(){
        console.log('componentDidMount from billing history',this.props);
    }
    componentDidUpdate() {
        console.log('componentDidUpdate from billing history', this.props);
        // this.setState({
        //     totalCount: this.props.totalCount
        // }, () => console.log('this.state.totalCount', this.state.totalCount));
    }

    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber });

        this.props.handleGetBillingHistryList(
            pageNumber > 0 ? pageNumber - 1 : 0,
        );
    };

    handleChangeItemPerPage = (e) => {
        this.setState({ itemPerPage: e.target.value },
            () => {
                this.props.handleGetBillingHistryList(
                    this.state.activePage > 0 ? this.state.activePage - 1 : 0,
                );
            });
    }

    render() {
        return (
            <div className="bg-white">
                <BillingHistoryTable 
                    billingHistryList={this.props.billingHistryList} 
                    billingHistryLstLoader={this.props.billingHistryLstLoader}
                    billingHistryLstError={this.props.billingHistryLstError}
                />

                {this.state.totalCount ? (
                    <Row className="px-3">
                        <Col md={4} className="d-flex flex-row mt-20">
                            <span className="mr-2 mt-2 font-weight-500">Items per page</span>
                            <select
                                id={this.state.itemPerPage}
                                className="form-control truncatefloat-left w-90"
                                onChange={this.handleChangeItemPerPage}
                                value={this.state.itemPerPage}>
                                <option value='50'>50</option>
                                <option value='100'>100</option>
                                <option value='150'>150</option>
                                <option value='200'>200</option>
                                <option value='250'>250</option>

                            </select>
                        </Col>
                        <Col md={8}>
                            <div className="paginationOuter text-right">
                                <Pagination
                                    activePage={this.state.activePage}
                                    itemsCountPerPage={this.state.itemPerPage}
                                    totalItemsCount={this.state.totalCount}
                                    onChange={this.handlePageChange}
                                />
                            </div>
                        </Col>
                    </Row>
                ) : null}    

            </div>
        );
    }
}

BillingHistory.propTypes = {
    billingHistryList: PropTypes.array,
    billingHistryLstLoader: PropTypes.bool,
    billingHistryLstError: PropTypes.string,
    totalCount:PropTypes.number,
    handleGetBillingHistryList:PropTypes.func,
};

export default BillingHistory;
