/* eslint-disable no-unused-vars */
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Field, FieldArray, Form, withFormik, getIn } from 'formik';
import * as Yup from 'yup';
import NumberFormat from 'react-number-format';
import crossSmIcon from './../../../../../../../assets/crossSmIcon.png';
import CustomNumberFormat from './../../../../../../../Components/CustomNumberFormat/CustomNumberFormat';
import {
    Button,
    Col,
    FormControl,
    FormGroup,
    Image,
    Modal,
    Collapse,
    Row,
    Tab,
    Table,
    Tabs,
    Link
} from 'react-bootstrap';



const miscSchema =Yup.object().shape({

    pricingForm: Yup.array().of(
        Yup.object().shape({
            product: Yup.string().required('product name is required!'),
            description: Yup.string(),
            bps: Yup.number()
                .max(999, 'maximum bps cannot be more than 3 digits')
            // .min(0.1, "minimum bps cannot be less than 0")
                .moreThan(0, 'bps should be greater than 0')
                .required('Please enter bps')
                .typeError('bps should be number')
                .test('bps', 'bps upto two decimal places', function(
                    value
                ) {
                    if (value) {
                        let isValid = false;
                        if (/^\d*(\.\d{0,2})?$/.test(value)) {
                            isValid = true;
                        }

                        return isValid;
                    }
                    return true;
                }),
        })
    )
});


const ProductPricingBaseValue = {
    product: '',
    description: '',
    bps: 0.0,
};



const getErrorStyles = (errors, touched, fieldName) => {
    if (getIn(errors, fieldName) && getIn(touched, fieldName)) {
        return {
            border: '1px solid red'
        };
    }
};


class ProductPricingVCFrom extends Component {



    clearAll = (values, setFieldValue) => {
        setFieldValue('pricingForm', []);
    };

    render() {
        const { 
            values, 
            handleChange,
            // eslint-disable-next-line react/prop-types
            errors,
            // eslint-disable-next-line react/prop-types
            touched,
            // eslint-disable-next-line react/prop-types
            isValid,
            setFieldValue,
            handleReset
        } = this.props;
        console.log('errors ### ', errors, isValid, touched);
        return(
            <div>
                {console.log('++++++++++++++++ values +++++++++++++++', values)}
                <Form>
                    <Fragment>
                        <FieldArray
                            name="pricingForm">
                            {(helpers) => {
                                console.log('------------ helpers ------------', helpers);
                                return (
                                    <Fragment>
                                        <Table responsive hover>
                                            <thead className="theaderBg">
                                                <tr>
                                                    <th className="nameComment">Product</th>
                                                    <th>BPS Revenue(%)</th>
                                                    
                                                    <th width="75"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {values.pricingForm.length > 0 ? (
                                                    values.pricingForm.map(
                                                        (prData, index) => {
                                                            return (
                                                                <tr key={index}>
                                                                    <td>
                                                                        <FormGroup controlId="formBasicText">
                                                                            <Field
                                                                                style={getErrorStyles(
                                                                                    errors,
                                                                                    touched,
                                                                                    `pricingForm.${index}.transactionType`
                                                                                )}
                                                                                component="input"
                                                                                type="text"
                                                                                name={`pricingForm.${index}.product`}
                                                                                className={
                                                                                    'form-control'
                                                                                }
                                                                            >
                                                                                
                                                                            </Field>

                                                                            <div className="mt-3">
                                                                                <Field
                                                                                    style={getErrorStyles(
                                                                                        errors,
                                                                                        touched,
                                                                                        `pricingForm.${index}.description`
                                                                                    )}
                                                                                    component="textarea"
                                                                                    name={`pricingForm.${index}.description`}
                                                                                    placeholder="Description"
                                                                                    className={
                                                                                        'form-control'
                                                                                    }
                                                                                />
                                                                            </div>
                                                                            <FormControl.Feedback />
                                                                        </FormGroup>
                                                                    </td>

                                                                    <td>
                                                                        <FormGroup controlId="formBasicText">
                                                                            <Field
                                                                                style={getErrorStyles(
                                                                                    errors,
                                                                                    touched,
                                                                                    `pricingForm.${index}.bps`
                                                                                )}
                                                                                component="input"
                                                                                type="number"
                                                                                name={`pricingForm.${index}.bps`}
                                                                                placeholder="0.00"
                                                                                className={
                                                                                    'form-control'
                                                                                }
                                                                                onBlur={e => {
                                                                                    if(e.target.value) {
                                                                                        const up = isNaN(parseFloat(e.target.value)) ? 0 : parseFloat(e.target.value);
                                                                                        setFieldValue(`pricingForm.${index}.bps`, up.toFixed(2));
                                                                                    } 
                                                                                }}
                                                                            />
                                                                            
                                                                            
                                                                            <FormControl.Feedback />
                                                                        </FormGroup>
                                                                    </td>

                                                                    
                                                                    <td width="75">
                                                                        <div className="showHideItem">
                                                                            <a
                                                                                to="#"
                                                                                className="smIconBg"
                                                                                onClick={() => helpers.remove(index)}
                                                                            >
                                                                                <Image
                                                                                    src={crossSmIcon}
                                                                                />
                                                                            </a>
                                                                        </div>
                                                                        
                                                                    </td>
                                                                </tr>
                                                            );
                                                        }
                                                    )
                                                ) : (
                                                    <tr>
                                                        <td colSpan='6' className="text-center">
                                                            <p>No items</p>
                                                        </td>
                                                    </tr>
                                                )}
                                            </tbody>
                                        </Table>
                                        <div className="row">
                                            <div className="col-sm-12 text-center">
                                                <button className="btn btn-outline-secondary mr-2" type='button' onClick={() => helpers.push(ProductPricingBaseValue)}>Add new line</button>
                                                {values.pricingForm.length > 0 && (<button className="btn btn-outline-secondary ml-2" type='button' onClick={() => this.clearAll(values, setFieldValue)}>Delete all row</button>)}
                                            </div>
                                        </div>
                                    </Fragment>
                                );
                            }}

                        </FieldArray>
                    </Fragment>
                    {values.pricingForm.length > 0 && (
                        <div className="row mt-4">
                            <div className="col-sm-12 text-center">
                                <Button type="submit" className="btn btn-primary">
                                    Submit
                                </Button>
                            </div>
                        </div>
                    )}       
                    
                </Form>
            </div>
        );

    


    }


}

ProductPricingVCFrom.propTypes = {
    values: PropTypes.object,
    handleChange: PropTypes.func,
    data: PropTypes.object,
    setFieldValue: PropTypes.func,
    handleReset:PropTypes.func,
};
const withFormikConfig = {
    validationSchema: miscSchema,
    mapPropsToValues: ({ data }) => {
        console.log('+++vcForm data', data);
        return {
            pricingForm: data,
        };
    },

    handleSubmit(values) {
        console.log('values', values);
    },
};
export default withFormik(withFormikConfig)(ProductPricingVCFrom);
