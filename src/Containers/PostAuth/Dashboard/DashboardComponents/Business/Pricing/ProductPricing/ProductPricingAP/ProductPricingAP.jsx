import React, { Component } from 'react';
import ProductPricingFrom from './../ProductPricingFrom';
//import PropTypes from 'prop-types';



class ProductPricingAP extends Component {

    state={
        paymentData: [
            {
                chargeFrequency: 'MONTHLY',
                description: 'ACV',
                effectiveFrom: 'IMMEDIATELY',
                fees: 12.55,
                transactionType: 'ACH File',
            },
            {
                chargeFrequency: 'MONTHLY',
                description: 'RGB',
                effectiveFrom: 'IMMEDIATELY',
                fees: 100.55,
                transactionType: 'ACH File',
            }
        ]
    }

    

    render() {
        return (
            <div>
                <ProductPricingFrom data={this.state.paymentData}/>
            </div>
        );
    }
}

// ProductPricingAP.propTypes = {

// };

export default ProductPricingAP;
