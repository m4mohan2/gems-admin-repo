import React, { Component } from 'react';
import ProductPricingMiscFrom from './../ProductPricingMiscFrom';
//import PropTypes from 'prop-types';

class ProductPricingMF extends Component {
    state={
        miscData: [
            {
                chargeFrequency: 'MONTHLY',
                description: 'ACV',
                effectiveFrom: 'IMMEDIATELY',
                fees: 12.55,
                transactionType: 'ACH File',
            },
            {
                chargeFrequency: 'MONTHLY',
                description: 'RGB',
                effectiveFrom: 'IMMEDIATELY',
                fees: 100.55,
                transactionType: 'ACH File',
            }
        ]
    }

    

    render() {
        return (
            <div>
                <ProductPricingMiscFrom data={this.state.miscData}/>
            </div>
        );
    }
}

ProductPricingMF.propTypes = {

};

export default ProductPricingMF;
