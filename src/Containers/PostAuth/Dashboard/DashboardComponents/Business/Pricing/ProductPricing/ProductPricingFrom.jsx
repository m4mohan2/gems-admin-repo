/* eslint-disable no-unused-vars */
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Field, FieldArray, Form, withFormik, getIn } from 'formik';
import * as Yup from 'yup';
import NumberFormat from 'react-number-format';
import crossSmIcon from './../../../../../../../assets/crossSmIcon.png';
import CustomNumberFormat from './../../../../../../../Components/CustomNumberFormat/CustomNumberFormat';
import {
    Button,
    Col,
    FormControl,
    FormGroup,
    Image,
    Modal,
    Collapse,
    Row,
    Tab,
    Table,
    Tabs,
    Link
} from 'react-bootstrap';



const paymentSchema =Yup.object().shape({

    pricingForm: Yup.array().of(
        Yup.object().shape({
            chargeFrequency: Yup.string().required('Charge frequency is required!'),
            description: Yup.string(),
            effectiveFrom: Yup.string().required('Effective from is required!'),
            fees: Yup.number()
                .max(99999, 'maximum fees cannot be more than 5 digits')
            // .min(0.1, "minimum fees cannot be less than 0")
                .moreThan(0, 'fees should be greater than 0')
                .required('Please enter fees')
                .typeError('fees should be number')
                .test('fees', 'fees upto two decimal places', function(
                    value
                ) {
                    if (value) {
                        let isValid = false;
                        if (/^\d*(\.\d{0,2})?$/.test(value)) {
                            isValid = true;
                        }

                        return isValid;
                    }
                    return true;
                }),
            transactionType: Yup.string().required('Transaction type is required!'),
        })
    )
});


const ProductPricingBaseValue = {
    chargeFrequency: '',
    description: '',
    effectiveFrom: '',
    fees: parseFloat(0.00).toFixed(2),
    transactionType: '',
};


const transactionOpt = () => {
    return (
        <Fragment>
            <option value="">Select</option>
            <option value="achTransaction">ACH Transaction</option>
            <option value="check">Check</option>
            <option>ACH File</option>
            <option>Invoice and Payment Reconciliation</option>
            <option>Invoice Capture</option>
            <option>Client Cost Per Dynamic Negotiation</option>
            <option>Vendor Cost Per Dynamic Negotiation</option>
        </Fragment>
    );
};
const effectiveOpt = () => {
    return (
        <Fragment>
            <option value="">Select</option>
            <option value="IMMEDIATELY">Current billing Cycle</option>
            <option value="NEXT_BILLING_PERIOD">Next billing period</option>
        </Fragment>
    );
};

const periodOpt = () => {
    return (
        <Fragment>
            <option value="">Select</option>
            {/* <option value="IMMEDIATE">Immediate</option>
            <option value="WEEKLY">Weekly</option> */}
            <option value="DAILY">Daily</option>
            <option value="MONTHLY">Monthly</option>
            {/* <option value="QUARTERLY">Quarterly</option> */}
        </Fragment>
    );
};
const getErrorStyles = (errors, touched, fieldName) => {
    if (getIn(errors, fieldName) && getIn(touched, fieldName)) {
        return {
            border: '1px solid red'
        };
    }
};


class ProductPricingFrom extends Component {



    clearAll = (values, setFieldValue) => {
        setFieldValue('pricingForm', []);
    };

    render() {
        const { 
            values, 
            handleChange,
            // eslint-disable-next-line react/prop-types
            errors,
            // eslint-disable-next-line react/prop-types
            touched,
            // eslint-disable-next-line react/prop-types
            isValid,
            setFieldValue,
            handleReset
        } = this.props;
        console.log('errors ### ', errors, isValid, touched);
        return(
            <div>
                {console.log('++++++++++++++++ values +++++++++++++++', values)}
                <Form>
                    <Fragment>
                        <FieldArray
                            name="pricingForm">
                            {(helpers) => {
                                console.log('------------ helpers ------------', helpers);
                                return (
                                    <Fragment>
                                        <Table responsive hover>
                                            <thead className="theaderBg">
                                                <tr>
                                                    <th className="nameComment">Product</th>
                                                    <th>Fess(Per count)</th>
                                                    <th>Effective From</th>
                                                    <th>Occurence</th>
                                                    <th width="50">&nbsp;&nbsp;</th>
                                                    <th width="75"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {values.pricingForm.length > 0 ? (
                                                    values.pricingForm.map(
                                                        (prData, index) => {
                                                            return (
                                                                <tr key={index}>
                                                                    <td>
                                                                        <FormGroup controlId="formBasicText">
                                                                            <Field
                                                                                style={getErrorStyles(
                                                                                    errors,
                                                                                    touched,
                                                                                    `pricingForm.${index}.transactionType`
                                                                                )}
                                                                                component="select"
                                                                                name={`pricingForm.${index}.transactionType`}
                                                                                className={
                                                                                    'form-control'
                                                                                }
                                                                            >
                                                                                {transactionOpt()}
                                                                            </Field>

                                                                            <div className="mt-3">
                                                                                <Field
                                                                                    style={getErrorStyles(
                                                                                        errors,
                                                                                        touched,
                                                                                        `pricingForm.${index}.description`
                                                                                    )}
                                                                                    component="textarea"
                                                                                    name={`pricingForm.${index}.description`}
                                                                                    placeholder="Description"
                                                                                    className={
                                                                                        'form-control'
                                                                                    }
                                                                                />
                                                                            </div>
                                                                            <FormControl.Feedback />
                                                                        </FormGroup>
                                                                    </td>

                                                                    <td>
                                                                        <FormGroup controlId="formBasicText">
                                                                            <Field
                                                                                style={getErrorStyles(
                                                                                    errors,
                                                                                    touched,
                                                                                    `pricingForm.${index}.fees`
                                                                                )}
                                                                                component="input"
                                                                                type="number"
                                                                                name={`pricingForm.${index}.fees`}
                                                                                placeholder="0.00"
                                                                                className={
                                                                                    'form-control'
                                                                                }
                                                                                onBlur={e => {
                                                                                    if(e.target.value) {
                                                                                        const up = isNaN(parseFloat(e.target.value)) ? 0 : parseFloat(e.target.value);
                                                                                        setFieldValue(`pricingForm.${index}.fees`, up.toFixed(2));
                                                                                    } 
                                                                                }}
                                                                            />
                                                                            
                                                                            {/* <CustomNumberFormat
                                                                                value={prData.unitPrice}
                                                                            /> */}
                                                                            <FormControl.Feedback />
                                                                        </FormGroup>
                                                                    </td>

                                                                    <td>
                                                                        <FormGroup controlId="formBasicText">
                                                                            <Field
                                                                                style={getErrorStyles(
                                                                                    errors,
                                                                                    touched,
                                                                                    `pricingForm.${index}.effectiveFrom`
                                                                                )}
                                                                                name={`pricingForm.${index}.effectiveFrom`}
                                                                                component="select"
                                                                                className={
                                                                                    'form-control'
                                                                                }
                                                                            >
                                                                                {effectiveOpt()}
                                                                            </Field>
                                                                            <FormControl.Feedback />
                                                                        </FormGroup>
                                                                    </td>

                                                                    <td>
                                                                        <FormGroup controlId="formBasicText">
                                                                            <Field
                                                                                style={getErrorStyles(
                                                                                    errors,
                                                                                    touched,
                                                                                    `pricingForm.${index}.chargeFrequency`
                                                                                )}
                                                                                name={`pricingForm.${index}.chargeFrequency`}
                                                                                component="select"
                                                                                className={
                                                                                    'form-control'
                                                                                }
                                                                            >
                                                                                {periodOpt()}
                                                                            </Field>
                                                                            <FormControl.Feedback />
                                                                        </FormGroup>
                                                                    </td>

                                                                    <td>
                                                                        <Button className="btn btn-primary">
                                                                            Pause
                                                                        </Button>
                                                                    </td>
                                                                    <td width="75">
                                                                        <div className="showHideItem">
                                                                            <a
                                                                                to="#"
                                                                                className="smIconBg"
                                                                                onClick={() => helpers.remove(index)}
                                                                            >
                                                                                <Image
                                                                                    src={crossSmIcon}
                                                                                />
                                                                            </a>
                                                                        </div>
                                                                        
                                                                    </td>
                                                                </tr>
                                                            );
                                                        }
                                                    )
                                                ) : (
                                                    <tr>
                                                        <td colSpan='6' className="text-center">
                                                            <p>No items</p>
                                                        </td>
                                                    </tr>
                                                )}
                                            </tbody>
                                        </Table>
                                        <div className="row">
                                            <div className="col-sm-12 text-center">
                                                <button className="btn btn-outline-secondary mr-2" type='button' onClick={() => helpers.push(ProductPricingBaseValue)}>Add new line</button>
                                                {values.pricingForm.length > 0 && (<button className="btn btn-outline-secondary ml-2" type='button' onClick={() => this.clearAll(values, setFieldValue)}>Delete all row</button>)}
                                            </div>
                                        </div>
                                    </Fragment>
                                );
                            }}

                        </FieldArray>
                    </Fragment>
                    {values.pricingForm.length > 0 && (
                        <div className="row mt-4">
                            <div className="col-sm-12 text-center">
                                <Button type="submit" className="btn btn-primary">
                                    Submit
                                </Button>
                            </div>
                        </div>
                    )}       
                    
                </Form>
            </div>
        );

    


    }


}

ProductPricingFrom.propTypes = {
    values: PropTypes.object,
    handleChange: PropTypes.func,
    data: PropTypes.object,
    setFieldValue: PropTypes.func,
    handleReset:PropTypes.func,
};
const withFormikConfig = {
    validationSchema: paymentSchema,
    mapPropsToValues: ({ data }) => {
        console.log('+++pricingForm data', data);
        return {
            pricingForm: data,
        };
    },

    handleSubmit(values) {
        console.log('values', values);
    },
};
export default withFormik(withFormikConfig)(ProductPricingFrom);
