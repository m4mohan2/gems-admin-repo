import React, { Component } from 'react';
import ProductPricingVCFrom from './../ProductPricingVCFrom';
//import PropTypes from 'prop-types';

class ProductPricingVC extends Component {
    state={
        vcData: [
            {
                product: 'Card on file',
                description: 'ACV',
                bps: 12.55,
            },
            
        ]
    }

    

    render() {
        return (
            <div>
                <ProductPricingVCFrom data={this.state.vcData}/>
            </div>
        );
    }
}

ProductPricingVC.propTypes = {

};

export default ProductPricingVC;
