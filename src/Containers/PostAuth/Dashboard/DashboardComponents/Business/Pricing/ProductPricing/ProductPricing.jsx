import React, { Component } from 'react';
import {
    Tabs,
    Tab,
} from 'react-bootstrap';
import ProductPricingAP from './ProductPricingAP/ProductPricingAP';
import ProductPricingAR from './ProductPricingAR/ProductPricingAR';
import ProductPricingVC from './ProductPricingVC/ProductPricingVC';
import ProductPricingMF from './ProductPricingMF/ProductPricingMF';

class ProductPricing extends Component {
    state = {
        key: 'accountPayable',
    }    
    render() {
        return (
            <div>
                <Tabs
                    id="controlled-tab-example"
                    activeKey={this.state.key}
                    onSelect={key => this.setState({ key })}
                    className="subTab px-4 subTabBg"
                >
                    <Tab eventKey="accountPayable" title="Account Payable"><ProductPricingAP/></Tab>
                    <Tab eventKey="accountReceivable" title="Accounts Receivable"><ProductPricingAR/></Tab>
                    <Tab eventKey="virtualCard" title="Virtual Card"><ProductPricingVC/></Tab>
                    <Tab eventKey="miscFees" title="Misc Fees"><ProductPricingMF/></Tab>
                </Tabs>   
            </div>
        );
    }
}

export default ProductPricing;
