import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import axios from './../../../../../../../shared/eaxios';
import LoadingSpinner from './../../../../../../../Components/LoadingSpinner/LoadingSpinner';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';
//let moment = require('moment');
import styles from './ProductUsage.module.scss';
import { handleProdInvoiceDes } from './../../../../../../../redux/actions/product';
import { Modal } from 'react-bootstrap';
import { Formik, Form, Field } from 'formik';
import SuccessIco from './../../../../../../../assets/success-ico.png';
import ProdUsageHistory from './ProdUsageHistory';
import * as Yup from 'yup';
//import LastSix from './LastSix';
import { handleProdHisStat, handleReStat } from './../../../../../../../redux/actions/product';
import * as Fields from './productUsageFields';

let moment = require('moment');

let initialValues = {
    achFees: 0,
    checkFees: 0,
    checkFilesFees: 0,
    achEffectiveForm: '',
    checkEffectiveForm: '',
    checkFilesEffectiveForm: '',
    achChargeFrequency: '',
    checkChargeFrequency: '',
    checkFilesChargeFrequency: '',
};


const editProductUsageSchema = Yup.object().shape({
    achFees: Yup
        .number()
        .moreThan(-1, 'Please enter proper ACH fees')
        .required('Please enter ACH fees'),
    checkFees: Yup
        .number()
        .moreThan(-1, 'Please enter proper check fees')
        .required('Please enter check fees'),
    checkFilesFees: Yup
        .number()
        .moreThan(-1, 'Please enter proper check files fees')
        .required('Please enter check files fees'),

    checkEffectiveForm: Yup
        .string()
        .required('Please select an option for product fees'),
    achEffectiveForm: Yup
        .string()
        .required('Please select an option for product fees'),
    checkFilesEffectiveForm: Yup
        .string()
        .required('Please select an option for product fees'),

    achChargeFrequency: Yup
        .string()
        .required('Please select an option for period frequency'),

    checkChargeFrequency: Yup
        .string()
        .required('Please select an option for period frequency'),

    checkFilesChargeFrequency: Yup
        .string()
        .required('Please select an option for period frequency'),              


});


class ProductUsage extends Component {

    state = {
        productStatLoader: false,
        errorMessge: null,
        productStatlist: [],
        prodUsageEditModal: false,
        updateLoader: false,
        ChngeProductUsageError: null,
        showConfirMmsg: false,
        alertMsg: false,

        GetEditProductStatLoader: false,
        GetEditProductStatlist: [],
        GetEditProductStatError: null,
        selectedMonth: null,
        seletedYear: null,

        startDate: null,
        endDate: null,
        slcTimeStamp: null,
        prevSix: []
    }

    _isMounted = false;


    handleinitialdata = () => {
        let currentMonth = new Date().getMonth() + 1;
        let currentYear = new Date().getFullYear();
        console.log('+++++++++++++++++', currentYear);
        this.setState({
            selectedMonth: currentMonth,
            seletedYear: currentYear
        }, () => { this.handelGetProductStat(); console.log('select-----------------------------', this.state.seletedYear);});


    }
    componentDidMount() {
        this._isMounted = true;
        this.LastSix();
        this.handleinitialdata();

    }
    componentDidUpdate(prevProps) {

        console.log('calling from update ProductUsage');
        if (this.props.reacload) {
            this.props.handleReStat(false);
            this.handleinitialdata();
        }
        if (prevProps !== this.props) {
            console.log('++++++++++++++++ I am here prevProps !== this.props +++++++++++++++' , this.props.prodpayselectedMonth, this.props.prodpayseletedYear);
            if (this.props.prodpayselectedMonth) {
                console.log('month block');
                this.setState({
                    selectedMonth: this.props.prodpayselectedMonth,
                    seletedYear: this.props.prodpayseletedYear,
                    slcTimeStamp: this.props.prodpayselectedMonth + '-' + this.props.prodpayseletedYear
                    //isDisable:true
                },()=> {
                    this.handelGetProductStat();
                    this.LastSix();
                } 
                );
            }


        }
    }
    componentWillUnmount() {
        this._isMounted = false;
    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }

        return errorMessge;
    }

    LastSix = () => {
        let d, m, prevSix = [];
        d = new Date();
        m = d.getMonth() + 1;
        for (let i = 6; i > 0; i--) {
            prevSix.push(new Date().setMonth(m - i));
        }
        prevSix = prevSix.reverse();
        prevSix = prevSix.map((m, i) => (
            < option key={m.toString()} value={(new Date(m).getMonth() + 1) + '-' + new Date(m).getFullYear()} >
                {i === 0 ? 'This month' : moment(m).format('MMMM')}
            </option>
        ));
        this.setState({ prevSix });
    };

    handleGetYrMo = (e) => {
        
        let temp = e.target.value.split('-');
        this.setState({
            slcTimeStamp: e.target.value,
            selectedMonth: temp[0] ,
            seletedYear: temp[1]
        }, () => {
            
            let data = {};
            data.mode = 'ALL';
            data.id = 0;
            data.selectedMonth = this.state.selectedMonth;
            data.seletedYear = this.state.seletedYear;
            this.props.handleTrasactionMode(data);

            this.handelGetProductStat();
        });
    }

    closeAlertMsg = () => {
        this.setState({
            alertMsg: false
        });
    }

    openAlertMsg = () => {
        this.setState({
            alertMsg: true
        });

    }



    handleConfirmReviewClose = () => {
        this.setState({
            showConfirMmsg: false,
        }, () => this.handleProdUsageClose());
    };

    handleConfirmReviewShow = () => {
        this.setState({ showConfirMmsg: true });
    };



    handelGetProductStat = () => {
        this.setState({
            productStatLoader: true,
            productStatlist: []
        });


        axios
            .get(
                `billings/product/usage/${this.props.businessID}?month=${this.state.selectedMonth}&year=${this.state.seletedYear}`
            )
            .then(res => {
                console.log('%%%%%%%%%%% product usage %%%%%%%%%%%%%%%%%', res.data.length);
                if (res.data.length > 0) {
                    if (this._isMounted) {
                        //const isfuturepay = res.data.filter(data => data.isFutureCharge === false);

                        this.setState({
                            productStatlist: res.data,
                            startDate: res.data[0].startDate,
                            endDate: res.data[0].endDate,
                            productStatLoader: false,
                        }
                        );
                    }
                } else if (res.data.length == 0) {

                    this.setState({
                        startDate: '',
                        endDate: '',
                        errorMessge: 'No records found',
                        productStatLoader: false
                    });

                }

            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    errorMessge: errorMsg,
                    productStatLoader: false
                });
                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);

            });
    }


    // get api call for product add edit
    handelGetEditProductStat = () => {
        this.setState({
            GetEditProductStatLoader: true
        });

        axios
            .get(
                `billings/details/${this.props.businessID}`
            )
            .then(res => {

                if (this._isMounted) {
                    if (res.data.length > 0) {
                        initialValues = {

                            achFees: `${res.data[0][Fields.PRODUCT_STAT_AMOUNT]}`,
                            checkFees: `${res.data[1][Fields.PRODUCT_STAT_AMOUNT]}`,
                            checkFilesFees: `${res.data[2][Fields.PRODUCT_STAT_AMOUNT]}`,

                            achEffectiveForm: `${res.data[0][Fields.PRODUCT_STAT_EFFECTIVE_FROM]}`,
                            checkEffectiveForm: `${res.data[1][Fields.PRODUCT_STAT_EFFECTIVE_FROM]}`,
                            checkFilesEffectiveForm: `${res.data[2][Fields.PRODUCT_STAT_EFFECTIVE_FROM]}`,

                            achChargeFrequency: `${res.data[0][Fields.PRODUCT_STAT_CHARGE_FREQUENCY]}`,
                            checkChargeFrequency: `${res.data[1][Fields.PRODUCT_STAT_CHARGE_FREQUENCY]}`,
                            checkFilesChargeFrequency: `${res.data[2][Fields.PRODUCT_STAT_CHARGE_FREQUENCY]}`,
                            
                        };

                    }
                    this.setState({
                        GetEditProductStatlist: res.data,
                        GetEditProductStatLoader: false
                    }, () => {
                    }


                    );
                }
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    GetEditProductStatError: errorMsg,
                    GetEditProductStatLoader: false
                });
                setTimeout(() => {
                    this.setState({ GetEditProductStatError: null });
                }, 5000);

            });
    }


    handleInvoiceCount = (param) => {
        let data = {};
        data.mode = param.productDescription;
        data.id = param.id;
        data.startDate = param.startDate;
        data.selectedMonth = this.state.selectedMonth;
        data.seletedYear = this.state.seletedYear;
        data.paymodeDisable= true;
        // if (param.productDescription !== 'Check File') {
        this.props.goToInvoice();
        this.props.handleTrasactionMode(data);
        // }
    }

    handleProdUsageEdit = () => {
        this.setState({
            prodUsageEditModal: true,
        });
    }

    handleProdUsageClose = () => {
        this.setState({
            prodUsageEditModal: false,
        });
    }

    handleEditProductUsage = (param) => {

        this.setState({
            updateLoader: true,
        });

        axios
            .post(
                `billings/create/${this.props.businessID}`, param
            ).then((res) => {
                console.log('change plan', res);
                this.setState({
                    updateLoader: false,
                    alertMsg: false,
                    showConfirMmsg: true,
                });
                this.handelGetProductStat();
                this.props.handleReload(true);
                this.props.handleReStat(true);
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    updateLoader: false,
                    //alertMsg: false,
                    ChngeProductUsageError: errorMsg,


                });
                this.handelGetProductStat();
                this.props.handleReload(true);
                this.props.handleReStat(true);
                setTimeout(() => {
                    this.setState({ ChngeProductUsageError: null, alertMsg: false });
                }, 5000);

            });

    }




    handleSubmit = (values) => {

        //console.log('product usage edit', values);

        let productStatlist = this.state.GetEditProductStatlist;
        productStatlist[0].amount = (values.achFees == '' ? 0 : values.achFees) ;
        productStatlist[1].amount = (values.checkFees == '' ? 0 : values.checkFees);
        productStatlist[2].amount = (values.checkFilesFees == '' ? 0 : values.checkFilesFees);

        productStatlist[0].effectiveForm = values.achEffectiveForm;
        productStatlist[1].effectiveForm = values.checkEffectiveForm;
        productStatlist[2].effectiveForm = values.checkFilesEffectiveForm;

        productStatlist[0].chargeFrequency = values.achChargeFrequency;
        productStatlist[1].chargeFrequency = values.checkChargeFrequency;
        productStatlist[2].chargeFrequency = values.checkFilesChargeFrequency;


        console.log('product usage edit ----------- productStatlist', productStatlist);

        this.setState({
            productStatlist
        }, () => this.openAlertMsg());
        
    }

    formatDate = date => {
        let d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        let temp =  [year, month, day].join('-');
        return moment(temp).format('MMM DD YYYY');
    }

    effectiveOpt=()=>{
        return(
            <Fragment>
                <option value="">
                    Select
                </option>
                <option value='IMMEDIATELY'>Today</option>
                <option value='NEXT_BILLING_PERIOD'>Next billing period</option>
            </Fragment>
        );  
    }

    periodOpt = () =>{
        return (
            <Fragment>
                <option value="">
                    Select
                </option>
                <option value="IMMEDIATE">Immediate</option>
                <option value="WEEKLY">Weekly</option>
                <option value="MONTHLY">Monthly</option>
                <option value="QUARTERLY">Quarterly</option>
            </Fragment>
        ); 

    }



    render() {
        const {

            prevSix
        } = this.state;
        return (
            <div>

                <div className="prodUsageOuter p-3">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="border rounded bg-white mb-3">
                                <div className="bg-light clearfix p-2">
                                    <div className="float-left pt-2 pl-2 font-weight-500">Product Usage Statistics</div>
                                </div>
                                <div className="p-3 clearfix">
                                    <span className="float-left text-dark pt-3">{(`Period: ${this.formatDate(new Date(this.state.seletedYear, this.state.selectedMonth - 1, 1))} - ${this.formatDate(new Date(this.state.seletedYear, this.state.selectedMonth, 0))}`)}</span>
                                    {this.props.editOption && <button className="btn btn-primary float-right ml-3 px-3" onClick={() => { this.handleProdUsageEdit(); this.handelGetEditProductStat(); }}>Edit</button>}
                                    <span className={'float-right mt-1 form-group'}>
                                        <select className='mr-3 pr-3 py-1 form-control' onChange={(e) => this.handleGetYrMo(e)} value={this.state.slcTimeStamp}>
                                            {prevSix}
                                        </select>

                                    </span>
                                </div>

                                <div className="p-3">
                                    <table className="table">
                                        <thead>
                                            <tr className={styles.cusTh}>
                                                <th scope="col">Product</th>
                                                {/* <th scope="col">Active</th> */}
                                                <th scope="col" className="text-right pr-3">Fees <div className="font-weight-light">(Per count)</div></th>
                                                <th scope="col">Count</th>
                                                <th scope="col" className="text-right pr-3">Amount</th>
                                                <th scope="col">Effective Date From</th>
                                                <th scope="col">Effective Date Till</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.productStatLoader
                                                ?
                                                <tr>
                                                    <td colSpan='6'>
                                                        <LoadingSpinner />
                                                    </td>
                                                </tr>
                                                :
                                                this.state.productStatlist.length > 0 ?
                                                    (this.state.productStatlist.map(
                                                        productStat =>
                                                            <tr key={productStat.id}>

                                                                <td>
                                                                    {productStat.productDescription}
                                                                    {productStat.futureChargeDetails ? (
                                                                        <Fragment>
                                                                            <span className="text-left futurePay">
                                                                                From next billing period:
                                                                                <NumberFormat
                                                                                    value={productStat.futureChargeDetails.amount}
                                                                                    displayType={'text'}
                                                                                    thousandSeparator={true}
                                                                                    prefix={'$'}
                                                                                    decimalScale={2}
                                                                                    fixedDecimalScale={true}
                                                                                    thousandsGroupStyle={'thousand'}
                                                                                    renderText={value => <span>{value}</span>}
                                                                                />
                                                                            </span>
                                                                            <span className="futurePay futurePayDate"> {productStat.futureChargeDetails.effectiveStartDate}</span>
                                                                           

                                                                        </Fragment>
                                                                    ) : null




                                                                    }

                                                                </td>
                                                                {/* <td>{productStat.isActive === true ? 'Yes' : 'No' }</td> */}
                                                             

                                                                {/* <td>{productStat.amount}</td> */}
                                                                <td className="text-right pr-3">
                                                                    <NumberFormat
                                                                        value={productStat.amount}
                                                                        displayType={'text'}
                                                                        thousandSeparator={true}
                                                                        fractionSize={2}
                                                                        prefix={'$'}
                                                                        decimalScale={2}
                                                                        fixedDecimalScale={true}
                                                                        thousandsGroupStyle={'thousand'}
                                                                        renderText={value => <span>{value}</span>}
                                                                    />
                                                                </td>
                                                                <td onClick={() => { this.handleInvoiceCount(productStat); }}><span className={'text-primary ml-3 pointer'}><u>{productStat.invoiceCount}</u></span></td>
                                                                <td className="text-right pr-3">
                                                                    <NumberFormat
                                                                        value={productStat.totalAmount}
                                                                        displayType={'text'}
                                                                        thousandSeparator={true}
                                                                        fractionSize={2}
                                                                        prefix={'$'}
                                                                        decimalScale={2}
                                                                        fixedDecimalScale={true}
                                                                        thousandsGroupStyle={'thousand'}
                                                                        renderText={value => <span>{value}</span>}
                                                                    />
                                                                </td>
                                                                <td>{productStat.effectiveStartDate}</td>
                                                                <td>{productStat.effectiveEndDate}</td>
                                                            </tr>
                                                    ))
                                                    : this.state.errorMessge ? <tr>
                                                        <td colSpan={6}>
                                                            <p className="text-center">{this.state.errorMessge}</p>
                                                        </td>
                                                    </tr> : (
                                                        <tr>
                                                            <td colSpan={6}>
                                                                <p className="text-center">No records found</p>
                                                            </td>
                                                        </tr>
                                                    )
                                            }

                                        </tbody>
                                    </table>
                                </div>

                                {this.props.prodHistory && <div className="p-3"><ProdUsageHistory /></div>}

                            </div>
                        </div>
                    </div>
                </div>

                <Modal
                    show={this.state.prodUsageEditModal}
                    onHide={this.handleProdUsageClose}
                    // size="lg"
                    className="right medium  noPadding slideModal usageEditModal"
                >
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body className="text-center">
                        <div>

                            <div className="modalHeader">
                                <div className="row">
                                    <div className="text-left col-sm-12">
                                        <h1>Edit Product Usage</h1>
                                    </div>
                                </div>


                            </div>
                            <div className="modalBody content-body noTabs">
                                {this.state.GetEditProductStatLoader ? <LoadingSpinner /> :
                                    <div className="row">
                                        <div className="col-sm-12">
                                            <Formik
                                                initialValues={initialValues}
                                                validationSchema={editProductUsageSchema}
                                                onSubmit={this.handleSubmit}
                                                enableReinitialize
                                            >
                                                {({ values,
                                                    errors,
                                                    touched,
                                                    //handleChange,
                                                    //handleBlur 
                                                }) => (
                                                    <Form>

                                                        <table className="table">
                                                            <thead>
                                                                <tr className={styles.cusTh}>
                                                                    <th scope="col" className='text-left'>Product</th>
                                                                    <th scope="col" className='text-left'>Fees <span className="font-weight-light">(Per count)</span></th>
                                                                    <th scope="col" className='text-left'>Effective From</th>
                                                                    <th scope="col" className='text-left'>Period</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                                <tr>
                                                                    <td className='text-left'>ACH Transactions</td>
                                                                    <td className='text-left'>
                                                                        <Field
                                                                            name="achFees"
                                                                            type="number"
                                                                            className={'form-control'}
                                                                            autoComplete="nope"
                                                                            placeholder='0'
                                                                            value={values.achFees || ''}
                                                                        />
                                                                        {errors.achFees && touched.achFees ? (
                                                                            <span className="errorMsg pl-3">{errors.achFees}</span>
                                                                        ) : null}

                                                                    </td>
                                                                    <td className='text-left'>
                                                                        <div className='form-group' controlid="formBasicText">
                                                                            <Field
                                                                                name="achEffectiveForm"
                                                                                component="select"
                                                                                className={'form-control'}
                                                                                autoComplete="nope"
                                                                                placeholder='0'
                                                                                value={values.achEffectiveForm || ''}
                                                                            >
                                                                                {this.effectiveOpt()}
                                                                            </Field>


                                                                            {errors.achEffectiveForm && touched.achEffectiveForm ? (
                                                                                <span className="errorMsg pl-3">{errors.achEffectiveForm}</span>
                                                                            ) : null}
                                                                        </div>


                                                                    </td>
                                                                    <td className='text-left'>

                                                                        <div className='form-group' controlid="formBasicText">
                                                                            <Field
                                                                                name="achChargeFrequency"
                                                                                component="select"
                                                                                className={'form-control'}
                                                                                value={values.achChargeFrequency || ''}
                                                                            >
                                                                                {this.periodOpt()}
                                                                            </Field>


                                                                            {errors.achChargeFrequency && touched.achChargeFrequency ? (
                                                                                <span className="errorMsg pl-3">{errors.achChargeFrequency}</span>
                                                                            ) : null}
                                                                        </div>      
                                                                    </td>               
                                                                </tr>

                                                                <tr>
                                                                    <td className='text-left'>Check</td>
                                                                    <td className='text-left'>
                                                                        <Field
                                                                            name="checkFees"
                                                                            type="number"
                                                                            className={'form-control'}
                                                                            autoComplete="nope"
                                                                            placeholder='0'
                                                                            value={values.checkFees || ''}
                                                                        />
                                                                        {errors.checkFees && touched.checkFees ? (
                                                                            <span className="errorMsg pl-3">{errors.checkFees}</span>
                                                                        ) : null}

                                                                    </td>
                                                                    <td className='text-left'>
                                                                        <div className='form-group' controlid="formBasicText">
                                                                            <Field
                                                                                name="checkEffectiveForm"
                                                                                component="select"
                                                                                className={'form-control'}
                                                                                autoComplete="nope"
                                                                                placeholder='0'
                                                                                value={values.checkEffectiveForm || ''}
                                                                            >
                                                                                {this.effectiveOpt()}
                                                                            </Field>


                                                                            {errors.checkEffectiveForm && touched.checkEffectiveForm ? (
                                                                                <span className="errorMsg pl-3">{errors.checkEffectiveForm}</span>
                                                                            ) : null}
                                                                        </div>


                                                                    </td>
                                                                    <td className='text-left'>
                                                                        <div className='form-group' controlid="formBasicText">
                                                                            <Field
                                                                                name="checkChargeFrequency"
                                                                                component="select"
                                                                                className={'form-control'}
                                                                                value={values.checkChargeFrequency || ''}
                                                                            >
                                                                                {this.periodOpt()}
                                                                            </Field>


                                                                            {errors.checkChargeFrequency && touched.checkChargeFrequency ? (
                                                                                <span className="errorMsg pl-3">{errors.checkChargeFrequency}</span>
                                                                            ) : null}
                                                                        </div>  
                                                                    </td>            
                                                                </tr>

                                                                <tr>
                                                                    <td className='text-left'>Check File</td>
                                                                    <td className='text-left'>
                                                                        <Field
                                                                            name="checkFilesFees"
                                                                            type="number"
                                                                            className={'form-control'}
                                                                            autoComplete="nope"
                                                                            placeholder='0'
                                                                            value={values.checkFilesFees || ''}
                                                                        />
                                                                        {errors.checkFilesFees && touched.checkFilesFees ? (
                                                                            <span className="errorMsg pl-3">{errors.checkFilesFees}</span>
                                                                        ) : null}

                                                                    </td>
                                                                    <td className='text-left'>
                                                                        <div className='form-group' controlid="formBasicText">
                                                                            <Field
                                                                                name="checkFilesEffectiveForm"
                                                                                component="select"
                                                                                className={'form-control'}
                                                                                autoComplete="nope"
                                                                                placeholder='0'
                                                                                value={values.checkFilesEffectiveForm || ''}
                                                                            >
                                                                                {this.effectiveOpt()}
                                                                            </Field>


                                                                            {errors.checkFilesEffectiveForm && touched.checkFilesEffectiveForm ? (
                                                                                <span className="errorMsg pl-3">{errors.checkFilesEffectiveForm}</span>
                                                                            ) : null}
                                                                        </div>


                                                                    </td>
                                                                    <td className='text-left'>
                                                                        <div className='form-group' controlid="formBasicText">
                                                                            <Field
                                                                                name="checkFilesChargeFrequency"
                                                                                component="select"
                                                                                className={'form-control'}
                                                                                value={values.checkFilesChargeFrequency || ''}
                                                                            >
                                                                                {this.periodOpt()}
                                                                            </Field>
                                                                            {errors.checkFilesChargeFrequency && touched.checkFilesChargeFrequency ? (
                                                                                <span className="errorMsg pl-3">{errors.checkFilesChargeFrequency}</span>
                                                                            ) : null}
                                                                        </div>
                                                                    </td>            
                                                                </tr>

                                                            </tbody>
                                                        </table>

                                                        <div className="m-auto text-center">
                                                            {/* <button className="btn but-gray mt-3" onClick={() => this.handleProdUsageClose()}>Return</button> */}
                                                            <button className="btn btn-primary ml-3 mt-3" type="submit">Submit</button>
                                                        </div>
                                                    </Form>
                                                )}
                                            </Formik>

                                        </div>
                                    </div>
                                }


                            </div>



                        </div>
                    </Modal.Body>

                </Modal>


                {/*======  confirmation popup  ===== */}

                <Modal
                    show={this.state.showConfirMmsg}
                    onHide={this.handleConfirmReviewClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <div className='row'>
                            <div className="text-center col-sm-12">
                                <image src={SuccessIco} />
                            </div>
                        </div>
                        <div className='row'>
                            <div className="text-center my-3 col-sm-12">
                                <h6>Product usage has been successfully updated</h6>
                            </div>
                        </div>
                        <div className="text-center">
                            <button
                                onClick={this.handleConfirmReviewClose}
                                className="but-gray btn btn-dark"
                            >
                                Done
                            </button>
                        </div>

                    </Modal.Body>
                </Modal>


                {/*======  before action confirmation massage popup  =====*/}
                <Modal
                    show={this.state.alertMsg}
                    onHide={this.closeAlertMsg}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <div className="m-auto text-center">
                            {this.state.ChngeProductUsageError ? <div className="text-danger">{this.state.ChngeProductUsageError}</div> : <h6 className="mb-3">
                                {this.state.updateLoader ? <LoadingSpinner /> : null}
                                <span>Do you want to edit product usage</span>
                            </h6>}

                        </div>
                        <div className="m-auto text-center">
                            <button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.closeAlertMsg()}>Return</button>
                            {!this.state.ChngeProductUsageError ? <button className="btn btn-primary" onClick={() => this.handleEditProductUsage(this.state.productStatlist)}>Confirm</button> : null}
                        </div>

                    </Modal.Body>
                </Modal>





            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        businessID: state.business.businessData.id,
        reacload: state.product.reacload,
        prodpayselectedMonth: state.product.prodpayselectedMonth,
        prodpayseletedYear: state.product.prodpayseletedYear,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        handleTrasactionMode: (pram) => dispatch(handleProdInvoiceDes(pram)),
        handleReload: data => dispatch(handleProdHisStat(data)),
        handleReStat: data => dispatch(handleReStat(data)),
    };
};




ProductUsage.propTypes = {
    businessID: PropTypes.number,
    handleTrasactionMode: PropTypes.func,
    goToInvoice: PropTypes.func,
    editOption: PropTypes.bool,
    prodHistory: PropTypes.bool,
    handleReload: PropTypes.func,
    handleReStat: PropTypes.func,
    reacload: PropTypes.bool,
    prodpayseletedYear: PropTypes.number,
    prodpayselectedMonth: PropTypes.number,

};

export default connect(mapStateToProps, mapDispatchToProps)(ProductUsage);
