import React, {Fragment } from 'react';
let moment = require('moment');
import PropTypes from 'prop-types';

const LastSix = ()=>  {
    
    let d, m, prevSix = [];
    d = new Date();
    m = d.getMonth() + 1;
    for (let i = 6; i > 0; i--) {
        let temp = d.setMonth(m - i);

        prevSix.push(
            <option key={temp.toString()} value={temp.toString()} >
                {i == 1 ? 'This month' : moment(temp).format('MMMM')}
            </option>
        );
    }
    prevSix = prevSix.reverse();
    return (
        <Fragment>
            {prevSix}
        </Fragment>

    );
   
};

LastSix.propTypes = {
   
    selection: PropTypes.string,

};

export default LastSix;
