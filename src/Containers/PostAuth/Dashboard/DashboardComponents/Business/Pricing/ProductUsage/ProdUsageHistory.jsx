import React, { Component } from 'react';
import axios from './../../../../../../../shared/eaxios';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';
import LoadingSpinner from '../../../../../../../Components/LoadingSpinner/LoadingSpinner';
import Pagination from 'react-js-pagination';
import styles from './ProductUsage.module.scss';
import { handleProdHisStat } from './../../../../../../../redux/actions/product';
import {
    Row,
    Col,
} from 'react-bootstrap';

class ProdUsageHistory extends Component {

    state = {
        historyLists: [],
        historyLoader:false,
        historyError:null,
        activePage: 1,
        totalCount: 0,
        itemPerPage: 250,
    };
    _isMounted = false;


    componentDidMount() {
        this._isMounted = true;
        this.handelGetHistoryList();
    }
    componentDidUpdate() {

        if (this.props.statload == true){
            this.props.handleReload(false);
            this.handelGetHistoryList();
        }
        console.log('calling from update');

    }
    componentWillUnmount() {
        this._isMounted = false;
    }

    displayError = (e) => {
        console.log('displayError', e.data.message);
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }

        return errorMessge;
    }

    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber });
        this.handelGetHistoryList(pageNumber > 0 ? pageNumber - 1 : 0);
    };


    handleChangeItemPerPage = (e) => {
        this.setState({ itemPerPage: e.target.value },
            () => {
                this.handelGetHistoryList(this.state.activePage > 0 ? this.state.activePage - 1 : 0);
            });
    }

    displayError = (e) => {
        console.log('displayError', e.data.message);
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }

        return errorMessge;
    }

    handelGetHistoryList = (since = 0)=>{

        this.setState({
            historyLoader:true,
        });

        axios
            .get(

                `billings/old/product/usage/${this.props.businessID}?since=${since}&limit=${this.state.itemPerPage}`
            )
            .then(res => {
                
                console.log('billings/old/product/usage/**********', res.data);
                const historyLists = res.data.entries;
                const totalCount = res.data.total;
                if (this._isMounted) {
                    this.setState({
                        historyLists: historyLists,
                        totalCount: totalCount,
                        historyLoader: false
                    });
                }

                console.log('business list-----------------------', this.state.historyLists);
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    historyError: errorMsg,
                    historyLoader: false
                });
                setTimeout(() => {
                    this.setState({ historyError: null });
                }, 5000);

            });
    }


    render() {
        return (
            <div>
                <h6 className="p-3">Product Usage History</h6>
                <table className="table">
                    <thead>
                        <tr className={styles.cusTh}>
                            <th scope="col">Product</th>
                            <th scope="col" className="text-right pr-3">Fees <div className="font-weight-light">(Per count)</div></th>
                            <th scope="col">Effective From</th>
                            <th scope="col">Effective Date From</th>
                            <th scope="col">Effective Date Till</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.historyLoader
                            ?
                            <tr>
                                <td colSpan='4'>
                                    <LoadingSpinner />
                                </td>
                            </tr>
                            :
                            this.state.historyLists.length > 0 ?
                                (this.state.historyLists.map(
                                    productStat =>
                                        <tr key={productStat.id}>
                                            <td>{productStat.productDescription}</td>
                                            {/* <td>{productStat.amount}</td> */}
                                            <td className="text-right pr-3">
                                                <NumberFormat
                                                    value={productStat.amount}
                                                    displayType={'text'}
                                                    thousandSeparator={true}
                                                    fractionSize={2}
                                                    prefix={'$'}
                                                    decimalScale={2}
                                                    fixedDecimalScale={true}
                                                    thousandsGroupStyle={'thousand'}
                                                    renderText={value => <span>{value}</span>}
                                                />
                                            </td>
                                            <td>{productStat.effectiveForm == 'IMMEDIATELY' ? 'Immediately to new transactions' :'Next billing period'}</td>
                                            <td>{productStat.effectiveStartDate}</td>
                                            <td>{productStat.effectiveEndDate}</td>
                                        </tr>
                                ))
                                : this.state.historyError ? <tr>
                                    <td colSpan={6}>
                                        <p className="text-center">{this.state.historyError}</p>
                                    </td>
                                </tr> : (
                                    <tr>
                                        <td colSpan={6}>
                                            <p className="text-center">No records found</p>
                                        </td>
                                    </tr>
                                )
                        }


                    </tbody>
                </table>
                {this.state.totalCount ? (
                    <Row className="px-3">
                        <Col md={4} className="d-flex flex-row mt-20">
                            <span className="mr-2 mt-2 font-weight-500">Items per page</span>
                            <select
                                id={this.state.itemPerPage}
                                className="form-control truncatefloat-left w-90"
                                onChange={this.handleChangeItemPerPage}
                                value={this.state.itemPerPage}>
                                <option value='50'>50</option>
                                <option value='100'>100</option>
                                <option value='150'>150</option>
                                <option value='200'>200</option>
                                <option value='250'>250</option>

                            </select>
                        </Col>
                        <Col md={8}>
                            <div className="paginationOuter text-right">
                                <Pagination
                                    activePage={this.state.activePage}
                                    itemsCountPerPage={this.state.itemPerPage}
                                    totalItemsCount={this.state.totalCount}
                                    onChange={this.handlePageChange}
                                />
                            </div>
                        </Col>
                    </Row>
                ) : null}    
            </div>
        );
    }
}


const mapStateToProps = state => {
    return {
        businessID: state.business.businessData.id,
        statload: state.product.statload
    };
};
const mapDispatchToProps = dispatch => {
    return {
        handleReload: data => dispatch(handleProdHisStat(data))
    };
};

ProdUsageHistory.propTypes = {
    businessID: PropTypes.number,
    handleReload: PropTypes.func,
    statload: PropTypes.bool,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProdUsageHistory);

