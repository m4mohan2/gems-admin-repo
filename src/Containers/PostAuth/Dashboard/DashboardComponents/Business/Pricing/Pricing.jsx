import React, { Component } from 'react';
import {
    Tabs,
    Tab,
} from 'react-bootstrap';
import LoadingSpinner from './../../../../../../Components/LoadingSpinner/LoadingSpinner';
import Account from './Account/Account';
import ProductUsage from './ProductUsage/ProductUsage';
//import ProductPricing from './ProductPricing/ProductPricing';
// import Invoices from './../../Invoice/BusinessInvoice';
import BillingHistory from './BillingHistory/BillingHistory';
import BillingTansaction from './BillingTansaction/BillingTansaction';
import axios from './../../../../../../shared/eaxios';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
//const ProductUsage = React.lazy(() => import('./ProductUsage/ProductUsage'));
import { handleProdInvoiceDes } from './../../../../../../redux/actions/product';

class Pricing extends Component {
    state = {
        key: 'account',
        accountEditLoader: true,

        billingHistryList: [],
        billingHistryLstLoader: false,
        billingHistryLstError: null,
        limit:10,
        totalCount: 0,

    }
    _isMounted = false;

    componentDidMount() {
        console.log('pricing component mount');
        this._isMounted = true;
        this.handleGetBillingHistryList();
        
    }

    componentDidUpdate() {

    }

    componentWillUnmount() {
        console.log('pricing component unmount');
        this.props.handleTrasactionMode('ALL');
        this._isMounted = false;
    }


    handleGetBillingHistryList = (since = 0,) => {
        this.setState({
            billingHistryLstLoader: true,
        });
        axios
            .get(
                `billings/history/${this.props.businessID}?since=${since}&limit=${this.state.limit}`
            )
            .then(res => {

                console.log('billings/history/@@@@@@@@@@@@', res);

                if (this._isMounted) {

                    if (res.data) {
                        const totalCount = res.data.total;
                        this.setState({
                            billingHistryList: res.data.entries,
                            totalCount,
                            billingHistryLstLoader: false
                        }, () => {

                        });
                    } else {
                        this.setState({
                            billingHistryLstLoader: false,

                        });
                    }


                }
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    billingHistryLstError: errorMsg,
                    billingHistryLstLoader: false,
                   
                });
                setTimeout(() => {
                    this.setState({ billingHistryLstError: null });
                }, 5000);

            });

    }



    displayError = (e) => {
        console.log('displayError', e.data.message);
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }

        return errorMessge;
    }


    handleToBilling = () => {
        this.setState({
            key: 'billinghistory'
        });
    }

    handleToInvoice =()=>{
        this.setState({
            key:'invoices'
        });
    }

    handleToBillTransaction = () => {
        this.setState({
            key: 'transactions'
        });
    }
    
    render() {
        return (
            <div>
                {this.state.businessEditLoader === true ? <LoadingSpinner /> :
                    <Tabs
                        id="controlled-tab-example"
                        activeKey={this.state.key}
                        onSelect={key => this.setState({ key })}
                        className="subTab px-4 subTabBg"
                    >
                        <Tab eventKey="account" title="Account">
                            <Account 
                                goToInvoice={this.handleToBillTransaction}
                                goToBilling={this.handleToBilling}
                                billingHistryList={this.state.billingHistryList}
                                billingHistryLstLoader={this.state.billingHistryLstLoader}
                                billingHistryLstError={this.state.billingHistryLstError}
                            />
                        </Tab>

                        <Tab eventKey="billinghistory" title="Billing History">
                            <BillingHistory 
                                billingHistryList={this.state.billingHistryList} 
                                billingHistryLstLoader={this.state.billingHistryLstLoader}
                                billingHistryLstError={this.state.billingHistryLstError}
                                totalCount={this.state.totalCount}
                                handleGetBillingHistryList={(param)=>this.handleGetBillingHistryList(param)}
                            />
                        </Tab>

                        <Tab eventKey="productusage" title="Product Usage">
                            <ProductUsage goToInvoice={this.handleToBillTransaction} editOption={true} prodHistory={true}/>
                        </Tab>
                        {/* <Tab eventKey="invoices" title="Invoices">
                            <Invoices />
                        </Tab> */}
                        <Tab eventKey="transactions" title="Transactions">
                            <BillingTansaction />
                        </Tab>
                        {/* <Tab eventKey="productPricing" title="ProductPricing">
                            <ProductPricing />
                        </Tab> */}
                        
                    </Tabs>    
                }    
            </div>
        );
    }
}
const mapDispatchToProps = dispatch => {
    return {
        handleTrasactionMode: (pram) => dispatch(handleProdInvoiceDes(pram)),
    };
};
const mapStateToProps = state => {
    return {
        businessID: state.business.businessData.id
    };
};

Pricing.propTypes = {
    businessID: PropTypes.number,
    handleTrasactionMode: PropTypes.func,

};

export default connect(mapStateToProps, mapDispatchToProps)(Pricing);

