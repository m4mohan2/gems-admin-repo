import React, { Component } from 'react';
import { IoIosCheckmarkCircle } from 'react-icons/io';
import BillingHistoryTable from './../BillingHistory/BillingHistoryTable';
import axios from './../../../../../../../shared/eaxios';
import LoadingSpinner from './../../../../../../../Components/LoadingSpinner/LoadingSpinner';
import { Formik, Form, Field } from 'formik';
import NumberFormat from 'react-number-format';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ProductUsage from './../ProductUsage/ProductUsage';
import SuccessIco from './../../../../../../../assets/success-ico.png';
import { handleReStat } from './../../../../../../../redux/actions/product';

import * as Yup from 'yup';

import {
    Modal,
} from 'react-bootstrap';

let initialValues = {
    userPrice: 0.00,
    billingSchedule: 'monthly',
    numberOfUser: 0,
    bankId: ''
};
//let moment = require('moment');

const SignupSchema = Yup.object().shape({
    userPrice: Yup
        .number()
        .moreThan(-1, 'Please enter price per user')
        .required('Please enter price per user'),
    billingSchedule: Yup.string()
        .required('Please select billing schedule'),
    numberOfUser: Yup
        .number()
        .moreThan(0, 'Please enter number of user')
        .required('Please enter number of user'),
    // bankId: Yup
    //     .number()
    //     .required('Please select bank'),           
});

// let noOfUser;

class Account extends Component {
    state = {
        actionModal: false,
        productStatLoader: false,
        errorMessge: null,
        productStatlist: [],

        planLoader: false,
        plans: {},
        planError: null,
        noOfUser: null,
        pricePerUser: null,
        plnNotFound: null,
        payMethodNotFound: null,

        changePlan: {},
        changePlanError: null,
        changePlanLoader: false,

        bankLoader: false,
        bankDetails: null,
        bankError: null,

        changeBanks: [],
        changeBanksLoader: false,
        changeBanksError: null,

        planRateLoader: false,
        planRateDetails: null,
        planRateError: null,

        noOfSubscriber: 0,
        amtPerSubscriber: 0,

        alertMsg: false,
        updateLoader: false,
        showConfirMmsg: false,

        ChngefrmPlnError: null,
        changePlanValues: {},
        //changePlanDisable:false




    }

    _isMounted = false;


    componentDidMount() {
        this._isMounted = true;
        //this.handelGetProductStat();
        this.handelGetPlan();

    }

    componentDidUpdate() {
        console.log('this---------------------------------------------------', this.state.plans);

        if (this.state.plans.amount<0){
            initialValues = {
                userPrice: 0.00,
                billingSchedule: 'monthly',
                numberOfUser: 0,
                bankId: ''
            };

        }

    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    displayError = (e) => {
        console.log('displayError', e.data.message);
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }

        return errorMessge;
    }

    closeAlertMsg = () => {
        this.setState({
            alertMsg: false
        });
    }

    openAlertMsg = () => {
        this.setState({
            alertMsg: true
        });

    }

    handleConfirmReviewClose = () => {
        this.setState({
            showConfirMmsg: false,
        });
    };

    handleConfirmReviewShow = () => {
        this.setState({ showConfirMmsg: true });
    };



    handleChangeCofirmPlan = (param) => {


        this.setState({
            updateLoader: true,
        });

        let temp = {
            'amount': param.amount,
            'billingSchedule': param.billingSchedule,
            //'bankId': this.state.plans.bankId,
            'customerBilling': [
                {
                    'noOfUser': param.numberOfUser,
                    'bankId': this.state.plans.bankId
                }
            ]
        };

        axios
            .post(
                `customer/plan/create/${this.props.businessID}`, temp
            ).then((res) => {
                console.log('change plan', res);
                this.setState({
                    updateLoader: false,
                    alertMsg: false,
                    showConfirMmsg: true,
                });
                this.handelGetPlan();
            }).catch(e => {
                console.log('change plan error', e);
                let errorMsg = this.displayError(e);
                this.setState({
                    updateLoader: false,
                    //alertMsg: false,
                    ChngefrmPlnError: errorMsg,


                });
                this.handelGetPlan();
                setTimeout(() => {
                    this.setState({ ChngefrmPlnError: null, alertMsg: false });
                }, 5000);

            });

    }

    // perticualar bank data fetech with logo

    handleGetBank = (id) => {
        this.setState({
            bankLoader: true
        });
        axios
            .get(
                `transaction/getbank/${id}`
            )
            .then(res => {

                if (this._isMounted) {
                    if (res.data) {
                        this.setState({
                            bankDetails: res.data,
                            bankLoader: false
                        }, () => {


                        });
                    } else {
                        this.setState({
                            bankLoader: false,
                        });
                    }


                }
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    bankError: errorMsg,
                    bankLoader: false
                });
                // setTimeout(() => {
                //     this.setState({ bankError: null });
                // }, 5000);

            });

    }

    handlePlanChangeBanks = () => {
        this.setState({
            changeBanksLoader: true
        });
        axios
            .get(
                `transaction/bank/${this.props.businessID}`
            )
            .then(res => {

                if (this._isMounted) {
                    if (res.data) {
                        // if (res.data.entries.length === 0){
                        //     this.setState({
                        //         changePlanDisable: true
                        //     });
                        // }
                        this.setState({
                            changeBanks: res.data.entries,
                            changeBanksLoader: false
                        }, () => {

                        });

                    } else {
                        this.setState({
                            changeBanksLoader: false,
                        });
                    }


                }
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    changeBanksError: errorMsg,
                    changeBanksLoader: false
                });
                // setTimeout(() => {
                //     this.setState({ changeBanksError: null });
                // }, 5000);

            });

    }

    handleGetPlanRate = () => {
        this.setState({
            planRateLoader: true
        });
        axios
            .get(
                'plan/get?billingType=MONTHLY'
            )
            .then(res => {
                if (this._isMounted) {
                    if (res.data) {
                        this.setState({
                            changePlan: res.data,
                            planRateLoader: false,

                            noOfSubscriber: res.data.minNoUser,
                            amtPerSubscriber: res.data.amount

                        }, () => {
                            // initialValues.userPrice = this.state.changePlan.amount;
                            // initialValues.numberOfUser = this.state.changePlan.minNoUser;

                            initialValues.userPrice = this.state.amtPerSubscriber;
                            initialValues.numberOfUser = this.state.noOfSubscriber;

                        });
                    } else {
                        this.setState({
                            planRateLoader: false,
                        });
                    }


                }
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    planRateError: errorMsg,
                    planRateLoader: false
                });
                // setTimeout(() => {
                //     this.setState({ planRateError: null });
                // }, 5000);

            });
    }




    calculateNoUsers = (plans) => {
        console.log('plans======================', plans);
        const User = plans.customerBilling.filter(cb => Number(cb.isActive) == true);
        // noOfUser = plans.customerBilling.map(plan => {
        //     if (plan.isActive === true) {
        //         return plan.noOfUser;
        //     }
        // });
        const noOfUser = User && User.length > 0 ? User[0].noOfUser : 0;
        this.setState({
            noOfUser
        });

    }


    handelGetPlan = () => {
        this.setState({
            planLoader: true,
            bankLoader: true
        });
        axios
            .get(
                `customer/plan/${this.props.businessID}`
            )
            .then(res => {
                if (this._isMounted) {

                    if (res.data) {
                        let customerBilling = res.data.customerBilling.filter(cb => cb.isActive);
                        this.setState({
                            plans: res.data,
                            //noOfUser: res.data.customerBilling[0].noOfUser,
                            //pricePerUser: res.data.plan.amount,
                            amtPerSubscriber: res.data.amount,
                            noOfSubscriber: customerBilling[0].noOfUser,
                            planLoader: false
                        }, () => {
                            initialValues.userPrice = this.state.amtPerSubscriber;
                            initialValues.numberOfUser = this.state.noOfSubscriber;

                            initialValues.bankId = this.state.plans.bankId; //for selected banks
                            {
                                this.state.plans.bankId > 0
                                    ? this.handleGetBank(this.state.plans.bankId)
                                    : this.setState({ payMethodNotFound: 'Payment method is not Available', bankLoader: false });
                            }
                            this.calculateNoUsers(this.state.plans);

                        });
                    } else {
                        this.setState({
                            planLoader: false,
                            plnNotFound: 'Plan is not Available',
                            payMethodNotFound: 'Payment method is not Available',
                            bankLoader: false
                        });
                    }


                }
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    planError: errorMsg,
                    planLoader: false,
                    bankError: 'Payment method is not available',
                    bankLoader: false
                });
                // setTimeout(() => {
                //     this.setState({ planError: null });
                // }, 5000);

            });

    }




    openActionModal = () => {
        this.setState({
            actionModal: true
        });
    }
    closeActionModal = () => {
        this.setState({
            actionModal: false
        });
    }

    handleSubmit = values => {
        this.openAlertMsg();

        values.amount = values.userPrice != this.state.plans.amount ? values.userPrice : this.state.plans.amount,
        values.isActive = true,
        //delete values.userPrice;
        console.log('form values', values);
        let changePlanValues = values;
        changePlanValues.bankId = (changePlanValues.bankId == 0 ? null : changePlanValues.bankId);
        console.log('*******************************^^^^^^^^^^^^^^^^^********************************', changePlanValues);
        this.setState({
            changePlanValues,
            actionModal: false
        });


    };

    handleUpdateNoOfUser = (e) => {
        this.setState({
            noOfSubscriber: e.target.value
        });
    }

    handleUpdateUserPrice = (e) => {
        this.setState({
            amtPerSubscriber: e.target.value
        });
    }

    handleChangePlan = () => {
        this.openActionModal();
        //this.handlePlanChangeBanks();
        //this.handleGetPlanRate(); ///// it will be change soon
    }
    handleGotoBillHisry = () => {
        console.log('handleGotoBillHisry running');
        this.props.goToBilling();
    }



    render() {
        const {
            plans,
            noOfUser,
            planLoader,
            plnNotFound,
            planError,
            bankDetails,
            bankError,
            bankLoader,
            payMethodNotFound
        } = this.state;
        console.log('plans==================############################', plans);
        return (
            <div>


                <div className="p-3">
                    <div className="row">
                        <div className="col-sm-3">

                            <div className="border rounded bg-white mb-3">
                                <div className="bg-light clearfix p-2">
                                    <div className="float-left pt-2 pl-2 font-weight-500">Plan Details</div>
                                    <div className="float-right"><button onClick={() => this.handleChangePlan()} type="button" className="btn btn-light"><span className="text-warning">Change</span></button></div>
                                </div>


                                <div className="p-3">
                                    {planLoader ? <LoadingSpinner /> :
                                        planError ? <div className="text-danger">{planError}</div> :
                                            (plans && plans.totalAmount>=0) ? (<React.Fragment>
                                                <div className="clearfix">
                                                    <div>
                                                        <span className="float-left"><h5 className="font-weight-bold mb-0">STANDARD</h5></span> <span className="text-primary float-right text-capitalize">{plans.billingSchedule}</span>

                                                    </div>
                                                </div>
                                                <div className="">
                                                    <span className='font-10 text-black-50'>Currently subscribe to <strong>standard</strong> plan with <strong>{plans.billingSchedule} billing</strong></span>
                                                </div>

                                                <div>
                                                    <div className="text-center my-3"><sup className="font-large text-black-50">$</sup><span className='customPrice'><NumberFormat
                                                        value={plans.totalAmount}
                                                        displayType={'text'}
                                                        thousandSeparator={true}
                                                        prefix={''}
                                                        decimalScale={2}
                                                        fixedDecimalScale={true}
                                                        thousandsGroupStyle={'thousand'}
                                                        renderText={value => <span>{value}</span>}
                                                    /></span><sub className="font-large text-black-50">/{plans.billingSchedule === 'monthly' ? 'month' : null}</sub></div>
                                                </div>
                                                <div>
                                                    <div className="text-warning">Summary</div>
                                                    <div>
                                                        <div className='font-10 text-black-50 clearfix'>
                                                            <span className="float-left"><IoIosCheckmarkCircle className="text-warning" /></span>
                                                            <span className="ml-1 float-right width-92">Includes full access to Fintainium payment platform</span>
                                                        </div>
                                                        <div className='font-10 text-black-50 clearfix'>
                                                            <span className="float-left"><IoIosCheckmarkCircle className="text-warning" /></span>
                                                            <span className="ml-1 float-right width-92">Includes license for <strong>{noOfUser} users</strong></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="pt-3 font-12 text-black-50">
                                                    <div>Next Payment for <strong>
                                                        <NumberFormat
                                                            value={plans.totalAmount}
                                                            displayType={'text'}
                                                            thousandSeparator={true}
                                                            fractionSize={2}
                                                            prefix={'$'}
                                                            decimalScale={2}
                                                            fixedDecimalScale={true}
                                                            thousandsGroupStyle={'thousand'}
                                                            renderText={value => <span>{value}</span>}
                                                        /></strong> is scheduled on <strong>{plans.nextPaymentDate}</strong></div>
                                                </div>
                                            </React.Fragment>): <div className='text-muted'>{plnNotFound}</div> 
                                                    

                                    }
                                   


                                </div>
                            </div>

                            <div className="border rounded bg-white">
                                <div className="bg-light clearfix p-2">
                                    <div className="float-left pt-2 pl-2 font-weight-500">Payment Method</div>
                                </div>
                                <div className="p-3">
                                    {
                                        bankLoader ? <LoadingSpinner /> :
                                            bankError ? <div className='text-danger'>{bankError}</div> :
                                                payMethodNotFound ? <div className="text-muted">{payMethodNotFound}</div> :
                                                    <React.Fragment>
                                                        <h5>ACH DEBIT</h5>
                                                        <img src={bankDetails && bankDetails.bankLogoUrl} className="billbnkImg mt-3" />
                                                        <div>{bankDetails && <div>A/C {bankDetails.accountNumber}</div>} </div>
                                                    </React.Fragment>
                                    }

                                </div>
                            </div>

                        </div>
                        <div className="col-sm-9 accountOuter">
                            <ProductUsage goToInvoice={this.props.goToInvoice} editOption={false} prodHistory={false} />
                            <div className="border rounded bg-white">
                                <div className="bg-light clearfix p-2">
                                    <div className="float-left pt-2 pl-2 font-weight-500">Billing History</div>
                                </div>
                                <div className="clearfix custom-row-manupulate">

                                    <BillingHistoryTable
                                        billingHistryList={this.props.billingHistryList}
                                        billingHistryLstLoader={this.props.billingHistryLstLoader}
                                        billingHistryLstError={this.props.billingHistryLstError}
                                    />
                                    {this.props.billingHistryList.length > 2 && <div className="text-center text-primary mb-3 pointer" onClick={() => this.handleGotoBillHisry()}><u>View More</u></div>}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <Modal
                    show={this.state.actionModal}
                    onHide={this.closeActionModal}
                    size="lg"
                    className=""
                >

                    <Modal.Body>

                        {this.state.changeBanksLoader ? <LoadingSpinner /> :
                            // this.state.changePlanDisable ? (
                            //     <div className="text-center">
                            //         <div className='alert alert-warning'> 
                            //             <h5>Please add a bank to subscribe a plan</h5>   
                            //         </div>
                            //         <button onClick={() => this.closeActionModal()} className='btn btn-dark mt-3'>Close</button>
                            //     </div>):
                            this.state.planRateLoader ? <LoadingSpinner /> :

                                <Formik
                                    initialValues={initialValues}
                                    validationSchema={SignupSchema}
                                    onSubmit={this.handleSubmit}
                                    enableReinitialize={true}
                                >
                                    {({ values,
                                        errors,
                                        touched,
                                        handleChange,
                                        handleBlur }) => (
                                        <Form>
                                            <div className="row">
                                                <div className="col-sm-6">
                                                    <div>
                                                        {/*                                                         
                                                        <div className="row">
                                                            <div className="col-sm-12">
                                                                <div className='form-group' controlid="formBasicText">
                                                                    <div className="mb-2 font-weight-bold">Select a Bank<span className="required">*</span></div>
                                                                    <Field
                                                                        name="bankId"
                                                                        component="select"
                                                                        className="form-control truncate w-100"
                                                                        autoComplete="nope"
                                                                        onChange={e => {
                                                                            handleChange(e);
                                                                            // setFieldValue('key', e);
                                                                        }}
                                                                        value={values.bankId || ''}
                                                                        onBlur={handleBlur}
                                                                    >

                                                                        <option value=''>
                                                                            {this.state.changeBanks.length > 0 ? 'Select a Bank' : 'Loading...'}
                                                                        </option>
                                                                        {
                                                                            this.state.changeBanks.length > 0 && this.state.changeBanks.map(bank => (
                                                                                <option key={bank.id} value={bank.id}> {bank.bankName}</option>
                                                                            ))
                                                                        }
                                                                    </Field>

                                                                    {errors.bankId && touched.bankId ? (
                                                                        <span className="errorMsg ml-3 mt-1">
                                                                            {errors.bankId}
                                                                        </span>
                                                                    ) : null}

                                                                </div>
                                                            </div>
                                                        </div>
                                                         */}

                                                        <div className="form-group" controlid="formBasicText">
                                                            <div className="font-weight-bold">Billing Schedule <span className="required">*</span></div>

                                                            <Field
                                                                name="billingSchedule"
                                                                component="select"
                                                                className={`input-elem ${values.billingSchedule &&
                                                                        'input-elem-filled'} form-control`}
                                                                autoComplete="nope"
                                                                value={values.billingSchedule || ''}
                                                                onChange={e => {
                                                                    handleChange(e);
                                                                }}
                                                            >
                                                                {/* <option value="weekly">
                                                                        weekly
                                                                </option> */}
                                                                <option value="monthly" defaultValue="select">
                                                                        monthly
                                                                </option>
                                                                {/* <option value="yearly">
                                                                        yearly
                                                                </option> */}

                                                            </Field>

                                                            {errors.billingSchedule && touched.billingSchedule ? (
                                                                <span className="errorMsg ml-3">
                                                                    {errors.billingSchedule}
                                                                </span>
                                                            ) : null}


                                                        </div>

                                                        <div className="row">
                                                            <div className="col-sm-6">

                                                                <div className='form-group' controlid="formBasicText">
                                                                    <div className="mb-2 font-weight-bold">Price per user ($) <span className="required">*</span></div>
                                                                    <Field
                                                                        name="userPrice"
                                                                        type="number"
                                                                        className="form-control lightPlaceholder"
                                                                        autoComplete="nope"
                                                                        value={values.userPrice}
                                                                        onBlur={handleBlur}
                                                                        placeholder="0.00"
                                                                        onChange={e => {
                                                                            handleChange(e);
                                                                            this.handleUpdateUserPrice(e);
                                                                        }}
                                                                    />
                                                                    {errors.userPrice && touched.userPrice ? (
                                                                        <span className="errorMsg pl-3">{errors.userPrice}</span>
                                                                    ) : null}

                                                                </div>

                                                            </div>
                                                            <div className="col-sm-6">
                                                                <div className='form-group' controlid="formBasicText">
                                                                    <div className="mb-2 font-weight-bold">Number of user <span className="required">*</span></div>
                                                                    <Field
                                                                        name="numberOfUser"
                                                                        type="number"
                                                                        className="form-control"
                                                                        autoComplete="nope"
                                                                        value={values.numberOfUser || 0}
                                                                        onBlur={handleBlur}
                                                                        placeholder="0"
                                                                        onChange={e => {
                                                                            handleChange(e);
                                                                            this.handleUpdateNoOfUser(e);
                                                                        }}
                                                                    />
                                                                    {errors.numberOfUser && touched.numberOfUser ? (
                                                                        <span className="errorMsg pl-3">{errors.numberOfUser}</span>
                                                                    ) : null}

                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div>

                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div className="text-center my-3"><sup className="font-large text-black-50">$</sup><span className='customPrice'><NumberFormat
                                                            value={this.state.amtPerSubscriber * this.state.noOfSubscriber}
                                                            displayType={'text'}
                                                            thousandSeparator={true}
                                                            prefix={''}
                                                            decimalScale={2}
                                                            fixedDecimalScale={true}
                                                            thousandsGroupStyle={'thousand'}
                                                            renderText={value => <span>{value}</span>}
                                                        /></span><sub className="font-large text-black-50">/month</sub></div>
                                                    </div>
                                                </div>
                                                <div className="col-sm-6">
                                                    <div className="text-warning">Summary</div>
                                                    <div>
                                                        <div className='font-10 text-black-50 clearfix'>
                                                            <span className="float-left"><IoIosCheckmarkCircle className="text-warning" /></span>
                                                            <span className="ml-1 float-right width-92">Includes full access to Fintainium payment platform</span>
                                                        </div>
                                                        <div className='font-10 text-black-50 clearfix'>
                                                            <span className="float-left"><IoIosCheckmarkCircle className="text-warning" /></span>
                                                            <span className="ml-1 float-right width-92">Includes license for <strong>{this.state.noOfSubscriber} users</strong></span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-6"></div>
                                                <div className="col-sm-6">
                                                    <div className='text-center'>
                                                        <span className="btn btn-secondary mr-2" onClick={this.closeActionModal} >Cancel</span>
                                                        <button className="btn btn-primary ml-2" type="submit">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </Form>
                                    )}
                                </Formik>

                        }

                    </Modal.Body>
                </Modal>

                {/*======  before action confirmation massage popup  =====*/}
                <Modal
                    show={this.state.alertMsg}
                    onHide={this.closeAlertMsg}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        {this.state.ChngefrmPlnError ? <div className="m-auto text-center text-danger">{this.state.ChngefrmPlnError}</div> :
                            <div className="m-auto text-center">

                                <h6 className="mb-3">
                                    {this.state.updateLoader ? <LoadingSpinner /> : null}
                                    <span>Do you want to change plan</span>
                                </h6>
                            </div>
                        }


                        <div className="m-auto text-center">
                            <button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.closeAlertMsg()}>Return</button>
                            {!this.state.ChngefrmPlnError ? <button className="btn btn-primary" onClick={() => this.handleChangeCofirmPlan(this.state.changePlanValues)}>Confirm</button> : null}
                        </div>

                    </Modal.Body>
                </Modal>

                {/*======  confirmation popup  ===== */}

                <Modal
                    show={this.state.showConfirMmsg}
                    onHide={this.handleConfirmReviewClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <div className='row'>
                            <div className="text-center col-sm-12">
                                <image src={SuccessIco} />
                            </div>
                        </div>
                        <div className='row'>
                            <div className="text-center my-3 col-sm-12">
                                <h6>Plan has been successfully updated</h6>
                            </div>
                        </div>
                        <div className="text-center">
                            <button
                                onClick={this.handleConfirmReviewClose}
                                className="btn btn-dark but-gray"
                            >
                                Done
                            </button>
                        </div>

                    </Modal.Body>
                </Modal>


            </div>

        );
    }
}
const mapStateToProps = state => {
    return {
        businessID: state.business.businessData.id,
        reacload: state.product.reacload,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        handleReStat: data => dispatch(handleReStat(data)),
    };
};

Account.propTypes = {
    businessID: PropTypes.number,
    goToInvoice: PropTypes.func,
    goToBilling: PropTypes.func,
    billingHistryList: PropTypes.array,
    billingHistryLstLoader: PropTypes.bool,
    billingHistryLstError: PropTypes.string,
    handleReStat: PropTypes.func,
    reacload: PropTypes.bool,
};

export default connect(mapStateToProps, mapDispatchToProps)(Account);
