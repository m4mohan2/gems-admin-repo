import React, { Component } from 'react';
import axios from './../../../../../../../shared/eaxios';
import {
    Table,
    Image,
    Row,
    Col,

} from 'react-bootstrap';
import LoadingSpinner from '../../../../../../../Components/LoadingSpinner/LoadingSpinner';
import NumberFormat from 'react-number-format';
import { Link } from 'react-router-dom';
import refreshIcon from './../../../../../../../assets/refreshIcon.png';
import Pagination from 'react-js-pagination';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styles from './../ProductUsage/ProductUsage.module.scss';
import { invoiceDescription } from './../../../../../../../redux/actions/invoice';
import { Base64 } from 'js-base64';
import { handleProdInvoiceDes } from './../../../../../../../redux/actions/product';
let moment = require('moment');

class BillingTansaction extends Component {


    state = {

        invoiceLists: [],
        activePage: 1,
        totalCount: 0,
        itemPerPage: 250,
        invoiceListsLoading: false,
        errorMessge: null,
        paymentMethod: 'ALL',
        searchKey: '',
        chargeId:0,
        currentMonth: new Date().getMonth() + 1,
        currentYear: new Date().getFullYear(),
        isReset:false,
        selectedMonth: null,
        seletedYear: null,

        startDate: null,
        endDate: null,
        slcTimeStamp: null,
        prevSix: []
    }
    _isMounted = false;

    handleinitialdata = () => {
        let currentMonth = new Date().getMonth() + 1;
        let currentYear = new Date().getFullYear();
        console.log('+++++++++++++++++', currentYear);
        this.setState({
            selectedMonth: currentMonth,
            seletedYear: currentYear
        }, () => { this.handleGetInvoice(); console.log('select-----------------------------', this.state.seletedYear); });


    }


    LastSix = () => {
        let d, m, prevSix = [];
        d = new Date();
        m = d.getMonth() + 1;
        for (let i = 6; i > 0; i--) {
            prevSix.push(new Date().setMonth(m - i));
        }
        prevSix = prevSix.reverse();
        prevSix = prevSix.map((m, i) => (
            < option key={m.toString()} value={(new Date(m).getMonth() + 1) + '-' + new Date(m).getFullYear()} >
                {i === 0 ? 'This month' : moment(m).format('MMMM')}
            </option>
        ));
        this.setState({ prevSix });
    };

    handleGetYrMo = (e) => {
        
        let temp = e.target.value.split('-');
        this.setState({
            slcTimeStamp: e.target.value,
            selectedMonth: temp[0] ,
            seletedYear: temp[1]
        }, () => {
            
            let data = {};
            data.mode = 'ALL';
            data.id = 0;
            data.selectedMonth = this.state.selectedMonth;
            data.seletedYear = this.state.seletedYear;
            this.props.handleTrasactionMode(data);

            this.handleGetInvoice();
        });
    }


    displayError = (e) => {
        console.log('displayError', e.data.message);
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber });
        this.handleGetInvoice(pageNumber > 0 ? pageNumber - 1 : 0, Base64.encode(this.state.searchKey));
    };

    handleChangeItemPerPage = (e) => {
        this.setState({ itemPerPage: e.target.value },
            () => {
                this.handleGetInvoice(this.state.activePage > 0 ? this.state.activePage - 1 : 0, Base64.encode(this.state.searchKey));
            });
    }

    resetPagination = () => {
        this.setState({ activePage: 1 });
    }

    formatDate = date => {
        let d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        let temp = [year, month, day].join('-');
        return moment(temp).format('MMM DD YYYY');
    }


    businessAdd = () => {
        this.setState({
            showBusinessModal: true,
        }, () => { });
    }

    handleGetInvoice = (
        since = 0,
        searchKey = ''
    ) => {

        // let currentMonth = new Date().getMonth() + 1;
        // let currentYear = new Date().getFullYear();
        // const { id } = this.props.globalState.business.businessData;
        this.setState({
            invoiceListsLoading: true,
            //currentMonth,
            //currentYear
        },()=>{
            axios
                .get(
                    `transaction/billing/list/${this.props.businessID}?key=${searchKey}&paymentMethod=${this.state.paymentMethod}&chargeId=${this.state.chargeId}&month=${this.state.currentMonth}&year=${this.state.currentYear}&direction=true&prop&since=${since}&limit=${this.state.itemPerPage}`
                )
                .then(res => {
                    const totalCount = res.data.total;
                    if (this._isMounted) {
                        this.setState({
                            invoiceLists: res.data.entries,
                            invoiceListsLoading: false,
                            totalCount
                        }, () => {
                            console.log('this.state.invoiceLists', this.state.invoiceLists);
                        });
                    }


                })
                .catch(e => {
                    let errorMsg = this.displayError(e);
                    this.setState({
                        errorMessge: errorMsg,
                        invoiceListsLoading: false
                    });

                });
        });
        
    }



    handleChange = (e) => {
        this.setState({ paymentMethod: e.target.value },
            () => {
                this.handleGetInvoice(0);
                //this.resetPagination();
            });
    }



    handlekeyPress = (e) => {
        
        if (e.key === 'Enter') {
            let searchKey = this.state.searchKey;
            searchKey = searchKey.trim();
            this.setState({
                searchKey
            }, () => {
                let searchKeyB64 = Base64.encode(this.state.searchKey);
                this.handleGetInvoice(0, searchKeyB64);
            });
            
        }

    }

    handelOnchange = (e) => { this.setState({ searchKey: e.target.value.trim() }); }

    handelSearch = () => {
        if (this.state.searchKey != '') {
            let searchKey = this.state.searchKey;
            searchKey = searchKey.trim();
            this.setState({
                searchKey
            }, () => {
                let searchKeyB64 = Base64.encode(this.state.searchKey);
                this.handleGetInvoice(0, searchKeyB64);
            });

            
        }

    }
    resetSearch = () => {
        //this.props.onClickAction('');
        this.setState({
            searchKey: '',
            paymentMethod: 'ALL',
            chargeId:0,
            currentMonth: new Date().getMonth() + 1,
            currentYear: new Date().getFullYear(),
            
            
        }, () => {
            let data = {};
            data.mode = this.state.paymentMethod;
            data.id = this.state.chargeId;
            data.selectedMonth = this.state.currentMonth;
            data.seletedYear = this.state.currentYear;
            data.paymodeDisable = false;
            this.props.handleTrasactionMode(data);

            this.handleGetInvoice(0, this.state.searchKey);
            this.resetPagination();
            
            
        });

    }

    componentDidMount() {

        console.log('this.props.prodpaymodeID ----------------------------------', this.props.prodpaymodeID);
        this.LastSix();
        this.handleinitialdata();
        
        if (this.props.prodpaymodeID){
            this.setState({ chargeId: this.props.prodpaymodeID });
        }
        
        this._isMounted = true;
        // let data = {};
        // data.id = 0;
        // data.mode = 'ALL';

        // this.props.handleTrasactionMode(data);

        if (this.props.globalState.invoice.description !== '') {
            this.setState({
                searchKey: this.props.globalState.invoice.description
            }, () => {
                this.handelSearch();
            });
        } else {
            this.handleGetInvoice(0);
        }


    }

    componentDidUpdate(prevProps) {

        if (prevProps !== this.props) {
            console.log('++++++++++++++++ I am here prevProps !== this.props +++++++++++++++', this.props.prodpayselectedMonth, this.props.prodpayseletedYear);
            if (this.props.prodpayselectedMonth) {
                console.log('month block');
                this.setState({
                    selectedMonth: this.props.prodpayselectedMonth,
                    seletedYear: this.props.prodpayseletedYear,
                    slcTimeStamp: this.props.prodpayselectedMonth + '-' + this.props.prodpayseletedYear
                    //isDisable:true
                }, () => {
                    this.handleGetInvoice();
                    this.LastSix();
                }
                );
            }


        }



        
        if (prevProps !== this.props) {
            if (this.props.prodpayseletedYear ) {

                
                this.setState({
                    currentYear: this.props.prodpayseletedYear,
                    currentMonth: this.props.prodpayselectedMonth,
                });
            }

            let paymentMethod;
            {
                this.props.paymentMode === 'ACH Transactions' ?
                    paymentMethod = 'ACH' :
                    this.props.paymentMode === 'Check' ?
                        paymentMethod = 'CHECK' : 
                        this.props.paymentMode === 'Check File' ?
                            paymentMethod = 'CHECK_FILE' :
                            this.props.paymentMode === 'Virtual Credit Card' ?
                                paymentMethod = 'VC' : paymentMethod = 'ALL';
            }

            this.setState({
                chargeId: this.props.prodpaymodeID,
                paymentMethod: paymentMethod,
                startDate: ''
            }, () => { this.handleGetInvoice(0); console.log('-------this.state.paymentMethod------', this.state.paymentMethod); });

        }

    }

    componentWillUnmount() {
        this._isMounted = false;

    }

    render() {
        const {
            paymodeDisable
        } = this.props;
        const {

            prevSix
        } = this.state;
        return (
            <div className="dashboardInner businessOuter pt-0">
                <div className="row my-3">
                    <div className="col-7">
                        <select
                            disabled={paymodeDisable}
                            id={this.state.selectedBusiness}
                            className="form-control truncate pr-35 float-left w-210 mr-3"
                            onChange={this.handleChange}
                            value={this.state.paymentMethod}>
                            <option value='ALL'>All</option>
                            <option value='ACH'>ACH</option>
                            <option value='CHECK'> Check </option>
                            <option value='CHECK_FILE'> Check Files</option>
                            <option value='VC'> Virtual Credit Card</option>
                        </select>

                        <input
                            type="text"
                            className="w-210 form-control float-left"
                            placeholder="Type to search"
                            value={this.state.searchKey}
                            onChange={this.handelOnchange}
                            onKeyPress={this.handlekeyPress}
                        />
                        <button
                            type="button"
                            className="btn ml-3 search-btn text-black float-left cus-mt-3"
                            onClick={() => this.handelSearch()}
                        >
                            Search
                        </button>

                        <Link to="#" className='ml-2'>
                            <Image className="mt-2" src={refreshIcon} onClick={() => this.resetSearch()} />
                        </Link>

                    </div>
                    <div className="col-5">
                        <span className={'float-right mt-1 ml-2 ' + styles.billing}>
                            <select className='px-2 py-1' onChange={(e) => this.handleGetYrMo(e)} value={this.state.slcTimeStamp}>
                                {prevSix}
                            </select>

                        </span>
                        <span className="float-right text-dark pt-2">{(`Period: ${this.formatDate(new Date(this.state.seletedYear, this.state.selectedMonth - 1, 1))} - ${this.formatDate(new Date(this.state.seletedYear, this.state.selectedMonth, 0))}`)}</span>
                        
                        
                    </div>
                </div>
                <div className="boxBg">
                    <Table responsive hover>
                        <thead className="theaderBg">
                            <tr>
                                {/* <th></th> */}

                                <th>
                                    <span className="float-left pr-2">Vendor Name</span>
                                </th>
                                <th>
                                    <span className="float-left pr-2">Invoice No.</span>

                                </th>
                                <th>

                                    <span className="float-left pr-2">Processing Date</span>

                                </th>
                                <th>
                                    <span className="float-left pr-2">Reference No</span>

                                </th>
                                <th className="text-right">
                                    <span>Amount</span>
                                </th>
                                <th>
                                    <span className="float-left pr-2">Payment Method</span>

                                </th>
                                <th className="text-right">
                                    <span >Invoice Amount</span>

                                </th>
                                <th>
                                    <span className="float-left pr-2">Payment Status</span>

                                </th>
                            </tr>
                        </thead>
                        <tbody>

                            {this.state.invoiceListsLoading ? (<tr>
                                <td colSpan={12}>
                                    <LoadingSpinner />
                                </td>
                            </tr>) :
                                this.state.invoiceLists.length > 0 ? (
                                    this.state.invoiceLists.map(invoiceList => (
                                        <tr
                                            key={invoiceList.id}
                                        >
                                            <td>{invoiceList.customerVendorName}</td>
                                            <td>{invoiceList.invoiceNumber}</td>
                                            <td>{invoiceList.processingDate}</td>
                                            <td>{invoiceList.referenceNo}</td>
                                            <td className="text-right">
                                                <NumberFormat
                                                    value={invoiceList.amount}
                                                    displayType={'text'}
                                                    thousandSeparator={true}
                                                    fractionSize={2}
                                                    prefix={'$'}
                                                    decimalScale={2}
                                                    fixedDecimalScale={true}
                                                    thousandsGroupStyle={'thousand'}
                                                    renderText={value => <span>{value}</span>}
                                                />
                                            </td>
                                            <td>{invoiceList.paymentMethod}</td>
                                            <td className="text-right"><NumberFormat
                                                value={invoiceList.invoiceAmount}
                                                displayType={'text'}
                                                thousandSeparator={true}
                                                fractionSize={2}
                                                prefix={'$'}
                                                decimalScale={2}
                                                fixedDecimalScale={true}
                                                thousandsGroupStyle={'thousand'}
                                                renderText={value => <span>{value}</span>}
                                            /></td>
                                            <td>{invoiceList.status}</td>
                                            {/* <td>{invoiceList.status ? 'True':'False'}</td> */}
                                        </tr>
                                    ))
                                )
                                    :
                                    this.state.errorMessge ? <tr>
                                        <td colSpan={12}>
                                            <p className="text-center">{this.state.errorMessge}</p>
                                        </td>
                                    </tr> : (
                                        <tr>
                                            <td colSpan={12}>
                                                <p className="text-center">No records found</p>
                                            </td>
                                        </tr>
                                    )
                            }

                        </tbody>
                    </Table>
                </div>
                {this.state.totalCount ? (
                    <Row className="px-3">
                        <Col md={4} className="d-flex flex-row mt-20">
                            <span className="mr-2 mt-2 font-weight-500">Items per page</span>
                            <select
                                id={this.state.itemPerPage}
                                className="form-control truncatefloat-left w-90"
                                onChange={this.handleChangeItemPerPage}
                                value={this.state.itemPerPage}>
                                <option value='50'>50</option>
                                <option value='100'>100</option>
                                <option value='150'>150</option>
                                <option value='200'>200</option>
                                <option value='250'>250</option>

                            </select>
                        </Col>
                        <Col md={8}>
                            <div className="paginationOuter text-right">
                                <Pagination
                                    activePage={this.state.activePage}
                                    itemsCountPerPage={this.state.itemPerPage}
                                    totalItemsCount={this.state.totalCount}
                                    onChange={this.handlePageChange}
                                />
                            </div>
                        </Col>
                    </Row>
                ) : null}

            </div>
        );
    }
}
BillingTansaction.propTypes = {

    onClickAction: PropTypes.func,
    globalState: PropTypes.object,
    paymentMode: PropTypes.string,
    businessID: PropTypes.number,
    prodpaymodeID: PropTypes.number,
    prodpayseletedYear: PropTypes.number,
    prodpayselectedMonth: PropTypes.number,
    handleTrasactionMode: PropTypes.func,
    paymodeDisable: PropTypes.bool,
    reacload: PropTypes.bool,
    editOption: PropTypes.bool,
};

const mapStateToProps = state => {
    return {
        globalState: state,
        paymentMode: state.product.prodpaymode,
        prodpaymodeID: state.product.prodpaymodeID,
        prodpayselectedMonth: state.product.prodpayselectedMonth,
        prodpayseletedYear: state.product.prodpayseletedYear,
        businessID: state.business.businessData.id,
        paymodeDisable: state.product.paymodeDisable,
        reacload: state.product.reacload,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onClickAction: data => dispatch(invoiceDescription(data)),
        handleTrasactionMode: (pram) => dispatch(handleProdInvoiceDes(pram)),

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(BillingTansaction);

