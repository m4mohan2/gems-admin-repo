/* eslint-disable no-useless-escape */
import { Field, Form, Formik } from 'formik';
import React, { Component, Fragment } from 'react';
import {
    Button,
    Col,
    FormControl,
    FormGroup,
    Row
} from 'react-bootstrap';
import InputMask from 'react-input-mask';
import * as Yup from 'yup';
import { CountryService } from '../../../../../../services/country.service';
import axios from '../../../../../../shared/eaxios';
import LoadingSpinner from '../../../../../../Components/LoadingSpinner/LoadingSpinner';

import PropTypes from 'prop-types';

let modifiedObject = {};
//Formik and Yup validation
const editVendorSchema = Yup.object().shape({
    companyName: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter Company Name'),
    taxNo: Yup
        .string()
        //.min(10, 'taxNo must be at least 9 characters'),
        .test(
            'taxtValidNo',
            'Please enter minimum 9 digit ',
            function (val) {
                if (val) {
                    // eslint-disable-next-line no-useless-escape
                    const rValue = val.replace(/\-/, '');
                    if ((rValue.length) < 9) {
                        return false;
                    }
                }
                return true;
            }
        ),
    //.required('Please enter business tax ID/SSN'),
    firstName: Yup
        .string()
        .trim('Please remove whitespace')
        .required('Please enter first name')
        .strict(),
    lastName: Yup
        .string()
        .trim('Please remove whitespace')
        .required('Please enter last name')
        .strict(),
    paymentMode: Yup
        .string()
        .notOneOf(['']),
    emailAddress: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .email('Email must be valid'),
    phoneNumber: Yup.string().min(14, 'Phone number must be 10 digit'),

    // address: Yup
    //     .string()
    //     .trim('Please remove whitespace')
    //     .strict()
    //     .required('Please enter address'),
    
    addressLine1: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter address line 1')
        .max(40, 'maximum characters length 40'),
    addressLine2: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .max(40, 'maximum characters length 40'),
    addressLine3: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .max(40, 'maximum characters length 40'),
    addressLine4: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .max(40, 'maximum characters length 40'),
    addressLine5: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .max(40, 'maximum characters length 40'),


    zipCode: Yup
        .string()
        .trim('Please remove whitespace')
        .strict()
        .test(
            'zipCode',
            'Please enter a valid zip code',
            function (value) {
                if (value) {
                    let isValid = false;
                    if (/^\d{5}([\-|\s]\d{1,4})?$/.test(value)) {
                        isValid = true;
                    } else if (/^[a-zA-Z0-9]{3}\s[a-zA-Z0-9]{3}$/.test(value)) {
                        let expArr = value.split(' ');

                        if (/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{3}$/.test(expArr[0]) && /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{3}$/.test(expArr[1])) {
                            isValid = true;
                        }
                    }
                    return isValid;
                }

            }
        )
        .required('Please enter zip code'),
    //countryCode: Yup.string(),
    countryCode: Yup
        .string()
        .required('Please select country'),
    stateCode: Yup
        .string()
        .trim('Please remove whitespace')
        .required('Please select state'),
    city: Yup
        .string()
        .trim('Please remove whitespace'),
    cityId: Yup
        .number()
        .required('Please select city')
});

class EditBusinessCustomers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showVendor: false,
            countries: [],
            states: [],
            cities: [],
            selectedStates: [],
            selectedCities: [],
            errorMessge: null,
            editVendorEnable: false,
            disabled: false,
            editVendorLoader:false,
            editErrorMessge:false
        };
    }

    static getDerivedStateFromProps(props, state) {
        console.log(' getDerivedStateFromProps : ', props, state);
        if (!state.vendorData) {
            return {
                ...props
            };
        }
    }

    handleEditVendorEnable = () => {
        this.setState({
            editVendorEnable: true,
            disabled: true
        });
    }

    handleEditVendorDisable = () => {
        this.setState({
            editVendorEnable: false,
            disabled: false
        });
        this.props.onReload(this.state.vendorData);
    }

    handleCloseVendor = () => {
        this.props.onClick();
    };


    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }

        return errorMessge;
    }



    //after editing form submit
    handleSubmit = values => {
        this.setState({
            editVendorLoader:true,
        });
        console.log('handleSubmit', values);

        for (const st of this.state.states) {
            if (parseInt(st.id) === parseInt(values.stateCode)) values.stateCode = st.stateCode;
            console.log('values.stateCode',values.stateCode);
        }

        for (const country of this.state.countries) {
            if (parseInt(country.id) === parseInt(values.countryCode)) values.countryCode = country.countryCode;
            console.log('values.countryCode', values.countryCode);
        }

        let newValue = {
            companyName: values.companyName,
            //taxNo: values.taxNo,
            taxNo: values.taxNo ? values.taxNo.split('-').join('') : null,
            firstName: values.firstName,
            lastName: values.lastName,
            emailAddress: values.emailAddress,
            phoneNumber: values.phoneNumber,
            paymentMode: values.paymentMode,
            //address: values.address,
            addressLine1: values.addressLine1,
            addressLine2: values.addressLine2,
            addressLine3: values.addressLine3,
            addressLine4: values.addressLine4,
            addressLine5: values.addressLine5,
            zipCode: values.zipCode,
            //countryCode: values.countryCode,
            countryCode: values.countryCode,
            stateCode: values.stateCode,
            cityId: values.cityId,
            isCustomer: true
        };
        console.log('new Value submitted 2', newValue);

        const vendorData = {};
        Object.assign(vendorData, newValue);
        console.log('vendorData', vendorData);
        axios
            .put(`/customerVendor/customer/update/${values.businessId}/${values.id}`, newValue)
            .then(res => {
                console.log('Vendor details get response', res);
                console.log('Vendor details get data', res.data);
                this.setState({
                    editVendorLoader:false,
                    editVendorEnable: false,
                    disabled: false,
                    vendorData
                },()=>{
                    this.props.handleEditConfirMmsg();
                });
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    editVendorLoader: false,
                    editErrorMessge: errorMsg,
                });

                setTimeout(() => {
                    this.setState({ deleteErrorMessge: null });
                }, 5000);

            });
    };

    componentDidMount() {
        const countryService = new CountryService();
        countryService.getData().then(({ countries, states, cities }) => { //countries, states or cities
            this.setState({ countries, states, cities }, () => {
                this.populateState({ target: { value: this.props.countryCode } });
            });
        });
    }


    populateState = e => {
        const countryCode = e.target.value;
        const states = this.state.states;
        let countryId = 0;
        for (let country of this.state.countries) if (country.countryCode === countryCode) countryId = country.id;
        const newStates = states.filter(state => Number(state.countryId) === Number(countryId));
        this.setState({ selectedStates: newStates, selectedCities: [] }, () => {
            this.populateCity({ target: { value: this.props.stateCode } });
        });
    };

    populateCity = e => {
        console.log('populate city ', e);
        const stateCode = e.target.value;
        const cities = this.state.cities;
        let stateId = 0;
        for (let st of this.state.states) if (st.stateCode === stateCode) stateId = st.id;
        const newCities = cities.filter(ct => Number(ct.stateId) === Number(stateId));
        this.setState({ selectedCities: newCities });
    };

    handelMod = (getObject) => {
        console.log('calling..', getObject);
        if (getObject) {
            // blank space removed
            getObject.companyName && (getObject.companyName = getObject.companyName.trim());
            //getObject.address && (getObject.address = getObject.address.trim());
            getObject.addressLine1 && (getObject.addressLine1 = getObject.addressLine1.trim());
            getObject.addressLine2 && (getObject.addressLine2 = getObject.addressLine2.trim());
            getObject.addressLine3 && (getObject.addressLine3 = getObject.addressLine3.trim());
            getObject.addressLine4 && (getObject.addressLine4 = getObject.addressLine4.trim());
            getObject.addressLine5 && (getObject.addressLine5 = getObject.addressLine5.trim());

            getObject.firstName && (getObject.firstName = getObject.firstName.trim());
            getObject.lastName && (getObject.lastName = getObject.lastName.trim());
            getObject.taxNo && (getObject.taxNo = getObject.taxNo.trim());
            getObject.zipCode && (getObject.zipCode = getObject.zipCode.trim());
            getObject.emailAddress && (getObject.emailAddress = getObject.emailAddress.trim());
        }

        /****** phone no formatting started *****/
        if (getObject.phoneNumber && getObject.phoneNumber !== '') {

            let formatedPhone = '';
            let formatedPhoneArray = [];

            for (let letter of getObject.phoneNumber) {
                if (isNaN(letter) === false) {
                    formatedPhoneArray.push(letter);
                }
            }

            formatedPhoneArray = formatedPhoneArray.filter(entry => entry.trim() !== '');
            if (formatedPhoneArray.length === 11) {
                formatedPhoneArray.splice(0, 1);
            }
            formatedPhoneArray.splice(0, 0, '1 ');
            formatedPhoneArray.splice(5, 0, '-');
            formatedPhoneArray.splice(9, 0, '-');

            for (let n of formatedPhoneArray) {
                formatedPhone += n;
            }

            getObject.phoneNumber = formatedPhone;

        }
        return getObject;
    }



    render() {
        const initialValues = { ...this.props };
        console.log('initial value', initialValues);

        modifiedObject = this.handelMod(initialValues);
        console.log('MOD value', modifiedObject);

        const {
            selectedCities,
            selectedStates,
            //selectedCountryId, 
            disabled
        } = this.state;


        return (
            <Formik
                initialValues={modifiedObject}
                validationSchema={editVendorSchema}
                onSubmit={this.handleSubmit}
                enableReinitialize={true}
            >
                {({
                    values,
                    errors,
                    touched,
                    //isSubmitting,
                    handleChange,
                    //setFieldValue,
                    handleBlur
                }) => {
                    return (
                        <Form className={disabled === false ? ('hideRequired') : null}>
                            <Row>
                                <Col md={12}>
                                    {this.state.editErrorMessge ? (
                                        <div className="alert alert-danger" role="alert">
                                            {this.state.editErrorMessge}
                                        </div>
                                    ) : null}
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12}>
                                    {this.state.editVendorLoader ? (
                                        <LoadingSpinner/>
                                    ) : null}
                                </Col>
                            </Row>
                            <Row className="show-grid">
                                <Col xs={6} md={6}>
                                    <FormGroup controlId="formBasicText">
                                        <span>Customer Company Name <span className="required">*</span></span>
                                        <Field
                                            name="companyName"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.companyName}
                                            disabled={true}
                                            //disabled={disabled === false ? 'disabled' : ''}
                                        />
                                        {errors.companyName && touched.companyName ? (
                                            <span className="errorMsg ml-3">{errors.companyName}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>

                                <Col xs={6} md={6}>
                                    <FormGroup controlId="formBasicText">
                                        <span>Business Tax ID/SSN 
                                            {/* <span className="required">*</span> */}
                                        </span>
                                        {/* <Field
                                            name="taxNo"
                                            type="text"
                                            className={`form-control`}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.taxNo}
                                        /> */}
                                        <InputMask
                                            mask="99-99999999"
                                            maskChar={null}
                                            type="text"
                                            name="taxNo"
                                            className={'form-control'}
                                            value={values.taxNo}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            disabled={disabled === false ? 'disabled' : ''}
                                        />
                                        {errors.taxNo && touched.taxNo ? (
                                            <span className="errorMsg ml-3">{errors.taxNo}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                        {/* <HelpBlock>Validation Text</HelpBlock> */}
                                    </FormGroup>
                                </Col>
                            </Row>

                            <Row className="show-grid">
                                {/* <Col xs={12} md={6}>
                                    <FormGroup controlId="formControlsTextarea">
                                        <span>Company Street Address <span className="required">*</span></span>
                                        <Field
                                            name="address"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.address}
                                            disabled={disabled === false ? 'disabled' : ''}
                                        />
                                        {errors.address && touched.address ? (
                                            <span className="errorMsg ml-3">{errors.address}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col> */}

                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formControlsTextarea">
                                        <label>Address Line 1 <span className="required">*</span></label>
                                        <Field
                                            name="addressLine1"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.addressLine1 || ''}
                                            disabled={disabled === false ? 'disabled' : ''}
                                        />
                                        {errors.addressLine1 && touched.addressLine1 ? (
                                            <span className="errorMsg ml-3">{errors.addressLine1}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formControlsTextarea">
                                        <label>Address Line 2
                                        </label>
                                        <Field
                                            name="addressLine2"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.addressLine2 || ''}
                                            disabled={disabled === false ? 'disabled' : ''}
                                        />
                                        {errors.addressLine2 && touched.addressLine2 ? (
                                            <span className="errorMsg ml-3">{errors.addressLine2}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>

                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formControlsTextarea">
                                        <label>Address Line 3
                                        </label>
                                        <Field
                                            name="addressLine3"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.addressLine3 || ''}
                                            disabled={disabled === false ? 'disabled' : ''}
                                        />
                                        {errors.addressLine3 && touched.addressLine3 ? (
                                            <span className="errorMsg ml-3">{errors.addressLine3}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formControlsTextarea">
                                        <label>Address Line 4
                                        </label>
                                        <Field
                                            name="addressLine4"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.addressLine4 || ''}
                                            disabled={disabled === false ? 'disabled' : ''}
                                        />
                                        {errors.addressLine4 && touched.addressLine4 ? (
                                            <span className="errorMsg ml-3">{errors.addressLine4}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formControlsTextarea">
                                        <label>Address Line 5</label>
                                        <Field
                                            name="addressLine5"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.addressLine5 || ''}
                                            disabled={disabled === false ? 'disabled' : ''}
                                        />
                                        {errors.addressLine5 && touched.addressLine5 ? (
                                            <span className="errorMsg ml-3">{errors.addressLine5}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>

                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formBasicText">
                                        <span>Country <span className="required">*</span></span>

                                        <Field
                                            component="select"
                                            name="countryCode"
                                            placeholder="select"
                                            className="form-control"
                                            value={values.countryCode || ''}
                                            disabled={disabled === false ? 'disabled' : ''}
                                            onChange={e => {
                                                handleChange(e);
                                                this.populateState(e);
                                            }}
                                        >
                                            <option value="">
                                                {this.state.countries.length ? 'Select Country' : 'Loading...'}
                                            </option>


                                            {this.state.countries.map(country => (
                                                <option key={country.id} value={country.countryCode}>
                                                    {country.countryName}
                                                </option>
                                            ))}
                                        </Field>
                                        {errors.countryCode && touched.countryCode ? (
                                            <span className="errorMsg ml-3">{errors.countryCode}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                            </Row>

                            <Row className="show-grid">
                                <Col xs={12} md={6} className="cityField">
                                    <FormGroup controlId="formBasicText">
                                        <span>State <span className="required">*</span></span>
                                        <Field
                                            component="select"
                                            name="stateCode"
                                            placeholder="select"
                                            className="form-control"
                                            value={values.stateCode || ''}
                                            onChange={e => {
                                                handleChange(e);
                                                this.populateCity(e);
                                            }}
                                            disabled={selectedStates.length && disabled !== false ? false : true}
                                        >
                                            <option value="">Select State</option>

                                            {selectedStates.map(stName => (
                                                <option value={stName.stateCode} key={stName.id}>
                                                    {stName.stateName}
                                                </option>
                                            ))}
                                        </Field>
                                        {errors.stateCode && touched.stateCode ? (
                                            <span className="errorMsg ml-3">{errors.stateCode}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                                <Col xs={12} md={6} className="cityField">
                                    <FormGroup controlId="formBasicText">
                                        <span>City <span className="required">*</span></span>

                                        <Field
                                            component="select"
                                            name="cityId"
                                            placeholder="select"
                                            className="form-control"
                                            value={values.cityId || ''}
                                            onChange={e => {
                                                handleChange(e);
                                            }}
                                            disabled={selectedCities.length && disabled !== false ? false : true}
                                        >
                                            <option value="">Select City</option>

                                            {selectedCities.map(ct => (
                                                <option value={ct.id} key={ct.id}>
                                                    {ct.cityName}
                                                </option>
                                            ))}
                                        </Field>
                                        {errors.cityId && touched.cityId ? (
                                            <span className="errorMsg ml-3">{errors.cityId}</span>
                                        ) : null}


                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row className="show-grid">
                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formBasicText">
                                        <span>Zip Code <span className="required">*</span></span>
                                        <Field
                                            name="zipCode"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            disabled={disabled === false ? 'disabled' : ''}
                                        />
                                        {errors.zipCode && touched.zipCode ? (
                                            <span className="errorMsg ml-3">{errors.zipCode}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                            </Row>

                            <Row className="show-grid">
                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formBasicText">
                                        <span>First Name <span className="required">*</span></span>

                                        <Field
                                            name="firstName"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.firstName}
                                            disabled={disabled === false ? 'disabled' : ''}
                                        />
                                        {errors.firstName && touched.firstName ? (
                                            <span className="errorMsg ml-3">{errors.firstName}</span>
                                        ) : null}

                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formBasicText">
                                        <span>Last Name <span className="required">*</span></span>
                                        <Field
                                            name="lastName"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.lastName}
                                            disabled={disabled === false ? 'disabled' : ''}
                                        />
                                        {errors.lastName && touched.lastName ? (
                                            <span className="errorMsg ml-3">{errors.lastName}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                            </Row>

                            <Row className="show-grid">
                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formBasicText">
                                        <span>Work Email</span>
                                        <Field
                                            name="emailAddress"
                                            type="text"
                                            className={'form-control'}
                                            autoComplete="nope"
                                            placeholder="Enter"
                                            value={values.emailAddress}
                                            disabled={disabled === false ? 'disabled' : ''}
                                        />
                                        {errors.emailAddress && touched.emailAddress ? (
                                            <span className="errorMsg ml-3">{errors.emailAddress}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formBasicText">
                                        <span>Phone</span>

                                        <InputMask
                                            mask="1 999-999-9999"
                                            maskChar={null}
                                            type="tel"
                                            name="phoneNumber"
                                            className={`form-control ${values.phoneNumber &&
                                                'input-elem-filled'}`}
                                            value={values.phoneNumber}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            placeholder="Enter"
                                            disabled={disabled === false ? 'disabled' : ''}
                                        />
                                        {errors.phoneNumber && touched.phoneNumber ? (
                                            <span className="errorMsg ml-3">{errors.phoneNumber}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                            </Row>

                            <Row className="show-grid">
                                <Col xs={12} md={6}>
                                    <FormGroup controlId="formControlsSelect">
                                        <span>Payment Mode</span>

                                        <Field
                                            component="select"
                                            name="paymentMode"
                                            placeholder="select"
                                            className="form-control"
                                            value={values.paymentMode}
                                            disabled
                                        >
                                            <option value="">Select</option>
                                            <option value="ACH">ACH</option>
                                            <option value="WIRE">Wire Transfer</option>
                                            <option value="CHECK">CHECK</option>
                                        </Field>
                                        {errors.paymentMode && touched.paymentMode ? (
                                            <span className="errorMsg ml-3">{errors.paymentMode}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </FormGroup>
                                </Col>
                            </Row>

                            <Row className="show-grid">
                                <Col xs={12} md={12}>
                                    &nbsp;
                                </Col>
                            </Row>
                            <Row>&nbsp;</Row>
                            <Row className="show-grid text-center">
                                <Col xs={12} md={12}>

                                    <Fragment>
                                        {
                                            this.state.editVendorEnable !== true ? (
                                                <Fragment>
                                                    <Button className="blue-btn border-0" onClick={this.handleEditVendorEnable}>
                                                        Edit
                                                    </Button>
                                                </Fragment>
                                            ) : (
                                                <Fragment>
                                                    <Button
                                                        onClick={this.props.handelviewEditModalClose}
                                                        className="but-gray border-0 mr-2"
                                                    >
                                                            Cancel
                                                    </Button>
                                                    <Button type="submit" className="blue-btn ml-2 border-0">
                                                            Save
                                                    </Button>
                                                </Fragment>
                                            )
                                        }
                                    </Fragment>

                                </Col>
                            </Row>
                            {disabled === false ? null : (<Fragment>
                                <Row>
                                    <Col md={12}>
                                        <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                    </Col>
                                </Row>
                            </Fragment>)}
                        </Form>
                    );
                }}
            </Formik>
        );
    }
}

EditBusinessCustomers.propTypes = {
    globState: PropTypes.object,
    onClickAction: PropTypes.func,
    countryCode : PropTypes.string,
    stateCode: PropTypes.string,
    onReload: PropTypes.func,
    //onSuccess: PropTypes.func,
    onClick: PropTypes.func,
    handelviewEditModalClose:PropTypes.func,
    handleEditConfirMmsg:PropTypes.func,

};

export default EditBusinessCustomers;
