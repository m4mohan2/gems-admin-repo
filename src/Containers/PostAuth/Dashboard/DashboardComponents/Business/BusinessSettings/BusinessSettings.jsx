/* eslint-disable indent */
/* eslint-disable quotes */
/* eslint-disable no-unused-vars */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
    Collapse,
    FormGroup,
    //Container, 
    Image,
    Row,
    Modal,
    Col,
    Button
} from 'react-bootstrap';
import SuccessIco from './../../../../../../assets/success-ico.png';
import axios from './../../../../../../shared/eaxios';
import LoadingSpinner from './../../../../../../Components/LoadingSpinner/LoadingSpinner';
import LoadingOval from './../../../../../../Components/LoadingOval/LoadingOval';
//let _ = require('lodash');
import * as Yup from 'yup';
import { Formik, Field, Form, yupToFormErrors } from 'formik';
//import { OverlayTrigger, Tooltip } from 'react-bootstrap';

// let initialValues = {
//     bankId: 0,
//     companyId: 0,
//     accountNumber: 0,
//     outsourceId: 0,
//     accountName:'',
//     companyName:'',
//     customerVcOption:false,
//     creditLimit:0

// };


const iniValueCustVC = {};
let accountReceivable;
let accountpayable;


const iniValueIC = Yup.object().shape({

    accountReceivableEmail: Yup.string()
        .trim('Please remove whitespace')
        .email('Email must be valid')
        .strict()
        .required('Please enter valid email id'),

    accountPayableEmail: Yup.string()
        .trim('Please remove whitespace')
        .email('Email must be valid')
        .strict()
        .required('Please enter valid email id'),

});


const iniValueIL = Yup.object().shape({

    achLimit: Yup
        .number()
        .moreThan(0, 'Please enter valid ACH limit')
        .required('Please enter ACH limit'),

    invoiceAmount: Yup
        .number()
        .moreThan(0, 'Please enter valid invoice amount')
        .required('Please enter invoice amount'),

});

const qbOnlineIC = Yup.object().shape({

    clientId: Yup.string()
        .trim('Please remove whitespace')
        .required('Please enter valid client id'),

    clientSecret: Yup.string()
        .trim('Please remove whitespace')
        .required('Please enter valid client secret'),

});

const validateVC = Yup.object().shape({
    bankId: Yup
        .number()
        .required('Please select bank')
        .moreThan(0, 'Please select bank'),
    // wexMinimumBalance: Yup
    //     .number()
    //     .moreThan(0,'Please enter minimum balance')
    //     //.trim()
    //     .required('Please enter minimum balance'),
    companyId: Yup
        .number()
        .required('Please enter company ID')
        .moreThan(0, 'Please enter company ID'),
    accountNumber: Yup
        .number()
        .required('Please enter account number')
        .moreThan(0, 'Please enter account number'),
    outsourceId: Yup
        .number()
        .required('Please enter outsource ID'),
    accountName: Yup
        .string()
        .trim()
        .required('Please enter account name'),
    companyName: Yup
        .string()
        .trim()
        .required('Please enter company name'),
    customerVcOption: Yup
        .boolean(),
    // creditLimit: Yup
    //     .number(),

    creditLimit: Yup.mixed().when('customerVcOption', {
        is: true,
        then: Yup
            .number()
            .moreThan(0, 'Please enter credit limit'),
        otherwise: Yup.mixed().notRequired()
    })

    //.moreThan(0, 'Please enter minimum balance')
    //.required('Please enter amount'),


});

const validateCustVC = Yup.object().shape({
    cofCreditLimit: Yup
        .number()
        .required('Please enter credit limit')
        .moreThan(0, 'Must be more than 0')
        .typeError('Please enter a number'),

    cofAccountName: Yup.string().required('Please enter account name'),

    cofAccountNumber: Yup
        .number()
        .required('Please enter account number')
        .moreThan(0, 'Must be more than 0')
        .typeError('Please enter a number'),

    cofCompanyId: Yup
        .number()
        .required('Please enter company id')
        .typeError('Please enter a number'),

    cofCompanyName: Yup.string().required('Please enter company name'),

    cofOutsourceId: Yup
        .number()
        .required('Please enter outsource id')
        .typeError('Please enter a number'),
});

const iniValuePBOS = Yup.object().shape({

    // centralPaymentRoutingNumber:
    //     Yup.mixed().when('centralPaymentBankOption', {
    //         is: 'BOM',
    //         then: Yup.string()
    //             .trim('Please remove whitespace')
    //             // .strict()
    //             .required('Please enter a routing number'),
    //         otherwise: Yup.mixed().notRequired()
    //     }),
    achAccountNumber:
        Yup.mixed().when('centralPaymentBankOption', {
            is: 'BOM',
            then: Yup.string()
                .required('Please enter ACH account number')
                .max(10, "ACH account number can't more than 10 digit"),
            otherwise: Yup.mixed().notRequired()
        }),

    centralPaymentBankOption: Yup.string()
        .required('Please select bank'),

    centralPaymentAccountNumber:
        Yup.number()
            .max(99999999999999999, "Account number cannot be more than 17 digits")
            .required('Please enter a account number'),



    lockboxId:
        Yup.mixed().when('centralPaymentBankOption', {
            is: 'BOM',
            then: Yup
                .string()
                .required('Please enter Lock Box ID')
                .min(3, "Lock Box ID can't less than 3 digit")
                .max(3, "Lock Box ID can't more than 3 digit"),
            otherwise: Yup.mixed().notRequired()
        }),        


   

  
        

            
});

const iniValuePBO = {
    centralPaymentRoutingNumber: '101019084',
    centralPaymentBankOption: 'CP',
    centralPaymentAccountNumber: '923432745086'

};

const iniCardCO = Yup.object().shape({

    siteName: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .lowercase('site name must be in lowercase string')
        .required('Please enter site name'),

    merchantId: Yup.string()
        .trim('Please remove whitespace')
        .strict()
        .required('Please enter valid merchant ID'),

});

const ValidateNegoPeriod = Yup.object().shape({

    value: Yup.number()
        .max(999, 'No of days less than 999')
        .required('Please enter no of days'),

   

});

class BusinessSettings extends Component {



    state = {
        vartualCardEnebaled: false,
        vcErrorMessge: null,
        fetchErrorMsg: null,
        settings: [],
        newSettingsKey: [],

        initialLoader: false,
        selectedBusinessId: null,
        //bankName: [],
        bankName: [],
        vcMiniBal: 2000,
        iniValueVC: {},
        iniValueIC: {},
        iniValueIL: {},
        qbOnlineIC: {},
        qbdesktopIC: {},
        netSuiteIC: {},
        iniValuePBO: {},
        iniValueAUTO: {},
        iniValueAccTool: {},
        iniCardCO: {},
        iniNegoPeriod:{},
        iniDashboard:{},
        errorMessge: null,
        updateLoader: false,
        alertMsg: false,
        alertState: null,
        showConfirMmsg: false,
        autofylogMmsg: false,
        logStatusText: null,
        settingText: null,
        disableVC: false,
        bodyObj: {},
        autofyLoader: true,
        pingStattusError: null,
        autofyPing: false,
        UnitedMailConf: false,
        negotiationPeriod:false,
        iniValueUOI: {},
        logError: null,
        logLoader: false,
        vcPermission: '',
        creditChk: false,
        selectedPayementBank: '',
        // pbSubField: true,
        cpbusinesslist: [],
        //qbOption: '',
        qbOnlineVisible: false,
        qbDesktopVisible: false,
        netSuiteVisible: false,
        targetval: '',
        negoValue:0,

    }
    _isMounted = false;

    handelUnitedMailConf = () => {
        this.setState({
            UnitedMailConf: !this.state.UnitedMailConf
        });
    }
    handelNegotiationPeriod = () => {
        this.setState({
            negotiationPeriod: !this.state.negotiationPeriod
        });
    }

    pbSubFieldEneble = (e, setFieldValue) => {
        const cpbusiness = this.state.cpbusinesslist.filter(ct => e.target.value === ct.bankOption);
        console.log('this.state.iniValuePBO ', cpbusiness);
        if (cpbusiness && cpbusiness.length > 0) {
            setFieldValue('lockboxId', cpbusiness[0].lockboxId);
            setFieldValue('achAccountNumber', cpbusiness[0].achAccountNumber);
            setFieldValue('centralPaymentAccountNumber', cpbusiness[0].accountNumber);
            setFieldValue('centralPaymentRoutingNumber', cpbusiness[0].routingNumber);
        }
        else {

            setFieldValue('lockboxId', '');
            setFieldValue('achAccountNumber', '');
            setFieldValue('centralPaymentAccountNumber', '');
            setFieldValue('centralPaymentRoutingNumber', '');
        }


    }


    getcpforbusiness = () => {
        try {
            return new Promise(async (resolve) => {
                axios
                    .get(
                        `/centralPayment/${this.props.globalState.business.businessData.id}`
                    )
                    .then(res => {
                        resolve();
                        console.log('getcpforbusiness', res);
                        this.setState({
                            cpbusinesslist: res.data
                        });
                    })
                    .catch(e => {
                        resolve();
                    });
            });
        } catch (err) {
            console.log('centralPayment error ', err);
            this.handelInitialSettings();
        }
    }
    getAutofyPingStatus = () => {
        this.setState({
            autofyLoader: true,
        });
        axios
            .get(
                `business/settings/autofy/pingstatus/${this.props.globalState.business.businessData.id}`
            )
            .then(res => {
                console.log('res.data for settings', res.data);
                if (this._isMounted) {
                    this.setState({
                        autofyLoader: false,
                        autofyPing: res.data.autofyPing
                    });
                }
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    pingStattusError: errorMsg,
                    autofyLoader: false
                }, () => {
                    console.log('this.state.pingStattusError', this.state.pingStattusError);
                });

                setTimeout(() => {
                    this.setState({ pingStattusError: null });
                }, 5000);
            });

    }
    getAutofyPingStatus = () => {
        this.setState({
            autofyLoader: true
        });
        axios
            .get(
                `business/settings/autofy/pingstatus/${this.props.globalState.business.businessData.id}`
            )
            .then(res => {
                console.log('res.data for settings', res.data);
                if (this._isMounted) {
                    this.setState({
                        autofyLoader: false,
                        autofyPing: res.data.autofyPing
                    });
                }
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    pingStattusError: errorMsg,
                    autofyLoader: false
                }, () => {
                    console.log('this.state.pingStattusError', this.state.pingStattusError);
                });

                setTimeout(() => {
                    this.setState({ pingStattusError: null });
                }, 5000);
            });

    }

    handelAutofyLog = () => {
        this.setState({
            logLoader: true
        });
        axios
            .get(
                `business/autofylog/${this.props.globalState.business.businessData.id}`
            )
            .then(res => {
                this.setState({
                    logLoader: false
                });
                console.log('%%%%%%autofylog%%%%%', res.data.logFileUrl);

                if (res.data.logFileUrl == '') {
                    this.setState({
                        logError: 'Unable to download logfile due to Autofy not sync',
                        autofylogMmsg: true
                    });
                    // setTimeout(() => {
                    //     this.setState({ logError: null });
                    // }, 5000);
                } else {
                    this.setState({
                        autofylogMmsg: true,
                        logStatusText: 'Autofy log file download successfully',
                    });

                    let a = document.createElement('a');
                    a.href = res.data.logFileUrl;
                    a.click();
                }

            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    logError: errorMsg,
                    logLoader: false,
                    autofylogMmsg: true
                }, () => {
                    console.log('this.state.logError', this.state.logError);
                });

                // setTimeout(() => {
                //     this.setState({ logError: null });
                // }, 5000);
            });

    }

    handelInitialSettings = () => {
        // console.log('handelInitialSettings');
        this.setState({
            initialLoader: true,
            iniValueVC: {},
            iniValueIC: {},
            iniValueIL: {},
            iniCardCO: {},
            qbOnlineIC: {},
            qbdesktopIC: {},
            netSuiteIC: {},
            iniValuePBO: {},
            iniValueAccTool: {},
            iniNegoPeriod: {},
            iniDashboard:{}

        }, () => {
            axios
                .get(
                    `business/settings/list/${this.props.globalState.business.businessData.id}`
                )
                .then(res => {
                    console.log('res.data for settings', res.data);
                    const settings = res.data;

                    let newSettingsKey = [];

                    if (this._isMounted) {
                        settings.map(settings => (
                            settings.value === 'true' ? newSettingsKey.push(settings.key) : null,
                            settings.key === 'VC_ENABLED'
                                ? this.setState({ iniValueVC: settings.settingsProp }, () => {

                                    let iniValueVC = this.state.iniValueVC;
                                    // iniValueVC.creditLimit = iniValueVC.customerVC[1].value;
                                    // iniValueVC.customerVcOption = this.state.iniValueVC.customerVC[0].value == 'true' ? true : false;
                                    // this.setState({ creditChk: !iniValueVC.customerVcOption });

                                    if (iniValueVC.creditLimit) {
                                        this.setState({
                                            iniValueVC
                                        }
                                            // , () => { initialValues = iniValueVC;}
                                        );
                                    }
                                })
                                : (settings.key === 'AUTOFY_ENABLED' ? this.setState({ iniValueAUTO: settings.settingsProp })
                                    : (settings.key === 'UnitedMail Onboarding Information' ? this.setState({ iniValueUOI: settings.settingsProp })
                                        : settings.key === 'INVOICE_CAPTURE' ? this.setState({ iniValueIC: settings.settingsProp })
                                            : settings.key === 'INVOICE_LIMIT' ? this.setState({ iniValueIL: settings.settingsProp })
                                            : settings.key === 'CARD_CONNECT_OPTION' ? this.setState({ iniCardCO: settings.settingsProp })
                                                : settings.key === 'PAYMENT_BANK_OPTION' ? this.setState({ iniValuePBO: settings.settingsProp })
                                                    : settings.key === 'NEGOTIATION_PERIOD' ? this.setState({ iniNegoPeriod: settings })
                                                            : settings.key === 'ENABLE_DASHBOARD' ? this.setState({ iniDashboard: settings.settingsProp }, () => { console.log('iniDashboard####################################', this.state.iniDashboard);})
                                                            : settings.key === 'ACCOUNTING_TOOL' ? this.setState({ iniValueAccTool: settings.settingsProp }, () => {
                                                                const { iniValueAccTool } = this.state;
                                                                if (iniValueAccTool) {
                                                                    this.setState({ qbOnlineIC: iniValueAccTool.qbOnline, qbdesktopIC: iniValueAccTool.qbDesktop, netSuiteIC: iniValueAccTool.netSuite});
                                                                }
                                                                if ((iniValueAccTool.qbDesktop.value == "false") && (iniValueAccTool.qbOnline.value == "false") && (iniValueAccTool.netSuite.value == "false")) {
                                                                    this.setState({qbOnlineVisible: false, qbDesktopVisible: false, netSuiteVisible: false}),
                                                                    this.setState({targetval:''});
                                                                } else if (iniValueAccTool.qbDesktop.value == "true") {
                                                                    this.setState({qbDesktopVisible: true, qbOnlineVisible: false,netSuiteVisible: false }, 
                                                                    () => { this.getAutofyPingStatus(); },
                                                                    this.setState({targetval:'qbDesktop'}));
                                                                }else if (iniValueAccTool.netSuite.value == "true") {
                                                                    this.setState({qbDesktopVisible: false, qbOnlineVisible: false,netSuiteVisible: true}),
                                                                    this.setState({targetval:'netSuite'});
                                                                }else
                                                                    if (iniValueAccTool.qbOnline.value == "true") {
                                                                        this.setState({qbOnlineVisible: true, qbDesktopVisible: false, netSuiteVisible: false }),
                                                                        this.setState({targetval:'qbOnline'});
                                                                    }
                                                            })
                                                                : settings.key === 'CUSTOMER_VC_ENABLED' ? Object.assign(iniValueCustVC, settings.settingsProp)
                                                                        : settings.key === 'ACCOUNT_RECEIVABLE' ? (accountReceivable= settings.value)
                                                                            : settings.key === 'ACCOUNT_PAYAEBLE' ? (accountpayable = settings.value)
                                                                            : null
                                    )),

                            settings.key === 'AUTOFY_ENABLED' && settings.value === 'true' ? this.getAutofyPingStatus() : null


                        ));
                        console.log('----------------////////////////newSettingsKey---------------------', newSettingsKey);

                    }
                    this.setState({
                        settings,
                        newSettingsKey,
                        initialLoader: false
                    }, () => {
                        console.log('settingsProp.autofyToken', this.state.settings);
                    });

                })
                .catch(e => {
                    let errorMsg = this.displayError(e);
                    this.setState({
                        fetchErrorMsg: errorMsg,
                        initialLoader: false
                    }, () => {
                        console.log('this.state.fetchErrorMsg', this.state.fetchErrorMsg);
                    });

                    setTimeout(() => {
                        this.setState({ fetchErrorMsg: null });
                    }, 5000);
                });

        });

    }

    updateSettings = (param) => {



        this.setState({
            updateLoader: true,
        });

        axios
            .put(
                `business/settings/update/${this.props.globalState.business.businessData.id}`, param
            ).then((res) => {
                if (param.clientId) {

                    axios.post(
                        `business/settings/setBusinessAuth/${this.props.globalState.business.businessData.id}`,
                        { 'qbClientId': param.clientId, 'qbClientSecret': param.clientSecret }
                    );
                }
                console.log('vc res', res);
                this.setState({
                    updateLoader: false,
                    alertMsg: false,
                    alertState: null,
                    showConfirMmsg: true,
                    bodyObj: {}
                });
                this.getcpforbusiness().then(() => {
                    this.handelInitialSettings();
                });
            }).catch(e => {
                console.log('vc error', e);
                let errorMsg = this.displayError(e);
                this.setState({
                    updateLoader: false,
                    alertMsg: false,
                    errorMessge: errorMsg,
                    bodyObj: {}

                }, () => console.log('vc error msg', this.state.errorMessge));
                this.getcpforbusiness().then(() => {
                    this.handelInitialSettings();
                });
                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);

            });
    }

    handelAlertMsg = (param, settingText) => {
        this.setState({
            alertMsg: true,
            alertState: param,
            settingText
        });

    }

    showVcPermission = () => {
        this.setState({
            vcPermission: 'Virtual Credit Card is disabled since no bank is linked, please add a bank.'
        });
        setTimeout(() => {
            this.setState({
                vcPermission: ''
            });
        }, 3000);
    }

    handelSetting = (param, settingText) => {
        let newSettingsKeyTemp = this.state.newSettingsKey;
        let paramIndex = newSettingsKeyTemp.indexOf(param);

        if (paramIndex > -1) {
            newSettingsKeyTemp.splice(paramIndex, 1);
        } else {
            newSettingsKeyTemp.push(param);
        }

        this.setState({
            newSettingsKey: newSettingsKeyTemp
        });

        if (param === 'VC_ENABLED') {
            if (paramIndex > -1) {
                let temp = {};
                temp.key = 'VC_ENABLED';
                temp.value = 'false';

                this.setState({
                    bodyObj: temp
                });
                this.handelAlertMsg('VC_ENABLED', settingText);
            }
        }

        if (param === 'AUTOFY_ENABLED') {
            let temp = {};
            temp.key = 'AUTOFY_ENABLED';
            temp.value = paramIndex > -1 ? 'false' : 'true';

            this.setState({
                bodyObj: temp
            });
            this.handelAlertMsg('AUTOFY_ENABLED', settingText);
        }

        if (param === 'INVOICE_CAPTURE') {
            if (paramIndex > -1) {
                let temp = {};
                temp.key = 'INVOICE_CAPTURE';
                temp.value = 'false';

                this.setState({
                    bodyObj: temp
                });
                this.handelAlertMsg('INVOICE_CAPTURE', settingText);
            }

        }

        if (param === 'INVOICE_LIMIT') {
            if (paramIndex > -1) {
                let temp = {};
                temp.key = 'INVOICE_LIMIT';
                temp.value = 'false';

                this.setState({
                    bodyObj: temp
                });
                this.handelAlertMsg('INVOICE_LIMIT', settingText);
            }

        }

        if (param === 'PAYMENT_BANK_OPTION') {
            if (paramIndex > -1) {
                let temp = {};
                temp.key = 'PAYMENT_BANK_OPTION';
                temp.value = 'false';

                this.setState({
                    bodyObj: temp
                });
                this.handelAlertMsg('PAYMENT_BANK_OPTION', settingText);
            }

        }

        if (param === 'ACCOUNTING_TOOL') {
            if (paramIndex > -1) {
                let temp = {};
                temp.key = 'ACCOUNTING_TOOL';
                temp.value = 'false';

                this.setState({
                    bodyObj: temp
                });
                this.handelAlertMsg('ACCOUNTING_TOOL', settingText);
            }

        }

        if (param === 'CARD_CONNECT_OPTION') {
            if (paramIndex > -1) {
                let temp = {};
                temp.key = 'CARD_CONNECT_OPTION';
                temp.value = 'false';

                this.setState({
                    bodyObj: temp
                });
                this.handelAlertMsg('CARD_CONNECT_OPTION', settingText);
            }

        }

        if (param === 'CUSTOMER_VC_ENABLED') {
            if (paramIndex > -1) {
                let temp = {};
                temp.key = 'CUSTOMER_VC_ENABLED';
                temp.value = 'false';
                this.setState({
                    bodyObj: temp
                });
                this.handelAlertMsg('CUSTOMER_VC_ENABLED', settingText);
            }
        }

        if (param === 'NEGOTIATION_PERIOD') {
            // if (paramIndex > -1) {
                // let temp = {};
                // temp.key = 'NEGOTIATION_PERIOD';
                // temp.value = this.state.negoValue;
                // this.setState({
                //     bodyObj: temp
                // });
                this.handelAlertMsg('NEGOTIATION_PERIOD', settingText);
            // }
        }

        if (param === 'ENABLE_DASHBOARD') {
            if (paramIndex > -1) {
                let temp = {};
                temp.key = 'ENABLE_DASHBOARD';
                temp.value = 'false';
                this.setState({
                    bodyObj: temp
                });
                this.handelAlertMsg('ENABLE_DASHBOARD', settingText);
            }
        }

        if (param === 'ACCOUNT_RECEIVABLE') {
            if (paramIndex > -1) {
                let temp = {};
                temp.key = 'ACCOUNT_RECEIVABLE';
                temp.value = 'false';
                this.setState({
                    bodyObj: temp
                });
                this.handelAlertMsg('ACCOUNT_RECEIVABLE', settingText);
            }
        }

        if (param === 'ACCOUNT_PAYAEBLE') {
            if (paramIndex > -1) {
                let temp = {};
                temp.key = 'ACCOUNT_PAYAEBLE';
                temp.value = 'false';
                this.setState({
                    bodyObj: temp
                });
                this.handelAlertMsg('ACCOUNT_PAYAEBLE', settingText);
            }
        }

    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }

        return errorMessge;
    }

    bankOnchange = (e) => {
        this.setState(
            { selectedBankName: e.target.value },
            () => {
                console.log('selectedBankName', this.state.selectedBankName);
            }
        );
    }

    paymentankOnchange = (e) => {
        this.setState(
            { selectedPayementBank: e.target.value },
            () => {
                console.log('selectedPayementBank', this.state.selectedPayementBank);
            }
        );
    }
    minibalOnchange = (e) => {
        this.setState(
            { vcMiniBal: e.target.value },
            () => {
                console.log('vcMiniBal', this.state.vcMiniBal);
            }
        );
    }

    handleChange = (e, field) => {
        this.setState({
            [field]: e.target.value
        });
    };

    handleCustomerVcOpt = (e) => {

        this.setState({ creditChk: !e.target.checked }, () => console.log('---------', !this.state.creditChk));
    }

    handleSubmitVC = (values) => {
        let temp = {};

        temp = values;
        // EXP
        // temp.customerVC[0].value = parseFloat(values.customerVcOption).toFixed(2);
        // temp.customerVC[1].value = (values.creditLimit).toString();
        // temp.creditLimit = (values.creditLimit).toString();
        temp.key = 'VC_ENABLED';
        temp.value = 'true';
        //temp.wexMinimumBalance = parseFloat(values.wexMinimumBalance).toFixed(2);
        delete temp.customerVC;
        //console.log('llllllllllllllllllllllllllllllllllll', temp);
        this.setState({
            bodyObj: temp
        });

        this.handelAlertMsg('VC_ENABLED', 'Virtual Card');
    }
    handleSubmitIC = (values) => {
        let temp = {};

        temp = values;
        temp.key = 'INVOICE_CAPTURE';
        temp.value = 'true';
        this.setState({
            bodyObj: values
        });
        this.handelAlertMsg('INVOICE_CAPTURE', 'invoice capture');
    }

    handleSubmitIL = (values) => {
        let temp = {};

        temp = values;
        temp.key = 'INVOICE_LIMIT';
        temp.value = 'true';
        this.setState({
            bodyObj: values
        });
        this.handelAlertMsg('INVOICE_LIMIT', 'invoice limit');
    }

    handleSubmitDashboardType = (values)=>{
       
        
        const temp = values;
        temp.key = 'ENABLE_DASHBOARD',
        temp.value = 'true';
        this.setState({
            bodyObj: temp
        }, () => { console.log('------------------ENABLE_DASHBOARD bodyObj -----------------', this.state.bodyObj);});
        this.handelAlertMsg('ENABLE_DASHBOARD', 'enable dashboard');
    }

    handleSubmitAR = ()=>{
        const temp={};
        temp.key = 'ACCOUNT_RECEIVABLE',
            temp.value = 'true';
        this.setState({
            bodyObj: temp
        }, () => { console.log('------------------ACCOUNT_RECEIVABLE bodyObj -----------------', this.state.bodyObj); });
        this.handelAlertMsg('ACCOUNT_RECEIVABLE', 'account receivable');
    }

    handleSubmitAP = () => {
        const temp = {};
        temp.key = 'ACCOUNT_PAYAEBLE',
            temp.value = 'true';
        this.setState({
            bodyObj: temp
        }, () => { console.log('------------------ACCOUNT_PAYAEBLE bodyObj -----------------', this.state.bodyObj); });
        this.handelAlertMsg('ACCOUNT_PAYAEBLE', 'account payable');
    }

    handleSubmitCardCO = (values) => {
        let temp = {};

        temp = values;
        temp.key = 'CARD_CONNECT_OPTION';
        temp.value = 'true';
        this.setState({
            bodyObj: values
        });
        this.handelAlertMsg('CARD_CONNECT_OPTION', 'card connect option');
    }

    handleSubmitNegoPeriod = (values) => {
            this.setState({
                bodyObj: values
            });
            this.handelAlertMsg('NEGOTIATION_PERIOD', 'preferred negotiation period');
   
        
    }




    /**hang***/
    handleSubmitQBonline = (values) => {
        let temp = {};

        temp = values;

        temp.key = 'ACCOUNTING_TOOL';
        temp.value = 'true';
        temp.qbOnline = 'true';
        this.setState({
            bodyObj: temp
        });
        this.handelAlertMsg('ACCOUNTING_TOOL', 'accounting tool');
    }

    handleQBdesktop = () => {

        let temp = {};
        temp = this.state.iniValueAccTool.qbDesktop;
        temp.key = 'ACCOUNTING_TOOL';
        temp.value = 'true';
        temp.qbDesktop = 'true';
        this.setState({
            bodyObj: temp
        });
        this.handelAlertMsg('ACCOUNTING_TOOL', 'accounting tool');

    }

    handleNetSuite = () => {

        let temp = {};
        temp = this.state.iniValueAccTool.netSuite;
        temp.key = 'ACCOUNTING_TOOL';
        temp.value = 'true';
        temp.netSuite = 'true';
        this.setState({
            bodyObj: temp
        });
        this.handelAlertMsg('ACCOUNTING_TOOL', 'accounting tool');

    }

    handleSubmitPBO = (values) => {
        console.log(values);

        let temp = {};

        temp = values;
        temp.key = 'PAYMENT_BANK_OPTION';
        temp.value = values.centralPaymentBankOption;
        values.achAccountNumber = values.achAccountNumber.toString();
        values.centralPaymentAccountNumber = values.centralPaymentAccountNumber.toString();
        this.setState({
            bodyObj: values
        });
        this.handelAlertMsg('PAYMENT_BANK_OPTION', 'payment bank option');

    }

    handleSubmitAT = (values) => {
        let temp = {};

        temp = values;
        temp.key = 'ACCOUNTING_TOOL';
        temp.value = 'true';
        this.setState({
            bodyObj: values
        });
        this.handelAlertMsg('ACCOUNTING_TOOL', 'accounting tools');
    }

    numberFormat = (num) => {
        return parseFloat(num).toFixed(2);
    }


    closeAlertMsg = () => {
        this.getcpforbusiness().then(() => {
            this.handelInitialSettings();
        });
        this.setState({
            alertMsg: false,
            alertState: null
        });
    }
    // handleSubmit = (values) => {
    //     let temp = {};
    //     console.log('submitted value',values.wexMinimumBalance);
    //     if(values.hasOwnProperty('wexMinimumBalance') === true){
    //         temp.key = 'VC_ENABLED';
    //     }

    // }

    handleConfirmReviewClose = () => {
        this.setState({
            showConfirMmsg: false,
            settingText: null
        });
    };

    handleConfirmReviewShow = () => {
        this.setState({ showConfirMmsg: true });
    };

    handleLogReviewClose = () => {
        this.setState({
            autofylogMmsg: false,
            logStatusText: null,
            logError: null

        });
    };

    handleLogReviewShow = () => {
        this.setState({
            autofylogMmsg: true
        });
    };

    qbTypeHandler = (e) => {
        this.setState({targetval : e.target.value}); 
        this.setState({
            //qbOption: this.state.targetval == 'qbDesktop' ? true : this.state.targetval == 'netSuite' ? true : false,

        }, () => {
            //console.log('this.state.qbOption', this.state.qbOption);
            console.log('targetval', this.state.targetval);
            if (this.state.targetval == 'qbDesktop') {
                this.setState({
                    qbDesktopVisible: true,
                    qbOnlineVisible: false,
                    netSuiteVisible: false
                }, () => {
                    this.getAutofyPingStatus();
                });
            } 
            if (this.state.targetval == 'qbOnline') {
                this.setState({
                    qbOnlineVisible: true,
                    qbDesktopVisible: false,
                    netSuiteVisible: false
                });
            } 
            if(this.state.targetval == 'netSuite'){
                this.setState({
                    qbOnlineVisible: false,
                    qbDesktopVisible: false,
                    netSuiteVisible: true
                });
            }
        });
    }

    


    getVcBanks = () => {
        axios.get(`transaction/bank/${this.state.selectedBusinessId}/?since=0&limit=20`)
            .then(res => {
                if (res.data.entries.length > 0) {
                    if (this._isMounted) {
                        this.setState({
                            bankName: res.data.entries,
                        }, () => {
                            console.log('this.state.bankName----- transaction', this.state.bankName);
                        });
                    }
                } else {
                    if (this._isMounted) {
                        this.setState({
                            disableVC: true
                        });
                    }
                }


            })
            .catch(err => {
                console.log('error', err);
            });
    }

    handleSubmitCustVC = (values) => {
        console.log(values);
        let temp = {};
        temp = values;
        temp.value = 'true';
        temp.key = 'CUSTOMER_VC_ENABLED';
        this.setState({
            bodyObj: temp
        });
        this.handelAlertMsg('CUSTOMER_VC_ENABLED', 'card on file');

    }


    /*dashOnchange=(e)=>{
        console.log('Onchange values----------------------', e.target.value);
        if (e.target.value == 'cit'){
            console.log('cit state', this.state.iniDashboard.isCitEnable);
            

            const  iniDashboard = { ...this.state.iniDashboard };
            iniDashboard.isCitEnable = (!this.state.iniDashboard.isCitEnable).toString();
            this.setState({ iniDashboard },() => console.log('cit ----', this.state.iniDashboard));

        }
        if (e.target.value == 'dox') {
            console.log('dox state', this.state.iniDashboard.isDoximEnable);
            const iniDashboard = { ...this.state.iniDashboard };
            iniDashboard.isDoximEnable = (!this.state.iniDashboard.isDoximEnable).toString();
            this.setState({ iniDashboard }, () => console.log('dox ----', this.state.iniDashboard));


        }



    }*/

    componentDidMount() {
        this._isMounted = true;

        this.setState({
            selectedBusinessId: this.props.globalState.business.businessData.id,
            initialLoader: true,
        }, () => {
            this.getVcBanks();
            this.getcpforbusiness().then(() => {
                this.handelInitialSettings();
            });
        });
    }
    componentWillUnmount() {
        this._isMounted = false;

    }

    render() {
        const { newSettingsKey, initialLoader,
            cpbusinesslist, iniValueUOI } = this.state;
        console.log('initialLoader', initialLoader);
        return (

            <div className="bg-white">
                {this.state.errorMessge ? <div className="alert alert-danger" role="alert">{this.state.errorMessge}</div> : null}
                {this.state.fetchErrorMsg ? <div className="alert alert-danger">{this.state.fetchErrorMsg}</div> : null}
                {

                    this.state.initialLoader
                        ?
                        <LoadingSpinner />
                        :

                        <div>
                            <div className="p-3" >
                                <label className="switch mt-2">
                                    <input
                                        type="checkbox"
                                        name="vartual Card"
                                        readOnly
                                        checked={newSettingsKey.indexOf('VC_ENABLED') > -1 ? true : false}
                                        onClick={this.state.disableVC ? () => this.showVcPermission() : () => this.handelSetting('VC_ENABLED', 'Virtual Card')}
                                    //disabled={this.state.disableVC ? true : false}
                                    />
                                    <span className="slider round"></span>
                                </label>
                                {/* {this.state.disableVC
                                    ?
                                    <OverlayTrigger placement="right" overlay={<Tooltip id="tooltip">
                                        Virtual Credit Card is disabled since no bank is linked, please add a bank.
                                    </Tooltip>}>
                                        <label className="switch mt-2">
                                            <input
                                                type="checkbox"
                                                name="vartual Card"
                                                checked={newSettingsKey.indexOf('VC_ENABLED') > -1 ? true : false}
                                                //onClick={() => this.handelSetting('VC_ENABLED', 'Virtual Card')}
                                                disabled={this.state.disableVC ? true : false}
                                            />
                                            <span className="slider round"></span>
                                        </label>
                                    </OverlayTrigger>
                                    :
                                    <label className="switch mt-2">
                                        <input
                                            type="checkbox"
                                            name="vartual Card"
                                            checked={newSettingsKey.indexOf('VC_ENABLED') > -1 ? true : false}
                                            onClick={() => this.handelSetting('VC_ENABLED', 'Virtual Card')}
                                            disabled={this.state.disableVC ? true : false}
                                        />
                                        <span className="slider round"></span>
                                    </label>
                                } */}


                                <span className="label float-left mr-3 mt-2 font-weight-bold">Virtual Credit Card Enable</span>
                                <Collapse in={newSettingsKey.indexOf('VC_ENABLED') > -1 ? true : false}>

                                    <div>
                                        <Formik
                                            enableReinitialize
                                            initialValues={this.state.iniValueVC}
                                            validationSchema={validateVC}
                                            onSubmit={this.handleSubmitVC}
                                        >
                                            {({
                                                values,
                                                errors,
                                                touched,
                                                handleChange,
                                                handleBlur,
                                                //onChange,
                                                setFieldValue
                                            }) => {
                                                return (
                                                    <Form className="p-4 border shadow-sm">
                                                        <div className="row">
                                                            <div className="col-sm-6">
                                                                <FormGroup controlId="formBasicText">
                                                                    <div className="mb-2">Select Bank <span className="required">*</span></div>
                                                                    <Field
                                                                        name="bankId"
                                                                        component="select"
                                                                        className="form-control truncate w-100"
                                                                        autoComplete="nope"
                                                                        onChange={e => {
                                                                            handleChange(e);
                                                                            // setFieldValue('key', e);
                                                                        }}
                                                                        value={values.bankId || ''}
                                                                        onBlur={handleBlur}
                                                                    >
                                                                        <option value=''>
                                                                            {this.state.bankName.length > 0 ? 'Select Bank' : 'Loading...'}
                                                                        </option>
                                                                        {
                                                                            this.state.bankName.length > 0 && this.state.bankName.map(i => (
                                                                                <option key={i.id} value={i.id}> {i.bankName}</option>
                                                                            ))
                                                                        }
                                                                    </Field>

                                                                    {errors.bankId && touched.bankId ? (
                                                                        <span className="errorMsg ml-3 mt-1">
                                                                            {errors.bankId}
                                                                        </span>
                                                                    ) : null}

                                                                </FormGroup>
                                                            </div>

                                                            {/* <div className="col-sm-6">
                                                                <FormGroup controlId="formBasicText">
                                                                    <div className="mb-2">Minimum Balance ($) <span className="required">*</span></div>
                                                                    <Field
                                                                        name="wexMinimumBalance"
                                                                        type="number"
                                                                        className="form-control"
                                                                        autoComplete="nope"
                                                                        value={values.wexMinimumBalance || ''}
                                                                        onBlur={handleBlur}
                                                                        placeholder="0.00"
                                                                    />
                                                                    {errors.wexMinimumBalance && touched.wexMinimumBalance ? (
                                                                        <span className="errorMsg pl-3">{errors.wexMinimumBalance}</span>
                                                                    ) : null}

                                                                </FormGroup>
                                                            </div> */}

                                                            <div className="col-sm-6">
                                                                <FormGroup controlId="formBasicText">
                                                                    <div className="mb-2">Account Name<span className="required">*</span></div>
                                                                    <Field
                                                                        name="accountName"
                                                                        type="text"
                                                                        className="form-control"
                                                                        autoComplete="nope"
                                                                        value={values.accountName || ''}
                                                                        onBlur={handleBlur}
                                                                    />


                                                                    {errors.accountName && touched.accountName ? (
                                                                        <span className="errorMsg ml-3 mt-1">
                                                                            {errors.accountName}
                                                                        </span>
                                                                    ) : null}
                                                                </FormGroup>
                                                            </div>

                                                            <div className="col-sm-6">
                                                                <FormGroup controlId="formBasicText">
                                                                    <div className="mb-2">Account Number<span className="required">*</span></div>
                                                                    <Field
                                                                        name="accountNumber"
                                                                        type="number"
                                                                        className="form-control"
                                                                        autoComplete="nope"
                                                                        value={values.accountNumber || ''}
                                                                        onBlur={handleBlur}
                                                                    />


                                                                    {errors.accountNumber && touched.accountNumber ? (
                                                                        <span className="errorMsg ml-3 mt-1">
                                                                            {errors.accountNumber}
                                                                        </span>
                                                                    ) : null}
                                                                </FormGroup>
                                                            </div>

                                                            <div className="col-sm-6">
                                                                <FormGroup controlId="formBasicText">
                                                                    <div className="mb-2">Company Id<span className="required">*</span></div>
                                                                    <Field
                                                                        name="companyId"
                                                                        type="number"
                                                                        className="form-control"
                                                                        autoComplete="nope"
                                                                        value={values.companyId || ''}
                                                                        onBlur={handleBlur}
                                                                    />


                                                                    {errors.companyId && touched.companyId ? (
                                                                        <span className="errorMsg ml-3 mt-1">
                                                                            {errors.companyId}
                                                                        </span>
                                                                    ) : null}
                                                                </FormGroup>
                                                            </div>
                                                            <div className="col-sm-6">
                                                                <FormGroup controlId="formBasicText">
                                                                    <div className="mb-2">Company Name<span className="required">*</span></div>
                                                                    <Field
                                                                        name="companyName"
                                                                        type="text"
                                                                        className="form-control"
                                                                        autoComplete="nope"
                                                                        value={values.companyName || ''}
                                                                        onBlur={handleBlur}
                                                                    />


                                                                    {errors.companyName && touched.companyName ? (
                                                                        <span className="errorMsg ml-3 mt-1">
                                                                            {errors.companyName}
                                                                        </span>
                                                                    ) : null}
                                                                </FormGroup>
                                                            </div>

                                                            <div className="col-sm-6">
                                                                <FormGroup controlId="formBasicText">
                                                                    <div className="mb-2">Outsource Id<span className="required">*</span></div>
                                                                    <Field
                                                                        name="outsourceId"
                                                                        type="number"
                                                                        className="form-control"
                                                                        autoComplete="nope"
                                                                        value={values.outsourceId || ''}
                                                                        onBlur={handleBlur}
                                                                    />


                                                                    {errors.outsourceId && touched.outsourceId ? (
                                                                        <span className="errorMsg ml-3 mt-1">
                                                                            {errors.outsourceId}
                                                                        </span>
                                                                    ) : null}
                                                                </FormGroup>
                                                            </div>
                                                            {/* <div className="col-sm-6">

                                                                <FormGroup controlId="formBasicText">
                                                                    <div className="mb-2 mt-3">
                                                                        <label className="customCkBox customCkBox2 businessCustom">
                                                                            <Field
                                                                                name="customerVcOption"
                                                                                type="checkbox"
                                                                                value={values.customerVcOption}
                                                                                onChange={(e) => {
                                                                                    console.log('OnChange', e.target.checked);
                                                                                    this.handleCustomerVcOpt(e);
                                                                                    setFieldValue('customerVcOption', e.target.checked);
                                                                                }}
                                                                                checked={values.customerVcOption}
                                                                                className={'float-left'}
                                                                            />
                                                                            <span className="checkmark" />
                                                                        </label>
                                                                        <div className={'clearfix font-weight-500'}>Enable virtual card for business</div>
                                                                    </div> 


                                                                </FormGroup>
                                                            </div> */}
                                                            {/* {!this.state.creditChk &&
                                                                <div className="col-sm-6">
                                                                    <FormGroup controlId="formBasicText">
                                                                        <div className="mb-2">
                                                                            <div className=''>Credit Limit ($)<span className="required">*</span></div>
                                                                            <Field
                                                                                name="creditLimit"
                                                                                type="number"
                                                                                className="form-control float-right"
                                                                                autoComplete="nope"
                                                                                value={values.creditLimit || ''}
                                                                                onBlur={handleBlur}
                                                                                placeholder="0"
                                                                                disabled={this.state.creditChk}
                                                                            />
                                                                            <div className={'clearfix'}></div>
                                                                            {errors.creditLimit && touched.creditLimit ? (
                                                                                <span className="errorMsg pl-3">{errors.creditLimit}</span>
                                                                            ) : null}
                                                                        </div>
                                                                    </FormGroup>

                                                                </div>
                                                            } */}

                                                        </div>

                                                        <div className="my-2 text-center"><button className='btn btn-primary text-center' type="submit">Submit</button></div>

                                                    </Form>
                                                );
                                            }}
                                        </Formik>
                                    </div>

                                </Collapse>

                                {this.state.vcPermission !== '' && <div className="alert alert-danger" role="alert">{this.state.vcPermission}</div>}
                            </div>


                            <div className="p-3" >
                                <label className="switch mt-2">
                                    <input
                                        type="checkbox"
                                        name="invoicecapture"
                                        readOnly
                                        checked={newSettingsKey.indexOf('INVOICE_CAPTURE') > -1 ? true : false}
                                        onClick={() => this.handelSetting('INVOICE_CAPTURE', 'invoice capture')}

                                    />
                                    <span className="slider round"></span>
                                </label>


                                <span className="label float-left mr-3 mt-2 font-weight-bold">Enable Invoice Capture</span>
                                <Collapse in={newSettingsKey.indexOf('INVOICE_CAPTURE') > -1 ? true : false}>

                                    <div>
                                        <Formik
                                            enableReinitialize
                                            initialValues={this.state.iniValueIC}
                                            validationSchema={iniValueIC}
                                            onSubmit={this.handleSubmitIC}
                                        >
                                            {({
                                                values,
                                                errors,
                                                touched,
                                                //handleChange,
                                                //handleBlur,
                                                //onChange,
                                            }) => {
                                                return (
                                                    <Form className="p-4 border shadow-sm">
                                                        {/* {this.state.errorMessge ? <div className="alert alert-danger" role="alert">{this.state.errorMessge}</div> : null} */}

                                                        <div className="row">
                                                            <div className="col-sm-6">
                                                                <FormGroup controlId="formBasicText">
                                                                    <div>Account Receivable Email <span className="required">*</span></div>

                                                                    <Field
                                                                        name="accountReceivableEmail"
                                                                        type="text"
                                                                        className={`input-elem ${values.accountReceivableEmail &&
                                                                            'input-elem-filled'} form-control`}
                                                                        autoComplete="nope"
                                                                    />
                                                                    {errors.accountReceivableEmail && touched.accountReceivableEmail ? (
                                                                        <span className="errorMsg pl-3">{errors.accountReceivableEmail}</span>
                                                                    ) : null}


                                                                </FormGroup>
                                                            </div>
                                                            <div className="col-sm-6">
                                                                <FormGroup controlId="formBasicText">
                                                                    <div>Account Payable Email <span className="required">*</span></div>

                                                                    <Field
                                                                        name="accountPayableEmail"
                                                                        type="text"
                                                                        className={`input-elem ${values.accountPayableEmail &&
                                                                            'input-elem-filled'} form-control`}
                                                                        autoComplete="nope"
                                                                    />
                                                                    {errors.accountPayableEmail && touched.accountPayableEmail ? (
                                                                        <span className="errorMsg pl-3">{errors.accountPayableEmail}</span>
                                                                    ) : null}


                                                                </FormGroup>
                                                            </div>


                                                        </div>



                                                        <div className="my-2 text-center"><button className='btn btn-primary text-center' type="submit">Submit</button></div>

                                                    </Form>
                                                );
                                            }}
                                        </Formik>
                                    </div>

                                </Collapse>
                            </div>


                            <div className="p-3" >
                                <label className="switch mt-2">
                                    <input
                                        type="checkbox"
                                        name="invoicecapture"
                                        readOnly
                                        checked={newSettingsKey.indexOf('INVOICE_LIMIT') > -1 ? true : false}
                                        onClick={() => this.handelSetting('INVOICE_LIMIT', 'invoice limit')}

                                    />
                                    <span className="slider round"></span>
                                </label>


                                <span className="label float-left mr-3 mt-2 font-weight-bold">Enable Invoice Limit</span>
                                <Collapse in={newSettingsKey.indexOf('INVOICE_LIMIT') > -1 ? true : false}>

                                    <div>
                                        <Formik
                                            enableReinitialize
                                            initialValues={this.state.iniValueIL}
                                            validationSchema={iniValueIL}
                                            onSubmit={this.handleSubmitIL}
                                        >
                                            {({
                                                values,
                                                errors,
                                                touched,
                                                //handleChange,
                                                //handleBlur,
                                                //onChange,
                                            }) => {
                                                return (
                                                    <Form className="p-4 border shadow-sm">
                                                        {/* {this.state.errorMessge ? <div className="alert alert-danger" role="alert">{this.state.errorMessge}</div> : null} */}

                                                        <div className="row">
                                                            <div className="col-sm-6">
                                                                <FormGroup controlId="formBasicText">
                                                                    <div>ACH Limit <span className="required">*</span></div>

                                                                    <Field
                                                                        name="achLimit"
                                                                        type="number"
                                                                        className={`input-elem ${values.achLimit &&
                                                                            'input-elem-filled'} form-control`}
                                                                        autoComplete="nope"
                                                                    />
                                                                    {errors.achLimit && touched.achLimit ? (
                                                                        <span className="errorMsg pl-3">{errors.achLimit}</span>
                                                                    ) : null}


                                                                </FormGroup>
                                                            </div>
                                                            <div className="col-sm-6">
                                                                <FormGroup controlId="formBasicText">
                                                                    <div>Invoice Amount <span className="required">*</span></div>

                                                                    <Field
                                                                        name="invoiceAmount"
                                                                        type="number"
                                                                        className={`input-elem ${values.invoiceAmount &&
                                                                            'input-elem-filled'} form-control`}
                                                                        autoComplete="nope"
                                                                    />
                                                                    {errors.invoiceAmount && touched.invoiceAmount ? (
                                                                        <span className="errorMsg pl-3">{errors.invoiceAmount}</span>
                                                                    ) : null}


                                                                </FormGroup>
                                                            </div>


                                                        </div>



                                                        <div className="my-2 text-center"><button className='btn btn-primary text-center' type="submit">Submit</button></div>

                                                    </Form>
                                                );
                                            }}
                                        </Formik>
                                    </div>

                                </Collapse>
                            </div>

                            
                            
                            {/* a/c tool start */}

                            <div className="p-3" >
                                <label className="switch mt-2">
                                    <input
                                        type="checkbox"
                                        name="accountingtools"
                                        readOnly
                                        checked={newSettingsKey.indexOf('ACCOUNTING_TOOL') > -1 ? true : false}
                                        onClick={() => this.handelSetting('ACCOUNTING_TOOL', 'accounting tools')}

                                    />
                                    <span className="slider round"></span>
                                </label>


                                <span className="label float-left mr-3 mt-2 font-weight-bold">Enable Accounting Tools</span>
                                <Collapse in={newSettingsKey.indexOf('ACCOUNTING_TOOL') > -1 ? true : false}>
                                    {/*ACCOUNTING_TOOL RADIO  Start*/}
                                    <div className="p-4 border shadow-sm">
                                        <Formik

                                        >
                                            {() => {
                                                return (
                                                    <Form >
                                                        <div className="row">
                                                            <div className="col-sm-12">
                                                                <FormGroup controlId="qbType">

                                                                    <Field
                                                                        name="qbType"
                                                                        type="radio"
                                                                        value="qbOnline"
                                                                        onChange={this.qbTypeHandler}
                                                                        checked={this.state.targetval == "qbOnline" ? "checked" : ""}
                                                                    /> Quick Books Online
                                                                      &nbsp; &nbsp;
                                                                    <Field
                                                                        name="qbType"
                                                                        type="radio"
                                                                        value="qbDesktop"
                                                                        onChange={this.qbTypeHandler}
                                                                        checked={this.state.targetval == "qbDesktop" ? "checked" : ""}
                                                                    /> Quick Books Desktop
                                                                      &nbsp; &nbsp;
                                                                    <Field
                                                                        name="qbType"
                                                                        type="radio"
                                                                        value="netSuite"
                                                                        onChange={this.qbTypeHandler}
                                                                        checked={this.state.targetval == "netSuite" ? "checked" : ""}
                                                                    /> NetSuite
                                                                </FormGroup>
                                                            </div>
                                                        </div>


                                                    </Form>
                                                );
                                            }}
                                        </Formik>

                                        {this.state.qbOnlineVisible &&
                                            <div>
                                                <Formik
                                                    enableReinitialize
                                                    initialValues={this.state.qbOnlineIC}
                                                    validationSchema={qbOnlineIC}
                                                    onSubmit={this.handleSubmitQBonline}
                                                >
                                                    {({
                                                        values,
                                                        errors,
                                                        touched,
                                                        //handleChange,
                                                        //handleBlur,
                                                        //onChange,
                                                    }) => {
                                                        return (
                                                            <Form className="p-4 border">
                                                                {/* {this.state.errorMessge ? <div className="alert alert-danger" role="alert">{this.state.errorMessge}</div> : null} */}

                                                                <div className="row">
                                                                    <div className="col-sm-6">
                                                                        <FormGroup controlId="formBasicText">
                                                                            <div>Client ID <span className="required">*</span></div>

                                                                            <Field
                                                                                name="clientId"
                                                                                type="text"
                                                                                className={`input-elem ${values.clientId &&
                                                                                    'input-elem-filled'} form-control`}
                                                                                autoComplete="nope"
                                                                            />
                                                                            {errors.clientId && touched.clientId ? (
                                                                                <span className="errorMsg pl-3">{errors.clientId}</span>
                                                                            ) : null}


                                                                        </FormGroup>
                                                                    </div>
                                                                    <div className="col-sm-6">
                                                                        <FormGroup controlId="formBasicText">
                                                                            <div>Client Secret <span className="required">*</span></div>

                                                                            <Field
                                                                                name="clientSecret"
                                                                                type="text"
                                                                                className={`input-elem ${values.clientSecret &&
                                                                                    'input-elem-filled'} form-control`}
                                                                                autoComplete="nope"
                                                                            />
                                                                            {errors.clientSecret && touched.clientSecret ? (
                                                                                <span className="errorMsg pl-3">{errors.clientSecret}</span>
                                                                            ) : null}
                                                                        </FormGroup>
                                                                    </div>


                                                                </div>



                                                                <div className="my-2 text-center"><button className='btn btn-primary text-center' type="submit">Submit</button></div>

                                                            </Form>
                                                        );
                                                    }}
                                                </Formik>
                                            </div>
                                        }

                                        {
                                            this.state.qbDesktopVisible ? (
                                                <div className="p-4 border">

                                                    <div><span>Autofy Token :</span>{this.state.iniValueAccTool.qbDesktop && this.state.iniValueAccTool.qbDesktop.agentToken}</div>
                                                    <div>
                                                        <span>Autofy Ping Status :</span>
                                                        <span>
                                                            {this.state.autofyLoader ? <span>Loading...</span> : (this.state.autofyPing === true ? <span className="text-success">Green</span> : <span className="text-danger">Red</span>)}
                                                            {/* {this.state.iniValueAUTO.autofyPing === true ? <span className="text-success">Green</span>:<span className="text-danger">Red</span>} */}
                                                        </span>
                                                    </div>
                                                    <div className="pb-3 pt-1 border-bottom">
                                                        <button disabled={this.state.logLoader ? true : false} className="btn btn-info btn-sm" onClick={this.handelAutofyLog}>{this.state.logLoader ? <LoadingOval /> : null}Autofy Log Download</button>
                                                    </div>
                                                    <div className="pt-3 text-center"><button className='btn btn-primary text-center' onClick={this.handleQBdesktop}>Submit</button></div>
                                                </div>
                                            ) : null

                                        }

{
                                            this.state.netSuiteVisible ? (
                                                <div className="pt-3 text-center"><button className='btn btn-primary text-center' onClick={this.handleNetSuite}>Submit</button></div>
                                            ) : null

                                        }
                                    </div>

                                    {/* ACCOUNTING_TOOL RADIO END */}



                                </Collapse>
                            </div>

                            {/* a/c tool end */}


                            <div className="p-3" >
                                <div className="clearfix"> <span className={`label float-left mr-3 mt-2 font-weight-bold ${this.state.UnitedMailConf ? 'collapseClose' : 'collapseOpen'}`} onClick={this.handelUnitedMailConf}>UnitedMail Configuration</span></div>
                                <Collapse in={this.state.UnitedMailConf}>
                                    <div className="mt-2 p-4 border shadow-sm">
                                        <div><span>Company: </span>{iniValueUOI.company}</div>
                                        <div><span>Address: </span>{iniValueUOI.address1}</div>
                                        <div><span>City/State/Zip: </span>{iniValueUOI.address2}</div>
                                        <div><span>Fintainium Customer ID: </span>{iniValueUOI.customerId}</div>
                                        <div><span>Customer Logo/Return Address: </span>{iniValueUOI.logoImage} </div>
                                        <div><span>Promotional JPEG Image: </span>{iniValueUOI.promotionalImage}</div>

                                    </div>
                                </Collapse>
                            </div>
                            <div className="clearfix"></div>
                            <div className="p-3" >
                                <div className="clearfix"> <span className={`label float-left mr-3 mt-2 font-weight-bold ${this.state.negotiationPeriod ? 'collapseClose' : 'collapseOpen'}`} onClick={this.handelNegotiationPeriod}>Preferred Negotiation Period</span></div>
                                <Collapse in={this.state.negotiationPeriod}>
                                    <div className="mt-2 p-4 border shadow-sm">
                                        <div>
                                            <Formik
                                                enableReinitialize
                                                initialValues={this.state.iniNegoPeriod}
                                                validationSchema={ValidateNegoPeriod}
                                                onSubmit={this.handleSubmitNegoPeriod}
                                            >
                                                {({
                                                    values,
                                                    errors,
                                                    touched,
                                                    //handleChange,
                                                    //handleBlur,
                                                    //onChange,
                                                }) => {
                                                    return (
                                                        <Form className="">
                                                            <div className="row">
                                                                <div className="col-sm-2">
                                                                    <FormGroup controlId="formBasicText">
                                                                        <div>Number of days <span className="required">*</span></div>

                                                                        <Field
                                                                            name="value"
                                                                            type="number"
                                                                            className={`input-elem ${values.value &&
                                                                                'input-elem-filled'} form-control`}
                                                                            autoComplete="nope"
                                                                        />
                                                                        {errors.value && touched.value ? (
                                                                            <span className="errorMsg pl-3">{errors.value}</span>
                                                                        ) : null}


                                                                    </FormGroup>
                                                                </div>
                                                                <div className="col-sm-8">
                                                                    <div>&nbsp;</div>
                                                                    <div className=""><button className='btn btn-primary text-center' type="submit">Submit</button></div>
                                                                </div>


                                                            </div>



                                                            

                                                        </Form>
                                                    );
                                                }}
                                            </Formik>
                                        </div>




                                       
                                    </div>
                                </Collapse>
                            </div>
                            <div className="clearfix"></div>
                            {/*outer */}
                            <div className="p-3" >
                                <span className="label float-left mr-3 mt-2 font-weight-bold">Payment Bank Option</span>
                            </div>
                            <div className="clearfix"></div>

                            <div className="pmBankOptionSec clearfix mx-3 mt-2 p-4 border shadow-sm">
                                <Formik
                                    enableReinitialize
                                    initialValues={this.state.iniValuePBO}
                                    // initialValues={iniValuePBO}
                                    validationSchema={iniValuePBOS}
                                    onSubmit={this.handleSubmitPBO}
                                >
                                    {({
                                        values,
                                        errors,
                                        touched,
                                        handleChange,
                                        handleBlur,
                                        setFieldValue
                                        //onChange,
                                    }) => {
                                        // { console.log(errors);} 
                                        return (
                                            <Form>
                                                <div className="row">

                                                    <div className="col-sm-6">
                                                        <FormGroup controlId="formBasicText">
                                                            <div className="mb-2">Select Bank <span className="required">*</span></div>
                                                            <Field
                                                                name="centralPaymentBankOption"
                                                                component="select"
                                                                className="form-control truncate w-100"
                                                                autoComplete="nope"
                                                                onChange={e => {
                                                                    handleChange(e);
                                                                    this.pbSubFieldEneble(e, setFieldValue);
                                                                }}
                                                                value={values.centralPaymentBankOption || ''}
                                                                onBlur={handleBlur}
                                                            >
                                                                {cpbusinesslist.length ?
                                                                    cpbusinesslist.map(cpbusiness => (
                                                                        <option
                                                                            value={cpbusiness.bankOption}
                                                                            key={cpbusiness.bankOption}

                                                                        >
                                                                            {cpbusiness.bankOption}
                                                                        </option>
                                                                    )) : null}
                                                            </Field>

                                                            {errors.centralPaymentBankOption && touched.centralPaymentBankOption ? (
                                                                <span className="errorMsg ml-3 mt-1">
                                                                    {errors.centralPaymentBankOption}
                                                                </span>
                                                            ) : null}

                                                        </FormGroup>
                                                    </div>
                                                </div>

                                                <div className="row">
                                                    <div className="col-sm-6">
                                                        <FormGroup controlId="formBasicText">
                                                            <div>Account Number <span className="required">*</span></div>

                                                            <Field
                                                                name="centralPaymentAccountNumber"
                                                                type="number"
                                                                className={`input-elem form-control`}
                                                                autoComplete="nope"
                                                                value={values.centralPaymentAccountNumber || ''}
                                                            />
                                                            {errors.centralPaymentAccountNumber && touched.centralPaymentAccountNumber ? (
                                                                <span className="errorMsg pl-3">{errors.centralPaymentAccountNumber}</span>
                                                            ) : null}


                                                        </FormGroup>
                                                    </div>
                                                    <div className="col-sm-6">
                                                        <FormGroup controlId="formBasicText">
                                                            <div>Routing Number
                                                            </div>
                                                            <Field
                                                                name="centralPaymentRoutingNumber"
                                                                type="text"
                                                                className={`input-elem form-control`}
                                                                autoComplete="nope"
                                                                value={values.centralPaymentRoutingNumber || ''}
                                                                disabled="true"
                                                            />
                                                            {/* {errors.centralPaymentRoutingNumber && touched.centralPaymentRoutingNumber ? (
                                                                <span className="errorMsg pl-3">{errors.centralPaymentRoutingNumber}</span>
                                                            ) : null} */}


                                                        </FormGroup>
                                                    </div>
                                                </div>
                                                {values.centralPaymentBankOption === 'BOM' ?
                                                    (
                                                        <div className="row">

                                                            <div className="col-sm-6">
                                                                <FormGroup controlId="formBasicText">
                                                                    <div>ACH Account Number <span className="required">*</span></div>

                                                                    <Field
                                                                        name="achAccountNumber"
                                                                        type="text"
                                                                        className={`input-elem form-control`}
                                                                        autoComplete="nope"
                                                                        value={values.achAccountNumber || ''}
                                                                    />
                                                                    {errors.achAccountNumber && touched.achAccountNumber ? (
                                                                        <span className="errorMsg pl-3">{errors.achAccountNumber}</span>
                                                                    ) : null}


                                                                </FormGroup>
                                                            </div>
                                                            {/* {values.lockboxId && */}

                                                                <div className="col-sm-6">
                                                                    <FormGroup controlId="formBasicText">
                                                                    <div>Lockbox Id <span className="required">*</span></div>
                                                                        <Field
                                                                            name="lockboxId"
                                                                            type="text"
                                                                            className={`input-elem form-control`}
                                                                            autoComplete="nope"
                                                                            value={values.lockboxId}
                                                                            //disabled="true"
                                                                        />
                                                                    {errors.lockboxId && touched.lockboxId ? (
                                                                        <span className="errorMsg pl-3">{errors.lockboxId}</span>
                                                                    ) : null}

                                                                    </FormGroup>
                                                                </div>
                                                             {/* null} */}
                                                        </div>
                                                    ) : null
                                                }
                                                <div className="my-2 text-center"><button className='btn btn-primary text-center' type="submit">Submit</button></div>

                                            </Form>
                                        );
                                    }}
                                </Formik>
                            </div>



                            <div className="p-3" >
                                <label className="switch mt-2">
                                    <input
                                        type="checkbox"
                                        name="cardconnectoption"
                                        readOnly
                                        checked={newSettingsKey.indexOf('CARD_CONNECT_OPTION') > -1 ? true : false}
                                        onClick={() => this.handelSetting('CARD_CONNECT_OPTION', 'card connect option')}

                                    />
                                    <span className="slider round"></span>
                                </label>


                                <span className="label float-left mr-3 mt-2 font-weight-bold">Enable Card Connect Option</span>
                                <Collapse in={newSettingsKey.indexOf('CARD_CONNECT_OPTION') > -1 ? true : false}>

                                    <div>
                                        <Formik
                                            enableReinitialize
                                            initialValues={this.state.iniCardCO}
                                            validationSchema={iniCardCO}
                                            onSubmit={this.handleSubmitCardCO}
                                        >
                                            {({
                                                values,
                                                errors,
                                                touched,
                                                //handleChange,
                                                //handleBlur,
                                                //onChange,
                                            }) => {
                                                return (
                                                    <Form className="p-4 border shadow-sm">
                                                        <div className="row">
                                                            <div className="col-sm-6">
                                                                <FormGroup controlId="formBasicText">
                                                                    <div>Site Name <span className="required">*</span></div>

                                                                    <Field
                                                                        name="siteName"
                                                                        type="text"
                                                                        className={`input-elem ${values.siteName &&
                                                                            'input-elem-filled'} form-control`}
                                                                        autoComplete="nope"
                                                                    />
                                                                    {errors.siteName && touched.siteName ? (
                                                                        <span className="errorMsg pl-3">{errors.siteName}</span>
                                                                    ) : null}


                                                                </FormGroup>
                                                            </div>
                                                            <div className="col-sm-6">
                                                                <FormGroup controlId="formBasicText">
                                                                    <div>Merchant ID <span className="required">*</span></div>

                                                                    <Field
                                                                        name="merchantId"
                                                                        type="text"
                                                                        className={`input-elem ${values.merchantId &&
                                                                            'input-elem-filled'} form-control`}
                                                                        autoComplete="nope"
                                                                    />
                                                                    {errors.merchantId && touched.merchantId ? (
                                                                        <span className="errorMsg pl-3">{errors.merchantId}</span>
                                                                    ) : null}


                                                                </FormGroup>
                                                            </div>


                                                        </div>



                                                        <div className="my-2 text-center"><button className='btn btn-primary text-center' type="submit">Submit</button></div>

                                                    </Form>
                                                );
                                            }}
                                        </Formik>
                                    </div>

                                </Collapse>
                            </div>


                            <Row>
                                <Col md={12}>
                                    <p style={{ paddingTop: '20px' }} className="p-3"><span className="required">*</span> These fields are required.</p>
                                </Col>
                            </Row>
                            {/* EXP */}
                            <div className="p-3" >
                                <label className="switch mt-2">
                                    <input
                                        type="checkbox"
                                        name="cardconnectoption"
                                        readOnly
                                        checked={newSettingsKey.includes('CUSTOMER_VC_ENABLED')}
                                        onClick={() => this.handelSetting('CUSTOMER_VC_ENABLED', 'customer virtual credit card enabled')}

                                    />
                                    <span className="slider round"></span>
                                </label>
                                <span className="label float-left mr-3 mt-2 font-weight-bold">Enable Card On File</span>
                                <Collapse in={newSettingsKey.includes('CUSTOMER_VC_ENABLED')}>
                                    <div>
                                        <Formik
                                            enableReinitialize={true}
                                            initialValues={iniValueCustVC}
                                            validationSchema={validateCustVC}
                                            onSubmit={this.handleSubmitCustVC}>
                                            {
                                                ({ errors, touched }) => {
                                                    return (
                                                        <Form className="p-4 border shadow-sm">
                                                            <div className="row">
                                                                <div className="col-sm-6">
                                                                    <FormGroup controlId="formBasicText">
                                                                        <div className="mb-2">Credit Limit</div>
                                                                        <Field
                                                                            name="cofCreditLimit"
                                                                            type="input"
                                                                            className="form-control truncate w-100" />
                                                                        {errors.cofCreditLimit && touched.cofCreditLimit ? (
                                                                            <span className="errorMsg pl-3">{errors.cofCreditLimit}</span>
                                                                        ) : null}
                                                                    </FormGroup>
                                                                </div>
                                                                <div className="col-sm-6">
                                                                    <FormGroup controlId="formBasicText">
                                                                        <div className="mb-2">Account Name</div>
                                                                        <Field
                                                                            name="cofAccountName"
                                                                            type="input"
                                                                            className="form-control truncate w-100" />
                                                                        {errors.cofAccountName && touched.cofAccountName ? (
                                                                            <span className="errorMsg pl-3">{errors.cofAccountName}</span>
                                                                        ) : null}
                                                                    </FormGroup>
                                                                </div>

                                                                <div className=" col-sm-6">
                                                                    <FormGroup controlId="formBasicText">
                                                                        <div className="mb-2">Account Number</div>
                                                                        <Field
                                                                            name="cofAccountNumber"
                                                                            type="input"
                                                                            className="form-control truncate w-100" />
                                                                        {errors.cofAccountNumber && touched.cofAccountNumber ? (
                                                                            <span className="errorMsg pl-3">{errors.cofAccountNumber}</span>
                                                                        ) : null}
                                                                    </FormGroup>
                                                                </div>
                                                                <div className=" col-sm-6">
                                                                    <FormGroup controlId="formBasicText">
                                                                        <div className="mb-2">Company Id</div>
                                                                        <Field
                                                                            name="cofCompanyId"
                                                                            type="input"
                                                                            className="form-control truncate w-100" />
                                                                        {errors.cofCompanyId && touched.cofCompanyId ? (
                                                                            <span className="errorMsg pl-3">{errors.cofCompanyId}</span>
                                                                        ) : null}
                                                                    </FormGroup>
                                                                </div>
                                                                <div className=" col-sm-6">
                                                                    <FormGroup controlId="formBasicText">
                                                                        <div className="mb-2">Company Name</div>
                                                                        <Field
                                                                            name="cofCompanyName"
                                                                            type="input"
                                                                            className="form-control truncate w-100" />
                                                                        {errors.cofCompanyName && touched.cofCompanyName ? (
                                                                            <span className="errorMsg pl-3">{errors.cofCompanyName}</span>
                                                                        ) : null}
                                                                    </FormGroup>
                                                                </div>
                                                                <div className="col-sm-6">
                                                                    <FormGroup controlId="formBasicText">
                                                                        <div className="mb-2">Out Source Id</div>
                                                                        <Field
                                                                            name="cofOutsourceId"
                                                                            type="input"
                                                                            className="form-control truncate w-100" />
                                                                        {errors.cofOutsourceId && touched.cofOutsourceId ? (
                                                                            <span className="errorMsg pl-3">{errors.cofOutsourceId}</span>
                                                                        ) : null}
                                                                    </FormGroup>
                                                                </div>
                                                            </div>
                                                            <div className="my-2 text-center">
                                                                <button className="btn btn-primary text-center" type="submit">
                                                                    Submit
                                                                    </button>
                                                            </div>
                                                        </Form>
                                                    );
                                                }
                                            }
                                        </Formik>
                                    </div>
                                </Collapse>
                            </div>



                            <div className="p-3" >
                                <label className="switch mt-2">
                                    <input
                                        type="checkbox"
                                        name="cardconnectoption"
                                        readOnly
                                        checked={newSettingsKey.indexOf('ENABLE_DASHBOARD') > -1 ? true : false}
                                        onClick={() => this.handelSetting('ENABLE_DASHBOARD', 'enable dashboard option')}

                                    />
                                    <span className="slider round"></span>
                                </label>


                                <span className="label float-left mr-3 mt-2 font-weight-bold">Enable Dashboard</span>
                                <Collapse in={newSettingsKey.indexOf('ENABLE_DASHBOARD') > -1 ? true : false}>

                                    <div>
                                        <Formik
                                            enableReinitialize
                                            initialValues={this.state.iniDashboard}
                                            //validationSchema={iniCardCO}
                                            onSubmit={this.handleSubmitDashboardType}
                                            >
                                            {({ values, handleChange, setFieldValue }) => {
                                                return (
                                                    <Form >
                                                        <div className="row">
                                                            <div className="col-sm-12">
                                                                <FormGroup controlId="dashtype">
                                                                    <Field
                                                                        name="isDoximEnable"
                                                                        type="checkbox"
                                                                        onChange={(e) => {
                                                                            handleChange(e);
                                                                            if(e.target.checked) {
                                                                                setFieldValue("isDoximEnable", "true");
                                                                            } else {
                                                                                setFieldValue("isDoximEnable", "false");
                                                                            }
                                                                        }}
                                                                        checked={values.isDoximEnable === "true" ? true: false}
                                                                    /> Deluxe
                                                                      &nbsp; &nbsp;
                                                                    <Field
                                                                        name="isCitEnable"
                                                                        type="checkbox"
                                                                        onChange={(e) => {
                                                                            handleChange(e);
                                                                            if(e.target.checked) {
                                                                                setFieldValue("isCitEnable", "true");
                                                                            } else {
                                                                                setFieldValue("isCitEnable", "false");
                                                                            }
                                                                        }}
                                                                        checked={values.isCitEnable === "true" ? true : false}
                                                                    /> CIT
                                                                </FormGroup>
                                                            </div>
                                                        </div>

                                                        <div className="my-2 text-center"><button className='btn btn-primary text-center' type="submit">Submit</button></div>
                                                    </Form>
                                                );
                                            }}
                                        </Formik>
                                    </div>

                                </Collapse>
                            </div>


                            <div className="p-3" >
                                <label className="switch mt-2">
                                    <input
                                        type="checkbox"
                                        name="cardconnectoption"
                                        readOnly
                                        checked={newSettingsKey.indexOf('ACCOUNT_RECEIVABLE') > -1 ? true : false}
                                        onClick={() => this.handelSetting('ACCOUNT_RECEIVABLE', 'account receivable option')}

                                    />
                                    <span className="slider round"></span>
                                </label>


                                <span className="label float-left mr-3 mt-2 font-weight-bold">Enable Account Receivable</span>
                                <Collapse in={newSettingsKey.indexOf('ACCOUNT_RECEIVABLE') > -1 ? true : false}>

                                    <div>
                                        <div className='row'>
                                            <div className='col-sm-12 text-center'>
                                                <button 
                                                    className='btn btn-primary' 
                                                    onClick={() => this.handleSubmitAR()}
                                                    disabled={accountReceivable == 'true'? true:false}
                                                >
                                                    Submit
                                                </button>
                                            </div>

                                        </div>
                                    </div>

                                </Collapse>
                            </div>

                            <div className="p-3" >
                                <label className="switch mt-2">
                                    <input
                                        type="checkbox"
                                        name="cardconnectoption"
                                        readOnly
                                        checked={newSettingsKey.indexOf('ACCOUNT_PAYAEBLE') > -1 ? true : false}
                                        onClick={() => this.handelSetting('ACCOUNT_PAYAEBLE', 'account payable option')}

                                    />
                                    <span className="slider round"></span>
                                </label>


                                <span className="label float-left mr-3 mt-2 font-weight-bold">Enable Account Payable</span>
                                <Collapse in={newSettingsKey.indexOf('ACCOUNT_PAYAEBLE') > -1 ? true : false}>

                                    <div>
                                        <div className='row'>
                                            <div className='col-sm-12 text-center'>
                                                <button 
                                                    className='btn btn-primary' 
                                                    onClick={() => this.handleSubmitAP()}
                                                    disabled={accountpayable == 'true' ? true : false}
                                                >
                                                    Submit
                                                </button>
                                            </div>

                                        </div>
                                    </div>

                                </Collapse>
                            </div>



                        </div>

                }




                {/*======  before action confirmation massage popup  =====*/}
                <Modal
                    show={this.state.alertMsg}
                    onHide={this.closeAlertMsg}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <div className="m-auto text-center">
                            <h6 className="mb-3">
                                {this.state.updateLoader ? <LoadingSpinner /> : null}
                                <span>Do you want to change <span className="text-lowercase">{this.state.settingText}</span> status?</span>
                            </h6>
                        </div>
                        <div className="m-auto text-center">
                            <button className="btn btn-secondary mr-2 btn-darkBlue" onClick={() => this.closeAlertMsg()}>Return</button>
                            <button className="btn btn-primary" onClick={() => this.updateSettings(this.state.bodyObj)}>Confirm</button>
                        </div>

                    </Modal.Body>
                </Modal>

                {/*======  confirmation popup  ===== */}

                <Modal
                    show={this.state.showConfirMmsg}
                    onHide={this.handleConfirmReviewClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                <Image src={SuccessIco} />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center my-3">
                                {/* <h6><span className="text-capitalize">{this.state.settingText}</span> setting have been successfully updated</h6> */}
                                <h6><span className="text-capitalize">{this.state.settingText}</span> settings have been successfully updated</h6>
                            </Col>
                        </Row>
                        <div className="text-center">
                            <Button
                                onClick={this.handleConfirmReviewClose}
                                className="but-gray"
                            >
                                Done
                            </Button>
                        </div>

                    </Modal.Body>
                </Modal>


                {/*======  Autofy download status confirmation popup  ===== */}

                <Modal
                    show={this.state.autofylogMmsg}
                    onHide={this.handleLogReviewClose}
                    className="payOptionPop"
                >
                    <Modal.Body>
                        <Row>
                            <Col md={12} className="text-center">
                                {this.state.logStatusText ? <Image src={SuccessIco} /> : null}
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="text-center my-3">
                                {this.state.logStatusText ? <h5>{this.state.logStatusText}</h5> : this.state.logError ? <div className="alert alert-danger" role="alert">{this.state.logError}</div> : null}
                            </Col>
                        </Row>
                        <div className="text-center">
                            <Button
                                onClick={this.handleLogReviewClose}
                                className="but-gray"
                            >
                                {this.state.logStatusText ? 'Done' : 'Return'}
                            </Button>
                        </div>

                    </Modal.Body>
                </Modal>



            </div>
        );
    }

}
const mapStateToProps = state => {
    return {
        globalState: state
    };
};

BusinessSettings.propTypes = {
    globalState: PropTypes.object,
};
export default connect(mapStateToProps)(BusinessSettings);
