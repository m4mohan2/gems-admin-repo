import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
	Container,
	Image,
	Row,
	Col,
	Modal,
	Button
} from 'react-bootstrap';
import 'font-awesome/css/font-awesome.min.css';
import { Link } from 'react-router-dom';
import logoImg from './../../../assets/logo-white.png';
import logoImgSm from './../../../assets/logo-small.png';
import SuccessIco from '../../../assets/success-ico.png';

import menuClosed from './../../../assets/menu-closed-icon.png';
import DashboardRoute from '../../../routes/DashboardRoute';

import './Dashboard.scss';
import './slider.scss';
import LeftNavigation from '../LeftNav';
import { connect } from 'react-redux';
//import axios from './../../../shared/eaxios';
import {
	//FaAngleRight,
	FaChevronUp,
	FaChevronDown,
	//FaAngleLeft,
	//FaChevronRight

} from 'react-icons/fa';
import { MdKeyboardArrowLeft } from 'react-icons/md';
import { FaArrowRight } from 'react-icons/fa';
let HEADING = {
	//'/dashboard/dashboard-home': 'Welcome',
	'/dashboard/home': 'Welcome',
	'/dashboard/users': 'Gemster',
	'/dashboard/gems': 'Gems',
	'/dashboard/cms': 'CMS',
	'/dashboard/news': 'News',
	'/dashboard/homepage': 'Homepage',
	'/dashboard/faq': 'Faq',
	'/dashboard/testimonial': 'Testimonial',
	'/dashboard/services': 'Services',
	'/dashboard/license': 'Membership License',
	'/dashboard/gems-category': 'Gems Category',
	'/dashboard/category': 'Category',
	'/dashboard/subcategory': 'Subcategory',
	'/dashboard/providers': 'Providers',
	'/dashboard/audittrails': 'AuditTrails',
	'/dashboard/vendor': 'Vendors',
	'/dashboard/business': 'Business',
	'/dashboard/business/view': 'Business View',
	'/dashboard/transaction': 'Transactions',
	'/dashboard/transaction/ar': 'Transactions: Account Receivable',
	'/dashboard/transaction/ap': 'Transactions: Account Payable',
	'/dashboard/paymentfiles': 'Payment Files',
	'/dashboard/archive': 'Archive',
	'/dashboard/bank': 'Bank',
	'/dashboard/bankholidays': 'Bank Holidays',
	'/dashboard/invoice': 'Invoice',
	'/dashboard/invoice-capture': 'Invoice Capture',
	'/dashboard/invoice-capture/all': 'Invoice Capture',
	'/dashboard/invoice-capture/draft': 'Invoice Capture',
	'/dashboard/invoice-capture/created': 'Invoice Capture',
	'/dashboard/invoice-capture/sent': 'Invoice Capture',
	'/dashboard/invoice-capture/paid': 'Invoice Capture',
	'/dashboard/samplechart': 'Chart',
	'/dashboard/coupon': 'Coupon',
	'/dashboard/role': 'Role Permissions',
	'/dashboard/claimlist': 'Claim List'
};

import Profile from './DashboardComponents/Profile/Profile';
import { logout } from './../../../redux/actions/auth';
import { handelUser } from './../../../redux/actions/username';

class Dashboard extends Component {
	state = {
		sidebarOpen: true,
		showNotification: false,
		isMouseInside: false,
		actionModal: false,
		disabled: false,
		profileEditConfirmMessage: false,
	};

	editProfileHandler = () => {
		this.setState({
			disabled: true
		});
	}

	cancelProfileHandler = () => {
		this.setState({
			disabled: false
		});
	}

	openActionModal = () => {
		this.setState({
			actionModal: true
		});
	}
	closeActionModal = () => {
		this.setState({
			actionModal: false
		});
	}



	mouseEnter = () => {
		this.setState({ isMouseInside: true });
	}
	mouseLeave = () => {
		this.setState({ isMouseInside: false });
	}

	sidebarCollapse = () => {
		const sidebarOpen = this.state.sidebarOpen;
		this.setState({ sidebarOpen: !sidebarOpen });
		if ('localStorage' in window) {
			localStorage.setItem('closedrawer', !sidebarOpen);
		}
	}

	setWrapperRef = node => {
		this.wrapperRef = node;

	}

	componentDidUpdate() {
		if (this.props.businessData) {
			console.log('dashboard update---------', this.props.businessData.name);

		}

	}

	componentDidMount() {
		console.log('dashboard mount', this.props);
		if ('localStorage' in window) {
			const isCloseDrawer = localStorage.getItem('closedrawer');
			if (isCloseDrawer !== null) {
				this.setState({ sidebarOpen: (isCloseDrawer === 'true') });
			}
		}
		document.addEventListener('mousedown', this.handleClickOutside);

	}

	componentWillUnmount() {
		document.removeEventListener('mousedown', this.handleClickOutside);
	}

	handleClickOutside = (event) => {
		if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
			this.state.showNotification && this.setState({ showNotification: false });
		} else {
			!this.state.showNotification && this.setState({ showNotification: true });
		}
	}

	logOutHandler = () => {
		this.props.handleLogout();
	}

	handelProfileModalClose = () => {
		this.setState({ actionModal: false });
	}

	handleProfileConfirMmsg = () => {
		this.setState({ actionModal: false, profileEditConfirmMessage: true });
	}

	handleProfileConfirMmsgClose = () => {
		this.setState({ profileEditConfirmMessage: false });
	}


	render() {
		const { sidebarOpen } = this.state;
		const { location } = this.props.history;
		const heading = HEADING[location.pathname] || 'Dashboard';

		// const profileProps = {
		// 	disabled: this.state.disabled
		// };

		// let heading;
		// if (this.props.businessData !== null || this.props.businessData !== '') {
		//     if (location.pathname === '/dashboard/business/view') {
		//         heading = this.props.businessData.name;
		//     }
		// } else if (this.props.businessData==null){
		//     heading = HEADING[location.pathname] || 'Dashboard';
		// }
		// else {
		//     heading = HEADING[location.pathname] || 'Dashboard';
		// }

		return (
			<div>
				<Container fluid>
					<Row className="show-grid">
						<div className="dashboardCont w-100">
							<div className="wrapper">
								<nav id="sidebar" className={sidebarOpen ? 'expandSidebar' : 'active'}>
									<div className="logoleft">
										<Link to="/dashboard/home">
											<div className="sidebar-header">
												<div className="logoPan">
													<Image src={logoImg} className="fullLogo" />
													<Image src={logoImgSm} className="smallLogo" />
												</div>
											</div>
										</Link>

										<div className="shrinkExpand">
											<button type="button" onClick={this.sidebarCollapse} className="btn navbar-btn customArrowBut">
												<span><MdKeyboardArrowLeft /> Collapse Menu</span>
												<Image src={menuClosed} className="expandIcon" />
												{/* <i className="glyphicon glyphicon-align-justify"></i>  */}
												{/*<span>Toggle Sidebar</span>*/}
											</button>
										</div>
										<LeftNavigation />
										<h5>{(new Date().getFullYear())} Gems. All Rights Reserved</h5>
									</div>
								</nav>
								<div id="content" className={sidebarOpen ? 'shrinkCon' : 'expandCon'}>
									<nav className="navbar navbar-default">
										<div className="container-fluid p-0">
											<div className="navbar-header">

												<div className="shrinkExpandMobile">
													<button type="button" onClick={this.sidebarCollapse} className="btn">
														<Image src={menuClosed} className="expandIcon" />
													</button>
												</div>
												<h3 className="titleTop">
													<span className='float-left'>{heading} {this.props.businessData && <FaArrowRight className='arrow-14' />} </span>

													<span className='ml-3 float-left bnPosion'>

														<h6><span></span>{this.props.businessData && this.props.businessData.name}</h6>
													</span>
												</h3>

											</div>
											<div className="pull-right">
												<div className="adminUser">

													<div className="userSec" onMouseEnter={this.mouseEnter} onMouseLeave={this.mouseLeave}>
														<Link to="#" className="userIcon">
															{/* <h5 className="text-capitalize">{sessionStorage.getItem('firstName')} {sessionStorage.getItem('lastName')}</h5> */}
															{this.props.firstName && <h5 className="text-capitalize">{this.props.firstName} {this.props.lastName}</h5>}
															{this.state.isMouseInside ?
																<span className="ml-2">
																	<FaChevronUp />
																</span>

																// <i className="glyphicon glyphicon-menu-up"></i> 
																:
																<span className="ml-2">
																	<FaChevronDown />
																</span>

																// <i className="glyphicon glyphicon-menu-down"></i>
															}
														</Link>

														{this.state.isMouseInside ?
															<div className="userSecDropdown" onMouseEnter={this.mouseEnter}>
																<ul>
																	<li className="profileIco">
																		<Link to='#' onClick={this.openActionModal}>Profile</Link>
																	</li>
																	<li className="logoutIco">
																		<Link to='#' onClick={this.logOutHandler}>Logout</Link>
																	</li>
																</ul>
															</div>
															: null
														}

													</div>

												</div>
											</div>

										</div>
									</nav>

									<DashboardRoute />

								</div>
							</div>
						</div>
					</Row>
				</Container>

				{/*====== Profile  ====== */}
				<Modal
					show={this.state.actionModal}
					onHide={this.closeActionModal}
					className="right half noPadding slideModal"
				>
					<Modal.Header closeButton></Modal.Header>
					<Modal.Body>
						<div className="modalHeader">
							<div className="row">
								<Col md={4}>
									<h1>My Profile</h1>
								</Col>
								{/* <Col md={8} className="text-right">
									<Button className="blue-btn" onClick={this.editProfileHandler}>
										Edit
                                        </Button>
								</Col> */}
							</div>
						</div>
						<div className="modalBody content-body noTabs">
							<Profile
								// {...profileProps}
								// onCancle={this.cancelProfileHandler}
								handelProfileClose={this.handelProfileModalClose}
								handleProfileConfirMmsg={this.handleProfileConfirMmsg}
							/>
						</div>
					</Modal.Body>
				</Modal>

				{/*====== Profile confirm popup ====== */}
				<Modal
					show={this.state.profileEditConfirmMessage}
					onHide={this.handleProfileConfirMmsgClose}
					className="payOptionPop"
				>
					<Modal.Body>
						<Row>
							<Col md={12} className="text-center">
								<Image src={SuccessIco} />
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<h5>Record has been successfully edited</h5>
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<Button
									onClick={this.handleProfileConfirMmsgClose}
									className="but-gray"
								>
									Return
                                </Button>
							</Col>
						</Row>
					</Modal.Body>
				</Modal>

			</div>
		);
	}
}

Dashboard.propTypes = {
	location: PropTypes.object,
	history: PropTypes.object,
	authToken: PropTypes.string,
	businessData: PropTypes.object,
	firstName: PropTypes.string,
	lastName: PropTypes.string,
	handleLogout: PropTypes.func,
	handelUser: PropTypes.func
};

const mapStateToPros = state => {
	return {
		token: state.auth.token,
		auth: state.auth,
		authToken: state.auth.authToken,
		businessData: state.business.businessData,
		firstName: state.user.firstName,
		lastName: state.user.lastName
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		handleLogout: () => {
			dispatch(logout());
		},
		handelUser: (username) => dispatch(handelUser(username))
	};
};



export default connect(mapStateToPros, mapDispatchToProps)(Dashboard);
