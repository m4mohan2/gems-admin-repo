import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Button, Col, Container, Row } from 'react-bootstrap';
import FooterWelcome from '../FooterWelcome';
import HeaderLogoWelcome from '../LogoWelcomeHeader';
import { Formik, Field, Form } from 'formik';
import { connect } from 'react-redux';
import { accessToken, authDataUpdate, logoutMsg } from './../../../redux/actions/auth';
//import rightCurve from '../../../assets/fintainium-wel-right.png';
//import leftCurve from '../../../assets/fintainium-wel-left.png';


import { handelUser } from './../../../redux/actions/username';
import * as Yup from 'yup';

import axios from './../../../shared/eaxios';
import LoadingSpinner2 from './../../../Components/LoadingSpinner/LoadingSpinner2';

import '../PreAuth.scss';
import * as AppConst from '../../../common/constants';

const initialValues = {
	email: '',
	password: '',
	mfaCode: null
};

const LoginSchema = Yup.object().shape({
	email:
		Yup.string()
			.trim('Please remove whitespace')
			.email('Email must be valid')
			.strict()
			.required('Please enter email address'),
	password:
		Yup
			.string()
			.trim('Please remove whitespace')
			.strict()
			.required('Please enter password'),
	// mfaCode:
	//     Yup
	//         .number()
	//         .strict()
	//         .required('Please enter MFA code'),        

});





class Login extends Component {

	state = {
		errorMsg: '',
		loader: false
	}

	handleSubmit = (values) => {

		const val = {
			'email': values.email,
			'password': values.password,
			'user_type': 0
		};


		this.setState({ loader: true }, () => {
			axios.post(AppConst.APIURL + '/api/login', val) //dev
				.then(res => {
					let timeStamp = new Date().getTime();
					let user = {};
					user.firstName = res.data.user.user_detail.first_name;
					user.lastName = res.data.user.user_detail.last_name;
					user.timeStamp = timeStamp;
					user.id = res.data.user.id;

					this.props.handelUser(user);
					this.props.sessionTokenUpdate(res.data.token, res.data.user.id, res.data.user.role.facility);
					this.props.history.push('/dashboard/home');
				})
				.catch(err => {
					let errorMessage = '';
					try {
						errorMessage = err.data.message ? err.data.message : err.data.error_description;
					} catch (err) {
						errorMessage = 'No records found.';
					}
					this.setState({
						loader: false,
						errorMsg: errorMessage
					});
					setTimeout(() => {
						this.setState({ errorMsg: null });
					}, 2500);
				});
		});
	}

	componentDidMount() {
		setTimeout(() => {
			this.props.resetLogoutMsg(null);
		}, 5000);

	}

	render() {
		const { token } = this.props;
		console.log('"Login Token" ', token);
		if (token) return (<Redirect to="/dashboard" />);
		return (
			<div className="position-relative">
				{this.state.loader ?
					<span className='loaderStyle'><LoadingSpinner2 /></span>
					: null
				}
				<div className="welcomeOuter">
					<HeaderLogoWelcome />
					<div className="regWhiteOuter">
						<Container>
							<div className="welcomePage">
								<div className="row">
									<div className="col-sm-12">
										{
											this.props.authlogoutMsg && this.props.authlogoutMsg !== null ?
												<div className="alert alert-warning alert-dismissible fade show" role="alert">
													{this.props.authlogoutMsg}
												</div>

												:
												null
										}
									</div>
								</div>
								<Row className="show-grid">
									<Col md={12}>
										<div className="welcomeIntro">
											<div className="login position-relative">
												<h2>Admin</h2>
												{this.state.errorMsg ?
													<p className="alert alert-danger text-center" role="alert"> {this.state.errorMsg} </p>
													: null
												}
												<Formik initialValues={initialValues}
													validationSchema={LoginSchema}
													onSubmit={this.handleSubmit}>
													{({ errors, touched }) => {
														return (<Form>
															<div className="position-relative">
																<div className="form-group">
																	<label className="form-label">Email Address</label>
																	<Field
																		type="email"
																		name="email"
																		placeholder="Enter email"
																		className="form-control"
																	/>
																	{errors.email && touched.email ? <span className="errorMsg">{errors.email}</span> : null}
																</div>
																<div className="form-group">
																	<label className="form-label">Password</label>
																	<Field
																		type="password"
																		name="password"
																		placeholder="Password"
																		className="form-control"
																	/>
																	{errors.password && touched.password ? <span className="errorMsg">{errors.password}</span> : null}
																</div>

																{/* <div className="form-group numArrowHide">
                                                                    <h6>Get MFA code from the <strong>Google Authenticator </strong>app</h6>
                                                                    <label className="form-label">Please enter MFA code</label>
                                                                    <Field
                                                                        type="number"
                                                                        name="mfaCode"
                                                                        placeholder="MFA code"
                                                                        className="form-control"
                                                                    />
                                                                    {errors.mfaCode && touched.mfaCode ? <span className="errorMsg">{errors.mfaCode}</span> : null}
                                                                </div> */}

																<Button variant="primary" type="submit" className="btnStylePrimary mt-3">
																	<span className="font-weight-normal">Login</span>
																</Button>
															</div>

														</Form>);
													}}
												</Formik>

											</div>{/* end: login */}
										</div>
									</Col>
								</Row>
							</div>{/* end: welcomePage*/}
						</Container>
					</div>{/* end: regWhiteOuter*/}

					<FooterWelcome />

				</div>

			</div>
		);
	}
}


Login.propTypes = {
	location: PropTypes.object,
	history: PropTypes.object,
	sessionTokenUpdate: PropTypes.func,
	handelUser: PropTypes.func,
	token: PropTypes.any,
	authlogoutMsg: PropTypes.string,
	resetLogoutMsg: PropTypes.func,
};

const mapStateToProps = (state) => {
	return {
		token: state.auth.token,
		authlogoutMsg: state.auth.logoutMsg,

	};
};

const mapDispatchToProps = dispatch => {
	return {
		sessionTokenUpdate: (key, token, facility) => dispatch(accessToken(key, token, facility)),
		updateAuthState: (authItem) => dispatch(authDataUpdate(authItem)),
		handelUser: (user) => dispatch(handelUser(user)),
		resetLogoutMsg: data => dispatch(logoutMsg(data)),
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
