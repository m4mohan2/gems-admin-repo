import React, { Component } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import Logo from '../../Components/Logo/Logo';

class LogoWelcomeHeader extends Component {
    render() {
        return (
            <div className="regToppanel">
                <Container>
                    <Row className="show-grid">
                        <Col md={12}>
                            <div className="logoHeadFull" id="logoHeadFull">
                                <Logo />
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default LogoWelcomeHeader;
