import React from 'react';
import * as serviceWorker from './serviceWorker';

import { Provider } from 'react-redux';

import ReactDOM from 'react-dom';
import { HashRouter } from 'react-router-dom';
import { store } from './redux/store';

import 'bootstrap/scss/bootstrap.scss';
import './index.scss';

import App from './App';

import axiosInstance, { setupAxiosClient } from './shared/eaxios';
setupAxiosClient(store, axiosInstance);


const app = (
    <Provider store={store}>
        <HashRouter>
            <App />
        </HashRouter>
    </Provider>

);

ReactDOM.render(app, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
