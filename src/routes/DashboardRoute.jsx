import React, { Suspense, lazy, Fragment } from 'react';
import Loader from './../Components/Loader/Loader';
import { Route, Switch } from 'react-router-dom';

const Vendor = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/Vendor/Vendor'));
const Business = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/Business/Business'));
const ViewBusiness = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/Business/ViewBusiness/ViewBusiness'));
//const Transaction = lazy(() => import(/* webpackChunkName: "transaction" */'./../Containers/PostAuth/Dashboard/DashboardComponents/Transaction/Transaction'));
const TrasactionContainer = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/Transaction/TrasactionContainer'));
const PaymentFiles = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/PaymentFiles/PaymentFiles'));
const Archive = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/Archive/Archive'));
const InvoiceCapture = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/InvoiceCapture/InvoiceCapture'));
const Bank = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/Bank/Bank'));
const Invoice = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/Invoice/Invoice'));
const BankHolidays = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/BankHolidays/BankHolidays'));


const Home = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/Home/Home'));
const Gems = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/Gems/Gems'));
const Users = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/Users/User'));
const Cms = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/Cms/Cms'));
const Services = lazy(() => import('../Containers/PostAuth/Dashboard/DashboardComponents/Services/Services'));
const License = lazy(() => import('../Containers/PostAuth/Dashboard/DashboardComponents/MembershipLicense/License'));
const GemsCategory = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/GemsCategory/GemsCategory'));
const Providers = lazy(() => import('../Containers/PostAuth/Dashboard/DashboardComponents/Providers/Providers'));
const AuditTrails = lazy(() => import('../Containers/PostAuth/Dashboard/DashboardComponents/AuditTrails/AuditTrails'));

const News = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/News/News'));
const Testimonial = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/Testimonial/Testimonial'));
const Faq = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/Faq/Faq'));
const Homepage = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/Homepage/Homepage'));
const Category = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/Category/Category'));
const Subcategory = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/Subcategory/Subcategory'));
const SampleChart = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/SampleChart/SampleChart'));
const Coupon = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/Coupon/Coupon'));
const Role = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/Role/Role'));
const ClaimList = lazy(() => import('./../Containers/PostAuth/Dashboard/DashboardComponents/ClaimList/ClaimList'));

const DashboardRoute = () => {
    return (
        <Suspense fallback={<Fragment><Loader /></Fragment>}>
            <Switch>
                <Route path="/dashboard/vendor" render={props => <Vendor {...props} />}></Route>
                <Route path="/dashboard/business/view" render={props => <ViewBusiness {...props} />}></Route>
                {/* <Route path="/dashboard/transaction" render={props => <Transaction {...props} />}></Route> */}
                <Route path="/dashboard/transaction" render={props => <TrasactionContainer {...props} />}></Route>
                <Route path="/dashboard/paymentfiles" render={props => <PaymentFiles {...props} />}></Route>
                <Route path="/dashboard/archive" render={props => <Archive {...props} />}></Route>
                <Route path="/dashboard/invoice-capture" render={props => <InvoiceCapture {...props} />}></Route>
                <Route path="/dashboard/bank" render={props => <Bank {...props} />}></Route>
                <Route path="/dashboard/invoice" render={props => <Invoice {...props} />}></Route>
                <Route path="/dashboard/bankholidays" render={props => <BankHolidays {...props} />}></Route>
                <Route exact path="/dashboard/business" render={props => <Business {...props} />}></Route>
                <Route path="/dashboard/home" render={props => <Home {...props} />}></Route>
                <Route path="/dashboard/gems" render={props => <Gems {...props} />}></Route>
                <Route path="/dashboard/users" render={props => <Users {...props} />}></Route>
                <Route path="/dashboard/cms" render={props => <Cms {...props} />}></Route>
                <Route path="/dashboard/services" render={props => <Services {...props} />}></Route>
                <Route path="/dashboard/license" render={props => <License {...props} />}></Route>
                <Route path="/dashboard/gems-category" render={props => <GemsCategory {...props} />}></Route>
                <Route path="/dashboard/subcategory" render={props => <Subcategory {...props} />}></Route>
                <Route path="/dashboard/category" render={props => <Category {...props} />}></Route>
                <Route path="/dashboard/news" render={props => <News {...props} />}></Route>
                <Route path="/dashboard/testimonial" render={props => <Testimonial {...props} />}></Route>
                <Route path="/dashboard/faq" render={props => <Faq {...props} />}></Route>
                <Route path="/dashboard/homepage" render={props => <Homepage {...props} />}></Route>
                <Route path="/dashboard/providers" render={props => <Providers {...props} />}></Route>
                <Route path="/dashboard/audittrails" render={props => <AuditTrails {...props} />}></Route>
                <Route path="/dashboard/samplechart" render={props => <SampleChart {...props} />}></Route>
                <Route path="/dashboard/coupon" render={props => <Coupon {...props} />}></Route>
                <Route path="/dashboard/role" render={props => <Role {...props} />}></Route>
                <Route path="/dashboard/claimlist" render={props => <ClaimList {...props} />}></Route>
            </Switch>
        </Suspense>
    );
};

export default DashboardRoute;

