import React, { Component, Fragment } from 'react';
import { Redirect, Route, Switch, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import Login from './Containers/PreAuth/Login/Login';
import Dashboard from './Containers/PostAuth/Dashboard/Dashboard';
import { accessToken, authDataUpdate } from './redux/actions/auth';
import { connect } from 'react-redux';
import { PrivateRoute } from './shared/PrivateRoute';
import { handelUser } from './redux/actions/username';
import './App.scss';
import AppLoader from './Components/AppLoader/AppLoader';

const BODY_CLASSES = {
    'login': 'loginPage'
};

class App extends Component {
    constructor(props) {
        super(props);
        this.beforeMount();
        console.log('[constructor finish]');
        this.state = {
            reload: true
        };
    }

    mountAuthData = () => {
        return new Promise((resolve, reject) => {
            try {
                const authItem = sessionStorage.getItem('auth-item');
                if (authItem) {
                    this.props.updateAuthState(JSON.parse(authItem));
                    sessionStorage.removeItem('auth-item');
                }
                const user = sessionStorage.getItem('user');
                if (user) {
                    const userData = JSON.parse(user);
                    const newObj = Object.assign({}, userData, { timeStamp: (new Date()).getTime() });
                    this.props.updateUser(newObj);
                    sessionStorage.removeItem('user');
                } else {
                    this.props.updateUser({
                        firstName: null,
                        lastName: null,
                        timeStamp: (new Date()).getTime()
                    });
                }
                resolve();
            } catch (e) {
                reject(e);
            }
        });
    }

    beforeMount = async () => {
        await this.mountAuthData();
    };

    componentDidMount() {
        this.updateBodyClass();
        window.addEventListener('beforeunload', () => {
            this.setState({
                reload: true
            }, () => {
                sessionStorage.setItem('auth-item', JSON.stringify(this.props.auth));
                sessionStorage.setItem('user', JSON.stringify(this.props.user));
            });
        });
    }

    componentDidUpdate(prevProps) {
        if (prevProps.token && prevProps.token !== this.props.token) {
            console.log('Logout Sucessfully');
            sessionStorage.clear();
            this.props.history.push('/');
        } else if (!prevProps.token && prevProps.token !== this.props.token) {
            console.log('Inside ', prevProps);
            this.setState({
                reload: false
            });
        } else if (!prevProps.ut && prevProps.ut != this.props.ut) {
            console.log('Reload without login ', prevProps);
            this.setState({
                reload: false
            });
        }
        this.updateBodyClass();
    }

    pickClassName = urlString => {
        if (urlString) {
            if (BODY_CLASSES[urlString]) {
                document.body.classList.add(BODY_CLASSES[urlString]);
            } else {
                let pos = urlString.lastIndexOf('?');
                if (pos === '-1') {
                    pos = urlString.lastIndexOf('/');
                }
                let foundStr;
                if (pos > 0) {
                    foundStr = urlString.substring(0, pos);
                } else if (pos === 0) {
                    foundStr = urlString;
                }
                if (foundStr) {
                    this.pickClassName(foundStr);
                }
            }
        }
    };

    updateBodyClass = () => {
        const { location } = this.props;
        const values = Object.values(BODY_CLASSES);
        document.body.classList.remove(...values);
        if (location.pathname !== '/') {
            this.pickClassName(location.pathname.substring(1));
        }
    };

    render() {
        const { token } = this.props;
        if (this.state.reload) return (<AppLoader />);
        else {
            return (
                <Fragment>
                    <Switch>
                        <PrivateRoute path="/dashboard" component={Dashboard} token={token} />
                        <Route path="/login" component={Login} />
                        <Redirect from="/" to="/login" />
                    </Switch>
                </Fragment>
            );
        }
    }
}

App.propTypes = {
    location: PropTypes.object,
    updateAuthState: PropTypes.func,
    authToken: PropTypes.string,
    auth: PropTypes.object,
    token: PropTypes.string,
    history: PropTypes.object,
    updateUser: PropTypes.func,
    ut: PropTypes.any,
    user: PropTypes.object
};

const mapStateToPros = state => {
    return {
        token: state.auth.token,
        auth: state.auth,
        authToken: state.auth.authToken,
        user: state.user,
        ut: state.user.timeStamp
    };
};

const mapDispatchToProps = dispatch => {
    return {
        sessionTokenUpdate: (token) => dispatch(accessToken(token)),
        updateAuthState: (authItem) => dispatch(authDataUpdate(authItem)),
        updateUser: (data) => dispatch(handelUser(data))
    };
};

export default withRouter(connect(mapStateToPros, mapDispatchToProps)(App));

