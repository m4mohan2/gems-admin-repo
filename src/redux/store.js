import { createStore } from 'redux';

import { AppCombineReducers } from './combineReducers';
import { Enhancers } from './enhancer';

export const store = createStore(AppCombineReducers, Enhancers);
