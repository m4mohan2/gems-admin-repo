import {
    PAYMENT_VERIFY_CODE
} from "./actionTypes";


export const paymentVerifyCode = ( data ) => {
    return {
        type: PAYMENT_VERIFY_CODE,
        refCode : data 
    }
}