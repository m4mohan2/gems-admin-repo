import {
    REGISTRATION_PROCESS_STEP1,
    REGISTRATION_PROCESS_STEP2,
    REGISTRATION_PROCESS_STEP3,
    REGISTRATION_PROCESS_STEP3_SUB,
    //ACCESS_TOKEN
} from './actionTypes';

export const registrationProcessStep1 = ( data ) => {
    return {
        type: REGISTRATION_PROCESS_STEP1,
        step1: data 
    };
};

export const registrationProcessStep2 = ( data ) => {
    return {
        type: REGISTRATION_PROCESS_STEP2,
        step2: data 
    };
};

export const registrationProcessStep3 = ( data ) => {
    return {
        type: REGISTRATION_PROCESS_STEP3,
        step3: data
    };
};

export const registrationProcessStep3Sub = (data) => {
    return {
        type : REGISTRATION_PROCESS_STEP3_SUB,
        step3Sub : data
    };
    
};
