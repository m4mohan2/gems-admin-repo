import {
    BUSINESS_DETAILS
} from './actionTypes';

export const handelBusinessIDName = (data) => {
    return {
        type: BUSINESS_DETAILS,
        businessData: data
    };
};
