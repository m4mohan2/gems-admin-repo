import {
    INVOICE_RELOAD_STATE ,
    INVOICE_SUCCESS_STATE , 
    INVOICE_UPLOAD_RESULT , 
    INVOICE_SEARCH_KEY,
    INVOICE_DESCRIPTION,
    INVOICE_FILTER_FROM

} from './actionTypes';

export const invoiceReloadState = ( data ) => {
    return {
        type: INVOICE_RELOAD_STATE,
        reloadState : data
    };
};

export const invoiceSuccessState = ( data ) => {
    return {
        type: INVOICE_SUCCESS_STATE,
        successState : data 
    };
};

export const invoiceUploadResult = ( data ) => {
    return {
        type: INVOICE_UPLOAD_RESULT,
        uploadResult : data 
    };
};

export const invoiceSearchKey = ( data ) => {
    return {
        type: INVOICE_SEARCH_KEY,
        searchKey : data 
    };
};

export const invoiceDescription = (data) => {
    return {
        type: INVOICE_DESCRIPTION,
        description: data
    };
};

export const invoiceFilterFrom = (data) => {
    return {
        type: INVOICE_FILTER_FROM,
        filterFrom: data
    };
};
