import {
    USER_NAME
} from './actionTypes';

export const handelUser = (data) => {
    return {
        type: USER_NAME,
        ...data
    };
};
