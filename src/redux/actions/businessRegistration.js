
import {
    BUSINESS_REGISTRATION_PROCESS_STEP1,
    BUSINESS_REGISTRATION_PROCESS_STEP2,
    BUSINESS_REGISTRATION_PROCESS_STEP3,
    BUSINESS_REGISTRATION_PROCESS_STEP3_SUB,
    BUSINESS_REGISTRATION_PROCESS_CLEANUP
} from './actionTypes';

export const businessRegistrationProcessStep1 = ( data ) => {
    return {
        type: BUSINESS_REGISTRATION_PROCESS_STEP1,
        step1: data 
    };
};

export const businessRegistrationProcessStep2 = ( data ) => {
    return {
        type: BUSINESS_REGISTRATION_PROCESS_STEP2,
        step2: data 
    };
};

export const businessRegistrationProcessStep3 = (data) => {
    return {
        type: BUSINESS_REGISTRATION_PROCESS_STEP3,
        step3: data
    };
};

export const businessRegistrationProcessStep3Sub = (data) => {
    return {
        type: BUSINESS_REGISTRATION_PROCESS_STEP3_SUB,
        step3Sub: data,
        step1: data,
        step3: data

    };
};

export const businessRegistrationProcessCleanup = (data) => {
    return {
        type: BUSINESS_REGISTRATION_PROCESS_CLEANUP,
        step1: data,
        step3: data,
        step3Sub: data,
    };
};
