import { 
    PRODUCT_DESCRIPTION, 
    PRODUCT_HISTORYSTAT, 
    PRODUCT_RESTAT 
} from './actionTypes'; 

export const handleProdInvoiceDes = (data)=>{
    return {
        type: PRODUCT_DESCRIPTION,
        prodpaymode: data.mode,
        prodpaymodeID: data.id,
        prodpayselectedMonth: data.selectedMonth,
        prodpayseletedYear: data.seletedYear,
        paymodeDisable: data.paymodeDisable
    };
};
export const handleProdHisStat = (data) => {
    return {
        type: PRODUCT_HISTORYSTAT,
        statload: data
    };
};

export const handleReStat = (data) => {
    return {
        type: PRODUCT_RESTAT,
        reacload: data
    };
};
