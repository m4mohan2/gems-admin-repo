import {
    BUSINESS_EDIT_PROCESS_STEP1,
    BUSINESS_EDIT_PROCESS_STEP2,
    BUSINESS_EDIT_PROCESS_REST,
    BUSINESS_EDIT_PROCESS_CLEANUP
} from './actionTypes';

export const businessEditProcessStep1 = (data) => {
    return {
        type: BUSINESS_EDIT_PROCESS_STEP1,
        step1: data
    };
};

export const businessEditProcessStep2 = (data) => {
    return {
        type: BUSINESS_EDIT_PROCESS_STEP2,
        step2: data
    };
};

export const businessEditProcessRest = (data) => {
    return {
        type: BUSINESS_EDIT_PROCESS_REST,
        rest: data
    };
};


export const businessEditProcessCleanup = (data) => {
    return {
        type: BUSINESS_EDIT_PROCESS_CLEANUP,
        step1:data,
        step2:data,
        rest:data
    };
};
