import {
    REGISTRATION_PROCESS_STEP1,
    REGISTRATION_PROCESS_STEP2,
    REGISTRATION_PROCESS_STEP3,
    REGISTRATION_PROCESS_STEP3_SUB
} from '../actions';

const initialState = {
    step1: null,
    step2: null,
    step3: null,
    step3sub: null
};
const registrationProcessStep1 = (state, { step1 }) => {
    return {
        ...state,
        step1
    };
};

const registrationProcessStep2 = (state, { step2 }) => {
    return {
        ...state,
        step2
    };
};

const registrationProcessStep3 = (state, { step3 }) => {
    return {
        ...state,
        step3
    };
};

const registrationProcessStep3Sub = (state, {step3Sub}) => {
    return {
        ...state,
        step3Sub
    };
};

const reducer = ( state = initialState, action) => {
    switch( action.type ) {
        case REGISTRATION_PROCESS_STEP1: return registrationProcessStep1( state, action);
        case REGISTRATION_PROCESS_STEP2: return registrationProcessStep2( state, action);
        case REGISTRATION_PROCESS_STEP3: return registrationProcessStep3( state, action);
        case REGISTRATION_PROCESS_STEP3_SUB: return registrationProcessStep3Sub(state, action);
        default: return state;
    }
};

export default reducer;
