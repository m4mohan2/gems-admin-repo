import {
    BUSINESS_EDIT_PROCESS_STEP1,
    BUSINESS_EDIT_PROCESS_STEP2,
    BUSINESS_EDIT_PROCESS_REST,
    BUSINESS_EDIT_PROCESS_CLEANUP

} from '../actions';

const initialState = {
    step1: null,
    step2: null,
    rest:null
};

const businessEditProcessStep1 = (state, { step1 }) => {
    return {
        ...state,
        step1
    };
};

const businessEditProcessStep2 = (state, { step2 }) => {
    return {
        ...state,
        step2
    };
};

const businessEditProcessRest = (state, { rest }) => {
    return {
        ...state,
        rest
    };
};

const businessEditProcessCleanup = (state, { step1, step2, rest }) => {
    return {
        ...state,
        step1,
        step2,
        rest
    };
};


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case BUSINESS_EDIT_PROCESS_STEP1: return businessEditProcessStep1(state, action);
        case BUSINESS_EDIT_PROCESS_STEP2: return businessEditProcessStep2(state, action);
        case BUSINESS_EDIT_PROCESS_REST: return businessEditProcessRest(state, action);
        case BUSINESS_EDIT_PROCESS_CLEANUP: return businessEditProcessCleanup(state, action);

        default: return state;
    }
};

export default reducer;
