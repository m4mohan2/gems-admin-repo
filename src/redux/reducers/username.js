import {
    USER_NAME
} from '../actions';

const initialState = {
    firstName : null,
    lastName: null,
    timeStamp:null
};

const handelUser = (state, { firstName, lastName, timeStamp }) => {
    return {
        ...state,
        firstName,
        lastName,
        timeStamp
    };
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case USER_NAME: return handelUser(state, action);
        default: return state;
    }
};

export default reducer;
