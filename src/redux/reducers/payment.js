import {
    PAYMENT_VERIFY_CODE
} from "../actions";

const initialState = {
    refCode: null
}


const paymentVerifyCode = (state, { refCode }) => {
    return {
        ...state,
        refCode
    }
}

const reducer = ( state = initialState, action) => {
    switch( action.type ) {
        case PAYMENT_VERIFY_CODE: return paymentVerifyCode( state, action);
        default: return state;
    }
}

export default reducer;
