import { PRODUCT_DESCRIPTION, PRODUCT_HISTORYSTAT, PRODUCT_RESTAT} from '../actions';

const initialState = {
    prodpaymode: null,
    prodpaymodeID: 0,//changed
    prodpayselectedMonth:null,
    prodpayseletedYear:null,
    statload:false,
    reacload:false,
    paymodeDisable:false
};
const handleProdInvoiceDes = (state, { prodpaymode, prodpaymodeID, prodpayselectedMonth, prodpayseletedYear, paymodeDisable }) => {
    return {
        ...state,
        prodpaymode,
        prodpaymodeID,
        prodpayselectedMonth,
        prodpayseletedYear,
        paymodeDisable
    };
};

const handleProdHisStat = (state, { statload }) => {
    return {
        ...state,
        statload
    };
};

const handleReStat = (state, { reacload }) => {
    return {
        ...state,
        reacload
    };
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case PRODUCT_DESCRIPTION: return handleProdInvoiceDes(state, action);
        case PRODUCT_HISTORYSTAT: return handleProdHisStat(state, action);
        case PRODUCT_RESTAT: return handleReStat(state, action);
        default: return state;
    }
};

export default reducer;
