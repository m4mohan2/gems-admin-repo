/* eslint-disable no-unused-vars */
import {
    AUTH_START,
    AUTH_FAIL,
    AUTH_SUCCESS,
    AUTH_LOGOUT,
    ACCESS_TOKEN,
    AUTH_LOGOUT_MSG,
    AUTH_ALL_DATA_UPDATE
} from '../actions';

const initialState = {
    token: null,
    userId: 0,
    loading: false,
    error: null,
    authToken: null,
    facility: null
};

const authStart = (state, action) => {
    return {
        ...state,
        loading: true
    };
};

const authFail = (state, action) => {
    return {
        ...state,
        loading: false,
        error: action.error
    };
};

const authSuccess = (state, action) => {
    return {
        ...state,
        loading: false,
        token: action.token,
        userId: action.id
    };
};

const authLogout = (state, action) => {
    return {
        ...state,
        token: null,
        userId: null,
        authToken: null,
        facility: null
    };
};

const logoutMsg = (state, { logoutMsg }) => {
    return {
        ...state,
        logoutMsg
    };
};

const authToken = (state, { authToken, token, facility }) => {
    return {
        ...state,
        authToken,
        token,
        facility
    };
};

const authAllDataUpdate = (state, action) => {
    return {
        ...state,
        ...action.data
    };
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case AUTH_START: return authStart(state, action);
        case AUTH_FAIL: return authFail(state, action);
        case AUTH_SUCCESS: return authSuccess(state, action);
        case AUTH_LOGOUT: return authLogout(state, action);
        case ACCESS_TOKEN: return authToken(state, action);
        case AUTH_LOGOUT_MSG: return logoutMsg(state, action);
        case AUTH_ALL_DATA_UPDATE: return authAllDataUpdate(state, action);
        default: return state;
    }
};

export default reducer;
