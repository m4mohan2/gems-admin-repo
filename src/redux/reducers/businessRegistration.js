import {
    BUSINESS_REGISTRATION_PROCESS_STEP1,
    BUSINESS_REGISTRATION_PROCESS_STEP2,
    BUSINESS_REGISTRATION_PROCESS_STEP3,
    BUSINESS_REGISTRATION_PROCESS_STEP3_SUB,
    BUSINESS_REGISTRATION_PROCESS_CLEANUP
    
} from '../actions';

const initialState = {
    step1: null,
    step2: null,
    step3:null,
    step3Sub: null
};
const businessRegistrationProcessStep1 = (state, { step1 }) => {
    return {
        ...state,
        step1
    };
};

const businessRegistrationProcessStep2 = (state, { step2 }) => {
    return {
        ...state,
        step2
    };
};

const businessRegistrationProcessStep3 = (state, { step3 }) => {
    return {
        ...state,
        step3
    };
};

const businessRegistrationProcessStep3Sub = (state, { step1, step3 }) => {
    return {
        ...state,
        step1,
        step3
    };
};

const businessRegistrationProcessCleanup = (state, { step1, step3, step3Sub }) => {
    return {
        ...state,
        step1,
        step3,
        step3Sub
    };
};


const reducer = ( state = initialState, action) => {
    switch( action.type ) {
        case BUSINESS_REGISTRATION_PROCESS_STEP1: return businessRegistrationProcessStep1( state, action);
        case BUSINESS_REGISTRATION_PROCESS_STEP2: return businessRegistrationProcessStep2( state, action);
        case BUSINESS_REGISTRATION_PROCESS_STEP3: return businessRegistrationProcessStep3(state, action);
        case BUSINESS_REGISTRATION_PROCESS_STEP3_SUB: return businessRegistrationProcessStep3Sub(state, action);
        case BUSINESS_REGISTRATION_PROCESS_CLEANUP: return businessRegistrationProcessCleanup(state, action);

        default: return state;
    }
};

export default reducer;
