import {
    INVOICE_RELOAD_STATE , 
    INVOICE_SUCCESS_STATE , 
    INVOICE_UPLOAD_RESULT , 
    INVOICE_SEARCH_KEY,
    INVOICE_DESCRIPTION,
    INVOICE_FILTER_FROM


} from '../actions';

const initialState = {
    reloadState: false,
    successState: false,
    uploadResult: [],
    searchKey: '',
    description:'',
    filterFrom: ''
};

const invoiceReloadState = (state, { reloadState }) => {
    return {
        ...state,
        reloadState
    };
};

const invoiceSuccessState = (state, { successState }) => {
    return {
        ...state,
        successState
    };
};

const invoiceUploadResult = (state, { uploadResult }) => {
    return {
        ...state,
        uploadResult
    };
};

const invoiceSearchKey = (state, { searchKey }) => {
    return {
        ...state,
        searchKey
    };
};
const invoiceDescription = (state, { description }) => {
    return {
        ...state,
        description
    };
};

const invoiceFilterFrom = (state, { filterFrom }) => {
    return {
        ...state,
        filterFrom
    };
};

const reducer = ( state = initialState, action) => {
    switch( action.type ) {
        case INVOICE_RELOAD_STATE: return invoiceReloadState( state, action);
        case INVOICE_SUCCESS_STATE: return invoiceSuccessState( state, action);
        case INVOICE_UPLOAD_RESULT: return invoiceUploadResult( state, action);
        case INVOICE_SEARCH_KEY: return invoiceSearchKey( state, action);
        case INVOICE_DESCRIPTION: return invoiceDescription( state, action);
        case INVOICE_FILTER_FROM: return invoiceFilterFrom(state, action);
        default: return state;
    }
};

export default reducer;
